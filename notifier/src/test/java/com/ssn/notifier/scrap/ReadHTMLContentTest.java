/**
 * 
 */
package com.ssn.notifier.scrap;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;

import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;

/**
 * @author shantanu.sikdar
 *
 */
class ReadHTMLContentTest {

	/**
	 * Test method for
	 * {@link com.ssn.notifier.scrap.ReadHTMLContent#readHtmlElementContent(java.lang.String, java.lang.String)}.
	 * 
	 * @throws Exception
	 */
	@Test
	void testReadHtmlElementContent() throws Exception {
		String jsonStringDownloaded = getJson("C:\\data\\projects\\shandar_ws\\shandar\\notifier\\src\\main\\resources\\testdata\\json\\hazelFold.json");
		//String jsonStringDownloaded = "{\"name\":\"shantanu\",\"id\":100,\"age\":40}";
		String urlString = "https://www.bellway.co.uk/new-homes/manchester/hazel-fold";
		ReadHTMLContent rhc = new ReadHTMLContent();
		String jsonString = rhc.readHtmlElementContent(urlString, "script").trim();
		//jsonString = "{\"name\":\"shantanu\",\"id\":100,\"age\":40}";
		//System.out.println(jsonString);
		//assertEquals(jsonStringDownloaded, jsonString);
		JSONAssert.assertEquals(jsonStringDownloaded, jsonString, true);
	}

	private String getJson(String relativeFilePath) throws IOException {
		Scanner sc = new Scanner(new FileInputStream(new File(relativeFilePath)));
		StringBuilder jsonStringDownloaded = new StringBuilder();
		while (sc.hasNext()) {
			jsonStringDownloaded.append(sc.next());
		}
		return jsonStringDownloaded.toString();
	}
	
	

}
