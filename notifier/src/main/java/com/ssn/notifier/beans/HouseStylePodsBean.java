package com.ssn.notifier.beans;

import java.util.Arrays;

public class HouseStylePodsBean {

	private ImageBean Image;
	private String Title; // "The Tailor",
	private Boolean IsArtisan; // false,
	private String OverlayText; // "",
	private String MinPrice; // "234995",
	private String MaxPrice; // "234995",
	private String[] Bedrooms; // [],
	private String MinBedrooms; // "3",
	private String MaxBedrooms; // "3",
	private BenefitsPanelBeans BenefitsPanel; // {},
	private String Url; // "/new-homes/manchester/hazel-fold/the-tailor-3-bedroom-semi-detached-home",
	private String HouseType; // "Semi-Detached",
	private String SalesStatus; // "RSAL",
	private String ShowHomeActive; // false,
	private PlotsBean Plots;
	private String UmbracoHouseStyleId; // 205125

	public ImageBean getImage() {
		return Image;
	}

	public void setImage(ImageBean image) {
		Image = image;
	}

	public String getTitle() {
		return Title;
	}

	public void setTitle(String title) {
		Title = title;
	}

	public Boolean getIsArtisan() {
		return IsArtisan;
	}

	public void setIsArtisan(Boolean isArtisan) {
		IsArtisan = isArtisan;
	}

	public String getOverlayText() {
		return OverlayText;
	}

	public void setOverlayText(String overlayText) {
		OverlayText = overlayText;
	}

	public String getMinPrice() {
		return MinPrice;
	}

	public void setMinPrice(String minPrice) {
		MinPrice = minPrice;
	}

	public String getMaxPrice() {
		return MaxPrice;
	}

	public void setMaxPrice(String maxPrice) {
		MaxPrice = maxPrice;
	}

	public String[] getBedrooms() {
		return Bedrooms;
	}

	public void setBedrooms(String[] bedrooms) {
		Bedrooms = bedrooms;
	}

	public String getMinBedrooms() {
		return MinBedrooms;
	}

	public void setMinBedrooms(String minBedrooms) {
		MinBedrooms = minBedrooms;
	}

	public String getMaxBedrooms() {
		return MaxBedrooms;
	}

	public void setMaxBedrooms(String maxBedrooms) {
		MaxBedrooms = maxBedrooms;
	}

	public BenefitsPanelBeans getBenefitsPanel() {
		return BenefitsPanel;
	}

	public void setBenefitsPanel(BenefitsPanelBeans benefitsPanel) {
		BenefitsPanel = benefitsPanel;
	}

	public String getUrl() {
		return Url;
	}

	public void setUrl(String url) {
		Url = url;
	}

	public String getHouseType() {
		return HouseType;
	}

	public void setHouseType(String houseType) {
		HouseType = houseType;
	}

	public String getSalesStatus() {
		return SalesStatus;
	}

	public void setSalesStatus(String salesStatus) {
		SalesStatus = salesStatus;
	}

	public String getShowHomeActive() {
		return ShowHomeActive;
	}

	public void setShowHomeActive(String showHomeActive) {
		ShowHomeActive = showHomeActive;
	}

	public PlotsBean getPlots() {
		return Plots;
	}

	public void setPlots(PlotsBean plots) {
		Plots = plots;
	}

	public String getUmbracoHouseStyleId() {
		return UmbracoHouseStyleId;
	}

	public void setUmbracoHouseStyleId(String umbracoHouseStyleId) {
		UmbracoHouseStyleId = umbracoHouseStyleId;
	}

	@Override
	public String toString() {
		return "HouseStylePodsBean [Image=" + Image + ", Title=" + Title + ", IsArtisan=" + IsArtisan + ", OverlayText="
				+ OverlayText + ", MinPrice=" + MinPrice + ", MaxPrice=" + MaxPrice + ", Bedrooms="
				+ Arrays.toString(Bedrooms) + ", MinBedrooms=" + MinBedrooms + ", MaxBedrooms=" + MaxBedrooms
				+ ", BenefitsPanel=" + BenefitsPanel + ", Url=" + Url + ", HouseType=" + HouseType + ", SalesStatus="
				+ SalesStatus + ", ShowHomeActive=" + ShowHomeActive + ", Plots=" + Plots + ", UmbracoHouseStyleId="
				+ UmbracoHouseStyleId + "]";
	}

}
