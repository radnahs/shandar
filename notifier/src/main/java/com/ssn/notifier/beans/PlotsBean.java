package com.ssn.notifier.beans;

import java.util.List;

public class PlotsBean {

	private String DevelopmentId;
	private String MasterHouseStyleId;
	private List<PlotBean> Plots;
	private String MinPrice;
	private String MaxPrice;
	private List<String> NumberOfBedrooms;
	private List<String> HouseTypes;
	private String HouseType;
	private String WebsiteMarketingName;

	public String getDevelopmentId() {
		return DevelopmentId;
	}

	public void setDevelopmentId(String developmentId) {
		DevelopmentId = developmentId;
	}

	public String getMasterHouseStyleId() {
		return MasterHouseStyleId;
	}

	public void setMasterHouseStyleId(String masterHouseStyleId) {
		MasterHouseStyleId = masterHouseStyleId;
	}

	public List<PlotBean> getPlots() {
		return Plots;
	}

	public void setPlots(List<PlotBean> plots) {
		Plots = plots;
	}

	public String getMinPrice() {
		return MinPrice;
	}

	public void setMinPrice(String minPrice) {
		MinPrice = minPrice;
	}

	public String getMaxPrice() {
		return MaxPrice;
	}

	public void setMaxPrice(String maxPrice) {
		MaxPrice = maxPrice;
	}

	public List<String> getNumberOfBedrooms() {
		return NumberOfBedrooms;
	}

	public void setNumberOfBedrooms(List<String> numberOfBedrooms) {
		NumberOfBedrooms = numberOfBedrooms;
	}

	public List<String> getHouseTypes() {
		return HouseTypes;
	}

	public void setHouseTypes(List<String> houseTypes) {
		HouseTypes = houseTypes;
	}

	public String getHouseType() {
		return HouseType;
	}

	public void setHouseType(String houseType) {
		HouseType = houseType;
	}

	public String getWebsiteMarketingName() {
		return WebsiteMarketingName;
	}

	public void setWebsiteMarketingName(String websiteMarketingName) {
		WebsiteMarketingName = websiteMarketingName;
	}

	@Override
	public String toString() {
		return "PlotsBean [DevelopmentId=" + DevelopmentId + ", MasterHouseStyleId=" + MasterHouseStyleId + ", Plots="
				+ Plots + ", MinPrice=" + MinPrice + ", MaxPrice=" + MaxPrice + ", NumberOfBedrooms=" + NumberOfBedrooms
				+ ", HouseTypes=" + HouseTypes + ", HouseType=" + HouseType + ", WebsiteMarketingName="
				+ WebsiteMarketingName + "]";
	}

}
