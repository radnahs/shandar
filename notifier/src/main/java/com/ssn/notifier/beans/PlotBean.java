/**
 * 
 */
package com.ssn.notifier.beans;

/**
 * @author shantanu.sikdar
 *
 */
public class PlotBean {

	private String DivisionId; // d0e78ca2-86fc-4b6c-894d-e654ac8f0eab",
	private String DivisionName; // Manchester",
	private String DevelopmentId; // 67ef119e-cf5e-4cae-8d6b-abce00c5c723",
	private String DevelopmentName; // Hazel Fold",
	private String PhaseId; // ab75c473-44e6-4d11-aa2f-abce00c5c723",
	private Boolean PlotWebVisible; // true,
	private String PhaseName; // Phase 1",
	private String PlotId; // 3c4f6f65-e87b-4253-9c2a-ac71009cca6c",
	private String PlotNumber; // 150,
	private String PlotDisplay; // 150 ",
	private String HouseStyleName; // The Tailor",
	private Boolean ShowHome; // false,
	private Boolean ViewHome; // false,
	private String ReleasePrice; // 0,
	private String NumberOfBedrooms; // 3,
	private Boolean IsCurrentlyReleasedForSale; // false,
	private String HouseType; // Semi-Detached",
	private String HouseStyleId; // ef3abe87-af5e-4a5e-9159-9b7f7cfa7b99",
	private String Address1; // 32 Hercules Road",
	private String Address2; // Hercules Road",
	private String Address3; // Lostock",
	private String Town; // Bolton",
	private String County; // ",
	private String PostCode; // BL6 4NS",
	private String SalesStatus; // NREL",
	private Boolean Section106; // false,
	private Boolean Affordable106; // false

	public String getDivisionId() {
		return DivisionId;
	}

	public void setDivisionId(String divisionId) {
		DivisionId = divisionId;
	}

	public String getDivisionName() {
		return DivisionName;
	}

	public void setDivisionName(String divisionName) {
		DivisionName = divisionName;
	}

	public String getDevelopmentId() {
		return DevelopmentId;
	}

	public void setDevelopmentId(String developmentId) {
		DevelopmentId = developmentId;
	}

	public String getDevelopmentName() {
		return DevelopmentName;
	}

	public void setDevelopmentName(String developmentName) {
		DevelopmentName = developmentName;
	}

	public String getPhaseId() {
		return PhaseId;
	}

	public void setPhaseId(String phaseId) {
		PhaseId = phaseId;
	}

	public Boolean getPlotWebVisible() {
		return PlotWebVisible;
	}

	public void setPlotWebVisible(Boolean plotWebVisible) {
		PlotWebVisible = plotWebVisible;
	}

	public String getPhaseName() {
		return PhaseName;
	}

	public void setPhaseName(String phaseName) {
		PhaseName = phaseName;
	}

	public String getPlotId() {
		return PlotId;
	}

	public void setPlotId(String plotId) {
		PlotId = plotId;
	}

	public String getPlotNumber() {
		return PlotNumber;
	}

	public void setPlotNumber(String plotNumber) {
		PlotNumber = plotNumber;
	}

	public String getPlotDisplay() {
		return PlotDisplay;
	}

	public void setPlotDisplay(String plotDisplay) {
		PlotDisplay = plotDisplay;
	}

	public String getHouseStyleName() {
		return HouseStyleName;
	}

	public void setHouseStyleName(String houseStyleName) {
		HouseStyleName = houseStyleName;
	}

	public Boolean getShowHome() {
		return ShowHome;
	}

	public void setShowHome(Boolean showHome) {
		ShowHome = showHome;
	}

	public Boolean getViewHome() {
		return ViewHome;
	}

	public void setViewHome(Boolean viewHome) {
		ViewHome = viewHome;
	}

	public String getReleasePrice() {
		return ReleasePrice;
	}

	public void setReleasePrice(String releasePrice) {
		ReleasePrice = releasePrice;
	}

	public String getNumberOfBedrooms() {
		return NumberOfBedrooms;
	}

	public void setNumberOfBedrooms(String numberOfBedrooms) {
		NumberOfBedrooms = numberOfBedrooms;
	}

	public Boolean getIsCurrentlyReleasedForSale() {
		return IsCurrentlyReleasedForSale;
	}

	public void setIsCurrentlyReleasedForSale(Boolean isCurrentlyReleasedForSale) {
		IsCurrentlyReleasedForSale = isCurrentlyReleasedForSale;
	}

	public String getHouseType() {
		return HouseType;
	}

	public void setHouseType(String houseType) {
		HouseType = houseType;
	}

	public String getHouseStyleId() {
		return HouseStyleId;
	}

	public void setHouseStyleId(String houseStyleId) {
		HouseStyleId = houseStyleId;
	}

	public String getAddress1() {
		return Address1;
	}

	public void setAddress1(String address1) {
		Address1 = address1;
	}

	public String getAddress2() {
		return Address2;
	}

	public void setAddress2(String address2) {
		Address2 = address2;
	}

	public String getAddress3() {
		return Address3;
	}

	public void setAddress3(String address3) {
		Address3 = address3;
	}

	public String getTown() {
		return Town;
	}

	public void setTown(String town) {
		Town = town;
	}

	public String getCounty() {
		return County;
	}

	public void setCounty(String county) {
		County = county;
	}

	public String getPostCode() {
		return PostCode;
	}

	public void setPostCode(String postCode) {
		PostCode = postCode;
	}

	public String getSalesStatus() {
		return SalesStatus;
	}

	public void setSalesStatus(String salesStatus) {
		SalesStatus = salesStatus;
	}

	public Boolean getSection106() {
		return Section106;
	}

	public void setSection106(Boolean section106) {
		Section106 = section106;
	}

	public Boolean getAffordable106() {
		return Affordable106;
	}

	public void setAffordable106(Boolean affordable106) {
		Affordable106 = affordable106;
	}

	@Override
	public String toString() {
		return "PlotBean [DivisionId=" + DivisionId + ", DivisionName=" + DivisionName + ", DevelopmentId="
				+ DevelopmentId + ", DevelopmentName=" + DevelopmentName + ", PhaseId=" + PhaseId + ", PlotWebVisible="
				+ PlotWebVisible + ", PhaseName=" + PhaseName + ", PlotId=" + PlotId + ", PlotNumber=" + PlotNumber
				+ ", PlotDisplay=" + PlotDisplay + ", HouseStyleName=" + HouseStyleName + ", ShowHome=" + ShowHome
				+ ", ViewHome=" + ViewHome + ", ReleasePrice=" + ReleasePrice + ", NumberOfBedrooms=" + NumberOfBedrooms
				+ ", IsCurrentlyReleasedForSale=" + IsCurrentlyReleasedForSale + ", HouseType=" + HouseType
				+ ", HouseStyleId=" + HouseStyleId + ", Address1=" + Address1 + ", Address2=" + Address2 + ", Address3="
				+ Address3 + ", Town=" + Town + ", County=" + County + ", PostCode=" + PostCode + ", SalesStatus="
				+ SalesStatus + ", Section106=" + Section106 + ", Affordable106=" + Affordable106 + "]";
	}

	
}
