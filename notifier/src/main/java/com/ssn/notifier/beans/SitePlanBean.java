package com.ssn.notifier.beans;

public class SitePlanBean {

	private String Label; // "",
	private SiteBean Site;

	public String getLabel() {
		return Label;
	}

	public void setLabel(String label) {
		Label = label;
	}

	public SiteBean getSite() {
		return Site;
	}

	public void setSite(SiteBean site) {
		Site = site;
	}

	@Override
	public String toString() {
		return "SitePlanBean [Label=" + Label + ", Site=" + Site + "]";
	}

}
