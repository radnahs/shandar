package com.ssn.notifier.beans;

import java.util.List;

public class SitePlansBean {

	private List<SitePlanBean> SitePlans;
	private String DevelopmentTitle; // "Hazel Fold",
	private String SitePlanTitle; // "",
	private String SitePlanDisclaimerText; // "The site plan is drawn to show the relative position of individual
											// properties, NOT TO SCALE. This is a two dimensional drawing and will not
											// show land contours and gradients, boundary treatments, landscaping or
											// local authority street lighting. Footpaths subject to change. For details
											// of individual properties and availability please refer to our Sales
											// Advisor.",
	private Boolean IsPopulated; // true,
	private String Theme; // "Bellway"

	public List<SitePlanBean> getSitePlans() {
		return SitePlans;
	}

	public void setSitePlans(List<SitePlanBean> sitePlans) {
		SitePlans = sitePlans;
	}

	public String getDevelopmentTitle() {
		return DevelopmentTitle;
	}

	public void setDevelopmentTitle(String developmentTitle) {
		DevelopmentTitle = developmentTitle;
	}

	public String getSitePlanTitle() {
		return SitePlanTitle;
	}

	public void setSitePlanTitle(String sitePlanTitle) {
		SitePlanTitle = sitePlanTitle;
	}

	public String getSitePlanDisclaimerText() {
		return SitePlanDisclaimerText;
	}

	public void setSitePlanDisclaimerText(String sitePlanDisclaimerText) {
		SitePlanDisclaimerText = sitePlanDisclaimerText;
	}

	public Boolean getIsPopulated() {
		return IsPopulated;
	}

	public void setIsPopulated(Boolean isPopulated) {
		IsPopulated = isPopulated;
	}

	public String getTheme() {
		return Theme;
	}

	public void setTheme(String theme) {
		Theme = theme;
	}

	/**
	@Override
	public String toString() {
		return "SitePlansBean [SitePlans=" + SitePlans + ", DevelopmentTitle=" + DevelopmentTitle + ", SitePlanTitle="
				+ SitePlanTitle + ", SitePlanDisclaimerText=" + SitePlanDisclaimerText + ", IsPopulated=" + IsPopulated
				+ ", Theme=" + Theme + "]";
	}
	*/

}
