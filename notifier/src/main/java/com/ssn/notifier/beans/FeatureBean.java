package com.ssn.notifier.beans;

public class FeatureBean {

	private String Information; // "Open-plan kitchen, dining and family area",
	private String Category; // null

	public String getInformation() {
		return Information;
	}

	public void setInformation(String information) {
		Information = information;
	}

	public String getCategory() {
		return Category;
	}

	public void setCategory(String category) {
		Category = category;
	}

	@Override
	public String toString() {
		return "FeatureBean [Information=" + Information + ", Category=" + Category + "]";
	}

}
