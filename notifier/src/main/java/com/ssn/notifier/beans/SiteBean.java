package com.ssn.notifier.beans;

public class SiteBean {

	private String ImageUrl; // "/media/57051/hazel_fold_site_plan_2048px.jpg",
	private String AltText; // ""

	public String getImageUrl() {
		return ImageUrl;
	}

	public void setImageUrl(String imageUrl) {
		ImageUrl = imageUrl;
	}

	public String getAltText() {
		return AltText;
	}

	public void setAltText(String altText) {
		AltText = altText;
	}

	@Override
	public String toString() {
		return "SiteBean [ImageUrl=" + ImageUrl + ", AltText=" + AltText + "]";
	}

}
