package com.ssn.notifier.beans;

public class ImageBean {

	private String ImageUrl; /// media/50721/tailor_tacb105e01s02b_2048px.jpg",
	private String AltText; // "

	public String getImageUrl() {
		return ImageUrl;
	}

	public void setImageUrl(String imageUrl) {
		ImageUrl = imageUrl;
	}

	public String getAltText() {
		return AltText;
	}

	public void setAltText(String altText) {
		AltText = altText;
	}

	@Override
	public String toString() {
		return "ImageBean [ImageUrl=" + ImageUrl + ", AltText=" + AltText + "]";
	}

}
