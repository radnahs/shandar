package com.ssn.notifier.beans;

import java.util.List;

public class MainBean {

	private String DevelopmentName;
	private List<HouseStylePodsBean> HouseStylePods;
	private String MaxPriceAvailable; // 304995,
	private String MinPriceAvailable; // 0,
	private String MaxBedAvailable; // 4,
	private String MinBedAvailable; // 1,
	private Boolean IsSoldOut; // false,
	private SitePlansBean SitePlans; //
	private String DevelopmentNodeId; // 0,
	private Boolean IsPopulated; // false,
	private String Theme; // "Bellway"

	public String getDevelopmentName() {
		return DevelopmentName;
	}

	public void setDevelopmentName(String developmentName) {
		DevelopmentName = developmentName;
	}

	public List<HouseStylePodsBean> getHouseStylePods() {
		return HouseStylePods;
	}

	public void setHouseStylePods(List<HouseStylePodsBean> houseStylePods) {
		HouseStylePods = houseStylePods;
	}

	public String getMaxPriceAvailable() {
		return MaxPriceAvailable;
	}

	public void setMaxPriceAvailable(String maxPriceAvailable) {
		MaxPriceAvailable = maxPriceAvailable;
	}

	public String getMinPriceAvailable() {
		return MinPriceAvailable;
	}

	public void setMinPriceAvailable(String minPriceAvailable) {
		MinPriceAvailable = minPriceAvailable;
	}

	public String getMaxBedAvailable() {
		return MaxBedAvailable;
	}

	public void setMaxBedAvailable(String maxBedAvailable) {
		MaxBedAvailable = maxBedAvailable;
	}

	public String getMinBedAvailable() {
		return MinBedAvailable;
	}

	public void setMinBedAvailable(String minBedAvailable) {
		MinBedAvailable = minBedAvailable;
	}

	public Boolean getIsSoldOut() {
		return IsSoldOut;
	}

	public void setIsSoldOut(Boolean isSoldOut) {
		IsSoldOut = isSoldOut;
	}

	public SitePlansBean getSitePlans() {
		return SitePlans;
	}

	public void setSitePlans(SitePlansBean sitePlans) {
		SitePlans = sitePlans;
	}

	public String getDevelopmentNodeId() {
		return DevelopmentNodeId;
	}

	public void setDevelopmentNodeId(String developmentNodeId) {
		DevelopmentNodeId = developmentNodeId;
	}

	public Boolean getIsPopulated() {
		return IsPopulated;
	}

	public void setIsPopulated(Boolean isPopulated) {
		IsPopulated = isPopulated;
	}

	public String getTheme() {
		return Theme;
	}

	public void setTheme(String theme) {
		Theme = theme;
	}

	@Override
	public String toString() {
		return "MainBean [DevelopmentName=" + DevelopmentName + ", HouseStylePods=" + HouseStylePods
				+ ", MaxPriceAvailable=" + MaxPriceAvailable + ", MinPriceAvailable=" + MinPriceAvailable
				+ ", MaxBedAvailable=" + MaxBedAvailable + ", MinBedAvailable=" + MinBedAvailable + ", IsSoldOut="
				+ IsSoldOut + ", SitePlans=" + SitePlans + ", DevelopmentNodeId=" + DevelopmentNodeId + ", IsPopulated="
				+ IsPopulated + ", Theme=" + Theme + "]";
	}

}