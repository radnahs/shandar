package com.ssn.notifier.beans;

import java.util.Arrays;
import java.util.List;

public class BenefitsPanelBeans {

	private String Title; // "House style features ",
	private String Image; // null,
	private List<FeatureBean> Features; //
	private String UsefulToKnowHeading; // null,
	private String[] UsefulToKnow; // [],
	private String SpecialDevelopmentColour; // null,
	private Boolean IsHouseStyle; // true,
	private String CustomUsefulToKnow; // null,
	private Boolean IsPopulated; // true,
	private String Theme; // "Bellway"

	public String getTitle() {
		return Title;
	}

	public void setTitle(String title) {
		Title = title;
	}

	public String getImage() {
		return Image;
	}

	public void setImage(String image) {
		Image = image;
	}

	public List<FeatureBean> getFeatures() {
		return Features;
	}

	public void setFeatures(List<FeatureBean> features) {
		Features = features;
	}

	public String getUsefulToKnowHeading() {
		return UsefulToKnowHeading;
	}

	public void setUsefulToKnowHeading(String usefulToKnowHeading) {
		UsefulToKnowHeading = usefulToKnowHeading;
	}

	public String[] getUsefulToKnow() {
		return UsefulToKnow;
	}

	public void setUsefulToKnow(String[] usefulToKnow) {
		UsefulToKnow = usefulToKnow;
	}

	public String getSpecialDevelopmentColour() {
		return SpecialDevelopmentColour;
	}

	public void setSpecialDevelopmentColour(String specialDevelopmentColour) {
		SpecialDevelopmentColour = specialDevelopmentColour;
	}

	public Boolean getIsHouseStyle() {
		return IsHouseStyle;
	}

	public void setIsHouseStyle(Boolean isHouseStyle) {
		IsHouseStyle = isHouseStyle;
	}

	public String getCustomUsefulToKnow() {
		return CustomUsefulToKnow;
	}

	public void setCustomUsefulToKnow(String customUsefulToKnow) {
		CustomUsefulToKnow = customUsefulToKnow;
	}

	public Boolean getIsPopulated() {
		return IsPopulated;
	}

	public void setIsPopulated(Boolean isPopulated) {
		IsPopulated = isPopulated;
	}

	public String getTheme() {
		return Theme;
	}

	public void setTheme(String theme) {
		Theme = theme;
	}

	/**
	@Override
	public String toString() {
		return "BenefitsPanelBeans [Title=" + Title + ", Image=" + Image + ", Features=" + Features
				+ ", UsefulToKnowHeading=" + UsefulToKnowHeading + ", UsefulToKnow=" + Arrays.toString(UsefulToKnow)
				+ ", SpecialDevelopmentColour=" + SpecialDevelopmentColour + ", IsHouseStyle=" + IsHouseStyle
				+ ", CustomUsefulToKnow=" + CustomUsefulToKnow + ", IsPopulated=" + IsPopulated + ", Theme=" + Theme
				+ "]";
	}
	*/

}
