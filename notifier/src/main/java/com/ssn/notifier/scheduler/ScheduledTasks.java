/**
 * <p>Project: notifier </p>
 * <p>Package Name: com.ssn.notifier.scheduler </p>
 * <p>File Name: ScheduledTasks.java </p>
 * <p>Create Date: 24-Sep-2021 </p>
 * <p>Create Time: 11:04:19 AM </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.ssn.notifier.scheduler;

import java.time.LocalDateTime;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.ssn.notifier.service.NotifierService;

/**
 * @author : Shantanu Sikdar
 *
 */
@Component
public class ScheduledTasks {

	private static final Logger logger = LoggerFactory.getLogger(ScheduledTasks.class);

	@Autowired
	private Environment env;

	// @Scheduled(fixedRate = 120000) // every 2 minutes
	@Scheduled(fixedRate = 600000) // every 10 minutes
	public void performTask10Minute() {
		System.out.println("Checked every 10 minutes " + LocalDateTime.now());
		String urlString = env.getProperty("site.toScrap");
		String houseType = env.getProperty("schedule.houseType");
		String[] houseTypeArray = houseType.split(",");
		final String fromEmail = env.getProperty("email.fromEmail");
		final String toAddressList = env.getProperty("email.toAddressList");
		final String toCCAddressList = env.getProperty("email.toCCAddressList");
		final String toBCCAddressList = env.getProperty("email.toBCCAddressList");
		NotifierService ns = new NotifierService();
		try {
			ns.processSuccessData(urlString, Arrays.asList(houseTypeArray), fromEmail, toAddressList, toCCAddressList,
					toBCCAddressList);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	// @Scheduled(fixedRate = 600000)//every 10 minutes
	@Scheduled(fixedRate = 86400000) // everyday
	public void performTaskDaily() {
		System.out.println("Daily status at :- " + LocalDateTime.now());
		String urlString = env.getProperty("site.toScrap");
		String houseType = env.getProperty("schedule.houseType");
		String[] houseTypeArray = houseType.split(",");
		final String fromEmail = env.getProperty("email.fromEmail");
		final String toAddressList = env.getProperty("email.toAddressList");
		final String toCCAddressList = env.getProperty("email.toCCAddressList");
		final String toBCCAddressList = env.getProperty("email.toBCCAddressList");
		NotifierService ns = new NotifierService();
		try {
			ns.processDailyData(urlString, Arrays.asList(houseTypeArray), fromEmail, toAddressList, toCCAddressList,
					toBCCAddressList);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	@Scheduled(fixedRate = 3600000) // every hour
	public void performTaskHourly() {
		System.out.println("Checked every Hour " + LocalDateTime.now());
		String urlString = env.getProperty("site.toScrap");
		String houseType = env.getProperty("schedule.houseType");
		String[] houseTypeArray = houseType.split(",");
		final String fromEmail = env.getProperty("email.fromEmail");
		final String toAddressList = env.getProperty("email.toCCAddressList");
		final String toCCAddressList = "";
		final String toBCCAddressList = "";
		NotifierService ns = new NotifierService();
		try {
			ns.processDailyData(urlString, Arrays.asList(houseTypeArray), fromEmail, toAddressList, toCCAddressList,
					toBCCAddressList);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

}
