/**
 * <p>Project: notifier </p>
 * <p>Package Name: com.ssn.notifier.controller </p>
 * <p>File Name: NotifierController.java </p>
 * <p>Create Date: 21-Sep-2021 </p>
 * <p>Create Time: 6:19:51 PM </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.ssn.notifier.controller;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ssn.notifier.service.NotifierService;

/**
 * @author : Shantanu Sikdar
 *
 */

@RestController
public class NotifierController {

	private static final Logger logger = LoggerFactory.getLogger(NotifierController.class);

	@Autowired
	private Environment env;

	@Autowired
	private NotifierService ns;

	@RequestMapping("/dashboard")
	public String dashboard() throws Exception {
		final String urlString = env.getProperty("site.toScrap");
		String houseType = env.getProperty("schedule.houseType");
		String[] houseTypeArray = houseType.split(",");
		final String fromEmail = env.getProperty("email.fromEmail");
		final String toAddressList = env.getProperty("email.toAddressList");
		final String toCCAddressList = env.getProperty("email.toCCAddressList");
		final String toBCCAddressList = env.getProperty("email.toBCCAddressList");
		String processDailyData = ns.processDailyData(urlString, Arrays.asList(houseTypeArray), fromEmail,
				toAddressList, toCCAddressList, toBCCAddressList);
		return processDailyData;
	}

}
