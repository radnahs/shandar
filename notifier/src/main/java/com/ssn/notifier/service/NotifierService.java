package com.ssn.notifier.service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.ssn.notifier.beans.HouseStylePodsBean;
import com.ssn.notifier.beans.MainBean;
import com.ssn.notifier.beans.PlotBean;
import com.ssn.notifier.beans.PlotsBean;
import com.ssn.notifier.email.EmailSSL;
import com.ssn.notifier.mapper.JsonToObjectMapper;
import com.ssn.notifier.scrap.ReadHTMLContent;
import com.ssn.notifier.vo.ViewPlotData;

@Service
public class NotifierService {

	private static final Logger logger = LoggerFactory.getLogger(NotifierService.class);

	@Autowired
	private Environment env;

	private MainBean formatData(String urlString) throws Exception {
		ReadHTMLContent rhc = new ReadHTMLContent();
		String jsonString = rhc.readHtmlElementContent(urlString, "script");
		JsonToObjectMapper jtom = new JsonToObjectMapper();
		MainBean mainBean = jtom.parseJsonToObject(jsonString);
		return mainBean;
	}

	private Map<String, ViewPlotData> selectAndFormatPlotsData(String urlString, MainBean mainBean,
			List<String> houseStyleToShow) {
		Map<String, ViewPlotData> mapSelectAndFormatPlots = new HashMap<String, ViewPlotData>();
		List<String> listViewPlots = new ArrayList<String>();
		List<String> availableListViewPlots = new ArrayList<String>();
		String emailBodyStart = "<html><style>table, th, td {border: 1px solid black;}</style>"
				+ "<body><h2>Hazel Fold</h2><table><tr><th>Plot</th><th>House Type</th><th>PRICE</th></tr>";
		listViewPlots.add(emailBodyStart);
		StringBuilder successSbldrMailBody = new StringBuilder(emailBodyStart);
		StringBuilder dailySbldrMailBody = new StringBuilder(emailBodyStart);

		for (int i = 0; i < mainBean.getHouseStylePods().size(); i++) {
			HouseStylePodsBean hsp = mainBean.getHouseStylePods().get(i);
			PlotsBean plotsBean = hsp.getPlots();
			for (int j = 0; j < plotsBean.getPlots().size(); j++) {
				PlotBean plot = plotsBean.getPlots().get(j);
				String mailBody = "<tr><td>Plot -" + plot.getPlotNumber() + "</td><td>" + plot.getHouseStyleName()
						+ "</td><td>"
						+ (Integer.parseInt(plot.getReleasePrice()) == 0 ? "Awaiting Release"
								: "<strong style=\"color:red;\">" + plot.getReleasePrice() + "</strong>")
						+ "</td></tr>";
				if (!houseStyleToShow.isEmpty()) {
					for (int k = 0; k < houseStyleToShow.size(); k++) {
						if (plot.getHouseStyleName().equals(houseStyleToShow.get(k))) {

							if (Integer.parseInt(plot.getReleasePrice()) != 0) {
								availableListViewPlots.add(mailBody);
								successSbldrMailBody.append(mailBody);
								logger.debug(mailBody);
							}
							listViewPlots.add(mailBody);
							dailySbldrMailBody.append(mailBody);
						}
					}
				} else {
					listViewPlots.add(mailBody);
					dailySbldrMailBody.append(mailBody);
				}
			}
		}
		String emailBodyEnd = "</table><p>Please click on the <a href=" + urlString
				+ ">Hazel Fold</a> link to see the status</p></body></html>";
		successSbldrMailBody.append(emailBodyEnd);
		dailySbldrMailBody.append(emailBodyEnd);
		if (availableListViewPlots.size() != 0) {
			ViewPlotData vpdSuccess = buildViewPlotData("SUCCESS", availableListViewPlots, successSbldrMailBody);
			mapSelectAndFormatPlots.put("SUCCESS", vpdSuccess);
		}
		ViewPlotData vpdDaily = buildViewPlotData("DAILY", listViewPlots, dailySbldrMailBody);
		mapSelectAndFormatPlots.put("DAILY", vpdDaily);
		return mapSelectAndFormatPlots;
	}

	private ViewPlotData buildViewPlotData(String id, List<String> listViewPlots, StringBuilder sbHtmlBody) {
		ViewPlotData vpd = new ViewPlotData();
		vpd.setId(id);
		vpd.setListViewPlots(listViewPlots);
		vpd.setSbHtmlBody(sbHtmlBody);
		return vpd;
	}

	private int counter = 0;

	public void processSuccessData(String urlString, List<String> houseStyleToShow, final String fromEmail,
			final String toAddressList, final String toCCAddressList, final String toBCCAddressList) throws Exception {

		MainBean mainBean = formatData(urlString);
		Map<String, ViewPlotData> selectedAndFormattedPlotsData = selectAndFormatPlotsData(urlString, mainBean,
				houseStyleToShow);
		String atTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("EEEE, MMM dd, yyyy 'at' HH:mm:ss a"));
		if (selectedAndFormattedPlotsData.containsKey("SUCCESS")) {
			ViewPlotData vpd = selectedAndFormattedPlotsData.get("SUCCESS");
			if (vpd.getListViewPlots().size() != 0 && counter % 10 == 0) {
				new EmailSSL().sendEmail(fromEmail, toAddressList, toCCAddressList, toBCCAddressList,
						"Avaliable Noification Hazel Fold at " + atTime, vpd.getSbHtmlBody().toString());
				// logger.debug("successSbldrMailBody : " + vpd.getSbHtmlBody().toString());
				logger.info("successSbldrMailBody : " + vpd.getSbHtmlBody().toString());
				if (counter < 100) {
					counter += 10;
				} else {
					counter++;
				}
			}
		}
	}

	public String processDailyData(String urlString, List<String> houseStyleToShow, final String fromEmail,
			final String toAddressList, final String toCCAddressList, final String toBCCAddressList) throws Exception {

		MainBean mainBean = formatData(urlString);
		Map<String, ViewPlotData> selectedAndFormattedPlotsData = selectAndFormatPlotsData(urlString, mainBean,
				houseStyleToShow);
		ViewPlotData vpd = new ViewPlotData();
		String atTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("EEEE, MMM dd, yyyy 'at' HH:mm:ss a"));
		if (selectedAndFormattedPlotsData.containsKey("DAILY")) {
			vpd = selectedAndFormattedPlotsData.get("DAILY");
			new EmailSSL().sendEmail(fromEmail, toAddressList, toCCAddressList, toBCCAddressList,
					"Status of Hazel Fold at " + atTime, vpd.getSbHtmlBody().toString());
			// logger.debug("DailySbldrMailBody : " + vpd.getSbHtmlBody().toString());
			logger.info("DailySbldrMailBody : " + vpd.getSbHtmlBody().toString());
		}
		return vpd.getSbHtmlBody().toString();
	}

}
