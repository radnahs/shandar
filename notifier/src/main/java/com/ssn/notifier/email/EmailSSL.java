/**
 * <p>Project: notifier </p>
 * <p>Package Name: com.ssn.notifier.email </p>
 * <p>File Name: EmailSSL.java </p>
 * <p>Create Date: 24-Sep-2021 </p>
 * <p>Create Time: 9:28:35 AM </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.ssn.notifier.email;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 * @author : Shantanu Sikdar
 *
 */
@Component
public class EmailSSL {

	@Autowired
	private Environment env;
	
	public void sendEmail(final String fromEmail, final String toAddressList, final String toCCAddressList, final String toBCCAddressList, 
			String emailSubject, String emailBody) {
		final String password = "Pune@411012";//Pune@411012
		
		Properties prop = new Properties();
		prop.put("mail.smtp.host", "smtp.gmail.com");
		prop.put("mail.smtp.port", "465");
		prop.put("mail.smtp.auth", "true");
		prop.put("mail.smtp.socketFactory.port", "465");
		prop.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");

		Session session = Session.getInstance(prop, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(fromEmail, password);
			}
		});

		try {
			Message message = new MimeMessage(session);
			message.setHeader("X-Priority", "1");
			message.addHeader("Content-type", "text/HTML; charset=UTF-8");
			message.setFrom(new InternetAddress("project.shandar@gmail.com"));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(toAddressList));
			message.setRecipients(Message.RecipientType.CC,
					InternetAddress.parse(toCCAddressList ));
			message.setRecipients(Message.RecipientType.BCC,
					InternetAddress.parse(toBCCAddressList ));

			//message.setSubject("Testing Gmail SSL");
			message.setSubject(emailSubject);
			//message.setText("Dear Mail Crawler," + "\n\n Please do not spam my email!");
			message.setContent(emailBody,"text/html");
			Transport.send(message);
			System.out.println("Done");

		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

}
