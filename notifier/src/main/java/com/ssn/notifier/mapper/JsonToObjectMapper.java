package com.ssn.notifier.mapper;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.ssn.notifier.beans.MainBean;

public class JsonToObjectMapper {

	public MainBean parseJsonToObject(String jsonText) {
		MainBean mainBean = null;
		Gson gson = new Gson();
		if (isJSONValid(jsonText, gson)) {
			mainBean = gson.fromJson(jsonText, MainBean.class);
		}
		return mainBean;
	}

	public boolean isJSONValid(String jsonInString, Gson gson) {
		try {
			gson.fromJson(jsonInString, Object.class);
			return true;
		} catch (JsonSyntaxException ex) {
			return false;
		}
	}

}
