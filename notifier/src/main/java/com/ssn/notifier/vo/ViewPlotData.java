/**
 * <p>Project: notifier </p>
 * <p>Package Name: com.ssn.notifier.vo </p>
 * <p>File Name: ViewPlanData.java </p>
 * <p>Create Date: 27-Sep-2021 </p>
 * <p>Create Time: 5:44:57 PM </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.ssn.notifier.vo;

import java.util.List;

/**
 * @author : Shantanu Sikdar
 *
 */
public class ViewPlotData {

	private String id;
	private List<String> listViewPlots;
	private StringBuilder sbHtmlBody;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<String> getListViewPlots() {
		return listViewPlots;
	}

	public void setListViewPlots(List<String> listViewPlots) {
		this.listViewPlots = listViewPlots;
	}

	public StringBuilder getSbHtmlBody() {
		return sbHtmlBody;
	}

	public void setSbHtmlBody(StringBuilder sbHtmlBody) {
		this.sbHtmlBody = sbHtmlBody;
	}

}
