/**
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.utils.browser </p>
 * <p>File Name: ScrappingWebsite.java </p>
 * <p>Create Date: 08-Sep-2021 </p>
 * <p>Create Time: 9:14:31 PM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.ssn.notifier.scrap;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * @author : Shantanu Sikdar
 * @Description : ScrappingWebsite
 */
public class ReadHTMLContent {

	public String readHtmlElementContent(String urlString, String tag) throws Exception {
		Document doc = Jsoup.connect(urlString).get();
		Elements scrpts = doc.select(tag);
		String jsonText = "";
		for (Element el : scrpts) {
			if (el.data().contains("var plots")) {
				jsonText = el.data().replaceAll("var plots = ", "");
				jsonText = jsonText.replaceAll("\r\n", "");
			}
		}
		return jsonText;
	}
	
}
