CREATE TABLE `ty_users` (
  			`user_id` int(11) NOT NULL AUTO_INCREMENT,
  			`user_name` varchar(50) NOT NULL,
  			`user_password` varchar(50) NOT NULL,
  			`user_fname` varchar(50) NOT NULL,
  			`user_mname` varchar(50) DEFAULT NULL,
  			`user_lname` varchar(50) NOT NULL,
  			`user_create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  			PRIMARY KEY (`user_id`),
  			UNIQUE KEY `user_name` (`user_name`)
			) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
			
			
CREATE TABLE `ty_roles` (
  			`role_id` int(11) NOT NULL AUTO_INCREMENT,
  			`role_name` varchar(50) NOT NULL,
  			`role_rank` char(1) NOT NULL,  			
  			`role_create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  			PRIMARY KEY (`role_id`),
  			UNIQUE KEY `role_name` (`role_name`)
			) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

			
CREATE TABLE `ty_role_user` (
  			`role_id` int(11) NOT NULL,
  			`user_id` int(11) NOT NULL,
  			FOREIGN KEY (role_id) REFERENCES ty_roles(role_id),
  			FOREIGN KEY (user_id) REFERENCES ty_users(user_id)
			) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

			

CREATE TABLE `ty_email_settings` (
  			`email_settings_id` int(11) NOT NULL AUTO_INCREMENT,
  			`company_name` varchar(50) NOT NULL,
  			
			 `inbox_smtp_host` varchar(100) NOT NULL,
			 `inbox_pop_host` varchar(100) NOT NULL,

  			
  			`inbox_email_address` varchar(50) NOT NULL,  			
  			`user_name` varchar(50) NOT NULL,
  			`user_password` varchar(50) NOT NULL,
  			`user_mname` varchar(50) DEFAULT NULL,
  			`user_lname` varchar(50) NOT NULL,
  			`user_create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  			PRIMARY KEY (`user_id`),
  			UNIQUE KEY `user_name` (`user_name`)
			) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

			
CREATE TABLE `ty_game_tambola` (
  			`game_num_id` int(11) NOT NULL AUTO_INCREMENT,
  			`game_name` varchar(50) NOT NULL,
  			`num_call_order` varchar(200) NOT NULL,
  			`call_count` char(1) NOT NULL,  			
  			`game_start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  			`game_end_time` timestamp NULL,
  			PRIMARY KEY (`game_num_id`),
  			UNIQUE KEY `game_name` (`game_name`)
			) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

			
CREATE TABLE `ty_profile` (
  			`profile_id` int(11) NOT NULL AUTO_INCREMENT,
  			`unique_name` varchar(10) NOT NULL,
  			`profile_fname` varchar(50) NOT NULL,
  			`profile_mname` varchar(150) DEFAULT NULL,
  			`profile_lname` varchar(50) NOT NULL,  			
  			`profile_create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,  			
  			PRIMARY KEY (`profile_id`),
  			UNIQUE KEY `unique_name` (`unique_name`)
			) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
			
/*
CREATE TABLE `car_detail` (
  			`car_id` int(11) NOT NULL AUTO_INCREMENT,  			
  			`car_name` varchar(50) NOT NULL,
  			`car_create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,  			
  			PRIMARY KEY (`car_id`)
			) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
			
			
CREATE TABLE `car_owner` (
  			`car_id` int(11) NOT NULL,
  			`car_owner` varchar(50) NOT NULL,
  			FOREIGN KEY (car_id) REFERENCES car_detail(car_id)  			
			) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;		
			
-- P:/Report Dev/MR Projects/Actuate_Library900/nonAcctSSS.rol*/