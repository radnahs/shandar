/**
 * Shantanu Sikdar 
 * 12-dec-2012 
 */

/**
 *  Following function 
 *  tblGrid(numOfRows, numOfColumns, parameter, color1, cellHeight, cellWidth )
 *  takes two parameter, numOfRows and numOfColumns .
 *  the function call be like
 *  var myArr = [];
 *	for (var i=0; i<1000; i++  ){
		myArr[i]=i+1;
	}
 *  $(document).ready(function() {
	   $('#exArr').tblGridParr(2,2,myArr);
	});
 *  where,
	#example is the div id and is written as <div id="exArr"></div>
 *  the function will create a 2 by 2 matrix with the data in the array in it.
 */
	jQuery.fn.gridWithParr=function(p1,p2,parr,colr,ht,wd){
		return this.append(
			    function(){			    	
			        var content='<table border="3" id="gwpGridTable">';
			        var idv=0;			        
			        for (var i=0; i<p1; i++  ){			        	 
			        	content=content+'<tr id="gwpRId_'+i+'">';
			        	for (var j=0; j<p2; j++  ){
			            	content=content+'<td height="'+ht+'"px" width="'+wd+'"px" bgcolor="'+colr+'" class="gwpCId_'+idv+'" id="gwpCId_'+idv+'">'+parr[idv]+'</td>';
			            	idv++;
			        	}
			        	content=content+'</tr>';
			        }
			        content= content+'</table>';
			        return content;
			    });
		}


/**
 *  Following function 
 *  alternateColorBoard(numOfRows, numOfColumns, parameter, color1, color2, cellHeight, cellWidth )
 *  takes seven parameter numOfRows, numOfColumns, parameter, color1, color2.
 *  the function call be like
 *  var myArr = [ "hello", "jq", "pw", "qw" ];
 *  $(document).ready(function() {
	   $('#tambolaBoard').alternateColorBoard(10,10,myArr,"biege","aero",50,50);
	});
 *  where,
	#example is the div id and is written as <div id="exArr"></div>
 *  the function will create a 2 by 2 matrix with the data in the array in it.
 */
		jQuery.fn.alternateColorBoard=function(p1,p2,parr,col1,col2,ht,wd){
			return this.append(
				    function(){
				        var content='<table border="1" id="acbGridTable">';
				        var idv=0;
				        var colr=0;
				        for (var i=0; i<p1; i++  ){			        	 
				        	content=content+'<tr id="acbRId_'+i+'">';
				        	for (var j=0; j<p2; j++  ){
				        		if(i%2<1){
				        			if(j%2<1)
				        				colr=col1;
				        			else 
				        				colr=col2;				        			
				        		}else{ 
				        			if(j%2<1)
				        				colr=col2;			        				
				        			else 
				        				colr=col1;				        			
				        		}
				        		content=content+'<td align="center" height="'+ht+'"px" width="'+wd+'"px" bgcolor="'+colr+'" class="acbCId_'+idv+'" id="acbCId_'+idv+'">'+parr[idv]+'</td>';
				            	idv++;
				        	}			        	
				        	content=content+'</tr>';
				        }
				        content= content+'</table>';
				        return content;
				    });
		}

/**
 *  Following function 
 *  diagonalColorBoard(numOfRows, numOfColumns, parameter, color1, color2, cellHeight, cellWidth )
 *  takes seven parameter numOfRows, numOfColumns, parameter, color1, color2, cellHeight, cellWidth.
 *  basically draws a board whose diagonals are colored.
 *  the function call be like
 *  var myArr = [ "hello", "jq", "pw", "qw" ];
 *  $(document).ready(function() {
	   $('#scrabbleBoard').diagonalColorBoard(15, 15, myArr, "white", "lightpink", 60, 60);
	});
 *  where,
	#example is the div id and is written as <div id="exArr"></div>
 *  the function will create a 2 by 2 matrix with the data in the array in it.
 */			
	jQuery.fn.diagonalColorBoard = function(p1,p2,parr,col1,col2,ht,wd){
		return this.append(
				function(){
			        var content='<table border="1" id="dcbGridTable">';
			        var idv=0;
			        var colr=col1;
			        var parTxt = 0;
			        for (var i=0; i<p1; i++  ){			        	 
			        	content=content+'<tr id="dcbRId_'+i+'">';
			        	for (var j=0; j<p2; j++  ){
			        		parTxt = parr[idv];
			        		if(i==j){
			        			colr=col2;
			        			//parTxt = 'i==j';
			        		} else if(j==(p2-1-i)){
			        			colr=col2;
			        			//parTxt = 'j==(p2-1-i)';
			        		} else{
			        			colr=col1;
			        			//parTxt = parr[idv];
			        		}
			        		content=content+'<td height="'+ht+'"px" width="'+wd+'"px" bgcolor="'+colr+'" class="dcbCId_'+idv+'" id="dcbCId_'+idv+'">'+parTxt+'</td>';
			            	idv++;
			        	}			        	
			        	content=content+'</tr>';
			        }
			        content= content+'</table>';
			        return content;
			    });
		}

