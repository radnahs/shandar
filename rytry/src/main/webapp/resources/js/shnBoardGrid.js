/**
 * Shantanu Sikdar 
 * 12-nov-2013 
 */

/**
 *  Following function 
 *  tblGrid(numOfRows, numOfColumns)
 *  takes two parameter, numOfRows and numOfColumns .
 *  the function call be like
 *  var myArr = [ "hello", "jq", "pw", "qw" ];
 *  $(document).ready(function() {
	   $('#exArr').tblGridParr(2,2,myArr);
	});
 *  where,
	#example is the div id and is written as <div id="exArr"></div>
 *  the function will create a 2 by 2 matrix with the data in the array in it.
 */
(function($){
	jQuery.fn.tblGridParr=function(p1,p2,parr){
		return this.append(
			    function(){
			        var content='<table border="3">';
			        var idv=0;			        
			        for (var i=0; i<p1; i++  ){			        	 
			        	content=content+'<tr id="id_'+i+'">';
			        	for (var j=0; j<p2; j++  ){			        		
			            	content=content+'<td id="id_'+idv+'">'+parr[idv]+'</td>';
			            	idv++;
			        	}			        	
			        	content=content+'</tr>';
			        }
			        content= content+'</table>';
			        return content;
			    });
	}
})(jQuery);


(function($){
	jQuery.fn.tblGridParrColor=function(p1,p2,parr){
		return this.append(
			    function(){
			        var content='<table border="1">';
			        var idv=0;			        
			        for (var i=0; i<p1; i++  ){			        	 
			        	content=content+'<tr id="id_'+i+'">';
			        	for (var j=0; j<p2; j++  ){
			        		if(i%2<1){
			        			if(j%2<1)
			        				content=content+'<td bgcolor="red" id="id_'+idv+'">'+parr[idv]+'</td>';
			        			else 
				        			content=content+'<td bgcolor="yellow" id="id_'+idv+'">'+parr[idv]+'</td>';
			        		}else 
			        			if(j%2<1)
			        				content=content+'<td bgcolor="yellow" id="id_'+idv+'">'+parr[idv]+'</td>';
			        			else 
				        			content=content+'<td bgcolor="red" id="id_'+idv+'">'+parr[idv]+'</td>';
			            	idv++;
			        	}			        	
			        	content=content+'</tr>';
			        }
			        content= content+'</table>';
			        return content;
			    });
	}
})(jQuery);