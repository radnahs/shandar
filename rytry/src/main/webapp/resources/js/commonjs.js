/**
 * 
 */

function shuffleArray(array) {
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
    return array;
}

/**
 * 
 * @param id
 */
function divVisibility(id) {
    var e = document.getElementById(id);
    if(e.style.display == 'block')
       e.style.display = 'none';
    else
       e.style.display = 'block';
}

function onChkShowDiv(box,chkBx) {
    var chboxs = document.getElementsByName(chkBx);
    var vis = "none";
    for(var i=0;i<chboxs.length;i++) { 
        if(chboxs[i].checked){
         vis = "block";
            break;
        }
    }
    document.getElementById(box).style.display = vis;
}


function addRow(rowIDToAdd) {
	var row = document.getElementById(rowIDToAdd); // find row to copy
	var table = document.getElementById("parametersRelatedInfoTbl"); // find table to append to
	var clone = row.cloneNode(true); // copy children too
	clone.id = "newID"; // change id or other attributes/contents
	table.appendChild(clone); // add new row to end of table
}


//Modal Window to show data	
jQuery.fn.modalWindowPopUp=function(){
	 $("#modalWindow").dialog({
	     resizable: false,
	     height:550,       
	     width:440, 
	     modal: true,	     
	     title:"Whats your decision?",	     
	     buttons: {	    	 	 
	         "Yes": function() {
	             $( this ).dialog( "close" );
	             $("#shanForm").submit();
	         },
	         "No": function() {
	             $( this ).dialog( "close" );
	             
	         }
	     },	 	
	 	open: function() {
	 		$('.ui-dialog-titlebar').css('backgroundColor', 'grey');
	 		$('.ui-dialog-title').css('color', 'yellow');
	 		
	        $('.ui-dialog-buttonpane').find('button:contains("Yes")').css('color', 'black');
	        $('.ui-dialog-buttonpane').find('button:contains("Yes")').css('border', '1px solid black');
	        
	        $('.ui-dialog-buttonpane').find('button:contains("No")').css('color', 'black');	        
	        $('.ui-dialog-buttonpane').find('button:contains("No")').css('border', '1px solid black');
	    }
	});	  
	 
 }

$( "#emailAnchor" ).click(function() {
    $(this).modalWindowPopUp();
});