<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page session="false"%>
<html>
<head>
<title>Tambola</title>
<script type="text/javascript" src="resources/js/jqry/jquery-1.8.2.min.js"></script>
<!-- <script type="text/javascript" src="resources/js/jqry/jquery.dataTables.min.js"></script> -->
<script type="text/javascript" src="resources/js/shnJqGrid.js"></script>

</head>
<body>

<div>
	<div id="tambolaBoard"></div>
	<br/>
	<div id="divNextNumber" >
		<input id="nextNumber" type="button" value="Next Number" />				
	</div>
	<div id="divShowNextNumber"></div>
	<div id="divShowPreviousNumber"></div>
</div>

</body>

<script>
var myArr = [];
for (var i=0; i<1000; i++  ){
	myArr[i]=i+1;
}
$(document).ready(function() {	   
	   //$('#exArr').tblGridParr(9,9,myArr);
	   //$('#tambolaBoard').twoColorBoard(10,10,myArr);	   
		$('#tambolaBoard').alternateColorBoard(10,10,myArr,"biege","skyblue",50,50);			
		//$('#divShowPreviousNumber').gridWithParr(1,100,myArr);
		$('#divShowPreviousNumber').gridWithParr(1,100,myArr,"lightpink",20,20);
	});

/* $( "#nextNumber" ).click(function() {
	  //alert( "Handler for .click() called." );
	  $('#divShowNextNumber').html("22");
	});
 */	

$("#nextNumber" ).click(function() {	 
	 $.ajax({url:"NextNumber.html",success:function(result){
		    $("#divShowNextNumber").html(result);		    
		    var arrRes = result.split('|');
		    var idv=arrRes[0];	
		    var idvv=idv-1;	    
		    var idc=arrRes[1]-1;
		    //$('#ctcbGridTable tr:has(td.id_'+result+')').css('bgcolor', 'red');
		    $('#acbCId_'+idvv).css('backgroundColor', 'red');
		    $('#gwpCId_'+idc).html(idv);		    
		  }}); 
	});

 
</script>
</html>