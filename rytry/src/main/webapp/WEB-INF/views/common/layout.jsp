<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>
	<tiles:insertAttribute name="title" ignore="true" />
</title>
</head>
<body>
    <div id="header">
    	<tiles:insertAttribute name="header" /> 
    </div>
        	
    <div id="menu"> 
		<tiles:insertAttribute name="menu" />
    </div>

	<div id="body">
    	<tiles:insertAttribute name="body" />
    </div>	
        	
</body>
</html>
