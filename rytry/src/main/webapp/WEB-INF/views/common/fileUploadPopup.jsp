<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page session="false"%>
<html>
<head>
<script type="text/javascript" src="resources/js/commonjs.js"></script>
</head>
<body onload="doOnload('${files}')">
	<%-- <div id="divUploadFiles">
		<form:form	method="POST" action="SaveFile.html" modelAttribute="uploadFile" enctype="multipart/form-data">
			Please select a file to upload : <input type="file" name="files[0]" />		
			<input type="submit" value="upload" />
		</form:form>	
	</div> --%>
	
	<div id="divUploadFiles" style="display: block;">	
		<form:form	method="POST" action="SaveFile.html" modelAttribute="uploadFile" enctype="multipart/form-data">
			Please select a file to upload : <input type="file" name="files[0]" />		
			<input type="submit" value="upload" />
		</form:form>			
	</div>
	
	<div id="divUploadFileSuccess" style="display: block;">
		<p>Following files are uploaded successfully.</p>
    	<ol>
        	<c:forEach items="${files}" var="file">
            	<li>${file}</li>
        	</c:forEach>
    	</ol>
    	<BUTTON onclick="Done('${files}')" type="button">Close</BUTTON>
	</div>

</body>

<script language="javascript">
function Done(files) {    
    var MyArgs = files;
    window.returnValue = MyArgs;
    alert(MyArgs);
    window.close();
}

function doOnload(files) {
	if (files ==null  || files == ''){
		divVisibility('divUploadFileSuccess');
	}else{
		divVisibility('divUploadFiles');		
	}	
}
        </script>
</html>