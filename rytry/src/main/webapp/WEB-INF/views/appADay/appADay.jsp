<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page session="false"%>
<html>
<head>
<title>AppADay</title>
<link rel="stylesheet" href="resources/js/jqry/jquery-ui-1.9.1.custom.min.css">
<script type="text/javascript" src="resources/js/jqry/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="resources/js/jqry/jquery-ui-1.9.1.custom.min.js"></script>
	<style>    
		.column { width: 370px; float: left; padding-bottom: 100px; }
		.portlet { margin: 0 1em 1em 0; }
		.portlet-header { margin: 0.3em; padding-bottom: 4px; padding-left: 0.2em; }
		.portlet-header .ui-icon { float: right; }
		.portlet-content { padding: 0.4em; }
		.ui-sortable-placeholder { border: 1px dotted black; visibility: visible !important; height: 50px !important; }
		.ui-sortable-placeholder * { visibility: hidden; }
		
		#modalWindow{display:none;}
		
	</style>

</head>
<body>

<div class="column"> 
 
	<div class="portlet"> 
		<div class="portlet-header">
			<a href="#">Email</a>			
		</div> 
		<div class="portlet-content">			
			<c:forEach var="listVar" items="${recent5Emails}">
				<div id="emailDiv"> 
					<a href="#" id="${listVar[0]}">				
						<c:out value="${listVar[1]}"/>
					</a>
				</div>
				<br/>    			
			</c:forEach>			
		</div>		
	</div>
	
	<div class="portlet"> 
		<div class="portlet-header">
			<a href="#"  id="jiraAnchor">Jira</a>
		</div> 
		<div class="portlet-content">
						Cash Reconciliation
			Cash Statement
			Asset Breakdown And Valuation
			Derivative Investment Detail
			CGCalc Transactions Report
			Corporate Actions Detail
			Direct Returns
		</div> 
	</div> 
	
</div>

<div class="column">
 
	<div class="portlet"> 
		<div class="portlet-header">My Source</div> 
		<div class="portlet-content">
						Cash Reconciliation
			Cash Statement
			Asset Breakdown And Valuation
			Derivative Investment Detail
			CGCalc Transactions Report
			Corporate Actions Detail
			Direct Returns
		</div> 
	</div>
	
	<div class="portlet">
		<div class="portlet-header">Appointments</div>
		<div class="portlet-content">
			<div id="datepicker" class="portlet-content">				
			</div>
		</div> 
	</div> 
 
</div>


<div id="modalWindow" >    
</div> 

</body>

<script>

//portlet code
$(function() {
	$( ".column" ).sortable({
		connectWith: ".column"
	});

	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" )
		.find( ".portlet-header" )
			.addClass( " ui-corner-all" )
			.prepend( "<span class='ui-icon ui-icon-minusthick'></span>")
			.end()
		.find( ".portlet-content" );

	$( ".portlet-header .ui-icon" ).click(function() {
		$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
		$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
	});

	$( ".column" ).disableSelection();
});

//Calendar
$(function() {
	$( "#datepicker" ).datepicker();
});


//Modal Window to show data	
jQuery.fn.modalWindowPopUp=function(){
	 $("#modalWindow").dialog({
	     resizable: false,
	     height:550,       
	     width:440, 
	     modal: true,	     
	     title:"Whats your decision?",	     
	     buttons: {	    	 	 
	         "Yes": function() {
	             $( this ).dialog( "close" );
	             $("#shanForm").submit();
	         },
	         "No": function() {
	             $( this ).dialog( "close" );
	             
	         }
	     },	 	
	 	open: function() {
	 		$('.ui-dialog-titlebar').css('backgroundColor', 'grey');
	 		$('.ui-dialog-title').css('color', 'yellow');
	 		
	        $('.ui-dialog-buttonpane').find('button:contains("Yes")').css('color', 'black');
	        $('.ui-dialog-buttonpane').find('button:contains("Yes")').css('border', '1px solid black');
	        
	        $('.ui-dialog-buttonpane').find('button:contains("No")').css('color', 'black');	        
	        $('.ui-dialog-buttonpane').find('button:contains("No")').css('border', '1px solid black');
	    }
	});	  
	 
 }

$( "#emailDiv a" ).click(function() {	
    $(this).modalWindowPopUp();
});

 
</script>
</html>