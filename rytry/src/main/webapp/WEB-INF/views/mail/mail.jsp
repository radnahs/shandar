<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<html>
<head>
<title>Mail</title>
<script type="text/javascript" src="resources/js/jqry/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="resources/js/shnJqGrid.js"></script>

</head>
<body>

<div>
	<div id="divInbox">
			<input id="inboxLink" type="button" value="Inbox Link" />
	</div>
	
	<br/>
	<div id="divShowInbox"></div>	
</div>

</body>

<script>
$("#inboxLink" ).click(function() {	 
	 $.ajax({url:"ExchangeMailLink.html",success:function(result){
		    $("#divShowInbox").html(result);
		  }}); 
	});

 
</script>
</html>