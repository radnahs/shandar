<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<html>
<head>
<title>Dashboard</title>
<script language="javascript">
function Done(files) {    
    var MyArgs = files;
    window.returnValue = MyArgs;
    alert(MyArgs);
    window.close();
}
</script>

</head>
<body>

    <p>Following files are uploaded successfully.</p>
    <ol>
        <c:forEach items="${files}" var="file">
            <li>${file}</li>
        </c:forEach>
    </ol>
    
    
    <BUTTON onclick="Done('${files}')" type="button">Close</BUTTON>
 
</body>
</html>