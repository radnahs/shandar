<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page session="false"%>
<html>
<head>
<title>Dynamic Jasper</title>
<script type="text/javascript" src="resources/js/jqry/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="resources/js/shnJqGrid.js"></script>
<script type="text/javascript" src="resources/js/commonjs.js"></script>

</head>
<body>

<div>
	
	<div id="templateBoard"></div>
	
</div>

</body>

<script>
var myArr = [];
myArr=['<a href="<c:url value="ViewServer.html" />" > View Server</a>',       
       ,,'<a href="<c:url value="DJReportXLS.html"/>" target="_blank" > View XLS</a>',
       '<a href="<c:url value="DJReportPDF.html"/>" target="_blank" > View PDF</a>'];

/* myArr=['<input id="djRepTmplt1" type="button" value="DJReportTemplate1" />',       
       ,,'<input id="djRepTmplt2" type="button" value="DJReportTemplate2" />',
       '<input id="djRepTmplt3" type="button" value="DJReportTemplate3" />'];
 */
var myArrLength = myArr.length; 

$(document).ready(function() {	   
		$('#templateBoard').alternateColorBoard(5,1,myArr,"biege","skyblue",100,100);
	});


$("#djRepTmplt1" ).click(function() {	 
	 $.ajax({url:"DJReportTemplate1.html",success:function(result){
		    $("#divShowNextNumber").html(result);
		  }}); 
	});


</script>
</html>