<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<html>
<head>
<title>rytry</title>
<link rel="stylesheet" href="resources/js/jqry/jquery-ui-1.9.1.custom.min.css">
<script type="text/javascript" src="resources/js/jqry/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="resources/js/jqry/jquery-ui-1.9.1.custom.min.js"></script>
<script type="text/javascript" src="resources/js/shnJqGrid.js"></script>
<script type="text/javascript" src="resources/js/shnJqModalPopUp.js"></script>
</head>
<body onload='document.user.userName.focus();'>
		<h1 align="center">Hello rytry!</h1>
	<div id="modalWindowOnLoad">
		<P>Time on rytry Server is ${serverTime}.</P>
		<br/>
		<form:form action="Login.html" method="post" id="rytryLoginForm" commandName="user">
		<table>
			<tr>
				<td>UserName:-<FONT color="red"><form:errors path="userName" /></FONT></td>
				<td><form:input path="userName" /></td>
			</tr>
			<tr>
				<td>Password:-<FONT color="red"><form:errors path="userPassword" /></FONT></td>
				<td><form:input type="password" path="userPassword" /></td>
			</tr>
			<tr>				
				<td><form:checkbox path="skipDBAuth"/></td>
				<td><FONT color="red">Skip database authentication</FONT></td>
			</tr>								
		</table>
		</form:form>
	</div>
	
</body>
<script type="text/javascript">

jQuery.fn.loginModalWindowPopUp=function(modalWindow,titleText,formId){	
	 $(modalWindow).dialog({
	     resizable: false,
	     height:550,       
	     width:440,	      
	     modal: true,	     
	     title:titleText,
	     buttons: {
	         "submit": function() {
	        	$(formId).submit();
	         }
	     },	 	
	 	open: function() {
	 		$('.ui-dialog-titlebar').css('backgroundColor', 'grey');
	 		$('.ui-dialog-title').css('color', 'yellow');
	 		$('.ui-dialog-body').css('color', 'yellow');
	 		$('.ui-dialog-body').css('align', 'center');
	        $('.ui-dialog-buttonpane').find('button:contains("close")').css('color', 'black');	        
	        $('.ui-dialog-buttonpane').find('button:contains("close")').css('border', '1px solid black');
	    }
	});	  
}

$(document).ready(function() {    
	$(this).loginModalWindowPopUp("#modalWindowOnLoad","Login","#rytryLoginForm");
});


</script>
</html>