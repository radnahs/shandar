<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page session="false"%>
<html>
<head>
<title>Wb Onboarding</title>
<link rel="stylesheet" href="resources/js/jqry/jquery-ui-1.9.1.custom.min.css">
<script type="text/javascript" src="resources/js/jqry/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="resources/js/jqry/jquery-ui-1.9.1.custom.min.js"></script>
	<style>    
		.column { width: 400px; float: left; padding-bottom: 100px; }
		.portlet { margin: 0 1em 1em 0; }
		.portlet-header { margin: 0.3em; padding-bottom: 4px; padding-left: 0.2em; font-weight:bold;  }
		.portlet-header .ui-icon { float: right; }
		.portlet-content { padding: 0.4em; background-color: white; }
		.ui-sortable-placeholder { border: 1px dotted black; visibility: visible !important; height: 50px !important; }
		.ui-sortable-placeholder * { visibility: hidden; }
		#modalWindow{display:none;}
		
	</style>
</head>
<body>
<input type="button" value="Add Portlet" onclick="generatePortlet()" />
<div id="body-div">		
</div>
<div id="modalWindow" >    
	<div id="modalWindowBodyDiv"></div>
</div> 

</body>
<script type="text/javascript">

function generatePortlet(){
	
	var mainContentDiv = document.getElementById("body-div");
	
	var elementDivColumn = addDiv("column","","","width: 400px; float: left; padding-bottom: 100px;","Column Div");
	var elementDivPortlet = addDiv("portlet","","","margin: 0 1em 1em 0;","Portlet Div");
	var elementDivPortletHeader = addDiv("portlet-header","","","margin: 0.3em; padding-bottom: 4px; padding-left: 0.2em; font-weight:bold;","Portlet Header Div");
	var elementDivPortletContent = addDiv("portlet-content","","","padding: 0.4em; background-color: white;","portlet content Div");

	elementDivPortlet.appendChild(elementDivPortletHeader);
	elementDivPortlet.appendChild(elementDivPortletContent);
	elementDivColumn.appendChild(elementDivPortlet);

	mainContentDiv.appendChild(elementDivColumn);
}

function addDiv(divClass,divId,divName,cssStyle,txtMsg){
	var elementDiv = document.createElement("div");
	elementDiv.className = divClass; 
    elementDiv.id = divId;
    elementDiv.name = divName;    
    elementDiv.style.cssText=cssStyle;
    elementDiv.innerHTML=txtMsg;
    return elementDiv;
}

//portlet code
$(function() {
	$( ".column" ).sortable({
		connectWith: ".column"
	});

	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" )
		.find( ".portlet-header" )
			.addClass( " ui-corner-all" )
			.prepend( "<span class='ui-icon ui-icon-minusthick'></span>")
			.end()
		.find( ".portlet-content" );

	$( ".portlet-header .ui-icon" ).click(function() {
		$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
		$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
	});

	$( ".column" ).disableSelection();
});


//Modal Window to show data
jQuery.fn.modalWindowPopUp=function(modalWindow,titleText){
	 $(modalWindow).dialog({
	     resizable: false,
	     height:550,       
	     width:940, 
	     modal: true,	     
	     title:titleText,
	     buttons: {
	         "close": function() {
	             $( this ).dialog( "close" );	             
	         }
	     },	 	
	 	open: function() {
	 		$('.ui-dialog-titlebar').css('backgroundColor', 'grey');
	 		$('.ui-dialog-title').css('color', 'yellow');
	 		
	 		$('.ui-dialog-body').css('color', 'yellow');
	 		
	        $('.ui-dialog-buttonpane').find('button:contains("close")').css('color', 'black');	        
	        $('.ui-dialog-buttonpane').find('button:contains("close")').css('border', '1px solid black');
	    }
	});	  	 
 }


</script>
</html>