<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page session="false"%>
<html>
<head>
<title>Wb Onboarding</title>
<link rel="stylesheet" href="resources/js/jqry/jquery-ui-1.9.1.custom.min.css">
<script type="text/javascript" src="resources/js/jqry/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="resources/js/jqry/jquery-ui-1.9.1.custom.min.js"></script>
	<style>    
		.column { width: 400px; float: left; padding-bottom: 100px; }
		.portlet { margin: 0 1em 1em 0; }
		.portlet-header { margin: 0.3em; padding-bottom: 4px; padding-left: 0.2em; font-weight:bold;  }
		.portlet-header .ui-icon { float: right; }
		.portlet-content { padding: 0.4em; background-color: white; }
		.ui-sortable-placeholder { border: 1px dotted black; visibility: visible !important; height: 50px !important; }
		.ui-sortable-placeholder * { visibility: hidden; }
		#modalWindow{display:none;}
		
	</style>
</head>
<body>
<div class="column">	
	<div class="portlet">
		<div id="reportRelatedInfoDivHeader" class="portlet-header">
			<span>Report Related Information</span>
		</div>		
		<div id="reportRelatedInfoDivBody" class="portlet-content">
			<a href="#" id="reportRelatedInfo" title="reportRelatedInfo">
				<span>Report Related Information</span>			
			</a>
			<div id="reportRelatedInfoDiv" style="display: none;">
				<form action="SendToCsv.html" method="post" id="reportRelatedInfo">
				<table>		
				<c:forEach var="inputFields" items="${reportRelatedInfo}">
					<tr>
						<td><c:out value="${inputFields.fieldName}"/></td>					
						<td><input id="${inputFields.fieldId}"/></td>				
					</tr>
				</c:forEach>
				</table>
				</form>
			</div>
		</div>
	</div>
</div>
<div class="column">	
	<div class="portlet">
		<div id="parameterRelatedInfoDivHeader" class="portlet-header">
			<span>Parameters Related Information</span>
		</div>
		<div id="parameterRelatedInfoDivBody" class="portlet-content">
			<a href="#" id="parameterRelatedInfo" title="parameterRelatedInfo">
				<span>Parameters Related Information</span>			
			</a>
			<div id="parameterRelatedInfoDiv" style="display: none;">
				<form action="SendToCsv.html" method="post" id="parameterRelatedInfo">
				<table>		
				<c:forEach var="inputFields" items="${parameterRelatedInfo}">
					<tr>
						<td><c:out value="${inputFields.fieldName}"/></td>					
						<td><input id="${inputFields.fieldId}"/></td>				
					</tr>
				</c:forEach>
				</table>
				</form>
			</div>
		</div>
	</div>
	<div class="portlet">
		<div id="reportDBInfoDivHeader" class="portlet-header">
			<span>Report data base Information</span>
		</div> 
		<div id="reportDBInfoDivBody" class="portlet-content">
			<a href="#" id="reportDBInfo" title="reportDBInfo">
				<span>Report data base Information</span>
			</a>
			<div id="reportDBInfoDiv" style="display: none;">
				<form action="SendToCsv.html" method="post" id="inputFieldsData">
				<table>
				<c:forEach var="inputFields" items="${reportDBInfo}">
					<tr>
						<td><c:out value="${inputFields.fieldName}"/></td>					
						<td><input id="${inputFields.fieldId}"/></td>
					</tr>
				</c:forEach>
				</table>
				</form>
			</div>
		</div>
	</div>	
</div>
<div class="column">		
	<div class="portlet">
		<div id="reportOutputInfoDivHeader" class="portlet-header">
			<span>Report Output ResultSet Information</span>
		</div> 
		<div id="reportOutputInfoDivBody" class="portlet-content">
			<a href="#" id="reportOutputInfo" title="reportOutputInfo">
				<span>Report Output ResultSet Information</span>
			</a>
			<div id="reportOutputInfoDiv" style="display: none;">
				<form action="SendToCsv.html" method="post" id="inputFieldsData">		
				<table>
				<c:forEach var="inputFields" items="${reportOutputInfo}">					
					<tr>						
						<td><c:out value="${inputFields.fieldName}"/></td> 
						<td><input id="${inputFields.fieldId}"/></td>
					</tr>	
				</c:forEach>
				</table>
				</form>
			</div>
		</div>
	</div>	
</div>	

<div id="modalWindow" >    
	<div id="modalWindowBodyDiv"></div>
</div> 

</body>
<script type="text/javascript">
//portlet code
$(function() {
	$( ".column" ).sortable({
		connectWith: ".column"
	});

	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" )
		.find( ".portlet-header" )
			.addClass( " ui-corner-all" )
			.prepend( "<span class='ui-icon ui-icon-minusthick'></span>")
			.end()
		.find( ".portlet-content" );

	$( ".portlet-header .ui-icon" ).click(function() {
		$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
		$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
	});

	$( ".column" ).disableSelection();
});


//Modal Window to show data
jQuery.fn.modalWindowPopUp=function(modalWindow,titleText){
	 $(modalWindow).dialog({
	     resizable: false,
	     height:550,       
	     width:940, 
	     modal: true,	     
	     title:titleText,
	     buttons: {
	         "close": function() {
	             $( this ).dialog( "close" );	             
	         }
	     },	 	
	 	open: function() {
	 		$('.ui-dialog-titlebar').css('backgroundColor', 'grey');
	 		$('.ui-dialog-title').css('color', 'yellow');
	 		
	 		$('.ui-dialog-body').css('color', 'yellow');
	 		
	        $('.ui-dialog-buttonpane').find('button:contains("close")').css('color', 'black');	        
	        $('.ui-dialog-buttonpane').find('button:contains("close")').css('border', '1px solid black');
	    }
	});	  	 
 }

$( "#reportRelatedInfoDivBody a" ).click(function() {
	var header = $(this).attr( 'title' );
	var modalContent = $("#reportRelatedInfoDiv").html();
    $("#modalWindowBodyDiv").html(modalContent);    
    $(this).modalWindowPopUp("#modalWindow",header);    
});


$( "#parameterRelatedInfoDivBody a" ).click(function() {
	var header = $(this).attr( 'title' );
	var modalContent = $("#parameterRelatedInfoDiv").html();
    $("#modalWindowBodyDiv").html(modalContent);    
    $(this).modalWindowPopUp("#modalWindow",header);    
});


$( "#reportDBInfoDivBody a" ).click(function() {
	var header = $(this).attr( 'title' );
	var modalContent = $("#reportDBInfoDiv").html();
    $("#modalWindowBodyDiv").html(modalContent);    
    $(this).modalWindowPopUp("#modalWindow",header);    
});


$( "#reportOutputInfoDivBody a" ).click(function() {
	var header = $(this).attr( 'title' );
	var modalContent = $("#reportOutputInfoDiv").html();
    $("#modalWindowBodyDiv").html(modalContent);    
    $(this).modalWindowPopUp("#modalWindow",header);    
});
</script>
</html>