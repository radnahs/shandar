<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page session="false"%>
<html>
<head>
<title>ResQuES</title>
<link rel="stylesheet" href="resources/js/jqry/jquery-ui-1.9.1.custom.min.css">
<script type="text/javascript" src="resources/js/jqry/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="resources/js/jqry/jquery-ui-1.9.1.custom.min.js"></script>
	<style>    
		.column1 { width: 400px; float: left; padding-bottom: 100px; }
		.column2 { width: 600px; float: left; padding-bottom: 100px; }
		.portlet { margin: 0 1em 1em 0; }
		.portlet-header { margin: 0.3em; padding-bottom: 4px; padding-left: 0.2em; font-weight:bold;  }
		.portlet-header .ui-icon { float: right; }
		.portlet-content { padding: 0.4em; }
		.ui-sortable-placeholder { border: 1px dotted black; visibility: visible !important; height: 50px !important; }
		.ui-sortable-placeholder * { visibility: hidden; }
		
		#modalWindow{display:none;}
	</style>

</head>
<body>

<div class="column1">
 		
	<div class="portlet"> 
		<div class="portlet-header">
			<a href="#" title="Add Query, with Data Source"><b>SQL Queries</b></a>			
			<button id="addQuery" >Add Query</button>
		</div> 
		<div class="portlet-content">
			
		</div> 
	</div> 
	
</div>

<div class="column2">
		
	<div class="portlet">
		<div class="portlet-header">
			<a href="#" title="Add Query, with Data Source"><b>Scheduled Emails</b></a>			
			<button id="addEmailTemplate" style="margin-left:auto; margin-right:0px;">Add Email Template</button>
		</div>
		<div class="portlet-content">
			<div id="appointmentDiv">
				
			</div>
		</div> 
	</div> 
	
 
</div>

<div id="modalWindow" >    
	<p id="paraBodyEmail"></p>
</div> 


</body>

<script>

//portlet code
$(function() {
	/* $( ".column" ).sortable({
		connectWith: ".column"
	}); */

	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" )
		.find( ".portlet-header" )
			.addClass( " ui-corner-all" )
			.prepend( "<span class='ui-icon ui-icon-minusthick'></span>")
			.end()
		.find( ".portlet-content" );

	$( ".portlet-header .ui-icon" ).click(function() {
		$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
		$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
	});

	//$( ".column" ).disableSelection();
});

//Calendar
$(function() {
	$( "#datepicker" ).datepicker({
			showOn: "div",
			divImage: "resources/icons/msicon.jpg",
			divImageOnly: true
			});
});


//Modal Window to show data	
jQuery.fn.modalWindowPopUp=function(modalWindow,titleText){
	 $(modalWindow).dialog({
	     resizable: false,
	     height:550,       
	     width:440, 
	     modal: true,	     
	     title:titleText,
	     buttons: {
	         "close": function() {
	             $( this ).dialog( "close" );	             
	         }
	     },	 	
	 	open: function() {
	 		$('.ui-dialog-titlebar').css('backgroundColor', 'grey');
	 		$('.ui-dialog-title').css('color', 'yellow');
	 		
	 		$('.ui-dialog-body').css('color', 'yellow');
	 		
	        $('.ui-dialog-buttonpane').find('button:contains("close")').css('color', 'black');	        
	        $('.ui-dialog-buttonpane').find('button:contains("close")').css('border', '1px solid black');
	    }
	});	  	 
 }

 $( "#addEmailTemplate" ).click(function() {
		var subject = $(this).attr( 'title' );
		var itemId = $(this).attr( 'id' );
		  $.ajax({
			  	type: "POST",
			  	url:"addEmailTemplate.html",
			  	data: "emailItemId=" + itemId,
			  	dataType:"HTML",			  
			 	success:function(result){
			    	$("#paraBodyEmail").html(result);		    		    
			  }}); 
	    $(this).modalWindowPopUp("#modalWindow",subject);
	});

$( "#addQuery" ).click(function() {
	var subject = $(this).attr( 'title' );
	var appointmentId = $(this).attr( 'id' );
	 $.ajax({
		 	type: "POST",
		 	url:"addQuery.html",
		 	data: "queryId=" + queryId,
		  	dataType:"HTML",			  
		 	success:function(result){
		    	$("#paraBodyEmail").html(result);		    		    
		  }}); 
    $(this).modalWindowPopUp("#modalWindow",subject);
});



</script>
</html>
