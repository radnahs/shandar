/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.games </p>
 * <p>File Name: GameOfLifeController.java</p>
 * <p>Create Date: Sep 30, 2014 </p>
 * <p>Create Time: 8:13:54 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.games.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shandar.rytry.games.manager.GOLManager;
import com.shandar.rytry.login.model.User;

/**
 * @author Shantanu Sikdar 
 *
 */
@Controller
public class GameOfLifeController {
	
	private static final Logger logger = LoggerFactory.getLogger(GameOfLifeController.class);
	
	@RequestMapping( value="GameOfLife")
    public String gameOfLifeView(User user, BindingResult result, Map modelMap, HttpSession session) {	
		logger.debug("GameOfLife");
        return "games/gameOfLife";
    }
	
	int count = 0;
	@RequestMapping( value="NextRun")    	
	public @ResponseBody String golNextRun(HttpSession session,HttpServletRequest request) {		
		System.out.println("ajax call");
		GOLManager golmgr = new GOLManager();
		Integer xsize = (Integer)request.getAttribute("xsize");
		Integer ysize = (Integer)request.getAttribute("ysize");
		System.out.println("xsize request = "+ xsize);
		System.out.println("ysize request = "+ ysize);
		Integer randNum = golmgr.populateRandomNum(0,100);
		System.out.println("1st rand : "+randNum);
		String strRet = (String)request.getAttribute("aliveArr");
		System.out.println("aliveArr request = "+ strRet);
		if(count==0){
			strRet = randNum+"";
		}
		count++;
		
		
        return strRet;
    }
	
	//Any live cell with fewer than two live neighbours dies, as if caused by under-population
	//Any live cell with two or three live neighbours lives on to the next generation.
	//Any live cell with more than three live neighbours dies, as if by overcrowding.
	//Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
}
