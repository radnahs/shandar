/**
 *
 * <p>Project: trunk_rytry </p>
 * <p>Package Name: com.shandar.rytry.tambola </p>
 * <p>File Name: TambolaMain.java</p>
 * <p>Create Date: Nov 11, 2013 </p>
 * <p>Create Time: 12:16:30 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.games;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * @author Shantanu Sikdar
 * 
 */
public class TambolaMain {

	/**
	 * @param args
	 */
	
	public static void main(String[] args) {
		TambolaMain tm = new TambolaMain();
		List<Long> setOfNum = tm.randomNumList(1 , 10);
		System.out.println(setOfNum);		
	}
	
	/**
	 * Gives you 
	 * @param start inclusive
	 * @param end inclusive
	 * @return
	 */
	private List<Long> randomNumList(long start, long end){
		List<Long> listOfNum = new ArrayList<>();
		for(long i=start;i<=end ;i++){
			listOfNum.add(new Long(i));
		}
		Collections.shuffle(listOfNum);
		return listOfNum;
	}

	/** Generate 10 random integers in the range 0..99. */
	public static final void randomNumberRange(String... aArgs) {
		log("Generating 10 random integers in range 0..99.");

		// note a single Random object is reused here
		Random randomGenerator = new Random();
		for (int idx = 1; idx <= 10; ++idx) {
			int randomInt = randomGenerator.nextInt(10);
			log("Generated : " + randomInt);
		}

		log("Done.");
	}

	/** Generate random integers in a certain range. */
	public static final void distinctRandomNumberInRange() {
		log("Generating random integers in the range 1..10.");

		int START = 1;
		int END = 10;
		Random random = new Random(); 
		for (int idx = 1; idx <= 10; ++idx) {
			generateRandomInteger(START, END, random);
		}		
		log("Done.");
	}

	private static long generateRandomInteger(int aStart, int aEnd, Random aRandom ) {
		
		if (aStart > aEnd) {
			throw new IllegalArgumentException("Start cannot exceed End.");
		}
		// get the range, casting to long to avoid overflow problems
		long range = (long) aEnd - (long) aStart + 1;

		// compute a fraction of the range, 0 <= frac < range
		
		long fraction = (long) (range * aRandom.nextDouble());
		int randomNumber = (int) (fraction + aStart);
		log("Generated : " + randomNumber);
		return randomNumber;
	}

	private static void log(String aMessage) {
		System.out.println(aMessage);
	}

}
