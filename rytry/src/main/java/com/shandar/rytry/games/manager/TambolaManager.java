/**
 *
 * <p>Project: trunk_rytry </p>
 * <p>Package Name: com.shandar.rytry.tambola.manager </p>
 * <p>File Name: TambolaManager.java</p>
 * <p>Create Date: Nov 15, 2013 </p>
 * <p>Create Time: 7:51:23 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.games.manager;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.shandar.rytry.games.util.TambolaUtil;

/**
 * @author Shantanu Sikdar 
 *
 */
public class TambolaManager {
	private static final Logger logger = LoggerFactory.getLogger(TambolaManager.class);
	
	//private List<Long> ranLst = TambolaUtil.getListOfRandomNum();
	private List<Long> ranLst = new TambolaUtil().getListOfRandomNum();
	private int count = 0; 
	
	/**
	 * Gives you 
	 * @param start inclusive
	 * @param end inclusive
	 * @return
	 */
	public String tambolaNextNumber(){
		logger.debug("count = "+count);
		String num = ranLst.get(count).toString();
		logger.debug("num = "+num);
		count++;
		num = num+"|"+count;
        return num;
	}
	
	public Long getRandomNum(List<Long> randomNumList){
		return null;
	}
	
}
