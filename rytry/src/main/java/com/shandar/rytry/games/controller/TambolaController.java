/**
 *
 * <p>Project: trunk_rytry </p>
 * <p>Package Name: com.shandar.rytry.tambola.controller </p>
 * <p>File Name: TambolaController.java</p>
 * <p>Create Date: Nov 11, 2013 </p>
 * <p>Create Time: 4:49:25 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.games.controller;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shandar.rytry.games.manager.TambolaManager;
import com.shandar.rytry.login.model.User;

/**
 * @author Shantanu Sikdar 
 *
 */
@Controller
public class TambolaController {
	private static final Logger logger = LoggerFactory.getLogger(TambolaController.class);

	@RequestMapping( value="Tambola")
    public String tambolaView(User user, BindingResult result, Map model, HttpSession session) {	
		logger.debug("Tambola");
		System.out.println(session.getAttribute("userName"));
		logger.debug("userName 1 == " + session.getAttribute("userName"));
        return "games/tambola";
    }
	
	@RequestMapping( value="NextNumber")    	
	public @ResponseBody String tambolaNextNumber(HttpSession session) {
		TambolaManager tMgr = new TambolaManager();		
		String num = tMgr.tambolaNextNumber();
		logger.debug("userName 2 == " + session.getAttribute("userName"));
		logger.debug("num = "+num);
        return num;
    }
	
}
