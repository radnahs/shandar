/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.tambola.util </p>
 * <p>File Name: TambolaUtil.java</p>
 * <p>Create Date: Jan 8, 2014 </p>
 * <p>Create Time: 4:25:46 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.games.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Shantanu Sikdar
 * 
 */
public class TambolaUtil {
	private static final Logger logger = LoggerFactory.getLogger(TambolaUtil.class);

	private static List<Long> listOfRandomNum = null;

	/**
	 * Gives you	  
	 * @param start  inclusive
	 * @param end    inclusive
	 * @return
	 */
	private void populateRandomNum(long start, long end) {
		try {
			listOfRandomNum = new ArrayList<Long>();
			for (long i = start; i <= end; i++) {
				listOfRandomNum.add(new Long(i));
			}
			System.out.println(listOfRandomNum);
			Collections.shuffle(listOfRandomNum);
			System.out.println(listOfRandomNum);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	public List<Long> getListOfRandomNum() {
		if (listOfRandomNum == null) {
			populateRandomNum(1, 100);
		}
		return listOfRandomNum;
	}

	/*public void setListOfRandomNum(List<Long> listOfRandomNum) {
		TambolaUtil.listOfRandomNum = listOfRandomNum;
	}*/

	/*public static void main(String[] args) {
		System.out.println(TambolaUtil.getListOfRandomNum());
		System.out.println(TambolaUtil.getListOfRandomNum());
	}*/

}
