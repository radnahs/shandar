/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.games.manager </p>
 * <p>File Name: GOLManager.java</p>
 * <p>Create Date: Oct 13, 2014 </p>
 * <p>Create Time: 11:18:05 AM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.games.manager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Shantanu Sikdar 
 *
 */
public class GOLManager {
	
	/*public void processGameBoard(int x_axis_size, int y_axis_size){		
		boolean[][] next = new boolean[x_axis_size][y_axis_size];  // empty board
		for (int j=0;j<x_axis_size;j++){     // loops through x-axis for computing the next generation
			for (int k=0; k< y_axis_size; k++){ // loops through y-axis
				next[j][k] = getDot(board,j,k,x_axis_size,y_axis_size); 
			}
		}
		board = next;    // use the 'next' array as the new 'board' array	
	}*/
	
	
	public static int getNeighbourCount(boolean[][] golGrid, int x, int y, int xsize, int ysize){
		int neighbourCount = 0;   
		
		if (golGrid[mod(x+1,xsize)][y]){
			neighbourCount++;
		}
		if (golGrid[mod(x+1,xsize)][mod(y+1, ysize)]){
			neighbourCount++;
		}
		if (golGrid[x][mod(y+1,ysize)]){
			neighbourCount++;
		}
		if (golGrid[x][mod(y-1,ysize)]){
			neighbourCount++;
		}
		if (golGrid[mod(x+1,xsize)][mod(y-1,ysize)]){
			neighbourCount++;
		}
		if (golGrid[mod(x-1,xsize)][y]){
			neighbourCount ++;
		}
		if (golGrid[mod(x-1,xsize)][mod(y-1,ysize)]){
			neighbourCount ++;
		}
		if (golGrid[mod(x-1,xsize)][mod(y+1,ysize)]){
			neighbourCount ++;
		}
		return neighbourCount;
	}
	

	public static int mod (int x, int m){  
		m = Math.abs(m);
		return (x % m + m) % m;
	}
	
	public Integer populateRandomNum(int start, int end) {
		List<Integer> listOfRandomNum = new ArrayList<Integer>();
		try {
			for (int i = start; i <= end; i++) {
				listOfRandomNum.add(new Integer(i));
			}
			Collections.shuffle(listOfRandomNum);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listOfRandomNum.get(0);
	}
	
	public static void main(String[] args) {
		

	}

}
