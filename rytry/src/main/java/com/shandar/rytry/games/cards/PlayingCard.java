/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.cards </p>
 * <p>File Name: PlayingCard.java</p>
 * <p>Create Date: Mar 25, 2014 </p>
 * <p>Create Time: 3:01:04 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.games.cards;

/**
 * @author Shantanu Sikdar 
 *
 */
public class PlayingCard implements Comparable<PlayingCard>{
	
	public static final int TOTAL_CARDS = 52;
    
    public enum Suit {	CLUB(0, 'C'), 		DIAMOND(1, 'D'),		HEART(2, 'H'),		SPADE(3, 'S');
		
		private final int intValue;
		private final Character symbol;
		
		private static final Suit [] INT_SUIT_MAP = {SPADE, HEART, DIAMOND, CLUB};
		
		Suit(int intValue, Character symbol) {
			this.intValue = intValue;
			this.symbol = symbol;
		}

		public int getIntValue() {
			return intValue;
		}

		public Character getSymbol() {
			return symbol;
		}
		
		static public Suit getSuit(int suit) {
			return INT_SUIT_MAP[suit];
		}
		
	}
    
	public enum Rank {
		TWO(2, '2'), THREE(3, '3'), FOUR(4, '4'), FIVE(5, '5'), SIX(6, '6'), SEVEN(7, '7'), EIGHT(8, '8'), NINE(9, '9'), TEN(10, 'T'),
		JACK(11, 'J'), QUEEN(12, 'Q'), KING(13, 'K'), ACE(14, 'A');
		
		private final int intValue;
		private final Character symbol;

		Rank(int intValue, Character symbol) {
			this.intValue = intValue;
			this.symbol = symbol;
		}

		public int getIntValue() {
			return intValue;
		}

		public Character getSymbol() {
			return symbol;
		}

		private static final Rank[] INT_RANK_MAP = {null, ACE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE}; 
		
		// both 1 and 14 are recognized as ACE
		public static Rank getRank(int intValue) {
			return INT_RANK_MAP[intValue];
		}		
	}
	
	private Rank rank;
	private Suit suit;
	
	public PlayingCard() {
		rank=Rank.ACE;
		suit=Suit.SPADE;
	}
	
	/**
	 *  rank: 1-14, suite: 0-3
	 * @param rank
	 * @param suit
	 */
	public PlayingCard(int rank, int suit) {
		this.rank = Rank.getRank(rank);
		this.suit = Suit.getSuit(suit);
	}

	/**
	 *  construct card from a unique number, [0-51]
	 * @param intValue
	 */
	public PlayingCard(int intValue) {
		rank = Rank.getRank( intValue%13 + 1 );
		suit = Suit.getSuit( intValue/13 );
	}
	
	/**
	 * @return a unique value of the card, [0-51]
	 */
	public int getIntValue() {
		int rankInt = rank.intValue - 1;
		if (rankInt==13)
			rankInt = 0;
		return suit.intValue*13+rankInt;
	}
	
	@Override
	public String toString() {
		return rank.getSymbol().toString() + suit.getSymbol();
	}

	@Override
	public int compareTo(PlayingCard o) {
		int c = rank.compareTo(o.rank);
		if (c!=0)
			return c;
		else
			return suit.compareTo(o.suit);
	}

	@Override
	public boolean equals(Object obj) {
		PlayingCard card2 = (PlayingCard) obj;
		if (rank == card2.rank && suit == card2.suit)
			return true;
		else
			return false;
	}

	
}
