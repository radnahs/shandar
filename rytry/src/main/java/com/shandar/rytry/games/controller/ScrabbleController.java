/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.scrable.controller </p>
 * <p>File Name: ScrabbleController.java</p>
 * <p>Create Date: Jan 9, 2014 </p>
 * <p>Create Time: 12:01:47 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.games.controller;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shandar.rytry.games.manager.ScrabbleManager;
import com.shandar.rytry.login.model.User;

/**
 * @author Shantanu Sikdar 
 *
 */

@Controller
public class ScrabbleController {
	
	private static final Logger logger = LoggerFactory.getLogger(ScrabbleController.class);

	@RequestMapping( value="Scrabble")
    public String scrabbleView(User user, BindingResult result, Map model, HttpSession session) {	
		logger.debug("Scrabble");
		System.out.println(session.getAttribute("userName"));
		logger.debug("userName 1 == " + session.getAttribute("userName"));
        return "games/scrabble";
    }
	
	@RequestMapping( value="WordCheck")    	
	public @ResponseBody String scrabbleWordCheck(HttpSession session) {
		ScrabbleManager sMgr = new ScrabbleManager();		
		String num = sMgr.scrabbleWordCheck();
		logger.debug("userName 2 == " + session.getAttribute("userName"));
		logger.debug("num = "+num);
        return num;
    }

}
