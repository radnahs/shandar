/**
 * <p>Project: rytry </p>
 * <p>Package Name: com.shandar.rytry.comments </p>
 * <p>File Name: FileProperties.java </p>
 * <p>Create Date: 01-Dec-2020 </p>
 * <p>Create Time: 1:43:58 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.rytry.comments;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.util.Date;

import com.ibm.icu.text.SimpleDateFormat;

/**
 * @author : Shantanu Sikdar
 * @Description : FileProperties
 * http://oliviertech.com/java/how-to-get-a-file-creation-date/
 */
public class FileProperties {

	public static void main(String[] args) {
		File file = new File("E:\\data\\project\\shandar\\rytry\\src\\main\\java\\com\\shandar\\rytry\\comments\\FileProperties.java");
		FileProperties fp = new FileProperties();
		fp.fileProperties(file);
	}

	
	public void fileProperties(File file) {
		BasicFileAttributes attrs;
		try {
			attrs = Files.readAttributes(file.toPath(), BasicFileAttributes.class);
			FileTime time = attrs.creationTime();

			String pattern = "yyyy-MM-dd HH:mm:ss";
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

			String formatted = simpleDateFormat.format(new Date(time.toMillis()));

			System.out.println("The file creation date and time is: " + formatted);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
