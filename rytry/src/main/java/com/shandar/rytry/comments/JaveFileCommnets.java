/**
 * <p>Project: rytry </p>
 * <p>Package Name: com.shandar.rytry.comments </p>
 * <p>File Name: JaveFileCommnets.java </p>
 * <p>Create Date: 01-Dec-2020 </p>
 * <p>Create Time: 10:31:57 am </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.rytry.comments;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.util.Date;

import com.ibm.icu.text.SimpleDateFormat;
import com.shandar.rytry.dbs.folder.DirectoryDetails;

/**
 * @author : Shantanu Sikdar
 * @Description : JaveFileCommnets
 */
public class JaveFileCommnets {

	public static void main(String[] args) {
		JaveFileCommnets jfc = new JaveFileCommnets();
		System.out.println(jfc.javaClassFileComments(DirectoryDetails.class));
	}

	public String javaClassFileComments(Class<?> classFile) {
		StringBuilder sb = new StringBuilder();
		sb.append("/**\r\n");
		sb.append(" * <p>Project: rytry </p>\r\n");
		sb.append(" * <p>Package Name:" + classFile.getPackageName() + "</p>\r\n");
		sb.append(" * <p>File Name: " + classFile.getName() + ".java </p>\r\n");
		sb.append(" * <p>Create Date: 29-Nov-2020 </p>\r\n");
		sb.append(" * <p>Create Time: 8:05:43 pm </p>\r\n");
		sb.append(" * <p>Copyright: Copyright (c) 2016</p>\r\n");
		sb.append(" * <p>Company:  </p>\r\n");
		sb.append(" * @author Shantanu Sikdar\r\n");
		sb.append(" * @version 1.0\r\n");
		sb.append(" */\r\n");
		return sb.toString();
	}

}
