/**
 * <p>Project: rytry </p>
 * <p>Package Name: com.shandar.rytry.comments </p>
 * <p>File Name: InsertPackageInfo.java </p>
 * <p>Create Date: 29-Nov-2020 </p>
 * <p>Create Time: 8:05:43 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.rytry.comments;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

import com.shandar.rytry.dbs.folder.DirectoryDetails;

/**
 * @author : Shantanu Sikdar
 * @Description : InsertPackageInfo
 */
public class InsertPackageInfo {

	public static void main(String[] args) {
		DirectoryDetails ddet = new DirectoryDetails();
		InsertPackageInfo ipi = new InsertPackageInfo();
		Map<String, List<File>> onlyFilesMap = ddet.contentListings(
				new File("E:\\data\\project\\shandar\\rytry\\src\\main\\java\\com\\shandar\\rytry\\utils\\schedule"));
		
		//System.out.println(onlyFilesMap);
		JaveFileCommnets jfc = new JaveFileCommnets();
		System.out.println(jfc.javaClassFileComments(String.class));
		
		for (Entry<String, List<File>> entryMap : onlyFilesMap.entrySet()) {
			List<File> files = entryMap.getValue();
			for (File fileObjs : files) {
				System.out.println(fileObjs.getAbsolutePath());
				System.out.println(ipi.javaFileHavingCommnets(fileObjs));
			}
		}
	}

	//
	private boolean javaFileHavingCommnets(File fileObjs) {
		boolean hasComments =false;
		try {
			Scanner scn = new Scanner(fileObjs);
			while(scn.hasNext()) {
				String ss= scn.nextLine();
				System.out.println(ss);
				if (ss.equalsIgnoreCase("/**")) {
					hasComments = true;
					break;
				}else if(ss.startsWith("package")) {
					hasComments = false;
					break;
				}
			}
		} catch (Exception e) {

		}
		return hasComments;

	}
	
}
