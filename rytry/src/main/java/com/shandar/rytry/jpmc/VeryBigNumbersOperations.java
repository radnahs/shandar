/**
 * <p>Project: rytry </p>
 * <p>Package Name: com.shandar.rytry.jpmc </p>
 * <p>File Name: VeryBigNumbersOperations.java </p>
 * <p>Create Date: 10-Jul-2023 </p>
 * <p>Create Time: 10:13:31 PM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.rytry.jpmc;

/**
 * @author : Shantanu Sikdar
 * @Description : VeryBigNumbersOperations
 */

public class VeryBigNumbersOperations {

	public static void main(String[] args) {
		System.out.println("sum");
		// sum = 12654646464465658798732132184761321684943132132131687497613213282
		sum("12654646464465658798732132184761321684943132132131687497613213191", "91");
		sum("891", "941");// 1832
	}

	private static void sum(String str1, String str2) {
		str1 = "0" + str1;
		str2 = "0" + str2;
		int str1Size = str1.length(), str2Size = str2.length();
		int size = str1Size >= str2Size ? str1Size : str2Size;
		StringBuffer sbSum = new StringBuffer();
		int carry = 0;
		for (int i = size, j = 0; i > 0; i--, j++) {
			Integer place = (str1.length() > j ? Integer.parseInt(str1.substring(str1Size - 1, str1Size--)) : 0)
					+ (str2.length() > j ? Integer.parseInt(str2.substring(str2Size - 1, str2Size--)) : 0) + carry;
			carry = 0;
			if (place > 10) {
				carry = place / 10;
				place = place % 10;
			}
			sbSum.append(place);
		}
		System.out.println(sbSum.reverse().toString());
	}

}
