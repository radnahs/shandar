package com.shandar.rytry.jppmc;

import java.util.*;
import java.util.stream.Collectors;

public class InvertedIndex {

    private static Map<String, List<String>> mapIndex = new HashMap<>();
    private static int pageNum = 1;

    public static void main(String[] args) {
        System.out.println("Shantanu");
        buildAppendix("this is a boy , a real boy");//page0
        buildAppendix("boy is good");//page1
        System.out.println(mapIndex);

        System.out.println(search("boy")); //0,1
        System.out.println(search("good")); //1
        System.out.println(search("this"));//0
        System.out.println(search("bad"));//-1

    }

    private static void buildAppendix(String str){
        String[] words = str.split(" ");

        for (int i=0;i<words.length;i++) {
            if(mapIndex.containsKey(words[i])){
                List<String> wrdAppindx= mapIndex.get(words[i]);
                wrdAppindx.add(pageNum+"");
                mapIndex.put(words[i],wrdAppindx);
            }else{
                List<String> wordIndx = new ArrayList<>();
                wordIndx.add(pageNum+"");
                mapIndex.put(words[i],wordIndx);
            }
        }
        pageNum++;
    }

    private static String search(String searchWord){
        if(mapIndex.containsKey(searchWord)){
            return mapIndex.get(searchWord).stream().distinct().collect(Collectors.joining(", "));
        }else{
            return "-1";
        }
    }


}