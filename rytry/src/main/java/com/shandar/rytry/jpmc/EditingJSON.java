package com.shandar.rytry.jppmc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

public class EditingJSON {

    public static void main(String[] args) throws JsonMappingException, JsonProcessingException {
        //String json = "{\"accountNumber\":\"848\",\"accountCategory\":\"Location\",\"accountSubCategory\":\"Agent Clearing Broker\",\"accountDesignation\":\"EXTERNAL\",\"accountPurpose\":\"Safekeeping\",\"custodyType\":\"Hold in Custody\",\"regRegime\":\"Neither\",\"rightOfUse\":\"Bene Owner Segregated\",\"rightOfUseLe\":\"\",\"rightOfUseValue\":\"102691\",\"tfIndicator\":\"N\",\"accountStatus\":\"Open\",\"ecid\":\"0350141842\",\"mmpMmcAccountNumber\":\"R1W848\",\"caid\":\"A114612166\",\"subAccountCode\":\"848\",\"subAccountRef\":\"01-100 415 327\",\"extendedAccountRef\":\"01-100 415 327\",\"localInvestorId\":\"0001 5883 7746\",\"subAccountFullName\":\"SPP PENSION AND FORSAKRING\",\"subAccountBriefName\":\"C1GG369\",\"nomineeCode\":\"R1W\",\"dle\":\"3982\",\"dleEcid\":\"0032734508\",\"dleDesc\":\"J.P. MORGAN SE - LUXEMBOURG BRANCH\",\"equity\":\"BOTH\",\"originatorGTI\":\"ETM56\",\"govTaxRate\":\"0.00000000000\",\"corpTaxRate\":\"0.00000000000\",\"dividendTaxRate\":\"0.00000000000\",\"depotJid\":\"R1W\",\"rejectReason\":\"Sub Account Maintenance\",\"nomineeDesc\":\"SEB SWEDEN\",\"countryCode\":\"SE\",\"countryDesc\":\"the Kingdom of Sweden\",\"lastPublishedDate\":\"Thu Oct 28 2021 12:33:07 UTC\",\"isReconciliation\":true,\"mmcStatus\":\"Open\",\"shortSell\":\"0\",\"investableAssetClass\":\"BOTH\",\"sapco\":{\"SAPCO\":\"3982\"},\"daysBetweenStmts\":\"31\",\"targetRecDays\":\"1\",\"disclosure\":\"Y\",\"regNominee\":\"00\",\"memos\":{},\"safekeepingExclusion\":\"N\",\"ctmMarketDataList\":[],\"ssiRepoEnrichment\":{\"governmentBond\":\"0001 5883 7746\",\"corporateBond\":\"0001 5883 7746\",\"hongKongConnectModelsList\":[]},\"oldDle\":\"3982\",\"oldDleEcid\":\"0032734508\",\"oldOriginatorGti\":\"ETM56\",\"oldAccountStatus\":\"Open\",\"oldAccountPurpose\":\"Safekeeping\",\"isSetEciMigrationEffectiveDate\":false,\"inValidGTIForRightOfUseValue\":false}";
        String json = "{\"accountNumber\":\"816\",\"accountCategory\":\"Location\",\"accountSubCategory\":\"Agent Clearing Broker\",\"accountDesignation\":\"EXTERNAL\",\"accountPurpose\":\"Safekeeping\",\"custodyType\":\"Hold in Custody\",\"regRegime\":\"Neither\",\"rightOfUse\":\"Bene Owner Segregated\",\"rightOfUseLe\":\"\",\"rightOfUseValue\":\"118565\",\"tfIndicator\":\"N\",\"accountStatus\":\"Open\",\"ecid\":\"0350141842\",\"mmpMmcAccountNumber\":\"R1W816\",\"caid\":\"A114611934\",\"subAccountCode\":\"816\",\"subAccountRef\":\"01-100 421 157\",\"extendedAccountRef\":\"01-100 421 157\",\"localInvestorId\":\"0001 5885 2230\",\"subAccountFullName\":\"ROBECO ALL STRATEGIES FDS\",\"subAccountBriefName\":\"C1GG65\",\"nomineeCode\":\"R1W\",\"dle\":\"3982\",\"dleEcid\":\"0032734508\",\"dleDesc\":\"J.P. MORGAN SE - LUXEMBOURG BRANCH\",\"equity\":\"BOTH\",\"originatorGTI\":\"EOC39\",\"govTaxRate\":\"0.00000000000\",\"corpTaxRate\":\"0.00000000000\",\"dividendTaxRate\":\"0.00000000000\",\"depotJid\":\"R1W\",\"rejectReason\":\"Sub Account Maintenance\",\"nomineeDesc\":\"SEB SWEDEN\",\"countryCode\":\"SE\",\"countryDesc\":\"the Kingdom of Sweden\",\"lastPublishedDate\":\"Tue Nov 09 2021 15:40:31 UTC\",\"isReconciliation\":true,\"mmcStatus\":\"Open\",\"shortSell\":\"0\",\"investableAssetClass\":\"BOTH\",\"sapco\":{\"SAPCO\":\"3982\"},\"daysBetweenStmts\":\"31\",\"targetRecDays\":\"1\",\"disclosure\":\"Y\",\"regNominee\":\"00\",\"memos\":{},\"safekeepingExclusion\":\"N\",\"ctmMarketDataList\":[],\"ssiRepoEnrichment\":{\"governmentBond\":\"0\",\"corporateBond\":\"0\",\"hongKongConnectModelsList\":[]},\"oldDle\":\"3982\",\"oldDleEcid\":\"0032734508\",\"oldOriginatorGti\":\"EOC39\",\"oldAccountStatus\":\"Open\",\"oldAccountPurpose\":\"Safekeeping\",\"isSetEciMigrationEffectiveDate\":false,\"inValidGTIForRightOfUseValue\":false}";
        //ScppAccount scppAccount = JsonUtils.fromJson(json, ScppAccount.class);
        //System.out.println("scppAccount = "+scppAccount);

        //System.out.println(json);
        ObjectMapper omapp = new ObjectMapper();
        ObjectNode onn = omapp.readValue(json,ObjectNode.class);
        //System.out.println(onn.toPrettyString());
        System.out.println(onn.toString());

        if(onn.get("ssiRepoEnrichment").has("governmentBond")){
            onn.with("ssiRepoEnrichment").put("governmentBond", "");
        }
        if(onn.get("ssiRepoEnrichment").has("corporateBond")){
            onn.with("ssiRepoEnrichment").put("corporateBond", "");
        }
        onn.put("equitytest", "0");
        //onn.put("equity", "0");
        //onn.set("equity", onn);
        //System.out.println(onn.toPrettyString());
        System.out.println(onn.toString());

    }
    public static void main1(String[] args) {
        Set<String> caidSet = Set.of("123","345","456","567");
        System.out.println("caidSet = " + caidSet);
        String inpt = String.join(", ", caidSet);
        System.out.println("marketName = "+inpt);
    }

    public static void main2(String[] args) {
        String marketName=null;
        if(!"".equals(marketName)) {
            //List<String> listToConsider = List.of("SWITZERLAND DIRECT", "DENMARK DIRECT");
            List<String> listToConsider = Arrays.asList("SWITZERLAND DIRECT", "DENMARK DIRECT");
            //listToConsider.add("");
            if (listToConsider.contains(marketName)) {
                System.out.println("CHASLULXACC");
            }
            else {
                System.out.println("marketName = "+marketName);
            }
        }
    }
}