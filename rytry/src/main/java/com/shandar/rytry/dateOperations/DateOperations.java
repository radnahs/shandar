/**
 * <p>Project: rytry </p>
 * <p>Package Name: com.shandar.rytry.dateOperations </p>
 * <p>File Name: DatesDifferences.java </p>
 * <p>Create Date: 07-Feb-2016 </p>
 * <p>Create Time: 8:33:26 am </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.rytry.dateOperations;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * @author : Shantanu Sikdar
 * @Description : DatesDifferences
 */
public class DateOperations {

	public static void main(String[] args) {
		DateOperations dtOp = new DateOperations();

		try {
			dtOp.givenTwoDatesBeforeJava8();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		dtOp.givenTwoDatesInJava8("2020-02-24","2020-03-03");
		dtOp.listOfDatesBetweenTwoDates();
	}

	public void givenTwoDatesBeforeJava8() throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH);
		Date firstDate = sdf.parse("02/24/2019");
		Date secondDate = sdf.parse("03/03/2019");
		long diffInMillies = Math.abs(secondDate.getTime() - firstDate.getTime());
		long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
		System.out.println(diff);
	}

	public void givenTwoDatesInJava8(String dateBeforeString, String dateAfterString ) {
		// 24-Feb-2016, desired Start Date
		//LocalDate dateBefore = LocalDate.of(2020, Month.FEBRUARY, 24);
		LocalDate dateBefore = LocalDate.parse(dateBeforeString);
		// 01-Mar-2016, desired End Date
		//LocalDate dateAfter = LocalDate.of(2020, Month.MARCH, 01);
		LocalDate dateAfter = LocalDate.parse(dateAfterString);
		long noOfDaysBetween = ChronoUnit.DAYS.between(dateBefore, dateAfter);
		System.out.println(noOfDaysBetween);
	}
	
	public void listOfDatesBetweenTwoDates(){
		String startDateStr = "2020-02-24";
		String endDate = "2020-03-03";
		LocalDate start = LocalDate.parse(startDateStr);
		LocalDate end = LocalDate.parse(endDate);
		List<LocalDate> totalDates = new ArrayList<>();
		while (!start.isAfter(end)) {
			System.out.print(start);
			System.out.println(start.getDayOfWeek());
			totalDates.add(start);
		    start = start.plusDays(1);
		}
		
	}
}
