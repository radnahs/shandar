/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.soap </p>
 * <p>File Name: WebServiceClient.java</p>
 * <p>Create Date: Mar 21, 2014 </p>
 * <p>Create Time: 1:35:29 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.soap;

import javax.xml.namespace.QName;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.ws.Dispatch;
import javax.xml.ws.Service;
import javax.xml.ws.soap.SOAPBinding;

/**
 * @author Shantanu Sikdar 
 *
 */
public class WebServiceClient {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String endpointUrl = "";
		
		try {
			//QName serviceName = new QName("http://com/ibm/was/wssample/echo/", "EchoService");
			//QName portName = new QName("http://com/ibm/was/wssample/echo/", "EchoServicePort");
			QName serviceName = new QName("http://com/ibm/was/wssample/echo/", "EchoService");
			QName portName = new QName("http://com/ibm/was/wssample/echo/", "EchoServicePort");
					
			/** Create a service and add at least one port to it. **/ 
			Service service = Service.create(serviceName);
			service.addPort(portName, SOAPBinding.SOAP11HTTP_BINDING, endpointUrl);
					
			/** Create a Dispatch instance from a service.**/ 
			Dispatch<SOAPMessage> dispatch = service.createDispatch(portName, 
			SOAPMessage.class, Service.Mode.MESSAGE);
				
			/** Create SOAPMessage request. **/
			// compose a request message
			MessageFactory mf = MessageFactory.newInstance();//SOAPConstants.SOAP_1_1_PROTOCOL);
	
			// Create a message.  This example works with the SOAPPART.
			SOAPMessage request = mf.createMessage();
			SOAPPart part = request.getSOAPPart();
	
			// Obtain the SOAPEnvelope and header and body elements.
			SOAPEnvelope env = part.getEnvelope();
			SOAPHeader header = env.getHeader();
			SOAPBody body = env.getBody();
	
			// Construct the message payload.
			SOAPElement operation = body.addChildElement("invoke", "ns1", "http://com/ibm/was/wssample/echo/");
			SOAPElement value = operation.addChildElement("arg0");
			value.addTextNode("ping");
			request.saveChanges();
	
			System.out.println(request);
			
			/** Invoke the service endpoint. **/
			SOAPMessage response = dispatch.invoke(request);
			System.out.println(response);

		} catch (SOAPException e) {
			// TODO: handle exception
		}catch (Exception e) {
			// TODO: handle exception
		}
		/** Process the response. **/
		

	}

}
