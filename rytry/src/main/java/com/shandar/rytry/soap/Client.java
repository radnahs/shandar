/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.soap </p>
 * <p>File Name: Client.java</p>
 * <p>Create Date: Mar 18, 2014 </p>
 * <p>Create Time: 8:39:43 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.soap;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;


/**
 * @author Shantanu Sikdar
 * 
 */
public class Client {

	 public static void main(String[] args) { 
		 //Client client = new Client();	 
		try{
			Map<String, String> mp = new HashMap<>();
			mp.put("pappu", "dance");
			mp.put("rangpur", "holi");
			mp.put("cunnect", "wb");
			
		 	//client.downloadSingleFile("https://webmail.bnymellon.com/ews/Services.wsdl");
		} catch(Exception e){ 
			e.printStackTrace();  
		}
	}
	 
	 
	 public void downloadSingleFile(String urlString){
		 URL url;
		 try{ 
			 Calendar nowStart = Calendar.getInstance(); 
			 System.out.println(nowStart.getTime());
			 
			 url = new URL(urlString);
			 
			 TrustManager[] trustAllCerts = new TrustManager[] { new
					 X509TrustManager() {	 
				 		public java.security.cert.X509Certificate[] getAcceptedIssuers() { 
				 			return null; 
				 		} 
				 		public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType){ 
				 			//No need to implement.
				 		} 
				 		public void	 checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType){ 
				 			//No need to implement. 
				 		} 
					} 
			 }; 
			 SSLContext sc = SSLContext.getInstance("SSL"); 
			 sc.init(null, trustAllCerts, new java.security.SecureRandom());
			 
			 HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
			 
			 HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
			 
			 BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
			 String input;

			 while ((input = br.readLine()) != null) {
				System.out.println(input);
			 }
			 br.close();
	  
		 } catch(NoSuchAlgorithmException nsae){ 
			 nsae.printStackTrace(); 
		 } catch(Exception ioe) { 
			 ioe.printStackTrace(); 
		 } 
	}
	

}
