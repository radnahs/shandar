/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.bnym.properties </p>
 * <p>File Name: ReportingProperties.java</p>
 * <p>Create Date: Jul 14, 2014 </p>
 * <p>Create Time: 4:21:44 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.bnym.properties;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import org.apache.log4j.Logger;

//import com.mellon.reportutility.run.NewReportUtilityConf;
import com.shandar.rytry.bnym.dao.impl.BnymDAOImpl;
import com.shandar.rytry.bnym.service.OutputQueryFactory;
import com.shandar.rytry.bnym.service.WBtoERSConstants;

public class ReportingProperties {
	
	private static String DELIMITER_SYMBOL = "~";
	
	private static Logger logger = Logger.getLogger(ReportingProperties.class.getName());
	
	private static Properties reportNameProperties = new Properties();
	private static Properties reportProperties = new Properties();
	private static Properties reportPathMapProperties = new Properties();
	private static InputStream inReportName = ReportingProperties.class.getResourceAsStream("/report/ReportNames.properties");
	private static InputStream inReport= ReportingProperties.class.getResourceAsStream("/report/Report.properties");
	private static InputStream inReportPathMap = ReportingProperties.class.getResourceAsStream("/report/ReportPathMappings.properties");
	
	static {
		try {
			reportNameProperties.load(inReportName);
			inReportName.close();
			reportProperties.load(inReport);
			inReport.close();
			reportPathMapProperties.load(inReportPathMap);
			inReportPathMap.close();
			
		} catch (IOException e) {
			logger.error(e);			
		} finally {

		}
	}
	
	private static String getValue(String key,Properties properties) {
		try {			
			return properties.getProperty(key);
		} catch (Exception e) {
			logger.debug("key not found: " + key);
			return "";
		}
	}
	
	private static List<String> getKey(String value, Properties properties) {
		Map<String, List<String>> mapValKey = new HashMap<String, List<String>>();
		List<String> lsts = new ArrayList<String>();
		try {			
			mapValKey = getValueKeyMapOfProperties(ReportingProperties.reportNameProperties);
			lsts = mapValKey.get(value);
		} catch (Exception e) {
			logger.debug("key not found: " + value);			
		}
		return lsts;
	}
	
	private static Map<String, List<String>> getValueKeyMapOfProperties(Properties properties) {
		Map<String, List<String>> mapValKey = new HashMap<String, List<String>>();
		try {			
			Set<Entry<Object, Object>>  set =  properties.entrySet();
			for (Entry<Object, Object> entry : set) {
				List<String> lst = new ArrayList<String>();
				if(mapValKey.containsKey(entry.getValue().toString())){
					lst = mapValKey.get(entry.getValue().toString());
					lst.add(entry.getKey().toString());					
				}else{
					lst.add(entry.getKey().toString());					
				}
				mapValKey.put(entry.getValue().toString(), lst);
			}
		} catch (Exception e) {
			logger.debug("conversion failed" + e );			
		}
		return mapValKey;
	}

	private String fetchDetails(String reportName){
		List<String> reportKey = new ReportingProperties().getKey(reportName,ReportingProperties.reportNameProperties);
		String key = null;		
		for (String string : reportKey) {
			if(!string.contains("_DB2")){
				key = string;
			}			
		}			
		return key.replace("report.", "");
	}
	
	// file from the user need to incorporate
	// check resource 
	public static void main(String[] args) throws Exception {		
		ReportingProperties rep= new ReportingProperties();
		Map<String, String> reportDataObjMap = new HashMap<String, String>();		
		
		List<String> lstStr = rep.readFromFile();
		//NewReportUtilityConf newReportUtilityConf = new NewReportUtilityConf();
		for (String string : lstStr) {
			System.out.println("**********************"+string+"**********************");
			Map<String, Object> map1 = rep.processDataSet(string);		
			String fileNameLoc = rep.writeToFile(rep.createCSV(map1),string);			
			//newReportUtilityConf.outSideCall(fileNameLoc);
		}
	}
	
	private Map<String, Object> getReprotDetail(String reportName, Map<String, Object> roxMap){
		String execution_path = getValue("report."+reportName+".execution.path",ReportingProperties.reportProperties);		
		roxMap.put(WBtoERSConstants.EXECUTION_PATH.cnKey(), execution_path);
		if (execution_path!=null && "QMR".equalsIgnoreCase(execution_path)) {						
			roxMap.put(WBtoERSConstants.REPORT_EXECUTION_AND_RENDERING_ENGINE.cnKey(), "<ALL|QMR|ERT>,<PDF|QMR|ACT>,<DHT|QMR|ACT>");
			roxMap.put(WBtoERSConstants.REPORT_DISTRIBUTION_FORMAT.cnKey(), getValue("report."+reportName+".outputTypes",ReportingProperties.reportProperties));
			roxMap.put(WBtoERSConstants.REPORT_VIEW_FORMAT.cnKey(), getValue("report."+reportName+".outputTypes.view",ReportingProperties.reportProperties));
		}else{
			roxMap.put(WBtoERSConstants.REPORT_EXECUTION_AND_RENDERING_ENGINE.cnKey(), "<ALL|ACT|ACT>");
			roxMap.put(WBtoERSConstants.REPORT_DISTRIBUTION_FORMAT.cnKey(), "RPT,XLS,XML,CSH,CSN,TSH,TSN,FXD");
			roxMap.put(WBtoERSConstants.REPORT_VIEW_FORMAT.cnKey(), "DHT,RPT,TBL");
		}
		return roxMap;
	}
	
	public Map<String, Object> processDataSet(String reportName){
		//step1: fetch the details from db
		BnymDAOImpl bb = new BnymDAOImpl();
		List<Map<String, Object>> mappedList = bb.reportParam(reportName);
		Map<String, Object> roxMap = new HashMap<String, Object>(); 

		//step2: get report key from the rox file name
		for (Map<String, Object> map : mappedList) {
			if(validReportName(map)){
				roxMap=map;				
				roxMap.put(WBtoERSConstants.KEY.cnKey(), map.get(WBtoERSConstants.REPORT_ROX_NAME.cnKey()).toString().replace(".rox", ""));
				roxMap.put(WBtoERSConstants.REPORT_CATEGORY_FOLDER_PATH.cnKey(),reportCategoryFolderPath(map.get(WBtoERSConstants.REPORT_ROX_NAME.cnKey()).toString().replace(".rox", "")));
				
				getReprotDetail(map.get(WBtoERSConstants.REPORT_ROX_NAME.cnKey()).toString().replace(".rox", ""),roxMap);
				roxMap.put("REPORT_ENGINE_KEY",map.get("REPORT_ROX_PATH").toString()+"/"+map.get(WBtoERSConstants.REPORT_ROX_NAME.cnKey()).toString());
				roxMap.put("UPS_PRODUCT_ID",map.get("REPORT_ROX_PATH").toString()+"/"+map.get(WBtoERSConstants.REPORT_ROX_NAME.cnKey()).toString());				
				
				roxMap.put(WBtoERSConstants.VIEW_EXPORT_DEFAULT.cnKey(),WBtoERSConstants.VIEW_EXPORT_DEFAULT.cnValue());
				roxMap.put(WBtoERSConstants.IS_LOB_HOSTING_PARAMETER_PAGE.cnKey(),WBtoERSConstants.IS_LOB_HOSTING_PARAMETER_PAGE.cnValue());
				roxMap.put(WBtoERSConstants.WHEN_DATA_AVAILABLE_OPERATOR.cnKey(),WBtoERSConstants.WHEN_DATA_AVAILABLE_OPERATOR.cnValue());
				
				roxMap.put(WBtoERSConstants.BASE_URLTO_ACCESS_LOB_PARAMETER_PAGE.cnKey(),WBtoERSConstants.BASE_URLTO_ACCESS_LOB_PARAMETER_PAGE.cnValue());				
				roxMap.put(WBtoERSConstants.REPORT_EXPORT_FORMAT.cnKey(),"");				
				roxMap.put(WBtoERSConstants.REPORT_APPLICATION_MNEMONIC.cnKey(),WBtoERSConstants.REPORT_APPLICATION_MNEMONIC.cnValue());
				roxMap.put(WBtoERSConstants.REPORT_MANAGEMENT_COST_CENTER.cnKey(),WBtoERSConstants.REPORT_MANAGEMENT_COST_CENTER.cnValue());				
				roxMap.put(WBtoERSConstants.REPORT_MANAGEMENT_UNIT.cnKey(),WBtoERSConstants.REPORT_MANAGEMENT_UNIT.cnValue());
				roxMap.put(WBtoERSConstants.LDAP_GROUP.cnKey(),WBtoERSConstants.LDAP_GROUP.cnValue());				
				roxMap.put(WBtoERSConstants.EXECUTION_CODE.cnKey(),WBtoERSConstants.EXECUTION_CODE.cnValue());
				
				roxMap.put(WBtoERSConstants.WHEN_ACCOUNT_IS_FINAL_SUPPORTED.cnKey(),WBtoERSConstants.WHEN_ACCOUNT_IS_FINAL_SUPPORTED.cnValue());
				roxMap.put(WBtoERSConstants.WHEN_ACCOUNT_FINAL_EXPIRATION_DATE_IN_DAYS.cnKey(),WBtoERSConstants.WHEN_ACCOUNT_FINAL_EXPIRATION_DATE_IN_DAYS.cnValue());				
				roxMap.put(WBtoERSConstants.REPORT_EXECUTION_DYNAMIC_PARAMS.cnKey(),WBtoERSConstants.REPORT_EXECUTION_DYNAMIC_PARAMS.cnValue());
				
				roxMap.put(WBtoERSConstants.STATIC_PARAMETERS_THAT_WILL_BE_PASSED_WITH_URL.cnKey(),staticParameterURL(reportName));
				
				detailsWDA(roxMap);
			}
		}		
		return roxMap;
	}	
	
	/**
	 * need to write the date from various regions
	 * @return
	 */
	private String staticParameterURL(String reportName){
		OutputQueryFactory opFac = OutputQueryFactory.getInstance();
		//String urlString = "acShellId=@test-nr|{@}~test|{@}~qa-nr|{@}~qa|{@}~prod|{@}@&hideSubject=true&appType=eReporting.mellon&menuId=";
		String urlString = "acShellId=@test-nr|{@}$test|{@}$qa-nr|{@}$qa|{@}$prod|{@}@&hideSubject=true&appType=eReporting.mellon&menuId=";
		String[] strArr = new String[]{getAcReportShellId(reportName, "testNR"),getAcReportShellId(reportName, "test"),
				getAcReportShellId(reportName, "qaNR"),getAcReportShellId(reportName, "qa"),getAcReportShellId(reportName, "")};
		String paramUrl = opFac.getSubstitutedQuery(urlString,"{@}",strArr);
				
		return paramUrl;
	}
	
	private String getAcReportShellId(String reportName, String region){
		BnymDAOImpl bb = new BnymDAOImpl();
		List<Map<String, Object>> mappedList = new ArrayList<Map<String,Object>>();			
		mappedList = bb.reportParam(reportName,region);
		String shellId = "";
		for (Map<String, Object> map : mappedList) {
			if(validReportName(map)){
				shellId = map.get(WBtoERSConstants.AC_REPORT_SHELL_ID.cnKey()).toString();
			}
		}
		return shellId;
	}
	
	private boolean validReportName(Map<String, Object> map){
		boolean validReportName =false;
		
		if(!map.get(WBtoERSConstants.REPORT_ROX_NAME.cnKey()).toString().contains("_DB2")
				&& !map.get(WBtoERSConstants.REPORT_ROX_NAME.cnKey()).toString().contains("_v2") 
				&& !map.get(WBtoERSConstants.REPORT_ROX_NAME.cnKey()).toString().contains("_Exadata")){			
			validReportName=true;			
		}		
		return validReportName;
	}
	
	private void detailsWDA(Map<String, Object> roxMap){
		BnymDAOImpl bb = new BnymDAOImpl();
		List<Map<String, Object>> mappedList = bb.isWDA(roxMap.get("AC_REPORT_SHELL_ID").toString());
		if(mappedList!=null & mappedList.size()>0){			
			roxMap.put(WBtoERSConstants.WHEN_DATA_IS_AVAILABLE_SUPPORTED.cnKey(),"Y");
			roxMap.put(WBtoERSConstants.WHEN_DATA_IS_AVAILABLE_EXPIRATION_DATE.cnKey(),WBtoERSConstants.WHEN_DATA_IS_AVAILABLE_EXPIRATION_DATE.cnValue());			
			roxMap.put(WBtoERSConstants.WHEN_DATA_AVAILABLE_EVENT_IDS.cnKey(),eventDetailsWDA(mappedList));			
		}else{
			roxMap.put("WHEN_DATA_IS_AVAILABLE_SUPPORTED","N");
			roxMap.put("WHEN_DATA_IS_AVAILABLE_EXPIRATION_DATE","NA");
			roxMap.put("WHEN_DATA_AVAILABLE_EVENT_IDS","NA");
		}
	}
	
	private String eventDetailsWDA(List<Map<String, Object>> mappedList){
		String eventId = null;
		BnymDAOImpl bb = new BnymDAOImpl();
		for (int i = 0; i < 1; i++) {
			Map<String, Object> mapped  = mappedList.get(0);			
			eventId = bb.eventIdWDA( mapped.get("REPORT_CONFIG_ID").toString()).get(0).get("EVENT_CODE").toString(); 
		}
		return eventId;
	}
	
	private String detailsRWF(List<Map<String, Object>> mappedList){
		String eventId = null;
		BnymDAOImpl bb = new BnymDAOImpl();
		for (int i = 0; i < 1; i++) {
			Map<String, Object> mapped  = mappedList.get(0);			
			eventId = bb.detailsRWF( mapped.get("REPORT_CONFIG_ID").toString()).get(0).get("EVENT_CODE").toString(); 
		}
		return eventId;
	}
	
	private String reportCategoryFolderPath(String reportName){
		String path =  getValue("__default.report."+reportName,ReportingProperties.reportPathMapProperties);		
		String[] ss = path.split("\\^");
		return ss[ss.length-1];
	}
	
	private String[] createCSV(Map<String, Object> mapStrObj){
		StringBuilder header = new StringBuilder();
		StringBuilder values = new StringBuilder();
		for (Map.Entry<String, Object> entMap: mapStrObj.entrySet()) {
			String kk = getKeyString(entMap.getKey());
			if(kk!=null && !"".equals(kk)){
				header.append(kk+DELIMITER_SYMBOL);
				values.append(entMap.getValue()+DELIMITER_SYMBOL);				
				System.out.println(kk + " |||| " + entMap.getValue());
			}
		}		
		String strDefaultLine = "Report Related Information~~~~~~~~~~~~~~~~~~~~~~~";
		String headerStr = header.toString();
		String valuesStr = values.toString();
		return new String[]{strDefaultLine,headerStr.substring(0, headerStr.length()-1), valuesStr.substring(0, valuesStr.length()-1)};
	}
	
	public static String getKeyString(String strKey){		 
		String sTru = null;
		for (WBtoERSConstants val : WBtoERSConstants.values()) {			
			if(strKey.equals(val.cnKey()) && !"".equals(val.columnHeader())){				
				sTru=val.columnHeader();
				break;
			}
		}
		return sTru;
	}
			
	private String writeToFile(String[] strArr, String fileName){
		String fileNameLoc="";
		try {			
			fileNameLoc="C:\\Data\\generatedERSscript\\"+fileName+".csv";
			BufferedWriter out = new BufferedWriter(new FileWriter(fileNameLoc));			
			for (String string : strArr) {
				out.write(string+"\n");
			}			
			out.close();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
		return fileNameLoc;
	}	
	
	private List<String> readFromFile(){
		List<String> lstStr =  new ArrayList<String>();
		try {			
			BufferedReader inBuff = new BufferedReader(new FileReader("C:\\Data\\generatedERSscript\\inputFile.txt"));			
			String line;
			while ((line = inBuff.readLine()) != null) {
				System.out.println(line);
				lstStr.add(line);
			}	
			inBuff.close();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
		return lstStr;
	}

}
