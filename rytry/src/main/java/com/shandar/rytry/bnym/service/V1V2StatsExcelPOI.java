/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.excelOperation </p>
 * <p>File Name: ExcelPOI.java</p>
 * <p>Create Date: Jul 11, 2014 </p>
 * <p>Create Time: 4:38:30 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.bnym.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFObjectData;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;

/**
 * @author Shantanu Sikdar 
 *
 */
public class V1V2StatsExcelPOI {
	
	public static void main(String[] args) {
		V1V2StatsExcelPOI xclPoi =  new V1V2StatsExcelPOI();		
		xclPoi.listOfExcelSheet("C:\\Data\\V1V2_Stats_Template.xls");		
	}
	
	public void readFromExcel(String fileName){
		
	}
	
	public void writeToExcel(){
		
	}
		
	public void updateAnExcel(String inputFileName){
		try {
		    FileInputStream file = new FileInputStream(new File(inputFileName));
		 
		    HSSFWorkbook workbook = new HSSFWorkbook(file);
		    HSSFSheet sheet = workbook.getSheetAt(0);
		    Cell cell = null;
		 
		    //Update the value of cell
		    cell = sheet.getRow(1).getCell(2);
		    cell.setCellValue(cell.getNumericCellValue() * 2);
		    cell = sheet.getRow(2).getCell(2);
		    cell.setCellValue(cell.getNumericCellValue() * 2);
		    cell = sheet.getRow(3).getCell(2);
		    cell.setCellValue(cell.getNumericCellValue() * 2);
		     
		    file.close();
		     
		    FileOutputStream outFile =new FileOutputStream(new File(inputFileName));
		    workbook.write(outFile);
		    outFile.close();
		     
		} catch (FileNotFoundException e) {
		    e.printStackTrace();
		} catch (IOException e) {
		    e.printStackTrace();
		}		
	}
	
	public void listOfExcelSheet(String inputFileName){
		try {
		    FileInputStream file = new FileInputStream(new File(inputFileName));		 
		    HSSFWorkbook workbook = new HSSFWorkbook(file);
		    int numOfSheets = workbook.getNumberOfSheets();
		    //System.out.println(workbook.getNumberOfSheets());
		    List<HSSFObjectData> sheetObject = workbook.getAllEmbeddedObjects();		    		    
		    for(int i=0;i<numOfSheets;i++) {
		    	HSSFSheet sheet = workbook.getSheetAt(i);
		    	System.out.println(i+" = "+sheet.getSheetName());
			}		    		    		 
		    
		} catch (FileNotFoundException e) {
		    e.printStackTrace();
		} catch (IOException e) {
		    e.printStackTrace();
		}	
	}

}
