/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.bnym.controller </p>
 * <p>File Name: ConnectJiraController.java</p>
 * <p>Create Date: Sep 18, 2014 </p>
 * <p>Create Time: 3:09:39 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.bnym.controller;

import java.util.Map;

import javax.naming.AuthenticationException;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.BindingResult;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.Base64;
import com.shandar.rytry.login.model.User;



/**
 * @author Shantanu Sikdar 
 *
 */
//@Controller
public class ConnectJiraController {
	
	private static final Logger logger = LoggerFactory.getLogger(ConnectJiraController.class);
	
	//@RequestMapping( value="AppADay")
    public String appADayView(User user, BindingResult result, Map model, HttpSession session) {	
		logger.debug("AppADay");
		System.out.println(session.getAttribute("userName"));
		logger.debug("userName 1 == " + session.getAttribute("userName"));
        return "appADay/appADay";
    }
	
	
    private static String BASE_URL = "https://bnymjira.bnymellon.net/jira";
    								  //https://bnymjira.bnymellon.net/jira/rest/api/2/project
    
    public static void main(String[] args) {
    	String auth = new String(Base64.encode("xbbkngi:20@Three"));
		
		try {
			//Get Projects
			String projects = invokeGetMethod(auth, BASE_URL+"/rest/api/2/project").trim().trim();
			 
			//System.out.println(projects);
			JSONArray projectArray = new JSONArray(projects);
			/*for (int i = 0; i < projectArray.length(); i++) {
				JSONObject proj = projectArray.getJSONObject(i);
				System.out.println("Key:"+proj.getString("key")+", Name:"+proj.getString("name"));
			}*/
			
			//Create Issue
			/*String createIssueData = "{\"fields\":{\"project\":{\"key\":\"DEMO\"},\"summary\":\"REST Test\",\"issuetype\":{\"name\":\"Bug\"}}}";
			String issue = invokePostMethod(auth, BASE_URL+"/rest/api/2/issue", createIssueData);
			System.out.println(issue);
			JSONObject issueObj = new JSONObject(issue);
			String newKey = issueObj.getString("key");
			System.out.println("Key:"+newKey);*/
			
			//Update Issue
			/*String editIssueData = "{\"fields\":{\"assignee\":{\"name\":\"test\"}}}";
			invokePutMethod(auth, BASE_URL+"/rest/api/2/issue/"+newKey, editIssueData);
			
			invokeDeleteMethod(auth, BASE_URL+"/rest/api/2/issue/DEMO-13");*/
			
		} catch (AuthenticationException e) {
			System.out.println("Username or Password wrong!");
			e.printStackTrace();
		} catch (ClientHandlerException e) {
			System.out.println("Error invoking REST method");
			e.printStackTrace();
		} catch (JSONException e) {
			System.out.println("Invalid JSON output");
			e.printStackTrace();
		}
	}
    
    private static String invokeGetMethod(String auth, String url) throws AuthenticationException, ClientHandlerException {
		Client client = Client.create();
		WebResource webResource = client.resource(url);
		ClientResponse response = webResource.header("Authorization", "Basic " + auth).type("application/json").accept("application/json").get(ClientResponse.class);
		int statusCode = response.getStatus();
		System.out.println("statusCode == "+statusCode);
		if (statusCode == 401) {
			throw new AuthenticationException("Invalid Username or Password");
		}
		return response.getEntity(String.class);
	}
	
	private static String invokePostMethod(String auth, String url, String data) throws AuthenticationException, ClientHandlerException {
		Client client = Client.create();
		WebResource webResource = client.resource(url);
		ClientResponse response = webResource.header("Authorization", "Basic " + auth).type("application/json").accept("application/json").post(ClientResponse.class, data);
		int statusCode = response.getStatus();
		if (statusCode == 401) {
			throw new AuthenticationException("Invalid Username or Password");
		}
		return response.getEntity(String.class);
	}
	
	private static void invokePutMethod(String auth, String url, String data) throws AuthenticationException, ClientHandlerException {
		Client client = Client.create();
		WebResource webResource = client.resource(url);
		ClientResponse response = webResource.header("Authorization", "Basic " + auth).type("application/json").accept("application/json").put(ClientResponse.class, data);
		int statusCode = response.getStatus();
		if (statusCode == 401) {
			throw new AuthenticationException("Invalid Username or Password");
		}
	}
	
	private static void invokeDeleteMethod(String auth, String url) throws AuthenticationException, ClientHandlerException {
		Client client = Client.create();
		WebResource webResource = client.resource(url);
		ClientResponse response = webResource.header("Authorization", "Basic " + auth).type("application/json").accept("application/json").delete(ClientResponse.class);
		int statusCode = response.getStatus();
		if (statusCode == 401) {
			throw new AuthenticationException("Invalid Username or Password");
		}
	}

}