/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.bnym.controller </p>
 * <p>File Name: AppADay.java</p>
 * <p>Create Date: Sep 18, 2014 </p>
 * <p>Create Time: 12:17:37 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.bnym.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.shandar.rytry.emailer.service.MSExchangeService;
import com.shandar.rytry.login.model.User;

/**
 * @author Shantanu Sikdar 
 *
 */
@Controller
public class AppADayController {

	private static final Logger logger = LoggerFactory.getLogger(AppADayController.class);

	@RequestMapping( value="AppADay")
    public ModelAndView appADayView(User user, BindingResult result, Map<String, Object> modelMap, HttpSession session) {
		MSExchangeService msExchangeService = new MSExchangeService();
		List<String[]> subList = formatSubLst(msExchangeService.getSubjectList(5));
		logger.debug("hello");
		modelMap.put("recent5Emails", subList);		
        return new ModelAndView("appADay/appADay", modelMap);
    }
	
	private List<String[]> formatSubLst(List<String[]> subList){
		List<String[]> formattedSubList =new ArrayList<String[]>();
		for (String[] string : subList) {
			String[] ss=new String[3];
			ss[0]=string[0];
			if(string[1].length()>20){
				ss[1]=string[1].substring(0, 20)+".....";
			}else{
				ss[1]=string[1];				
			}			
			ss[2]=string[1];
			formattedSubList.add(ss);			
		}
		return formattedSubList;
	}
}
