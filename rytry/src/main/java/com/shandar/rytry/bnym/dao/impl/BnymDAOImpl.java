/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.bnym.dao.impl </p>
 * <p>File Name: BnymDAOImpl.java</p>
 * <p>Create Date: Jul 14, 2014 </p>
 * <p>Create Time: 1:45:35 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.bnym.dao.impl;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.shandar.rytry.db.ConnectionFactory;
import com.shandar.rytry.db.DBConnection;
import com.shandar.rytry.db.DBQueries;
import com.shandar.rytry.db.impl.DBQueriesImpl;

/**
 * @author Shantanu Sikdar 
 *
 */
public class BnymDAOImpl {
	
	private static final Logger logger = LoggerFactory.getLogger(BnymDAOImpl.class);

	DBQueries dbQuery = new DBQueriesImpl("v1v2DAO-queries.xml");
	DBConnection dbConn = ConnectionFactory.getInstance().getConnectionOracle();
	DBConnection dbConnTest = ConnectionFactory.getInstance().getConnectionOracleTest();
	DBConnection dbConnTestNR = ConnectionFactory.getInstance().getConnectionOracleTestNR();
	DBConnection dbConnQA = ConnectionFactory.getInstance().getConnectionOracleQA();
	DBConnection dbConnQaNR = ConnectionFactory.getInstance().getConnectionOracleQaNR();
	
	
		
	public List<Map<String, Object>> reportParam(String reportName,String region){
		List<Map<String, Object>> mappedList = null;
		if("test".equals(region))
			dbConn = dbConnTest;			
		else if("testNR".equals(region))
			dbConn = dbConnTestNR;
		else if("qa".equals(region))
			dbConn = dbConnQA;
		else if("qaNR".equals(region))
			dbConn = dbConnQaNR;
		else
			dbConn = ConnectionFactory.getInstance().getConnectionOracle();
		
		mappedList = reportParam(reportName);		
		return mappedList;
	}
	
	public List<Map<String, Object>> reportParam(String reportName) {
		Map<String, String> queryMap = null;
		String query ;
		List<Map<String, Object>> mappedList = null;
		try{
			queryMap = dbQuery.getQueryBeanFromFactory("REPORT_PARAM");
			query =  dbQuery.getQuery(queryMap);
			mappedList = dbConn.dbQueryRead(query,new Object[]{reportName});
			
			
			
		}catch(Exception  ex){
			logger.error("reportParam :: ", ex);
		}
		return mappedList;
	}
	
	
	public List<Map<String, Object>> isWDA(String reportShellId) {
		Map<String, String> queryMap = null;
		String query ;
		List<Map<String, Object>> mappedList = null;
		try{
			queryMap = dbQuery.getQueryBeanFromFactory("WDA_QRY_1");
			query =  dbQuery.getQuery(queryMap);
			mappedList = dbConn.dbQueryRead(query,new Object[]{reportShellId});
		}catch(Exception  ex){
			logger.error("isWDA :: ", ex);
		}
		return mappedList;
	}
	
	public List<Map<String, Object>> eventIdWDA(String reportShellId) {
		Map<String, String> queryMap = null;
		String query ;
		List<Map<String, Object>> mappedList = null;
		try{
			queryMap = dbQuery.getQueryBeanFromFactory("WDA_QRY");
			query =  dbQuery.getQuery(queryMap);
			mappedList = dbConn.dbQueryRead(query,new Object[]{reportShellId});
		}catch(Exception  ex){
			logger.error("eventIdWDA :: ", ex);
		}
		//return mappedList.get(0).get("EVENT_CODE").toString();
		return mappedList;
	}
	
	public List<Map<String, Object>> detailsRWF(String reportShellId) {
		Map<String, String> queryMap = null;
		String query ;
		List<Map<String, Object>> mappedList = null;
		try{
			queryMap = dbQuery.getQueryBeanFromFactory("RWF_QRY");
			query =  dbQuery.getQuery(queryMap);
			mappedList = dbConn.dbQueryRead(query,new Object[]{reportShellId});
		}catch(Exception  ex){
			logger.error("detailsRWF :: ", ex);
		}
		return mappedList;
	}
	
	
	/*public static void main(String[] args) {
		System.out.println("Hello");
		BnymDAOImpl bb = new BnymDAOImpl();
		bb.avgTimeExecution();
		
		System.out.println("Hello World");
	}*/

}
