/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.bnym.service </p>
 * <p>File Name: WBtoERSConstants.java</p>
 * <p>Create Date: Jul 16, 2014 </p>
 * <p>Create Time: 8:19:55 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.bnym.service;

/**
 * @author Shantanu Sikdar
 * 
 */
public enum WBtoERSConstants {
	
	REPORT_ROX_DISPLAY_NAME ("REPORT_ROX_DISPLAY_NAME","Report Catalog - Report Name","" ), 
	REPORT_CATEGORY_FOLDER_PATH  ("REPORT_CATEGORY_FOLDER_PATH", "Report Category Folder Path","" ),
	REPORT_ENGINE_KEY ("REPORT_ENGINE_KEY", "Report Engine Key","" ),
	VIEW_EXPORT_DEFAULT ("VIEW_EXPORT_DEFAULT", "View/Export Default", "View" ),
	REPORT_VIEW_FORMAT ("REPORT_VIEW_FORMAT", "Report View Format","" ),
	REPORT_EXPORT_FORMAT ("REPORT_EXPORT_FORMAT", "Report Export Format","" ),
	REPORT_DISTRIBUTION_FORMAT ("REPORT_DISTRIBUTION_FORMAT","Report Distribution Format","" ), 
	REPORT_APPLICATION_MNEMONIC ("REPORT_APPLICATION_MNEMONIC", "Report Application Mnemonic", "WBS" ),
	REPORT_MANAGEMENT_UNIT("REPORT_MANAGEMENT_UNIT", "Report Management Unit", "AS" ), 
	REPORT_MANAGEMENT_COST_CENTER ("REPORT_MANAGEMENT_COST_CENTER", "Report Managent Cost Center", "1111" ),
	LDAP_GROUP("LDAP_GROUP", "LDAP Group", "<WCA|ACT>" ), 
	UPS_PRODUCT_ID ("UPS_PRODUCT_ID","UPS Product Id","" ), 
	EXECUTION_CODE("EXECUTION_CODE","Execution code", "ACT" ),
	REPORT_EXECUTION_AND_RENDERING_ENGINE("REPORT_EXECUTION_AND_RENDERING_ENGINE","Report Execution & Rendering Engine","" ),	
	IS_LOB_HOSTING_PARAMETER_PAGE("IS_LOB_HOSTING_PARAMETER_PAGE", "Is LOB hosting Parameter Page","Y" ),	
	BASE_URLTO_ACCESS_LOB_PARAMETER_PAGE("BASE_URLTO_ACCESS_LOB_PARAMETER_PAGE", "Base URLto access LOB parameter page", "https://workbench.test.bnymellon.com/cr/ERSparampage.jsp" ),
	STATIC_PARAMETERS_THAT_WILL_BE_PASSED_WITH_URL ("STATIC_PARAMETERS_THAT_WILL_BE_PASSED_WITH_URL", "Static parameters that will be passed with URL","" ),
	WHEN_DATA_IS_AVAILABLE_SUPPORTED ("WHEN_DATA_IS_AVAILABLE_SUPPORTED", "When data  is available supported","" ),	
	WHEN_DATA_IS_AVAILABLE_EXPIRATION_DATE ("WHEN_DATA_IS_AVAILABLE_EXPIRATION_DATE", "When data  is available Expiration date", "Day=+1;Time=12:09" ),	
	WHEN_DATA_AVAILABLE_EVENT_IDS ("WHEN_DATA_AVAILABLE_EVENT_IDS", "When data available event  Ids","" ),	
	WHEN_DATA_AVAILABLE_OPERATOR ("WHEN_DATA_AVAILABLE_OPERATOR", "When data available operator","" ),		
	WHEN_ACCOUNT_IS_FINAL_SUPPORTED ("WHEN_ACCOUNT_IS_FINAL_SUPPORTED", "When account is final supported","Y" ),	
	WHEN_ACCOUNT_FINAL_EXPIRATION_DATE_IN_DAYS ("WHEN_ACCOUNT_FINAL_EXPIRATION_DATE_IN_DAYS", "When account final  Expiration date (in Days)", "Default" ),	
	REPORT_EXECUTION_DYNAMIC_PARAMS ("REPORT_EXECUTION_DYNAMIC_PARAMS", "Report Execution Dynamic Params","Profile_Data" ),
			  
			  
	EXECUTION_PATH ("EXECUTION_PATH", "","" ),
	AC_REPORT_SHELL_ID ("AC_REPORT_SHELL_ID", "", ""), 
	REPORT_ROX_NAME("REPORT_ROX_NAME", "","" ), 
	REPORT_ROX_VERSION ("REPORT_ROX_VERSION", "","" ), 
	REPORT_ROX_PATH ("AC_REPORT_SHELL_ID", "","" ), 
	CREATE_USER  ("CREATE_USER", "","" ), 
	CREATE_DATE ("CREATE_DATE", "","" ), 
	KEY ("KEY","","")
	;			  

	/*REPORT_ROX_DISPLAY_NAME("Report Catalog - Report Name",""), 
	REPORT_CATEGORY_FOLDER_PATH("Report Category Folder Path",""), 
	REPORT_ENGINE_KEY("Report Engine Key",""), 
	VIEW_EXPORT_DEFAULT("View/Export Default","View"),
	REPORT_VIEW_FORMAT("Report View Format",""),
	REPORT_EXPORT_FORMAT("Report Export Format",""),
	REPORT_DISTRIBUTION_FORMAT("Report Distribution Format",""),
	REPORT_APPLICATION_MNEMONIC("Report Application Mnemonic","WBS"),
	REPORT_MANAGEMENT_UNIT("Report Management Unit","AST"),
	REPORT_MANAGEMENT_COST_CENTER("Report Managent Cost Center","1111"), 
	LDAP_GROUP("LDAP Group","<WCA|ACT>"),
	UPS_PRODUCT_ID("UPS Product Id",""),
	EXECUTION_CODE("Execution code","ACT"),
	REPORT_EXECUTION_AND_RENDERING_ENGINE("Report Execution & Rendering Engine",""),
	IS_LOB_HOSTING_PARAMETER_PAGE("Is LOB hosting Parameter Page",""),
	BASE_URLTO_ACCESS_LOB_PARAMETER_PAGE("Base URLto access LOB parameter page","https://workbench.test.bnymellon.com/cr/ERSparampage.jsp"),
	STATIC_PARAMETERS_THAT_WILL_BE_PASSED_WITH_URL("Static parameters that will be passed with URL",""),
	WHEN_DATA_IS_AVAILABLE_SUPPORTED("When data  is available supported",""),
	WHEN_DATA_IS_AVAILABLE_EXPIRATION_DATE("When data  is available Expiration date","Day=+1;Time=12:09"),
	WHEN_DATA_AVAILABLE_EVENT_IDS("When data available event Ids",""),
	WHEN_DATA_AVAILABLE_OPERATOR("When data available operator",""), 
	WHEN_ACCOUNT_IS_FINAL_SUPPORTED("When account is final supported","Y"),
	WHEN_ACCOUNT_FINAL_EXPIRATION_DATE_IN_DAYS("When account final Expiration date (in Days)","Default"),
	REPORT_EXECUTION_DYNAMIC_PARAMS("Report Execution Dynamic Params", ""),

	AC_REPORT_SHELL_ID("", ""),
	REPORT_ROX_NAME("", ""),
	REPORT_ROX_VERSION("", ""), 
	REPORT_ROX_PATH("", ""), 
	CREATE_USER("", ""), 
	CREATE_DATE("", ""), 
	KEY("KEY",""), 
	EXECUTION_PATH("", "");*/
	
	private final String key;
	private final String columnHeader;
	private final String constantValue;
	
	WBtoERSConstants(String key, String columnHeader, String value) {
		this.key = key;
		this.columnHeader = columnHeader;
		this.constantValue = value;
	}
	
/*	private final String columnOrder;

	WBtoERSConstants(String key, String columnHeader, String value, String columnOrder) {
		this.key = key;
		this.columnHeader = columnHeader;
		this.constantValue = value;
		this.columnOrder=columnOrder;
	}*/
	
	 public String cnKey()   { return key; }
	 
	 public String columnHeader() { return columnHeader; }
	 public String cnValue() { return constantValue; }
	 

}
