/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.bnym </p>
 * <p>File Name: V1V2Controller.java</p>
 * <p>Create Date: Jul 9, 2014 </p>
 * <p>Create Time: 6:16:31 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.bnym.controller;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author Shantanu Sikdar 
 *
 */
@Controller
public class V1V2Controller {
	
	private static final Logger logger = LoggerFactory.getLogger(V1V2Controller.class);
	
	@RequestMapping("ViewSR")
	public ModelAndView viewSR() {
		logger.debug("session in viewSR ");
		Map<String, String> modelMap = new HashMap<String, String>();
		return new ModelAndView("bnym/viewSR", modelMap);		
	}
	
	
	
	
}
