/**
 *
 * <p>Project: WbToERS_trunk </p>
 * <p>Package Name: com.bnym.WbToErs.v1v2.dao </p>
 * <p>File Name: V1V2DAOImpl.java</p>
 * <p>Create Date: Jul 30, 2014 </p>
 * <p>Create Time: 4:53:34 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.bnym.dao.impl;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.shandar.rytry.db.ConnectionFactory;
import com.shandar.rytry.db.DBQueries;
import com.shandar.rytry.db.impl.DBConnectionImpl;
import com.shandar.rytry.db.impl.DBQueriesImpl;



/**
 * @author Shantanu Sikdar 
 *
 */
public class V1V2DAOImpl {
	
	private static final Logger logger = LoggerFactory.getLogger(V1V2DAOImpl.class);
	
	
	
	DBQueries dbQuery = new DBQueriesImpl("v1DAO-queries.xml");
	DBConnectionImpl dbConn = (DBConnectionImpl)ConnectionFactory.getInstance().getConnectionOracle();
	
	

	public void avgTimeExecution() {
		Map<String, String> queryMap = null;
		String query ;
		List<Map<String, Object>> mappedList = null;
		try{
			queryMap = dbQuery.getQueryBeanFromFactory("AVG_TIME_EXECUTION");
			query =  dbQuery.getQuery(queryMap);
			mappedList = dbConn.dbQueryRead(query);
			System.out.println(mappedList);
		}catch(Exception  ex){
			logger.error("AVG_TIME_EXECUTION :: ", ex);
		}		
	}
	
	public void executionTime() {
		Map<String, String> queryMap = null;
		String query ;
		List<Map<String, Object>> mappedList = null;
		try{
			queryMap = dbQuery.getQueryBeanFromFactory("EXECUTION_TIME");
			query =  dbQuery.getQuery(queryMap);
			mappedList = dbConn.dbQueryRead(query);
			System.out.println(mappedList);
		}catch(Exception  ex){
			logger.error("EXECUTION_TIME :: ", ex);
		}		
	}
	
	public void waitTime() {
		Map<String, String> queryMap = null;
		String query ;
		List<Map<String, Object>> mappedList = null;
		try{
			queryMap = dbQuery.getQueryBeanFromFactory("WAIT_TIME");
			query =  dbQuery.getQuery(queryMap);
			mappedList = dbConn.dbQueryRead(query);
			System.out.println(mappedList);
		}catch(Exception  ex){
			logger.error("AVG_TIME_EXECUTION :: ", ex);
		}		
	}
	
	public void webcrStats() {
		Map<String, String> queryMap = null;
		String query ;
		List<Map<String, Object>> mappedList = null;
		try{
			queryMap = dbQuery.getQueryBeanFromFactory("WEBCR_STATS");
			query =  dbQuery.getQuery(queryMap);
			mappedList = dbConn.dbQueryRead(query);
			System.out.println(mappedList);
		}catch(Exception  ex){
			logger.error("WEBCR_STATS :: ", ex);
		}		
	}	
		
	
	
		

}
