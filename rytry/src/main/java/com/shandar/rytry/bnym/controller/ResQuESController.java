/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.bnym.controller </p>
 * <p>File Name: ResQuESController.java</p>
 * <p>Create Date: Aug 7, 2015 </p>
 * <p>Create Time: 12:34:26 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.bnym.controller;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.shandar.rytry.common.dataObject.UIControlsContainer;

/**
 * @author Shantanu Sikdar 
 *
 */
@Controller
public class ResQuESController {

	@RequestMapping(value = "resques", method = RequestMethod.GET)	
	public ModelAndView resquesMain(@ModelAttribute("UIControlsContainer") UIControlsContainer uiCC) {
		return new ModelAndView("resques/resquesDashboard");		
	}
	
	@RequestMapping( value="addEmailTemplate")
	public @ResponseBody String addEmailTemplate(@RequestParam(value = "voipNumber") String voipNumber,HttpSession session) {
		
		return "";
	}
	
	@RequestMapping( value="addQuery")
	public @ResponseBody String addQuery(@RequestParam(value = "voipNumber") String voipNumber,HttpSession session) {
		
		return "";
	}
}
