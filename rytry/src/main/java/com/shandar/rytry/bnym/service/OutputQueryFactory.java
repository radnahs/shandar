/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.bnym.service </p>
 * <p>File Name: OutputQueryFactory.java</p>
 * <p>Create Date: Jul 16, 2014 </p>
 * <p>Create Time: 3:43:07 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.bnym.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.shandar.rytry.bnym.properties.ReportingProperties;

/**
 * @author Shantanu Sikdar 
 *
 */
public class OutputQueryFactory {
	
	private static final Logger logger = LoggerFactory.getLogger(OutputQueryFactory.class);
	
	ApplicationContext appContxt;
	
	private static OutputQueryFactory outputQueryFactory = null;

	private OutputQueryFactory() {
		appContxt = new ClassPathXmlApplicationContext("ersInsertDAO-queries.xml");
	}

	public static OutputQueryFactory getInstance() {
		if (outputQueryFactory == null) {
			outputQueryFactory = new OutputQueryFactory();
		}
		return outputQueryFactory;
	}
	
	public Map<String, String> getQueryBeanFromFactory(String beanId){
		Map<String, String> queryMap = null;
		if (appContxt != null && beanId != null) {
			queryMap = (Map<String, String>)appContxt.getBean(beanId);
		}
		return queryMap;
	}
	
	private void processAC_REPORT_SHELL(String queryAcReportShell, Map<String, String> reportDataObjMap){
		String REPORT_ROX_NAME = reportDataObjMap.get("REPORT_ROX_NAME").toString();
		List<String> lstOfQry = new ArrayList<String>();
		String[] qryStrArr = new String[2];
		qryStrArr[0] = REPORT_ROX_NAME;
		lstOfQry.add(getSubstitutedQuery(queryAcReportShell, "?" , new String[]{REPORT_ROX_NAME,REPORT_ROX_NAME}));
		System.out.println("processACF_REPORT_MAPPING =  "+lstOfQry);
	}
	
	private void processACF_REPORT_MAPPING(String queryAcfReportMapping, Map<String, Object> reportDataObjMap){
		String REPORT_ROX_NAME = reportDataObjMap.get("REPORT_ROX_NAME").toString();
		List<String> lstOfQry = new ArrayList<String>();
		String[] qryStrArr = new String[2];
		qryStrArr[0] = REPORT_ROX_NAME;
		lstOfQry.add(getSubstitutedQuery(queryAcfReportMapping, "?" , new String[]{REPORT_ROX_NAME,REPORT_ROX_NAME}));
		System.out.println("processACF_REPORT_MAPPING =  "+lstOfQry);
	}
	
	
	private void processAC_PARAMETER_DEF(String queryAcParameterDef, Map<String, Object> reportDataObjMap){
		String REPORT_ROX_NAME = reportDataObjMap.get("REPORT_ROX_NAME").toString();
		List<String> lstOfQry = new ArrayList<String>();
		String[] qryStrArr = new String[21];
		qryStrArr[0] = REPORT_ROX_NAME;
		lstOfQry.add(getSubstitutedQuery(queryAcParameterDef, "?" , new String[]{REPORT_ROX_NAME,"OPTIONAL_DIST_FORMAT","OPTIONAL_DIST_FORMAT","String",null,"N","RPT","N",null,"N","N","1","0","1","DROPDOWN",null,null,null,null,null,null}));
		lstOfQry.add(getSubstitutedQuery(queryAcParameterDef, "?" , new String[]{REPORT_ROX_NAME,"OPTIONAL_OUTPUT_TYPE","OPTIONAL_OUTPUT_TYPE","String",null,"N","RPT","N",null,"N","N","1","0","1","DROPDOWN",null,null,null,null,null,null}));
		lstOfQry.add(getSubstitutedQuery(queryAcParameterDef, "?" , new String[]{REPORT_ROX_NAME,"OPTIONAL_VIEW_TYPE","OPTIONAL_VIEW_TYPE","String",null,"N","RPT","N",null,"N","N","1","0","1","DROPDOWN",null,null,null,null,null,null}));
		lstOfQry.add(getSubstitutedQuery(queryAcParameterDef, "?" , new String[]{REPORT_ROX_NAME,"Profile_Data","Profile_Data","String",null,"N","RPT","N",null,"N","N","1","0","1","DROPDOWN",null,null,null,null,null,null}));
		System.out.println("processAC_PARAMETER_DEF =  "+lstOfQry);
	}
	
	/**
	 * 
	 * @param queryAcParameterDefValues
	 * @param reportDataObjMap
	 */
	private void processAC_PARAMETER_DEF_VALUES(String queryAcParameterDefValues, Map<String, Object> reportDataObjMap){
		String REPORT_ROX_NAME = reportDataObjMap.get("REPORT_ROX_NAME").toString();
		String[] reportDistributionFormat = reportDataObjMap.get("REPORT_DISTRIBUTION_FORMAT").toString().split(",");
		//REPORT_DISTRIBUTION_FORMAT== RPT,XLS,XML,CSH,CSN,TSH,TSN,FXD,IVP,IVX,IVC,IVT
		List<String> lstOfQry = new ArrayList<String>();
		String[] qryStrArr = new String[5];
		qryStrArr[0] = REPORT_ROX_NAME;
		
		for (String string : reportDistributionFormat) {
			if("RPT".equalsIgnoreCase(string)){
				qryStrArr[1] = "OPTIONAL_DIST_FORMAT";
				qryStrArr[2] = "Report (PDF view)";
				qryStrArr[3] = "RPT";
				lstOfQry.add(getSubstitutedQuery(queryAcParameterDefValues,"?",qryStrArr));
				qryStrArr[1] = "OPTIONAL_OUTPUT_TYPE";
				lstOfQry.add(getSubstitutedQuery(queryAcParameterDefValues,"?",qryStrArr));
				qryStrArr[1] = "OPTIONAL_VIEW_TYPE";
				lstOfQry.add(getSubstitutedQuery(queryAcParameterDefValues,"?",qryStrArr));
			}else if("XLS".equalsIgnoreCase(string)){
				qryStrArr[1] = "OPTIONAL_DIST_FORMAT";
				qryStrArr[2] = "XLS with Headers";
				qryStrArr[3] = "XLS";
				lstOfQry.add(getSubstitutedQuery(queryAcParameterDefValues,"?",qryStrArr));
				qryStrArr[1] = "OPTIONAL_OUTPUT_TYPE";
				lstOfQry.add(getSubstitutedQuery(queryAcParameterDefValues,"?",qryStrArr));
				qryStrArr[1] = "OPTIONAL_VIEW_TYPE";
				lstOfQry.add(getSubstitutedQuery(queryAcParameterDefValues,"?",qryStrArr));
			}else if("XNP".equalsIgnoreCase(string)){
				qryStrArr[1] = "OPTIONAL_DIST_FORMAT";
				qryStrArr[2] = "Combined(PDF & XLS)";
				qryStrArr[3] = "XNP";
				lstOfQry.add(getSubstitutedQuery(queryAcParameterDefValues,"?",qryStrArr));
				qryStrArr[1] = "OPTIONAL_OUTPUT_TYPE";
				lstOfQry.add(getSubstitutedQuery(queryAcParameterDefValues,"?",qryStrArr));
				qryStrArr[1] = "OPTIONAL_VIEW_TYPE";
				lstOfQry.add(getSubstitutedQuery(queryAcParameterDefValues,"?",qryStrArr));
			}else if("CSH".equalsIgnoreCase(string)){
				qryStrArr[1] = "OPTIONAL_DIST_FORMAT";
				qryStrArr[2] = "CSV with Headers";
				qryStrArr[3] = "CSH";
				lstOfQry.add(getSubstitutedQuery(queryAcParameterDefValues,"?",qryStrArr));
				qryStrArr[1] = "OPTIONAL_OUTPUT_TYPE";
				lstOfQry.add(getSubstitutedQuery(queryAcParameterDefValues,"?",qryStrArr));
				qryStrArr[1] = "OPTIONAL_VIEW_TYPE";
				lstOfQry.add(getSubstitutedQuery(queryAcParameterDefValues,"?",qryStrArr));
			}else if("CSN".equalsIgnoreCase(string)){
				qryStrArr[1] = "OPTIONAL_DIST_FORMAT";
				qryStrArr[2] = "CSV without headers";
				qryStrArr[3] = "CSN";
				lstOfQry.add(getSubstitutedQuery(queryAcParameterDefValues,"?",qryStrArr));
				qryStrArr[1] = "OPTIONAL_OUTPUT_TYPE";
				lstOfQry.add(getSubstitutedQuery(queryAcParameterDefValues,"?",qryStrArr));
				qryStrArr[1] = "OPTIONAL_VIEW_TYPE";
				lstOfQry.add(getSubstitutedQuery(queryAcParameterDefValues,"?",qryStrArr));
			}else if("TSH".equalsIgnoreCase(string)){
				qryStrArr[1] = "OPTIONAL_DIST_FORMAT";
				qryStrArr[2] = "TSV with headers";
				qryStrArr[3] = "TSH";
				lstOfQry.add(getSubstitutedQuery(queryAcParameterDefValues,"?",qryStrArr));
				qryStrArr[1] = "OPTIONAL_OUTPUT_TYPE";
				lstOfQry.add(getSubstitutedQuery(queryAcParameterDefValues,"?",qryStrArr));
				qryStrArr[1] = "OPTIONAL_VIEW_TYPE";
				lstOfQry.add(getSubstitutedQuery(queryAcParameterDefValues,"?",qryStrArr));
			}else if("TSN".equalsIgnoreCase(string)){
				qryStrArr[1] = "OPTIONAL_DIST_FORMAT";
				qryStrArr[2] = "TSV without headers";
				qryStrArr[3] = "TSN";
				lstOfQry.add(getSubstitutedQuery(queryAcParameterDefValues,"?",qryStrArr));
				qryStrArr[1] = "OPTIONAL_OUTPUT_TYPE";
				lstOfQry.add(getSubstitutedQuery(queryAcParameterDefValues,"?",qryStrArr));
				qryStrArr[1] = "OPTIONAL_VIEW_TYPE";
				lstOfQry.add(getSubstitutedQuery(queryAcParameterDefValues,"?",qryStrArr));
			}else if("FXD".equalsIgnoreCase(string)){
				qryStrArr[1] = "OPTIONAL_DIST_FORMAT";
				qryStrArr[2] = "Fixed Format";
				qryStrArr[3] = "FXD";
				lstOfQry.add(getSubstitutedQuery(queryAcParameterDefValues,"?",qryStrArr));
				qryStrArr[1] = "OPTIONAL_OUTPUT_TYPE";
				lstOfQry.add(getSubstitutedQuery(queryAcParameterDefValues,"?",qryStrArr));
				qryStrArr[1] = "OPTIONAL_VIEW_TYPE";
				lstOfQry.add(getSubstitutedQuery(queryAcParameterDefValues,"?",qryStrArr));
			}else if("IVP".equalsIgnoreCase(string)){
				qryStrArr[1] = "OPTIONAL_DIST_FORMAT";
				qryStrArr[2] = "Interactive PDF";
				qryStrArr[3] = "IVP";
				lstOfQry.add(getSubstitutedQuery(queryAcParameterDefValues,"?",qryStrArr));
				qryStrArr[1] = "OPTIONAL_OUTPUT_TYPE";
				lstOfQry.add(getSubstitutedQuery(queryAcParameterDefValues,"?",qryStrArr));
				qryStrArr[1] = "OPTIONAL_VIEW_TYPE";
				lstOfQry.add(getSubstitutedQuery(queryAcParameterDefValues,"?",qryStrArr));
			}else if("IVX".equalsIgnoreCase(string)){
				qryStrArr[1] = "OPTIONAL_DIST_FORMAT";
				qryStrArr[2] = "Interactive XLS";
				qryStrArr[3] = "IVX";
				lstOfQry.add(getSubstitutedQuery(queryAcParameterDefValues,"?",qryStrArr));
				qryStrArr[1] = "OPTIONAL_OUTPUT_TYPE";
				lstOfQry.add(getSubstitutedQuery(queryAcParameterDefValues,"?",qryStrArr));
				qryStrArr[1] = "OPTIONAL_VIEW_TYPE";
				lstOfQry.add(getSubstitutedQuery(queryAcParameterDefValues,"?",qryStrArr));
			}else if("IXP".equalsIgnoreCase(string)){
				qryStrArr[1] = "OPTIONAL_DIST_FORMAT";
				qryStrArr[2] = "Combined IVR(PDF & XLS)";
				qryStrArr[3] = "IXP";
				lstOfQry.add(getSubstitutedQuery(queryAcParameterDefValues,"?",qryStrArr));
				qryStrArr[1] = "OPTIONAL_OUTPUT_TYPE";
				lstOfQry.add(getSubstitutedQuery(queryAcParameterDefValues,"?",qryStrArr));
				qryStrArr[1] = "OPTIONAL_VIEW_TYPE";
				lstOfQry.add(getSubstitutedQuery(queryAcParameterDefValues,"?",qryStrArr));
			}else if("IVC".equalsIgnoreCase(string)){
				qryStrArr[1] = "OPTIONAL_DIST_FORMAT";
				qryStrArr[2] = "Interactive CSV";
				qryStrArr[3] = "IVC";
				lstOfQry.add(getSubstitutedQuery(queryAcParameterDefValues,"?",qryStrArr));
				qryStrArr[1] = "OPTIONAL_OUTPUT_TYPE";
				lstOfQry.add(getSubstitutedQuery(queryAcParameterDefValues,"?",qryStrArr));
				qryStrArr[1] = "OPTIONAL_VIEW_TYPE";
				lstOfQry.add(getSubstitutedQuery(queryAcParameterDefValues,"?",qryStrArr));
			}else if("IVT".equalsIgnoreCase(string)){
				qryStrArr[1] = "OPTIONAL_DIST_FORMAT";
				qryStrArr[2] = "Interactive TSV";
				qryStrArr[3] = "IVT";
				lstOfQry.add(getSubstitutedQuery(queryAcParameterDefValues,"?",qryStrArr));
				qryStrArr[1] = "OPTIONAL_OUTPUT_TYPE";
				lstOfQry.add(getSubstitutedQuery(queryAcParameterDefValues,"?",qryStrArr));
				qryStrArr[1] = "OPTIONAL_VIEW_TYPE";
				lstOfQry.add(getSubstitutedQuery(queryAcParameterDefValues,"?",qryStrArr));
			}
		}
		System.out.println("processAC_PARAMETER_DEF_VALUES ==== " + lstOfQry);
	}	
	
	/**
	 * Information still pending 
	 * @param queryTErcRssEvntMetad
	 * @param reportDataObjMap
	 */
	private void processT_ERC_RSS_EVNT_METAD(String queryTErcRssEvntMetad, Map<String, String> reportDataObjMap){
		String REPORT_ROX_NAME = reportDataObjMap.get("REPORT_ROX_NAME");		
		String formattedQry = getSubstitutedQuery(queryTErcRssEvntMetad, "?", new String[]{"shantanu_sikdar"} );
		System.out.println(formattedQry);		
	}
	
	public static void main(String[] args) {		
		/*String formattedQry = getSubstitutedQuery("selct * from shan where id=? and pt=? order by pt", "?", new String[]{"shan11", "2002"} );
		System.out.println(formattedQry);*/
		Map<String, Object> reportDataObjMap = new HashMap<String, Object>();
		
		ReportingProperties rep= new ReportingProperties();
		reportDataObjMap = rep.processDataSet("Custody Security Transactions");				
		
		OutputQueryFactory opFac = OutputQueryFactory.getInstance();
		//System.out.println(opFac.getQueryBeanFromFactory("REPORT_PARAM_ERS"));
		Map<String, String> qryMap = opFac.getQueryBeanFromFactory("REPORT_PARAM_ERS");
		
		//opFac.processAC_REPORT_SHELL(qryMap.get("AC_REPORT_SHELL"),reportDataObjMap);
		/*opFac.processACF_REPORT_MAPPING(qryMap.get("ACF_REPORT_MAPPING"),reportDataObjMap);
		opFac.processAC_PARAMETER_DEF(qryMap.get("AC_PARAMETER_DEF"),reportDataObjMap);
		opFac.processAC_PARAMETER_DEF_VALUES(qryMap.get("AC_PARAMETER_DEF_VALUES"),reportDataObjMap);*/
		//opFac.processT_ERC_RSS_EVNT_METAD(qryMap.get("T_ERC_RSS_EVNT_METAD"),reportDataObjMap);
				
		/**
		 * 
		 */
		opFac.fetchDeleteQueries(qryMap,reportDataObjMap);
	}
	
	private void fetchDeleteQueries(Map<String, String> qryMap,Map<String, Object>  reportDataObjMap){		
		List<String> lstOfQry = new ArrayList<String>();		
		lstOfQry.add(getSubstitutedQuery(qryMap.get("T_ERC_RSS_EVNT_METAD_DELETE"),"?",new String[]{reportDataObjMap.get("REPORT_ROX_NAME").toString()}));
		lstOfQry.add(getSubstitutedQuery(qryMap.get("AC_PARAMETER_DEF_VALUES_DELETE"),"?",new String[]{reportDataObjMap.get("REPORT_ROX_NAME").toString()}));
		lstOfQry.add(getSubstitutedQuery(qryMap.get("AC_PARAMETER_DEF_DELETE"),"?",new String[]{reportDataObjMap.get("REPORT_ROX_NAME").toString()}));
		lstOfQry.add(getSubstitutedQuery(qryMap.get("ACF_REPORT_MAPPING_DELETE"),"?",new String[]{reportDataObjMap.get("REPORT_ROX_NAME").toString()}));
		lstOfQry.add(getSubstitutedQuery(qryMap.get("AC_REPORT_SHELL_DELETE"),"?",new String[]{reportDataObjMap.get("REPORT_ROX_NAME").toString()}));
		System.out.println("processAC_PARAMETER_DEF_VALUES ==== " + lstOfQry);		
	}
	
	public static String getSubstitutedQuery(String query, String dynPrmPlaceHolder, String[] dynamicParams) {
		int queryLength = query.length();
		StringBuffer sb = new StringBuffer();
		int dynPrmPlaceHolderLength = dynPrmPlaceHolder.length();
		int index = query.indexOf(dynPrmPlaceHolder);
		int lastIndex = 0;
		int paramIndex = 0;
		while (index != -1) {
			sb.append(query.substring(lastIndex, index));
			sb.append(dynamicParams[paramIndex]);
			lastIndex = index + dynPrmPlaceHolderLength;
			if (lastIndex < queryLength) {
				index = query.indexOf(dynPrmPlaceHolder, lastIndex);
			} else {
				break;
			}
			paramIndex++;
		}
		if (lastIndex < queryLength) {
			sb.append(query.substring(lastIndex));
		}
		return sb.toString();
	}
	
}
