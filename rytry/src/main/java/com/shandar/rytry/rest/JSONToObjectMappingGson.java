/**
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.rest </p>
 * <p>File Name: JSONToObjectMappingGson.java </p>
 * <p>Create Date: 15-Sep-2021 </p>
 * <p>Create Time: 4:10:42 PM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.rytry.rest;

import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

/**
 * @author : Shantanu Sikdar
 * @Description : JSONToObjectMappingGson
 */
public class JSONToObjectMappingGson {

	private static void readContent(String jsonString) {

		if (isJSONValid(jsonString)) {
			Gson gson = new Gson();
			PersonBean mainBean = new PersonBean();
			mainBean = gson.fromJson(jsonString, PersonBean.class);
			System.out.println(mainBean);
		}
	}

	public static boolean isJSONValid(String jsonInString) {
		Gson gson = new Gson();
		try {
			gson.fromJson(jsonInString, PersonBean.class);
			return true;
		} catch (JsonSyntaxException ex) {
			return false;
		}
	}

	public static void main(String[] args) throws Exception {
		String jsonString = "{	\r\n" + "			\"Name\" : \"shantanu\", \r\n" + "			\"Age\" : 40, \r\n"
				+ "			\"Address\" : {\r\n" + "				\"line1\" : \"address line 1\",\r\n"
				+ "				\"line2\" : \"address line 2\"\r\n" + "			},\r\n" + "			\"Family\" :\r\n"
				+ "				[\r\n" + "				 {\r\n" + "					 \"Name\" : \"Piyali\",\r\n"
				+ "					 \"RelationShip\" : \"Wife\"\r\n" + "				 },\r\n"
				+ "				 {\r\n" + "					 \"Name\" : \"Kiti\",\r\n"
				+ "					 \"RelationShip\" : \"Sister\"\r\n" + "				 }\r\n" + "				]\r\n"
				+ "			\r\n" + "		}";

		readContent(jsonString);
	}
}

class PersonBean {
	private String Name;
	private int Age;
	private AddressBean Address;
	private List<FamilyBean> Family;

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public int getAge() {
		return Age;
	}

	public void setAge(int age) {
		Age = age;
	}

	public AddressBean getAddress() {
		return Address;
	}

	public void setAddress(AddressBean address) {
		Address = address;
	}

	public List<FamilyBean> getFamily() {
		return Family;
	}

	public void setFamily(List<FamilyBean> family) {
		Family = family;
	}

	@Override
	public String toString() {
		return "PersonBean [Name=" + Name + ", Age=" + Age + ", Address=" + Address + ", Family=" + Family + "]";
	}
	
}

class AddressBean {
	private String line1;
	private String line2;

	public String getLine1() {
		return line1;
	}

	public void setLine1(String line1) {
		this.line1 = line1;
	}

	public String getLine2() {
		return line2;
	}

	public void setLine2(String line2) {
		this.line2 = line2;
	}

	@Override
	public String toString() {
		return "AddressBean [line1=" + line1 + ", line2=" + line2 + "]";
	}

}

class FamilyBean {
	private String Name;
	private String RelationShip;

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getRelationShip() {
		return RelationShip;
	}

	public void setRelationShip(String relationShip) {
		RelationShip = relationShip;
	}

	@Override
	public String toString() {
		return "FamilyBean [Name=" + Name + ", RelationShip=" + RelationShip + "]";
	}
	
}
