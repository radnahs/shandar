package com.shandar.rytry.rest.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.X509TrustManager;

public class RestController {
	
	//http://stackoverflow.com/questions/10258101/sslhandshakeexception-no-subject-alternative-names-present
	static {
	    HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier()
	        {
	            public boolean verify(String hostname, SSLSession session)
	            {
	                // ip address of the service URL(like.23.28.244.244)
	                if (hostname.equals("10.59.112.33")) //original
	            	      return true;
	                return false;
	            }
	        });
	}

	public static void main(String[] args) throws Exception{
		
	/*	String https_url = "https://workbench.mellon.com/perf";
		
		URL url;
		
		url = new URL(https_url);
		HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
		System.out.println(con.getResponseCode());
		System.out.println(con.getResponseMessage());
		System.out.println(con.getContent());*/
		
		//String httpsURL = "https://10.59.112.33/perf/";
		/*String httpsURL = "https://workbench.mellon.com/perf";
		
	    URL myurl = new URL(httpsURL);
	    SSLContext ssl = SSLContext.getInstance("TLSv1");
	    ssl.init(null, new TrustManager[]{new SimpleX509TrustManager()}, null);
	    SSLSocketFactory factory = ssl.getSocketFactory();


	    HttpsURLConnection con = (HttpsURLConnection)myurl.openConnection();
	    con.setSSLSocketFactory(factory);
	    System.out.println(con.getResponseCode());
	    System.out.println(con.getResponseMessage());
	    InputStream ins = con.getInputStream();
	    InputStreamReader isr = new InputStreamReader(ins);
	    BufferedReader in = new BufferedReader(isr);

	    String inputLine;

	    while ((inputLine = in.readLine()) != null)
	    {
	      System.out.println(inputLine);
	    }

	    in.close();*/
				
		new RestController().testIt();
	}

	private void testIt() {

		//String https_url = "https://workbench.mellon.com/perf";
		//String https_url = "https://160.254.112.21/perf";
		
		//String https_url = "https://ewb-ws2-sn1n.bnymellon.net/perf";
		//String https_url = "https://10.59.112.34/perf";
		
		//String https_url = "https://ewb-ws1-sn1k.bnymellon.net/perf";
		//String https_url = "https://10.59.112.33/perf";
		
		//String https_url = "https://websealqa.mykmart.com/HierarchyService/getAllDivision";
		String https_url = "https://157.241.11.49/HierarchyService/getAllDivision";
		
		URL url;
		try {
			url = new URL(https_url);
			HttpsURLConnection con = (HttpsURLConnection) url.openConnection();

			// dumpl all cert info
			print_https_cert(con);

			// dump all the content
			print_content(con);

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void print_https_cert(HttpsURLConnection con) {
		if (con != null) {
			try {
				System.out.println("Response Code : " + con.getResponseCode());
				System.out.println("Cipher Suite : " + con.getCipherSuite());
				System.out.println("\n");
				Certificate[] certs = con.getServerCertificates();
				for (Certificate cert : certs) {
					System.out.println("Cert Type : " + cert.getType());
					System.out.println("Cert Hash Code : " + cert.hashCode());
					System.out.println("Cert Public Key Algorithm : " + cert.getPublicKey().getAlgorithm());
					System.out.println("Cert Public Key Format : "	+ cert.getPublicKey().getFormat());
					System.out.println("\n");
				}
			} catch (SSLPeerUnverifiedException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void print_content(HttpsURLConnection con) {
		if (con != null) {
			try {
				System.out.println("****** Content of the URL ********");
				BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String input;
				while ((input = br.readLine()) != null) {
					System.out.println(input);
				}
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

	}

}
class SimpleX509TrustManager implements X509TrustManager {
    public void checkClientTrusted(
            X509Certificate[] cert, String s)
            throws CertificateException {
    }

    public void checkServerTrusted(
            X509Certificate[] cert, String s)
            throws CertificateException {
      }

    @Override
    public X509Certificate[] getAcceptedIssuers() {
        // TODO Auto-generated method stub
        return null;
    }

}
