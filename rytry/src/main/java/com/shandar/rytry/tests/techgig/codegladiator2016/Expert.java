package com.shandar.rytry.tests.techgig.codegladiator2016;

/**
 * A rectangular plot comprising n*m cells on which n*m blocks (cement block in shape of a cube) were kept, 
 * one block per each cell. The base of each block covers one cell completely and its surface is equals to one 
 * square meter.  * Cubes on adjacent cell are so close that there are no gaps between them. Due to a heavy rain on 
 * the construction, water is accumulated in the gaps created between a group of blocks due to the difference in 
 * their heights. 
 * 
 * Write a program to calculate the volume of water accumulated between the blocks on the construction.
 * 
 * The below image depicts the water collected in the gaps of the building blocks due to their respective heights.
 * <img src="http://www.techgig.com/files/nicUploads/416xNx562699608504772.jpg.pagespeed.ic.fE29-picLN.webp" 
 * width="416" pagespeed_url_hash="3072044018" onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
 * 
 * Input Specifications:
 * 	Your function must read three arguments i.e
 * 		plot_length, plot_breadth and ;	block_height 
 * 		where plot_length(r) provides the length of each rectangular plot in metres. 
 * 		plot_breadth(c) provides the breadth of each rectangular plot in metres. 
 * 		block_height(Integer array) provides the heights of r*c blocks row-wise. 
 * 	Constraints 
 * 		1 < plot_length, plot_breadth, block_height < 10 
 * 	Output Specifications: 
 * 		Output will be an integer that will depict the volume of water (in cubic metres) accumulated in the puddles
 *  	due to the difference in heights of blocks.
 *  Examples: 
 *  Example 1: 
 *  	input1 = 3 
 *  	input2 = 6 
 *  	input2 = {3,3,4,4,4,2,3,1,3,2,1,4,7,3,1,6,4,1} 
 *  Output: 
 *  	5
 *  Example 2:
 *  	input1 = 6
 *  	input2 = 3
 *  	input3 = {3,3,7,3,1,3,4,3,1,4,2,6,4,1,4,2,4,1}
 *  	Output: 5</p><p><b><br></b></p><p><b><br></b></p><p><b>Instructions:</b></p><p>1) <b>Do not write main function.&nbsp;</b></p><p>2) You can print and debug your code at any step of the code.</p><p>3) You need to return the required output from the given function.&nbsp;</p><p>4) Do not change the function and parameter names given in editor code.</p><p>5) Return type must be the same as mentioned in the problem statement.&nbsp;</p><p>6) When you submit your code, test cases of different complexity level are executed in the background and marks are given based on number of test cases passed.&nbsp;</p><p>7) If you do not plan to complete the code in one sitting, then please save your work on a local machine. The code is saved only when it has been submitted using Submit button.
 * @author ssikdar
 *
 */

public class Expert {

}
