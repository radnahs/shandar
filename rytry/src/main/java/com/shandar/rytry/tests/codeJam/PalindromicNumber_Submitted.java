/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.utils.codeJam </p>
 * <p>File Name: PalindromicNumber_Submitted.java</p>
 * <p>Create Date: Apr 14, 2014 </p>
 * <p>Create Time: 1:34:23 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.tests.codeJam;

/**
 * @author Shantanu Sikdar 
 *
 */
import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * 
 * Given A and B, count the numbers N such that A ≤ N ≤ B and N is a palindrome.
 * Examples:
 * Palindromes: 121, 11 , 11411
 * Not Palindromes: 122, 10
 * 
 * Input:
 * First line contains T, the number of testcases. Each testcase consists of two integers A and B in one line.
 * Output:
 * For each testcase, print the required answer in one line.
 * 
 * Constraints:
 * 1 ≤ T ≤ 10
 * 0 ≤ A ≤ B ≤ 105
 * @author Shantanu Sikdar 
 *
 */
class PalindromicNumber_Submitted{
    public static void main(String args[] ) throws Exception {
      BufferedReader br = null;
      	try {
			int i=0;			
			br = new BufferedReader(new InputStreamReader(System.in));
			String line=null;
			while((line=br.readLine())!=null){				
				if(i==0){					
				}else{						
					String[] starr = line.split(" ");
					System.out.println(new PalindromicNumber_Submitted().palindromicBetween(new Integer(starr[0]),new Integer(starr[1])));
				}
				i++;
			}
			br.close();
		} catch (Exception io) {
			io.printStackTrace();
		}		
    }
    
    private Integer palindromicBetween(int low, int high){		
		int count=0;
		for(int i=low;i<high; i++){
			if(isPalindrome(i+"")){				
				count++;
			}
		}
		return count;
	}
	
	public boolean isPalindrome(String str){
		int start=0,length =str.length(),end=length-1;		
		while(start<end){
			if(str.charAt(start)!=str.charAt(end)){
				return false;
			}
			start++;
			end--;
		}		
		return true;
	}
}
