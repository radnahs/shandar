/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.utils.codeJam </p>
 * <p>File Name: LuckyString.java</p>
 * <p>Create Date: Jan 16, 2014 </p>
 * <p>Create Time: 12:00:32 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.tests.codeJam;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * http://www.hackerearth.com/problems/
 * Practice Problem 83. Lucky String
 * 
 * 
 * Lucky numbers are those numbers which contain only "4" and/or "5". For example 4, 5, 44, 54,55,444 are lucky numbers 
 * while 457, 987 ,154 are not.
 * Lucky number sequence is one in which all lucky numbers exist in increasing order for example 
 * 4,5,44,45,54,55,444,445,454,455,544,545,554,555...
 * Now we concatenate all the lucky numbers (in ascending order) to make a lucky string "4544455455444445454455..."
 * Given n, your task is to find the nth digit of the lucky string. If the digit is 4 then you have to print 
 * "Hacker" else you have to print "Earth".
 * 
 * 
 * @author Shantanu Sikdar
 * Solution Using: jre1.7 
 * there might be annotation errors.   
 *
 */
public class LuckyString {
	
	public static void main(String[] args) {
		long l1=Calendar.getInstance().getTimeInMillis();
		System.out.println("now = " + Calendar.getInstance().getTime());
		String lckStr = null;
		List<String> lst = null;
		InputStream inReader = null;
		BufferedReader inBuffReader = null;
		try {
			int i=0;
			inReader = new FileInputStream("c:\\Data\\myFile.txt");			
			inBuffReader = new BufferedReader(new InputStreamReader(inReader));
			String line=null;
			lst = new ArrayList<String>();
			
			while((line=inBuffReader.readLine())!=null){
				System.out.println(line);
				if(i==0){
					lckStr = findLuckyNumber();
				}else{
					int k = new Integer(line);
					if(k>0)
						lst.add(findNthNumber(lckStr,k));
				}
				i++;
			}
			
		} catch (IOException io) {
			io.printStackTrace();
		}finally{
			
		}
		System.out.println(lst);
		long l2=Calendar.getInstance().getTimeInMillis()-l1;
		System.out.println("now = " + Calendar.getInstance().getTime());
		System.out.println(l2);
	}
	
	private static String findLuckyNumber(){
		StringBuilder luckyString = new StringBuilder();
		luckyString.append("45");
		int digitIncrFactor=1;
		int luck1=4, luck2=5;
		int lowerLimit=0, upperLimit=0;
		List<Integer> lst = new ArrayList<>();		
		for(int digit=0;digit<15;digit++){
			digitIncrFactor*=10;			
			upperLimit=(luck2+1)*digitIncrFactor-lowerLimit;
			lowerLimit=luck1*digitIncrFactor+lowerLimit;
			
			for(int i=lowerLimit;i<upperLimit;i++){
				boolean fal=false;
				for (char c : (i+"").toCharArray()) {
					if(c=='4' || c=='5'){
						fal=true;
					}else{
						fal=false;
						break;
					}					
				}
				if(fal){
					lst.add(i);
					luckyString.append(i);
				}		
			}			
		}
		//System.out.println(lst);
		System.out.println(luckyString.toString());
		return luckyString.toString();
	}
	
	private static String findNthNumber(String lckStr, int n){
		StringBuilder luckyString = new StringBuilder();
		char[] charArr =  lckStr.toCharArray();
		n=n-1;
		if(charArr[n]=='4'){
			luckyString.append("Hacker = 4");
		}else if(charArr[n]=='5'){
			luckyString.append("Earth = 5");
		}else{
			luckyString.append("test case is false");
		}
		return luckyString.toString();
	}

}
