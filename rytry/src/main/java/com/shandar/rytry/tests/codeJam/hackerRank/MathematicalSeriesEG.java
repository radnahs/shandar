/**
* Project: rytry
* Package Name:package com.shandar.rytry.tests.codeJam.hackerRank;
* File Name: MathematicalSeriesEG.java
* Create Date: Sep 13, 2016
* Create Time: 6:46:08 PM
* Copyright: Copyright (c) 2016
* @author: Shantanu Sikdar, ssikdar
* @version 1.0
*/
package com.shandar.rytry.tests.codeJam.hackerRank;

/**
 * 
 * We use the integers , , and  to create the following series:

You are given  queries in the form of , , and . For each query, print the series corresponding to the given , , and  values as a single line of  space-separated integers.

Input Format

The first line contains an integer, , denoting the number of queries. 
Each line  of the  subsequent lines contains three space-separated integers describing the respective , , and  values for that query.

Constraints

Output Format

For each query, print the corresponding series on a new line. Each series must be printed in order as a single line of  space-separated integers.

Sample Input

2
0 2 10
5 3 5
Sample Output

2 6 14 30 62 126 254 510 1022 2046
8 14 26 50 98
Explanation

We have two queries:

We use , , and  to produce some series :

... and so on.

Once we hit , we print the first ten terms as a single line of space-separated integers.

We use , , and  to produce some series :


 * 
 * @author ssikdar
 *
 */
public class MathematicalSeriesEG {

	public static void main(String[] args) {
		MathematicalSeriesEG mse = new MathematicalSeriesEG();
		//mse.series(5, 3, 5);
		mse.series(0, 2, 10);
	}
	
	private void series(int a, int b, int n){
		long prevVal=a;
		for (int i = 0; i < n; i++) {
			prevVal+=Math.pow(2, i)*b;
			System.out.print(prevVal+" ");
		}
		System.out.println();
	}
	

}
