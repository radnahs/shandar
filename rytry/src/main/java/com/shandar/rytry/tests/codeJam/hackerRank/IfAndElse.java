
/**
 * Task 
 * Given an integer, , perform the following conditional actions:
 * 
 * If n is odd, print Weird
 * If n is even and in the inclusive range of  to 2, 5 print Not Weird
 * If n is even and in the inclusive range of  to 6, 20 print Weird
 * If n is even and greater than 20, print Not Weird
 * Complete the stub code provided in your editor to print whether or not n is weird.
 */
		

package com.shandar.rytry.tests.codeJam.hackerRank;

import java.util.Scanner;

public class IfAndElse {

	public static void main(String[] args) {
		/*Scanner scan = new Scanner(System.in);
		String st = scan.hasNextLine()?scan.nextLine():"";*/
		int n =50;
		 String ans="";
         if(n%2==1){
        	 ans = "Weird";
         }else{
        	 if(n<5 && n>1){
        		 ans = "Not Weird";
        	 }else if(n>5 && n<=20){
        		 ans = "Weird";
        	 }else{
        		 ans = "Not Weird";
        	 }
         }
         System.out.println(ans);
	}

}
