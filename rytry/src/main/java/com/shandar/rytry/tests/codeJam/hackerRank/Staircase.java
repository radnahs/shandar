/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.tests.hackerrank </p>
 * <p>File Name: Staircase.java</p>
 * <p>Create Date: Jan 25, 2016 </p>
 * <p>Create Time: 7:45:46 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.tests.codeJam.hackerRank;

/**
 * Your teacher has given you the task of drawing a staircase structure. Being an expert programmer, you decided to make a program to draw it for you instead. Given the required height, can you print a staircase as shown in the example?

Input 
You are given an integer N depicting the height of the staircase.

Output 
Print a staircase of height N that consists of # symbols and spaces. For example for N=6, here's a staircase of that height:
 */

/**
 * @author Shantanu Sikdar 
 *
 */
public class Staircase {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int n=7;
		int revDi=n-1;
		for(int j = n-1; j > -1; j--){
			for  (int i=0;i<n;i++){
				if(revDi<=i)
					System.out.print("#");
				else
					System.out.print(" ");
			}
			revDi--;
			System.out.println();
		}
	}

}
