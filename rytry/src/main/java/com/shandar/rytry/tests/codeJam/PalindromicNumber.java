/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.utils.codeJam </p>
 * <p>File Name: PalindromicNumber.java</p>
 * <p>Create Date: Apr 14, 2014 </p>
 * <p>Create Time: 12:25:19 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.tests.codeJam;

import java.util.ArrayList;
import java.util.List;


/**
 * @author Shantanu Sikdar 
 *
 */
public class PalindromicNumber {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String sss = "10 9999";
		String[] starr = sss.split(" ");
		
		System.out.println(new PalindromicNumber().palindromicBetween(new Integer(starr[0]),new Integer(starr[1])).size());
	}
	
	private List<Integer> palindromicBetween(Integer low, Integer high){
		List<Integer> lstInt = new ArrayList<Integer>();
		for(int i=low;i<high; i++){
			if(isPalindrome(i+"")){
				lstInt.add(i);
			}
		}
		return lstInt;
	}
	
	public boolean isPalindrome(String str){
		int start=0,length =str.length(),end=length-1;		
		while(start<end){
			if(str.charAt(start)!=str.charAt(end)){
				return false;
			}
			start++;
			end--;
		}		
		return true;
	}

}
