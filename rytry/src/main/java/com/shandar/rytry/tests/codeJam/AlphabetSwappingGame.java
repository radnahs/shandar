/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.utils.codeJam </p>
 * <p>File Name: AlphabetSwappingGame.java</p>
 * <p>Create Date: Apr 11, 2014 </p>
 * <p>Create Time: 3:39:17 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.tests.codeJam;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * Sherlock and Watson are playing swapping game. Watson gives to Sherlock a string S on which he has performed K swaps. 
 * You need to help Sherlock in finding the original string. One swap on a string is performed in this way:
 * 
 * Assuming 1 indexing, the i'th letter from the end is inserted between i'th and (i+1)'th letter from the starting.
 * For example, we have "contest". After one swap, it would change to "ctosnet". Check this image:
 * enter image description here
 * 
 * Input:
 * First line contains K, the number of swaps performed by Watson. Next line contains S, the string Watson gives to Sherlock.
 * 
 * Output:
 * You have to print in one line the original string with which Watson had started.
 * Constraints:
 * 1 = K = 109
 * 3 = Length(S) = 1000
 * All characters in S will be small English alphabets.
 * 
 * Sample Input (Plaintext Link)
 * 3
 * hrrkhceaate
 * 
 * Sample Output (Plaintext Link)
 * hackerearth
 * 
 * 
 * @author Shantanu Sikdar 
 *
 */

public class AlphabetSwappingGame {
	
	public static void main(String[] args) {
		
		/*System.out.println(new AlphabetSwappingGame().alphabetSwap("shantanu"));
		System.out.println(new AlphabetSwappingGame().alphabetSwapChar("shantanu"));*/
		System.out.println(new AlphabetSwappingGame().alphabetUnSwapCharB("suhnaant"));
		System.out.println(new AlphabetSwappingGame().alphabetUnSwapCharB("ctosnet"));
		
		
/*		new AlphabetSwappingGame().alphabetSwap("shantanu");
		new AlphabetSwappingGame().alphabetUnSwap("suhnaant");
		
		new AlphabetSwappingGame().alphabetSwap("contest");
		new AlphabetSwappingGame().alphabetUnSwap("ctosnet");
		
		new AlphabetSwappingGame().alphabetSwap("mahesh");
		new AlphabetSwappingGame().alphabetUnSwap("mhashe");
		
		new AlphabetSwappingGame().alphabetSwap("chandrakant");
		new AlphabetSwappingGame().alphabetUnSwap("cthnaankdar");
		
		new AlphabetSwappingGame().alphabetSwap("gulam");
		new AlphabetSwappingGame().alphabetUnSwap("gmual");
*/		
		/*new AlphabetSwappingGame().alphabetSwap("mahesh");
		new AlphabetSwappingGame().alphabetSwap("mhashe");
		new AlphabetSwappingGame().alphabetSwap("mehhas");
		new AlphabetSwappingGame().alphabetSwap("mseahh");*/
		//new AlphabetSwappingGame().alphabetSwap("mhshea");
		System.out.println("-------------------------------");
		
		/*new AlphabetSwappingGame().alphabetUnSwap("mseahh");
		new AlphabetSwappingGame().alphabetUnSwap("mehhas");
		new AlphabetSwappingGame().alphabetUnSwap("mhashe");*/
		
		/*String st1 = "hackerearth";
		for(int i=0;i<3;i++){			
			st1 = new AlphabetSwappingGame().alphabetSwap(st1);
		}
		System.out.println(st1 );
		
		String st2 = "hackerearth";
		for(int i=0;i<3;i++){			
			st2 = new AlphabetSwappingGame().alphabetSwapChar(st2);
		}
		System.out.println(st2 );*/
		
		/*String st3="hrrkhceaate";
		for(int i=0;i<3;i++){
			st3 = new AlphabetSwappingGame().alphabetUnSwap(st3);
		}
		System.out.println(st3);*/
	}
	
	private String alphabetSwap(String str){
		int size = str.length();
		StringBuilder swapped=new StringBuilder();
		List<String> lst = new ArrayList<>();
		char[] chr =str.toCharArray();		
		for(int i=0,j=size-1;i<(size%2==0?size/2:(size/2)+1);i++, j--){
			lst.add(""+chr[i]);
			if(j>i){
				lst.add(""+chr[j]);
			}			
		}
		for (String string : lst) {
			swapped.append(string);
		}
		return swapped.toString();
	}
	
	private String alphabetSwapChar(String str){
		int size = str.length();		
		StringBuilder swapped=new StringBuilder();
		char[] chr =str.toCharArray();
		
		for(int i=1;i<size-2;i++){				
			char temp=chr[i];
			chr[i]=chr[size-1];
			chr[size-1]=temp;			
		}
		
		for (char ch : chr) {
			System.out.println(ch);
			swapped.append(ch);
		}
		return swapped.toString();
	}
	
	
	
	private String alphabetUnSwap(String str){
		int size = str.length();
		StringBuilder unSwapped=new StringBuilder();
		char[] chr =str.toCharArray();
		List<String> lst = new ArrayList<String>(size);
		for (int k=0;k<size;k++) {
			lst.add(""+k);
		}		
		int j=size-1,p=0;
		for(int i=0;i<size;i++){				
			if(i%2==0){				
				lst.add(p,""+chr[i]);				
				lst.remove(p+1);
				p++;
			}else{				
				lst.add(j,""+chr[i]);				
				lst.remove(j+1);
				j--;
			}			
		}		
		for (String string : lst) {
			unSwapped.append(string);
		}		
		return unSwapped.toString();
	}
	
	
	private String alphabetUnSwapCharB(String str){
		int size = str.length();
		char[] chr =str.toCharArray();
		char[] nchr = new char[size];			
		int p=size-1,k=0;
		for(int i=0,j=1;i<(size%2==0?size-1:size-2);i=+2,j=+2){
			nchr[k]=chr[i];
			if(i<j)
				nchr[p]=chr[j];
			p--;
			k++;					
		}
		return String.valueOf(nchr);
	}

	
	
	private String alphabetUnSwapChar(String str){
		int size = str.length();
		char[] chr =str.toCharArray();
		char[] nchr = new char[size];			
		int j=size-1,p=0;
		for(int i=0;i<size;i++){				
			if(i%2==0){				
				nchr[p]=chr[i];
				p++;
			}else{				
				nchr[j]=chr[i];
				j--;
			}			
		}
		return String.valueOf(nchr);
	}

}
