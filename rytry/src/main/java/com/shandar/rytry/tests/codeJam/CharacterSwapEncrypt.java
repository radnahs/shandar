/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.utils.codeJam </p>
 * <p>File Name: CharacterSwapEncrypt.java</p>
 * <p>Create Date: Apr 30, 2014 </p>
 * <p>Create Time: 7:29:26 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.tests.codeJam;

import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author Shantanu Sikdar 
 *
 */
public class CharacterSwapEncrypt {
			
	public static void main(String[] args) {
		CharacterSwapEncrypt clObj = new CharacterSwapEncrypt();
		long stl = System.nanoTime();
		String str = "april";
		/*System.out.println(clObj.encryptingWord(str));*/
		//System.out.println(clObj.checkDecryptWord(str));
		
		String strR = "pairl";
		//System.out.println(clObj.decryptingWord2DMatrix(strR));
		System.out.println(clObj.decryptingWord(strR));
		
		//System.out.println(System.nanoTime()-stl);
		/*String strR2 = "lriap";
		System.out.println(clObj.encryptWord(strR2));*/
		
		/*String strR2 = "pose";
		System.out.println(clObj.encryptingWord(strR2));
		String strR3 = "oeps";
		System.out.println(clObj.decryptingWord2DMatrix(strR3));*/
		
		/*String strR4 = "tool";
		System.out.println(clObj.encryptingWord(strR4));*/
		String strR5 = "ltoo";
		//System.out.println(clObj.decryptingWord2DMatrix(strR5));
		System.out.println(clObj.decryptingWord(strR5));
		
		/*String strR6 = "were";
		System.out.println(clObj.encryptingWord(strR6));*/
		String strR7 = "eerw";
		//System.out.println(clObj.decryptingWord2DMatrix(strR7));
		//System.out.println(clObj.decryptingWord(strR7));
		
		/*String strR8 = "representation";
		System.out.println(clObj.encryptingWord(strR8));*/
		String strR9 = "anepneieotrrst";
		//System.out.println(clObj.decryptingWord2DMatrix(strR9));
		System.out.println(clObj.decryptingWord(strR9));
		
		
		/*String strR10 = "bottle";
		System.out.println(clObj.encryptingWord(strR10));*/
		String strR11 = "otbtle";
		//System.out.println(clObj.decryptingWord2DMatrix(strR11));
		System.out.println(clObj.decryptingWord(strR11));
		
		//System.out.println(System.nanoTime() - stl);
	}
	
	private boolean checkDecryptWord(String str){
		char[] chr = str.toCharArray();
		System.out.println(String.valueOf(chr));
		Arrays.sort(chr);
		System.out.println(String.valueOf(chr));
		return false;
	}
	
	
	private String decryptingWord(String str){
		int size = str.length();
		StringBuilder sb = new StringBuilder();
		char[] chrEnd = sb.append(str).reverse().toString().toCharArray();		
		char[] chrStart = str.toCharArray();
		Arrays.sort(chrStart);
		char[] nchrn = new char[size];
		System.out.print("chrStart == ");
		displayArray(chrStart);
		System.out.println("");
		System.out.print("chrEnd == ");
		displayArray(chrEnd);
		System.out.println("");
		/*sb = new StringBuilder();
		String[] strArr  = new String[size];
		for (int i = 0; i < size; i++) {
			sb.append(String.valueOf(chrStart[i])+String.valueOf(chrEnd[i]));
			strArr[i]=(String.valueOf(chrStart[i])+String.valueOf(chrEnd[i]));
		}
		System.out.println("builder === "+sb.toString());
		System.out.println("strArr === "+String.valueOf(strArr));*/
		
		/*char c = '0';
		nchrn[0]=chrStart[0];
		int t=0;
		for (int i = 0; i < size; i++) {
			c = nchrn[t];
			for (int j = 1; j < size; j++) {
				if(c==chrEnd[j]){
					c=chrStart[j];
					chrEnd[j]='X';
					break;
				}			
			}
			nchrn[++t]=c;
			//t++;
		}*/
		System.out.println("");
		System.out.print("nchrn == ");
		displayArray(nchrn);
		return "";
	}
	private void displayArray(char[] chr){
		for (char c : chr) {
			System.out.print(c);
		}
	}
	
	private String decryptingWord2DMatrix(String str){		
		StringBuilder sb = new StringBuilder();
		int size = str.length();
		
		char[] chr = sb.append(str).reverse().toString().toCharArray();// not needed. we can write the logic in for loop
		char[] nchr = str.toCharArray();
		char[][] nchrn = new char[size][size];
		Arrays.sort(nchr);
		
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				if(j==0)
					nchrn[i][j] = nchr[i];
				else if(j==size-1)
					nchrn[i][j] = chr[i];
				else
					nchrn[i][j] = '-';
			}			
		}		
		display2DMatrix(nchrn);		
		return "";
	}
	
	private void display2DMatrix(char[][] nchrn){
		int size = nchrn.length;
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				System.out.print(nchrn[i][j]);
			}
			System.out.println("");
		}
	}
	
	private String encryptingWord(String str){
		String encrypt = null;
		Set<String> set = new TreeSet<String>();		
		int size=str.length();		
		for(int k=0;k<size;k++){
			set.add(str);
			str = moveOneChar(str,size);
		}		
		System.out.println(set);
		StringBuilder sb = new StringBuilder();
		for (String string : set) {
			char[] chr = string.toCharArray();
			sb.append(chr[size-1]);
		}
		encrypt = sb.reverse().toString();
		return encrypt;
	}
	
	private String moveOneChar(String str,int size){	
		char[] chr =str.toCharArray();
		char[] nchr = new char[size];		
		for (int i=0;i<chr.length;i++) {			
			if(i==0)
				nchr[i]=chr[size-1];
			else
				nchr[i]=chr[i-1];
		}
		return String.valueOf(nchr);
	}

}
