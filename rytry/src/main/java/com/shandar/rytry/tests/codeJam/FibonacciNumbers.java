package com.shandar.rytry.tests.codeJam;

/**
 *
 * 
 * @author Shantanu Sikdar 
 *
 */

public class FibonacciNumbers {
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		FibonacciNumbers  fbn = new FibonacciNumbers();
		
		System.out.println(fbn.fibonacciSeriesRecursion(9, 0, 1));
				
		/*System.out.println(fbn.fibonacciSeriesNthTerm(1));
		
		System.out.println();
		fbn.display(fbn.fibonacciSeries(11));// ans :: 1 2 3 5 8 13 21 34 55 89 144
		System.out.println();
		System.out.println(fbn.fibonacciSeriesNthTerm(11));
		System.out.println();
		fbn.display(fbn.fibonacciSeries(15));
		System.out.println();
		System.out.println(fbn.fibonacciSeriesNthTerm(15));
		System.out.println(fbn.fibonacciSeriesNthTerm(15)%1000000007);*/
		
		
		
		/*System.out.println(fbn.fibonacciNthTermFromSeries(fbn.fibonacciSeries(11),11));		
		System.out.println(fbn.fibonacciNthTermFromSeries(fbn.fibonacciSeries(3),3));
		System.out.println(fbn.fibonacciNthTermFromSeries(fbn.fibonacciSeries(12),12));
		System.out.println(fbn.fibonacciNthTermFromSeries(fbn.fibonacciSeries(13),13));*/
		
	}
	private void display(long[] arr){
		for (long l : arr) {
			System.out.print(l+" ");
		}
	}
	
	public long fibonacciNthTermFromSeries(long[] arr, int nTerm){
		return arr[nTerm-1];
	}
	
	public long[] fibonacciSeries(int nTerm){
		long[] arr = new long[nTerm];
		int i;
		arr[0]=1;
		arr[1]=2;
		for(i=2;i<nTerm;i++){			
			arr[i]=arr[i-1]+arr[i-2];
		}		
		return arr;
	}
	
	public long fibonacciSeriesNthTerm(long nTerm){
		long x=1,y=1,term=1;		
		while(1<nTerm && term<nTerm){
			long temp;
			temp = x+y;
			x=y;
			y=temp;			
			term++;
		}
		return y;
	}
	
	public int fibonacciSeriesRecursion(int nTerm,int prev,int latest){		
		if(nTerm==1){
			return prev+latest;
		}else{
			return (fibonacciSeriesRecursion(nTerm-1,latest,prev+latest));
		}
	}
	
}
