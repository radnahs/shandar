/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.tests.hackerrank </p>
 * <p>File Name: MatrixRotation.java</p>
 * <p>Create Date: Jan 27, 2016 </p>
 * <p>Create Time: 5:21:46 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.tests.codeJam.hackerRank;

import java.util.Scanner;
/**
 * @author Shantanu Sikdar
 * 
 */
public class MatrixRotation {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int m = scanner.nextInt(), n = scanner.nextInt(), r = scanner.nextInt();
		String[][] matrix = new String[m][n];
		int k = 0, l = 0;
		String[] strArr = new String[n];
		while (scanner.hasNext()) {
			if (l < n) {
				strArr[l] = scanner.next();
				l++;
			} else if (k < m) {
				matrix[k] = strArr;
				l = 0;
				k++;
				strArr = new String[n];
			}
		}
		matrix[k] = strArr;
		//display(matrix);
		rotateMatrixAntiClockwise(m, n, matrix);
		//display(matrixShift);
		scanner.close();
	}

	private static void rotateMatrixAntiClockwise(int m, int n, String mat[][]) {
		int row = 0, col = 0;
		String prev, curr;
		/*
		 * row - Staring row index m - ending row index col - starting column
		 * index n - ending column index i - iterator
		 */
		while (row < m && col < n) {
			if (row + 1 == m || col + 1 == n)
				break;
			// Store the last element of next row, this
			// element will replace last element of current
			// row
			prev = mat[row + 1][n-1];
			/* Move elements of first row from the remaining rows */
			for (int i = n-1; i > -1; i--) {
				curr = mat[row][i];
				mat[row][i] = prev;
				prev = curr;
			}
			row++;
			/* Move elements of last column from the remaining columns */
			for (int i = row; i < m; i++) {
				curr = mat[i][n - 1];
				mat[i][n - 1] = prev;
				prev = curr;
			}
			n--;

			/* Move elements of last row from the remaining rows */
			if (row < m) {
				for (int i = n - 1; i >= col; i--) {
					curr = mat[m - 1][i];
					mat[m - 1][i] = prev;
					prev = curr;
				}
			}
			m--;
			/* Move elements of first column from the remaining rows */
			if (col < n) {
				for (int i = m - 1; i >= row; i--) {
					curr = mat[i][col];
					mat[i][col] = prev;
					prev = curr;
				}
			}
			col++;
		}
		display(mat);
	}


	private static void rotateMatrixClockwise(int m, int n, String mat[][]) {
		int row = 0, col = 0;
		String prev, curr;
		/*
		 * row - Staring row index m - ending row index col - starting column
		 * index n - ending column index i - iterator
		 */
		while (row < m && col < n) {
			if (row + 1 == m || col + 1 == n)
				break;
			// Store the first element of next row, this
			// element will replace first element of current
			// row
			prev = mat[row + 1][col];
			/* Move elements of first row from the remaining rows */
			for (int i = col; i < n; i++) {
				curr = mat[row][i];
				mat[row][i] = prev;
				prev = curr;
			}
			row++;
			/* Move elements of last column from the remaining columns */
			for (int i = row; i < m; i++) {
				curr = mat[i][n - 1];
				mat[i][n - 1] = prev;
				prev = curr;
			}
			n--;

			/* Move elements of last row from the remaining rows */
			if (row < m) {
				for (int i = n - 1; i >= col; i--) {
					curr = mat[m - 1][i];
					mat[m - 1][i] = prev;
					prev = curr;
				}
			}
			m--;
			/* Move elements of first column from the remaining rows */
			if (col < n) {
				for (int i = m - 1; i >= row; i--) {
					curr = mat[i][col];
					mat[i][col] = prev;
					prev = curr;
				}
			}
			col++;
		}
		display(mat);
	}

	private static void display(String[][] matrix) {
		for (int i = 0; i < matrix.length; i++) {
			String[] strArr = matrix[i];
			for (int j = 0; j < strArr.length; j++) {
				System.out.print(strArr[j] + "  ");
			}
			System.out.println();
		}
	}

}
