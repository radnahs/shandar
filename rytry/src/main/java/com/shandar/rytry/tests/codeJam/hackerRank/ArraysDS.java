/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.tests.hackerrank </p>
 * <p>File Name: ArraysDS.java</p>
 * <p>Create Date: Feb 8, 2016 </p>
 * <p>Create Time: 5:38:31 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.tests.codeJam.hackerRank;

/**
 * @author Shantanu Sikdar 
 *
 */
public class ArraysDS {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//System.out.println();
		reverseArray(new int[]{1,2,3,4});
		reverseArray(new int[]{1,2,3,4,5});
	}
	
	private static void reverseArray(int[] intArr){
		display(intArr);
		int size = intArr.length;
		for (int i = 0, j=intArr.length-1; i < j; i++, j--) {
			int temp=intArr[i];
			intArr[i]=intArr[j];
			intArr[j]=temp;
		}		
		System.out.println();
		display(intArr);
	}
	
	private static void display(int[] intArr){
		for (int i = 0; i < intArr.length; i++) {
			System.out.print(intArr[i]+" ");
		}
	}
	
}
