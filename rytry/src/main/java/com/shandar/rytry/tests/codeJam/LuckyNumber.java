/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.utils.codeJam </p>
 * <p>File Name: LuckyNumber.java</p>
 * <p>Create Date: Jan 17, 2014 </p>
 * <p>Create Time: 2:44:57 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.tests.codeJam;

import java.util.HashMap;
import java.util.Map;

/**
 * Raju is interested in numbers especially larger integers.
 * He wants to find the sum of products of the digits and their number of occurrences. 
 * Raju considers this number as lucky. Help him find the lucky number. 
 * See sample explanation for further clarification.
 * 
 * Explanation 
 * For the first case, 1 occurs 1 time, 2 occurs 2 twice, 3 occurs once. Hence the answer is 
 * 1*1 + 2*2 + 1*3 = 8 
 * Similarly for the second test case, 
 * 1*1 + 2*0 = 1
 * 
 * @author Shantanu Sikdar 
 *
 */
public class LuckyNumber {
	
	public static void main(String[] args) {
		findLuckyNumber(1223001);		
	}
	
	private static String findLuckyNumber(int n){
		Map<Integer, Integer> charMap = new HashMap<>();
		String str = n+""; System.out.println(str);
		int total = 0;
		for(int i=0;i<str.length();i++){			
			Integer k = new Integer (String.valueOf(str.charAt(i)));
			if(charMap.containsKey(k)){
				charMap.put(k, charMap.get(k) + k);
			}else {
				charMap.put(k, k);
			}
			total = total+k;
		}
		System.out.println(total);
		return null;
	}
	
	/*dil khudgarz hai
	phisla hai yeh phir haat se
	kal uska raha
	aab hai tera is raat se

	dil khudgarz hai
	phisla hai yeh phir haat se
	kal uska raha
	aab hai tera is raat se

	o meri jaan uuhuuu uuuhuu
	o meri jaan uuhuuu uuuhuu
	o meri jaan

	tu aa gaya yuh nazar main
	jaise subah dopaher main
	madhoshi yun hi nahin dil pe chhayi
	neyat ne li angdayi
	chhuwa tune kuch is tarah
	badli fiza, badla sama

	o meri jaan uuhuuu uuuhuu
	o meri jaan uuhuuu uuuhuu
	o meri jaan

	nadan samjhe na hai yeh dil mera
	jaanu naa jaanu naa isko kya hua
	teri bahon ki phir se dhunde yeh panah
	tu hai kahan
	tu hai kahan

	o meri jaan uuhuuu uuuhuu
	o meri jaan uuhuuu uuuhuu
	o meri jaan*/
}
