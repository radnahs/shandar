/**
* Project: rytry
* Package Name:package com.shandar.rytry.tests.prblm1;
* File Name: To100.java
* Create Date: Feb 15, 2017
* Create Time: 6:58:43 PM
* Copyright: Copyright (c) 2016
* @author: Shantanu Sikdar, ssikdar
* @version 1.0
*/
package com.shandar.rytry.tests.prblm1;

import java.util.BitSet;


public class To100 {

	public static void main(String[] args) {
		String strngSet =  new BitSet(){{
			set(1,100+1);
		}}.toString();
		System.out.println(strngSet);
		System.out.append(strngSet,1,strngSet.length()-1);
	}

}
