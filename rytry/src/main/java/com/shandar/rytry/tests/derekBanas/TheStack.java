/**
* Project: rytry
* Package Name:package com.shandar.rytry.tests.derekBanas;
* File Name: TheStack.java
* Create Date: Jan 20, 2017
* Create Time: 8:18:51 PM
* Copyright: Copyright (c) 2016
* @author: Shantanu Sikdar, ssikdar
* @version 1.0
*/
package com.shandar.rytry.tests.derekBanas;

import java.util.Arrays;

public class TheStack {

	private String[] stackArray;
	private int stackSize;
	private int topOfStack=-1;
	
	public TheStack(int size){
		stackSize=size;
		stackArray = new String[stackSize];
		Arrays.fill(stackArray , "-1");
	}
	
	public void push(String input){
		if(topOfStack+1<stackSize){
			topOfStack++;
			stackArray[topOfStack]=input;
		}else{
			System.out.println("Sorry but the stack is full");
		}
		displayTheStack();
		System.out.println("Push "+ input +" Was added to the stack");
	}
	
	public String pop(){
		if(topOfStack>=0){
			displayTheStack();
			System.out.println("Pop "+stackArray[topOfStack]+" was removed from the stack\n");
			stackArray[topOfStack] = "-1";
			return stackArray[topOfStack--];
 		}else{
 			displayTheStack();
 			System.out.println("Sorry but the stack is empty");
 			return "-1";
 		}
	}
	
	public String peek(){
		displayTheStack();
		System.out.println("Peek "+ stackArray[topOfStack]+ "Is at the Top of the stack");
		return stackArray[topOfStack];
	}
	
	public void displayTheStack(){
		for(int n=0; n<61; n++){
			System.out.print("-"); 
		}
		System.out.println();
		
		for (int n = 0; n < stackSize; n++) {
			System.out.format("| %2s " + " ", n);
		}
		System.out.println("|");
		
		for(int n=0; n<61; n++){
			System.out.print("-");
		}
		System.out.println();
		
		for (int n = 0; n < stackSize; n++) {
			System.out.format("| %2s " + " ", stackArray[n]);
		}
		System.out.println("|");
		
		for(int n=0; n<61; n++){
			System.out.print("-");
		}
		System.out.println();
		
	}
	
	public static void main(String[] args) {
		TheStack theStack = new TheStack(10);
		theStack.push("10");
		theStack.push("15");
		
		theStack.peek();
	}

}
