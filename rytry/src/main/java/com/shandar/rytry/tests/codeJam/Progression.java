/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.utils.codeJam </p>
 * <p>File Name: Progression.java</p>
 * <p>Create Date: Apr 30, 2014 </p>
 * <p>Create Time: 12:48:00 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.tests.codeJam;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Shantanu Sikdar 
 *
 */
public class Progression {
	
	

	/**
	 * @param args
	 */
	static Map<Integer, Integer> map = new HashMap<>();
	static List<Integer> lst = new ArrayList<>();
	public static void main(String[] args) {
		int kk = 1000000000;
		System.out.println();
		
		System.out.println(Math.pow(8, 2));
		/*System.out.println("gp == "+new Progression().geometricSeries(1, 1, 10));
		System.out.println("gp == "+new Progression().geometricSeries(2, 2, 10));
		System.out.println("gp == "+new Progression().geometricSeries(5, 2, 3));
		System.out.println("gp == "+new Progression().geometricSeries(8, 2, 100));
		System.out.println("gp == "+new Progression().geometricSeries(1, 10, 10));
		System.out.println("gp == "+new Progression().geometricSeries(9, 3, 100));
		System.out.println("gp == "+new Progression().geometricSeries(17, 12, 11));*/
		
		
		/*System.out.println(new Progression().powerProgressionJava(5, 3));
		System.out.println(new Progression().powerProgressionJava(8, 2));
		System.out.println(new Progression().powerProgressionJava(11, 3));*/
		
		System.out.println("gpj == "+new Progression().powerProgressionJava(1, 1, 10));
		System.out.println("gp == "+new Progression().powerProgressionJava(2, 2, 10));
		System.out.println("gp == "+new Progression().powerProgressionJava(5, 2, 3));
		System.out.println("gp == "+new Progression().powerProgressionJava(8, 2, 100));
		System.out.println("gp == "+new Progression().powerProgressionJava(1, 10, 10));
		System.out.println("gp == "+new Progression().powerProgressionJava(9, 3, 100));
		System.out.println("gp == "+new Progression().powerProgressionJava(17, 12, 11));
		
		
		/*new Progression().powerProgression(5, 2, 0);
		System.out.println(map);
		System.out.println(new Progression().sumProgression(lst)%3);*/
		
		/*System.out.println(new Progression().powerProgression(5, 3));
		System.out.println(new Progression().powerProgression(8, 2));
		System.out.println(new Progression().powerProgression(11, 3));
*/	}
	
	private int powerProgression(int n, int p, int sum){		
		if(p==1){
			sum=n*1;
			lst.add(sum);
			map.put(p,sum);
			return sum;
		}else{
			sum = n*powerProgression(n,p-1, sum);
			lst.add(sum);
			map.put(p,sum);
			return sum;
		}
	}
	
	private int sumProgression(List<Integer> lst){
		int sum=0;
		for (Integer intgr : lst) {
			sum = sum +intgr;  
		}
		return 2*sum;		
	}
	
	private int powerProgression(int n, int p){
		int prod = 1,sum=0;		
		for(int i=0;i<p;i++){
			prod=n*prod;
			sum+=prod;
		}
		return sum;
	}
	
	
	private int powerProgressionJava(int n, int p){
		int sum=0;		
		for(int i=1;i<=p;i++){			
			sum+=Math.pow(n, i);
		}
		return sum;
	}
	
	private long powerProgressionJava(int X, int N, int M){
		long sum=0;		
		for(int i=1;i<=N;i++){
			sum+=(2*Math.pow(X, i))%M;
			//sum+=(Math.pow(X, i))%M;
		}
		return (sum)%M;
		//return 2*sum%M;
	}
	
	private int geometricSeries(int X, int N, int M){
		double sum=X*((1-Math.pow(X, N))/(1-X));
		return (int)(2*sum)%M;
	}
}

