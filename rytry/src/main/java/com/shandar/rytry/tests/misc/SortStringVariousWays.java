/**
* Project: rytry
* Package Name:package com.shandar.rytry.tests.misc;
* File Name: RepeatCharactersInString.java
* Create Date: Mar 21, 2018
* Create Time: 09:25:57 AM
* Copyright: Copyright (c) 2016
* @author: Shantanu Sikdar, ssikdar
* @version 1.0
*/

package com.shandar.rytry.tests.misc;

import java.util.Arrays;

public class SortStringVariousWays {

	public static void main(String[] args) {
		SortStringVariousWays obj = new SortStringVariousWays();
		System.out.println(obj.sortCharsJavaWay("abababsfessffee"));
		System.out.println(obj.sortCharsAlgoWay("abababsfessffee"));
	}
	
	private String sortCharsJavaWay(String strToSort){
		char[] charArr = strToSort.toCharArray();
		Arrays.sort(charArr);
		String sortedStr = new String(charArr);
		return sortedStr;
	}
	
	private String sortCharsAlgoWay(String strToSort){
		char[] charArr = strToSort.toCharArray();
		char[] newCharArr = new char[strToSort.length()];
		//Arrays.sort(charArr);
		for (int i = 0; i < charArr.length; i++) {
			System.out.println(charArr[i]);
			//newCharArr[i];
		}
		String sortedStr = new String(charArr);
		return sortedStr;
	}

}
