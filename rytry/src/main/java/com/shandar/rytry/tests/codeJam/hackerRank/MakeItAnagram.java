/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.tests.hackerrank </p>
 * <p>File Name: MakeItAnagram.java</p>
 * <p>Create Date: Feb 12, 2016 </p>
 * <p>Create Time: 7:03:38 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.tests.codeJam.hackerRank;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * @author Shantanu Sikdar 
 *
 */
public class MakeItAnagram {
	
	private Map<Character, Integer> alphabetCount(char[] charArr){
		Map<Character, Integer> charCountMap = new HashMap<Character, Integer>();
		for (int i = 0; i < charArr.length; i++) {
			if(charCountMap.containsKey(charArr[i])){
				charCountMap.put(charArr[i], charCountMap.get(charArr[i])+1);
			}else{
				charCountMap.put(charArr[i], 1);
			}
		}
		System.out.println(charCountMap);
		return charCountMap;
	}
	
	public int makeAnagram(Map<Character, Integer> charCountMap1, Map<Character, Integer> charCountMap2){
		int count =0 ;
		for (Entry<Character, Integer> entry : charCountMap2.entrySet()) {
			if(charCountMap1.containsKey(entry.getKey())){
				count+=Math.abs(entry.getValue()-charCountMap1.get(entry.getKey()));
				charCountMap1.remove(entry.getKey());
			}else{
				count+=entry.getValue();
			}
		}
		for (Entry<Character, Integer> entry : charCountMap1.entrySet()){
			count+=entry.getValue();
		}
		return count;
	}
	
	private boolean checkIf2StringAnagram(String str1, String str2){
		boolean retVal = false;
		char[] char1 = str1.toLowerCase().toCharArray();
		Arrays.sort(char1);
		String ss1 = new String(char1);
		char[] char2 = str2.toLowerCase().toCharArray();
		Arrays.sort(char2);
		String ss2 = new String(char2);
		//System.out.println(ss1+" ------ "+ss2);
		retVal=ss1.equalsIgnoreCase(ss2);
		return retVal;
	}
	
	public static void main(String[] args) {
		MakeItAnagram mia = new MakeItAnagram();
		/*Map<Character, Integer> charCountMap1 = mia.alphabetCount("cdeshsjejfjfjfkd".toCharArray());
		Map<Character, Integer> charCountMap2 = mia.alphabetCount("abcshsnkmfhfkgsj".toCharArray());
		System.out.println(mia.makeAnagram(charCountMap1 ,charCountMap2));*/
		Map<Character, Integer> charCountMap1 = mia.alphabetCount("cde".toCharArray());
		Map<Character, Integer> charCountMap2 = mia.alphabetCount("abc".toCharArray());
		System.out.println(mia.makeAnagram(charCountMap1 ,charCountMap2));
		
		/*System.out.println(mia.checkIf2StringAnagram("achevrolet", "lovethecar"));
		System.out.println(mia.checkIf2StringAnagram("anagramm", "marganaa"));
		System.out.println(mia.checkIf2StringAnagram("Hello", "hello"));*/
	}

}
