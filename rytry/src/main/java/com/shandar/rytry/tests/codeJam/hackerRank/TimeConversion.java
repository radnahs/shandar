/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.tests.hackerrank </p>
 * <p>File Name: TimeConversion.java</p>
 * <p>Create Date: Jan 25, 2016 </p>
 * <p>Create Time: 8:04:18 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.tests.codeJam.hackerRank;

/**
 * @author Shantanu Sikdar 
 *
 */
public class TimeConversion {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		amPmToHour("07:05:45AM");
		amPmToHour("07:05:45PM");
		
		amPmToHour("12:05:45PM");
		amPmToHour("01:05:45PM");
		
		amPmToHour("00:05:45AM");
		amPmToHour("12:05:45AM");
		amPmToHour("12:00:00AM");
	}
	private static void amPmToHour(String str){
		String strArr[]=str.split(":");
		String amPm = strArr[2].substring(2, 4);
		if(amPm.equalsIgnoreCase("AM")){
			if(strArr[0].equalsIgnoreCase("12")){
				System.out.println("00:"+strArr[1]+":"+strArr[2].replace("AM", ""));
			}else{
				System.out.println(str.replace("AM", ""));
			}
		}else{
			int pm = new Integer(strArr[0])==12?12:new Integer(strArr[0])+12;
			System.out.println(pm+":"+strArr[1]+":"+strArr[2].replace("PM", ""));
		}
	}

}
