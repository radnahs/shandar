/**
* Project: rytry
* Package Name:package com.shandar.rytry.tests.codeJam.hackerRank;
* File Name: JavaEOF.java
* Create Date: Jul 8, 2016
* Create Time: 4:07:47 PM
* Copyright: Copyright (c) 2016
* @author: Shantanu Sikdar, ssikdar
* @version 1.0
*/
package com.shandar.rytry.tests.codeJam.hackerRank;

import java.util.Scanner;

public class JavaEOF {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	private void javaEOF(){
		Scanner scanner = new Scanner(System.in);
		int k=1;
		while (scanner.hasNextLine()) {
			System.out.println(k+" "+scanner.nextLine());
			k++;
		}

	}

}
