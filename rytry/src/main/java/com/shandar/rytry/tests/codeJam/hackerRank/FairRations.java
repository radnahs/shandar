/**
* Project: rytry
* Package Name:package com.shandar.rytry.tests.codeJam.hackerRank;
* File Name: FairRations.java
* Create Date: Oct 18, 2016
* Create Time: 12:55:07 PM
* Copyright: Copyright (c) 2016
* @author: Shantanu Sikdar, ssikdar
* @version 1.0
*/
package com.shandar.rytry.tests.codeJam.hackerRank;

/**
 * 
 * You are the benevolent ruler of Rankhacker Castle, and today you're distributing bread to a 
 * straight line of subjects. Each ith subject (where 1<=i<=N ) already has Bi loaves of bread.
 * 
 * Times are hard and your castle's food stocks are dwindling, so you must distribute as 
 * few loaves as possible according to the following rules:
 * 
 * Every time you give a loaf of bread to some person , you must also give a loaf of bread 
 * to the person immediately in front of or behind them in the line (i.e., persons i+1 or i-1).
 * After all the bread is distributed, each person must have an even number of loaves.
 * Given the number of loaves already held by each citizen, find and print the minimum number 
 * of loaves you must distribute to satisfy the two rules above. If this is not possible, print NO.
 * 
 * Input Format
 * 
 * The first line contains an integer, N, denoting the number of subjects in the bread line.
 * The second line contains N space-separated integers describing the respective loaves of 
 * bread already possessed by each person in the line (i.e.,B1, B2,....BN ).
 * 
 * Constraints
 * 2<=N<=1000
 * 1<=Bi<=10, where 1<=i<=N
 * 
 * Output Format
 * 
 * Print a single integer denoting the minimum number of loaves you must distribute to adjacent 
 * people in the line so that every person has an even number of loaves; if it's not possible to do this, print NO.
 * 
 * Sample Input 0
 * 5
 * 2 3 4 5 6
 * Sample Output 0
 * 4
 * 
 * Sample Input 1
 * 2
 * 1 2
 * Sample Output 1
 * NO
 * 
 * Explanation
 * 
 * Sample Case 0:
 * The initial distribution is . You can satisfy the problem's requirements by performing the following actions:

Give  loaf of bread each to the second and third people so that the distribution becomes .
Give  loaf of bread each to the third and fourth people so that the distribution becomes .
Because each of the  subjects now has an even number of loaves, we can stop distributing bread. We then print the total number of loaves distributed, which is , on a new line.

Sample Case 1: 
The initial distribution is . As there are only  people in the line, any time you give one person a loaf you must always give the other person a loaf. Because the first person has an odd number of loaves and the second person has an even number of loaves, no amount of distributed loaves will ever result in both subjects having an even number of loaves. Thus, we print NO as our answer.
 * 
 * @author ssikdar
 *
 */

public class FairRations {

	public static void main(String[] args) {
		int[] arr = {2,3,4,5,6};
		int r = madeEvenDistribution(arr);
		System.out.println(r==(-1)?"No":r);
		int[] arr1 = {1,2};
		int r1 = madeEvenDistribution(arr1);
		System.out.println(r1==(-1)?"No":r1);
	}
	
	
	private static int madeEvenDistribution(int[] arr){
		int steps = 0;
		for (int i=0; i<arr.length; i++) {
			if (arr[i]%2 == 0){	 
				continue;
			}
			if (i == arr.length-1){
				break;
			}
			arr[i]++;
			arr[i+1]++;
			
			i--;
			steps++;
		}
		for (int i=0;i<arr.length;i++) {
			if (arr[i]%2==1){
				return -1;
			}
		}
		return steps*2;
	}
}
