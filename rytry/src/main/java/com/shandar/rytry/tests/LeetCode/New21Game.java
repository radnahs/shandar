/**
 * <p>Project: rytry </p>
 * <p>Package Name: com.shandar.rytry.tests.LeetCode </p>
 * <p>File Name: New21Game.java </p>
 * <p>Create Date: 27-May-2023 </p>
 * <p>Create Time: 8:59:03 AM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.rytry.tests.LeetCode;

/**
 * @author : Shantanu Sikdar
 * @Description : New21Game
 * @ref : https://leetcode.com/problems/new-21-game/
 */
public class New21Game {

	public static void main(String[] args) {
		New21Game new21Game = new New21Game();
		System.out.println(new21Game.new21Game(1, 1, 10));// Input: n = 10, k = 1, maxPts = 10, Output: 1.00000,
															// Explanation: Alice gets a single card, then stops.
		System.out.println(new21Game.new21Game(6, 1, 10)); // Input: n = 6, k = 1, maxPts = 10; Output: 0.60000;
															// Explanation: Alice gets a single card, then stops. In 6
															// out of 10 possibilities, she is at or below 6 points.

		System.out.println(new21Game.new21Game(21, 17, 10)); // Input: n = 21, k = 17, maxPts = 10 Output: 0.73278
	}
	
	public double new21Game(int n, int k, int maxPts) {
		if (k == 0 || n >= k + maxPts) {
			return 1;
		}
		double tempSum = 1, probability = 0;

		double[] dp = new double[n + 1];
		dp[0] = 1;
		
		for (int i = 1; i <= n; i++) {
			dp[i] = tempSum / maxPts;

			if (i < k) {
				tempSum += dp[i];
			} else {
				probability += dp[i];
			}
			
			if (i >= maxPts) {
				tempSum -= dp[i - maxPts];
			}
		}
		return probability;
	}
}
