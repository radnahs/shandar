/**
* Project: rytry
* Package Name:package com.shandar.rytry.tests.derekBanas;
* File Name: ArrayStructures.java
* Create Date: Jan 20, 2017
* Create Time: 1:42:35 PM
* Copyright: Copyright (c) 2016
* @author: Shantanu Sikdar, ssikdar
* @version 1.0
*/
package com.shandar.rytry.tests.derekBanas;
/*https://www.youtube.com/watch?v=f5OD9CKrZEw&list=PLvVMzZGFLf-zb0aLLosOrXb3Utw_b7XUS  
 */

public class ArrayStructures {

	private int[] theArray = new int[50];
	private int arraySize=10;
	
	public void generateRandomArray(){
		for (int i = 0; i < arraySize; i++) {
			theArray[i] = (int)(Math.random()*10)+10;
		}
	}
	
	public void printArray(){
		System.out.println("-------");
		for (int i = 0; i < arraySize; i++) {
			System.out.print("| " + i + " |");
			System.out.println(theArray[i]+" |");
			System.out.println("-------");
		}
	}
	
	public int getValueAtIndex(int index){
		if(index<arraySize) 
			return theArray[index];
		return 0;
	}
	
	public boolean doesArrayContainThisValue(int searchValue){
		boolean valueInAray=false;
		for (int i = 0; i < arraySize; i++) {
			if(theArray[i]==searchValue){
				valueInAray=true;
				//break;// though not in the tutorial 
			}
		}
		return valueInAray;
	}
	
	public void deleteIndex(int index){
		if(index<arraySize){
			for (int i = index; i < arraySize; i++) {
				theArray[i]=theArray[i+1];
			}
			arraySize--;
		}
	}
	
	public void insertValue(int value){
		if(arraySize<50){
			theArray[arraySize]=value;
			arraySize++;
		}
	}

	// by shantanu
	public void insertValueInBetween(int value, int index){
		for(int i=index; i<=arraySize+1;i++){
			int temp = theArray[i];
			theArray[i]=value;			
			value=theArray[i+1];
			theArray[i+1]=temp;
			i++;
		}
		arraySize++;
	}
	
	public String linearSearchForValue(int value){
		boolean valueInArray=false;
		String indexsWithValue="";
		System.out.println("The value was found in the following: ");
		for (int i = 0; i < arraySize; i++) {
			if(theArray[i]==value){
				valueInArray=true;
				System.out.print(i+ " ");
				indexsWithValue+= i+" ";
			}
		}
		
		if(!valueInArray){
			indexsWithValue="None ";
			System.out.print(indexsWithValue);
		}
		System.out.println();
		
		return indexsWithValue;
	}

	
	public static void main(String[] args) {
		ArrayStructures newArray = new ArrayStructures();
		newArray.generateRandomArray();
		newArray.printArray();
		
		System.out.println(newArray.getValueAtIndex(3));//--1
		System.out.println(newArray.doesArrayContainThisValue(11));//--2
		
		newArray.deleteIndex(4);//--3
		newArray.printArray();//--3
		
		newArray.insertValue(55);//--4
		newArray.printArray();//--4
		
/*		newArray.insertValueInBetween(38, 2);//--5
		newArray.printArray();//--5
*/		
		newArray.linearSearchForValue(17);
		
	}

}
