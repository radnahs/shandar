/**
 * <p>Project: rytry </p>
 * <p>Package Name: com.shandar.rytry.tests.LeetCode </p>
 * <p>File Name: MostFrequentOccuringLeastNumber.java </p>
 * <p>Create Date: 13-Jun-2023 </p>
 * <p>Create Time: 8:59:03 AM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.rytry.tests.LeetCode;

/**
 * @author : Shantanu Sikdar
 * @Description : MostFrequentOccuringLeastNumber
 * @ref : MostFrequentOccuringLeastNumber
 */
public class MostFrequentOccuringLeastNumber {

	public static void main(String[] args) {
        //int[] arr = {1, 8, 3, 5, 6, 3, 8, 1, 4, 5, 5, 4, 4, 5, 2, 2,8};//4
        //int[] arr = {4,4,4,2,2,2,1,5};//2
        int[] arr = {8154,9139,8194,3346,5450,9190,133,8239,4606,8671,8412,6290};//3346
        System.out.println(mostFrequentLeastEvenNumber(arr));
    }

    public static int mostFrequentLeastEven(int[] nums) {
        Integer[] intarr = Arrays.stream( nums ).boxed().toArray( Integer[]::new );
        Map<Integer,Long> intMap =  Arrays.stream(intarr).filter(val->val%2==0).collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        Map<Integer,Long> treeMap = new TreeMap<>(intMap);
        int ans = Collections.max(treeMap.entrySet(), Map.Entry.comparingByValue()).getKey();
        return ans;
    }

}


