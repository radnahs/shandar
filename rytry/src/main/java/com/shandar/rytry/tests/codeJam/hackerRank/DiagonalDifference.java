/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.tests.hackerrank </p>
 * <p>File Name: DiagonalDifference.java</p>
 * <p>Create Date: Jan 25, 2016 </p>
 * <p>Create Time: 6:00:21 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.tests.codeJam.hackerRank;
/**
 * 
 * Given a square matrix of size N�N, calculate the absolute difference between the sums of its diagonals.

Input Format

The first line contains a single integer, N. The next N lines denote the matrix's rows, with each line containing N space-separated integers describing the columns.

Output Format

Print the absolute difference between the two sums of the matrix's diagonals as a single integer.

Sample Input

3
11 2 4
4 5 6
10 8 -12
Sample Output

15
Explanation

The primary diagonal is: 
11
      5
            -12

Sum across the primary diagonal: 11 + 5 - 12 = 4

The secondary diagonal is:
            4
      5
10
Sum across the secondary diagonal: 4 + 5 + 10 = 19 
Difference: |4 - 19| = 15


 */


/**
 * @author Shantanu Sikdar 
 *
 */
public class DiagonalDifference {
	
	public static void main(String[] args) {
		
	}
	
	void diagonalDifference(int a[][]){
		int n = a.length;
		int dia1=0,dia2=0,revDi=n-1;
		for(int a_i=0; a_i < n; a_i++){
            for(int a_j=0; a_j < n; a_j++){
            	if(a_j==a_i){
            		dia1+=a[a_i][a_j];
            	}
            	if(a_j==revDi){
            		dia2+=a[a_i][a_j];
            	}
            }
            revDi--;
        }
		System.out.println(dia1);
		System.out.println(dia2);
	}
	
	/*void sumOfDiagonalNum(){
		
	}*/
	
}
