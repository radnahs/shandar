/**
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.utils.codeJam </p>
 * <p>File Name: QuidditchPractice.java</p>
 * <p>Create Date: Apr 14, 2014 </p>
 * <p>Create Time: 6:32:50 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.tests.codeJam;

/**
 * @author Shantanu Sikdar 
 *
 */
public class JindalSeries {

	
	public static void main(String[] args) {
		System.out.println(new JindalSeries().findSum(5,3));

	}
	
	private int findSum(int sum, int limit){
		if(sum<=limit){
			System.out.println("sum = "+sum);
			return sum;
		}else{
			System.out.println("sum = "+sum);			
			return findSum(sum-limit,limit);
		}
	}
	
	

}
