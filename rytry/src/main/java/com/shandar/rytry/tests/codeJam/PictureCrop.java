/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.utils.codeJam </p>
 * <p>File Name: PictureCrop.java</p>
 * <p>Create Date: Jun 16, 2014 </p>
 * <p>Create Time: 7:23:11 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.tests.codeJam;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Roy wants to change his profile picture on Facebook. Now Facebook has some restriction over the dimension of picture that we can upload.
 * Minimum dimension of the picture can be L x L, where L is the length of the side of square.
 * 
 * Now Roy has N photos of various dimensions.
 * Dimension of a photo is denoted as W x H
 * where W - width of the photo and H - Height of the photo
 * 
 * When any photo is uploaded following events may occur:
 * [1] If any of the width or height is less than L, user is prompted to upload another one. Print "UPLOAD ANOTHER" in this case.
 * [2] If width and height, both are large enough and
 * (a) if the photo is already square then it is accepted. Print "ACCEPTED" in this case.
 * (b) else user is prompted to crop it. Print "CROP IT" in this case.
 * 
 * (quotes are only for clarification)
 * 
 * Given L, N, W and H as input, print appropriate text as output.
 * 
 * Input:
 * First line contains L.
 * Second line contains N, number of photos.
 * Following N lines each contains two space separated integers W and H.
 * 
 * Output:
 * Print appropriate text for each photo in a new line.
 * 
 * Constraints:
 * 1 <= L,W,H <= 10000
 * 1 <= N <= 1000
 * 
 * 
 * 
 * @author Shantanu Sikdar 
 *
 */
public class PictureCrop {
	
	/**
	 * this code worked
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		Scanner s = new Scanner(System.in);
		int sideL = s.nextInt();
		int cases = s.nextInt();
		List<int[]> list = new ArrayList<int[]>();		
		
		for (int i = 0; i < cases; i++) {			
			int[] a = new int[2];
			a[0]=s.nextInt();
			a[1]=s.nextInt();			
			list.add(a);
		}		
		s.close();
		
		for (int[] intgr : list) {
			if(sideL>intgr[0] || sideL>intgr[1])
				System.out.println("UPLOAD ANOTHER");
			else{
				if(intgr[0]==intgr[1])				
					System.out.println("ACCEPTED");				
				else				
					System.out.println("CROP IT");				
			}
		}
		
	}
	
}
