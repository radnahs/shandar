package com.shandar.rytry.tests.techgig.codegladiator2016;

import java.util.ArrayList;
import java.util.List;

/**
 * In a Multinational Company employees are ordered to seat according to their height in a line. 
 * They always choose their positions randomly to displeasure their Manager. One evening, 
 * the employees learn that their strict manager has secretly recorded their seating positions 
 * from that morning, and that he will be checking their positions in the next morning to make sure 
 * they are exactly the same.
 * 
 * Each employee only remembers one thing from that morning: the number of people to his left 
 * that were taller than him. There are n employees, each with a different height between 1 to n.
 *  Using this information, you must reconstruct the seating arrangement from that morning. 
 *  
 *  You are given a int[] , the ith element of which represents the number of taller employees to the left of 
 *  the employee with height i (where i is a 1-based index). Return a int[ ] containing the heights of 
 *  the employees from left to right in the line. 
 *  
 *  Note: The input is guaranteed to produce a valid and unique output.
 *   	Inputs Specifications:
 *   			Your function should accept the following inputs:
 *   			Input 1: n
 *   			Input 2: An array(left[]) of n integers 
 *   	Output Specifications: 
 *   			You need to return the int [ ] containing the heights of the employees from left to right in the line.
 *   	Example:
 *   		Input :
 *   			input 1: 4
 *   			input 2: {2,1,1,0}
 *   			Output:
 *   					{4,2,1,3} 
 *   			Explanation:
 *   				Employee of height 1 remembered there were 2 employees taller than him to his left.
 *   				Employee of height 2 remembered there was 1 employee taller than him to his left.
 *   				Employee of height 3 remembered there was 1 employee taller than him to his left.
 *   				Employee of height 4 remembered there was no employee taller than him to his left.
 *   				The original order from left to right must have been 4, 2, 1, 3.
 *   				This ordering satisfies all four conditions. 
 *   				For example, there are exactly two employees to the left of the employees with height 1 
 *   				that are taller than him (heights 4 and 2). A different ordering, like 4, 3, 1, 2, 
 *   				satisfies some, but not all of the four conditions. In this incorrect ordering, there are 
 *   				two employees to the left of the employee with height 2 that are taller than him (heights 4 and 3), 
 *   				but the input states that there was only one. 
 *   				So valid and unique output will be {4, 2, 1, 3}. 
 *   			Instructions: 
 *   				1) Do not write main function.
 *   				2) You can print and debug your code at any step of the code.
 *   				3) You need to return the required output from the given function.
 *   				4) Do not change the function and parameter names given in editor code.
 *   				5) Return type must be the same as mentioned in the problem statement.
 *   				6) When you submit your code, test cases of different complexity level are executed in the 
 *   					background and marks are given based on&nbsp;number&nbsp;of test cases passed.
 *   				7) If you do not plan to complete the code in one sitting, then please save your work on a 
 *   					local machine. The code is saved only when it has been submitted using Submit button.
 * @author ssikdar
 *
 */

public class OrderedHeight {

	public static void main(String[] args) {
		//int[] unq= uniqueValue(4,new int[]{2,1,1,0});//passed
		//int[] unq= uniqueValue(5,new int[]{3,2,2,1,0});//passed
		//int[] unq= uniqueValue(5,new int[]{2,2,2,0,0});//failed, expected 4,5,1,2,3//passed after the break statement.
		//int[] unq= uniqueValue(5,new int[]{4,3,2,1,0});//passed
		//int[] unq= uniqueValue(5,new int[]{2,2,2,1,0});//failed expected 5,4,1,2,3//passed after the break statement.
		int[] unq= uniqueValue(9,new int[]{7,2,8,3,2,2,0,1,0});//not as expected
		for (int i = 0; i < unq.length; i++) {
			System.out.println(unq[i]);
		}
	}
	
	public static int[] uniqueValue(int input1,int[] input2){
		int left, current;
	    int arr[] = new int[input1];
	    for(int i=0;i<input1;i++)  {
	    	arr[i]=0;
	    }
	    for(int j=0;j<input1;j++)  {
	    	current = input2[j];
	    	int num = j+1;
	    	if(arr[current]==0){
	    		arr[current]=num;
	    	}else{
	    		for(int k =current; k<arr.length; k++){
	    			if(arr[k]==0 ){
	    				arr[k]=num;
	    				break;
	    			}
	    		}
	    	}
	    }
	    return arr;
	}
	
	private static int[] arrayInsert(int[] arr, int index, int value){
		for(int i =index; i<arr.length; i++){
			if(arr[index]==0 ){
				arr[index]=value;
			}
		}
		return arr;
	}
	
	/*public static int[] uniqueValue(int input1,int[] input2){
		int left, current;
	    int arr[] = new int[input1];
	    List<Integer> lstInt = new ArrayList<>();
	    for(int i=0;i<input1;i++)  {
	    	//lstInt.add(i, 0);
	    	arr[i]=0;
	    }
	    //System.out.println(lstInt);
	    for(int j=0;j<input1;j++)  {
	    	current = input2[j];
	    	//if(lstInt.get(j+current)==0){
	    	if(arr[j]==0){
	    		//lstInt.set(current,j+1);
	    		arr[j]=j+1;
	    	}else{
	    		lstInt.add(j+current,j+1);
	    	}
	    	//System.out.println(lstInt);
	    }
	    System.out.println(lstInt);
	    return arr;
	}*/
	
	
	/*public static int[] uniqueValue(int input1,int[] input2){
		int left, current;
	    int arr[] = new int[input1];
	    for (int i = 0; i < input1; i++) {
	    	arr[i]=0;
		}
	    int i = 0;
	    for(i=0;i<input1;i++)  {
	        current = input2[i];
	        left    = 0;
	        if(current > 0){
	            left    = arr[(current-1)];
	        }
	        if(left == 0 && arr[current] == 0){
	            arr[current] = input1-current;
	        }else{
	            for(int j=(i+1);j<input1;j++){
	                if(arr[j] == 0){
	                    left = arr[(j-1)];
	                    arr[j] = left - 1;
	                }
	            }
	        }
	    }
	    return arr;
	}*/

}
