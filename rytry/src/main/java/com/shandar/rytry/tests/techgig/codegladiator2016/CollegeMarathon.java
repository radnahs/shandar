package com.shandar.rytry.tests.techgig.codegladiator2016;

import java.util.Arrays;

/**
 * Students from two different colleges participate in marathons organized across India. 
 * The number of participants from each college completing in a marathon is recorded. 
 * The two colleges are said to be equal if the number of marathons completed by the participants of both 
 * colleges is the same in any order of the marathons both colleges participated in. 
 * 
 * INPUT SPECIFICATION:	Two lists with the number of participants completing the marathons from the two colleges
 * 		1) Total number of marathons
 * 		2) It is an array of integers of number of participants completing various marathons from college one
 * 		3) It is an array of integers of number of participants completing various marathons from college two 
 * OUTPUT SPECIFICATION: Equal/Unequal/Invalid 
 * 		Equal: if colleges are equal  
 * 		Unequal: if colleges are unequal  
 * 		Invalid: if any college has invalid number of completion in any marathon or the colleges have participated 
 * 				in different number of marathons 
 * EXAMPLES:
 * 		Example 1:Two colleges participated in seven marathons. Completion by participants of college one in all the 
 * 					seven marathons: {12,11,5,2,7,5,-11} 
 * 					Completion by participants of college two in all the seven marathons: {5,12,5,7,11,2,11}  
 * 				The college one has an invalid number of completions (-11). So the output will be "Invalid". 
 * 		Input : 1) 7 
 * 				2) {12,11,5,2,7,5,-11}  
 * 				3) {5,12,5,7,11,2,11}
 * 		Output: Invalid 
 * 		Example 2: Two colleges participated in seven marathons. Completion by participants of college one in all the 
 * 					seven marathons: {12,11,5,2,7,5,11}  
 * 					Completion by participants of college two in all the seven marathons: {5,12,5,7,11,2,11}  
 * 					Both the colleges are equal because they have got 11(2 times each), 5(2 times each),7( 1 time each), 
 * 					2(1 time each), 12( 1 time each) and all other outputs (0 times each). 
 * 		Input : 1) 7 
 * 				2) {12,11,5,2,7,5,11} 
 * 				3) {5,12,5,7,11,2,11} 
 * 		Output:  Equal 
 * 		Example 3:  Input  1) 7 
 * 							2) {12,11,5,2,7,5,11}  
 * 							3) {5,0,5,7,11,2,11}  
 * 					Output: Unequal 
 * 		Example 4: Input : 1) 7 
 * 							2) {12,11,5,2,7,5,11}  
 * 							3) {5,0,5,7,11,2}
 *  				Output:  Invalid
 *  		INSTRUCTIONS: 
 *  		1)  Do not write main function.  
 *  		2) You can print and debug your code at any step of the code. 
 *  		3) You need to return the required output from the given function.  
 *  		4) Do not change the function and parameter names given in editor code.
 *  		5) Return type must be the same as mentioned in the problem statement.  
 *  		6) When you submit your code, test cases of different complexity level are executed in the background and marks are 
 *  			given based on number of test cases passed.  
 *  		7) If you do not plan to complete the code in one sitting, then please save your work on a local machine. 
 *  			The code is saved only when it has been submitted using Submit button.
 * @author ssikdar
 *
 */

public class CollegeMarathon {

	public static void main(String[] args) {
		int[] coll1 = {12,11,5,2,-7,-5,11};//{12,11,5,2,7,5,-11};
		String inp1 = display(coll1);
		
		int[] coll3 = {5,12,5,7,11,2,11};
		String inp3 = display(coll3);
		inp3.contains("-");
		inp1.equalsIgnoreCase(inp3);
	}
	
		
	private static String display(int[] coll){
		Arrays.sort(coll);
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < coll.length; i++) {
			//System.out.print(coll[i]+",");
			sb.append(coll[i]);
		}
		//System.out.println();
		System.out.println(sb.toString());
		return sb.toString();
	}

}
