/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.tests.hackerrank </p>
 * <p>File Name: PlusMinus.java</p>
 * <p>Create Date: Jan 25, 2016 </p>
 * <p>Create Time: 6:48:32 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.tests.codeJam.hackerRank;

/**
 * 
 * Given an array of integers, calculate which fraction of the elements are positive, negative, and zeroes, respectively. Print the decimal value of each fraction.

Input Format

The first line, N, is the size of the array. 
The second line contains N space-separated integers describing the array of numbers (A1,A2,A3,⋯,AN).

Output Format

Print each value on its own line with the fraction of positive numbers first, negative numbers second, and zeroes third.

Sample Input

6
-4 3 -9 0 4 1         
Sample Output

0.500000
0.333333
0.166667
Explanation

There are 3 positive numbers, 2 negative numbers, and 1 zero in the array. 
The fraction of the positive numbers, negative numbers and zeroes are 36=0.500000, 26=0.333333 and 16=0.166667, respectively.

Note: This challenge introduces precision problems. The test cases are scaled to six decimal places, though answers with absolute error of up to 10−4 are acceptable.
 */

/**
 * @author Shantanu Sikdar 
 *
 */
public class PlusMinus {
	
	public static void main(String[] args) {
		System.out.println(3.0/6.0);
		zeroNegPos(new int[]{-4,3,-9,0,4,1});
	}
	
	private static void zeroNegPos(int arr[]){
		int posCnt=0,negCnt=0,zeroCnt=0;
		for (int i = 0; i < arr.length; i++) {
			if(arr[i]>0){
				posCnt++;
			}else if(arr[i]<0){
				negCnt++;
			}else{
				zeroCnt++;
			}
		}
		System.out.println((double)posCnt/(double)arr.length);
		System.out.println((double)negCnt/(double)arr.length);
		System.out.println((double)zeroCnt/(double)arr.length);
	}
	
}
