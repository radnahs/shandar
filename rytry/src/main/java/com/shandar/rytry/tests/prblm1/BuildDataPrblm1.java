/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.tests.prblm1 </p>
 * <p>File Name: BuildDataPrblm1.java</p>
 * <p>Create Date: Dec 18, 2015 </p>
 * <p>Create Time: 12:58:57 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.tests.prblm1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 * @author Shantanu Sikdar
 * 
 */
public class BuildDataPrblm1 {
	
	public double GBP_USD  = 1.654;
	public double CHF_USD  = 1.10;
	public double EUR_USD  = 1.35;

	private List<String> readDATFile() {
		List<String> lstStr = new ArrayList<String>();
		try {
			File file = new File("C:\\Data\\Projects\\rytry\\trunk\\src\\main\\java\\com\\webtual\\rytry\\tests\\prblm1\\FILE.DAT");
			Scanner scanner = new Scanner(file);
			while (scanner.hasNextLine()) {
				lstStr.add(scanner.nextLine());
			}
			scanner.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return lstStr;
	}

	private Map<String, String> createDataMap(String str0Index,String strVal) {
		Map<String, String> mapStr = new HashMap<>();
		String[] strArrIndex = str0Index.split("\t");
		String[] strArrValue = strVal.split("\t");
		for (int i = 0; i < strArrIndex.length; i++) {
			mapStr.put(strArrIndex[i], strArrValue[i]);
			
		}
		return mapStr;
	}

	private void createDataStructure() {
		Map<Integer, Map<String, String>> mapStrMap = new HashMap<>();
		List<String> lstStr = readDATFile();
		for (int i = 1; i < lstStr.size(); i++) {
			Map<String, String> mapStr = createDataMap(lstStr.get(0), lstStr.get(i));
			mapStrMap.put(i, mapStr);
		}
		System.out.println(mapStrMap);
	}
	
	public double convertCurrency(String fromCurr, String toCurr, double amnt){
		double conversionFactor=1;
		if(fromCurr!=toCurr){
			if(fromCurr.equalsIgnoreCase("GBP")){
				if(toCurr.equalsIgnoreCase("USD")){
					conversionFactor=GBP_USD; 
				}else if(toCurr.equalsIgnoreCase("CHF")){
					conversionFactor=GBP_USD/CHF_USD; 
				}else if(toCurr.equalsIgnoreCase("EUR")){
					conversionFactor=GBP_USD/EUR_USD; 
				}
			}else if(fromCurr.equalsIgnoreCase("CHF")){
				if(toCurr.equalsIgnoreCase("USD")){
					conversionFactor=CHF_USD; 
				}else if(toCurr.equalsIgnoreCase("GBP")){
					conversionFactor=CHF_USD/GBP_USD; 
				}else if(toCurr.equalsIgnoreCase("EUR")){
					conversionFactor=CHF_USD/EUR_USD; 
				}
			}else if(fromCurr.equalsIgnoreCase("EUR")){
				if(toCurr.equalsIgnoreCase("USD")){
					conversionFactor=EUR_USD; 
				}else if(toCurr.equalsIgnoreCase("GBP")){
					conversionFactor=EUR_USD/GBP_USD; 
				}else if(toCurr.equalsIgnoreCase("CHF")){
					conversionFactor=EUR_USD/CHF_USD; 
				}
			}else if(fromCurr.equalsIgnoreCase("USD")){
				if(toCurr.equalsIgnoreCase("EUR")){
					conversionFactor=1/EUR_USD; 
				}else if(toCurr.equalsIgnoreCase("GBP")){
					conversionFactor=1/GBP_USD; 
				}else if(toCurr.equalsIgnoreCase("CHF")){
					conversionFactor=1/CHF_USD; 
				}
			}
		}
		return conversionFactor*amnt;
	}

	public static void main(String[] args) {
		BuildDataPrblm1 bdp1 = new BuildDataPrblm1();
		System.out.println("test");
		bdp1.createDataStructure();
	}
	
	
}
