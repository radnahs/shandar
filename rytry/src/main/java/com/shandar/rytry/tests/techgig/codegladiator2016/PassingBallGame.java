/**
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.utils.tests.thoughtworks </p>
 * <p>File Name: ChennaiRlyStation.java</p>
 * <p>Create Date: Mar 31, 2016 </p>
 * <p>Create Time: 12:25:35 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.tests.techgig.codegladiator2016;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *There are N friends sitting in a circle, they are numbered clockwise 1 to N. 
 *Game starts with Player 1 receiving the ball. A player needs to pass the ball 
 *to other player based on some conditions
 * 1. If he is receiving ball for pth time , he passes that ball to person L places to left if p is even or L places right if p is odd.
 * 2. If any player receives the ball M times then game is over.
 * Given the values of N , L and M, you have to tell the number of times the ball is passed among players.
 * Input Specification:
 * 		First input is number of players(N) 3<N<1000
 * 		Second input is maximum number of times a player receive ball(M) 3<M<1000
 * 		Third input is a number(L)
 * Output Specification:
 * 		The function should return the number of times the ball is passed among players.
 *  	In case of any invalid input, the function should return -1.
 *  Example:
 *  	Input 1(N): 5
 *  	Input 2(M): 3
 *  	Input 3(L) : 2 
 *  Output :10
 *  Explanation:
 *  	First, player 1 gets the ball. Since he has held the ball 1 time, he passes the ball to player 4, who is two places to his right. 
 *  	This is player 4's first time holding the ball, so he gives it to player 2, who passes it to player 5. Player 5 then passes the
 *  	ball to player 3, who passes it back to player 1. Since player 1 has now held the ball 2 times, he passes it to player 3, who passes 
 *  	it to player 5, who then passes the ball to player 2. Finally, player 2 passes the ball to player 4, who then passes it to player 1. 
 *  	Player 1 has now held the ball 3 times, and the game ends.
 *  
 *  Instructions:
 *  	1) Do not write main	function
 *  	2) You can print and debug your code at any step of the code. 
 *  	3) You need to return the required output from the given function. 
 *  	4) Do not change the function and parameter names given in editor code.
 *  	5) Return type must be the same as mentioned in the problem statement. 
 *  	6) When you submit your code, test cases of different complexity level are executed in the background and marks are given based on
 *  		number of test cases passed. 
 *  	7) If you do not plan to complete the code in one sitting, then please save your work on a local machine. 
 *  		The code is saved only when it has been submitted using Submit button 
 */

public class PassingBallGame {
	
	public static void main(String[] args) {
		//System.out.println("Code Gladiator");
		PassingBallGame pbg = new PassingBallGame();
		//pbg.passCount(5, 3, 2);
		System.out.println(pbg.passCount(5, 3, 2));
		//System.out.println(6%5);
		//System.out.println(3%5);
	}
	
	private int passCount(int input1,int input2,int input3)	{
		Map<Integer, List<Integer>> numPlyr = new HashMap<>(input1);
		for(int i=1; i<=input1 ; i++){
			numPlyr.put(i, new ArrayList<Integer>());
		}
		int k = 1;
		boolean terminate = true;
		int tempPrevIndex=0;
		
		while(terminate){
			if(k == 1){
				List<Integer> indxVal = new ArrayList<>();
				indxVal.add(k);
				numPlyr.put(1, indxVal);
				tempPrevIndex=1;
			}else{
				int indx = 0;
				int cnt = numPlyr.get(tempPrevIndex).size();
				if((cnt%2)==0){
					indx = passToIndexWhenEven(tempPrevIndex,input1,input3);
				}else{
					indx = passToIndexWhenOdd(tempPrevIndex,input1,input3);
				}
				List<Integer> indxVal = numPlyr.get(indx)==null ?new ArrayList<Integer>():numPlyr.get(indx);
				indxVal.add(k);
				numPlyr.put(indx, indxVal);
				tempPrevIndex=indx;
				if(indxVal.size() == input2 ){
					terminate=false;
				}
			}
			k++;
		}
		print(numPlyr);
		return k-2;
	}
	
	private int passToIndex(int plyrsCnt,int prvIndx,int input1,int input3){
		int nxtIndxPlyr=0;
		if(plyrsCnt%2==0){
			nxtIndxPlyr = ((prvIndx+input3))%input1==0?input1:((prvIndx+input3))%input1;
		}else{
			nxtIndxPlyr = (input1+(prvIndx-input3))%input1==0?input1:(input1+(prvIndx-input3))%input1;
		}
		return nxtIndxPlyr;
	}
	
	private int passToIndexWhenOdd(int prvIndx,int input1,int input3){
		//return (input1+(prvIndx-input3))%input1;
		return (input1+(prvIndx-input3))%input1==0?input1:(input1+(prvIndx-input3))%input1;
	}

	private int passToIndexWhenEven(int prvIndx,int input1,int input3){
		//return ((prvIndx+input3))%input1;
		return ((prvIndx+input3))%input1==0?input1:((prvIndx+input3))%input1;
	}
	
	private void print(Map<Integer, List<Integer>> numPlyr){
		for (Map.Entry<Integer, List<Integer>> entryVal : numPlyr.entrySet()) {
			System.out.print(entryVal.getKey());
			System.out.println(entryVal.getValue());
		}
	}
}