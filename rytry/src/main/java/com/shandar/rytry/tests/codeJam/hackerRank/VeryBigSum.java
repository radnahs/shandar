/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.tests </p>
 * <p>File Name: VeryBigSum.java</p>
 * <p>Create Date: Jan 25, 2016 </p>
 * <p>Create Time: 4:47:42 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.tests.codeJam.hackerRank;


/**
 * 
 * You are given an array of integers of size N. You need to print the sum of the elements in the array, keeping in mind that some of those integers may be quite large.

Input

The first line of the input consists of an integer N. The next line contains N space-separated integers contained in the array.

Constraints 
1≤N≤10 
0≤A[i]≤1010

Sample Input 
5
1000000001 1000000002 1000000003 1000000004 1000000005

Output 
Print a single value equal to the sum of the elements in the array. In the above sample, you would print 5000000015.

Note: The range of the 32-bit integer is (−231) to (231−1) or [−2147483648,2147483647].
When we add several integer values, the resulting sum might exceed the above range. You might need to use long long int in C/C++ or long data type in Java to store such sums.


 * 
 */

import java.math.BigInteger;

/**
 * @author Shantanu Sikdar 
 *
 */
public class VeryBigSum {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		sum(new int[]{1,2,3,4,5,6,8,9});
	}
	
	private static void sum(int[] iarr){
		BigInteger sum=new BigInteger("0");
		BigInteger tempSum=new BigInteger("0");
		for (int i = 0; i < iarr.length; i++) {
			BigInteger temp=new BigInteger(""+iarr[i]);
			sum=tempSum;
			tempSum = sum.add(temp);
			System.out.println("temp == "+temp);
		}
		System.out.println("final sum == "+tempSum);
		
	}

}
