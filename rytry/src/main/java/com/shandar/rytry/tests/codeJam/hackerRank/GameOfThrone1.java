/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.tests.hackerrank </p>
 * <p>File Name: GameOfThrone1.java</p>
 * <p>Create Date: Feb 9, 2016 </p>
 * <p>Create Time: 6:28:10 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.tests.codeJam.hackerRank;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * @author Shantanu Sikdar 
 *
 *
 *Dothraki are planning an attack to usurp King Robert's throne. King Robert learns of this conspiracy from Raven and plans to lock the single door through which the enemy can enter his kingdom.

door

But, to lock the door he needs a key that is an anagram of a certain palindrome string.

The king has a string composed of lowercase English letters. Help him figure out whether any anagram of the string can be a palindrome or not.

Input Format 
A single line which contains the input string.

Constraints 
1≤1≤ length of string ≤105≤105 
Each character of the string is a lowercase English letter.

Output Format 
A single line which contains YES or NO in uppercase.

Sample Input : 01

aaabbbb
Sample Output : 01

YES
Explanation 
A palindrome permutation of the given string is bbaaabb. 

Sample Input : 02

cdefghmnopqrstuvw
Sample Output : 02

NO
Explanation 
You can verify that the given string has no palindrome permutation. 

Sample Input : 03

cdcdcdcdeeeef
Sample Output : 03

YES
Explanation 
A palindrome permutation of the given string is ddcceefeeccdd.
 */
public class GameOfThrone1 {

	public static char[] charArray;
	public static Set<String> chrStr = new HashSet<String>();

    public void changeOrder(int newsize) {
        int j;
        int pointAt = charArray.length - newsize;
        char temp = charArray[pointAt];
        for (j = pointAt + 1; j < charArray.length; j++) {
            charArray[j - 1] = charArray[j];
        }
        charArray[j - 1] = temp;
    }

    public void doAnagram(int newsize) {
        if (newsize == 1) {
            return;
        }
        for (int i = 0; i < newsize; i++) {
            doAnagram(newsize - 1);
            if (newsize == 2) {
                processDoorKey();
            }
            changeOrder(newsize);
        }
    }
    
    public void processDoorKey() {
    	StringBuilder sbldr = new StringBuilder();
        for (int i = 0; i < charArray.length; i++) {
            sbldr.append(charArray[i]);
        }
        if(!chrStr.contains(sbldr.toString()) && isPalindrome(sbldr.toString()))
        	chrStr.add(sbldr.toString());
    }
    
	private boolean isPalindrome(String isPalindrmStr){
		int start=0,length=isPalindrmStr.length(),end=length-1;
		while(start<end){
			if(isPalindrmStr.charAt(start)!=isPalindrmStr.charAt(end)){
				return false;
			}
			start++;end--;			
		}
		return true;
	}
	
	/*private boolean isPalindrome(char[] isPalindrmChrArr){
		int start=0,length=isPalindrmChrArr.length,end=length-1;
		while(start<end){
			if(isPalindrmChrArr[start]!=isPalindrmChrArr[end]){
				return false;
			}
			start++;end--;			
		}
		return true;
	}*/
	
	public static void main(String[] args) {
		GameOfThrone1 got1 =  new GameOfThrone1();
		//charArray = "aaabbbb".toCharArray();
		//charArray = "cdcdcdcdeeeef".toCharArray();
		//charArray = "cdefghmnopqrstuvw".toCharArray();
		//charArray = "love".toCharArray();
		//char[] charArr = "cdcdcdcdeeeeffhb".toCharArray();//cdcdcdcdeeeef
		//char[] charArr = "cdefghmnopqrstuvw".toCharArray();
		char[] charArr = "aaaabbbb".toCharArray();
		//got1.alphabetCount(charArr);
		got1.isPalindrome(got1.alphabetCount(charArr));
/*		got1.doAnagram(charArray.length);
		for (String strArr : chrStr) {
			System.out.println(strArr);
		}
		System.out.println(chrStr.size());
*/		/*System.out.println(got1.isPalindrome("oyo"));
		System.out.println(got1.isPalindrome("poyo"));*/
	}
	
	private Map<Character, Integer> alphabetCount(char[] charArr){
		Map<Character, Integer> charCountMap = new HashMap<Character, Integer>();
		for (int i = 0; i < charArr.length; i++) {
			if(charCountMap.containsKey(charArr[i])){
				charCountMap.put(charArr[i], charCountMap.get(charArr[i])+1);
			}else{
				charCountMap.put(charArr[i], 1);
			}
		}
		System.out.println(charCountMap);
		return charCountMap;
	}
	
	private boolean isPalindrome(Map<Character, Integer> charCountMap){
		boolean isPalindrome=false;
		int count=0;
		for(Entry<Character, Integer> entry :charCountMap.entrySet()){
			System.out.println(entry.getKey());
			if(count<1 && entry.getValue()%2==1){
				System.out.println("if");
				isPalindrome=true;
				count++;
			}else if(count>0 && entry.getValue()%2==1){
				System.out.println("else if");
				isPalindrome=false;
			}else if(count==0 && entry.getValue()%2==0){
				isPalindrome=true;
			}
			
		}
		System.out.println(isPalindrome);
		return isPalindrome;
	}
	
}
