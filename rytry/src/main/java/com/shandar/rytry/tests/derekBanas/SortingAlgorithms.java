/**
* Project: rytry
* Package Name:package com.shandar.rytry.tests.derekBanas;
* File Name: SortingAlgorithms.java
* Create Date: Jan 20, 2017
* Create Time: 4:41:19 PM
* Copyright: Copyright (c) 2016
* @author: Shantanu Sikdar, ssikdar
* @version 1.0
*/
package com.shandar.rytry.tests.derekBanas;
/*https://www.youtube.com/watch?v=JUOyKSZScW0&index=2&list=PLvVMzZGFLf-zb0aLLosOrXb3Utw_b7XUS
*/
public class SortingAlgorithms {
	
	private int[] theArray = new int[50];
	private int arraySize=10;
	
	public void generateRandomArray(){
		for (int i = 0; i < arraySize; i++) {
			theArray[i] = (int)(Math.random()*10)+10;
		}
	}

	public void bubbleSort(){
		for (int i = arraySize-1; i > 1 ; i--) {
			for (int j = 0; j < i; j++) {
				if(theArray[j]>theArray[j+1]){//change the greaterThan sign to lessThan for making it decreasing order
					swapValues(j, j+1);
					printHorzArray(i,j);
				}
				printHorzArray(i,j);
			}
		}
	}
	
	public void swapValues(int index1, int index2){
		int temp=theArray[index1];
		theArray[index1]=theArray[index2];
		theArray[index2]=temp;
	}
	
	public void binarySearchForValue(int value){
		int lowIndex=0;
		int highIndex=arraySize-1;
		
		while(lowIndex<=highIndex){
			int middleIndex=(highIndex+lowIndex)/2;
			if(theArray[middleIndex]<value){
				lowIndex=middleIndex+1;
			}else if(theArray[middleIndex]<value) {
				highIndex=middleIndex-1;
			}else{
				System.out.println("found a Match for " + value +" at index "+ middleIndex);
				lowIndex=highIndex+1;
			}
			printHorzArray(middleIndex, -1);
		}
	}
	
	public void selectionSort(){
		for (int x = 0; x < arraySize; x++) {
			int minimum=x;
			for (int y = x; y < arraySize; y++) {
				if(theArray[minimum]>theArray[y]){ //change the greaterThan sign to lessThan for making it decreasing order
					minimum=y;
				}
			}
			swapValues(x, minimum);
			printHorzArray(x, -1);
		}
	}
	
	public void printHorzArray(int i, int j){

	    for(int n = 0; n < 51; n++){
	    	System.out.print("-");
	    }
	    System.out.println();

	    for(int n = 0; n < arraySize; n++){
	        System.out.print("| " + n + "  ");
	    }

	    System.out.println("|");

	    for(int n = 0; n < 51; n++)System.out.print("-");
	    System.out.println();

	    for(int n = 0; n < arraySize; n++){
	        System.out.print("| " + theArray[n] + " ");
	    }
	     
	    System.out.println("|");

	    for(int n = 0; n < 51; n++)System.out.print("-");
	    System.out.println();

	    // END OF FIRST PART

	    // ADDED FOR BUBBLE SORT
	    if(j != -1){
	        // ADD THE +2 TO FIX SPACING
	        for(int k = 0; k < ((j*5)+2); k++)System.out.print(" ");
	        System.out.print(j);
	    }

	    // THEN ADD THIS CODE
	    if(i != -1){
	        // ADD THE -1 TO FIX SPACING
	        for(int l = 0; l < (5*(i - j)-1); l++)System.out.print(" ");
	        System.out.print(i);
	    }

	    System.out.println();
	}

	public void insertionSort(){
		for (int i = 1; i < arraySize; i++) {
			int j=i;
			int toInsert=theArray[i];
			
			while ((j>0) && (theArray[j-1]>toInsert)) {
				theArray[j]=theArray[j-1];
				j--;
				printHorzArray(i, j);
			}
			theArray[j]=toInsert;
			printHorzArray(i, j);
			System.out.println("\n Array[i] = "+theArray[i]+" Array[j] = "+theArray[j]+" toInsert = " +toInsert);
		}
	}
	
	public static void main(String[] args) {
		SortingAlgorithms sortAlgo = new SortingAlgorithms();
		sortAlgo.generateRandomArray();
		//sortAlgo.bubbleSort();//--1
		//sortAlgo.binarySearchForValue(11);//--2
		//sortAlgo.selectionSort();//--3
		
		sortAlgo.insertionSort();//-4
	}

}
