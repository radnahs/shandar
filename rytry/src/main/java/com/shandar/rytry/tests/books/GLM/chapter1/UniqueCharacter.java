/**
* Project: rytry
* Package Name:package com.shandar.rytry.tests.books.GLM.chapter1;
* File Name: UniqueCharacter.java
* Create Date: Oct 20, 2016
* Create Time: 4:48:26 PM
* Copyright: Copyright (c) 2016
* @author: Shantanu Sikdar, ssikdar
* @version 1.0
*/
package com.shandar.rytry.tests.books.GLM.chapter1;
/**
 * Implement an algorithm to determine if a string has all unique characters. What if
 * you cannot use additional data structures?
 */

import java.util.Arrays;

public class UniqueCharacter {

	public boolean isUniqueCharCheck(String str){
		char[] strCharArr = str.toLowerCase().toCharArray();
		Arrays.sort(strCharArr);
		for (int i=1;i<strCharArr.length;i++) {
			if(strCharArr[i-1]==strCharArr[i]){
				return false;
			}
		}
		return true;
	}
	
	public boolean isUniqueCharCheck2(String str){
		boolean[] charSet=new boolean[256];
		for (int i = 0; i < str.length(); i++) {
			int idx = str.charAt(i);
			if(charSet[idx]){
				return false;
			}
			charSet[idx]=true;
		}
		return true;
	}
	public void display(char[] strCharArr){
		for (char c : strCharArr) {
			System.out.print(c);
		}
		System.out.println("");
	}
}
