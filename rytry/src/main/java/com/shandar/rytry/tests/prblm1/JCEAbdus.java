/**
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.tests.prblm1 </p>
 * <p>File Name: JCEAbdus.java</p>
 * <p>Create Date: Dec 18, 2015 </p>
 * <p>Create Time: 12:21:25 PM </p>
 * <p>Description:
	 2HRS!
		Subject: Java Coding Exercise
		Question:		 
		Given the supplied tab delimited input file (FILE.DAT), write a Java program that loads the data and determines
		 the average amount in Euros (EUR),	grouped by Country and Credit Rating.		 
		If a country is not available, the city should be used instead.
		 
		Exchange Rates:
		GBP -> USD  = 1.654
		CHF -> USD  = 1.10
		EUR -> USD  = 1.35
		 
		Rules:		 
		1.This exercise should be treated as a regular work task (i.e. apply usual professional dev practices: unit tests, etc.), 
			within 2 hours.
		2.The candidate must work independently, no collaboration is permitted.
		3.The candidate may conduct research either online or offline as they see fit, in order to complete the task.
 </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.tests.prblm1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

/**
 * @author Shantanu Sikdar 
 *
 */
public class JCEAbdus {

	public double GBP_USD  = 1.654;
	public double CHF_USD  = 1.10;
	public double EUR_USD  = 1.35;

	private List<String> readDATFile() {
		List<String> lstStr = new ArrayList<String>();
		try {
			File file = new File("C:\\Data\\Projects\\rytry\\trunk\\src\\main\\java\\com\\webtual\\rytry\\tests\\prblm1\\FILE.DAT");
			Scanner scanner = new Scanner(file);
			while (scanner.hasNextLine()) {
				lstStr.add(scanner.nextLine());
			}
			scanner.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return lstStr;
	}

	private Map<String, String> createDataMap(String str0Index,String strVal) {
		Map<String, String> mapStr = new HashMap<>();
		String[] strArrIndex = str0Index.split("\t");
		String[] strArrValue = strVal.split("\t");
		for (int i = 0; i < strArrIndex.length; i++) {
			mapStr.put(strArrIndex[i], strArrValue[i]);
		}
		mapStr.put("EUR Value",convertCurrency(mapStr.get("Currency"), "EUR", Double.parseDouble(mapStr.get("Amount")))+"");
		return mapStr;
	}

	private Map<Integer, Map<String, String>> createDataStructure() {
		Map<Integer, Map<String, String>> mapStrMap = new HashMap<>();
		List<String> lstStr = readDATFile();
		for (int i = 1; i < lstStr.size(); i++) {
			Map<String, String> mapStr = createDataMap(lstStr.get(0), lstStr.get(i));
			mapStrMap.put(i, mapStr);
		}
		//System.out.println(mapStrMap);
		return mapStrMap;
	}
	
	private double convertCurrency(String fromCurr, String toCurr, double amnt){
		double conversionFactor=1;
		if(fromCurr!=toCurr){
			if(fromCurr.equalsIgnoreCase("GBP")){
				if(toCurr.equalsIgnoreCase("USD")){
					conversionFactor=GBP_USD; 
				}else if(toCurr.equalsIgnoreCase("CHF")){
					conversionFactor=GBP_USD/CHF_USD; 
				}else if(toCurr.equalsIgnoreCase("EUR")){
					conversionFactor=GBP_USD/EUR_USD; 
				}
			}else if(fromCurr.equalsIgnoreCase("CHF")){
				if(toCurr.equalsIgnoreCase("USD")){
					conversionFactor=CHF_USD; 
				}else if(toCurr.equalsIgnoreCase("GBP")){
					conversionFactor=CHF_USD/GBP_USD; 
				}else if(toCurr.equalsIgnoreCase("EUR")){
					conversionFactor=CHF_USD/EUR_USD; 
				}
			}else if(fromCurr.equalsIgnoreCase("EUR")){
				if(toCurr.equalsIgnoreCase("USD")){
					conversionFactor=EUR_USD; 
				}else if(toCurr.equalsIgnoreCase("GBP")){
					conversionFactor=EUR_USD/GBP_USD; 
				}else if(toCurr.equalsIgnoreCase("CHF")){
					conversionFactor=EUR_USD/CHF_USD; 
				}
			}else if(fromCurr.equalsIgnoreCase("USD")){
				if(toCurr.equalsIgnoreCase("EUR")){
					conversionFactor=1/EUR_USD; 
				}else if(toCurr.equalsIgnoreCase("GBP")){
					conversionFactor=1/GBP_USD; 
				}else if(toCurr.equalsIgnoreCase("CHF")){
					conversionFactor=1/CHF_USD; 
				}
			}
		}
		return conversionFactor*amnt;
	}
	
	private void groupByParamsTheMap(Map<Integer, Map<String, String>> mapStrMap, String[] firstParam, String secondParam){
		Map<Integer, Map<String, String>> mapStrMapGrouped =  new TreeMap<Integer, Map<String,String>>();
		List<Map.Entry<Integer, Map<String, String>>> list =new LinkedList<>(mapStrMap.entrySet());
		
		System.out.println("ungrped == "+list);
		
		Collections.sort(list, new Comparator<Map.Entry<Integer, Map<String, String>>>() {
			public int compare(Map.Entry<Integer, Map<String, String>> o1,
								Map.Entry<Integer, Map<String, String>> o2) {
				return (o1.getValue().get("Country")).compareTo(o2.getValue().get("Country"));
			}
		});
		
		System.out.println("grped == "+list);
		
		/*for (Entry<Integer, Map<String, String>> entry : list) {
			mapStrMapGrouped.put(entry.getKey(), entry.getValue());
		}
		
		System.out.println("MapGrouped : "+mapStrMapGrouped);*/
	}

	public static void main(String[] args) {
		JCEAbdus jceAbds1 = new JCEAbdus();
		Map<Integer, Map<String, String>> mapStrMap = jceAbds1.createDataStructure();
		
		jceAbds1.groupByParamsTheMap(mapStrMap, new String[]{"Country","City"}, "Credit Rating");
		
		/*double dd = jceAbds1.convertCurrency("GBP", "EUR", 600);
		System.out.println(dd);*/
	}

}
