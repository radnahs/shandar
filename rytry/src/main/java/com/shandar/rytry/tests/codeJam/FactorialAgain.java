/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.utils.codeJam </p>
 * <p>File Name: FactorialAgain.java</p>
 * <p>Create Date: Apr 15, 2014 </p>
 * <p>Create Time: 3:48:23 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.tests.codeJam;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * Kate has finally calmed down and decides to forgive Little Deepu, but she won't forgive him just like that. 
 * She agrees to forgive him on the grounds that he can solve a mathematical question for her.
 * She gives Deepu a large number N and a prime number P and asks him to calculate ((3*N)! / (3!^N) )%P.
 * Your task is to help Little Deepu get back together with Kate.
 * 
 * Input
 * First line contains number of test cases T.
 * Next T lines contain two integers N and P, where P is a prime.
 * 
 * Output
 * For each test case output the result ( (3*N)! / (3!)N ) % P.
 * 
 * Constraints:
 * 1 = T = 10000
 * 1 = N = 333333333
 * 1 = P = 1000000000
 * 1 = abs(3*N-P) = 1000
 * 
 * 
 * @author Shantanu Sikdar 
 *
 */
public class FactorialAgain {

	public static void main(String[] args) {
		
		//System.out.println(calculate(3, 11));
		/*System.out.println(calculate(2, 11));*/
		//System.out.println(calculate(4, 13));
		//System.out.println(calculate(4, 17));
		System.out.println("********************************");
		
		//System.out.println(calculateUsingCollection(3, 11));
		//System.out.println(calculateUsingCollection(2, 11));
		//System.out.println(calculateUsingCollection(4, 13));
		//System.out.println(calculateUsingCollection(6, 23));
		
		/*System.out.println(calculateUsingArray(3, 11));
		System.out.println(calculateUsingArray(2, 11));*/
		//System.out.println(isPrimeDiv(51,17));
		//System.out.println(isPrimeDiv(55,17));
		/*System.out.println(factorialCalculation(3, 11));
		System.out.println(factorialCalculationB(3, 11));
		System.out.println(factorialCalculation(2, 11));
		System.out.println(factorialCalculationB(2, 11));
		System.out.println(factorialCalculation(7, 7));
		System.out.println(factorialCalculationB(7, 7));*/
		//System.out.println(raiseToPower(19, 2));
		//System.out.println(factorialTill(8, 5));
		//System.out.println(factorialTill(5, 1));
		//System.out.println(factorialTill(8,6));
		//System.out.println(factorialRecursion(new BigInteger(6+"")));
		System.out.println(raiseToPower(new BigInteger(703+""),2));
		
	}
		
	private static BigInteger factorialRecursion(BigInteger bint){
		if(bint.equals(BigInteger.ONE))
			return BigInteger.ONE;
		else{
			return (bint.multiply(factorialRecursion(bint.subtract(BigInteger.ONE))));
		}				
	}
	
	private static BigInteger raiseToPower(BigInteger bint, int p){
		if(p==1)
			return bint.multiply(BigInteger.ONE);
		else
			return bint.multiply(raiseToPower(bint,p-1));		
	}
	
	private static int calculateUsingArray(int num, int p){
		Integer[] listNum = new Integer[3*num];
		Integer[] listDen = new Integer[num];
		int l=0;
		for (int i = 1; i <= 3*num; i++) {			
			if(i<=num){				
				if (i>1) {					
					listDen[l]=2;
					listDen[++l]=3;
					l++;
				}				
			}else{
				int k=0;				
				int cc=i;
				while(k<listDen.length){
					int t = listDen[k];
					if(t>1 && cc%t==0){
						listDen[k]=1 ;						
						cc=cc/t;
					}					
					k++;
					
				}
				listNum[i]=cc;
			}
		}
		System.out.println(listNum);
		System.out.println(listDen);		
		return 0;		
	}
	
	//8888166667 abhay pratap singh	godrej service  manager.
	private static int calculateUsingCollection(int num, int p){
		List<Integer> listNum = new ArrayList<>();// we can get rid of it 
		List<Integer> listDen = new ArrayList<>();
		int res=1;
		
		
		for (int i = 1; i <= 3*num; i++) {			
			if(i<=num){
				if (i>1) {
					listDen.add(2);
					listDen.add(3);
				}
				if(i>3){					
					listNum.add(i);// we can get rid of it
					res=i;
				}
			}else{
				int cc=i;
				int k=0;
				while(k<listDen.size()){
					int t = listDen.get(k);
					if(t>1 && cc%t==0){
						listDen.add(k, 1);
						listDen.remove(k+1);						
						cc=cc/t;
					}					
					k++;					
				}				
				listNum.add(cc); // we can get rid of it
				res*=cc;
			}
		}
		System.out.println(listNum);// we can get rid of it
		System.out.println(listDen);		
		System.out.println(res%p);
		
		return 0;		
	}
	
	
	/**
	 * This function strictly following Calculating ((3*N)! / (3!^N) )%P
	 * @param num
	 * @param p
	 * @return
	 */
	private static int calculate(int num, int p){		
		int prod=1;
		int i = 0;
		int lftdiv=1;
		for (int j = 3*num; j > 0; j--,i++) {
			prod = prod*j;
			while(i<num){
				if(prod%6==0){
					prod = prod/6;
				}else if(prod%3==0){
					prod = prod/3;
					lftdiv*=2;
				}else if(prod%2==0){
					prod = prod/2;
					lftdiv*=3;
				}else{
					lftdiv=6;
				}
				break;
			}
			
			if(prod%lftdiv==0){
				prod = prod/lftdiv;
				lftdiv=1;
			}
		}		
		return prod%p;
	}
	
	private static long raiseToPowerRecursion(long n, long p){		
		if(p==1){
			return n*1l;
		}else{
			return (n*raiseToPowerRecursion(n,p-1));
		}				
	}
	
	private static int raiseToPower(int n, int p){
		int i=0;
		int prod=1;
		while(!(i==p)){
			prod*=n;
			i++;
		}
		return prod;
	}
	
	
	
	// type 1
	
	private static long factorialCalculationB(long n, long p){	
		//long l= (factorialTill(3*n,3)/raiseToPower(factorialTill(3,1),n-1))%p;
		long l= (factorialTillRecursion(3*n,3)/raiseToPowerRecursion(6,n-1))%p;
		return l;
	}
	
	private static long factorialTillRecursion(long n, long p){
		if(n==p){
			return 1;
		}else{
			return (n*factorialTillRecursion(n-1,p));
		}				
	}
	
	private static int factorialTill(int n, int p){		
		int prod=1;
		while(!(n==p)){
			prod*=n;
			n--;			
		}
		return prod;		
	}
	
	// type 2	
	
	private static long factorialCalculation(long n, long p){	
		long l= (factorialWithRecursion(3*n)/raiseToPowerRecursion(factorialWithRecursion(3),n))%p;
		return l;
	}
	
	private static long factorialWithRecursion(long n){
		if(n==1){
			return 1;
		}else{
			return (n*factorialWithRecursion(n-1));
		}				
	}
	
	
	private static int isPrimeDiv(int dd,int prim){			
		while(dd%prim==0)
			dd=dd/prim;		
		return dd%prim;
	}
	
	public int factorial(int m){
		if(m==1){
			return m;
		}
		return m*factorial(m-1);
	}
	
	public boolean isPrimeNumber(int number){
        for(int i=2; i<=number/2; i++){
            if(number % i == 0){
                return false;
            }
        }
        return true;
    }

}

