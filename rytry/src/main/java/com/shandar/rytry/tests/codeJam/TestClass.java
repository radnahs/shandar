/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.utils.codeJam </p>
 * <p>File Name: TestClass.java</p>
 * <p>Create Date: Jun 4, 2014 </p>
 * <p>Create Time: 12:31:35 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.tests.codeJam;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * ******For :: Hackers Earth, Input reading.
 * 
 * @author Shantanu Sikdar 
 *
 */
public class TestClass {
	
	public static void main(String[] args) throws Exception {
		Scanner s = new Scanner(System.in);
		int sideL = s.nextInt();
		int cases = s.nextInt();
		List<int[]> list = new ArrayList<int[]>();
		
		for (int i = 0; i < cases; i++) {			
			int[] a = new int[2];
			a[0]=s.nextInt();
			a[1]=s.nextInt();			
			list.add(a);
		}		
		s.close();
		
		for (int[] intgr : list) {
			if(sideL>intgr[0] || sideL>intgr[1])
				System.out.println("UPLOAD ANOTHER");
			else{
				if(intgr[0]==intgr[1])				
					System.out.println("ACCEPTED");				
				else				
					System.out.println("CROP IT");				
			}
		}
		
	}
}
