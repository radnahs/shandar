/**
* Project: rytry
* Package Name:package com.shandar.rytry.tests.LeetCode;
* File Name: ReversePolishNotation.java
* Create Date: Jan 19, 2017
* Create Time: 2:29:50 PM
* Copyright: Copyright (c) 2016
* @author: Shantanu Sikdar, ssikdar
* @version 1.0
*/
package com.shandar.rytry.tests.LeetCode;

/**
 * Evaluate the value of an arithmetic expression in Reverse Polish Notation .
Valid operators are + , -,* , /. Each operand may be an integer or another expression.
Some examples:
[ " 2 " , " 1 " , "+" , " 3 " , " * " ] -> ( ( 2 + 1) * 3) -> 9
[ " 4 " , " 13 " , " 5 " , "/" , "+" ] -> (4 + (13 / 5) ) -> 6
 */

import java.util.Stack;

public class ReversePolishNotation {

	public static void main(String[] args) {
		ReversePolishNotation rpn = new ReversePolishNotation();
		String[] rpnInputArr = { "2" , "1" , "+" , "3" , "*" };
		//String[] rpnInputArr = {"4" , "13" , "5" , "/" , "+" };
		int rpnr = rpn.evaluateRPN(rpnInputArr);
		System.out.println(rpnr);
	}
	
	//My solution 
	//also asked in TOMTOM written test.
	private int evaluateRPN(String[] rpnInputArr){
		Stack<String> stkStr =  new Stack<>();
		int t=0;
		try{
			for (String strng : rpnInputArr) {
				if(strng.equalsIgnoreCase("*")){
					int k = new Integer(stkStr.pop()); 
					int l = new Integer(stkStr.pop());
					//int rslt = l*k;
					stkStr.push((l*k)+"");
				}else if(strng.equalsIgnoreCase("+")){
					int k = new Integer(stkStr.pop()); 
					int l = new Integer(stkStr.pop());
					//int rslt = l+k;
					stkStr.push((l+k)+"");
				}else if(strng.equalsIgnoreCase("-")){
					int k = new Integer(stkStr.pop()); 
					int l = new Integer(stkStr.pop());
					//int rslt = l-k;
					stkStr.push((l-k)+"");
				}else if(strng.equalsIgnoreCase("/")){
					int k = new Integer(stkStr.pop()); 
					int l = new Integer(stkStr.pop());
					//int rslt = l/k;
					stkStr.push((l/k)+"");
				}else{
					stkStr.push(strng);
				}
			}
			t = Integer.parseInt(stkStr.pop());
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return t ;
	}

}
