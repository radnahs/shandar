package com.shandar.rytry.tests.codeJam;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * http://www.hackerearth.com/problems/
 * Practice Problem 83. Lucky String
 * 
 * 
 * Lucky numbers are those numbers which contain only "4" and/or "5". For example 4, 5, 44, 54,55,444 are lucky numbers 
 * while 457, 987 ,154 are not.
 * Lucky number sequence is one in which all lucky numbers exist in increasing order for example 
 * 4,5,44,45,54,55,444,445,454,455,544,545,554,555...
 * Now we concatenate all the lucky numbers (in ascending order) to make a lucky string "4544455455444445454455..."
 * Given n, your task is to find the nth digit of the lucky string. If the digit is 4 then you have to print 
 * "Hacker" else you have to print "Earth".
 * 
 * 
 * @author Shantanu Sikdar
 * Solution Using: jre1.7 
 * there might be annotation errors.   
 *
 */
public class LuckyString_Submitted {
	
	public static void main(String[] args) {
		String lckStr = null;		
		BufferedReader br = null;
		try {
			int i=0;			
			br = new BufferedReader(new InputStreamReader(System.in));
			String line=null;
			while((line=br.readLine())!=null){				
				if(i==0){
					lckStr = findLuckyNumber();
				}else{	
					int k = new Integer(line);
					if(k>0)
						System.out.println(findNthNumber(lckStr,k));					
				}
				i++;
			}
			br.close();
		} catch (Exception io) {
			io.printStackTrace();
		}		
	}
	
	private static String findLuckyNumber(){
		StringBuilder luckyString = new StringBuilder();
		luckyString.append("45");
		int digitIncrFactor=1;
		int luck1=4, luck2=5;
		int lowerLimit=0, upperLimit=0;
		List<Integer> lst = new ArrayList<Integer>();
		//int limit = Integer.MAX_VALUE/10000;
		for(int digit=0;digit<6;digit++){
			digitIncrFactor*=10;			
			upperLimit=(luck2+1)*digitIncrFactor-lowerLimit;
			lowerLimit=luck1*digitIncrFactor+lowerLimit;			
			for(int i=lowerLimit;i<upperLimit;i++){
				boolean fal=false;
				for (char c : (i+"").toCharArray()) {
					if(c=='4' || c=='5'){
						fal=true;
					}else{
						fal=false;
						break;
					}					
				}
				if(fal){
					lst.add(i);
					luckyString.append(i);
				}		
			}			
		}		
		return luckyString.toString();
	}
	
	private static String findNthNumber(String lckStr, int n){
		StringBuilder luckyString = new StringBuilder();
		char[] charArr =  lckStr.toCharArray();
		n=n-1;
		if(charArr[n]=='4'){
			luckyString.append("Hacker");
		}else {
			luckyString.append("Earth");
		}
		return luckyString.toString();
	}

}
