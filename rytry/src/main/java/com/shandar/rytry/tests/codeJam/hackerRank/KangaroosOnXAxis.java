/**
* Project: rytry
* Package Name:package com.shandar.rytry.tests.codeJam.hackerRank;
* File Name: KangaroosOnXAxis.java
* Create Date: Oct 18, 2016
* Create Time: 6:53:37 PM
* Copyright: Copyright (c) 2016
* @author: Shantanu Sikdar, ssikdar
* @version 1.0
*/
package com.shandar.rytry.tests.codeJam.hackerRank;

/**
 * There are two kangaroos on an x-axis ready to jump in the positive direction 
 * (i.e, toward positive infinity). The first kangaroo starts at location  and moves 
 * at a rate of  meters per jump. The second kangaroo starts at location  and moves at 
 * a rate of  meters per jump. Given the starting locations and movement rates for 
 * each kangaroo, can you determine if they'll ever land at the same location at the same time?
 * 
 * Input Format
 * A single line of four space-separated integers denoting the respective values of x1, v1, x2, and v2.
 * 
 * Constraints
 * 0<=x1<=x2<=10000
 * 1<=v1<=1000
 * 1<=v2<=1000
 *
 * Output Format
 * Print YES if they can land on the same location at the same time; otherwise, print NO.
 * Note: The two kangaroos must land at the same location after making the same number of jumps.
 * 
 * Sample Input 0
 * 0 3 4 2
 * Sample Output 0
 * YES
 * Explanation 0
 * 1. 0->3->6->9->12
 * 2. 4->6->8->10->12
 * 
 * The two kangaroos jump through the following sequence of locations:
 * Thus, the kangaroos meet after  jumps and we print YES.
 * 
 * Sample Input 1
 * 0 2 5 3
 * Sample Output 1
 * NO
 * Explanation 1
 * The second kangaroo has a starting location that is ahead (further to the right) 
 * of the first kangaroo's starting location (i.e., x2>x1 ). Because the second kangaroo 
 * moves at a faster rate (meaning v2>v1) and is already ahead of the first kangaroo, 
 * the first kangaroo will never be able to catch up. Thus, we print NO.
 * 
 * @author ssikdar
 *
 */

public class KangaroosOnXAxis {

	public static void main(String[] args) {
		findMeetingPoint(0, 3, 4, 2);
		System.out.println();
	}
	
	private static String findMeetingPoint(int x1, int v1, int x2, int v2){
		long r1=x1, r2=x2;
		if(x1<=x2 && v1<v2 || x2<=x1 && v2<v1){
			return "NO";
		}else {
			while(r1!=r2){
				r1=r1+v1;
				r2=r2+v2;
			}
			return "YES";
		}
		
	}
	

}
