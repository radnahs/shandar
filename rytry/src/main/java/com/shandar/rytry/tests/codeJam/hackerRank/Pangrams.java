/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.tests.hackerrank </p>
 * <p>File Name: Pangrams.java</p>
 * <p>Create Date: Feb 8, 2016 </p>
 * <p>Create Time: 8:46:23 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.tests.codeJam.hackerRank;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import org.apache.commons.lang.ArrayUtils;

/**
 * @author Shantanu Sikdar
 * 
 */
public class Pangrams {

	private static Set<Character> setAlphabetCount = new HashSet<Character>();

	static {
		char ch;
		for (ch = 'a'; ch <= 'z'; ch++){
			setAlphabetCount.add(ch);
		}
	}

	public static void main(String[] args) {
		//System.out.println(setAlphabetCount);
		//System.out.println(setAlphabetCount.size());
		Scanner in = new Scanner(System.in);
        String pangramStr = in.nextLine();
		System.out.println(isPangram("We promptly judged antique ivory buckles for the prize"));
		System.out.println(isPangram("We promptly judged antique ivory buckles for the next prize"));
		
	
		//System.out.println(testPangram("We promptly judged antique ivory buckles for the prize"));
		System.out.println(testPangram("We promptly judged antique ivory buckles for the next prize"));
		System.out.println(testPangram("We promptly judged antique ivory buckles for the next prize"));
	}
	
	
	private static String testPangram(String str){
		boolean contain=false;
		char[] chrLst = str.replace(" ", "").toCharArray();
		List<Character> listChr = new ArrayList<Character>();
		for (Character character : chrLst) {
			listChr.add(character);
		}
		for (Character character : setAlphabetCount) {
			if(listChr.contains(Character.toLowerCase(character)) || listChr.contains(Character.toUpperCase(character))){
				contain=true;
			}else{
				contain=false;
				break;
			}
		}
		return contain?"pangram":"not pangram";
	}
	
	
	/**
	 * ArrayUtils is apache class
	 * @param str
	 * @return
	 */
	private static String isPangram(String str){
		boolean contain=false;
		List<Character> listChr = Arrays.asList(ArrayUtils.toObject( str.replace(" ", "").toCharArray()));
		for (Character character : setAlphabetCount) {
			if(listChr.contains(Character.toLowerCase(character)) || listChr.contains(Character.toUpperCase(character))){
				contain=true;
			}else{
				contain=false;
				break;
			}
		}
		return contain?"pangram":"not pangram";
	}

}
