/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.utils.codeJam </p>
 * <p>File Name: SherlockAndDate.java</p>
 * <p>Create Date: Aug 4, 2014 </p>
 * <p>Create Time: 3:07:58 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.tests.codeJam;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Shantanu Sikdar 
 *
 */
public class SherlockAndDate {
	
	/*private SherlockAndDate() throws Exception{
		throw new Exception("my exception");
	}*/

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SherlockAndDate ssdd = new SherlockAndDate();
		ssdd.yesterday("23 July 1914", "dd mmm yyyy:HH:mm");
	}
	
	private void yesterday(String date, String dateFormat){		
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
			Date dt = sdf.parse(date);
			System.out.println(dt);
			
			
		} catch (ParseException pe) {
			pe.printStackTrace();
		}
	}

}
