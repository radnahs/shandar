/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.utils.codeJam </p>
 * <p>File Name: SmallFactorial.java</p>
 * <p>Create Date: Jun 17, 2014 </p>
 * <p>Create Time: 3:36:47 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.tests.codeJam;

import java.math.BigInteger;
import java.util.Scanner;


/**
 * You are asked to calculate factorials of some small positive integers.
 * 
 * Input
 * An integer T, denoting the number of testcases, followed by T lines, each containing a single integer N.
 * 
 * Output 
 * For each integer N given at input, output a single line the value of N!
 * 
 * Input Constraint
 * 1 <= T <= 100
 * 1 <= N <= 100
 * 
 * @author Shantanu Sikdar 
 *
 */
public class SmallFactorial {
	
	public static void main(String args[] ) throws Exception {
    	Scanner s = new Scanner(System.in);
    	int cases = s.nextInt();
    	BigInteger[] bint = new BigInteger[cases];		
		for (int i = 0; i < cases; i++) {						
			int a=s.nextInt();
			bint[i]=new BigInteger(a+"");			
		}
		s.close();      
		for (int i = 0; i<cases;i++){
	        System.out.println(factorialRecursion(bint[i]));
		}
    }
    
    public static BigInteger factorialRecursion(BigInteger bint){
		if(bint.equals(BigInteger.ONE)){
			return BigInteger.ONE;
		}else{
			return (bint.multiply(factorialRecursion(bint.subtract(BigInteger.ONE))));
		}				
	}
}
