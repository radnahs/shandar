/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.utils.codeJam.hackerRank </p>
 * <p>File Name: UtopianTree.java</p>
 * <p>Create Date: Aug 23, 2014 </p>
 * <p>Create Time: 5:09:27 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.tests.codeJam.hackerRank;

/**
 * @author Shantanu Sikdar 
 *
 */
public class UtopianTree {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		UtopianTree utopianTree=new UtopianTree();
		System.out.println(utopianTree.growthCycle(4));
		System.out.println(utopianTree.growthCycle(8));
	}
		
	public long growthCycle(int numCycle){
		long ht=0;
		for (int i = 0; i <= numCycle; i++) {
			if(i%2==0){
				ht=ht+1;
			}else{
				ht=2*ht;
			}
		}
		return ht;
	}

}
