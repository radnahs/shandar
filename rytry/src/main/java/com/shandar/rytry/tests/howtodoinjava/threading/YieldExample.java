/**
* Project: rytry
* Package Name:package com.shandar.rytry.tests.howtodoinjava.threading;
* File Name: YieldExample.java
* Create Date: Jan 13, 2017
* Create Time: 3:08:01 PM
* Copyright: Copyright (c) 2016
* @author: Shantanu Sikdar, ssikdar
* @version 1.0
*/
package com.shandar.rytry.tests.howtodoinjava.threading;

public class YieldExample {

	public static void main(String[] args) {
		Thread producer = new ProducerYield();
		Thread consumer = new ConsumerYield();
      
		producer.setPriority(Thread.MIN_PRIORITY); //Min Priority
		consumer.setPriority(Thread.MAX_PRIORITY); //Max Priority
      
		producer.start();
		consumer.start();

	}

}

class ProducerYield extends Thread{
   public void run()   {
      for (int i = 0; i < 5; i++)      {
         System.out.println("I am ProducerYield : Produced Item " + i);
         Thread.yield();
      }
   }
}

class ConsumerYield extends Thread{
   public void run()   {
      for (int i = 0; i < 5; i++)      {
         System.out.println("I am ConsumerYield : Consumed Item " + i);
         Thread.yield();
      }
   }
}
