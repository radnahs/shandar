/**
 * <p>Project: rytry </p>
 * <p>Package Name: com.shandar.rytry.tutorial.princeton </p>
 * <p>File Name: PrimeSieve.java </p>
 * <p>Create Date: 31-Mar-2012 </p>
 * <p>Create Time: 8:22:35 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.rytry.tutorial.princeton;

/**
 * @author : Shantanu Sikdar
 * @Description : PrimeSieve
 */
public class PrimeSieve {

	public static void main(String[] args) {
		
		int n = 100;//Integer.parseInt(args[0]);

		// initially assume all integers are prime
		boolean[] isPrime = new boolean[n + 1];
		for (int i = 2; i <= n; i++) {
			isPrime[i] = true;
		}

		// mark non-primes <= n using Sieve of Eratosthenes
		for (int factor = 2; factor * factor <= n; factor++) {

			// if factor is prime, then mark multiples of factor as nonprime
			// suffices to consider mutiples factor, factor+1, ..., n/factor
			if (isPrime[factor]) {
				for (int j = factor; factor * j <= n; j++) {
					isPrime[factor * j] = false;
				}
			}
		}

		// count primes
		int primes = 0;
		for (int i = 2; i <= n; i++) {
			if (isPrime[i])
				primes++;
		}
		System.out.println("The number of primes <= " + n + " is " + primes);
	}

}
