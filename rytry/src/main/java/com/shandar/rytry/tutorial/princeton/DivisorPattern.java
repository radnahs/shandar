/**
 * <p>Project: com.shandar.rytry.tutorial.princeton </p>
 * <p>Package Name: com.shandar.rytry.tutorial.princeton </p>
 * <p>File Name: DivisorPattern.java </p>
 * <p>Create Date: Jun 3, 2017 </p>
 * <p>Create Time: 10:30:37 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.tutorial.princeton;

/**
* Compilation: javac DivisorPattern.java
* Execution: java DivisorPattern n
*
* Prints a table where entry (i, j) is a '* ' if i divides j
* or j divides i and '. ' otherwise.
*
*
* % java DivisorPattern 20
* * * * * * * * * * * * * * * * * * * * * 1
* * * * * * * * * * * * 2
* * * * * * * * 3
* * * * * * * * 4
* * * * * * 5
* * * * * * * 6
* * * * 7
* * * * * * 8
* * * * * 9
* * * * * * 10
* * * 11
* * * * * * * 12
* * * 13
* * * * * 14
* * * * * 15
* * * * * * 16
* * * 17
* * * * * * * 18
* * * 19
* * * * * * * 20
*
*/


/**
 * @author: Shantanu Sikdar
 */
public class DivisorPattern {
	

}
