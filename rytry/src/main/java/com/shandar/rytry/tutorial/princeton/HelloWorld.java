/**
 * <p>Project: com.shandar.rytry.tutorial.princeton </p>
 * <p>Package Name: com.shandar.rytry.tutorial.princeton </p>
 * <p>File Name: HelloWorld.java </p>
 * <p>Create Date: Jun 3, 2017 </p>
 * <p>Create Time: 5:23:57 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.tutorial.princeton;

/**
 * ****************************************************************************
 * Compilation: javac HelloWorld.java
 * Execution: java HelloWorld
 *
 * Prints "Hello, World". By tradition, this is everyone's first program.
 *
 * % java HelloWorld
 * Hello, World
 *
 * These 17 lines of text are comments. They are not part of the program;
 * they serve to remind us about its properties. The first two lines tell
 * us what to type to compile and test the program. The next line describes
 * the purpose of the program. The next few lines give a sample execution
 * of the program and the resulting output. We will always include such
 * lines in our programs and encourage you to do the same.
 *
 */


/**
 * @author: Shantanu Sikdar
 */
public class HelloWorld {

	public static void main(String[] args) {
		// Prints "Hello, World" to the terminal window.
		System.out.println("Hello, World");
	}

}
