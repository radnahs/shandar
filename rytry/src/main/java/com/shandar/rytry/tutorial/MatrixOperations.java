/**
 * <p>Project: com.shandar.rytry.tutorial </p>
 * <p>Package Name: com.shandar.rytry.tutorial </p>
 * <p>File Name: TransposeMatrix.java </p>
 * <p>Create Date: Jun 14, 2017 </p>
 * <p>Create Time: 8:16:31 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.tutorial;

/**
 * @author: Shantanu Sikdar
 */
public class MatrixOperations {

	private void display1D(int[] oneDArray){
		System.out.println();
		for (int i = 0; i < oneDArray.length; i++) {
			System.out.print(oneDArray[i]+"      ");
		}
	}
	
	private void display2D(int[][] twoDArray){
		for (int i = 0; i < twoDArray.length; i++) {
			display1D(twoDArray[i]);
		}
	}
	
	private int[][] constructMatrixContNum(int m, int n, int startVal){
		int[][] twoDArray=new int[m][n];
		for (int i = 0; i < twoDArray.length; i++) {
			for (int j = 0; j < twoDArray[i].length; j++) {
				twoDArray[i][j]=startVal;
				startVal++;
			}
		}
		return twoDArray;
	}
	
	private int[][] transpose(int[][] origMatrix){
		int m = origMatrix.length;
		int n = origMatrix[0].length;
		int[] arr1DTemp = new int[m*n];
		int temp=0;
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				arr1DTemp[temp]=origMatrix[i][j];
				temp++;
			}
		}
		int[][] transposeMatrix=new int[n][m];
		for (int i = 0; i < transposeMatrix.length; i++) {
			int tmp=i;
			for (int j = 0; j < transposeMatrix[i].length; j++) {
				transposeMatrix[i][j]=arr1DTemp[tmp];
				tmp= tmp+(n);
			}
		}
		return transposeMatrix;
	}
	
	public static void main(String[] args) {
		MatrixOperations mtrxOprns = new MatrixOperations(); 
		int[][] twoDArray= mtrxOprns.constructMatrixContNum(10, 8, 6);
		mtrxOprns.display2D(twoDArray);
		System.out.println(" ");
		int[][] twoDArrayTranspose= mtrxOprns.transpose(twoDArray);
		mtrxOprns.display2D(twoDArrayTranspose);
	}

}
 