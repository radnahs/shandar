/**
 * <p>Project: com.shandar.rytry.tutorial.princeton </p>
 * <p>Package Name: com.shandar.rytry.tutorial.princeton </p>
 * <p>File Name: PlayThatTune.java </p>
 * <p>Create Date: Sep 2, 2015 </p>
 * <p>Create Time: 11:28:26 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2014</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.tutorial.princeton;

/**
* Compilation: javac UseArgument.java
* Execution: java UseArgument yourname
*
* Prints "Hi, Bob. How are you?" where "Bob" is replaced by the
* command-line argument.
*
* % java UseArgument Bob
* Hi, Bob. How are you?
*
* % java UseArgument Alice
* Hi, Alice. How are you?
*
*/

public class UseArgument {

public static void main(String[] args) {
System.out.print("Hi, ");
System.out.print(args[0]);
System.out.println(". How are you?");
}

}
