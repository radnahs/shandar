/**
 * <p>Project: com.shandar.rytry.tutorial </p>
 * <p>Package Name: com.shandar.rytry.tutorial </p>
 * <p>File Name: Initials.java </p>
 * <p>Create Date: May 25, 2017 </p>
 * <p>Create Time: 3:01:34 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.tutorial;

/**
 * @author: Shantanu Sikdar
 */
public class Initials {

	public static void main(String[] args) {
		
		System.out.println("*****************           *               *");
        System.out.println("*               *           *               *");
        System.out.println("*                           *               *");
        System.out.println("*                           *               *");
        System.out.println("*****************           *****************");
        System.out.println("                *           *               *");
        System.out.println("                *           *               *");
        System.out.println("*               *           *               *");
        System.out.println("*****************           *               *");
		
	}

}
