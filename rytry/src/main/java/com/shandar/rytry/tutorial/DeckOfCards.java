/**
 * <p>Project: com.shandar.rytry.tutorial </p>
 * <p>Package Name: com.shandar.rytry.tutorial </p>
 * <p>File Name: DeckOfCards.java </p>
 * <p>Create Date: May 31, 2017 </p>
 * <p>Create Time: 6:38:59 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.tutorial;

/**
 * @author: Shantanu Sikdar
 */
public class DeckOfCards {

	char[] SUITS = new char[]{'C','D','H','S'};
	char[] RANKS = new char[]{'A','2','3','4','5','6','7','8','9','T','J','Q','K'};
	char[][] DECKS = new char[SUITS.length][RANKS.length];
	char[][] playerM = new char[SUITS.length][RANKS.length];
	char[][] playerN = new char[SUITS.length][RANKS.length];
	char[][] playerO = new char[SUITS.length][RANKS.length];
	char[][] playerP = new char[SUITS.length][RANKS.length];
	
	private void deckUnShuffled(){
		for (int i = 0; i < SUITS.length; i++) {
			for (int j = 0; j < RANKS.length; j++) {
				DECKS[i][j]=RANKS[j];
			}
		}
	}
	
	
	private void displayCards(char[][] cards){
		for (int i = 0; i < cards.length; i++) {
			for (int j = 0; j < cards[i].length; j++) {
				System.out.print(cards[i][j]+" ");
			}
			System.out.println();
		}
	}
	
	private void distribute(){
		//int k=0;
		for (int i = 0; i < DECKS.length; i++) {
			System.out.println("DECKS[i].length = "+DECKS[i].length);
			for (int j = 0; j < DECKS[i].length; ) {
				/*if(j==DECKS[i].length){
					break;
				}*/
				/*playerM[i][k]=DECKS[i][j++];
				playerN[i][k]=DECKS[i][j++];
				playerO[i][k]=DECKS[i][j++];
				playerP[i][k]=DECKS[i][j++];
				k++;*/
				if(DECKS[i][j]!=0){
					playerM[i][j]=DECKS[i][j];
					DECKS[i][j]=0;
					
				}
				
			}
			System.out.println();
		}
	}
	
	public static void main(String[] args) {
		DeckOfCards deckOfCards = new DeckOfCards();
		deckOfCards.deckUnShuffled();
		deckOfCards.displayCards(deckOfCards.DECKS);
		deckOfCards.distribute();
		deckOfCards.displayCards(deckOfCards.playerM);
		deckOfCards.displayCards(deckOfCards.playerN);
		deckOfCards.displayCards(deckOfCards.playerO);
		deckOfCards.displayCards(deckOfCards.playerP);
	}

}
