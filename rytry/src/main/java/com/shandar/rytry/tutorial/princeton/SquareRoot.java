/**
 * <p>Project: rytry </p>
 * <p>Package Name: com.shandar.rytry.tutorial.princeton </p>
 * <p>File Name: Sqrt.java </p>
 * <p>Create Date: 31-Mar-2012 </p>
 * <p>Create Time: 8:45:37 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.rytry.tutorial.princeton;

/**
 * @author : Shantanu Sikdar
 * @Description : Sqrt
 */
public class SquareRoot {

	public static void main(String[] args) {
		// read in the command-line argument
		String ss = "3";
		//double c = Double.parseDouble(args[0]);
		double c = Double.parseDouble(ss);
		double epsilon = 1e-15; // relative error tolerance
		double t = c; // estimate of the square root of c

		// repeatedly apply Newton update step until desired precision is achieved
		while (Math.abs(t - c / t) > epsilon * t) {
			t = (c / t + t) / 2.0;
		}

		// print out the estimate of the square root of c
		System.out.println(t);

	}

}
