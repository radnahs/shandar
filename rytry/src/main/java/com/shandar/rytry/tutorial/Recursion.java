/**
 * <p>Project: com.shandar.rytry.tutorial </p>
 * <p>Package Name: com.shandar.rytry.tutorial </p>
 * <p>File Name: Recursion.java </p>
 * <p>Create Date: May 31, 2017 </p>
 * <p>Create Time: 4:15:18 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.tutorial;

/**
 * @author: Shantanu Sikdar
 */
public class Recursion {

	public static void main(String[] args) {
		Recursion recursion = new Recursion();
		//recursion.backwardRecursion(23);
		System.out.println();
		recursion.forwardRecursion(23,1);
	}
	
	private boolean backwardRecursion(int val){
		System.out.print(val+" ");
		if(val==1){
			return false;
		}
		backwardRecursion(val-1);
		return true;
	}

	private boolean forwardRecursion(int val, int k){
		System.out.print(k+" ");
		if(k==val){
			return false;
		}
		forwardRecursion(val,++k);
		return true;
	}

}
