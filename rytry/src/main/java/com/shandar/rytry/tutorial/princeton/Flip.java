/**
 * <p>Project: com.shandar.rytry.tutorial.princeton </p>
 * <p>Package Name: com.shandar.rytry.tutorial.princeton </p>
 * <p>File Name: Flip.java </p>
 * <p>Create Date: Sep 2, 2015 </p>
 * <p>Create Time: 11:28:26 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2014</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.tutorial.princeton;

/**
 * Compilation: javac Flip.java Execution: java Flip
 *
 * Simulate a fair coin flip and print out "Heads" or "Tails" accordingly.
 *
 * % java Flip Heads
 *
 * % java Flip Heads
 *
 * % java Flip Tails
 *
 *
 */

public class Flip {

	public static void main(String[] args) {

		// Math.random() returns a value between 0.0 and 1.0
		// so it is heads or tails 50% of the time
		if (Math.random() < 0.5)
			System.out.println("Heads");
		else
			System.out.println("Tails");
	}
}