/**
 * <p>Project: rytry </p>
 * <p>Package Name: com.shandar.rytry.barclays </p>
 * <p>File Name: LineLimits.java </p>
 * <p>Create Date: Nov 27, 2019 </p>
 * <p>Create Time: 3:05:37 PM </p>
 * <p>Copyright: Copyright (c) 2014</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.barclays;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @author: Shantanu Sikdar
 * @description: breaking a line having more than 50 character, into set of 
 * 				 50 characters per line. using string.split("(?<=\\G.{50})");
 *
 */
public class LineLimits {

	public static void main(String[] args) throws Exception {
		List<String> limitChar = new ArrayList<String>();
		System.out.println("out");
		File file = new File(
				"C:\\DATA\\projects\\shandar\\rytry\\src\\main\\java\\com\\shandar\\rytry\\barclays\\fileLimits.txt");
		Scanner scanner = new Scanner(file);
		while (scanner.hasNext()) {
			String string = (String) scanner.nextLine();
			String[] strArr = string.split("(?<=\\G.{50})");
			for (String string2 : strArr) {
				limitChar.add(string2);
			}
		}
		for (String string : limitChar) {
			System.out.println(string);
		}

	}

}
/*
 * fileLimits.txt
 *following is the output
 *
An overdraft limit is a borrowing facility whic
h allows you to borrow money through your curren
t account.<br>There are two types of Overdraf
t - arranged and unarranged.<br>An Arranged Over
draft is a pre-agreed limit, which lets you spen
d more money than you have in your current accou
nt. It can be a safety net to cover short-term o
utgoings, like an unexpected bill. It is not sui
table for longer-term borrowing. We charge you f
or every day of the month that you use your Arra
nged Overdraft where you go beyond any fee-free l
imit you may have.<br>An Unarranged Overdraft i
s when you spend more money than you have in you
r current account and you have not agreed an Arr
anged Overdraft limit with us in advance or you h
ave exceeded an existing Arranged Overdraft limit
.<br>You can only make payments from your accoun
t if you have enough money in your account or th
rough an Arranged Overdraft to cover them. Barcl
ays will always attempt to return any transactio
n that could take your account into an Unarrange
d Overdraft position. Having enough money in you
r current account or having an Arranged Overdraf
t limit could help prevent payments such as prio
rity bills from being returned unpaid.<br>Any re
turned payments will incur an Unpaid Transactio
n Fee (£8 per day - maximum of 4 Unpaid Transacti
on Fees per charging period).<br>On very rare occ
asions we may be unable to return a payment (e.g
. due to an offline transaction made on a flight
) and the account may enter an Unarranged Overdra
ft. No additional charges will be applied in thi
s situation.<br>Information regarding the conduc
t of your account may be sent to credit referenc
e agencies. As with any debt or borrowing, this m
ay affect your ability to get credit in the futur
e.<br>Our Eligibility tool can show you the likel
ihood of getting an Arranged Overdraft and the Ov
erdraft calculator lets you to see how much it co
uld cost to use an Overdraft. To use these tool
s and find out more about Overdrafts charges, ple
ase visit www.barclays.co.uk/overdrafts<br>You ca
n reduce or remove an agreed limit by visitin
g a branch or by calling us on 0345 734 5345. Lim
its cannot be reduced to a position below any exi
sting debit balance. Any debit balance must be pa
id off before a limit can be removed.<br>If we ho
ld a valid mobile number for you, we’ll automatic
ally enrol you to receive relevant alerts regardi
ng borrowing and refused payments, to help you av
oid charges. You can also choose to receive addit
ional alerts, including Low Balance and Large Cre
dit or Debit. Tailor your alerts to your persona
l needs online, by phone or in branch. To find ou
t more, visit Barclays.co.uk/alerts
*/
