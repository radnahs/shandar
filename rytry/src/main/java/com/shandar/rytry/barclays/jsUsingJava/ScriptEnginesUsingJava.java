/**
 * <p>Project: rytry </p>
 * <p>Package Name: com.shandar.rytry.barclays.jsUsingJava </p>
 * <p>File Name: EnumerateScriptEngines.java </p>
 * <p>Create Date: Jul 31, 2016 </p>
 * <p>Create Time: 3:49:24 PM </p>
 * <p>Copyright: Copyright (c) 2014</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.barclays.jsUsingJava;

import java.util.List;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineFactory;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

/**
 * @author Shantanu Sikdar
 * @desc https://www.javaworld.com/article/2077796/ajax-programming-with-the-java-scripting-api.html
 */
public class ScriptEnginesUsingJava {

	public static void enumerateScriptEngines() {

		ScriptEngineManager manager = new ScriptEngineManager();

		List<ScriptEngineFactory> factories = manager.getEngineFactories();
		for (ScriptEngineFactory factory : factories) {
			System.out.println("Engine name (full): " + factory.getEngineName());
			System.out.println("Engine version: " + factory.getEngineVersion());
			System.out.println("Supported extensions:");
			List<String> extensions = factory.getExtensions();
			for (String extension : extensions)
				System.out.println("  " + extension);
			System.out.println("Language name: " + factory.getLanguageName());
			System.out.println("Language version: " + factory.getLanguageVersion());
			System.out.println("Supported MIME types:");
			List<String> mimetypes = factory.getMimeTypes();
			for (String mimetype : mimetypes)
				System.out.println("  " + mimetype);
			System.out.println("Supported short names:");
			List<String> shortnames = factory.getNames();
			for (String shortname : shortnames)
				System.out.println("  " + shortname);
			System.out.println();
		}
	}

	public static void obtainScriptEngine() {
		ScriptEngineManager manager = new ScriptEngineManager();

		ScriptEngine engine1 = manager.getEngineByExtension("js");
		System.out.println(engine1);

		ScriptEngine engine2 = manager.getEngineByMimeType("application/javascript");
		System.out.println(engine2);

		ScriptEngine engine3 = manager.getEngineByName("nashorn");
		System.out.println(engine3);
	}

	static void invokeAjaxCallUsingJS() {
		try {
			ScriptEngineManager manager = new ScriptEngineManager();
			ScriptEngine engine = manager.getEngineByName("JavaScript");

			// JavaScript code in a String
			String script1 = (String) "function loadDoc() { " + "  var xhttp = new XMLHttpRequest(); "
					+ "  xhttp.onreadystatechange = function() { "
					+ "    if (this.readyState == 4 && this.status == 200) { "
					+ "      document.getElementById(\"demo\").innerHTML = this.responseText; " + "    } " + "  }; "
					+ "  xhttp.open(\"GET\", \"https://search.barclays.co.uk/content/bf/en/4_0/branches_atms?lat=51.4044448&lng=-0.2518595000000232&facilities=t\", true); "
					+ "  xhttp.send(); " + "}";
			// evaluate script
			engine.eval(script1);
			Invocable inv = (Invocable) engine;
			inv.invokeFunction("loadDoc", "Scripting!!"); // This one works.
		} catch (ScriptException exp) {
			exp.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
	}

	// https://stackoverflow.com/questions/22856279/call-external-javascript-functions-from-java-code
	static void invokeScriptFunction() {
		try {
			ScriptEngineManager manager = new ScriptEngineManager();
			ScriptEngine engine = manager.getEngineByName("JavaScript");

			// JavaScript code in a String
			String script1 = (String) "function functionName(name) {print ('Hello, ' + name);}";
			String script2 = (String) "function getValue(a,b) { if (a===\"Number\") return 1; else return b;}";
			String script3 = (String) "function getSum(a,b) { if (a===\"Number\") {print (a+b);} else {print (a-b);} }";
			// evaluate script
			engine.eval(script1);
			engine.eval(script2);
			engine.eval(script3);

			Invocable inv = (Invocable) engine;

			inv.invokeFunction("functionName", "Shantanu!!"); // This one works.
			inv.invokeFunction("getValue", "200", "100");
			System.out.println(script2);
			inv.invokeFunction("getSum", "200", "100");

		} catch (ScriptException exp) {
			exp.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		// enumerateScriptEngines();
		obtainScriptEngine();

		invokeScriptFunction();
		invokeAjaxCallUsingJS();
	}

}
