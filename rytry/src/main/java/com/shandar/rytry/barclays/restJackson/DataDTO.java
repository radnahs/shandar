/**
 * <p>Project: rytry </p>
 * <p>Package Name: com.shandar.rytry.barclays.restJackson </p>
 * <p>File Name: DataDTO.java </p>
 * <p>Create Date: 21-Sep-2017 </p>
 * <p>Create Time: 8:29:12 am </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.rytry.barclays.restJackson;

/**
 * @author : Shantanu Sikdar
 * @Description : DataDTO
 */
public class DataDTO {
	
	private String id;
	private String email;
	private String first_name;
	private String last_name;
	private String avatar;
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the first_name
	 */
	public String getFirst_name() {
		return first_name;
	}
	/**
	 * @param first_name the first_name to set
	 */
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	/**
	 * @return the last_name
	 */
	public String getLast_name() {
		return last_name;
	}
	/**
	 * @param last_name the last_name to set
	 */
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	/**
	 * @return the avatar
	 */
	public String getAvatar() {
		return avatar;
	}
	/**
	 * @param avatar the avatar to set
	 */
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	
	@Override
	public String toString() {
		return "DataDTO [id=" + id + ", email=" + email + ", first_name=" + first_name + ", last_name=" + last_name
				+ ", avatar=" + avatar + "]";
	}

}
