/**
 * <p>Project: rytry </p>
 * <p>Package Name: com.shandar.rytry.barclays.pkgInfo </p>
 * <p>File Name: ReadJavaFile.java </p>
 * <p>Create Date: Mar 17, 2020 </p>
 * <p>Create Time: 8:10:40 PM </p>
 * <p>Copyright: Copyright (c) 2014</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.barclays.pkgInfo;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;

/**
 * @author Shantanu Sikdar
 *
 */
public class ReflectJavaFile {

	public void reflectJavaContents(String classWithPkgName) {
		try {
			Class<?> beanClass = Class.forName(classWithPkgName);
			reflectFields(beanClass);
			//reflectMethods(beanClass);
			reflectConstructors(beanClass);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void reflectFields(Class<?> beanClass) {
		Field[] fields = beanClass.getDeclaredFields();
		Arrays.stream(fields).forEach(field -> System.out.println(field.toString()));
	}

	private void reflectMethods(Class<?> beanClass) {
		Method[] methods = beanClass.getDeclaredMethods();
		Arrays.stream(methods).forEach(method -> System.out.println(method.toString() ));
	}
	
	public void reflectConstructors(Class<?> beanClass) {
		Constructor<?>[] constructors = beanClass.getConstructors();
		Arrays.stream(constructors).forEach(constructor -> System.out.println(constructor.toString()));
	}

	public static void main(String[] args) {
		ReflectJavaFile reflectJavaFile = new ReflectJavaFile();
		reflectJavaFile.reflectJavaContents("com.shandar.rytry.barclays.pkgInfo.ReflectJavaFile");
	}

}
