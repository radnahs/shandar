/**
 * <p>Project: rytry </p>
 * <p>Package Name: com.shandar.rytry.barclays.restJackson </p>
 * <p>File Name: PageDTO.java </p>
 * <p>Create Date: 21-Sep-2017 </p>
 * <p>Create Time: 8:24:53 am </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.rytry.barclays.restJackson;

import java.util.List;

/**
 * @author : Shantanu Sikdar
 * @Description : PageDTO
 */
public class PageDTO {
	
	private String page;
	private String per_page;
	private String total;
	private String total_pages;
	private List<DataDTO> data;
	/**
	 * @return the page
	 */
	public String getPage() {
		return page;
	}
	/**
	 * @param page the page to set
	 */
	public void setPage(String page) {
		this.page = page;
	}
	/**
	 * @return the per_page
	 */
	public String getPer_page() {
		return per_page;
	}
	/**
	 * @param per_page the per_page to set
	 */
	public void setPer_page(String per_page) {
		this.per_page = per_page;
	}
	/**
	 * @return the total
	 */
	public String getTotal() {
		return total;
	}
	/**
	 * @param total the total to set
	 */
	public void setTotal(String total) {
		this.total = total;
	}
	/**
	 * @return the total_pages
	 */
	public String getTotal_pages() {
		return total_pages;
	}
	/**
	 * @param total_pages the total_pages to set
	 */
	public void setTotal_pages(String total_pages) {
		this.total_pages = total_pages;
	}
	/**
	 * @return the data
	 */
	public List<DataDTO> getData() {
		return data;
	}
	/**
	 * @param data the data to set
	 */
	public void setData(List<DataDTO> data) {
		this.data = data;
	}
	
}
