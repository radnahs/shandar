/**
 * <p>Project: rytry </p>
 * <p>Package Name: com.shandar.rytry.barclays </p>
 * <p>File Name: FetchRestParseJSONData.java </p>
 * <p>Create Date: Dec 10, 2016 </p>
 * <p>Create Time: 2:01:31 AM </p>
 * <p>Copyright: Copyright (c) 2014</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.barclays;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 * @author Shantanu Sikdar
 *
 */
public class FetchRestParseJSONData {

	public static void main13(String[] args) throws IOException{
		HttpURLConnection.setFollowRedirects(false); // Set follow redirects to false
		//final URL url = new URL("https://reqres.in/api/users");
		URL url = new URL("https://maps.googleapis.com/maps/api/js?key=AIzaSyBmljnMMiKYGqgm0eli1CSyuSBDcmYNfvg");
		HttpURLConnection huc = (HttpURLConnection) url.openConnection();
		huc.setRequestMethod("GET");
		System.out.println(huc.getResponseCode());
	}

	public static void main(String[] args) {
		try {
			URL url = new URL("https://reqres.in/api/users");

			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("GET");

			con.setRequestProperty("Content-Type", "application/json; utf-8");
			con.setRequestProperty("Accept", "application/json");

			con.setDoOutput(true);

			// JSON String need to be constructed for the specific resource.
			// We may construct complex JSON using any third-party JSON libraries such as
			// jackson or org.json
			String jsonInputString = "{\"name\": \"Upendra\", \"job\": \"Programmer\"}";

			try (OutputStream os = con.getOutputStream()) {
				byte[] input = jsonInputString.getBytes("utf-8");
				os.write(input, 0, input.length);
			}

			int code = con.getResponseCode();
			System.out.println(code);

			try (BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
				StringBuilder response = new StringBuilder();
				String responseLine = null;
				while ((responseLine = br.readLine()) != null) {
					response.append(responseLine.trim());
				}
				System.out.println(response.toString());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main11(String[] args) {
		System.out.println("blah blah");

		URL url = null;
		try {
			System.setProperty("https.proxyHost", "138.64.240.73");
			System.setProperty("https.proxyPort", "8080");
			System.setProperty("https.proxyHost", "138.64.240.73");
			System.setProperty("https.proxyPort", "8080");

			System.setProperty("Authorization",
					"Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6IlRJQUFfQVMtU2lnbiJ9.eyJhZGdyb3VwcyI6WyJDTj1HbG8tSW50LVNlYy1MZWdhY3lQYXNzd29yZFBvbGljeTEwLE9VPUFjdGl2ZURpcmVjdG9yeSxPVT1Qcml2aWxlZ2VkR3JvdXBzLE9VPV9JbmZyYXN0cnVjdHVyZSxEQz1JTlRSQU5FVCxEQz1CQVJDQVBJTlQsREM9Y29tIiwiQ049R0xPLUlOVC1TRUMtTm9uSW50ZXJhY3RpdmUsT1U9QWN0aXZlRGlyZWN0b3J5LE9VPVByaXZpbGVnZWRHcm91cHMsT1U9X0luZnJhc3RydWN0dXJlLERDPUlOVFJBTkVULERDPUJBUkNBUElOVCxEQz1jb20iLCJDTj1mR0xCSUJETlRFU1QsT1U9R3JvdXBzLE9VPUdMQixEQz1JTlRSQU5FVCxEQz1CQVJDQVBJTlQsREM9Y29tIiwiQ049ZkdMQlBydGdTSVRPRFNULE9VPUdyb3VwcyxPVT1HTEIsREM9SU5UUkFORVQsREM9QkFSQ0FQSU5ULERDPWNvbSJdLCJzdWIiOiJzeXNUSUFBQVNNQ0FBU1NJVCIsIlgtU1NMQ2xpZW50Q2VydFZhbGlkVXBUbyI6IjE2MzQxNjk1OTkwMDAiLCJYLVNTTENsaWVudENlcnRTQU4iOiJbMiwgVElBQS1DTElFTlRdIiwiWC1TU0xDbGllbnRDZXJ0RE4iOiJDTj1USUFBLUNMSUVOVCwgT1U9R0lTLCBPPUJhcmNsYXlzIFBMQywgTD1Mb25kb24sIFNUPUxvbmRvbiwgQz1HQiIsIlgtU1NMQ2xpZW50SXNzdWVyQ2VydEROIjoiQ049VGVzdCBHbG9iYWwgSW5mcmFzdHJ1Y3R1cmUgSW50ZXJtZWRpYXRlIENBICg3KSwgT1U9VGVzdCBHbG9iYWwgSW5mcmFzdHJ1Y3R1cmUgSW50ZXJtZWRpYXRlIENBICg3KSwgTz1CYXJjbGF5cyBQTEMiLCJleHAiOjE1NzU5MDExMDEsInNjb3BlIjpbXSwiY2xpZW50X2lkIjoiSUJETkNMSUVOVCIsImlzcyI6IlRJQUEtQVMiLCJhdWQiOiJJQkROIn0.cG6jrNXuTzAdiM6X50a_MiKJuc3vZwb7j_5TJ8o7zsXQAkF4qF2a_u1TgmqCycwqFcNnIHJrMpGVQw8ZU2SnqRBHOxP0AKd-79WIxE4XwWmhHQ-WR1ZoFuoosw7tTYPIDpRB5b4edBo6uM_UMW-3LHqyPEqmiGQcXZMmaiko-WTpJ4OnxQJGayYvLWH6ETVDkngsFiAPmfODoEnJ4i9o_WtqxhPOa5ztBCqOgaV6G6JjkN74oj7X9-GtFqH-Vk_2_Duzx74KeuhWM1FG6YpglRWKV79FWGSENrOCoVVBrjZGvPxD1kNjQm8X-gByCPvgGLLOsNMlYdRq52BbbCtsyg");
			System.setProperty("Proxy-Authorization",
					"Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6IlRJQUFfQVMtU2lnbiJ9.eyJhZGdyb3VwcyI6WyJDTj1HbG8tSW50LVNlYy1MZWdhY3lQYXNzd29yZFBvbGljeTEwLE9VPUFjdGl2ZURpcmVjdG9yeSxPVT1Qcml2aWxlZ2VkR3JvdXBzLE9VPV9JbmZyYXN0cnVjdHVyZSxEQz1JTlRSQU5FVCxEQz1CQVJDQVBJTlQsREM9Y29tIiwiQ049R0xPLUlOVC1TRUMtTm9uSW50ZXJhY3RpdmUsT1U9QWN0aXZlRGlyZWN0b3J5LE9VPVByaXZpbGVnZWRHcm91cHMsT1U9X0luZnJhc3RydWN0dXJlLERDPUlOVFJBTkVULERDPUJBUkNBUElOVCxEQz1jb20iLCJDTj1mR0xCSUJETlRFU1QsT1U9R3JvdXBzLE9VPUdMQixEQz1JTlRSQU5FVCxEQz1CQVJDQVBJTlQsREM9Y29tIiwiQ049ZkdMQlBydGdTSVRPRFNULE9VPUdyb3VwcyxPVT1HTEIsREM9SU5UUkFORVQsREM9QkFSQ0FQSU5ULERDPWNvbSJdLCJzdWIiOiJzeXNUSUFBQVNNQ0FBU1NJVCIsIlgtU1NMQ2xpZW50Q2VydFZhbGlkVXBUbyI6IjE2MzQxNjk1OTkwMDAiLCJYLVNTTENsaWVudENlcnRTQU4iOiJbMiwgVElBQS1DTElFTlRdIiwiWC1TU0xDbGllbnRDZXJ0RE4iOiJDTj1USUFBLUNMSUVOVCwgT1U9R0lTLCBPPUJhcmNsYXlzIFBMQywgTD1Mb25kb24sIFNUPUxvbmRvbiwgQz1HQiIsIlgtU1NMQ2xpZW50SXNzdWVyQ2VydEROIjoiQ049VGVzdCBHbG9iYWwgSW5mcmFzdHJ1Y3R1cmUgSW50ZXJtZWRpYXRlIENBICg3KSwgT1U9VGVzdCBHbG9iYWwgSW5mcmFzdHJ1Y3R1cmUgSW50ZXJtZWRpYXRlIENBICg3KSwgTz1CYXJjbGF5cyBQTEMiLCJleHAiOjE1NzU5MDExMDEsInNjb3BlIjpbXSwiY2xpZW50X2lkIjoiSUJETkNMSUVOVCIsImlzcyI6IlRJQUEtQVMiLCJhdWQiOiJJQkROIn0.cG6jrNXuTzAdiM6X50a_MiKJuc3vZwb7j_5TJ8o7zsXQAkF4qF2a_u1TgmqCycwqFcNnIHJrMpGVQw8ZU2SnqRBHOxP0AKd-79WIxE4XwWmhHQ-WR1ZoFuoosw7tTYPIDpRB5b4edBo6uM_UMW-3LHqyPEqmiGQcXZMmaiko-WTpJ4OnxQJGayYvLWH6ETVDkngsFiAPmfODoEnJ4i9o_WtqxhPOa5ztBCqOgaV6G6JjkN74oj7X9-GtFqH-Vk_2_Duzx74KeuhWM1FG6YpglRWKV79FWGSENrOCoVVBrjZGvPxD1kNjQm8X-gByCPvgGLLOsNMlYdRq52BbbCtsyg");

			// url = new URL("https://api.spotify.com/v1/search?q=Muse&type=track");
			url = new URL("https://search-solr74.barclays.co.uk/content/bf/en/4_0/branches_atms");
			// url = new
			// URL("https://search.barclays.co.uk/content/bf/en/4_0/branches_atms?lat=51.4044448&lng=-0.2518595000000232&facilities=");

			// url.setDoOutput(true);
			// url.setRequestProperty("Content-Type", "application/json; charset=utf-8");

			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setConnectTimeout(1000000);
			connection.setReadTimeout(1000000);
			// connection.setRequestProperty("Authorization", "Bearer <MY_ACCESS_TOKEN>");
			// connection.setRequestProperty("Authorization", "Bearer
			// eyJhbGciOiJSUzI1NiIsImtpZCI6IlRJQUFfQVMtU2lnbiJ9.eyJhZGdyb3VwcyI6WyJDTj1HbG8tSW50LVNlYy1MZWdhY3lQYXNzd29yZFBvbGljeTEwLE9VPUFjdGl2ZURpcmVjdG9yeSxPVT1Qcml2aWxlZ2VkR3JvdXBzLE9VPV9JbmZyYXN0cnVjdHVyZSxEQz1JTlRSQU5FVCxEQz1CQVJDQVBJTlQsREM9Y29tIiwiQ049R0xPLUlOVC1TRUMtTm9uSW50ZXJhY3RpdmUsT1U9QWN0aXZlRGlyZWN0b3J5LE9VPVByaXZpbGVnZWRHcm91cHMsT1U9X0luZnJhc3RydWN0dXJlLERDPUlOVFJBTkVULERDPUJBUkNBUElOVCxEQz1jb20iLCJDTj1mR0xCSUJETlRFU1QsT1U9R3JvdXBzLE9VPUdMQixEQz1JTlRSQU5FVCxEQz1CQVJDQVBJTlQsREM9Y29tIiwiQ049ZkdMQlBydGdTSVRPRFNULE9VPUdyb3VwcyxPVT1HTEIsREM9SU5UUkFORVQsREM9QkFSQ0FQSU5ULERDPWNvbSJdLCJzdWIiOiJzeXNUSUFBQVNNQ0FBU1NJVCIsIlgtU1NMQ2xpZW50Q2VydFZhbGlkVXBUbyI6IjE2MzQxNjk1OTkwMDAiLCJYLVNTTENsaWVudENlcnRTQU4iOiJbMiwgVElBQS1DTElFTlRdIiwiWC1TU0xDbGllbnRDZXJ0RE4iOiJDTj1USUFBLUNMSUVOVCwgT1U9R0lTLCBPPUJhcmNsYXlzIFBMQywgTD1Mb25kb24sIFNUPUxvbmRvbiwgQz1HQiIsIlgtU1NMQ2xpZW50SXNzdWVyQ2VydEROIjoiQ049VGVzdCBHbG9iYWwgSW5mcmFzdHJ1Y3R1cmUgSW50ZXJtZWRpYXRlIENBICg3KSwgT1U9VGVzdCBHbG9iYWwgSW5mcmFzdHJ1Y3R1cmUgSW50ZXJtZWRpYXRlIENBICg3KSwgTz1CYXJjbGF5cyBQTEMiLCJleHAiOjE1NzU5MDExMDEsInNjb3BlIjpbXSwiY2xpZW50X2lkIjoiSUJETkNMSUVOVCIsImlzcyI6IlRJQUEtQVMiLCJhdWQiOiJJQkROIn0.cG6jrNXuTzAdiM6X50a_MiKJuc3vZwb7j_5TJ8o7zsXQAkF4qF2a_u1TgmqCycwqFcNnIHJrMpGVQw8ZU2SnqRBHOxP0AKd-79WIxE4XwWmhHQ-WR1ZoFuoosw7tTYPIDpRB5b4edBo6uM_UMW-3LHqyPEqmiGQcXZMmaiko-WTpJ4OnxQJGayYvLWH6ETVDkngsFiAPmfODoEnJ4i9o_WtqxhPOa5ztBCqOgaV6G6JjkN74oj7X9-GtFqH-Vk_2_Duzx74KeuhWM1FG6YpglRWKV79FWGSENrOCoVVBrjZGvPxD1kNjQm8X-gByCPvgGLLOsNMlYdRq52BbbCtsyg");
			String responseMessage = connection.getResponseMessage();
			BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			StringBuffer content = new StringBuffer();

			try (reader) {
				String line;

				while ((line = reader.readLine()) != null) {

					content.append(line);
					System.out.println(line);
				}
			} finally {
				reader.close();
			}
			System.out.println("Response: " + responseMessage);
			System.out.println("Content: " + content.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/*
	 * public static void main(String[] args) { HttpClient client =
	 * HttpClientBuilder.create().build(); HttpUriRequest httpUriRequest = new
	 * HttpGet("URL");
	 * 
	 * HttpResponse response = client.execute(httpUriRequest);
	 * System.out.println(response); }
	 */

	/*
	 * public static void main3(String args[]) {
	 * 
	 * final String uri =
	 * "https://search-solr74.barclays.co.uk/content/bf/en/4_0/branches_atms";
	 * 
	 * RestTemplate restTemplate = new RestTemplate();
	 * 
	 * ResponseEntity<String> response; try { response =
	 * restTemplate.getForEntity(uri + "/1", String.class); } catch (Exception e) {
	 * e.printStackTrace(); }
	 * 
	 * System.out.println(response);
	 * 
	 * }
	 */

	public static void main1(String[] args) {
		try {
			URL url = new URL(
					"https://search.barclays.co.uk/content/bf/en/4_0/branches_atms?lat=51.4044448&lng=-0.2518595000000232&facilities=");
			// URL url = new
			// URL("https://search-solr74.barclays.co.uk/content/bf/en/4_0/branches_atms");
			// URL url = new URL("https://stackoverflow.com/");
			URLConnection urlConnection = url.openConnection();
			DataInputStream dis = new DataInputStream(urlConnection.getInputStream());

			String inputLine;

			while ((inputLine = dis.readLine()) != null) {
				System.out.println(inputLine);
			}
			dis.close();
		} catch (MalformedURLException me) {
			System.out.println("MalformedURLException: " + me);
		} catch (IOException ioe) {
			System.out.println("IOException: " + ioe);
		}
	}

	public static void main2(String[] args) {
		// inline will store the JSON data streamed in string format
		String inline = "";

		try {
			// URL url = new
			// URL("http://maps.googleapis.com/maps/api/geocode/json?address=chicago&sensor=false&#8221");
			URL url = new URL(
					"https://search.barclays.co.uk/content/bf/en/4_0/branches_atms?lat=51.4044448&lng=-0.2518595000000232&facilities=");
			// URL url = new
			// URL("https://search-solr74.barclays.co.uk/content/bf/en/4_0/branches_atms");
			// Parse URL into HttpURLConnection in order to open the connection in order to
			// get the JSON data
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			// Set the request to GET or POST as per the requirements
			conn.setRequestMethod("GET");
			// Use the connect method to create the connection bridge
			conn.connect();
			// Get the response status of the Rest API
			int responsecode = conn.getResponseCode();
			System.out.println("Response code is: " + responsecode);

			// Iterating condition to if response code is not 200 then throw a runtime
			// exception
			// else continue the actual process of getting the JSON data
			if (responsecode != 200)
				throw new RuntimeException("HttpResponseCode: " + responsecode);
			else {
				// Scanner functionality will read the JSON data from the stream
				Scanner sc = new Scanner(url.openStream());
				while (sc.hasNext()) {
					inline += sc.nextLine();
				}
				System.out.println("\nJSON Response in String format");
				System.out.println(inline);
				// Close the stream when reading the data has been finished
				sc.close();
			}

			// JSONParser reads the data from string object and break each data into key
			// value pairs
			JSONParser parse = new JSONParser();
			// Type caste the parsed json data in json object
			JSONObject jobj = (JSONObject) parse.parse(inline);
			// Store the JSON object in JSON array as objects (For level 1 array element i.e
			// Results)
			JSONArray jsonarr_1 = (JSONArray) jobj.get("results");
			// Get data for Results array
			for (int i = 0; i < jsonarr_1.size(); i++) {
				// Store the JSON objects in an array
				// Get the index of the JSON object and print the values as per the index
				JSONObject jsonobj_1 = (JSONObject) jsonarr_1.get(i);
				// Store the JSON object in JSON array as objects (For level 2 array element i.e
				// Address Components)
				JSONArray jsonarr_2 = (JSONArray) jsonobj_1.get("address_components");
				System.out.println("Elements under results array");
				System.out.println("\nPlace id: " + jsonobj_1.get("place_id"));
				System.out.println("Types: " + jsonobj_1.get("types"));
				// Get data for the Address Components array
				System.out.println("Elements under address_components array");
				System.out.println("The long names, short names and types are:");
				for (int j = 0; j < jsonarr_2.size(); j++) {
					// Same just store the JSON objects in an array
					// Get the index of the JSON objects and print the values as per the index
					JSONObject jsonobj_2 = (JSONObject) jsonarr_2.get(j);
					// Store the data as String objects
					String str_data1 = (String) jsonobj_2.get("long_name");
					System.out.println(str_data1);
					String str_data2 = (String) jsonobj_2.get("short_name");
					System.out.println(str_data2);
					System.out.println(jsonobj_2.get("types"));
					System.out.println("\n");
				}

			}
			// Disconnect the HttpURLConnection stream
			conn.disconnect();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
