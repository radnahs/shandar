/**
 * <p>Project: rytry </p>
 * <p>Package Name: com.shandar.rytry.barclays </p>
 * <p>File Name: WebServiceClient.java </p>
 * <p>Create Date: 10-Dec-2019 </p>
 * <p>Create Time: 9:12:28 am </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.rytry.barclays;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

/**
 * @author : Shantanu Sikdar
 * @Description : WebServiceClient
 */
public class WebServiceClient {

	public static void main(String[] args) {
		// webServiceJavaClient("https://search.barclays.co.ukj/content/bf/en/4_0/branches_atms?lat=51.4044448&lng=-0.2518595000000232&facilities=","GET");
		//webServiceApacheClient("https://search.barclays.co.uk/content/bf/en/4_0/branches_atms?lat=51.4044448&lng=-0.2518595000000232&facilities=",	"GET");
		webServiceJerseyClient("https://search.barclays.co.uk/content/bf/en/4_0/branches_atms?lat=51.4044448&lng=-0.2518595000000232&facilities=",	"GET");
	}

	private static void webServiceJavaClient(String url, String httpMethod) {
		URL urlObj;
		HttpURLConnection httpURLConnection;
		try {
			urlObj = new URL(url);
			httpURLConnection = (HttpURLConnection) urlObj.openConnection();
			httpURLConnection.setRequestMethod(httpMethod);
			httpURLConnection.setRequestProperty("Accept", "application/json");
			httpURLConnection.setRequestProperty("Content-Type", "application/json");

			System.out.println("Http URL Connection Response Code : " + httpURLConnection.getResponseCode());

			if (httpURLConnection.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + httpURLConnection.getResponseCode());
			}

			BufferedReader bufferedReader = new BufferedReader(
					new InputStreamReader((httpURLConnection.getInputStream())));

			String output;
			System.out.println("Output from Server .... \n");
			while ((output = bufferedReader.readLine()) != null) {
				System.out.println(output);
			}

			httpURLConnection.disconnect();

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private static void webServiceApacheClient(String url, String httpMethod) {
		HttpClient client = new DefaultHttpClient();
		HttpGet request = new HttpGet(url);
		HttpResponse response;
		try {
			response = client.execute(request);
			System.out.println(response.getEntity().getContent());
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			String line = "";
			while ((line = rd.readLine()) != null) {
				System.out.println(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void webServiceJerseyClient(String url, String httpMethod) {
		ClientConfig config = new DefaultClientConfig();
		Client client = Client.create(config);
		WebResource service = client.resource(UriBuilder.fromUri(url).build());
		// getting XML data
		System.out.println(service.path("restPath").path("resourcePath").accept(MediaType.APPLICATION_JSON).get(String.class));
		// getting JSON data
		System.out.println(service.path("restPath").path("resourcePath").accept(MediaType.APPLICATION_XML).get(String.class));
	}

}
