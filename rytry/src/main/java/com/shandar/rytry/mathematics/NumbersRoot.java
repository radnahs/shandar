/**
 * <p>Project: rytry </p>
 * <p>Package Name: com.shandar.rytry.mathematics </p>
 * <p>File Name: SquareRoot.java </p>
 * <p>Create Date: 12-Feb-2020 </p>
 * <p>Create Time: 7:37:52 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.rytry.mathematics;

/**
 * @author : Shantanu Sikdar
 * @Description : SquareRoot
 */
public class NumbersRoot {

	public static void main(String[] args) {
		double dd = Math.sqrt(6.25);
		System.out.println(dd);
		double ddl = squareRoot(1024);
		System.out.println(ddl);
		System.out.println(square(ddl));
		System.out.println(cubeRoot(12, 0.000001));
	}

	public static double squareRoot(double number) {
		double temp;
		double sr = number / 2;
		do {
			temp = sr;
			sr = (temp + (number / temp)) / 2;
		} while ((temp - sr) != 0);

		return sr;
	}
	
	private static double square(double dd) {
		return dd * dd;
	}
	
	
	//not correct
	private static double cubeRoot(int dd, double precision) {
		int i = 1;
		for (i = 1; (i * i * i) <= dd; ++i)
			for (--i; (i * i * i) < dd; i += precision)
				System.out.println( "Cube root of "+dd +" the given number is " + i);
		
		return i;
	}

	

	private static double cube(double dd) {
		return dd * dd * dd;
	}

}
