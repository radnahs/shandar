/**
* Project: rytry
* Package Name:package com.shandar.rytry.mathematics;
* File Name: Combinations.java
* Create Date: Aug 23, 2016
* Create Time: 7:28:37 PM
* Copyright: Copyright (c) 2016
* @author: Shantanu Sikdar, ssikdar
* @version 1.0
*/
package com.shandar.rytry.mathematics;

import java.util.ArrayList;
import java.util.List;


//http://introcs.cs.princeton.edu/java/23recursion/Combinations.java.html

public class Combinations {

	//Start : for String single character
	 // print all subsets of the characters in s
    public static void combinationStringCharacter1(String s) { 
    	combinationStringCharacter1("", s); 
    }

    // print all subsets of the remaining elements, with given prefix 
    private static void combinationStringCharacter1(String prefix, String s) {
        if (s.length() > 0) {
            System.out.println(prefix + s.charAt(0));
            combinationStringCharacter1(prefix + s.charAt(0), s.substring(1));
            combinationStringCharacter1(prefix,s.substring(1));
        }
    }  
    
    // alternate implementation
    public static void combinationStringCharacter2(String s) { 
    	combinationStringCharacter2("", s); 
    }
    
    private static void combinationStringCharacter2(String prefix, String s) {
    	System.out.println(prefix);
        for (int i = 0; i < s.length(); i++)
            combinationStringCharacter2(prefix + s.charAt(i), s.substring(i + 1));
    }  


    // read in N from command line, and print all subsets among N elements
    public static void main(String[] args) {
        /*int n = Integer.parseInt("5");
        String alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        
        String elements = alphabet.substring(0, n);*/

        // using first implementation
        /*combinationStringCharacter1(elements);
        System.out.println();*/

        // using second implementation
        /*combinationStringCharacter2(elements);
        System.out.println();*/
    	
//    	System.out.println(nCr(5, 2));
    	//System.out.println(lstArr);
    	Integer[] intArr = {8,9,10,11};
    	List<List<Integer>> lstsInt = permutationsArrayIndexes(3, intArr.length); 
    	System.out.println(lstsInt);
    	//System.out.println(generateCombinations(4, Arrays.asList(intArr)));
    	//System.out.println(combinationArray(3, 2));
    	
    }
    

    //static List<Integer> lstArr = new  ArrayList<>();
    private static int nCr(int n, int r) {
    	System.out.print("n = " + n);
    	System.out.println(" r = " + r);
    	if (r == 0 || r == n)
    		return 1;  // stop recursion, we know the answer.
    	int p = nCr(n-1, r-1);
    	System.out.println(" p = "+p);
    	int q = nCr(n-1, r);
    	System.out.println(" q = "+q);
   	  	return p + q; // the answer is made of the sum of two "easier" ones
    }
    
    
    static private List<List<Integer>> permutationsArrayIndexes(int R, int N)  {
        int carry;
        int[] indices = new int[R];
        List<List<Integer>> lstsInt = new ArrayList<>();
        do {
        	List<Integer> lstInt = new ArrayList<>();
            for(int index : indices){
                lstInt.add(index);
            }
            carry = 1;
            for(int i = indices.length - 1; i >= 0; i--){
                if(carry == 0){
                    break;
                }
                indices[i] += carry;
                carry = 0;
                if(indices[i] == N) {
                    carry = 1;
                    indices[i] = 0;
                }
            }
            lstsInt.add(lstInt);
        }
        while(carry != 1); // Call this method iteratively until a carry is left over
        return lstsInt;
    }
    
    
    private static void combinationArray(int n, int r){
    	List<Integer> lstInt = new ArrayList<>();
    	for(int k = 0; k<n; ){
    	//.add();
    		k=k+r;
    	}
    }

    
    static private void generateCombinations(int arraySize, List<Integer> possibleValues)  {
        int carry;
        int[] indices = new int[arraySize];
        do {
            for(int index : indices){
                System.out.print(possibleValues.get(index) + " ");
            }
            System.out.println("");

            carry = 1;
            for(int i = indices.length - 1; i >= 0; i--){
                if(carry == 0){
                    break;
                }
                indices[i] += carry;
                carry = 0;

                if(indices[i] == possibleValues.size()) {
                    carry = 1;
                    indices[i] = 0;
                }
            }
        }
        while(carry != 1); // Call this method iteratively until a carry is left over
    }
    
}
