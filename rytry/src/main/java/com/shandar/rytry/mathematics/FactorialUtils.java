/**
* Project: rytry
* Package Name:package com.shandar.rytry.mathematics;
* File Name: FactorialUtils.java
* Create Date: Nov 17, 2016
* Create Time: 6:46:49 PM
* Copyright: Copyright (c) 2016
* @author: Shantanu Sikdar, ssikdar
* @version 1.0
*/
package com.shandar.rytry.mathematics;

public class FactorialUtils {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		FactorialUtils otb = new FactorialUtils();
		System.out.println(otb.factorial(4));
		System.out.println(otb.summation(5));

	}
	
	public int factorial(int m){
		if(m==1){
			return m;
		}
		return m*factorial(m-1);
	}
	
	public int summation(int m){
		if(m==1){
			return m;
		}
		return m+factorial(m-1);
	}

}
