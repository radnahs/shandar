/**
 * <p>Project: rytry </p>
 * <p>Package Name: com.shandar.rytry.flocareer </p>
 * <p>File Name: PatternType1.java </p>
 * <p>Create Date: 23-May-2022 </p>
 * <p>Create Time: 2:13:08 PM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.rytry.flocareer;

/**
 * @author : Shantanu Sikdar
 * @Description : PatternType1
 * 
 * Try to find the logic to print the pattern given below.
 * 		1
 * 		2	3
 * 		4	5	6
 * 		7	8	9	10
 * 
 */
public class PatternType1 {

	public static void main(String[] args) {
		printPattern(1);
	}

	private static void printPattern(int num){
		for(int i=1; i<5; i++) {
			for(int j=1; j<i;j++) {
				System.out.print(num);
				++num;
			}
			System.out.println();

		}
	}
}
