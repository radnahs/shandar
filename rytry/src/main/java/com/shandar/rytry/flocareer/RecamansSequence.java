/**
 * <p>Project: rytry </p>
 * <p>Package Name: com.shandar.rytry.flocareer </p>
 * <p>File Name: RecamansSequence.java </p>
 * <p>Create Date: 16-May-2022 </p>
 * <p>Create Time: 5:32:11 PM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.rytry.flocareer;

import java.util.HashSet;
import java.util.Set;

/**
 * @author : Shantanu Sikdar
 * @Description : RecamansSequence
 * 
 *              Recaman's sequence is defined as following:
 *              enter image description here
 * 
 *              Simplest way to explain the sequence is: "go back, and if you
 *              can't go back, go forward".
 * 
 *              Write a program to print first N number of Recaman's sequence.
 * 
 *              First few numbers would be:
 * 
 *              0, 1, 3, 6, 2, 7, 13, 20, 12, 21, 11, 22, 10, 23....
 * 
 *              Here's the Numberphile video about the same:
 *              www.youtube.com/watch?v=FGC5TdIiT9U
 * 
 *              Interesting Tidbits:
 * 
 *              Neil Sloane (creator of the On-Line Encyclopedia of Integer
 *              Sequences (OEIS)) has conjectured that every number eventually
 *              appears, but it has not been proved. Allan Wilks, Nov 06 2001,
 *              computed 10^15 terms of this sequence. At this point all the
 *              numbers below 852655 had appeared, but 852655 = 5315501 was
 *              missing. After 10^25 terms of A005132 the smallest missing
 *              number is still - Benjamin Chaffin, Jun 13 2006 Even after
 *              7.78*10^37 terms, the smallest missing number is still -
 *              Benjamin Chaffin, Mar 28 2008 Even after 4.28*10^73 terms, the
 *              smallest missing number is still - Benjamin Chaffin, Mar 22 2010
 *              Even after 10^230 terms, the smallest missing number is still
 *              852655. - Benjamin Chaffin, 2018
 * 
 *              Ideal Answer (5 star)
 * 
 *              MAX = 10
 * 
 *              seq = [] prev = 0 # seeding the sequence
 * 
 *              for i in range(MAX): if (prev - i) > 0 and (prev - i) not in
 *              seq: prev = prev - i else: prev = prev + i
 * 
 *              seq.append(prev) print prev
 */
public class RecamansSequence extends Object{

	/**
	 * @param args
	 */
	public static void main(String[] args) {
	}

}
