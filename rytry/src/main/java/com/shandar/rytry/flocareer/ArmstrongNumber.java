/**
 * <p>Project: rytry </p>
 * <p>Package Name: com.shandar.rytry.flocareer </p>
 * <p>File Name: ArmstrongNumber.java </p>
 * <p>Create Date: 10-May-2022 </p>
 * <p>Create Time: 1:27:55 PM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.rytry.flocareer;

/**
 * @author : Shantanu Sikdar
 * @Description : ArmstrongNumber
 * 
 *              If we input a random number, try to find the logic to check
 *              whether that number is an Armstrong or not. An Armstrong number
 *              is a 3 digit number for which sum of cube of its digits is equal
 *              to the number itself. 
 *              
 *              An example of Armstrong number is 
 *              153 as 153= 1+ 125+27 which is equal to 1^3+5^3+3^3.
 * 
 *              One more example of the Armstrong number is 
 *              371 because it is the sum of 27 + 343 + 1 which is equal to 3^3 + 7^3 + 1^3
 */
public class ArmstrongNumber {

	/**
	 * @param args
	 */
	public static void main1(String[] args) {
		System.out.println("Armstrong Number");
		System.out.println(isArmStrong(157));
		System.out.println(isArmStrong(153));
	}

	public static boolean isArmStrong(int number) {
		int result = 0;
		int orig = number;
		while (number != 0) {
			int remainder = number % 10;
			result = result + remainder * remainder * remainder;
			number = number / 10;
		}
		// number is Armstrong return true
		if (orig == result) {
			return true;
		}
		return false;
	}

	public static void main2(String args[]) {
		int a = 153;
		int sum = 0;
		int b = a;
		while (a > 0) {
			int digit = a % 10;
			sum = sum + digit * digit * digit;
			a = a / 10;
		}
		if (sum == b) {
			System.out.println("Yes, its an amstrong number");
		} else {
			System.out.println("No, its not an amstrong number");
		}
	}

	public static void main(String[] args) {
		int n = 153;
		int temp = n;
		int digit;
		int sum = 0;
		while (n > 0) {
			digit = n % 10;
			n = n / 10;
			sum = sum + n * n * n;
		}
		if (sum == temp)
			System.out.println("Armstrong number");
		else
			System.out.println("not an armstrong number");
	}

}
