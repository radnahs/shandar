/**
 * <p>Project: rytry </p>
 * <p>Package Name: com.shandar.rytry.flocareer </p>
 * <p>File Name: JosephusProblem.java </p>
 * <p>Create Date: 19-May-2022 </p>
 * <p>Create Time: 3:59:57 PM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.rytry.flocareer;

/**
 * @author : Shantanu Sikdar
 * @Description : JosephusProblem
 * 
 * There was a group of 41 Jewish soldiers surrounded by Roman army, 
 * and they didn't want to get caught. So, they sat down in a circle 
 * and came up with an algorithm. Everybody had a sword, and starting 
 * from person #1 in the circle, everybody will kill the next living 
 * person on the left. So, #1 will kill #2. #3 will kill #4, #5 will 
 * kill #6 and so on. The last living person will have to commit 
 * suicide to avoid getting caught by Romans.
 * 
 * The soldier called Josephus preferred to be caught over committing 
 * suicide. So, in the group of 41 soldiers, he chose the location 
 * where he will be the last person living.
 * 
 * Write a program to figure out, in a group of given N people, where 
 * should Josephus sit to live at the end of all internal killing.
 * 
 * There is a mathematical solution to this problem (check out 
 * www.youtube.com/watch?v=uCsD3ZGzMgE). But, your program should use the 
 * brute force method to find the position. The output of the program may 
 * look like this:
 * 
 * Solving Josephus problem for 5 soldiers:
 * 1 kills 2
 * 3 kills 4
 * 5 kills 1
 * 3 kills 5
 * 
 * Josephus should sit on position# 3
 * 
 */
public class JosephusProblem {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
