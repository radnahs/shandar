/**
 * <p>Project: rytry </p>
 * <p>Package Name: com.shandar.rytry.flocareer </p>
 * <p>File Name: RepeatedCharacterInString.java </p>
 * <p>Create Date: 23-May-2022 </p>
 * <p>Create Time: 2:39:50 PM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.rytry.flocareer;

/**
 * @author : Shantanu Sikdar
 * @Description : RepeatedCharacterInString
 * 
 * Find the first repeated character in a string. 
 * For example, if input is "john doe", output would be "o"
 * 
 */
public class RepeatedCharacterInString {

	public static void main(String[] args) {

	}

	private static char firstRepeatedChar(String str){
		char[] chrArr = str.toCharArray();
		
		return ' ' ;
		
	}
}
