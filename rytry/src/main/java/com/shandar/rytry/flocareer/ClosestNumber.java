/**
 * <p>Project: rytry </p>
 * <p>Package Name: com.shandar.rytry.flocareer </p>
 * <p>File Name: ClosestNumber.java </p>
 * <p>Create Date: 18-May-2022 </p>
 * <p>Create Time: 5:40:10 PM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.rytry.flocareer;

/**
 * @author : Shantanu Sikdar
 * @Description :ClosestNumber Given an array of sorted integers and find the
 *               closest value to the given number. The array may contain
 *               duplicate values and negative numbers.
 * 
 *               Example : Array : 2,5,6,7,8,8,9 Target number : 5 Output : 5
 *  
 *               Target number : 11 Output : 9
 * 
 *               Target Number : 4 Output : 5
 */

public class ClosestNumber {

	public static void main(String[] args) {
		System.out.println(closestNumbers(new int[]{2, 8, 6, 8, 7, 5, 9}, 4));
	}

	static int closestNumbers(int a[], int n) {
		int diff = Math.abs(a[0] - n);
		int retVal = a[0];
		for (int i = 1; i < a.length; i++) {
			int newDiff = Math.abs(a[i] - n);
			if (diff > newDiff) {
				diff = newDiff;
				retVal = a[i];
			}
		}
		return retVal;
	}

	int getClosestNumber(int a[], int n) {
		int len = a.length;
		if (n < a[0])
			return a[0];
		else if (n > a[len - 1])
			return a[len - 1];
		for (int i = 0; i < a.length; i++) {

		}
		return 0;
	}

	public static void mainPallaviGupta(String args[]) {
		int a[] = { 2, 5, 6, 7, 8, 8, 9 };
		int search = 4;
		for (int i = 0; i < a.length; i++) {
			if (search == a[i]) {
				System.out.println(a[i]);
			} else if (i < a.length - 1) {
				if (search < a[i] && search > a[i + 1]) {
					int leftMin = search - a[i];
					int rightMin = a[i + 1] - search;
					if (leftMin < rightMin) {
						System.out.println(leftMin);
					} else
						System.out.println(rightMin);
				}
			}
		}
	}

}
