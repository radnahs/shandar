/**
 * <p>Project: rytry </p>
 * <p>Package Name: com.shandar.rytry.flocareer </p>
 * <p>File Name: ReverseString.java </p>
 * <p>Create Date: 20-Oct-2022 </p>
 * <p>Create Time: 3:04:52 PM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.rytry.flocareer;

import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author : Shantanu Sikdar
 * @Description : ReverseString
 */
public class ReverseString {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println(reverse("Shantanu Sikdar"));
		System.out.println(reverse("Reverse a String using Java 8 features"));

	}

	public static String reverse(String string) {
        return Stream.of(string)
            .map(word->new StringBuilder(word).reverse())
            .collect(Collectors.joining(" "));
    }
	
	
}
