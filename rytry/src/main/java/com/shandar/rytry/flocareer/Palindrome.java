/**
 * <p>Project: rytry </p>
 * <p>Package Name: com.shandar.rytry.flocareer </p>
 * <p>File Name: Palindrome.java </p>
 * <p>Create Date: 26-May-2022 </p>
 * <p>Create Time: 2:33:43 PM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.rytry.flocareer;

/**
 * @author : Shantanu Sikdar
 * @Description : Palindrome
 */
public class Palindrome {

	public static void main(String[] args) {
		System.out.println(isPalindromeLokeshOracle(""));
	}

	static boolean isPalindromeLokeshOracle(String str) {
		int i = 0, j = str.length() - 1;

		while (i < j) {
			if (str.charAt(i) != str.charAt(j))
				return false;
			i++;
			j--;
		}

		return true;
	}

}
