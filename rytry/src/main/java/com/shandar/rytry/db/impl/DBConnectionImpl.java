/**
 *
 * <p>Project: rytry </p>
 * <p>Package Name: com.shandar.rytry.db.impl </p>
 * <p>File Name: DBConnectionImpl.java</p>
 * <p>Create Date: Mar 6, 2013 </p>
 * <p>Create Time: 3:45:34 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.db.impl;

import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import com.shandar.rytry.db.DBConnection;

public class DBConnectionImpl implements DBConnection {
	
	private DataSource dataSourceOracle;	
	private DataSource dataSourceOracleTest;
	private DataSource dataSourceOracleTestNR;
	private DataSource dataSourceOracleQA;
	private DataSource dataSourceOracleQaNR;
	
	private JdbcTemplate jdbcTemplate;
	
	public void setDataSourceOracle(DataSource dataSource) {
		this.dataSourceOracle = dataSource;
		this.jdbcTemplate = new JdbcTemplate(this.dataSourceOracle); 
	}
	
	public void setDataSourceOracleTest(DataSource dataSource) {
		this.dataSourceOracleTest = dataSource;
		this.jdbcTemplate = new JdbcTemplate(this.dataSourceOracleTest); 
	}
	
	public void setDataSourceOracleTestNR(DataSource dataSource) {
		this.dataSourceOracleTestNR = dataSource;
		this.jdbcTemplate = new JdbcTemplate(this.dataSourceOracleTestNR); 
	}
	
	public void setDataSourceOracleQA(DataSource dataSource) {
		this.dataSourceOracleQA = dataSource;
		this.jdbcTemplate = new JdbcTemplate(this.dataSourceOracleQA); 
	}
	
	public void setDataSourceOracleQaNR(DataSource dataSource) {
		this.dataSourceOracleQaNR = dataSource;
		this.jdbcTemplate = new JdbcTemplate(this.dataSourceOracleQaNR); 
	}
	
	
	@Override
	public void dbQueryInsert(String insertSQL) {
		// TODO Auto-generated method stub		
	}
	
	@Override
	public List<Map<String, Object>> dbQueryRead(String sql){
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
		return rows;
	}
	
	@Override
	public List<Map<String, Object>> dbQueryRead(String sql,Object[] args){
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql, args);
		return rows;
	}

	@Override
	public void dbQueryUpdate() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dbQueryDelete() {
		// TODO Auto-generated method stub
		
	}

}
