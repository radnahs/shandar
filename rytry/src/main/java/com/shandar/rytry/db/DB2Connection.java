/**
 * Project: rytry
 * Package Name:package com.shandar.rytry.db;
 * File Name: DB2Connection.java
 * Create Date: Feb 16, 2017
 * Create Time: 4:19:33 PM
 * Copyright: Copyright (c) 2016
 * @author: Shantanu Sikdar, ssikdar
 * @version 1.0
 */
package com.shandar.rytry.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DB2Connection {

	public static void main(String[] args) {
		String urlPrefix = "jdbc:db2:";
		
		//Connection con=DriverManager.getConnection("jdbc:db2://SCH020OSA.SEARS.COM:1313/DB90","scdosqa","rt53kp7s");//given by priya lal @Sears 
		String url = "jdbc:db2://SCH020OSA.SEARS.COM:1313/DB90";
		String user = "scdosqa";
		String password = "rt53kp7s";
		String soNo;
		Connection con;
		Statement stmt;
		ResultSet rs;

		System.out.println("**** Enter class EzJava");

		try {
			// Load the driver
			Class.forName("com.ibm.db2.jcc.DB2Driver");
			System.out.println("**** Loaded the JDBC driver");

			// Create the connection using the IBM Data Server Driver for JDBC
			// and SQLJ
			con = DriverManager.getConnection(url, user, password);
			// Commit changes manually
			con.setAutoCommit(false);
			System.out.println("**** Created a JDBC connection to the data source");

			// Create the Statement
			stmt = con.createStatement();
			System.out.println("**** Created JDBC Statement object");

			// Execute a query and generate a ResultSet instance
			//rs = stmt.executeQuery("SELECT EMPNO FROM EMPLOYEE");
			rs = stmt.executeQuery("select * from DBTOME1.OKORTOH");
			System.out.println("**** Created JDBC ResultSet object");

			// Print all of the employee numbers to standard output device
			while (rs.next()) {
				soNo = rs.getString(1);
				System.out.println("Employee number = " + soNo);
			}
			System.out.println("**** Fetched all rows from JDBC ResultSet");
			// Close the ResultSet
			rs.close();
			System.out.println("**** Closed JDBC ResultSet");

			// Close the Statement
			stmt.close();
			System.out.println("**** Closed JDBC Statement");

			// Connection must be on a unit-of-work boundary to allow close
			con.commit();
			System.out.println("**** Transaction committed");

			// Close the connection
			con.close();
			System.out.println("**** Disconnected from data source");
			System.out.println("**** JDBC Exit from class EzJava - no errors");

		}

		catch (ClassNotFoundException e) {
			System.err.println("Could not load JDBC driver");
			System.out.println("Exception: " + e);
			e.printStackTrace();
		}

		catch (SQLException ex) {
			System.err.println("SQLException information");
			while (ex != null) {
				System.err.println("Error msg: " + ex.getMessage());
				System.err.println("SQLSTATE: " + ex.getSQLState());
				System.err.println("Error code: " + ex.getErrorCode());
				ex.printStackTrace();
				ex = ex.getNextException(); // For drivers that support chained
											// exceptions
			}
		}
	} // End main
}
