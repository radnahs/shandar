package com.shandar.rytry.db.impl;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;

import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.shandar.rytry.db.QueryResultSetMapper;

public class QueryResultSetMapperImpl<T> implements QueryResultSetMapper{
	
	private static final Logger logger = LoggerFactory.getLogger(QueryResultSetMapperImpl.class);

	/**
	 * 
	 */
	@SuppressWarnings("uncehecked")
	@Override
	public List<T> mapRersultSetToObject(List<Map<String, Object>> mappedDataClassList, Class outputClass){
		List<T> outputList = null;
		try {
			// make sure mappedDataClassList is not null
			if (mappedDataClassList != null) {
				// check if outputClass has 'Entity' annotation
				if (outputClass.isAnnotationPresent(Entity.class)) {					
					// get all the attributes of outputClass
					Field[] fields = outputClass.getDeclaredFields();
					for (Map<String, Object> mappedData: mappedDataClassList) {
						T bean = (T) outputClass.newInstance();
						for(String columnName : mappedData.keySet()){
							// getting the SQL column name							
							// reading the value of the SQL column
							Object columnValue = mappedData.get(columnName);
							// iterating over outputClass attributes to check if any attribute has 'Column' annotation with matching 'name' value
							for (Field field : fields) {
								if (field.isAnnotationPresent(Column.class)) {
									Column column = field.getAnnotation(Column.class);
									if (column.name().equalsIgnoreCase(columnName) && columnValue != null) {
										BeanUtils.setProperty(bean, field.getName(), columnValue);
										break;
									}
								}
							}
						}
						if (outputList == null) {
							outputList = new ArrayList<T>();
						}
						outputList.add(bean);
					}
				} else {
					// throw some error
				}
			} else {
				return null;
			}
		} catch (IllegalAccessException iae) {
			logger.error(iae.getMessage());			
		}catch (InstantiationException inse) {
			logger.error(inse.getMessage());
		} catch (InvocationTargetException invce) {
			logger.error(invce.getMessage());
		}
		return outputList;	
	}
	
	/**
	 * 
	 * @param resultSet
	 * @param outputClass
	 * @return
	 */
	@SuppressWarnings("uncehecked")	
	public List<T> mapRersultSetToObject(ResultSet resultSet, Class outputClass) {
		List<T> outputList = null;
		try {
			// make sure resultset is not null
			if (resultSet != null) {
				// check if outputClass has 'Entity' annotation
				if (outputClass.isAnnotationPresent(Entity.class)) {
					// get the resultset metadata
					ResultSetMetaData rsmd = resultSet.getMetaData();
					// get all the attributes of outputClass
					Field[] fields = outputClass.getDeclaredFields();
					while (resultSet.next()) {
						T bean = (T) outputClass.newInstance();
						for (int _iterator = 0; _iterator < rsmd.getColumnCount(); _iterator++) {
							// getting the SQL column name
							String columnName = rsmd.getColumnName(_iterator + 1);
							// reading the value of the SQL column
							Object columnValue = resultSet.getObject(_iterator + 1);
							// iterating over outputClass attributes to check if any attribute has 'Column' annotation with matching 'name' value
							for (Field field : fields) {
								if (field.isAnnotationPresent(Column.class)) {
									Column column = field.getAnnotation(Column.class);
									if (column.name().equalsIgnoreCase(columnName) && columnValue != null) {
										BeanUtils.setProperty(bean, field.getName(), columnValue);
										break;
									}
								}
							}
						}
						if (outputList == null) {
							outputList = new ArrayList<T>();
						}
						outputList.add(bean);
					}

				} else {
					// throw some error
				}
			} else {
				return null;
			}
		} catch (IllegalAccessException iae) {
			logger.error(iae.getMessage());
		} catch (SQLException inste) {
			logger.error(inste.getMessage());
		} catch (InstantiationException inste) {
			logger.error(inste.getMessage());
		} catch (InvocationTargetException invcte) {			
			logger.error(invcte.getMessage());
		}
		return outputList;
	}
	
}
