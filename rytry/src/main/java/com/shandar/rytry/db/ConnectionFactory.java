/**
 *
 * <p>Project: trunk_rytry </p>
 * <p>Package Name: com.shandar.rytry.db </p>
 * <p>File Name: ConnectionFactory.java</p>
 * <p>Create Date: Jul 1, 2013 </p>
 * <p>Create Time: 7:06:13 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.db;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.shandar.rytry.db.impl.DBConnectionImpl;
import com.shandar.rytry.db.impl.DBConnectionMySQLImpl;

/**
 * @author Shantanu Sikdar
 * 
 */
public class ConnectionFactory {

	ApplicationContext appContxt;
	
	private static ConnectionFactory connectionFactory = null;

	private ConnectionFactory() {
		appContxt = new ClassPathXmlApplicationContext("jdbc-context.xml");
	}

	public DBConnection getConnectionMySQL() {
		DBConnection dbConnMySQL = (DBConnectionMySQLImpl) appContxt.getBean("dbConnMySQL");
		return dbConnMySQL;
	}
	
	public DBConnection getConnectionOracle() {
		DBConnection dbConnOracle = (DBConnectionImpl) appContxt.getBean("dbConnOracle");
		return dbConnOracle;
	}
	
	public DBConnection getConnectionOracleTest() {
		DBConnection dbConnOracleTest = (DBConnectionImpl) appContxt.getBean("dbConnOracleTest");
		return dbConnOracleTest;
	}
	
	public DBConnection getConnectionOracleTestNR() {
		DBConnection dbConnOracleTestNR = (DBConnectionImpl) appContxt.getBean("dbConnOracleTestNR");
		return dbConnOracleTestNR;
	}
	
	
	public DBConnection getConnectionOracleQA() {
		DBConnection dbConnOracleQA = (DBConnectionImpl) appContxt.getBean("dbConnOracleQA");
		return dbConnOracleQA;
	}
	
	
	public DBConnection getConnectionOracleQaNR() {
		DBConnection dbConnOracleQaNR = (DBConnectionImpl) appContxt.getBean("dbConnOracleQaNR");
		return dbConnOracleQaNR;
	}
	
	
	public static ConnectionFactory getInstance() {
		if (connectionFactory == null) {
			connectionFactory = new ConnectionFactory();
		}
		return connectionFactory;
	}

}
