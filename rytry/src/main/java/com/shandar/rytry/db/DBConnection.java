/**
 *
 * <p>Project: rytry </p>
 * <p>Package Name: com.shandar.rytry.db.impl </p>
 * <p>File Name: DBCOnnection.java</p>
 * <p>Create Date: Mar 6, 2013 </p>
 * <p>Create Time: 3:51:21 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.db;

import java.util.List;
import java.util.Map;

/**
 * @author Shantanu Sikdar 
 *
 */
public interface DBConnection {
	
	public void dbQueryInsert(String insertSQL);
	
	public List<Map<String, Object>> dbQueryRead(String sql);
	
	public List<Map<String, Object>> dbQueryRead(String sql, Object[] args);
	
	public void dbQueryUpdate();
	
	public void dbQueryDelete();	

}
