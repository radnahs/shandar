package com.shandar.rytry.db.impl;

import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import com.shandar.rytry.db.DBConnection;

public class DBConnectionMySQLImpl implements DBConnection {
	
	private DataSource dataSourceMySQL;	
	private JdbcTemplate jdbcTemplate;
	
	public void setDataSourceMySQL(DataSource dataSource) {
		this.dataSourceMySQL = dataSource;
		this.jdbcTemplate = new JdbcTemplate(this.dataSourceMySQL); 
	}
	

	@Override
	public void dbQueryInsert(String insertSQL) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public List<Map<String, Object>> dbQueryRead(String sql){
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
		return rows;
	}
	
	@Override
	public List<Map<String, Object>> dbQueryRead(String sql,Object[] args){
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql, args);
		return rows;
	}

	@Override
	public void dbQueryUpdate() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dbQueryDelete() {
		// TODO Auto-generated method stub
		
	}

}
