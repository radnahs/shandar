/**
 *
 * <p>Project: rytry </p>
 * <p>Package Name: com.shandar.rytry.db </p>
 * <p>File Name: DBConnection.java</p>
 * <p>Create Date: Mar 6, 2013 </p>
 * <p>Create Time: 3:40:34 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.db.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.dbcp.PoolingDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.shandar.rytry.db.DBConnection;

/**
 * @author Shantanu Sikdar 
 *
 */
public class MySqlDBConnection implements DBConnection {
	
	private static final Logger logger = LoggerFactory.getLogger(MySqlDBConnection.class);
	
	private DataSource dataSource;
	private PoolingDataSource pooledDS;
	
	public void setPooledDS(PoolingDataSource pooledDS) {
		this.pooledDS = pooledDS;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public void dbQueryInsert(String insertSQL) {
		Connection connection =  null;
		try{
			connection = dataSource.getConnection();
			PreparedStatement pstmt = connection.prepareStatement(insertSQL);
			
		}catch(SQLException sqle) {
			throw new RuntimeException(sqle);
		}finally{
			if(connection!=null){
				try{
					connection.close();
				}catch (SQLException sqle) {
					
				}
			}
		}
		
	}

	
	@Override
	public void dbQueryUpdate() {

	}

	@Override
	public void dbQueryDelete() {
		
	}

	@Override
	public List<Map<String, Object>> dbQueryRead(String sql) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Map<String, Object>> dbQueryRead(String sql, Object[] args) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
