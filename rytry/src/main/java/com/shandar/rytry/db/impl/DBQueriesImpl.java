package com.shandar.rytry.db.impl;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.shandar.rytry.db.DBQueries;
import com.shandar.rytry.db.constants.QueryConstants;


public class DBQueriesImpl implements DBQueries {
	
	private static final Logger logger = LoggerFactory.getLogger(DBQueriesImpl.class);
		
	private ApplicationContext queriesCtx ;
	
	/**
	 * Loading the queryContext xml
	 * @param queryContext
	 */
	public DBQueriesImpl(String queryContext) {
		queriesCtx = new ClassPathXmlApplicationContext(queryContext);
	}
	
	/**
	 * @Desc: Reading from the loaded application context and getting the query-map, .  
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Map<String, String> getQueryBeanFromFactory(String beanId){
		Map<String, String> queryMap = null;
		if (queriesCtx != null && beanId != null) {
			queryMap = (Map<String, String>)queriesCtx.getBean(beanId);
		}
		return queryMap;
	}
	
	/**
	 * @Desc: Getting the query-map, .  
	 */	
	@Override
	public String getQuery(Map<String, String> queryMap) {
		String query=null;
		try{
			if(queryMap.containsKey(QueryConstants.QUERY_NODE)){
				query = (String) queryMap.get(QueryConstants.QUERY_NODE);
				queryMap.remove(QueryConstants.QUERY_NODE);
			}else{
				throw new NoSuchFieldError();
			}
		}catch(Exception excp){
			excp.printStackTrace();
		}
		return query;
	}
	
	
	/*public static void main(String[] args) {
		System.out.println("hello spring123");
		DBQueriesImpl dbqi = new DBQueriesImpl("userDAO-queries.xml");
		Map<String, String> qMap = dbqi.getQueryBeanFromFactory("FIND_ALL_USER");
		System.out.println("query == "+dbqi.getQuery(qMap));
		System.out.println("query params123 == "+qMap);		
		for (String string : qMap.keySet()) {
			System.out.println("key = "+string+" || value = " + qMap.get(string));			
		}
	}*/
	
	/*facebook.com/help/?page=419
	twitip.com/twitter-security-dos-and-donts
	itworld.com/internet/64798/linkedin-privacy-settings-what-you-need-know
	youtube.com/t/privacy_at_youtube?hl=en*/
	
}
