package com.shandar.rytry.db;

import java.util.Map;


public interface DBQueries {
	
	public Map<String, String> getQueryBeanFromFactory(String beanId);
		
	public String getQuery(Map<String, String> queryMap);
	
}
