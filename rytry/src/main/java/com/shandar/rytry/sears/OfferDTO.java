package com.shandar.rytry.sears;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The DTO class which holds the offer reference data.
 *
 * @author INFOSYS
 * @version 20140820 TELL-17005 ksuryan Removed cloning implementation
 */
public class OfferDTO  {

	private static final long serialVersionUID = 5247394889333831643L;

	private long memberOfferPointsEarned;
	private long memberOfferTimesEarned;
	private long pointEarned;
	private long timesEarned;
	private BigInteger pointsRedeemed = BigInteger.ZERO;
	private BigInteger timesRedeemed = BigInteger.ZERO;
	private String repeatInd;
	private long maxRepeats;
	private Date pointAvailableEndDate;
	private Date pointAvailableStartDate;
	private Date effectiveDateEnd;
	private double grandTenderTotal;
	private double grandTaxTotal;
	private double grandSVTenderTotal;
	private long getMaxRepeats;
	private Double txnOfferPoints;
	private Double txnOfferTimes;
	private Double qualifiedAmountTowardOfer;
	private Double qualifiedQtyTowardOfer;
	private boolean ignoreMemberTimesPointsEarned = false;
	private Map<String, Set<Map<String, Object>>> getOfferMatchedFactMap;
	private BigInteger memberOfferTimesRedeemed = BigInteger.ZERO;
	private BigInteger memberOfferPointsRedeemed = BigInteger.ZERO;
	private BigInteger storeOfferTimesRedeemed = BigInteger.ZERO;
	private BigInteger storeOfferPointsRedeemed = BigInteger.ZERO;
	private boolean isEarnableBuyInsideGet;
	private boolean fromExchange;
	private boolean conditional = false;
	private String rewardBasis;
	private boolean hasEarnPoints;
	private boolean capReached = false;
	private int allItemsRedemptionExcluded;
	private boolean maxLimitExceeded = false;
	private boolean newDurationCycle;
	private int priority;
	private List<Map<String, Object>> qualifiedLineItems;
	private double qualifiedAmt;
	private String offerType;
	private boolean isSplitPurchaseTransaction = false;
	private List<String> storeFormat;
	private boolean isNewMemberOfferEntryRedemption = false;
	private Date startTrackDate;
	private boolean isMemberOfferHistory;
	private String alternateOfferId;
	private boolean isAlternateOffer;
	private boolean isLineLevelOffer;
	private List<String> storeItemActions;
	private boolean isLinePoints;	
	private Map<String,String> groupActionsMap;
	private float minValue;
	private List<Object> matchedAttributes;
	private List<String> storeItemGroups;
	private Long minTimesEarnable;
	
	// Constructors
	public OfferDTO() {
		super();
	}

	public long getMemberOfferPointsEarned() {
		return memberOfferPointsEarned;
	}

	public void setMemberOfferPointsEarned(long memberOfferPointsEarned) {
		this.memberOfferPointsEarned = memberOfferPointsEarned;
	}

	public long getMemberOfferTimesEarned() {
		return memberOfferTimesEarned;
	}

	public void setMemberOfferTimesEarned(long memberOfferTimesEarned) {
		this.memberOfferTimesEarned = memberOfferTimesEarned;
	}

	public long getPointEarned() {
		return pointEarned;
	}

	public void setPointEarned(long pointEarned) {
		this.pointEarned = pointEarned;
	}

	public long getTimesEarned() {
		return timesEarned;
	}

	public void setTimesEarned(long timesEarned) {
		this.timesEarned = timesEarned;
	}

	public BigInteger getPointsRedeemed() {
		return pointsRedeemed;
	}

	public void setPointsRedeemed(BigInteger pointsRedeemed) {
		this.pointsRedeemed = pointsRedeemed;
	}

	public BigInteger getTimesRedeemed() {
		return timesRedeemed;
	}

	public void setTimesRedeemed(BigInteger timesRedeemed) {
		this.timesRedeemed = timesRedeemed;
	}

	public String getRepeatInd() {
		return repeatInd;
	}

	public void setRepeatInd(String repeatInd) {
		this.repeatInd = repeatInd;
	}

	public long getMaxRepeats() {
		return maxRepeats;
	}

	public void setMaxRepeats(long maxRepeats) {
		this.maxRepeats = maxRepeats;
	}

	public Date getPointAvailableEndDate() {
		return pointAvailableEndDate;
	}

	public void setPointAvailableEndDate(Date pointAvailableEndDate) {
		this.pointAvailableEndDate = pointAvailableEndDate;
	}

	public Date getPointAvailableStartDate() {
		return pointAvailableStartDate;
	}

	public void setPointAvailableStartDate(Date pointAvailableStartDate) {
		this.pointAvailableStartDate = pointAvailableStartDate;
	}

	public Date getEffectiveDateEnd() {
		return effectiveDateEnd;
	}

	public void setEffectiveDateEnd(Date effectiveDateEnd) {
		this.effectiveDateEnd = effectiveDateEnd;
	}

	public double getGrandTenderTotal() {
		return grandTenderTotal;
	}

	public void setGrandTenderTotal(double grandTenderTotal) {
		this.grandTenderTotal = grandTenderTotal;
	}

	public double getGrandTaxTotal() {
		return grandTaxTotal;
	}

	public void setGrandTaxTotal(double grandTaxTotal) {
		this.grandTaxTotal = grandTaxTotal;
	}

	public double getGrandSVTenderTotal() {
		return grandSVTenderTotal;
	}

	public void setGrandSVTenderTotal(double grandSVTenderTotal) {
		this.grandSVTenderTotal = grandSVTenderTotal;
	}

	public long getGetMaxRepeats() {
		return getMaxRepeats;
	}

	public void setGetMaxRepeats(long getMaxRepeats) {
		this.getMaxRepeats = getMaxRepeats;
	}

	public Double getTxnOfferPoints() {
		return txnOfferPoints;
	}

	public void setTxnOfferPoints(Double txnOfferPoints) {
		this.txnOfferPoints = txnOfferPoints;
	}

	public Double getTxnOfferTimes() {
		return txnOfferTimes;
	}

	public void setTxnOfferTimes(Double txnOfferTimes) {
		this.txnOfferTimes = txnOfferTimes;
	}

	public Double getQualifiedAmountTowardOfer() {
		return qualifiedAmountTowardOfer;
	}

	public void setQualifiedAmountTowardOfer(Double qualifiedAmountTowardOfer) {
		this.qualifiedAmountTowardOfer = qualifiedAmountTowardOfer;
	}

	public Double getQualifiedQtyTowardOfer() {
		return qualifiedQtyTowardOfer;
	}

	public void setQualifiedQtyTowardOfer(Double qualifiedQtyTowardOfer) {
		this.qualifiedQtyTowardOfer = qualifiedQtyTowardOfer;
	}

	public boolean isIgnoreMemberTimesPointsEarned() {
		return ignoreMemberTimesPointsEarned;
	}

	public void setIgnoreMemberTimesPointsEarned(
			boolean ignoreMemberTimesPointsEarned) {
		this.ignoreMemberTimesPointsEarned = ignoreMemberTimesPointsEarned;
	}

	public Map<String, Set<Map<String, Object>>> getGetOfferMatchedFactMap() {
		return getOfferMatchedFactMap;
	}

	public void setGetOfferMatchedFactMap(
			Map<String, Set<Map<String, Object>>> getOfferMatchedFactMap) {
		this.getOfferMatchedFactMap = getOfferMatchedFactMap;
	}

	public BigInteger getMemberOfferTimesRedeemed() {
		return memberOfferTimesRedeemed;
	}

	public void setMemberOfferTimesRedeemed(BigInteger memberOfferTimesRedeemed) {
		this.memberOfferTimesRedeemed = memberOfferTimesRedeemed;
	}

	public BigInteger getMemberOfferPointsRedeemed() {
		return memberOfferPointsRedeemed;
	}

	public void setMemberOfferPointsRedeemed(BigInteger memberOfferPointsRedeemed) {
		this.memberOfferPointsRedeemed = memberOfferPointsRedeemed;
	}

	public BigInteger getStoreOfferTimesRedeemed() {
		return storeOfferTimesRedeemed;
	}

	public void setStoreOfferTimesRedeemed(BigInteger storeOfferTimesRedeemed) {
		this.storeOfferTimesRedeemed = storeOfferTimesRedeemed;
	}

	public BigInteger getStoreOfferPointsRedeemed() {
		return storeOfferPointsRedeemed;
	}

	public void setStoreOfferPointsRedeemed(BigInteger storeOfferPointsRedeemed) {
		this.storeOfferPointsRedeemed = storeOfferPointsRedeemed;
	}

	public boolean isEarnableBuyInsideGet() {
		return isEarnableBuyInsideGet;
	}

	public void setEarnableBuyInsideGet(boolean isEarnableBuyInsideGet) {
		this.isEarnableBuyInsideGet = isEarnableBuyInsideGet;
	}

	public boolean isFromExchange() {
		return fromExchange;
	}

	public void setFromExchange(boolean fromExchange) {
		this.fromExchange = fromExchange;
	}

	public boolean isConditional() {
		return conditional;
	}

	public void setConditional(boolean conditional) {
		this.conditional = conditional;
	}

	public String getRewardBasis() {
		return rewardBasis;
	}

	public void setRewardBasis(String rewardBasis) {
		this.rewardBasis = rewardBasis;
	}

	public boolean isHasEarnPoints() {
		return hasEarnPoints;
	}

	public void setHasEarnPoints(boolean hasEarnPoints) {
		this.hasEarnPoints = hasEarnPoints;
	}

	public boolean isCapReached() {
		return capReached;
	}

	public void setCapReached(boolean capReached) {
		this.capReached = capReached;
	}

	public int getAllItemsRedemptionExcluded() {
		return allItemsRedemptionExcluded;
	}

	public void setAllItemsRedemptionExcluded(int allItemsRedemptionExcluded) {
		this.allItemsRedemptionExcluded = allItemsRedemptionExcluded;
	}

	public boolean isMaxLimitExceeded() {
		return maxLimitExceeded;
	}

	public void setMaxLimitExceeded(boolean maxLimitExceeded) {
		this.maxLimitExceeded = maxLimitExceeded;
	}

	public boolean isNewDurationCycle() {
		return newDurationCycle;
	}

	public void setNewDurationCycle(boolean newDurationCycle) {
		this.newDurationCycle = newDurationCycle;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public List<Map<String, Object>> getQualifiedLineItems() {
		return qualifiedLineItems;
	}

	public void setQualifiedLineItems(List<Map<String, Object>> qualifiedLineItems) {
		this.qualifiedLineItems = qualifiedLineItems;
	}

	public double getQualifiedAmt() {
		return qualifiedAmt;
	}

	public void setQualifiedAmt(double qualifiedAmt) {
		this.qualifiedAmt = qualifiedAmt;
	}

	public String getOfferType() {
		return offerType;
	}

	public void setOfferType(String offerType) {
		this.offerType = offerType;
	}

	public boolean isSplitPurchaseTransaction() {
		return isSplitPurchaseTransaction;
	}

	public void setSplitPurchaseTransaction(boolean isSplitPurchaseTransaction) {
		this.isSplitPurchaseTransaction = isSplitPurchaseTransaction;
	}

	public List<String> getStoreFormat() {
		return storeFormat;
	}

	public void setStoreFormat(List<String> storeFormat) {
		this.storeFormat = storeFormat;
	}

	public boolean isNewMemberOfferEntryRedemption() {
		return isNewMemberOfferEntryRedemption;
	}

	public void setNewMemberOfferEntryRedemption(
			boolean isNewMemberOfferEntryRedemption) {
		this.isNewMemberOfferEntryRedemption = isNewMemberOfferEntryRedemption;
	}

	public Date getStartTrackDate() {
		return startTrackDate;
	}

	public void setStartTrackDate(Date startTrackDate) {
		this.startTrackDate = startTrackDate;
	}

	public boolean isMemberOfferHistory() {
		return isMemberOfferHistory;
	}

	public void setMemberOfferHistory(boolean isMemberOfferHistory) {
		this.isMemberOfferHistory = isMemberOfferHistory;
	}

	public String getAlternateOfferId() {
		return alternateOfferId;
	}

	public void setAlternateOfferId(String alternateOfferId) {
		this.alternateOfferId = alternateOfferId;
	}

	public boolean isAlternateOffer() {
		return isAlternateOffer;
	}

	public void setAlternateOffer(boolean isAlternateOffer) {
		this.isAlternateOffer = isAlternateOffer;
	}

	public boolean isLineLevelOffer() {
		return isLineLevelOffer;
	}

	public void setLineLevelOffer(boolean isLineLevelOffer) {
		this.isLineLevelOffer = isLineLevelOffer;
	}

	public List<String> getStoreItemActions() {
		return storeItemActions;
	}

	public void setStoreItemActions(List<String> storeItemActions) {
		this.storeItemActions = storeItemActions;
	}

	public boolean isLinePoints() {
		return isLinePoints;
	}

	public void setLinePoints(boolean isLinePoints) {
		this.isLinePoints = isLinePoints;
	}

	public Map<String, String> getGroupActionsMap() {
		return groupActionsMap;
	}

	public void setGroupActionsMap(Map<String, String> groupActionsMap) {
		this.groupActionsMap = groupActionsMap;
	}

	public float getMinValue() {
		return minValue;
	}

	public void setMinValue(float minValue) {
		this.minValue = minValue;
	}

	public List<Object> getMatchedAttributes() {
		return matchedAttributes;
	}

	public void setMatchedAttributes(List<Object> matchedAttributes) {
		this.matchedAttributes = matchedAttributes;
	}

	public List<String> getStoreItemGroups() {
		return storeItemGroups;
	}

	public void setStoreItemGroups(List<String> storeItemGroups) {
		this.storeItemGroups = storeItemGroups;
	}

	public Long getMinTimesEarnable() {
		return minTimesEarnable;
	}

	public void setMinTimesEarnable(Long minTimesEarnable) {
		this.minTimesEarnable = minTimesEarnable;
	}

}
