package com.shandar.rytry.sears;

import java.math.BigInteger;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

public interface OfferDTOIntf {
	
	public String getOfferId();
	
	public String getOfferCode();
	
	public String getOfferTypeCode();
	
	public String getOfferSubTypeCode();
	
	public String getOfferName();
	
	public String getStatus();	
	
	public String getOfferDescription();
	
	public Date getEffectiveDateStart();
	
	public Time getEffectiveTimeStart();
	
	public Date getEffectiveDateEnd();
	
	public Time getEffectiveTimeEnd();
	
	public String getMultiTransactionInd();
	
	public String getPointAvailableStartDateInd();
	
	public String getPointAvailableEndDateInd();
	
	public String getIncludeExcludeInd();
	
	public String getReturnInd();
	
	public boolean isSeperateInd();
	
	public Date getPointAvailableStartDate();
	
	public Date getPointAvailableEndDate();
	
	public int getPointAvailableStartAfterTxn();
	
	public int getPointAvailableEndAfterTxn();
	
	public String getPointAvailableAfterTxnUnit();
	
	public long getMaxTimesEarnable();
	
	public long getMaxPointsEarnable();
	
	public long getMaxTimesEarnablePerMember() ;
	
	public long getMaxPointsEarnablePerMember();
	
	public String getFlatPointsInd();
	
	public String getQualificationRuleId();
	
	public String getEarnRuleId();
	
	public String getHasCoupon();
	
	public String getIsDailyOfferInd();
	
	public long getMaxRepeats();
	
	public long getMaxRepeatPoint();
	
	public String getRangeBaseAttributeType();
	
	public long getPointEarned();
	
	public long getTimesEarned();
		
	public BigInteger getMaxTimesRedeemablePerOffer();
	
	public BigInteger getMaxPointsRedeemablePerOffer();
	
	public BigInteger getMaxTimesRedeemablePerMember();
	
	public BigInteger getMaxPointsRedeemablePerMember();
	
	public BigInteger getMaxTimesRedeemablePerStore();
	
	public BigInteger getMaxPointsRedeemablePerStore();
	
	public BigInteger getPointsRedeemed();
	
	public BigInteger getTimesRedeemed();
	
	public int getRedeemPriority();
	
	public String getBuyGetOfferType();
	
	public boolean isQualifyOnly();
	
	public boolean isSameTranInd();
	
	public String getOfferReceiptDescription();
	
	public String getOfferConditions();
	
	public long getMemberOfferTimesEarned();
	
	public long getMemberOfferPointsEarned();
	
	public String getHasExcludeCoupon();
	
	public String getSurprisePointsExclusion();
	
	public String getImageURL();
	
	public String getOnlineContent();
	
	public String getMapOfferName();
	
	public String getProgramType();
	
	public String getProgramSubType();
	
	public long getMaxPointsEarnableDuration();
	
	public String getOnlineURL();
	
	public long getMaxTimesEarnablePerStore();
	
	public long getMaxPointsEarnablePerStore();
	
	public double getSpendThreshold();
	
	public String getSameDayTripInd();
	
	public String getProgressiveInd();
	
	public double getItemThreshold();
	
	public long getInclusionOffersMaxCap();
	
	public String getPreQualifyInd();
	
	public String getNotPreQualifyInd();

	public String getOfferCategory();

	public String getLegalese();	
	
	//public RulesTreeCore getEarnRuleTreeCore();
	
	//public RulesTreeCore getQualRuleTreeCore();
		
	//public List<RuleAction> getRuleActions();	
	
	public List<String> getLinkedBuyOfferList();	
	
	public List<String> getLinkedGetOfferList();
		
	//public Map<String, GetActionDTO> getOfferGetActionMap();
		
	public Integer getRepeatSeqCap();
		
	public List<String> getCouponNumbers();
		
	public List<String> getInclusionOfferIdList();	
	
	public String getSET();	
	
	public boolean isItemLevelOffer();
	
	public Timestamp getUpdateTs();
	
	public List<String> getMemberGroups(); 
	
	public List<String> getRestrictedOfferIdList();
	
	public String getOfferReceiptDescriptionXR();
	
	//public List<com.sears.telluride.rulesengine.dto.RuleAction> getRuleActionList();

	//public com.sears.telluride.rulesengine.dto.RulesTree getQualificationRuleTree();

	//public com.sears.telluride.rulesengine.dto.RulesTree getEarnTreeRule();
	
	public String getActivationType();
	
	public boolean isMemberSpecificOffer();
	
	public List<String> getProductGroupList();
	
	//public Map<String, List<PriceActionDTO>> getPricingActions();
	
	public String getOfferDealType();
	
	List<String> getStoreProductGroupList();
	
	String getPriceIncreaseInd();
	
	//ThresholdDTO getThresholdDTO();
	
	public double getPriorityOrder();
	
	public boolean isIncludeSVTender();


}
