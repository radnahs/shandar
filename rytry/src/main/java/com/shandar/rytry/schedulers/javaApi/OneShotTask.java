package com.shandar.rytry.utils.schedule;

import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

public class OneShotTask {
	
	private final Timer timer = new Timer();
    private final int minutes;

    public OneShotTask(int minutes) {
        this.minutes = minutes;
    }

    public void start() {
    	System.out.println("1 Now == " + Calendar.getInstance().getTime());          
        timer.schedule(new TimerTask() {
            public void run() {
            	System.out.println("2 Now == " + Calendar.getInstance().getTime());
                playSound();
                timer.cancel();
            }
            private void playSound() {
            	System.out.println("3 Now == " + Calendar.getInstance().getTime());
                System.out.println("Your OneShotTask is ready!");                
            }
        }, minutes * 60 * 1000);
    }

    public static void main(String[] args) {
    	OneShotTask oneShotTask = new OneShotTask(1);
        oneShotTask.start();
    }
}
