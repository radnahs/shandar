package com.shandar.rytry.utils.schedule;

import java.util.Date;

public interface ScheduleIterator {
	public Date next();
}
