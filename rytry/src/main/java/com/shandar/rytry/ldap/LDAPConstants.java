/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.ldap </p>
 * <p>File Name: LDAPConstants.java</p>
 * <p>Create Date: Oct 6, 2014 </p>
 * <p>Create Time: 5:29:32 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.ldap;

/**
 * @author Shantanu Sikdar 
 *
 */
public class LDAPConstants {
	
	public static final String LDAP_SAM_ACCOUNT_NAME = "sAMAccountName";
	public static final String LDAP_GIVEN_NAME = "givenName";
	public static final String LDAP_SUR_NAME = "sn";
	public static final String LDAP_USER_PRINCIPAL_NAME = "userPrincipalName";
	public static final String LDAP_USER_EMAIL = "mail";

	public static final String LDAP_COMMON_NAME = "cn";
	public static final String LDAP_DOMAIN_COMPONENT = "dc";
	public static final String LDAP_HOME_PHONE = "homePhone";
	public static final String LDAP_MOBILE = "mobile";
	public static final String LDAP_ORGANIZATION_NAME = "o";
	public static final String LDAP_ORGANIZATION_UNIT_NAME = "ou";

	public static final String SEARCH_BY_USER_NAME = "1";
	public static final String SEARCH_BY_GIVEN_NAME = "2";
	public static final String SEARCH_BY_SUR_NAME = "3";
	public static final String SEARCH_BY_EMAIL = "4";

	public static final String INITIAL_CONTEXT_FACTORY = "com.sun.jndi.ldap.LdapCtxFactory";
	public static final String SECURITY_AUTHENTICATION = "simple";
	
	
	//Keys 
	public static final String LDAP_SERVER_URL = "LdapServerURL";
	public static final String LDAP_SECURITY_PRINCIPAL = "LdapSecurityPrincipal";
}
