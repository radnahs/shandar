package com.shandar.rytry.ldap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.naming.AuthenticationException;
import javax.naming.CommunicationException;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.shandar.rytry.common.dataObject.SimpleDataObject;
import com.shandar.rytry.common.utils.CommonUtils;

public class LDAPQueryProcessor {
	
	private static final Logger logger = LoggerFactory.getLogger(LDAPQueryProcessor.class);
	
	/**
	 * Try to initiate InitialLdapContext with given LDAP Server with given
	 * username and password.
	 * 
	 * @param serverDO
	 * @param userName
	 * @param password
	 * @return
	 * @throws CommunicationException
	 */
	public boolean isValidLDAPUser(SimpleDataObject serverDO, String userName, String password) throws CommunicationException {
		boolean isValid = false;
		
		 /* The method InitialLdapContext don't throw AuthenticationException if
		 * username and password is blank so it is checked here
		 */
		 
		if (CommonUtils.isBlankOrNull(userName) || CommonUtils.isBlankOrNull(password)) {
			return isValid;
		}
		try {
			getInitialLdapContext(serverDO, userName, password);
			isValid = true;
		} catch (CommunicationException e) {
			throw e;
		} catch (NamingException e) {
			logger.error("Error while validating user", e);
		}
		return isValid;
	}

	/**
	 * This method gives you LdapContext initialized, may throw NamingException
	 * if it is not able to create LdapContext
	 * 
	 * @param serverDO
	 * @param userName
	 * @param password
	 * @return
	 * @throws NamingException
	 */
	private LdapContext getInitialLdapContext(SimpleDataObject serverDO, String userName, String password) throws NamingException {
		Map<String, String> env = getLDAPEnv(serverDO, userName, password);
		Hashtable<String, String> ht = new Hashtable<String, String>();
		ht.putAll(env);
		LdapContext ctx = null;
		try {
			ctx = new InitialLdapContext( ht, null);
		} catch (AuthenticationException e) {
			String securityPrincipal = getModifiedSecurityPrincipal(serverDO.getAttribute(LDAPConstants.LDAP_SECURITY_PRINCIPAL).toString());
			env.put(Context.SECURITY_PRINCIPAL, userName + "@" + securityPrincipal);
			ctx = new InitialLdapContext(ht, null);
		}
		return ctx;
	}

	/**
	 * If normal security principal don't work this methods gives to different
	 * security principal in username@domain.com format
	 * 
	 * @param securityPrincipal
	 * @return
	 */
	private String getModifiedSecurityPrincipal(String securityPrincipal) {
		String userPrinicipalName = "";
		try {
			int index = 0;
			while (index <= securityPrincipal.length()) {
				index = securityPrincipal.indexOf("=", index);
				if (index > 0) {
					int beginIndex = index + 1;
					index = securityPrincipal.indexOf(",", index);
					if (index == -1) {
						index = securityPrincipal.length();
					}
					int lastIndex = index;
					userPrinicipalName = userPrinicipalName + (userPrinicipalName.length() > 0 ? "." : "") + securityPrincipal.substring(beginIndex, lastIndex).trim();
				} else {
					break;
				}
			}
		} catch (Exception e) {
			logger.error("Error in creating modified security principal", e);
		}
		return userPrinicipalName;
	}

	/**
	 * This methods takes LDAPServerData, username and password and returns a
	 * HashMap of environment for LDAP
	 * 
	 * @param serverDO
	 * @param userName
	 * @param password
	 * @return
	 */
	private Map<String, String> getLDAPEnv(SimpleDataObject serverDO, String userName, String password) {
		Map<String, String> env = new HashMap<String, String>();
		env.put(Context.INITIAL_CONTEXT_FACTORY, LDAPConstants.INITIAL_CONTEXT_FACTORY);
		env.put(Context.PROVIDER_URL, serverDO.getAttribute(LDAPConstants.LDAP_SERVER_URL).toString());
		// set security credentials, note using simple cleartext authentication
		String adminName = "CN=" + userName + ",CN=Users";
		if (!CommonUtils.isBlankOrNull(serverDO.getAttribute(LDAPConstants.LDAP_SECURITY_PRINCIPAL).toString())) {
			adminName += "," + serverDO.getAttribute(LDAPConstants.LDAP_SECURITY_PRINCIPAL).toString();
		}
		env.put(Context.SECURITY_AUTHENTICATION, LDAPConstants.SECURITY_AUTHENTICATION);
		env.put(Context.SECURITY_PRINCIPAL, adminName);
		env.put(Context.SECURITY_CREDENTIALS, password);
		return env;
	}

	/**
	 * This methods serach the given searchString in given list of ldap servers
	 * and returns a list of LDAPUserData
	 * 
	 * @param ldapServers
	 * @param searchBy
	 * @param serarchString
	 * @param userName
	 * @param password
	 * @return
	 */
	public ArrayList<SimpleDataObject> getUsersList(ArrayList<SimpleDataObject> ldapServers, String searchBy, String serarchString, String userName, String password) {
		ArrayList<SimpleDataObject> users = new ArrayList<SimpleDataObject>();
		try {
			for (int i = 0; i < ldapServers.size(); i++) {
				try {
					SimpleDataObject serverDO = ldapServers.get(i);
					LdapContext ctx = getInitialLdapContext(serverDO, userName, password);

					SearchControls searchCtls = new SearchControls();
					searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
					// specify the LDAP search filter
					String searchFilter = "(objectClass=user)";
					if (!CommonUtils.isBlankOrNull(searchBy)) {
						if (searchBy.equalsIgnoreCase(LDAPConstants.SEARCH_BY_USER_NAME))
							searchFilter = "(&(objectClass=user)(" + LDAPConstants.LDAP_USER_EMAIL + "=*)(" + LDAPConstants.LDAP_GIVEN_NAME + "=*)(" + LDAPConstants.LDAP_SAM_ACCOUNT_NAME + "=" + serarchString + "*))";
						else if (searchBy.equalsIgnoreCase(LDAPConstants.SEARCH_BY_GIVEN_NAME))
							searchFilter = "(&(objectClass=user)(" + LDAPConstants.LDAP_GIVEN_NAME + "=" + serarchString + "))";
						else if (searchBy.equalsIgnoreCase(LDAPConstants.SEARCH_BY_SUR_NAME))
							searchFilter = "(&(objectClass=user)(" + LDAPConstants.LDAP_SUR_NAME + "=" + serarchString + "))";
						else if (searchBy.equalsIgnoreCase(LDAPConstants.SEARCH_BY_EMAIL))
							searchFilter = "(&(objectClass=user)(" + LDAPConstants.LDAP_USER_EMAIL + "=" + serarchString + "))";
					}

					String searchBase = serverDO.getAttribute(LDAPConstants.LDAP_SECURITY_PRINCIPAL).toString();
					int totalResults = 0;

					// Specify the attributes to return
					String returnedAtts[] = { LDAPConstants.LDAP_COMMON_NAME, LDAPConstants.LDAP_DOMAIN_COMPONENT, LDAPConstants.LDAP_GIVEN_NAME, LDAPConstants.LDAP_HOME_PHONE,
							LDAPConstants.LDAP_MOBILE, LDAPConstants.LDAP_ORGANIZATION_NAME, LDAPConstants.LDAP_ORGANIZATION_UNIT_NAME, LDAPConstants.LDAP_SAM_ACCOUNT_NAME,
							LDAPConstants.LDAP_SUR_NAME, LDAPConstants.LDAP_USER_PRINCIPAL_NAME, LDAPConstants.LDAP_USER_EMAIL };
					searchCtls.setReturningAttributes(returnedAtts);

					// Search for objects using the filter
					NamingEnumeration answer = ctx.search(searchBase, searchFilter, searchCtls);
					// Collection userList = new User().getUsersNames();
					while (answer.hasMoreElements()) {
						SearchResult sr = (SearchResult) answer.next();
						// Print out the groups
						Attributes attrs = sr.getAttributes();
						SimpleDataObject ldapUserData = new SimpleDataObject();
						if (attrs != null) {
							try {
								for (NamingEnumeration ae = attrs.getAll(); ae.hasMore();) {
									Attribute attr = (Attribute) ae.next();
									for (NamingEnumeration e = attr.getAll(); e.hasMore(); totalResults++) {
										String id = attr.getID();
										String value = (String) e.next();
										ldapUserData.setAttribute(id, value);
									}
								}
							} catch (NamingException e) {
								logger.error("ERROR", e);
							}
						}
						users.add(ldapUserData);
					}
				} catch (Exception e) {
					logger.error("GlobalConstants.ERROR", e);
				}
			}
		} catch (Exception e) {
			logger.error("GlobalConstants.ERROR", e);
		}
		return users;
	}
	
	

	public static void main(String[] args) {
		SimpleDataObject sdo = new SimpleDataObject();
		sdo.setAttribute(LDAPConstants.LDAP_SERVER_URL, "ldap://10.10.1.11:389");		
		sdo.setAttribute(LDAPConstants.LDAP_SECURITY_PRINCIPAL,"dc=talentica-all,dc=com");
		LDAPQueryProcessor qry = new LDAPQueryProcessor();
		try {
			ArrayList<SimpleDataObject> ldapServers = new ArrayList<SimpleDataObject>();
			ldapServers.add(sdo);
//			LDAPServerData sdo1 = new LDAPServerData();
//			sdo1.setServerURL("ldap://10.10.1.1:389");
//			sdo1.setSecurityPrincipal("dc=talentica,dc=com");
//			ldapServers.add(sdo1);
			qry.getUsersList(ldapServers, LDAPConstants.SEARCH_BY_USER_NAME, "xbbkngi", "shivprasad", "shiv3@d");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

}
