/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.common.files </p>
 * <p>File Name: FilesAndFolderUtil.java</p>
 * <p>Create Date: Mar 4, 2013 </p>
 * <p>Create Time: 11:46:00 AM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.common.files.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.filechooser.FileSystemView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.shandar.rytry.common.dataObject.SimpleDataObject;
import com.shandar.rytry.common.files.ServerFilesAndFolder;

/**
 * @author Shantanu Sikdar
 * 
 */
public class WindowsFilesAndFolder implements ServerFilesAndFolder{
	
	private static final Logger logger = LoggerFactory.getLogger(WindowsFilesAndFolder.class);
	
	public List<SimpleDataObject> listDrives(){
		File[] paths;
		List<SimpleDataObject> listDrives = new ArrayList<SimpleDataObject>();
		SimpleDataObject sdo;
		try {
			FileSystemView fsv = FileSystemView.getFileSystemView();
			paths = File.listRoots();// returns pathnames for files and directory
			// for each pathname in pathname array
			for (File path : paths) {
				sdo = new SimpleDataObject();				
				sdo.setAttribute("driveName", path);				
				sdo.setAttribute("driveDescription", fsv.getSystemTypeDescription(path));
				listDrives.add(sdo);
			}
			/*for (SimpleDataObject simpleDataObject : listDrives) {
				System.out.println("2 driveName = "+simpleDataObject.getAttribute("driveName"));
				System.out.println("2 driveDescription = "+simpleDataObject.getAttribute("driveDescription"));
			}*/
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		return listDrives;
	}
	
	public List<SimpleDataObject> listSubFoldersAndFiles(File directory) {
		File dir_list[] = directory.listFiles();
		List<SimpleDataObject> listSubs = new ArrayList<SimpleDataObject>();
		SimpleDataObject sdo ;
		try {
			for (File fl : dir_list) {
				sdo = new SimpleDataObject();
				sdo.setAttribute("absolutePath", fl.getAbsolutePath());
				System.out.println(fl.getAbsolutePath());
				listSubs.add(sdo);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		return listSubs;		
	}

	public void listFiles(File f) {
		if (f.isFile()) {
			System.out.println(f.getAbsolutePath()); // displays path for a file
			return;
		} else {
			File dir_list[] = f.listFiles(); // lists all contents if a directory
			for (File t : dir_list)
				try {
					listFiles(t); // calls list for each content of directory
				} catch (Exception e) {
					e.printStackTrace();
				}
		}
	}

	public void listDirectoriesRecursively(File directory) {
		File dir_list[] = directory.listFiles(); // lists all contents if a
													// directory
		if (directory.isFile()) {
			return;
		}
		for (File t : dir_list)
			try {
				if (t.isDirectory())
					System.out.println(t.getAbsolutePath());
				listDirectoriesRecursively(t); // calls list for each content of directory
			} catch (Exception e) {
				e.printStackTrace();
			}
	}
	
	public static void main(String[] args) {
		WindowsFilesAndFolder wff= new WindowsFilesAndFolder();
		//wff.listDrives();
		//System.out.println(wff.listDrives());
/*		File[] paths;
		FileSystemView fsv = FileSystemView.getFileSystemView();

		// returns pathnames for files and directory
		paths = File.listRoots();

		// for each pathname in pathname array
		for (File path : paths) {
			// prints file and directory paths
			System.out.println("Drive Name: " + path);
			System.out.println("Description: "	+ fsv.getSystemTypeDescription(path));
			// listFiles(path);
			// listDirectoriesRecursively(path);
			new WindowsFilesAndFolder().listSubFoldersAndFiles(path);
			System.out.println("\n\n");
		}*/
		new WindowsFilesAndFolder().listSubFoldersAndFiles(new File("D:\\Study"));
		//new WindowsFilesAndFolder().listSubFoldersAndFiles(new File("P:\\KIS\\Corporate Systems\\Projects\\2007\\CMA Hardlines\\CMA Release 4.13\\Functional Design"));
		//new WindowsFilesAndFolder().listSubFoldersAndFiles(new File("P:\\KIS\\Corporate Systems\\Projects\\2007"));
		//cceapp301p.stress.ch3.s.com/appl/scinventory-dcu/dce/logs
		//new WindowsFilesAndFolder().listSubFoldersAndFiles(new File("cceapp301p.stress.ch3.s.com/appl/scinventory-dcu/dce/logs"));// for Priya
	}

}
