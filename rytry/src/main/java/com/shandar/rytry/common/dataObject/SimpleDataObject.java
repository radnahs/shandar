/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.common.dataObject </p>
 * <p>File Name: SimpleDataObject.java</p>
 * <p>Create Date: Feb 10, 2014 </p>
 * <p>Create Time: 1:12:28 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.common.dataObject;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Shantanu Sikdar 
 *
 */
public class SimpleDataObject implements IDataObject, Serializable {
		
	private static final long serialVersionUID = -1988720082851735586L;
	
	Map<String, Object> values;

	/**
	 * 
	 */
	public SimpleDataObject() {
		values = new HashMap<String, Object>();		
	}

	/* (non-Javadoc)
	 * @see com.shandar.rytry.common.dataObject.IDataObject#getAttribute(java.lang.String)
	 */
	@Override
	public Object getAttribute(String key) {		
		return values.get(key);
	}

	/* (non-Javadoc)
	 * @see com.shandar.rytry.common.dataObject.IDataObject#setAttribute(java.lang.String, java.lang.Object)
	 */
	@Override
	public void setAttribute(String key, Object value) {
		values.put(key, value);
	}

	/* (non-Javadoc)
	 * @see com.shandar.rytry.common.dataObject.IDataObject#getAttributes()
	 */
	@Override
	public Map<String, Object> getAttributes() {
		return values;
	}

	/* (non-Javadoc)
	 * @see com.shandar.rytry.common.dataObject.IDataObject#setAttributes(java.util.Map)
	 */
	@Override
	public void setAttributes(Map<String, Object> attributes) {
		values = attributes;		
	}

	@Override
	public String toString() {
		return "SimpleDataObject [values=" + values + "]";
	}
	

}
