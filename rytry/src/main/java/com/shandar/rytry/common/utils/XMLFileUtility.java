/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.common.utils </p>
 * <p>File Name: WriteXMLFile.java</p>
 * <p>Create Date: Jun 15, 2015 </p>
 * <p>Create Time: 8:19:49 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.common.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * @author Shantanu Sikdar
 * 
 */
public class XMLFileUtility {
	
	public enum EmployeeConstants{
		Id("Id","Id","")
			{@Override	public String dataValue(Employee empl) {return empl.getId(); } },	
		Name("Name","Name","")
			{@Override	public String dataValue(Employee empl) {return empl.getName(); } },	
		MName("MName","MName","")
			{@Override	public String dataValue(Employee empl) {return empl.getMname(); } };
		
		private final String key;
		private final String columnHeader;
		private final String constantValue;
		EmployeeConstants(String key, String columnHeader, String value) {
			this.key = key;
			this.columnHeader = columnHeader;
			this.constantValue = value;
		}
		public abstract String dataValue(Employee empl);
		public String cnKey()   { return key; }
		public String columnHeader() { return columnHeader; }
		public String cnValue() { return constantValue; }
	}

			
	public Element xmlElementEmployee(EmployeeContainer emplC, Document doc){		
		Element parentElemnt = doc.createElement("EmployeeContainer");
		List<Employee> employeeLst = emplC.getEmployee();		
		for (Employee valEmpl : employeeLst) {
			Element subParentElemnt = doc.createElement("Employee");			
			for(EmployeeConstants t : EmployeeConstants.values()){
				Element nodeName = doc.createElement(t.cnKey());
				nodeName.appendChild(doc.createTextNode(ifNullGiveBlankString(t.dataValue(valEmpl))));
				subParentElemnt.appendChild(nodeName);
			}
			parentElemnt.appendChild(subParentElemnt);
		}
		return parentElemnt;
	}
	
	public void buildEmployeeXMLData(EmployeeContainer emplC){		
		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			Document doc = docBuilder.newDocument();
			Element rootElement = doc.createElement("rootNode");
			doc.appendChild(rootElement);
			
			Element ePRIDC = xmlElementEmployee(emplC, doc);
			rootElement.appendChild(ePRIDC);
					
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			
			StreamResult result = new StreamResult(new File("C:\\data\\Empl.xml"));
			transformer.transform(source, result);
			System.out.println("File saved!");
			
		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		}
	}
	
	public void xmlFileGenerate() {
		try {

			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			// root elements
			Document doc = docBuilder.newDocument();
			Element rootElement = doc.createElement("rootNode");
			doc.appendChild(rootElement);

			// package elements
			Element pkgElement = doc.createElement("packageNode");
			rootElement.appendChild(pkgElement);

			// set attribute to package element
			Attr attr = doc.createAttribute("id");
			attr.setValue("1");
			pkgElement.setAttributeNode(attr);

			// shorten way
			// staff.setAttribute("id", "1");

			// firstname elements
			Element firstname = doc.createElement("firstname");
			firstname.appendChild(doc.createTextNode("Shantanu"));
			pkgElement.appendChild(firstname);

			// lastname elements
			Element lastname = doc.createElement("lastname");
			lastname.appendChild(doc.createTextNode("sikdar"));
			pkgElement.appendChild(lastname);

			// nickname elements
			Element nickname = doc.createElement("commit id");
			nickname.appendChild(doc.createTextNode("xbbkngi"));
			pkgElement.appendChild(nickname);

			// salary elements
			Element salary = doc.createElement("salary");
			salary.appendChild(doc.createTextNode("5000"));
			pkgElement.appendChild(salary);

			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File("C:\\data\\demoXML.xml"));

			// Output to console for testing
			// StreamResult result = new StreamResult(System.out);

			transformer.transform(source, result);

			System.out.println("File saved!");

		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		}
	}
	
	public static String ifNullGiveBlankString(String str){
		return str==null?"":str;
	}
	
	public static void main(String[] args) {
		XMLFileUtility xmlFU = new XMLFileUtility();
		//xmlFU.xmlFileGenerate();
		EmployeeContainer emplC = new EmployeeContainer(); 
		List<Employee> lstEmp =  new ArrayList<Employee>();
		Employee empl = new Employee(); 
		empl.setId("123");
		empl.setName("one");
		lstEmp.add(empl);
		Employee empl1 = new Employee();
		empl1.setId("1235");
		empl1.setName("five");
		lstEmp.add(empl1);
		Employee empl2 = new Employee();
		empl2.setId("1234");
		empl2.setName("four");
		lstEmp.add(empl2);
		
		emplC.setEmployee(lstEmp);
		
		xmlFU.buildEmployeeXMLData(emplC);
	}
}

class EmployeeContainer{
	List<Employee> employee;

	public List<Employee> getEmployee() {
		return employee;
	}

	public void setEmployee(List<Employee> employee) {
		this.employee = employee;
	}
	
}

class Employee{
	String id;
	String name;
	String mname;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMname() {
		return mname;
	}
	public void setMname(String mname) {
		this.mname = mname;
	}		
	
}
