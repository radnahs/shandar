/**
 *
 * <p>Project: trunk_rytry </p>
 * <p>Package Name: com.shandar.rytry.common.constants </p>
 * <p>File Name: CommonConstants.java</p>
 * <p>Create Date: Jul 11, 2013 </p>
 * <p>Create Time: 3:55:57 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.common.constants;


/**
 * @author Shantanu Sikdar 
 *
 */
public class CommonConstants {
	
	public static final int EXCEL_ROW_ZERO = 0;
	public static final int EXCEL_ROW_ONE = 1;
	public static final int EXCEL_ROW_TWO = 2;
	public static final int EXCEL_ROW_THREE = 3;
	
	public static void main(String[] args) {
		System.out.println("hello");
	}
	
}
