/**
 *
 * <p>Project: trunk_rytry </p>
 * <p>Package Name: com.shandar.rytry.common.utils </p>
 * <p>File Name: CommonUtils.java</p>
 * <p>Create Date: Jul 11, 2013 </p>
 * <p>Create Time: 4:06:35 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.common.utils;

import java.util.Collections;

/**
 * @author Shantanu Sikdar 
 *
 */
public class CommonUtils {

	/**
	 * To check if a string is blank or null.
	 * Following is the condition 
	 * s == null || "".equals(s.trim()) || "null".equals(s.trim()) 
	 * @param s
	 * @return
	 */
	public static boolean isBlankOrNull(String s) {
		return (s == null || "".equals(s.trim()) || "null".equals(s.trim()));
	}
	
	
	/**
	 * Null check in an enhanced for loop.
	 * 
	 * @usage for (Object object : emptyIfNull(someList)) { ... }
	 * 
	 * @param iterable
	 * @return
	 */	
	public static <T> Iterable<T> emptyIfNull(Iterable<T> iterable) {
	    return iterable == null ? Collections.<T>emptyList() : iterable;
	}
	
}
