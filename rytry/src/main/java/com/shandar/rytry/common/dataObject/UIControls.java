/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.common.dataObject </p>
 * <p>File Name: UIControls.java</p>
 * <p>Create Date: Apr 21, 2015 </p>
 * <p>Create Time: 8:36:54 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.common.dataObject;

/**
 * @author Shantanu Sikdar 
 *
 */
public class UIControls {
	
	private String controlInput1;
	private String controlRadioButton1;
	private String controlCheckBox1;
	
	public String getControlInput1() {
		return controlInput1;
	}
	public void setControlInput1(String controlInput1) {
		this.controlInput1 = controlInput1;
	}
	public String getControlRadioButton1() {
		return controlRadioButton1;
	}
	public void setControlRadioButton1(String controlRadioButton1) {
		this.controlRadioButton1 = controlRadioButton1;
	}
	public String getControlCheckBox1() {
		return controlCheckBox1;
	}
	public void setControlCheckBox1(String controlCheckBox1) {
		this.controlCheckBox1 = controlCheckBox1;
	}
	
	@Override
	public String toString() {
		return "UIControls [controlInput1=" + controlInput1
				+ ", controlRadioButton1=" + controlRadioButton1
				+ ", controlCheckBox1=" + controlCheckBox1 + "]";
	}
	
}
