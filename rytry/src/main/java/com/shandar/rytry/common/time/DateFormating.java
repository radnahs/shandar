/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.utils.dates </p>
 * <p>File Name: DateFormating.java</p>
 * <p>Create Date: Feb 3, 2016 </p>
 * <p>Create Time: 4:13:17 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.common.time;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Shantanu Sikdar 
 *
 */
public class DateFormating {

	
	public static void main(String[] args) {
		compareTime("01/22/2016 08:10:20","01/22/2016 10:14:20");
		compareTime("01/22/2016 08:10:20","01/22/2016 08:15:20");
	}
	
	static void compareTime(String t1, String t2){
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			Date dt1 = sdf.parse(t1);
			Date dt2 = sdf.parse(t2);

/*			System.out.println("modified date1 == " + dt1);
			System.out.println("modified date2 == " + dt2);

			System.out.println("hour 1 == " + dt1.getHours());
			System.out.println("minute 1 == " + dt1.getMinutes());
			System.out.println("hour 2 == " + dt2.getHours());
			System.out.println("minute 2 == " + dt2.getMinutes());
*/			
			if(dt2.getHours()== dt1.getHours()){
				System.out.println("hour equals");
				if(5<=(dt2.getMinutes()-dt1.getMinutes())){
					System.out.println("differ by 5");
				}
			}else if(1<(dt2.getHours()-dt1.getHours())){
				System.out.println("differ by hour");
			}
			
		} catch (ParseException pe) {
			pe.printStackTrace();
		}
		//01/22/2016 08:14:20	"01/22/2016 08:10:20"
	}

}
