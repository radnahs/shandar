/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.common.time </p>
 * <p>File Name: TimeManipulation.java</p>
 * <p>Create Date: Mar 4, 2014 </p>
 * <p>Create Time: 2:34:00 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.common.time;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * @author Shantanu Sikdar 
 *
 */
public class TimeManipulation {

	
	public static void main(String[] args) {
		TimeManipulation tm = new TimeManipulation();
		tm.checkPassedDate("02/28/2014:11:30", "America/New_York", "MM/dd/yyyy:HH:mm");
	}
	
	public Calendar checkPassedDate(String date, String timezone, String dateFormat){
		Calendar cal = Calendar.getInstance();
		try {			
			System.out.println(cal);
			System.out.println(cal.getTime());
			System.out.println("TZ 1 == "+ TimeZone.getDefault());
			
			
			SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
			//sdf.setTimeZone(TimeZone.getTimeZone("EST"));
			sdf.setTimeZone(TimeZone.getTimeZone(timezone));			
			Date dt = sdf.parse(date);
			
			
			System.out.println("modified date == "+dt);
			cal.setTime(dt);
			
			System.out.println("TZ 2  == "+ TimeZone.getTimeZone(timezone));
			
			TimeZone.setDefault(TimeZone.getTimeZone(timezone));
			//TimeZone.setDefault(TimeZone.getTimeZone("EST"));
			
			cal.setTimeZone(TimeZone.getDefault());
			System.out.println(cal);
			System.out.println(cal.getTime());
			
			
			
			/*for (String zones : TimeZone.getAvailableIDs()) {
				System.out.println(zones);
			}*/
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return cal;
	}

}

