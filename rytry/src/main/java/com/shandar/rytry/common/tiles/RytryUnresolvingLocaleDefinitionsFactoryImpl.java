/**
 *
 * <p>Project: trunk_rytry </p>
 * <p>Package Name: com.shandar.rytry.common.tiles </p>
 * <p>File Name: rytryUnresolvingLocaleDefinitionsFactoryImpl.java</p>
 * <p>Create Date: Nov 8, 2013 </p>
 * <p>Create Time: 5:08:05 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.common.tiles;

import org.apache.tiles.definition.UnresolvingLocaleDefinitionsFactory;

/**
 * @author Shantanu Sikdar 
 *
 */
public class rytryUnresolvingLocaleDefinitionsFactoryImpl extends UnresolvingLocaleDefinitionsFactory{

}
