/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.common.utils </p>
 * <p>File Name: FileUtils.java</p>
 * <p>Create Date: May 5, 2014 </p>
 * <p>Create Time: 8:41:13 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.common.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.formula.functions.T;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import com.shandar.rytry.common.model.FileUploadModel;
import com.shandar.rytry.common.properties.rytryProperties;

/**
 * @author Shantanu Sikdar
 * 
 */
public class FileUtils {
	
	private static final Logger logger = LoggerFactory.getLogger(FileUtils.class);
	
	
	/*public List<String> uploadFile(FileUploadModel fileUpload){
		List<MultipartFile> files = fileUpload.getFiles();
		List<String> fileNames = new ArrayList<String>();
		for (MultipartFile multipartFile : CommonUtils.emptyIfNull(files)) {
            String fileName = multipartFile.getOriginalFilename();      
            String fileExt = fileName.substring(fileName.lastIndexOf("."));
            try{	
            	String filePath = createTodayFolder(rytryProperties.documentsPath);
            	
            	File dest = new File(filePath);
            	
            	File temp = File.createTempFile("XBB", fileExt, dest);
            	String ss = temp.getName();
            	System.out.println(ss);
            	temp.delete();
            	
            	dest = new File(filePath+"\\"+fileName);	            	
        		multipartFile.transferTo(dest);
        		
        		//File ff = new File();
        		
        		dest.renameTo(new File(ss));
        		fileNames.add(fileName);        		
        	}catch(IOException ioe){
        		logger.error("EE ::"+ioe);
        	} catch(Exception e){
        		logger.error("EE ::"+e);
        	}                
        }
		return fileNames;
	}*/
	
	
	public List<String> uploadFile(FileUploadModel fileUpload){
		List<MultipartFile> files = fileUpload.getFiles();
		List<String> fileNames = new ArrayList<String>();
		for (MultipartFile multipartFile : CommonUtils.emptyIfNull(files)) {
            String fileName = multipartFile.getOriginalFilename();                        
            try{	
            	String filePath = createTodayFolder(rytryProperties.documentsPath);
            	File dest = new File(filePath);
            	dest = new File(filePath+"\\"+fileName);	            	
        		multipartFile.transferTo(dest);
        		fileNames.add(fileName);
        	}catch(IOException ioe){
        		logger.error("EE ::"+ioe);
        	} catch(Exception e){
        		logger.error("EE ::"+e);
        	}                
        }
		return fileNames;
	}

	private String createTodayFolder(String basePath) throws Exception {
		Calendar cal = new GregorianCalendar();
		String monthFolder = cal.get(Calendar.YEAR) + ((cal.get(Calendar.MONTH) + 1 < 10) ? "0"	+ (cal.get(Calendar.MONTH) + 1) : ""
						+ (cal.get(Calendar.MONTH) + 1));
		String dayFolder = cal.get(Calendar.DAY_OF_MONTH) < 10 ? "0" + cal.get(Calendar.DAY_OF_MONTH) : ""
						+ cal.get(Calendar.DAY_OF_MONTH);

		String monthFolderPath = basePath + "\\" + monthFolder;
		String dayFolderPath = monthFolderPath + "\\" + dayFolder;

		createDirectory(monthFolderPath);
		createDirectory(dayFolderPath);
		return dayFolderPath;
	}

	private boolean createDirectory(String dirName) throws Exception {
		File dirToCreate = new File(dirName);		
		if (!dirToCreate.isDirectory()) {
			return dirToCreate.mkdir();
		}
		return false;
	}
	
}
