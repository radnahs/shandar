/**
 *
 * <p>Project: rytry </p>
 * <p>Package Name: com.shandar.rytry.common.controller </p>
 * <p>File Name: FileUploadController.java</p>
 * <p>Create Date: Jun 14, 2013 </p>
 * <p>Create Time: 2:41:04 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.common.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.shandar.rytry.common.model.FileUploadModel;
import com.shandar.rytry.common.utils.FileUtils;

/**
 * @author Shantanu Sikdar 
 *
 */
@Controller
public class FileUploadController {
	
	private static final Logger logger = LoggerFactory.getLogger(FileUploadController.class);
		
	@RequestMapping( value="FileUploadPopup", method = RequestMethod.POST)
    public String fileUploadPopup(HttpSession session) {
        return "common/fileUploadPopup";
    }
	
	@RequestMapping(value = "/SaveFile", method = RequestMethod.POST)
    public String saveFile(@ModelAttribute("uploadFile") FileUploadModel fileUpload, Model map) {         
        List<String> fileNames = new ArrayList<String>();
        FileUtils fileUtils = new FileUtils();
        fileNames = fileUtils.uploadFile(fileUpload);
        map.addAttribute("files", fileNames);
        return "common/fileUploadPopup";
    }
	
    /*@RequestMapping(value = "/SaveFile", method = RequestMethod.POST)    
    public String saveFile(@ModelAttribute("uploadFile") FileUploadModel fileUpload, Model map) {
        List<MultipartFile> files = fileUpload.getFiles(); 
        List<String> fileNames = new ArrayList<String>();
        FileUtils fileUtils = new FileUtils();
    	for (MultipartFile multipartFile : CommonUtils.emptyIfNull(files)) {
            String fileName = multipartFile.getOriginalFilename();                        
            try{	
            	String filePath = fileUtils.createTodayFolder(rytryProperties.documentsPath);
            	File dest = new File(filePath);
            	dest = new File(filePath+"\\"+fileName);	            	
        		multipartFile.transferTo(dest);
        		fileNames.add(fileName);
        	}catch(IOException ioe){
        		logger.error("EE ::"+ioe);
        	} catch(Exception e){
        		logger.error("EE ::"+e);
        	}                
        }                
        map.addAttribute("files", fileNames);
        return "common/fileUploadPopup";
    }*/
   
   
}