/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.feature.controller </p>
 * <p>File Name: GridUI.java</p>
 * <p>Create Date: Apr 20, 2015 </p>
 * <p>Create Time: 5:12:29 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.common.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.shandar.rytry.common.dataObject.UIControls;
import com.shandar.rytry.common.dataObject.UIControlsContainer;

/**
 * @author Shantanu Sikdar 
 *
 */
@Controller
public class UIAdminController {
	
	private static final Logger logger = LoggerFactory.getLogger(UIAdminController.class);
	
	@RequestMapping(value = "UIAdmin", method = RequestMethod.GET)	
	public ModelAndView uiAdmin(@ModelAttribute("UIControlsContainer") UIControlsContainer uiCC) {
		return new ModelAndView("common/uiAdmin");		
	}
	
	@RequestMapping(value = "UIControls", method = RequestMethod.GET)	
	public ModelAndView uiControls(@ModelAttribute("UIControlsContainer") UIControlsContainer uiCC) {
		return new ModelAndView("common/uiControls");		
	}
	
	@RequestMapping(value = "multiUIControlValues", method = RequestMethod.POST)
	public ModelAndView multiUIControlValues(@ModelAttribute("UIControlsContainer") UIControlsContainer uiCC) {
		Map<String, Object> modelMap = new HashMap<String, Object>();
		List<UIControls> uiccLst = uiCC.getUiControls();
		System.out.println("UiControls List === " + uiccLst);
		System.out.println("uiControlsContainer = " + uiCC);
		return new ModelAndView("common/uiControls", modelMap);
	}
	
	
}
