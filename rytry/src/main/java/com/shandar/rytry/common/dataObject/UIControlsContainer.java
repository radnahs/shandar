/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.common.dataObject </p>
 * <p>File Name: UIControlsModel.java</p>
 * <p>Create Date: Apr 20, 2015 </p>
 * <p>Create Time: 6:55:36 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.common.dataObject;

import java.util.List;

/**
 * @author Shantanu Sikdar 
 *
 */
public class UIControlsContainer {
	
	private List<UIControls> uiControls;

	public List<UIControls> getUiControls() {
		return uiControls;
	}

	public void setUiControls(List<UIControls> uiControls) {
		this.uiControls = uiControls;
	}

	@Override
	public String toString() {
		return "UIControlsContainer [uiControls=" + uiControls + "]";
	}
	
	
}