/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.common.files </p>
 * <p>File Name: ListFilesUtil.java</p>
 * <p>Create Date: Jan 11, 2016 </p>
 * <p>Create Time: 4:10:20 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.common.files;

import java.io.File;
import java.io.IOException;

/**
 * @author Shantanu Sikdar 
 *
 */
public class ListFilesUtil {
	public static void main(String[] args) {
		File currentDir = new File("."); // current directory
		displayDirectoryContents(currentDir);
	}

	public static void displayDirectoryContents(File dir) {
		try {
			File[] files = dir.listFiles();
			for (File file : files) {
				if (file.isDirectory()) {
					System.out.println("directory:" + file.getCanonicalPath());
					displayDirectoryContents(file);
				} else {
					System.out.println("     file:" + file.getCanonicalPath());
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
