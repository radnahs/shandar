/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.common.files </p>
 * <p>File Name: FilesAndFolderUtil.java</p>
 * <p>Create Date: Mar 4, 2014 </p>
 * <p>Create Time: 11:46:00 AM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.common.files;

import java.io.File;
import java.util.List;

import com.shandar.rytry.common.dataObject.SimpleDataObject;

/**
 * @author Shantanu Sikdar
 * 
 */
public interface ServerFilesAndFolder {
	
	public List<SimpleDataObject> listDrives();
	
	public List<SimpleDataObject> listSubFoldersAndFiles(File directory) ;

	public void listFiles(File f);

	public void listDirectoriesRecursively(File directory);
	
	
	
}
