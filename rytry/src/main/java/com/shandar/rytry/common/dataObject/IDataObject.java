/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.common.dataObject </p>
 * <p>File Name: IDataObject.java</p>
 * <p>Create Date: Feb 10, 2014 </p>
 * <p>Create Time: 12:47:46 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.common.dataObject;

import java.util.Map;

/**
 * @author Shantanu Sikdar 
 *
 */
public interface IDataObject {
	
	/**
	 * Object specified by the String key
	 * @param key
	 * @return
	 */
	public Object getAttribute(String key);
	
	/**
	 * Set the attribute with name = String key and value= Object value
	 * @param key
	 * @param value
	 */
	public void setAttribute(String key, Object value);
	
	/**
	 * Map of all attributes key/value pair
	 * @return
	 */
	public Map<String, Object> getAttributes();
	
	/**
	 * set attributes as key/value pair from Map attributes
	 * @param attributes
	 */
	public void setAttributes(Map<String, Object> attributes);
	

}
