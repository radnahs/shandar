/**
 *
 * <p>Project: rytry </p>
 * <p>Package Name: com.shandar.rytry.common.model </p>
 * <p>File Name: FileUpload.java</p>
 * <p>Create Date: Jun 14, 2013 </p>
 * <p>Create Time: 2:44:40 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.common.model;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

/**
 * @author Shantanu Sikdar 
 *
 */
public class FileUploadModel{
	
	private List<MultipartFile> files;

	public List<MultipartFile> getFiles() {
		return files;
	}

	public void setFiles(List<MultipartFile> files) {
		this.files = files;
	}

	
	
}
