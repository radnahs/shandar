package com.shandar.rytry.common.properties;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

public class rytryProperties {

	private static Logger logger = Logger.getLogger(rytryProperties.class.getName());
	private static Properties properties = new Properties();
	private static InputStream in = rytryProperties.class.getResourceAsStream("/rytry.properties");
	
	static {
		try {
			properties.load(in);
			in.close();
		} catch (IOException e) {
			logger.error(e);			
		} finally {

		}
	}
	
	public static String installationPath = properties.getProperty("rytry.installation.path");
	public static String documents = properties.getProperty("rytry.documents");
	public static String documentsPath = installationPath+"\\"+documents;
	
	public static String defaultExcelTemplate = properties.getProperty("rytry.default.excelTemplate");
	
}
