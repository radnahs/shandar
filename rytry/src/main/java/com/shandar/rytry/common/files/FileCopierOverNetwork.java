/**
* Project: rytry
* Package Name:package com.shandar.rytry.common.files;
* File Name: FileCopierOverNetwork.java
* Create Date: Feb 7, 2017
* Create Time: 3:08:48 PM
* Copyright: Copyright (c) 2016
* @author: Shantanu Sikdar, ssikdar
* @version 1.0
*/
package com.shandar.rytry.common.files;


/**
 * https://vinaydvd.wordpress.com/2013/12/08/copying-files-from-a-remote-linux-server-to-local-windows-server-using-java/
 */

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

public class FileCopierOverNetwork {

	public static void main(String[] args) {
		String hostname = "cceapp301p.stress.ch3.s.com";
        String username = "plal0";
        String password = "priyalal2792";
        //String copyFrom = "/appl/scinventory-dcu/dce/logs";
        String copyFrom = "/appl/scinventory-dcu/dce/logs/DceCustomerOrders.log";
        //String copyFrom = "\\appl\\scinventory-dcu\\dce\\logs\\DceCustomerOrders.log";
        String copyTo = "C:\\Data\\misc\\priyalal_files"; 
        JSch jsch = new JSch();
        Session session = null;
        System.out.println("Trying to connect.....");
        try {
            session = jsch.getSession(username, hostname, 22);
            session.setConfig("StrictHostKeyChecking", "no");
            session.setPassword(password);
            session.connect(); 
            Channel channel = session.openChannel("sftp");
            channel.connect();
            ChannelSftp sftpChannel = (ChannelSftp) channel; 
            sftpChannel.get(copyFrom, copyTo);
            sftpChannel.exit();
            session.disconnect();
        } catch (JSchException e) {
            e.printStackTrace();  
        } catch (SftpException e) {
            e.printStackTrace();
        }
        System.out.println("Done !!");
	}

}
