/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.feature.manager </p>
 * <p>File Name: OCRManager.java</p>
 * <p>Create Date: Jan 20, 2015 </p>
 * <p>Create Time: 12:56:44 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.feature.manager;

import java.io.File;

import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;

/**
 * @author Shantanu Sikdar 
 *
 */
public class OCRManager {

	public static void main(String[] args) {
		String imgnm = "C:\\Users\\xbbkngi.PAC\\Desktop\\testOCR3.tif";
		File imageFile = new File(imgnm);//"eurotext.tif");
		Tesseract instance = Tesseract.getInstance(); // JNA Interface Mapping
		// Tesseract1 instance = new Tesseract1(); // JNA Direct Mapping
		
		try {
			String result = instance.doOCR(imageFile);
			System.out.println(result);
		} catch (TesseractException e) {
			System.err.println(e.getMessage());
		}

	}

}
