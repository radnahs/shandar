/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.ipgadgets </p>
 * <p>File Name: GadgetController.java</p>
 * <p>Create Date: Nov 3, 2014 </p>
 * <p>Create Time: 8:41:05 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.feature.controller;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author Shantanu Sikdar 
 *
 */
@Controller
public class GadgetController {

	/**
	 * @param args
	 */
	@RequestMapping(value = "/ipPhone", method = RequestMethod.GET)
	public ModelAndView home(Locale locale, Model model) {
		Map<String, Object> modelMap = null;
		try {
			modelMap= new HashMap<String, Object>();			
			Date date = new Date();
			DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
			String formattedDate = dateFormat.format(date);
			modelMap.put("serverTime", formattedDate);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ModelAndView("home", modelMap);
	}
	
	
	
	
	
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
