/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.feature.controller </p>
 * <p>File Name: SVNClientController.java</p>
 * <p>Create Date: Jun 16, 2015 </p>
 * <p>Create Time: 4:52:54 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.feature.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author Shantanu Sikdar 
 *
 */
@Controller
public class SVNController {
	
	private static final Logger logger = LoggerFactory.getLogger(SVNController.class);
	
	@RequestMapping(value = "reportRelatedInfo", method = RequestMethod.POST)	
	public ModelAndView processSVNInstruction() {
		
		ModelAndView modelAndView = null;
		try {
			modelAndView = new ModelAndView();
			modelAndView.setViewName("paramRelatedInfo");
		} catch (Exception e) {
			logger.error("Error : ",e);
		}
		return modelAndView;
	}
	
	
	
	
}
