/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.scraper.manager </p>
 * <p>File Name: ScraperManager.java</p>
 * <p>Create Date: Jan 23, 2014 </p>
 * <p>Create Time: 12:59:41 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.feature.manager;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpHost;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONArray;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.shandar.rytry.parser.converter.HTMLToPlainTextConverter;

/**
 * @author Shantanu Sikdar
 *
 */
public class ScraperManager {

	private static final Logger logger = LoggerFactory.getLogger(ScraperManager.class);

	/**
	 * 
	 * refer link
	 * :https://hc.apache.org/httpcomponents-client-ga/httpclient/examples/org/apache/http/examples/client/ClientProxyAuthentication.java
	 * 
	 * @param url
	 * @return
	 */
	public String getWebPageProxy(String url) {
		String result = null;
		OutputStream outputStream = null;
		HTMLToPlainTextConverter convrtr = new HTMLToPlainTextConverter();
		CredentialsProvider credsProvider = new BasicCredentialsProvider();
		// credsProvider.setCredentials(new AuthScope("localhost", 8080),new
		// UsernamePasswordCredentials("", ""));
		CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();
		try {
			/* HttpHost target = new HttpHost("www.verisign.com", 443, "https"); */
			HttpHost target = new HttpHost(url);
			// HttpHost proxy = new HttpHost("167.113.63.193", 8080);//inautix proxy
			HttpHost proxy = new HttpHost("localhost", 8181);

			RequestConfig config = RequestConfig.custom().setProxy(proxy).build();
			HttpGet httpget = new HttpGet("/");
			httpget.setConfig(config);

			CloseableHttpResponse response = httpclient.execute(target, httpget);
			try {
				InputStream ips = response.getEntity().getContent();
				result = IOUtils.toString(ips);
				System.out.println("result == " + result);
				result = convrtr.getResult(result);
				// EntityUtils.consume(response.getEntity());
			} finally {
				response.close();
			}
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} finally {
			try {
				httpclient.close();
			} catch (IOException ioe) {
				ioe.printStackTrace();
			}
		}
		return result;
	}

	public static void main2(String[] args) throws Exception {
		// String url = "http://www.geeksforgeeks.org/static-class-in-java/";

		// String url = "http://alistapart.com/articles";
		String url = "http://stackoverflow.com/questions/11087163/how-to-get-url-html-contents-to-string-in-java";
		System.out.println("html output started ::" + Calendar.getInstance().getTime());
		String output = new ScraperManager().getWebPageProxy(url);
		System.out.println("html output Ended ::" + Calendar.getInstance().getTime());
		System.out.println("*********************************");
		System.out.println(output);
	}

	public static void main(String[] args) throws IOException, Exception {
		// Document doc = Jsoup.connect("https://www.gosf.in/categories/all/").get();
		Document doc = Jsoup.connect("https://www.bellway.co.uk/new-homes/manchester/hazel-fold").get();
		// Document doc = Jsoup.connect("http://als.inautix.com/").get();
		// Document doc =
		// Jsoup.connect("http://als.inautix.com/ALS_AssociateCheckLeaveBalance.aspx/").get();

		/*
		 * //print all titles in main page Elements titles = doc.select(".entrytitle");
		 * for(Element e: titles){ System.out.println("text: " +e.text());
		 * System.out.println("html: "+ e.html()); }
		 * 
		 * //print all available links on page Elements links = doc.select("a[href]");
		 * for(Element l: links){ System.out.println("link: " +l.attr("abs:href")); }
		 */

		// displayEntity(doc,"h1");
		displayEntity(doc, "script");
	}

	private static void displayEntity(Document doc, String tag) throws Exception {
		/*
		 * Elements links = doc.select(tag); for(Element l: links){
		 * //System.out.println("link: " +l.attr("abs:href"));
		 * //System.out.println("text: " +l.text()); System.out.println("html: "+
		 * l.html());
		 * 
		 * }
		 */
		Elements scrpts = doc.select("script");
		List<String> result = new ArrayList<>();
		String jsonText = null;
		for (Element el : scrpts) {

			if (el.data().contains("var plots")) {
				jsonText = el.data().replaceAll("var plots = ", "");
				jsonText = jsonText.replaceAll("\r\n", "");
                jsonText = "["+jsonText+"]";
			}
		}
		
		System.out.println(jsonText);
		JSONArray imageArray = new JSONArray(jsonText);

		for (int i = 0; i < imageArray.length(); i++) {
			result.add(imageArray.getJSONObject(i).getString("HouseStyleName"));
		}

		System.out.println(result);
		// System.out.println(imageArray);
	}

}
