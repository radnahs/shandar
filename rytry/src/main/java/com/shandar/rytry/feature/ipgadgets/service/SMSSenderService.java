/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.ipgadgets.service </p>
 * <p>File Name: SMSSenderService.java</p>
 * <p>Create Date: Nov 4, 2014 </p>
 * <p>Create Time: 12:02:26 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.feature.ipgadgets.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

/**
 * @author Shantanu Sikdar 
 *
 */
public class SMSSenderService {
	
	final int DIGITS_IN_CELLPHONE=10;
	final String PROVIDER_URL = "URL";
				
	public void send(String message, String cellPhoneNumber, String senderId) throws Exception {
		try {			
			System.out.println("1");
			//URL url = new URL("http://66.36.229.70:8800/?user=talentp&password=tal567&PhoneNumber="+URLEncoder.encode(cellPhoneNumber, "UTF-8")+"&Text="+URLEncoder.encode(message, "UTF-8")+"&Sender=SMSjunction");
			URL url = new URL("http://66.36.229.70:8800/?user=talentp&password=tal567&PhoneNumber="+URLEncoder.encode(cellPhoneNumber, "UTF-8")+"&Text="+URLEncoder.encode(message, "UTF-8")+"&Sender=SMSjunction");
			
	        URLConnection conn = url.openConnection();
	        System.out.println("2");
	        conn.setDoOutput(true);
	        System.out.println("3");
	        System.out.println("sent message");
	        
	        // Get the response
	        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	        String line;
	        while ((line = rd.readLine()) != null) {	            
	            System.out.println(line.toString());
	        }
	        rd.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	public static void main(String[] args) {
		SMSSenderService smsss = new SMSSenderService();
		try {
			System.out.println("hillo 1");
			smsss.send("how is that", "9423577420", "talentp");
			System.out.println("hillo 2");
			//smsss.send("how is that", "9423577420", "talentp");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
