/**
* Project: rytry
* Package Name:package com.shandar.rytry.feature.compression;
* File Name: CompressionUsingMap.java
* Create Date: May 6, 2009
* Create Time: 12:53:12 PM
* Copyright: Copyright (c) 2016
* @author: Shantanu Sikdar, ssikdar
* @version 1.0
*/
package com.shandar.rytry.feature.compression;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Map.Entry;

public class CompressionUsingMap {

	static Map<String, List<Integer>> mapKeyIndex = new HashMap<String, List<Integer>>();
	
	public static void main(String[] args) {
		CompressionUsingMap cum = new CompressionUsingMap();
		//String ster = "I started working in 2006, a decade ago. Such milestone inspired me to look back and point at the mistakes I did, and the kind of advice I wished someone from the profession had told me. Things change fast, I am not sure how applicable this tips will be in ten years.";
		//String[] strArr = ster.split(" ");
		//System.out.println(strArr.length);
		//cum.mapKeyOccurence(strArr);
		//cum.displayCompression();
		//cum.readFile("C:\\Data\\misc\\shantanu.txt");
		cum.readFile("D:\\Misc\\tocompress.txt");
		cum.writeFile("D:\\Misc\\compressed.txt");
	}
	private void readFile(String fileName){
		CompressionUsingMap cum = new CompressionUsingMap();
		int wrdCnt=0;
		try {
			File file = new File(fileName);
			Scanner scanner = new Scanner(file);
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				String[] strArr = line.split(" ");
				wrdCnt=wrdCnt+strArr.length;
				cum.mapKeyOccurence(strArr,wrdCnt);
				//System.out.println("wrdCnt = "+wrdCnt);
			}
			scanner.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("wrdCnt = "+wrdCnt);
		//cum.displayCompression();
	}
	
	private void writeFile(String writeToFile){
		try{
		    //PrintWriter writer = new PrintWriter("C:\\Data\\misc\\sikdar.txt");
			PrintWriter writer = new PrintWriter(writeToFile);
		    for (Entry<String, List<Integer>> mapKey : mapKeyIndex.entrySet()) {
		    	writer.println(mapKey.getKey() + " - " + mapKey.getValue());
			}
		    writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void mapKeyOccurence(String[] strArr,int startIndex){
		for(int i=0; i<strArr.length;i++){
			if(mapKeyIndex.containsKey(strArr[i])){
				List<Integer> lst = mapKeyIndex.get(strArr[i]);
				lst.add(i+startIndex);
				mapKeyIndex.put(strArr[i], lst);
			}else{
				List<Integer> lst = new ArrayList<>();
				lst.add(i+startIndex);
				mapKeyIndex.put(strArr[i], lst);
			}
		}
	}
	
	private void displayCompression(){
		for (Entry<String, List<Integer>> mapKey : mapKeyIndex.entrySet()) {
			System.out.println(mapKey.getKey() + " - " + mapKey.getValue());
		}
		System.out.println(mapKeyIndex.size());
	}

}
