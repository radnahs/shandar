/**
 *
 * <p>Project: wbOnboarding </p>
 * <p>Package Name: com.shandar.rytry.feature.excelToUI.impl </p>
 * <p>File Name: Excel972003Operaions.java</p>
 * <p>Create Date: Mar 24, 2015 </p>
 * <p>Create Time: 2:08:11 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.feature.excelToUI.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;

import com.shandar.rytry.feature.excelToUI.ExcelOperation;
import static com.shandar.rytry.common.constants.CommonConstants.*;

/**
 * @author Shantanu Sikdar 
 *
 */
public class Excel2003XLS implements ExcelOperation{
	
	private List<String> listOfExcelSheet(HSSFWorkbook workbook){
		List<String> sheetList = new ArrayList<String>();
		try {		    
		    int numOfSheets = workbook.getNumberOfSheets();
		    for(int i=0;i<numOfSheets;i++) {
		    	HSSFSheet sheet = workbook.getSheetAt(i);
		    	sheetList.add(i,sheet.getSheetName());		    	
			}
		} catch (Exception e) {
		    e.printStackTrace();
		}
		return sheetList;
	}
	
	private Map<Integer, String> readExcelSheet(HSSFSheet worksheet){		
		HSSFRow row = worksheet.getRow(EXCEL_ROW_ONE);		
		Map<Integer, String> rowMap = null;
		if(!(row==null))			
			rowMap=readRow(row);
		//worksheet.get
		//readColumn()
		return rowMap;		
	}
	
	private Map<Integer, String> readRow(HSSFRow row){		
		Map<Integer, String> rowMap = new HashMap<Integer, String>();
		if(!(row==null)){
			int i=0;
			for (Cell cell : row) {
				rowMap.put(i++, cell.getStringCellValue());
			}
		}
		return rowMap;
	}
	
	public List<Map<Integer, String>> readExcel(String excelPath){
		List<Map<Integer, String>> listRowMap = new ArrayList<Map<Integer, String>>();
		try {
			FileInputStream file = new FileInputStream(new File(excelPath));
			HSSFWorkbook workbook = new HSSFWorkbook(file);			
			List<String> sheetList = listOfExcelSheet(workbook);
			for (String sheetName : sheetList) {
				HSSFSheet worksheet = workbook.getSheet(sheetName);
				System.out.println(sheetName);
				readExcelSheet(worksheet);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return listRowMap;
	}
	
	/*public static void main(String[] args) {
		System.out.println(WbOnboardingProperties.installationPath);
		ExcelOperation xclPoi =  new Excel2003XLS();
		String excelPath = WbOnboardingProperties.documentsPath+"\\"+WbOnboardingProperties.excelTemplateName;
		xclPoi.readExcel(excelPath);
	}*/
	
}
