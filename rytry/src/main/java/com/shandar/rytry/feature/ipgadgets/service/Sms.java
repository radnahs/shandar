/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.ipgadgets.service </p>
 * <p>File Name: Sms.java</p>
 * <p>Create Date: Mar 10, 2014 </p>
 * <p>Create Time: 8:36:04 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.feature.ipgadgets.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.commons.codec.binary.Base64;

/**
 * @author Shantanu Sikdar
 * re
 * 
 */
public class Sms {

	public static void main(String[] args) {
		try {
			String phoneNumber = "0919423577420";//"phone-number";
			String appKey = "2c402efe-3a2a-4421-8404-512b5eb93783"; //"your-app-key";
			String appSecret = "z75ZCcB2y0eCg8/QDl9Gxg==";//"your-app-secret";
			String message = "From iNautexpress, xbbkngi!";
			System.out.println("1");
			URL url = new URL("https://messagingapi.sinch.com/v1/sms/"+ phoneNumber);
			//URL url = new URL("https://sandbox.sinch.com/v1/sms/"+ phoneNumber);
			System.out.println("2");
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			System.out.println("3");
			connection.setDoOutput(true);
			//connection.setRequestMethod("POST");
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Content-Type", "application/json");			
			String userCredentials = "application\\" + appKey + ":" + appSecret;
			
			byte[] encoded = Base64.encodeBase64(userCredentials.getBytes());
			String basicAuth = "Basic " + new String(encoded);
			connection.setRequestProperty("Authorization", basicAuth);
			String postData = "{\"Message\":\"" + message + "\"}";
			System.out.println("4");
			OutputStream os = connection.getOutputStream();
			System.out.println("5");
			os.write(postData.getBytes());
			StringBuilder response = new StringBuilder();
			BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String line;
			while ((line = br.readLine()) != null)
				response.append(line);
			br.close();
			os.close();
			System.out.println(response.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
