/**
 *
 * <p>Project: wbOnboarding </p>
 * <p>Package Name: com.shandar.rytry.feature.excelToUI.impl </p>
 * <p>File Name: Excel2007XLSX.java</p>
 * <p>Create Date: Mar 24, 2015 </p>
 * <p>Create Time: 2:09:14 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.feature.excelToUI.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.shandar.rytry.feature.excelToUI.ExcelOperation;

import static com.shandar.rytry.common.constants.CommonConstants.*;
/**
 * @author Shantanu Sikdar 
 *
 */
public class Excel2007XLSX implements ExcelOperation{

	public List<String> listOfExcelSheet(XSSFWorkbook workbook){
		List<String> sheetList = new ArrayList<String>();
		try {		    
		    int numOfSheets = workbook.getNumberOfSheets();
		    for(int i=0;i<numOfSheets;i++) {
		    	XSSFSheet sheet = workbook.getSheetAt(i);
		    	sheetList.add(i,sheet.getSheetName());		    	
			}
		} catch (Exception e) {
		    e.printStackTrace();
		}
		return sheetList;
	}
	
	private Map<Integer, String> readExcelSheet(XSSFSheet worksheet, int rownum){		
		XSSFRow row = worksheet.getRow(rownum);
		Map<Integer, String> rowMap = null;
		if(!(row==null))			
			rowMap=readRow(row);
		return rowMap;
	}
	
	private Map<Integer, String> readRow(XSSFRow row){
		Map<Integer, String> rowMap = null;
		if(!(row==null)){
			int i=0;
			rowMap = new HashMap<Integer, String>();
			for (Cell cell : row) {
				rowMap.put(i++, cell.getStringCellValue());
			}
		}
		//System.out.println(rowMap);
		return rowMap;
	}
	
	public List<Map<Integer, String>> readExcel(String excelPath){
		List<Map<Integer, String>> listRowMap = new ArrayList<Map<Integer, String>>();
		try {
			FileInputStream file = new FileInputStream(new File(excelPath));
			XSSFWorkbook workbook = new XSSFWorkbook(file);			
			List<String> sheetList = listOfExcelSheet(workbook);
			Map<Integer, String> rowMap = new HashMap<Integer, String>();
			 
			for (String sheetName : sheetList) {
				XSSFSheet worksheet = workbook.getSheet(sheetName);				
				rowMap = readExcelSheet(worksheet,EXCEL_ROW_ONE);
				listRowMap.add(rowMap);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return listRowMap;
	}
	
}
