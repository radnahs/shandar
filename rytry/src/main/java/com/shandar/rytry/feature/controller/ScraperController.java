/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.scraper </p>
 * <p>File Name: ScraperController.java</p>
 * <p>Create Date: Jan 22, 2014 </p>
 * <p>Create Time: 5:45:55 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.feature.controller;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shandar.rytry.feature.manager.ScraperManager;
import com.shandar.rytry.login.model.User;

/**
 * @author Shantanu Sikdar 
 *
 */

@Controller
public class ScraperController {
	
	private static final Logger logger = LoggerFactory.getLogger(ScraperController.class);
	
	@RequestMapping( value="Scraper")
    public String scraperView(User user, BindingResult result, Map model, HttpSession session) {
        return "scraper/scraper";
    }
	
	@RequestMapping( value="ScraperLink")    	
	public @ResponseBody String fetchScraperLink(HttpSession session) {
		ScraperManager sm = new ScraperManager();
		String num = sm.getWebPageProxy("www.google.com");
		//String num = sm.getWebPageProxy("http://alistapart.com/");
		//String num = sm.getWebPageProxy("http://stackoverflow.com/questions/11087163/how-to-get-url-html-contents-to-string-in-java");
		
		//System.out.println(num);
		//return getHTML("http://www.google.co.uk");
        return num;        
    }

}
