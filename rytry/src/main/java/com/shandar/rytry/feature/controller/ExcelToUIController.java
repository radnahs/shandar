/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.feature.controller </p>
 * <p>File Name: ExcelToUIController.java</p>
 * <p>Create Date: Mar 26, 2015 </p>
 * <p>Create Time: 2:59:59 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.feature.controller;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.shandar.rytry.feature.excelToUI.InputFieldItem;
import com.shandar.rytry.feature.manager.ExcelToUIService;
import com.shandar.rytry.login.model.User;

/**
 * @author Shantanu Sikdar 
 *
 */

@Controller
public class ExcelToUIController {
	
	private static final Logger logger = LoggerFactory.getLogger(ExcelToUIController.class);
	
	@RequestMapping( value="ExcelToUI")
    public ModelAndView excelToUIView(User user, BindingResult result, Map model, HttpSession session, Locale locale) {
		ModelAndView modelAndView = null;
		try{
			modelAndView = new ModelAndView();
			Date date = new Date();
			DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
			String formattedDate = dateFormat.format(date);
			modelAndView.addObject("serverTime", formattedDate);
			
			ExcelToUIService xl2UiSrv = new ExcelToUIService();
			Map<Integer, List<InputFieldItem>> mapLstFields = xl2UiSrv.fetchField();
			modelAndView.addObject("reportRelatedInfo", mapLstFields.get(0));
			modelAndView.addObject("parameterRelatedInfo", mapLstFields.get(1));
			modelAndView.addObject("reportDBInfo", mapLstFields.get(2));
			modelAndView.addObject("reportOutputInfo", mapLstFields.get(3));
			
			modelAndView.setViewName("features/excelToUI");
		}catch(Exception e){
			logger.error("Error : ",e);
		}
		return modelAndView;
    }
	
	@RequestMapping( value="DynamicXL2UI")
    public ModelAndView dynamicXL2UIView(User user, BindingResult result, Map model, HttpSession session, Locale locale) {
		ModelAndView modelAndView = null;
		try{
			modelAndView = new ModelAndView();
			Date date = new Date();
			DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
			String formattedDate = dateFormat.format(date);
			modelAndView.addObject("serverTime", formattedDate);
			
			ExcelToUIService xl2UiSrv = new ExcelToUIService();
			Map<Integer, List<InputFieldItem>> mapLstFields = xl2UiSrv.fetchField();
			modelAndView.addObject("reportRelatedInfo", mapLstFields.get(0));
			modelAndView.addObject("parameterRelatedInfo", mapLstFields.get(1));
			modelAndView.addObject("reportDBInfo", mapLstFields.get(2));
			modelAndView.addObject("reportOutputInfo", mapLstFields.get(3));
			
			modelAndView.setViewName("features/dynamicXL2UI");
		}catch(Exception e){
			logger.error("Error : ",e);
		}
		return modelAndView;
    }

	
	/*@RequestMapping( value="ReportRelatedInfo")
	public @ResponseBody ModelAndView showReportRelatedInfo(HttpServletRequest request,HttpSession session) {
		ExcelToUIService xl2UiSrv = new ExcelToUIService();
		ModelAndView modelAndView = null;
		try{
			modelAndView = new ModelAndView();
			Map<Integer, List<InputFieldItem>> mapLstFields = xl2UiSrv.fetchField();
			modelAndView.addObject("reportRelatedInfo", mapLstFields.get(0));
		}catch(Exception e){
			logger.error("Error : ",e);
		}		
		return modelAndView;
    }*/
	
}