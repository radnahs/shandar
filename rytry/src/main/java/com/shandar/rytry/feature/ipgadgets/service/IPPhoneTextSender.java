/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.ipgadgets.service </p>
 * <p>File Name: IPPhoneTextSender.java</p>
 * <p>Create Date: Nov 4, 2014 </p>
 * <p>Create Time: 12:54:46 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.feature.ipgadgets.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

/**
 * @author Shantanu Sikdar 
 *
 */
public class IPPhoneTextSender {
	
	public void send(String message, String cellPhoneNumber, String senderId) throws Exception {
		try {			
			URL postUrl = new URL("http://10.183.133.118/CGI/Execute");
			URL fetchUrl = new URL("http://10.183.133.117/cgi-bin/nfl.cgi");
			URLConnection conn = postUrl.openConnection();			
	        System.out.println("2");
	        conn.setDoOutput(true);
	        
	        System.out.println("sent message");
	        
	        // Get the response
	        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	        String line;
	        while ((line = rd.readLine()) != null) {	            
	            System.out.println(line.toString());
	        }
	        rd.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public String getWebPageProxy(String url) {
	 	String result = null;
        CredentialsProvider credsProvider = new BasicCredentialsProvider();	        
        credsProvider.setCredentials(new AuthScope("localhost", 8080),new UsernamePasswordCredentials("xbbkngi", "9632580741"));	        
        CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();
        try {
            /*HttpHost target = new HttpHost("www.verisign.com", 443, "https");*/	        	
        	HttpHost target = new HttpHost(url);	        	
            HttpHost proxy = new HttpHost("10.183.133.118");
        	
            RequestConfig config = RequestConfig.custom().setProxy(proxy).build();	            
            HttpGet httpget = new HttpGet("/");
            httpget.setConfig(config);	            

            CloseableHttpResponse response = httpclient.execute(target, httpget);
            try {	            	
                InputStream ips = response.getEntity().getContent();           
                result = IOUtils.toString(ips);
                System.out.println(result);                
            } finally {
                response.close();
            }
        } catch(IOException ioe){
        	ioe.printStackTrace();
        }finally {
        	try{
        		httpclient.close();
        	}catch(IOException ioe){
	        	ioe.printStackTrace();
	        }
        }
        return result;
 }

	
	public static void main(String[] args) {
		IPPhoneTextSender smsss = new IPPhoneTextSender();
		try {
			System.out.println("hillo 1");
			//smsss.send("how is that", "9423577420", "talentp");
			smsss.getWebPageProxy("http://10.183.133.118/CGI/Execute");
			System.out.println("hillo 2");			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
