/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.feature.manager </p>
 * <p>File Name: ExcelToUIService.java</p>
 * <p>Create Date: Mar 26, 2015 </p>
 * <p>Create Time: 3:02:02 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.feature.manager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.shandar.rytry.common.properties.rytryProperties;
import com.shandar.rytry.feature.excelToUI.ExcelOperation;
import com.shandar.rytry.feature.excelToUI.InputFieldItem;
import com.shandar.rytry.feature.excelToUI.impl.Excel2007XLSX;

/**
 * @author Shantanu Sikdar 
 *
 */
public class ExcelToUIService {
	
	public Map<Integer, List<InputFieldItem>> fetchField(){
		ExcelOperation xclPoi =  new Excel2007XLSX();
		String excelPath = rytryProperties.documentsPath+"\\"+rytryProperties.defaultExcelTemplate;
		List<Map<Integer, String>> listRowMap = xclPoi.readExcel(excelPath);
		Map<Integer, List<InputFieldItem>> mapLstFields = new HashMap<Integer, List<InputFieldItem>>();
		for (int i = 0; i < listRowMap.size(); i++) {
			Map<Integer, String> map = listRowMap.get(i);
			List<InputFieldItem> lstItem = createFieldData(map);
			mapLstFields.put(i, lstItem);
		}
		return mapLstFields;
	}
	
	private List<InputFieldItem> createFieldData(Map<Integer, String> map){
		List<InputFieldItem> lstItem = new ArrayList<InputFieldItem>();
		for(Entry<Integer, String> entry : map.entrySet()){
			InputFieldItem inpItem = new InputFieldItem();
			inpItem.setFieldId(entry.getKey()+"_"+entry.getValue());
			inpItem.setFieldName(entry.getValue());
			inpItem.setFieldType("input");
			lstItem.add(inpItem);
		}
		return lstItem;
	}
	
	public static void main(String[] args) {
		System.out.println("H"+"a");
		System.out.println('H'+'a');
	/*	System.out.println(rytryProperties.installationPath);		
		ExcelOperation xclPoi =  new Excel2007XLSX();
		String excelPath = rytryProperties.documentsPath+"\\"+WbOnboardingProperties.excelTemplateName;
		xclPoi.readExcel(excelPath);*/
	}
	
}
