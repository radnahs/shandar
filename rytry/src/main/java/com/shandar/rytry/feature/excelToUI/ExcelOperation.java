/**
 *
 * <p>Project: rytry </p>
 * <p>Package Name: com.shandar.rytry.feature.excelToUI </p>
 * <p>File Name: ExcelOperaion.java</p>
 * <p>Create Date: Mar 23, 2015 </p>
 * <p>Create Time: 7:17:18 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.feature.excelToUI;

import java.util.List;
import java.util.Map;

/**
 * @author Shantanu Sikdar 
 *
 */
public interface ExcelOperation {
	
	public List<Map<Integer, String>> readExcel(String excelPath);
	
}
