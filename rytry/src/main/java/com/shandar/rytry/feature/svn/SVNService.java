/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.feature.svn </p>
 * <p>File Name: SVNClientService.java</p>
 * <p>Create Date: Jun 17, 2015 </p>
 * <p>Create Time: 12:36:42 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.feature.svn;

import java.io.ByteArrayInputStream;

import org.tmatesoft.svn.core.SVNCommitInfo;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNNodeKind;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.auth.ISVNAuthenticationManager;
import org.tmatesoft.svn.core.internal.io.fs.FSRepositoryFactory;
import org.tmatesoft.svn.core.io.ISVNEditor;
import org.tmatesoft.svn.core.io.SVNRepository;
import org.tmatesoft.svn.core.io.SVNRepositoryFactory;
import org.tmatesoft.svn.core.io.diff.SVNDeltaGenerator;
import org.tmatesoft.svn.core.wc.SVNWCUtil;

/**
 * @author Shantanu Sikdar 
 *	http://wiki.svnkit.com/Committing_To_A_Repository
 */
public class SVNService {
	
	
	private static SVNCommitInfo addDir(ISVNEditor editor, String dirPath, String filePath, byte[] data ) 
			throws SVNException {
		
        editor.openRoot(-1 );
        editor.addDir(dirPath, null, -1 );
        editor.addFile(filePath, null, -1 );
        editor.applyTextDelta(filePath, null );

        SVNDeltaGenerator deltaGenerator = new SVNDeltaGenerator();
        String checksum = deltaGenerator.sendDelta(filePath, new ByteArrayInputStream(data ), editor, true );

        editor.closeFile(filePath, checksum);
        editor.closeDir();        //Closes dirPath.
        editor.closeDir();        //Closes the root directory.

        return editor.closeEdit();
    }
	
	
	private static SVNCommitInfo modifyFile(ISVNEditor editor, String dirPath, String filePath, byte[] oldData, byte[] newData) 
			throws SVNException {
    
		editor.openRoot(-1 );
        editor.openDir(dirPath, -1 );
        editor.openFile(filePath, -1 );        
        editor.applyTextDelta(filePath, null );
        
        SVNDeltaGenerator deltaGenerator = new SVNDeltaGenerator();
        String checksum = deltaGenerator.sendDelta(filePath, new ByteArrayInputStream(oldData ), 0, new ByteArrayInputStream(newData ), editor, true );

        editor.closeFile(filePath, checksum );//Closes filePath.
        editor.closeDir();// Closes dirPath.
        editor.closeDir();//Closes the root directory.

       return editor.closeEdit();
   }
	
	private static SVNCommitInfo copyDir(ISVNEditor editor, String srcDirPath, String dstDirPath, long revision ) throws SVNException {
		editor.openRoot(-1 );        
        editor.addDir(dstDirPath, srcDirPath, revision );
        editor.closeDir(); //Closes dstDirPath.
        editor.closeDir();//Closes the root directory.
        return editor.closeEdit();
    }
	
	/**
	 * Directory deletion:
	 * @param editor
	 * @param dirPath
	 * @return
	 * @throws SVNException
	 */
	private static SVNCommitInfo deleteDir(ISVNEditor editor, String dirPath ) throws SVNException {
        editor.openRoot(-1 );
        editor.deleteEntry(dirPath, -1 );
        editor.closeDir();//Closes the root directory.
        return editor.closeEdit();
    }
	
	public static void main(String[] args) {
		try {
			FSRepositoryFactory.setup();
			
			SVNURL svnUrl = SVNURL.parseURIEncoded("file:///C:/Data/Env/SVNRepository/rytry/trunk/documents");			
			String userName="xbbkngi";
			char[] userPassword = "".toCharArray();
			
			byte[] contents = "This is a new file".getBytes();
			
			SVNRepository repository = SVNRepositoryFactory.create(svnUrl);			
			ISVNAuthenticationManager authManager = SVNWCUtil.createDefaultAuthenticationManager(userName, userPassword);
			repository.setAuthenticationManager(authManager );
			
			SVNNodeKind nodeKind = repository.checkPath("", -1);
			
			if (nodeKind == SVNNodeKind.NONE ) {
	            System.out.println("No entry at URL " + svnUrl);
	            System.exit(1 );
	        } else if (nodeKind == SVNNodeKind.FILE ) {
	            System.out.println("Entry at URL " + svnUrl + " is a file while directory was expected");
	            System.exit(1 );
	        }
			
			//Get exact value of the latest (HEAD) revision.
	         long latestRevision = repository.getLatestRevision();
	         System.out.println("Repository latest revision (before committing): " + latestRevision);
	         
	         ISVNEditor editor = repository.getCommitEditor("directory and file added", null);
	         
	         try {
	              SVNCommitInfo commitInfo = addDir(editor, "test", "test/file.txt", contents);
	              System.out.println("The directory was added: " + commitInfo );
	          } catch (SVNException svne ) {
	              editor.abortEdit();
	              throw svne;
	          }
	         
	         editor = repository.getCommitEditor("file contents changed", null);
			
		} catch (SVNException svne) {
			svne.printStackTrace();
		}
	}

}
