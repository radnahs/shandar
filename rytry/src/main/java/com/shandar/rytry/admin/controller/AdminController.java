/**
 *
 * <p>Project: trunk_rytry </p>
 * <p>Package Name: com.shandar.rytry.admin.controller </p>
 * <p>File Name: AdminController.java</p>
 * <p>Create Date: Jul 2, 2013 </p>
 * <p>Create Time: 12:50:14 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.admin.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.shandar.rytry.common.dataObject.SimpleDataObject;
import com.shandar.rytry.common.files.ServerFilesAndFolder;
import com.shandar.rytry.common.files.impl.WindowsFilesAndFolder;

/**
 * @author Shantanu Sikdar
 * 
 */
@Controller
public class AdminController {

	private static final Logger logger = LoggerFactory.getLogger(AdminController.class);

	/*@RequestMapping("/admin/AdminVu")*/
	@RequestMapping("AdminVu")
	// public String viewAdmin(HttpSession session) {
	public ModelAndView viewAdmin() {
		logger.debug("session in viewAdmin ");
		// return "admin/adminVu";
		Map<String, String> modelMap = new HashMap<String, String>();
		return new ModelAndView("admin/adminVu", modelMap);		
	}
	
	@RequestMapping("ViewServer")
	public ModelAndView viewServerFolderStructure() {
		logger.debug("session in viewAdmin ");		
		ServerFilesAndFolder sfaf = new WindowsFilesAndFolder(); 
		List<SimpleDataObject> listDrives = sfaf.listDrives();
		/*for (SimpleDataObject simpleDataObject : listDrives) {
			System.out.println("1 driveName = "+simpleDataObject.getAttribute("driveName"));
			System.out.println("1 driveDescription = "+simpleDataObject.getAttribute("driveDescription"));
		}*/		
		Map<String, List<SimpleDataObject>> modelMap = new HashMap<String, List<SimpleDataObject>>();		
		modelMap.put("listDrives", listDrives);
		System.out.println("listDrives = "+listDrives);
		return new ModelAndView("admin/viewServer", "modelMap" , modelMap);
	}

	@RequestMapping(value="formCentral",method=RequestMethod.GET)
	public ModelAndView formCentralExternalApp(){
		logger.debug("redirect to formCentral ");
		Map<String, String> modelMap = new HashMap<String, String>();
		modelMap.put("usrnm", "shanUsr");
		modelMap.put("pwwd", "shanPwd");
		//return new ModelAndView("redirect:http://localhost:8181/formCentral/Login","modelMap" , modelMap);
		return new ModelAndView("redirect:http://localhost:8181/formCentral/login?usrnm=\"shan\"&pwwd=\"shan\"");
		//return new ModelAndView("redirect:http://localhost:8181/formCentral");
	}
	
}
