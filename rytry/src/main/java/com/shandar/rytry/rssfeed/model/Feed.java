/**
 *
 * <p>Project: trunk_rytry </p>
 * <p>Package Name: com.shandar.rytry.rssfeed.model </p>
 * <p>File Name: Feed.java</p>
 * <p>Create Date: Jul 22, 2013 </p>
 * <p>Create Time: 2:45:16 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.rssfeed.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Shantanu Sikdar
 * 
 */
public class Feed{
	
	final String title;
	final String link;
	final String description;
	final String language;
	final String copyright;
	final String pubDate;
	
	final List<FeedMessage> entries = new ArrayList<FeedMessage>();
	
	 

	/**
	 * @param title
	 * @param link
	 * @param description
	 * @param language
	 * @param copyright
	 * @param pubDate
	 */
	public Feed(String title, String link, String description, String language,
			String copyright, String pubDate) {	
		this.title = title;
		this.link = link;
		this.description = description;
		this.language = language;
		this.copyright = copyright;
		this.pubDate = pubDate;
	}

	public String getTitle() {
		return title;
	}

	public String getLink() {
		return link;
	}

	public String getDescription() {
		return description;
	}

	public String getLanguage() {
		return language;
	}

	public String getCopyright() {
		return copyright;
	}

	public String getPubDate() {
		return pubDate;
	}

	public List<FeedMessage> getEntries() {
		return entries;
	}

	@Override
	public String toString() {
		return "Feed [title=" + title + ", link=" + link + ", description="
				+ description + ", language=" + language + ", copyright="
				+ copyright + ", pubDate=" + pubDate + ", entries=" + entries
				+ "]";
	}
	
}
