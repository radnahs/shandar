/**
 *
 * <p>Project: trunk_rytry </p>
 * <p>Package Name: com.shandar.rytry.rssfeed.manager </p>
 * <p>File Name: RSSFeedTest.java</p>
 * <p>Create Date: Jul 22, 2013 </p>
 * <p>Create Time: 3:02:38 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.rssfeed.manager;

import com.shandar.rytry.rssfeed.model.Feed;
import com.shandar.rytry.rssfeed.model.FeedMessage;

/**
 * @author Shantanu Sikdar
 * 
 */
public class RSSFeedTest {

	/**
	 * @see http://www.vogella.com/articles/RSSFeed/
	 * @param args
	 */
	public static void main(String[] args) {
		//RSSFeedParser parser = new RSSFeedParser("http://www.vogella.de/article.rss");
		RSSFeedParser parser = new RSSFeedParser("http://qa.bnymellonwealthmanagement.com/our-views/our-views.xml");		
		Feed feed = parser.readFeed();
		System.out.println(feed);
		for (FeedMessage message : feed.getEntries()) {
			System.out.println(message);
		}

	}

}
