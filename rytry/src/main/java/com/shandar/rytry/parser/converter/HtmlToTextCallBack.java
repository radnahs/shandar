/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.parser.converter </p>
 * <p>File Name: HtmlToTextCallBack.java</p>
 * <p>Create Date: Jan 24, 2013 </p>
 * <p>Create Time: 1:37:27 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.parser.converter;

import javax.swing.text.MutableAttributeSet;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLEditorKit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Shantanu Sikdar 
 *
 */
public class HtmlToTextCallBack extends HTMLEditorKit.ParserCallback{
	
	//private static final Logger logger = LoggerFactory.getLogger(HtmlToTextCallBack.class);
	StringBuilder result = new StringBuilder();

	public void handleComment(char[] data, int pos) {
		// insert code to handle comments
	}

	public void handleText(char[] data, int pos) {
		// insert code to handle comments
		try {
			String s = new String(data);
			result.append(s);
			
		} catch (Exception e) {
			//logger.error(""+e);
		}
	}

	public void handleEndTag(HTML.Tag t, int pos) {
		// insert code to handle ending tags
		
	}

	public void handleError(String errorMsg, int pos) {
		// insert error handling code
	}

	public void handleSimpleTag(HTML.Tag t, MutableAttributeSet a, int pos) {
		// insert code to handle simple tags
		
		try {
			if (t == HTML.Tag.BR) {
				result.append("\n");
			}
		} catch (Exception e) {
			//logger.error(""+e);
		}
	}

	public void handleStartTag(HTML.Tag t, MutableAttributeSet a, int pos) {
		
		try {
			if (t.breaksFlow()) {
				
				result.append("\n");
			} else if (t.isBlock()) {
				
				result.append("\n\n");
			} else if (t == HTML.Tag.P) {
				result.append("\n");
			}
			
		} catch (Exception e) {
			//logger.error(""+e);
		}

	}

	public String getResult() {
		return result.toString();
	}


}
