/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.parser.converter </p>
 * <p>File Name: HTMLToPlainTextConverter.java</p>
 * <p>Create Date: Jan 24, 2013 </p>
 * <p>Create Time: 1:07:50 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.parser.converter;

import java.io.Reader;
import java.io.StringReader;

import javax.swing.text.html.HTMLEditorKit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Shantanu Sikdar 
 *
 * reference link : http://www.java2s.com/Tutorial/Java/0120__Development/UsejavaxswingtexthtmlHTMLEditorKittoparseHTML.htm
 * 
 * 
 */
public class HTMLToPlainTextConverter {

	//private static final Logger logger = LoggerFactory.getLogger(HTMLToPlainTextConverter.class);
	
	public String convertText(String html) {
		String result = "";
		try {
			result = getResult(html);
		} catch (Exception e) {
			//logger.error("Error in convertText ", e);
		}
		return result;
	}
	
	public String getResult(String content) {
		String result = "";
		try {
			Reader r = new StringReader(content);
			// remove title tag to avoid name getting picked from title
			//content = content.replaceAll("(?mi)<title>.*?</title>", "");
			HtmlToTextCallBack callback = new HtmlToTextCallBack();
			
			HTMLEditorKit.Parser parser = new HTMLEditorParser().getParser();						
			parser.parse(new StringReader(content), callback, true);
			result = callback.getResult();			
			result = result.trim();
		} catch (Exception e) {			
			//logger.error("Error in getResult ", e);
			e.printStackTrace();
		}
		return result;
	}
	
	class HTMLEditorParser extends HTMLEditorKit{
		public HTMLEditorKit.Parser getParser() {
			return super.getParser();
		}
    }


}
