/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.parser </p>
 * <p>File Name: XMLParsing.java</p>
 * <p>Create Date: Feb 13, 2014 </p>
 * <p>Create Time: 12:26:08 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.parser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;

/**
 * @author Shantanu Sikdar 
 *
 */
public class XMLParsing {
	
	public static final String LINE = "Line 1\nLine 2\nLine 3\nLine 4\n";

	public static void main(String[] args) {
		try ( BufferedReader in = new BufferedReader(new StringReader(LINE))) {
			
            System.out.println(in.readLine());
            in.mark(0);                 // mark 'Line 2'
            System.out.println(in.readLine());
            System.out.println(in.readLine());
            in.reset();                 // reset 'Line 2'
            System.out.println(in.readLine());
            in.reset();                 // reset 'Line 2'
            System.out.println(in.readLine());
            in.mark(0);                 // mark 'Line 3'
            System.out.println(in.readLine());
            in.reset();                 // reset 'Line 3'
            System.out.println(in.readLine());
            System.out.println(in.readLine());

        } catch (IOException e) {
            e.printStackTrace();
        }
		
	}
	
	
	/*
	parse(){
		
	}*/

}
