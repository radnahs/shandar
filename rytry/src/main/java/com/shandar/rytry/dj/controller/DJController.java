/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.dj.controller </p>
 * <p>File Name: DJController.java</p>
 * <p>Create Date: Apr 15, 2014 </p>
 * <p>Create Time: 8:02:02 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.dj.controller;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shandar.rytry.dj.manager.DJReportInterface;
import com.shandar.rytry.dj.manager.impl.DJAIS;
import com.shandar.rytry.login.model.User;

/**
 * @author Shantanu Sikdar 
 *
 */

@Controller
public class DJController {
	
	private static final Logger logger = LoggerFactory.getLogger(DJController.class);
	
	@RequestMapping( value="DynJaspr")
    public String dynJasprView(User user, BindingResult result, Map model, HttpSession session) {
		
        return "dynJaspr/dynJaspr";
    }
	
	@RequestMapping( value="DJReportXLS")
    public @ResponseBody String djReportXLS(User user, BindingResult result, Map model, HttpSession session) {
		DJReportInterface djrI = new DJAIS();
		String fileName = djrI.generateDJReport("XLS");
        return fileName;
    }
	
	@RequestMapping( value="DJReportPDF")
    public @ResponseBody String djReportPDF(User user, BindingResult result, Map model, HttpSession session) {
		DJReportInterface djrI = new DJAIS();
		String fileName = djrI.generateDJReport("PDF");        
        return fileName;
    }
	
	public static void main(String[] args) {
		String fileNameX =new DJAIS().generateDJReport("XLS");
		String fileNameP =new DJAIS().generateDJReport("PDF");
	}
	
	
}
