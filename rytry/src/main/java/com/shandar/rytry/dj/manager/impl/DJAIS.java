/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.dj.manager </p>
 * <p>File Name: DJAIS.java</p>
 * <p>Create Date: Apr 22, 2014 </p>
 * <p>Create Time: 4:55:02 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.dj.manager.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ar.com.fdvs.dj.core.layout.HorizontalBandAlignment;
import ar.com.fdvs.dj.domain.AutoText;
import ar.com.fdvs.dj.domain.Style;
import ar.com.fdvs.dj.domain.builders.ColumnBuilder;
import ar.com.fdvs.dj.domain.builders.DynamicReportBuilder;
import ar.com.fdvs.dj.domain.constants.Font;
import ar.com.fdvs.dj.domain.constants.HorizontalAlign;
import ar.com.fdvs.dj.domain.constants.Page;
import ar.com.fdvs.dj.domain.constants.VerticalAlign;

import com.shandar.rytry.dj.manager.DJReportHelper;
import com.shandar.rytry.dj.manager.DJReportInterface;

/**
 * @author Shantanu Sikdar
 * 
 */
public class DJAIS extends DJReportHelper implements DJReportInterface{

	private static final Logger logger = LoggerFactory.getLogger(DJAIS.class);
	
	@Override
	public String generateDJReport(String reportType){
		String fileName = null;
		try {
			
			Style titleStyle = basicStyle("titleStyle", 20, Font._FONT_VERDANA, true, 2.0f, VerticalAlign.MIDDLE, HorizontalAlign.CENTER);			
			Style columnHeaderStyle = basicStyle("columnHeaderStyle", 14, Font._FONT_ARIAL, true, 0.0f, VerticalAlign.MIDDLE, HorizontalAlign.CENTER);			
			Style columnValueStyle = basicStyle("columnValueStyle", 10, Font._FONT_ARIAL, true, 0.0f, VerticalAlign.MIDDLE, HorizontalAlign.RIGHT);
						
			Integer rightMargin = new Integer(20);
			Integer leftMargin = new Integer(20);

			DynamicReportBuilder dynamicReportBuilder = new DynamicReportBuilder();

			// configure report-level settings
			dynamicReportBuilder.setTitle("Consolidated Cash and Holdings Statement")
								.setTitleStyle(titleStyle).setDetailHeight(new Integer(15))
								.setLeftMargin(rightMargin).setRightMargin(leftMargin);

			dynamicReportBuilder.setPageSizeAndOrientation(Page.Page_Letter_Landscape());

			// configure header
			dynamicReportBuilder.addAutoText(new AutoText("Consolidated Cash and Holdings Statement", AutoText.POSITION_HEADER,
							HorizontalBandAlignment.LEFT, 400));

			// add ISIN CUSIP column to report
			ColumnBuilder columnBuilderISIN_CUSIP = basicColumnBuilder("ISIN CUSIP",columnHeaderStyle,"ISIN CUSIP",String.class.getName(),"@isin",columnHeaderStyle);
			dynamicReportBuilder.addColumn(columnBuilderISIN_CUSIP.build());

			// add description column to report
			ColumnBuilder columnBuilderDescription = basicColumnBuilder("Description",columnHeaderStyle,"Description",String.class.getName(),"@description",columnValueStyle);
			dynamicReportBuilder.addColumn(columnBuilderDescription.build());

			// add email column to report
			ColumnBuilder columnBuilderAmortized = basicColumnBuilder("Units Amortized Units",columnHeaderStyle,"Amortized",String.class.getName(),"@amortized",columnValueStyle);
			dynamicReportBuilder.addColumn(columnBuilderAmortized.build());

			// add "Price Date Price Source Market Price" column to report
			ColumnBuilder columnBuilderDateSrcPrice = basicColumnBuilder("Price Date Price Source Market Price",
					columnHeaderStyle,"Price Date Price Source Market Price",String.class.getName(),"@datesrcprice",columnValueStyle);
			dynamicReportBuilder.addColumn(columnBuilderDateSrcPrice.build());

			// add "Market Value Local" column to report
			ColumnBuilder columnBuilderMVLocal = basicColumnBuilder("Market Value Local",columnHeaderStyle,"Market Value Local",String.class.getName(),"@mv_local",columnValueStyle);
			dynamicReportBuilder.addColumn(columnBuilderMVLocal.build());

			// add "Market Value Reporting Currency" column to report
			ColumnBuilder columnBuilderMVReportingCurrency = basicColumnBuilder("Market Value Reporting Currency",columnHeaderStyle,
					"Market Value Reporting Currency",String.class.getName(),"@mv_rep_curr",columnValueStyle);
			dynamicReportBuilder.addColumn(columnBuilderMVReportingCurrency.build());
			
			fileName = generateReport(reportType, dynamicReportBuilder);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fileName;
	}
}
