/**
 *
 * <p>Project: trunk_rytry </p>
 * <p>Package Name: com.shandar.rytry.dj </p>
 * <p>File Name: SimpleReportExample.java</p>
 * <p>Create Date: Dec 18, 2013 </p>
 * <p>Create Time: 4:01:50 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.dj;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRXmlDataSource;
import net.sf.jasperreports.engine.export.JExcelApiExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import ar.com.fdvs.dj.core.DynamicJasperHelper;
import ar.com.fdvs.dj.core.layout.ClassicLayoutManager;
import ar.com.fdvs.dj.core.layout.HorizontalBandAlignment;
import ar.com.fdvs.dj.domain.AutoText;
import ar.com.fdvs.dj.domain.DynamicReport;
import ar.com.fdvs.dj.domain.Style;
import ar.com.fdvs.dj.domain.builders.ColumnBuilder;
import ar.com.fdvs.dj.domain.builders.DynamicReportBuilder;
import ar.com.fdvs.dj.domain.constants.Border;
import ar.com.fdvs.dj.domain.constants.Font;
import ar.com.fdvs.dj.domain.constants.HorizontalAlign;
import ar.com.fdvs.dj.domain.constants.Page;
import ar.com.fdvs.dj.domain.constants.VerticalAlign;

/**
 * Following is the data.xml file.
 	<?xml version="1.0" encoding="UTF-8"?>
	   <employees>
		<employee id="1001" name="shantanu" email="shsikdar@inautix.co.in" salary="2000"/>
	    <employee id="1002" name="mahesh" email="mthakur@inautix.co.in" salary="1000"/>
	    <employee id="1003" name="chandu" email="shsikdar@inautix.co.in" salary="4000"/>
	    <employee id="1004" name="Neal Gafter" email="nealgafter@inautix.co.in" salary="20000"/>
	    <employee id="1005" name="Josh Bloch" email="joshbloch@inautix.co.in" salary="30000"/>
	  </employees>

 */

/**
 * @author Shantanu Sikdar
 * 
 */
public class SimpleDJReport {
	
	public static void main(String[] args) {
		try {
			
			Style titleStyle = new Style("titleStyle");
			titleStyle.setFont(new Font(18, Font._FONT_VERDANA, true));
			titleStyle.setBorder(new Border(2.0f));
			titleStyle.setVerticalAlign(VerticalAlign.MIDDLE);
			titleStyle.setHorizontalAlign(HorizontalAlign.CENTER);
			
			
			Style headerStyle = new Style("header");
			headerStyle.setFont(Font.ARIAL_MEDIUM_BOLD);			
			headerStyle.setHorizontalAlign(HorizontalAlign.CENTER);
			headerStyle.setVerticalAlign(VerticalAlign.MIDDLE);
			//headerStyle.setBorder(new Border(1.5f));
			
			Style valueStyle = new Style("valueStyle");
			valueStyle.setHorizontalAlign(HorizontalAlign.RIGHT);
			
			
			Integer rightMargin = new Integer(20);
			Integer leftMargin = new Integer(20);
			
			
			DynamicReportBuilder dynamicReportBuilder = new DynamicReportBuilder();
		 
		    // configure report-level settings
		    dynamicReportBuilder.setTitle("Consolidated Cash and Holdings Statement")		    						
		    					.setTitleStyle(titleStyle)
		    					.setDetailHeight(new Integer(15))
		    					.setLeftMargin(rightMargin)
		    					.setRightMargin(leftMargin);
		 
		    dynamicReportBuilder.setPageSizeAndOrientation(Page.Page_Letter_Landscape());
		 
		    // configure header
		    dynamicReportBuilder.addAutoText(new AutoText(
		        "Consolidated Cash and Holdings Statement", AutoText.POSITION_HEADER, HorizontalBandAlignment.LEFT, 400));
		 
		    // add ISIN CUSIP column to report
		    ColumnBuilder columnBuilderISIN_CUSIP = ColumnBuilder.getNew();
		    
		    columnBuilderISIN_CUSIP.setTitle("ISIN CUSIP").setHeaderStyle(headerStyle);
		    columnBuilderISIN_CUSIP.setWidth(120);
		    columnBuilderISIN_CUSIP.setFixedWidth(true);		    		    
		    columnBuilderISIN_CUSIP.setColumnProperty("ISIN CUSIP", String.class.getName(), "@isin");
		    				
		 
		    dynamicReportBuilder.addColumn(columnBuilderISIN_CUSIP.build());
		 
		    // add description column to report
		    ColumnBuilder columnBuilderDescription = ColumnBuilder.getNew();
		 
		    columnBuilderDescription.setTitle("Description").setHeaderStyle(headerStyle);
		    columnBuilderDescription.setWidth(120);
		    columnBuilderDescription.setFixedWidth(true);
		    columnBuilderDescription.setStyle(valueStyle);
		    columnBuilderDescription.setColumnProperty("Description", String.class.getName(), "@description");
		 
		    dynamicReportBuilder.addColumn(columnBuilderDescription.build());
		 
		    // add email column to report
		    ColumnBuilder columnBuilderAmortized = ColumnBuilder.getNew();
		 
		    columnBuilderAmortized.setTitle("Units Amortized Units").setHeaderStyle(headerStyle);
		    columnBuilderAmortized.setWidth(120);
		    columnBuilderAmortized.setFixedWidth(true);		   
		    columnBuilderAmortized.setStyle(valueStyle);
		    columnBuilderAmortized.setColumnProperty("Amortized", String.class.getName(), "@amortized");		    				  
		 
		    dynamicReportBuilder.addColumn(columnBuilderAmortized.build());
		 
		    // add salary column to report
		    ColumnBuilder columnBuilderDateSrcPrice = ColumnBuilder.getNew();
		 
		    columnBuilderDateSrcPrice.setTitle("Price Date Price Source Market Price").setHeaderStyle(headerStyle);
		    columnBuilderDateSrcPrice.setWidth(120);
		    columnBuilderDateSrcPrice.setFixedWidth(true);
		    columnBuilderDateSrcPrice.setStyle(valueStyle);
		    columnBuilderDateSrcPrice.setColumnProperty("Price Date Price Source Market Price", String.class.getName(), "@datesrcprice");
		    				   
		    dynamicReportBuilder.addColumn(columnBuilderDateSrcPrice.build());
		    
		    // add salary column to report
		    ColumnBuilder columnBuilderMVLocal = ColumnBuilder.getNew();
		 
		    columnBuilderMVLocal.setTitle("Market Value Local").setHeaderStyle(headerStyle);
		    columnBuilderMVLocal.setWidth(120);
		    columnBuilderMVLocal.setFixedWidth(true);
		    columnBuilderMVLocal.setStyle(valueStyle);
		    columnBuilderMVLocal.setColumnProperty("Market Value Local", String.class.getName(), "@mv_local");
		    				   
		    dynamicReportBuilder.addColumn(columnBuilderMVLocal.build());
		    
		    
		    // add salary column to report
		    ColumnBuilder columnBuilderMVReportingCurrency = ColumnBuilder.getNew();
		 
		    columnBuilderMVReportingCurrency.setTitle("Market Value Reporting Currency").setHeaderStyle(headerStyle);
		    columnBuilderMVReportingCurrency.setWidth(120);
		    columnBuilderMVReportingCurrency.setFixedWidth(true);
		    columnBuilderMVReportingCurrency.setStyle(valueStyle);
		    columnBuilderMVReportingCurrency.setColumnProperty("Market Value Reporting Currency", String.class.getName(), "@mv_rep_curr");
		    				   
		    dynamicReportBuilder.addColumn(columnBuilderMVReportingCurrency.build());

		 
		    /*AbstractColumn columnSalary = columnBuilderDateSrcPrice.build();
		 
		    dynamicReportBuilder.addColumn(columnSalary);*/
		 
		    // add a footer variable to handle total calculation
		    /*dynamicReportBuilder.addGlobalFooterVariable(columnSalary, DJCalculation.SUM);
		    dynamicReportBuilder.setGrandTotalLegend("Total:");*/
		 
		    DynamicReport dynamicReport = dynamicReportBuilder.build();
		 
		    // build a datasource representing the XML file
		    JRDataSource dataSource = new JRXmlDataSource(new File("c:\\data\\ccahs.xml"), "//statement");
		 
		    // build JasperPrint instance, filling the report with data from datasource created above
		    JasperPrint jasperPrint = DynamicJasperHelper.generateJasperPrint(
		        dynamicReport, new ClassicLayoutManager(), dataSource, new HashMap<String, Object>());
		 
		    // export to pdf
		    String pdfFile = "shantanu group---"+Math.round(Math.random() * 100000) + ".pdf";
		 
		    JRExporter exporter = new JRPdfExporter();
		 
		    exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
		    exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, pdfFile);
		 
		    exporter.exportReport();
		    
		    
		    // export to xls
		    String xlsFile = "shantanu---"+Math.round(Math.random() * 100000) + ".xls";
		    
		    //JRExporter xlsExporter = new JRXlsExporter();
		    JExcelApiExporter xlsExporter = new JExcelApiExporter();
		    			 
		    xlsExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
		    xlsExporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, xlsFile);
		 
		    xlsExporter.exportReport();
		 
		} catch(JRException e) {
		    e.printStackTrace();
		}
	}
	
	public static void main2(String[] args) {
		try {
			List<Map<String, Object>> resultSetData = new ArrayList<>();
			Map<String, Object> mappedData = new HashMap<>();
			mappedData.put("Id", "id");
			mappedData.put("Name", "name");
			mappedData.put("Email", "email");
			mappedData.put("Salary", "salary");			
			//JRDataSource dataSource1 = new JRXmlDataSource(new File("c:\\data\\data.xml"), "//employee");
			
			DynamicReportBuilder dynamicReportBuilder = buildReportColumns(mappedData);
		 
		    DynamicReport dynamicReport = dynamicReportBuilder.build();
		 
		    // build a datasource representing the XML file
		    JRDataSource dataSource = new JRXmlDataSource(new File("c:\\data\\data.xml"), "//employee");
		 
		    // build JasperPrint instance, filling the report with data from datasource created above
		    JasperPrint jasperPrint = DynamicJasperHelper.generateJasperPrint(
		        dynamicReport, new ClassicLayoutManager(), dataSource, new HashMap<String, Object>());

		    System.out.println(" start pdf "+ Calendar.getInstance().getTime());
		    
		    // export to pdf
		    String pdfFile = "shantanu---"+Math.round(Math.random() * 100000) + ".pdf";
		 
		    JRExporter pdfExporter = new JRPdfExporter();
		 
		    pdfExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
		    pdfExporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, pdfFile);
		 
		    pdfExporter.exportReport();
		    
		    System.out.println(" end pdf start xls "+ Calendar.getInstance().getTime());
		    
		    // export to xls
		    String xlsFile = "shantanu---"+Math.round(Math.random() * 100000) + ".xls";
		    
		    //JRExporter xlsExporter = new JRXlsExporter();
		    JExcelApiExporter xlsExporter = new JExcelApiExporter();
		    			 
		    xlsExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
		    xlsExporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, xlsFile);
		 
		    xlsExporter.exportReport();
		    
		    System.out.println(" end "+ Calendar.getInstance().getTime());
		 
		} catch(JRException e) {
		    e.printStackTrace();
		}
		
	}
	
	
	public static DynamicReportBuilder buildReportColumns(Map<String, Object> mappedData){
		DynamicReportBuilder dynamicReportBuilder = new DynamicReportBuilder();
		try {   
		    dynamicReportBuilder.setReportName("Shantanu DJ Report");
		    dynamicReportBuilder.setPageSizeAndOrientation(Page.Page_Letter_Landscape());
		    	    	
	    	ColumnBuilder columnBuilder = ColumnBuilder.getNew();
	    	for(String columnName : mappedData.keySet()){
	    		columnBuilder.setTitle(columnName);
	    		columnBuilder.setWidth(180);
	    		columnBuilder.setFixedWidth(true);
	    		columnBuilder.setColumnProperty(columnName, String.class.getName(), "@"+mappedData.get(columnName));
	    		dynamicReportBuilder.addColumn(columnBuilder.build());
	    	}		    	
		} catch (Exception e) {
			e.printStackTrace();	
		}
		return dynamicReportBuilder;
	}
	
	/***
	 * original piece of shit :)
	 * 
	 */
	public static void main1(String[] args) {
		try {
		    DynamicReportBuilder dynamicReportBuilder = new DynamicReportBuilder();
		 
		    // configure report-level settings
		    dynamicReportBuilder.setReportName("Shantanu DJ Report");
		 
		    dynamicReportBuilder.setPageSizeAndOrientation(Page.Page_Letter_Landscape());
		 
		    // add id column to report
		    ColumnBuilder columnBuilderID = ColumnBuilder.getNew();
		 
		    columnBuilderID.setTitle("ID");
		    columnBuilderID.setWidth(180);
		    columnBuilderID.setFixedWidth(true);
		    columnBuilderID.setColumnProperty("ID", Integer.class.getName(), "@id");
		 
		    dynamicReportBuilder.addColumn(columnBuilderID.build());
		 
		    // add name column to report
		    ColumnBuilder columnBuilderName = ColumnBuilder.getNew();
		 
		    columnBuilderName.setTitle("Name");
		    columnBuilderName.setWidth(180);
		    columnBuilderName.setFixedWidth(true);
		    columnBuilderName.setColumnProperty("Name", String.class.getName(), "@name");
		 
		    dynamicReportBuilder.addColumn(columnBuilderName.build());
		 
		    // add email column to report
		    ColumnBuilder columnBuilderEmail = ColumnBuilder.getNew();
		 
		    columnBuilderEmail.setTitle("Email");
		    columnBuilderEmail.setWidth(180);
		    columnBuilderEmail.setFixedWidth(true);
		    columnBuilderEmail.setColumnProperty("Email", String.class.getName(), "@email");
		 
		    dynamicReportBuilder.addColumn(columnBuilderEmail.build());
		 
		    // add salary column to report
		    ColumnBuilder columnBuilderSalary = ColumnBuilder.getNew();
		 
		    columnBuilderSalary.setTitle("Salary");
		    columnBuilderSalary.setWidth(180);
		    columnBuilderSalary.setFixedWidth(true);
		    columnBuilderSalary.setColumnProperty("Salary", Integer.class.getName(), "@salary");
		 
		    dynamicReportBuilder.addColumn(columnBuilderSalary.build());
		 
		    DynamicReport dynamicReport = dynamicReportBuilder.build();
		 
		    // build a datasource representing the XML file
		    JRDataSource dataSource = new JRXmlDataSource(new File("c:\\data\\data.xml"), "//employee");
		 
		    // build JasperPrint instance, filling the report with data from datasource created above
		    JasperPrint jasperPrint = DynamicJasperHelper.generateJasperPrint(
		        dynamicReport, new ClassicLayoutManager(), dataSource, new HashMap<String, Object>());

		    System.out.println(" start pdf "+ Calendar.getInstance().getTime());
		    
		    // export to pdf
		    String pdfFile = Math.round(Math.random() * 100000) + ".pdf";
		 
		    JRExporter pdfExporter = new JRPdfExporter();
		 
		    pdfExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
		    pdfExporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, pdfFile);
		 
		    pdfExporter.exportReport();
		    
		    System.out.println(" end pdf start xls "+ Calendar.getInstance().getTime());
		    
		    // export to xls
		    String xlsFile = Math.round(Math.random() * 100000) + ".xls";
		    
		    //JRExporter xlsExporter = new JRXlsExporter();
		    JExcelApiExporter xlsExporter = new JExcelApiExporter();
		    			 
		    xlsExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
		    xlsExporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, xlsFile);
		 
		    xlsExporter.exportReport();
		    
		    System.out.println(" end "+ Calendar.getInstance().getTime());
		 
		} catch(JRException e) {
		    e.printStackTrace();
		}
		
	}

}
