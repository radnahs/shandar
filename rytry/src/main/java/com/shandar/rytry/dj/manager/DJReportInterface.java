/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.dj.manager </p>
 * <p>File Name: DJReport.java</p>
 * <p>Create Date: Apr 23, 2014 </p>
 * <p>Create Time: 6:45:42 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.dj.manager;

/**
 * @author Shantanu Sikdar 
 *
 */
public interface DJReportInterface {
	
	public String generateDJReport(String reportType);
}
