/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.dj.utils </p>
 * <p>File Name: DJUtils.java</p>
 * <p>Create Date: Apr 23, 2014 </p>
 * <p>Create Time: 6:13:47 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.dj.manager;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRPrintPage;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRXmlDataSource;
import net.sf.jasperreports.engine.export.JExcelApiExporter;
import net.sf.jasperreports.engine.export.JRAbstractCsvExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import ar.com.fdvs.dj.core.DynamicJasperHelper;
import ar.com.fdvs.dj.core.layout.ClassicLayoutManager;
import ar.com.fdvs.dj.domain.DynamicReport;
import ar.com.fdvs.dj.domain.Style;
import ar.com.fdvs.dj.domain.builders.ColumnBuilder;
import ar.com.fdvs.dj.domain.builders.DynamicReportBuilder;
import ar.com.fdvs.dj.domain.constants.Border;
import ar.com.fdvs.dj.domain.constants.Font;
import ar.com.fdvs.dj.domain.constants.HorizontalAlign;
import ar.com.fdvs.dj.domain.constants.VerticalAlign;

/**
 * @author Shantanu Sikdar 
 *
 */
public abstract class DJReportHelper {
	
	/**
	 * 
	 * @param reportMake
	 * @param dynamicReportBuilder
	 */
	public String generateReport(String reportMake, DynamicReportBuilder dynamicReportBuilder){
		String fileName = null;
		JasperPrint jasperPrint = xmlDataSource(dynamicReportBuilder);		
		if(reportMake.equalsIgnoreCase("PDF")){
			fileName = getPDF(jasperPrint);
		}else if(reportMake.equalsIgnoreCase("XLS")){
			fileName = getXLS(jasperPrint);
		}
		return fileName;
	}
	
	/**
	 * 
	 * @param title
	 * @param columnHeaderStyle
	 * @param propertyName
	 * @param valueClassName
	 * @param fieldDescription
	 * @param columnValueStyle
	 * @return
	 */
	public static ColumnBuilder basicColumnBuilder(String title, Style columnHeaderStyle, String propertyName, String valueClassName, String fieldDescription, Style columnValueStyle){
		ColumnBuilder columnBuilder = ColumnBuilder.getNew();
		columnBuilder.setTitle(title).setHeaderStyle(columnHeaderStyle);
		columnBuilder.setWidth(120);
		columnBuilder.setFixedWidth(true);		
		columnBuilder.setColumnProperty(propertyName, String.class.getName(), fieldDescription).setStyle(columnValueStyle);
		return columnBuilder;
	}
	
	/**
	 * 
	 * @param styleName
	 * @param fontSize
	 * @param fontName
	 * @param bold
	 * @param width
	 * @param verticalAlign
	 * @param horizontalAlign
	 * @return
	 */
	public static Style basicStyle(String styleName,int fontSize, String fontName, boolean bold,float width,VerticalAlign verticalAlign, HorizontalAlign horizontalAlign){
		Style style = new Style(styleName);
		style.setFont(new Font(fontSize, fontName, bold));
		style.setBorder(new Border(width));
		style.setVerticalAlign(verticalAlign);
		style.setHorizontalAlign(horizontalAlign);		
		return style;
	}
	

	/**
	 * build a datasource representing the XML file 
	 * @param dynamicReportBuilder
	 * @return
	 */
	private static JasperPrint xmlDataSource(DynamicReportBuilder dynamicReportBuilder){
		JRDataSource dataSource = null;
		JasperPrint jasperPrint = null;
		try {			
			dataSource = new JRXmlDataSource(new File("c:\\data\\ccahs.xml"), "//statement");
			jasperPrint = fillingReportFromDatasource(dynamicReportBuilder,dataSource);
		} catch (JRException e) {
			e.printStackTrace();
		}		
		return jasperPrint;
	}

	/**
	 * build JasperPrint instance, filling the report with data from datasource created above
	 * @param dynamicReportBuilder
	 * @param dataSource
	 * @return
	 */
	private static JasperPrint fillingReportFromDatasource(DynamicReportBuilder dynamicReportBuilder, JRDataSource dataSource){		
		JasperPrint jasperPrint = null;
		DynamicReport dynamicReport = null;
		try {
			dynamicReport = dynamicReportBuilder.build();
			jasperPrint = DynamicJasperHelper.generateJasperPrint(dynamicReport, new ClassicLayoutManager(), dataSource,
					new HashMap<String, Object>());
		} catch (JRException e) {
			e.printStackTrace();
		}
		return jasperPrint;
	}
				
	/**
	 * export to pdf
	 * @param jasperPrint
	 */
	private static String getPDF(JasperPrint jasperPrint) {
		String pdfFile = null;
		try {			
			pdfFile = "shantanu group---" + Math.round(Math.random() * 100000) + ".pdf";
			JRExporter exporter = new JRPdfExporter();
			exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
			exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, pdfFile);
			exporter.exportReport();
		} catch (JRException e) {
			e.printStackTrace();
		}
		return pdfFile;
	}
	
	/**
	 * export to xls
	 * @param jasperPrint
	 */
	private static String getXLS(JasperPrint jasperPrint) {
		String xlsFile = null;
		try {			
			xlsFile = "shantanu---" + Math.round(Math.random() * 100000)	+ ".xls";
			JExcelApiExporter xlsExporter = new JExcelApiExporter();
			xlsExporter.setParameter(JRExporterParameter.JASPER_PRINT,	jasperPrint);
			xlsExporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME,	xlsFile);
			xlsExporter.exportReport();
		} catch (JRException e) {
			e.printStackTrace();
		}
		return xlsFile;
	}
	
	private static String getCSV(JasperPrint jasperPrint) {
		String xlsFile = null;
		try {			
			xlsFile = "shantanu---" + Math.round(Math.random() * 100000)	+ ".csv";
			JExcelApiExporter xlsExporter = new JExcelApiExporter();
			JRExporter csvExporter = new JRAbstractCsvExporter() {
				
				@Override
				protected void exportPage(JRPrintPage page) throws IOException {
					// TODO Auto-generated method stub
					
				}

				@Override
				protected void ensureOutput() {
					// TODO Auto-generated method stub
					
				}

				@Override
				protected Class getConfigurationInterface() {
					// TODO Auto-generated method stub
					return null;
				}

				@Override
				protected Class getItemConfigurationInterface() {
					// TODO Auto-generated method stub
					return null;
				}
			};
			xlsExporter.setParameter(JRExporterParameter.JASPER_PRINT,	jasperPrint);
			xlsExporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME,	xlsFile);
			xlsExporter.exportReport();
		} catch (JRException e) {
			e.printStackTrace();
		}
		return xlsFile;
	}

}
