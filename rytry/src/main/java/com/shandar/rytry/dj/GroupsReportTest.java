/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.dj </p>
 * <p>File Name: GroupsReportTest.java</p>
 * <p>Create Date: Apr 8, 2014 </p>
 * <p>Create Time: 5:37:57 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.dj;

import java.awt.Color;

import net.sf.jasperreports.view.JasperDesignViewer;
import net.sf.jasperreports.view.JasperViewer;
import ar.com.fdvs.dj.domain.AutoText;
import ar.com.fdvs.dj.domain.DJCalculation;
import ar.com.fdvs.dj.domain.DynamicReport;
import ar.com.fdvs.dj.domain.Style;
import ar.com.fdvs.dj.domain.builders.ColumnBuilder;
import ar.com.fdvs.dj.domain.builders.DynamicReportBuilder;
import ar.com.fdvs.dj.domain.builders.GroupBuilder;
import ar.com.fdvs.dj.domain.constants.Border;
import ar.com.fdvs.dj.domain.constants.Font;
import ar.com.fdvs.dj.domain.constants.GroupLayout;
import ar.com.fdvs.dj.domain.constants.HorizontalAlign;
import ar.com.fdvs.dj.domain.constants.Transparency;
import ar.com.fdvs.dj.domain.constants.VerticalAlign;
import ar.com.fdvs.dj.domain.entities.DJGroup;
import ar.com.fdvs.dj.domain.entities.columns.AbstractColumn;
import ar.com.fdvs.dj.domain.entities.columns.PropertyColumn;

/**
 * @author Shantanu Sikdar
 * 
 */
public class GroupsReportTest extends BaseDjReportTest {

	public DynamicReport buildReport() throws Exception {
		Style detailStyle = new Style("detail");

		Style headerStyle = new Style("header");
		headerStyle.setFont(Font.ARIAL_MEDIUM_BOLD);
		headerStyle.setBorderBottom(Border.PEN_1_POINT());
		headerStyle.setBackgroundColor(Color.gray);
		headerStyle.setTextColor(Color.white);
		headerStyle.setHorizontalAlign(HorizontalAlign.CENTER);
		headerStyle.setVerticalAlign(VerticalAlign.MIDDLE);
		headerStyle.setTransparency(Transparency.OPAQUE);

		Style headerVariables = new Style("headerVariables");
		headerVariables.setFont(Font.ARIAL_BIG_BOLD);
		headerVariables.setBorderBottom(Border.THIN());
		headerVariables.setHorizontalAlign(HorizontalAlign.RIGHT);
		headerVariables.setVerticalAlign(VerticalAlign.TOP);
		headerVariables.setStretchWithOverflow(true);

		Style groupVariables = new Style("groupVariables");
		groupVariables.setFont(Font.ARIAL_MEDIUM_BOLD);
		groupVariables.setTextColor(Color.BLUE);
		groupVariables.setBorderBottom(Border.THIN());
		groupVariables.setHorizontalAlign(HorizontalAlign.RIGHT);
		groupVariables.setVerticalAlign(VerticalAlign.BOTTOM);

		Style titleStyle = new Style("titleStyle");
		titleStyle.setFont(new Font(18, Font._FONT_VERDANA, true));
		Style importeStyle = new Style();
		importeStyle.setHorizontalAlign(HorizontalAlign.RIGHT);
		Style oddRowStyle = new Style();
		oddRowStyle.setBorder(Border.NO_BORDER());
		oddRowStyle.setBackgroundColor(Color.LIGHT_GRAY);
		oddRowStyle.setTransparency(Transparency.OPAQUE);

		DynamicReportBuilder drb = new DynamicReportBuilder();
		Integer margin = new Integer(20);
		drb.setTitleStyle(titleStyle)
				.setTitle("Consolidated Cash and Holdings Statement")
				// defines the title of the report
				.setSubtitle("Summary of asset held")
				.setDetailHeight(new Integer(15)).setLeftMargin(margin)
				.setRightMargin(margin).setTopMargin(margin)
				.setBottomMargin(margin).setPrintBackgroundOnOddRows(true)
				.setGrandTotalLegend("Grand Total")
				.setGrandTotalLegendStyle(headerVariables)
				.setOddRowBackgroundStyle(oddRowStyle);

		AbstractColumn columnAssetCategory = ColumnBuilder.getNew()
				.setColumnProperty("asset_category", String.class.getName())
				.setTitle("Asset Category").setWidth(new Integer(85))
				.setStyle(titleStyle).setHeaderStyle(titleStyle).build();

		AbstractColumn columnSharesUnits = ColumnBuilder.getNew()
				.setColumnProperty("shares_units", String.class.getName())
				.setTitle("Shares/Units").setWidth(new Integer(85))
				.setStyle(detailStyle).setHeaderStyle(headerStyle).build();

		AbstractColumn columnaAmortizedUnits = ColumnBuilder.getNew()
				.setColumnProperty("amortized_units", String.class.getName())
				.setTitle("Amortized Units").setWidth(new Integer(85))
				.setStyle(detailStyle).setHeaderStyle(headerStyle).build();

		AbstractColumn columnaMarketValueLocal = ColumnBuilder.getNew()
				.setColumnProperty("market_value_local", String.class.getName())
				.setTitle("Market Value Local").setWidth(new Integer(85))
				.setStyle(detailStyle).setHeaderStyle(headerStyle).build();
		
		AbstractColumn columnaMarketValueReportingCurrency = ColumnBuilder.getNew()
				.setColumnProperty("market_value_reporting_currency", String.class.getName())
				.setTitle("Market Value Reporting Currency").setWidth(new Integer(85))
				.setStyle(detailStyle).setHeaderStyle(headerStyle).build();

		
		
		return dr;
	}

	public static void main(String[] args) throws Exception {
		GroupsReportTest test = new GroupsReportTest();
		test.testReport();
		test.exportToJRXML();
		JasperViewer.viewReport(test.jp);
		JasperDesignViewer.viewReportDesign(test.jr);
	}

}
