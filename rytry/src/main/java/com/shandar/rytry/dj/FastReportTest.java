/**
 *
  * <p>Project: trunk_rytry </p>
 * <p>Package Name: com.shandar.rytry.dj </p>
 * <p>File Name: FastReportTest.java</p>
 * <p>Create Date: Dec 6, 2013 </p>
 * <p>Create Time: 3:37:43 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.dj;

import java.util.Date;
import java.util.Map;

import net.sf.jasperreports.view.JasperDesignViewer;
import net.sf.jasperreports.view.JasperViewer;
import ar.com.fdvs.dj.domain.DJCalculation;
import ar.com.fdvs.dj.domain.DJValueFormatter;
import ar.com.fdvs.dj.domain.DynamicReport;
import ar.com.fdvs.dj.domain.builders.FastReportBuilder;

/**
 * @author Shantanu Sikdar 
 *
 */
public class FastReportTest extends BaseDjReportTest{

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			FastReportTest test = new FastReportTest();
			test.testReport();
			test.exportToJRXML();
			JasperViewer.viewReport(test.jp); //finally display the report report
			JasperDesignViewer.viewReportDesign(test.jr);
		} catch (Exception e) {
			
		}

	}
	
	public DynamicReport buildReport() throws Exception {
 		FastReportBuilder drb = new FastReportBuilder();
 		drb.addColumn("State", "state", String.class.getName(),30)
 			.addColumn("Branch", "branch", String.class.getName(),30)
 			.addColumn("Product Line", "productLine", String.class.getName(),50)
 			.addColumn("Item", "item", String.class.getName(),50)
 			.addColumn("Item Code", "id", Long.class.getName(),30,true)
 			.addColumn("Quantity", "quantity", Long.class.getName(),60,true)
 			.addColumn("Amount", "amount", Float.class.getName(),70,true)
 			.addGroups(2)
			.setTitle("November \"2006\" sales report")
  			.setSubtitle("This report was generated at " + new Date())
  			.setPrintBackgroundOnOddRows(true)			
  			.setUseFullPageWidth(true);
  
          drb.addGlobalFooterVariable(drb.getColumn(4), DJCalculation.COUNT, null, new DJValueFormatter() {
              public String getClassName() {
                  return String.class.getName();
              }
              public Object evaluate(Object value, Map fields, Map variables,   Map parameters) {
                  return (value == null ? "0" : value.toString()) + " Clients";
              }
          });
  		DynamicReport dr = drb.build();  
  		return dr;
  	}
  

}
