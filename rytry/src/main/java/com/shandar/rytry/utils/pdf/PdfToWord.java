/**
 * <p>Project: com.shandar.rytry.utils.pdf </p>
 * <p>Package Name: com.shandar.rytry.utils.pdf </p>
 * <p>File Name: PdfToWord.java </p>
 * <p>Create Date: May 27, 2017 </p>
 * <p>Create Time: 12:31:39 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.utils.pdf;

import java.io.FileOutputStream;

import org.apache.poi.xwpf.usermodel.BreakType;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.canvas.parser.listener.SimpleTextExtractionStrategy;

/**
 * @author: Shantanu Sikdar
 */
public class PdfToWord {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
	}
	
/*	private void pdfDocConverter(){
		XWPFDocument doc = new XWPFDocument();
		// Open the pdf file
		String pdf = "myfile.pdf";
		PdfReader reader = new PdfReader(pdf);
		PdfReaderContentParser parser = new PdfReaderContentParser(reader);

		// Read the PDF page by page
		for (int i = 1; i <= reader.getNumberOfPages(); i++) {
		    TextExtractionStrategy strategy = parser.processContent(i, new SimpleTextExtractionStrategy());
		    // Extract the text
		    String text=strategy.getResultantText();
		    // Create a new paragraph in the word document, adding the extracted text
		    XWPFParagraph p = doc.createParagraph();
		    XWPFRun run = p.createRun();
		    run.setText(text);
		    // Adding a page break
		    run.addBreak(BreakType.PAGE);
		}
		// Write the word document
		FileOutputStream out = new FileOutputStream("myfile.docx");
		doc.write(out);
		// Close all open files
		out.close();
		reader.close();
	}*/

}
