/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.utils.pdf </p>
 * <p>File Name: PDFsUtils.java</p>
 * <p>Create Date: Jan 28, 2015 </p>
 * <p>Create Time: 7:53:21 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.utils.pdf;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFMergerUtility;
import org.apache.pdfbox.util.Splitter;

/**
 * @author Shantanu Sikdar 
 *
 */
public class PDFsUtils {

	public static void mergePdfs(List<String> pdfList, String pathToStoreMergedPdf){
		try{
			File dir = new File(pathToStoreMergedPdf);
			PDFMergerUtility mergePdf = new PDFMergerUtility();
			for (String pdfName : pdfList) {
				mergePdf.addSource(dir+"/"+pdfName);				
			}
			mergePdf.setDestinationFileName(dir+"/"+"merge.pdf");
			mergePdf.mergeDocuments();
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}
	
	
	public static void splitPdfs(String pdfToSplit,String pathToStoreSplitPdf) throws IOException{
		PDDocument document = new PDDocument();
        document = PDDocument.load(pdfToSplit);
        Splitter splitter = new Splitter();
        List<PDDocument> listOfSplitPages;
        listOfSplitPages = splitter.split(document);
        Iterator<PDDocument> iterator = listOfSplitPages.listIterator();
        int i = 1;
        while(iterator.hasNext()){
            PDDocument pd = iterator.next();
            try{
                //pd.save("D:\\pdf\\" + i++ + ".pdf");
                pd.save(pathToStoreSplitPdf+"\\"+ i++ + ".pdf");
            } catch (COSVisitorException anException){
                System.out.println("Something went wrong with page " + (i-1) + "\n Here is the error message" + anException);                
            }            
        }
	}
}
