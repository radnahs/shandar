/**
* Project: rytry
* Package Name:package com.shandar.rytry.utils.threading.tutorial2;
* File Name: UsingCachedThreadPool.java
* Create Date: Dec 19, 2016
* Create Time: 5:08:43 PM
* Copyright: Copyright (c) 2016
* @author: Shantanu Sikdar, ssikdar
* @version 1.0s
*/
package com.shandar.rytry.utils.threading.tutorial2;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class UsingCachedThreadPool {

	public static void main(String[] args) {
		System.out.println("Main method starts .... ");
		
		ExecutorService executorService = Executors.newCachedThreadPool();
		
		executorService.execute(new LoopTaskA());
		executorService.execute(new LoopTaskA());
		executorService.execute(new LoopTaskA());
		
		executorService.execute(new LoopTaskA());
		executorService.execute(new LoopTaskA());
		executorService.execute(new LoopTaskA());
		
		executorService.shutdown();
		
		//executorService.execute(new LoopTaskA());
		
		System.out.println("Main method ends .... ");
	}

}