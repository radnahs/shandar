/**
 *
 * <p>Project: trunk_rytry </p>
 * <p>Package Name: com.shandar.rytry.utils.numberFormat </p>
 * <p>File Name: NumberRelatedOperations.java</p>
 * <p>Create Date: Aug 14, 2013 </p>
 * <p>Create Time: 2:36:11 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.utils.numberFormat;

import java.math.BigDecimal;

/**
 * @author Shantanu Sikdar 
 *
 */
public class NumberRelatedOperations {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		NumberRelatedOperations nro = new NumberRelatedOperations();
		//
		System.out.println(nro.isNumber("885"));
	}
	
	private boolean isNumber(Object numbrObj){
		boolean isNum = false;
		String regex = "\\d+";
		if(numbrObj.toString().matches(regex)){
			System.out.println(numbrObj);
			isNum = true;
		}
		return isNum;
	}

	private void exponentToString(){
		/*Double a = 2.85 / 10000;
		System.out.println(a.doubleValue());
		System.out.println(a);
		System.out.println(BigDecimal.valueOf(a).toPlainString());*/
		
		//Double b = 2.85123 * 100000000;
		
		String str = "5.43455820000000E+009";
		System.out.println(Double.valueOf(str));
		Double b =  Double.parseDouble(str);

		System.out.println(b);
		System.out.println(b.doubleValue());
		System.out.println(BigDecimal.valueOf(b).toBigInteger());
		System.out.println(BigDecimal.valueOf(b).toPlainString());

	}
}
