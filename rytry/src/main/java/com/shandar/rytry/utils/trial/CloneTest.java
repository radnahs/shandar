/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.utils.trial </p>
 * <p>File Name: CloneTest.java</p>
 * <p>Create Date: Jun 20, 2014 </p>
 * <p>Create Time: 1:46:34 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.utils.trial;

/**
 * @author Shantanu Sikdar 
 *
 */
public class CloneTest {

	private int varTest =10;
	/**
	 * @param args
	 * @throws CloneNotSupportedException 
	 */
	public static void main(String[] args) throws CloneNotSupportedException {
		CloneTest ct1 = new CloneTest();
		CloneTest ct2 = (CloneTest)ct1.clone();
		System.out.println(ct1.varTest);
		ct1.varTest=11;
		System.out.println(ct1.varTest);
		ct2.varTest=12;
		System.out.println("c1"+ct1.varTest);
		System.out.println("c2"+ct2.varTest);
	}

}
