/**
 *
 * <p>Project: trunk_rytry </p>
 * <p>Package Name: com.shandar.rytry.utils </p>
 * <p>File Name: TechGig.java</p>
 * <p>Create Date: Nov 19, 2013 </p>
 * <p>Create Time: 5:49:56 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Shantanu Sikdar 
 *
 */
public class TechGig {
	
	
	public static void main(String[] args) {
		String input1="adfl";
		String input2="asdfgh";		
		char[] astrc = input1.toCharArray(),bstrc = input2.toCharArray();
		List<String> strLst = new ArrayList<>();
		for (char chr : astrc) {
			strLst.add(chr+"");
		}		
		for (char chr : bstrc) {
			strLst.add(chr+"");
		}		
		Set<String> setB = new HashSet<>(strLst);		
		for (char chr : bstrc) {
			setB.remove(chr+"");
		}		
		int countB=setB.size();		
		Set<String> setA = new HashSet<>(strLst);		
		for (char chr : astrc) {
			setA.remove(chr+"");
		}		
		int countA=setA.size();		
		System.out.println((countB+countA));
	}	
	
	
	public static void main1(String[] args) {
		String input1="adfghq";
		String input2="sdfgh";
		int counterT=0;
		boolean has=false;
		char[] astrc = input1.toCharArray(),bstrc = input2.toCharArray();
		int alen=astrc.length, blen=bstrc.length;
		for(int i=0; i<alen;i++){			
			System.out.println("i == "+ i);
			for(int j=0; j<blen;j++){
				System.out.println("--- j == "+ j);
				if(astrc[i]==bstrc[j]){
					break;										
				}
				if(alen> blen && j==blen-1){
					counterT++;
				}
				if(blen> alen && i==alen-1){
					counterT++;
				}
			}
			
		}
		System.out.println("counter == " + counterT );		
	}

}
