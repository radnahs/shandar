/**
* Project: rytry
* Package Name:package com.shandar.rytry.utils.javaInternals;
* File Name: WeakHashMapExample.java
* Create Date: Jan 6, 2017
* Create Time: 4:57:20 PM
* Copyright: Copyright (c) 2016
* @author: Shantanu Sikdar, ssikdar
* @version 1.0
*/
package com.shandar.rytry.utils.javaInternals;

import java.util.Map;
import java.util.WeakHashMap;

public class WeakHashMapExample {

	public static void main(String[] args) {
		Map<WHMItem, Integer> whmItems =  new WeakHashMap<WHMItem, Integer>();
		
		whmItems.put(new WHMItem(1, "item1"), 100);
		whmItems.put(new WHMItem(2, "item2"), 200);
		
		WHMItem whmItem  = new WHMItem(3, "item3");
		
		whmItems.put(whmItem,500);
		
		System.out.println(whmItems.size());
		
		try {
			Thread.sleep(10000);
		} catch (InterruptedException ie) {
			ie.printStackTrace();
		}
		
		System.gc();
		
		
		System.out.println(whmItems.size());
		
	}

}

class WHMItem{
	
	private int itmId;
	private String itmDetails;
	
	public WHMItem(int itmId,String itmDetails) {
		this.itmId=itmId;
		this.itmDetails=itmDetails;
	}
	
}
