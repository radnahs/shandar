package com.shandar.rytry.utils.pdf;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Properties;

import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.util.PDFTextStripper;

public class PDFWithText {

	private static Properties properties = new Properties();
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//String path = "C:\\Data\\Workbench\\document\\Erisa\\";		
		//new PDFWithText().createPDF("c:\\data");
		//new PDFWithText().parsePDF();
		
		//System.out.println("proper = "+properties.getProperty("rytry.path"));
		
		/*String input = "C:\\Data\\Workbench\\document\\Erisa\\ReportsSample.pdf";
		String output = "C:\\Data\\Workbench\\document\\Erisa\\CopyReportsSample789.txt";
		String text = new PDFWithText().pdftoText(input);
		new PDFWithText().writeTexttoFile(text, output);*/
		
		String input = "C:\\Data\\WORKBENCH\\PLATFORM\\document\\Erisa\\platform.pdf";
		String output = "C:\\Data\\WORKBENCH\\PLATFORM\\document\\Erisa\\platform_chandra.txt";
		String text = new PDFWithText().pdftoText(input);
		new PDFWithText().writeTexttoFile(text, output);
		System.out.println("done");		
	}

	private String pdftoText(String fileName) {	
		PDFParser parser;
	    String parsedText;
	    PDFTextStripper pdfStripper;
	    PDDocument pdDoc = null;
	    COSDocument cosDoc=null;
	    PDDocumentInformation pdDocInfo;		   
        System.out.println("Parsing text from PDF file " + fileName + "....");
        File f = new File(fileName);  
        if (!f.isFile()) {
            System.out.println("File " + fileName + " does not exist.");
            return null;
        }  
        try {
            parser = new PDFParser(new FileInputStream(f));
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }  
        try {
            parser.parse();
            cosDoc = parser.getDocument();
            System.out.println(cosDoc);
            pdfStripper = new PDFTextStripper();
            pdDoc = new PDDocument(cosDoc);
            System.out.println(pdDoc.getPageMap());
            
            parsedText = pdfStripper.getText(pdDoc);
        } catch (Exception e) {
        	System.out.println(e.getMessage());            
            try {
                   if (cosDoc != null) cosDoc.close();
                   if (pdDoc != null) pdDoc.close();
               } catch (Exception e1) {
               e.printStackTrace();
            }
            return null;
        }
        System.out.println("Done.");
        return parsedText;
    }
	
	void writeTexttoFile(String pdfText, String fileName) {		  
        System.out.println("\n PDF text to txt file " + fileName + "....");
        try {
            PrintWriter pw = new PrintWriter(fileName);
            pw.print(pdfText);
            pw.close();  
        } catch (Exception e) {
            System.out.println(e.getMessage());            
        }
        System.out.println("Done.");
    }
	
	private void createPDF(String path) {
		PDDocument pdDoc = null;
		PDPage page = null;
		try {
			path = "C:\\Data\\Workbench\\document\\Erisa\\";
			pdDoc = new PDDocument();
			page = new PDPage();

			pdDoc.addPage(page);
			PDFont font = PDType1Font.HELVETICA_BOLD;

			PDPageContentStream content = new PDPageContentStream(pdDoc, page);
			content.beginText();
			content.setFont(font, 12);
			content.moveTextPositionByAmount(100, 700);
			content.drawString("Hello from shantanu sikdar");

			content.endText();
			content.close();
			pdDoc.save(path + "PDFWithText.pdf");
			pdDoc.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	private void parsePDF() {
		PDDocument pdDoc = null;
		BufferedWriter wr;		
		try {
			System.out.println(pdDoc);
			String path = "C:\\Data\\Workbench\\document\\Erisa\\";
			File input = new File(path + "platform.pdf");
			//File input = new File(path + "ReportsSample.pdf");
			
			pdDoc = PDDocument.load(input);
			
			System.out.println(pdDoc.getPageMap());
			System.out.println(pdDoc.getNumberOfPages());
			System.out.println(pdDoc.isEncrypted());
			
			File output = new File(path + "CopiedText.txt");
			PDFTextStripper stripper = new PDFTextStripper();
			wr = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(output)));
			stripper.writeText(pdDoc, wr);
			if (pdDoc != null) {
				pdDoc.close();
			}
			wr.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	
}
