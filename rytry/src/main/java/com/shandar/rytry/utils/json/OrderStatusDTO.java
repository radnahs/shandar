package com.shandar.rytry.utils.json;

public class OrderStatusDTO {

	private static final long serialVersionUID = 1L;

	private String orderId;
	private String orderStatus;
	private String orderStatusDesc;
	private String lastUpdateTS;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getOrderStatusDesc() {
		return orderStatusDesc;
	}

	public void setOrderStatusDesc(String orderStatusDesc) {
		this.orderStatusDesc = orderStatusDesc;
	}

	public String getLastUpdateTS() {
		return lastUpdateTS;
	}

	public void setLastUpdateTS(String lastUpdateTS) {
		this.lastUpdateTS = lastUpdateTS;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "OrderStatusDTO [orderId=" + orderId + ", orderStatus=" + orderStatus + ", orderStatusDesc="
				+ orderStatusDesc + ", lastUpdateTS=" + lastUpdateTS + ", getOrderId()=" + getOrderId()
				+ ", getOrderStatus()=" + getOrderStatus() + ", getOrderStatusDesc()=" + getOrderStatusDesc()
				+ ", getLastUpdateTS()=" + getLastUpdateTS() + ", getClass()=" + getClass() + ", hashCode()="
				+ hashCode() + ", toString()=" + super.toString() + "]";
	}
}
