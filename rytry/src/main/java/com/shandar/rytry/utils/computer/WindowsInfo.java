/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.utils.computer </p>
 * <p>File Name: SystemInfo.java</p>
 * <p>Create Date: Mar 18, 2013 </p>
 * <p>Create Time: 8:15:41 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.utils.computer;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;

import com.shandar.rytry.common.properties.rytryProperties;

/**
 * @author Shantanu Sikdar 
 *
 */
public class WindowsInfo {
	
	public enum CommonWindowsCommand{
		HOSTNAME("HOSTNAME",batchCommand("hostname")),
		MACADDRESS("MACADDRESS",batchCommand("getmac"));
		
		private final String key;
		private final String constantValue;
		CommonWindowsCommand(String key, String value) {
			this.key = key;
			this.constantValue = value;
		}		
		public String cnKey()   { return key; }
		public String cnValue() { return constantValue; }
	}
			
	//public static final String HOSTNAME=batchCommand("hostname");	
	//public static final String MACADDRESS=batchCommand("getmac");
	public static final String SYSTEMINFO=batchCommand("systeminfo");
	public static final String TASKLIST=batchCommand("tasklist");
	public static final String DRIVERQUERY=batchCommand("DRIVERQUERY");
	public static final String ICACLS=batchCommand("icacls");
	public static final String GPRESULT=batchCommand("gpresult");
	public static final String XXXX=batchCommand("fsutil");
	public static final String XXX1=batchCommand("gpresult");
	public static final String XXX2=batchCommand("gpresult");
	public static final String XXX3=batchCommand("gpresult");
	public static final String XXX4=batchCommand("gpresult");
	
	
	public static final String batchCommand(String command) {
		String outputText = null;
		try {
			Process p = Runtime.getRuntime().exec(command);
			outputText = getString(p);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return outputText;
	}
	
	private static String getString(Process p) throws IOException{		
		InputStream stdoutStream = new BufferedInputStream(p.getInputStream()); 
		StringBuffer buffer= new StringBuffer();
		for (;;) {
			int c = stdoutStream.read();
			if (c == -1) break;
			buffer.append((char)c);
		}
		String outputText = buffer.toString(); 
		stdoutStream.close(); 
		return outputText;		
	}

	public static void main(String[] args) {
		try {
			System.setOut(new PrintStream(new FileOutputStream(rytryProperties.documents+"\\loggs.txt")));
			//systeminfo
			//ipconfig /all
			//msinfo32.exe
			/*System.out.println(WindowsInfo.HOSTNAME);
			System.out.println(WindowsInfo.MACADDRESS);*/
			//System.out.println(WindowsInfo.XXXX);
			//System.out.println(batchCommand("ping localhost"));
			System.out.println(CommonWindowsCommand.MACADDRESS.cnValue());
			System.out.println(CommonWindowsCommand.HOSTNAME.cnValue());
			
			/*System.out.println(windowsRunIpConfig());
			System.out.println(windowsGetMac());*/
		} catch (Exception e) {
			
		}	
	}
	
}

