package com.shandar.rytry.utils.fileConversion;


/**
 * recordType=TRIM(SUBSTR(@var,�1,�2)),
 * banner�=�TRIM(SUBSTR(@var,�3,�20)),
level1_code�=�TRIM(SUBSTR(@var,�23,�30)),
level1_description�=�TRIM(SUBSTR(@var,�53,�60)),
level2_code�=�TRIM(SUBSTR(@var,�113,�30)),
level2_description�=�TRIM(SUBSTR(@var,�143,�60)),
level3_code�=�TRIM(SUBSTR(@var,�203,�30)),
level3_description�=�TRIM(SUBSTR(@var,233,�60)),
level4_code�=�TRIM(SUBSTR(@var,�293,�30)),
level4_description�=�TRIM(SUBSTR(@var,�323,�60)),
level5_code�=�TRIM(SUBSTR(@var,�383,�30)),
level5_description�=�TRIM(SUBSTR(@var,�413,�60)),
level6_code�=�TRIM(SUBSTR(@var,�473,�30)),
level6_description�=�TRIM(SUBSTR(@var,�503,�60)),
level7_code�=�TRIM(SUBSTR(@var,�563,�30)),
level7_description�=�TRIM(SUBSTR(@var,�593,�60)),
level8_code�=�TRIM(SUBSTR(@var,�653,�30)),
level8_description�=�TRIM(SUBSTR(@var,�683,�60)),
level9_code�=�TRIM(SUBSTR(@var,�743,�30)),
level9_description�=�TRIM(SUBSTR(@var,�773,�60)),
UPC�=�TRIM(SUBSTR(@var,�833,�20)),
product_description�=�TRIM(SUBSTR(@var,�853,�60)),
brand�=�TRIM(SUBSTR(@var,�913,�20)),
exclude_from_base_point_ind�=�TRIM(SUBSTR(@var,�933,�1)),
status�=�TRIM(SUBSTR(@var,�934,�1)),
gl_division�=�TRIM(SUBSTR(@var,�935,�4)),
gl_category�=�TRIM(SUBSTR(@var,�939,�4));
	 

 * @author ssikdar
 *
 */


public enum FileDataStructure {
	recordType(1,1,2),
	banner(2,3,20),
	level1_code(3,23,30),
	level1_description(4,53,60),
	level2_code(5,113,30),
	level2_description(6,143,60),
	level3_code(7,203,30),
	level3_description(8,233,60),
	level4_code(9,293,30),
	level4_description(10,323,60),
	level5_code(11,383,30),
	level5_description(12,413,60),
	level6_code(13,473,30),
	level6_description(14,503,60),
	level7_code(15,563,30),
	level7_description(16,593,60),
	level8_code(17,653,30),
	level8_description(18,683,60),
	level9_code(19,743,30),
	level9_description(20,773,60),
	UPC(21,833,20),
	product_description(22,853,60),
	brand(23,913,20),
	exclude_from_base_point_ind(24,933,1),
	status(25,934,1),
	gl_division(26,935,4),
	gl_category(27,939,4);
	
	private final int key;
	private final int start;
	private final int count;

	FileDataStructure(int intKey, int intStart,int intCount){
		this.key=intKey;
		this.start=intStart;
		this.count=intCount;
	}
	
	public int getKey(){ return key;	}
	public int getStart(){ return start;	}
	public int getCount(){ return count;	}

}
