/**
 * Project: rytry
 * Package Name:package com.shandar.rytry.utils;
 * File Name: InternalCacheOfIntegers.java
 * Create Date: Jan 25, 2014
 * Create Time: 1:16:46 PM
 * Copyright: Copyright (c) 2014
 * @author: Shantanu Sikdar, ssikdar
 * @version 1.0
 */
//https://dzone.com/articles/the-internal-cache-of-integers
/**
 * Set the following value at Run Configuration - VM Arguments. 
 * -XX:AutoBoxCacheMax=1000 
 * 
 * 
 */
package com.shandar.rytry.utils.javaInternals;

public class InternalCacheOfIntegers {

	public static void main(String[] args) {
		test(17, 17);
		test(200, 200);
	}

	private static void test(Integer i, Integer i2) {
		System.out.println(i);
		if (i == i2)
			System.out.println(" the same");
		if (i != i2)
			System.out.println(" different");
		if (i.equals(i2))
			System.out.println(" equal");
	}

}
