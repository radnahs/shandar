/**
* Project: rytry
* Package Name:package com.shandar.rytry.utils.threading.tutorial2;
* File Name: FirstWay.java
* Create Date: Dec 19, 2016
* Create Time: 4:16:02 PM
* Copyright: Copyright (c) 2016
* @author: Shantanu Sikdar, ssikdar
* @version 1.0
*/
package com.shandar.rytry.utils.threading.tutorial2;

import java.util.concurrent.TimeUnit;

public class FirstWay {

	public static void main(String[] args) {
		System.out.println("Main method starts .... ");
		new FirstTask();
		Thread th = new FirstTask();
		
		System.out.println("Main method ends .... ");
	}
}

class FirstTask extends Thread{
	
	private static int count=0;
	private int id;
	
	@Override
	public void run() {
		for (int i = 10; i >0; i--) {
			System.out.println(Thread.currentThread().getName()+" <"+id+"> Tick tick"+i);
			try {
				TimeUnit.MILLISECONDS.sleep(200);
			} catch (InterruptedException iex) {
				iex.printStackTrace();
			}
		}
	}
	
	public FirstTask() {
		this.id = ++count;
		this.start();
	}
	
}
