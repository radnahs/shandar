/**
 * Project: rytry
 * Package Name:package com.shandar.rytry.utils.threading.concurrency;
 * File Name: MyThread.java
 * Create Date: Sep 22, 2016
 * Create Time: 6:58:25 PM
 * Copyright: Copyright (c) 2016
 * @author: Shantanu Sikdar, ssikdar
 * @version 1.0
 */
package com.shandar.rytry.utils.threading.concurrency;

public class MyThread extends Thread {

	public MyThread(String name) {
		super(name);
	}

	@Override
	public void run() {
		System.out.println("Executing thread " + Thread.currentThread().getName());
	}

	public static void main(String[] args) {
		MyThread myThread = new MyThread("myThread");
		myThread.start();
	}

}
