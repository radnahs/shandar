/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.utils.threading </p>
 * <p>File Name: ThreadSynchronization.java</p>
 * <p>Create Date: Mar 7, 2016 </p>
 * <p>Create Time: 5:34:35 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.utils.threading;

/**
 * @author Shantanu Sikdar
 * 
 */
public class ThreadSynchronizationTest {
	public static void main(String[] args) {
		NumberTable tbl = new NumberTable();
		Thread t1 = new Thread(new Thread1(tbl));
		Thread t2 = new Thread(new Thread2(tbl));
		t1.start();
		t2.start();
	}
}

class Thread1 implements Runnable {
	NumberTable t;  
	Thread1(NumberTable t){  
		this.t=t;  
	}
	
	@Override
	public void run() {
		t.printNumbers(5);
	}

}

class Thread2 implements Runnable {
	
	NumberTable t;  
	Thread2(NumberTable t){  
		this.t=t;  
	} 
	
	@Override
	public void run() {
		t.printNumbers(100);
	}

}

class NumberTable {

	//public void printNumbers(int n) {// method not synchronized
	public synchronized void printNumbers(int n) {// method synchronized	
		for (int i = 1; i <= 5; i++) {
			System.out.println(n * i);
			try {
				Thread.sleep(400);
			} catch (Exception e) {
				System.out.println(e);
			}
		}

	}
}