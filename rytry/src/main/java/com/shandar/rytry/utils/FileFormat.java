/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.utils </p>
 * <p>File Name: FileFormat.java</p>
 * <p>Create Date: Jun 2, 2014 </p>
 * <p>Create Time: 1:06:11 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Scanner;

/**
 * @author Shantanu Sikdar 
 *
 */
public class FileFormat {

    public static void main1(String args[]) {
        FileReader fr = null;
        FileWriter fw = null;
        try {
            fr = new FileReader("C:\\Data\\WORKBENCH\\WEBCR\\webcr report conversion\\OR8250 - Foreign Exchange Contracts_commnets1.txt");
            fw = new FileWriter("C:\\Data\\WORKBENCH\\WEBCR\\webcr report conversion\\OR8250 - Foreign Exchange Contracts_commnets2.txt");
            int c = fr.read();
            while(c!=-1) {
                fw.write(c);
                c = fr.read();
            }
        } catch(IOException e) {
            e.printStackTrace();
        } finally {
            //close(fr);
            //close(fw);
        }
    }
    
    public static void main(String[] args)throws Exception {
    	//readFile("C:\\Data\\WORKBENCH\\WEBCR\\webcr report conversion\\OR8250 - Foreign Exchange Contracts_commnets.txt");
    	//readFile("C:\\Data\\WORKBENCH\\WEBCR\\webcr report conversion\\OR8355 - Transaction Report_comment.txt");    	
    	//readFile("C:\\Data\\WORKBENCH\\WEBCR\\webcr report conversion\\OR8702 - Currency Settlement Statement_comment.txt");
    	//readFile("C:\\Data\\WORKBENCH\\WEBCR\\webcr report conversion\\ORA450 - Income Earned_comment.txt");
    	//readFile("C:\\Data\\WORKBENCH\\ERS\\ORA450.SQL");    	
    	//readFile("C:\\Data\\WORKBENCH\\ERS\\OR8250.SQL");
    	//readFile("C:\\Data\\WORKBENCH\\ERS\\OR8355.SQL");
    	readFile("C:\\Data\\WORKBENCH\\ERS\\OR8702.SQL");
    	
	}

	// Location of file to read
    public static void readFile(String filePathAndName) throws Exception {
		File file = new File(filePathAndName); 
		Scanner scanner = new Scanner(file); 
		while (scanner.hasNextLine()) {
			String line = scanner.nextLine();
			//System.out.println(line);
			writeFile(line);
		}
		scanner.close();
		System.out.println("File Copied"); 
	}
 
    // 	Location of file to output
	public static void writeFile(String copyText) throws Exception { 
		String newLine = System.getProperty("line.separator");
		//String newLine = "shantanu_sikdar";
	    Writer output = null;
	    //File file = new File("C:\\Data\\WORKBENCH\\WEBCR\\webcr report conversion\\OR8250 - Foreign Exchange Contracts_commnets2.txt");
	    //File file = new File("C:\\Data\\WORKBENCH\\WEBCR\\webcr report conversion\\OR8355 - Transaction Report_comment2.txt");
	    //File file = new File("C:\\Data\\WORKBENCH\\WEBCR\\webcr report conversion\\OR8702 - Currency Settlement Statement_comment2.txt");
	    //File file = new File("C:\\Data\\WORKBENCH\\WEBCR\\webcr report conversion\\ORA450 - Income Earned_comment2.txt");
	    //File file = new File("C:\\Data\\WORKBENCH\\ERS\\ORA450_slashN.SQL");
	    //File file = new File("C:\\Data\\WORKBENCH\\ERS\\OR8250_slashN.SQL");
	    //File file = new File("C:\\Data\\WORKBENCH\\ERS\\OR8355_slashN.SQL");
	    File file = new File("C:\\Data\\WORKBENCH\\ERS\\OR8702_slashN.SQL");
	    
	    output = new BufferedWriter(new FileWriter(file, true));
	    output.write(copyText+"\\n");
	    output.write(newLine);
	    output.close();		
	}
	
	

}
