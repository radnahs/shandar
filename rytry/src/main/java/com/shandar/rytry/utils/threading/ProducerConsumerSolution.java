/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.utils.threading </p>
 * <p>File Name: ProducerConsumerSolution.java</p>
 * <p>Create Date: Apr 17, 2015 </p>
 * <p>Create Time: 3:40:00 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.utils.threading;


/**
 * @author Shantanu Sikdar
 * 
 */
public class ProducerConsumerSolution {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SomeContainer someContainer= new SomeContainer();
		Producer prodThread = new Producer(someContainer, 1);
		Consumer consThread = new Consumer(someContainer, 1);
		prodThread.start();
		consThread.start();
	}
}

class SomeContainer {
	private int contents;
	private boolean available = false;

	public synchronized int get() {
		while (available == false) {
			try {
				wait();
			} catch (InterruptedException e) {
			}
		}
		available = false;
		notifyAll();
		return contents;
	}

	public synchronized void put(int value) {
		while (available == true) {
			try {
				wait();
			} catch (InterruptedException e) {
			}
		}
		contents = value;
		available = true;
		notifyAll();
	}
}

class Producer extends Thread {
	private SomeContainer someContainer;
	private int number;

	public Producer(SomeContainer c, int number) {
		someContainer = c;
		this.number = number;
	}
	public void run() {
		for (int i = 0; i < 10; i++) {
			someContainer.put(i);
			System.out.println("ProducerYield #" + this.number + " put: " + i);
			try {
				sleep((int) (Math.random() * 100));
			} catch (InterruptedException e) {
			}
		}
	}
}

class Consumer extends Thread {
	private SomeContainer someContainer;
	private int number;
	public Consumer(SomeContainer c, int number) {
		someContainer = c;
		this.number = number;
	}
	public void run() {
		int value = 0;
		for (int i = 0; i < 10; i++) {
			value = someContainer.get();
			System.out.println("ConsumerYield #" + this.number + " got: " + value);
		}
	}
}