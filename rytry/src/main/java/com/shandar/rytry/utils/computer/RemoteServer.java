/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.utils.computer </p>
 * <p>File Name: RemoteServer.java</p>
 * <p>Create Date: Oct 22, 2015 </p>
 * <p>Create Time: 4:25:30 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.utils.computer;

import java.lang.management.ManagementFactory;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.MBeanServer;
import javax.management.MBeanServerFactory;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.Query;
import javax.management.ReflectionException;

/**
 * @author Shantanu Sikdar
 * 
 */
public class RemoteServer {

	public static void main(String[] args) {
		try{
			getEndPoints();
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	static List<String> getEndPoints() throws MalformedObjectNameException,NullPointerException, UnknownHostException,
			AttributeNotFoundException, InstanceNotFoundException,MBeanException, ReflectionException {
		
		MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
		Set<ObjectName> objs = mbs.queryNames(new ObjectName("*:type=Connector,*"), Query.match(Query.attr("protocol"),Query.value("HTTP/1.1")));
		//String hostname = InetAddress.getLocalHost().getHostName();
		System.out.println("objs = "+objs);
		String remoteHostname = InetAddress.getByName("172.19.156.10").getHostName();
		System.out.println(remoteHostname);
		//InetAddress[] addresses = InetAddress.getAllByName(hostname);
		InetAddress[] addresses = InetAddress.getAllByName(remoteHostname);		
		ArrayList<String> endPoints = new ArrayList<String>();
		for (Iterator<ObjectName> i = objs.iterator(); i.hasNext();) {
			ObjectName obj = i.next();
			String scheme = mbs.getAttribute(obj, "scheme").toString();
			System.out.println("scheme : "+scheme);
			String port = obj.getKeyProperty("port");
			System.out.println("port : "+port);
			for (InetAddress addr : addresses) {
				String host = addr.getHostAddress();
				System.out.println("host : "+host);
				String ep = scheme + "://" + host + ":" + port;
				endPoints.add(ep);
			}
		}
		System.out.println("endPoints : "+endPoints);
		return endPoints;
	}
	
	public void test(){
		MBeanServer mBeanServer = MBeanServerFactory.findMBeanServer(null).get(0);
		//ObjectName name = new ObjectName("Catalina", "type", "Server");
		//Server server = (Server) mBeanServer.getAttribute(name, "managedResource");
	}

	/*
	 * public static void main(String[] args) { Server server =
	 * ServerFactory.getServer(); Service[] services = server.findServices();
	 * for (Service service : services) { for (Connector connector :
	 * service.findConnectors()) { ProtocolHandler protocolHandler =
	 * connector.getProtocolHandler(); if (protocolHandler instanceof
	 * Http11Protocol || protocolHandler instanceof Http11AprProtocol ||
	 * protocolHandler instanceof Http11NioProtocol) { serverPort =
	 * connector.getPort(); System.out.println("HTTP Port: " +
	 * connector.getPort()); } } } }
	 */

}
