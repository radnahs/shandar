/**
* Project: rytry
* Package Name:package com.shandar.rytry.utils.networking;
* File Name: NetworkDemo.java
* Create Date: Dec 25, 2016
* Create Time: 8:03:58 AM
* Copyright: Copyright (c) 2016
* @author: Shantanu Sikdar, ssikdar
* @version 1.0
*/
package com.shandar.rytry.utils.networking;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;

public class NetworkDemo {
	
	public static void main(String[] args) {
		//javaInetAddressDemo();
		javaURLDemo();
	}
	
	
	/**
	 *javaURLDemo show the uses of java.net.URL
	 * Uniform Resource Locator
	 */
	private static void javaURLDemo() {
		//hypertext transfer protocol
		isProtocolSupporte("http://www.vtc.com");
		//secure http
		isProtocolSupporte("http://www.vtc.com");
		//file transfer protocol
		isProtocolSupporte("http://www.vtc.com");
		//telnet
		isProtocolSupporte("http://www.vtc.com");
		//local file access
		isProtocolSupporte("http://www.vtc.com");
		//telnet
		isProtocolSupporte("http://www.vtc.com");
		
		try {
			URL url = new URL("http://www.vtc.com");
			URLConnection urlConnection = url.openConnection();
			urlConnection.connect();
			
			BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
			String inputLine;
			while((inputLine=in.readLine()) != null){
				System.out.println(inputLine);
			}
			in.close();
		
		} catch (MalformedURLException mue) {
			mue.printStackTrace();
		}catch (IOException ioe) {
			ioe.printStackTrace();
		}
		
	}
	 
	private static void isProtocolSupporte(String urlStr){
		try {
			URL url = new URL(urlStr);
			System.out.println(url.getProtocol()+"is supported");
			
		} catch (MalformedURLException e) {
			
		}
	}
	
	/**
	 * javaInetAddressDemo show the uses of java.net.InetAddress
	 *  
	 */
	private static void javaInetAddressDemo(){
		try {
			InetAddress inetAddress = InetAddress.getLocalHost();
			
			inetAddress = InetAddress.getByName("google.com");
			System.out.println(inetAddress);
			
			InetAddress inetAddresses[] = InetAddress.getAllByName("www.nba.com");
			
			for (InetAddress inetAddress2 : inetAddresses) {
				System.out.println(inetAddress2);
			}
			
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}
	
	

}
