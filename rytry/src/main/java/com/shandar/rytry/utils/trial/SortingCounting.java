/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.utils.trial </p>
 * <p>File Name: SortingCounting.java</p>
 * <p>Create Date: May 19, 2014 </p>
 * <p>Create Time: 3:25:29 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.utils.trial;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author Shantanu Sikdar 
 *
 */
public class SortingCounting {
	
	public static void main(String[] args) {
		String[] sortCount = {"star","car","dog","irs","tars","reason","pears","god","scar","cars"};
		sortCount(sortCount);
		sortCountContent(sortCount);
	}
	
	public static void sortCount(String[] strArr){
		Map<String, Integer> map = new TreeMap<String, Integer>();		
		int count=1;
		for (String string : strArr) {
			List<String> lst =  new ArrayList<String>();
			char[] chArr =	string.toCharArray();   
			Arrays.sort(chArr);
			String ss = String.valueOf(chArr);
			System.out.println(ss);
			if(map.containsKey(ss)){				
				map.put(ss, count+1);
			}else{				
				map.put(ss, count);
			}	
		}		
		System.out.println(map);
	}
	
	
	public static void sortCountContent(String[] strArr){
		Map<String, List<String>> map = new HashMap<String, List<String>>();		
		int count=1;
		for (String string : strArr) {
			List<String> lst =  new ArrayList<String>();
			char[] chArr =	string.toCharArray();   
			Arrays.sort(chArr);
			String ss = String.valueOf(chArr);
			System.out.println(ss);
			if(map.containsKey(ss)){
				lst = map.get(ss);
				lst.add(string);
				lst.add(count+1+"");
			}else{
				lst.add(string);
				lst.add(count+"");
			}	
			map.put(ss, lst);
		}		
		System.out.println(map);
	}

}
