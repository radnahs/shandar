/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.utils.server </p>
 * <p>File Name: TomcatServer.java</p>
 * <p>Create Date: Mar 10, 2016 </p>
 * <p>Create Time: 8:02:38 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.utils.server;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javax.management.MBeanServer;
import javax.management.MBeanServerFactory;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;

/**
 * @author Shantanu Sikdar 
 *
 */
public class TomcatServer {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		TomcatServer ts = new TomcatServer();
		ts.collectAllDeployedApps();
	}
	
	
	
	private Iterable<String> collectAllDeployedApps() {
		final Set<String> result = new HashSet<>();
		try {
	        final Set<ObjectName> instances = findServer().queryNames(new ObjectName("Tomcat:j2eeType=WebModule,*"), null);
	        int i =0;
	        for (ObjectName each : instances) {
	            //result.add(substringAfterLast(each.getKeyProperty("name"), "/")); //it will be in format like //localhost/appname
	        	System.out.println(i+"  "+each.getKeyProperty("name"));
	        	//result.add(substringAfterLast(each.getKeyProperty("name"), "/"));
	        	i++;
	        	result.add(each.getKeyProperty("name"));
	        }
	        
	    } catch (MalformedObjectNameException e) {
	         //handle
	    	e.printStackTrace();
	    }
	    return result;
	}

	private MBeanServer findServer() {
	    ArrayList<MBeanServer> servers = MBeanServerFactory.findMBeanServer(null);
	    for (MBeanServer eachServer : servers) {
	        for (String domain : eachServer.getDomains()) {
	            if (domain.equals("Tomcat")) {
	                return eachServer;
	            }
	        }
	    }
	    //handle. We are not in Tomcat.
	    return null;
	}

}
