/**
 *
 * <p>Project: rytry </p>
 * <p>Package Name: com.shandar.rytry.utils </p>
 * <p>File Name: AppUtils.java</p>
 * <p>Create Date: Mar 27, 2013 </p>
 * <p>Create Time: 1:43:13 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.utils;

/**
 * @author Shantanu Sikdar 
 *
 */
public class AppUtils {
	
	/**
	 * This method is specially meant for checking String.
	 * It returns true if the String is blank or null.
	 * @param str
	 * @return
	 */
	public static boolean isStringBlankOrNull(String str){
		boolean boo=false;
		if(str==null){			
			boo=true;
		}else if (str.trim().equals("")) {			
			boo=true;
		}else {
			boo=false;
		}
		return boo;
	}
	
	
	public static void main(String[] args) {		
		String ss=null;
		System.out.println(isStringBlankOrNull(ss));
	}

}
