/**
* Project: rytry
* Package Name:package com.shandar.rytry.utils.threading.concurrency;
* File Name: NotSynchronizedCounter.java
* Create Date: Dec 16, 2016
* Create Time: 7:19:46 PM
* Copyright: Copyright (c) 2016
* @author: Shantanu Sikdar, ssikdar
* @version 1.0
*/
package com.shandar.rytry.utils.threading.concurrency;

public class NotSynchronizedCounter implements Runnable{
	private static int counter = 0;
	
	@Override
	public void run() {
		while(counter < 10) {
			System.out.println("["+Thread.currentThread().getName()+"] before: "+counter);
			counter++;
			System.out.println("["+Thread.currentThread().getName()+"]  after: "+counter);
		}
	}
	
	public static void main(String[] args) {
		Thread[] threads = new Thread[5];
		
		try {
			for (int i = 0; i < threads.length; i++) {
				threads[i] = new Thread(new NotSynchronizedCounter(),"  thread-" + i);
				threads[i].start();
			}
			for (int i = 0; i < threads.length; i++) {
				threads[i].join();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
