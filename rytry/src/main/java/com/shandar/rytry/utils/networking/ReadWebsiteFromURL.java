/**
 * <p>Project: com.shandar.rytry.utils.networking </p>
 * <p>Package Name: com.shandar.rytry.utils.networking </p>
 * <p>File Name: ReadWebsiteFromURL.java </p>
 * <p>Create Date: Jun 3, 2017 </p>
 * <p>Create Time: 1:18:30 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.utils.networking;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;

import com.shandar.rytry.parser.converter.HTMLToPlainTextConverter;

/**
 * @author: Shantanu Sikdar
 */
public class ReadWebsiteFromURL {

//	9088241040
	
	private static void readURLContentWriteToFile(String URL,String fileName) {
		try {
			URL url = new URL(URL);
			URLConnection urlConnection = url.openConnection();
			urlConnection.connect();
			
			Scanner scanner = new Scanner(urlConnection.getInputStream());
			HTMLToPlainTextConverter htmlToPlainTextConverter = new HTMLToPlainTextConverter();
			PrintWriter writer = new PrintWriter("D:\\Misc\\tutorial\\"+fileName+".txt");
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				writer.println(htmlToPlainTextConverter.getResult(line));
				//System.out.println(htmlToPlainTextConverter.getResult(line));
			}
			scanner.close();
			writer.close();
		} catch (MalformedURLException mue) {
			mue.printStackTrace();
		}catch (IOException ioe) {
			ioe.printStackTrace();
		}
		
	}

	
	public static void main(String[] args) {
		readURLContentWriteToFile("http://introcs.cs.princeton.edu/java/11hello/HelloWorld.java.html","HelloWorld");
		readURLContentWriteToFile("http://introcs.cs.princeton.edu/java/11hello/UseArgument.java.html","UseArgument");
		readURLContentWriteToFile("http://introcs.cs.princeton.edu/java/12types/Ruler.java.html","Ruler");
		readURLContentWriteToFile("http://introcs.cs.princeton.edu/java//12types/IntOps.java.html","IntOps");
		readURLContentWriteToFile("http://introcs.cs.princeton.edu/java/12types/Quadratic.java.html","Quadratic");
		readURLContentWriteToFile("http://introcs.cs.princeton.edu/java/13flow/12types/LeapYear.java.html","LeapYear");
		readURLContentWriteToFile("http://introcs.cs.princeton.edu/java/12types/RandomInt.java.html","RandomInt");
		readURLContentWriteToFile("http://introcs.cs.princeton.edu/java/13flow/Flip.java.html","Flip");
		readURLContentWriteToFile("http://introcs.cs.princeton.edu/java/13flow/TenHellos.java.html","TenHellos");
		readURLContentWriteToFile("http://introcs.cs.princeton.edu/java/13flow/PowersOfTwo.java.html","PowersOfTwo");
		readURLContentWriteToFile("http://introcs.cs.princeton.edu/java/13flow/DivisorPattern.java.html","DivisorPattern");
		readURLContentWriteToFile("http://introcs.cs.princeton.edu/java/13flow/HarmonicNumber.java.html","HarmonicNumber");
		readURLContentWriteToFile("http://introcs.cs.princeton.edu/java/13flow/Sqrt.java.html","Sqrt");
		readURLContentWriteToFile("http://introcs.cs.princeton.edu/java/13flow/Binary.java.html","Binary");
		readURLContentWriteToFile("http://introcs.cs.princeton.edu/java/13flow/Gambler.java.html","Gambler");
		readURLContentWriteToFile("http://introcs.cs.princeton.edu/java/13flow/Factors.java.html","Factors");		
		readURLContentWriteToFile("http://introcs.cs.princeton.edu/java/14array/Sample.java.html","Sample");
		readURLContentWriteToFile("http://introcs.cs.princeton.edu/java/14array/CouponCollector.java.html","CouponCollector");
		readURLContentWriteToFile("http://introcs.cs.princeton.edu/java/14array/PrimeSieve.java.html","PrimeSieve");
		readURLContentWriteToFile("http://introcs.cs.princeton.edu/java/14array/SelfAvoidingWalk.java.html","SelfAvoidingWalk");
		readURLContentWriteToFile("http://introcs.cs.princeton.edu/java/15inout/RandomSeq.java.html","RandomSeq");
		readURLContentWriteToFile("http://introcs.cs.princeton.edu/java/15inout/TwentyQuestions.java.html","TwentyQuestions");
		readURLContentWriteToFile("http://introcs.cs.princeton.edu/java/15inout/Average.java.html","Average");
		readURLContentWriteToFile("http://introcs.cs.princeton.edu/java/15inout/RangeFilter.java.html","RangeFilter");
		readURLContentWriteToFile("http://introcs.cs.princeton.edu/java/15inout/PlotFilter.java.html","PlotFilter");
		readURLContentWriteToFile("http://introcs.cs.princeton.edu/java/15inout/BouncingBall.java.html","BouncingBall");
		readURLContentWriteToFile("http://introcs.cs.princeton.edu/java/15inout/PlayThatTune.java.html","PlayThatTune");
		readURLContentWriteToFile("http://introcs.cs.princeton.edu/java/16pagerank/Transition.java.html","Transition");
		readURLContentWriteToFile("http://introcs.cs.princeton.edu/java/16pagerank/RandomSurfer.java.html","RandomSurfer");
		readURLContentWriteToFile("http://introcs.cs.princeton.edu/java/16pagerank/Markov.java.html","Markov");
	}

}
