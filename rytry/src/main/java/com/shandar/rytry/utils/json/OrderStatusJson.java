package com.shandar.rytry.utils.json;

import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
/*import com.sears.rewards.dto.orderstatus.OrderStatusDTO;
*/
public class OrderStatusJson {

	public static void main(String[] args) {

		Map<String, OrderStatusDTO> orderStatusDTOMap = new HashMap<String, OrderStatusDTO>();

		Gson objGson = new GsonBuilder().setPrettyPrinting().create();

		OrderStatusDTO orderStatusDTO1 = new OrderStatusDTO();
		OrderStatusDTO orderStatusDTO2 = new OrderStatusDTO();

		orderStatusDTO1.setLastUpdateTS("2016-05-12 16:24:33");
		orderStatusDTO1.setOrderId("941258820");
		orderStatusDTO1.setOrderStatus("PRO");
		orderStatusDTO1.setOrderStatusDesc("PROCESSING");

		orderStatusDTO2.setLastUpdateTS("2010-04-1 16:24:33");
		orderStatusDTO2.setOrderId("ABCD941258820");
		orderStatusDTO2.setOrderStatus("PRO");
		orderStatusDTO2.setOrderStatusDesc("PROCESSING");

		orderStatusDTOMap.put(orderStatusDTO1.getOrderId(), orderStatusDTO1);
		orderStatusDTOMap.put(orderStatusDTO2.getOrderId(), orderStatusDTO2);

		String orderStatusDTOMapJson = objGson.toJson(orderStatusDTOMap);

		System.out.println(orderStatusDTOMapJson);



		/*JsonElement orderStatusDTOMapElement = new JsonParser().parse(orderStatusDTOMapJson);
		JsonObject orderStatusDTOMapObj = orderStatusDTOMapElement.getAsJsonObject();
		@SuppressWarnings("unchecked")
		HashMap<String, OrderStatusDTO> keyValueMap = new Gson().fromJson(orderStatusDTOMapObj,
				new HashMap<String, OrderStatusDTO>().getClass());*/

		Map<String, Object> jsonToMap = objGson.fromJson(orderStatusDTOMapJson,  new HashMap<String, Object>().getClass());

		StringBuffer stringBuffer = new StringBuffer();
		for (Map.Entry<String, Object> entry : jsonToMap.entrySet()) {
			stringBuffer.append(entry.getKey());
			stringBuffer.append(" = ");
			stringBuffer.append(entry.getValue());
			stringBuffer.append(" ");
		}
		System.out.println(stringBuffer);
	}
}


	/*public static void main(String[] args) {

		Map<String, List<OrderStatusDTO>> orderStatusDTOMap = new HashMap<String, List<OrderStatusDTO>>();
		List<OrderStatusDTO> orderStatusDTOList1 = new ArrayList<OrderStatusDTO>();

		String key = "7081075100514685_order";
		OrderStatusDTO orderStatusDTO1 = new OrderStatusDTO();
		OrderStatusDTO orderStatusDTO2 = new OrderStatusDTO();

		orderStatusDTO1.setLastUpdateTS("2016-05-12 16:24:33");
		orderStatusDTO1.setOrderId("941258820");
		orderStatusDTO1.setOrderStatus("PRO");
		orderStatusDTO1.setOrderStatusDesc("PROCESSING");

		orderStatusDTO2.setLastUpdateTS("2010-04-1 16:24:33");
		orderStatusDTO2.setOrderId("ABCD941258820");
		orderStatusDTO2.setOrderStatus("PRO");
		orderStatusDTO2.setOrderStatusDesc("PROCESSING");

		orderStatusDTOList1.add(orderStatusDTO1);
		orderStatusDTOList1.add(orderStatusDTO2);

		orderStatusDTOMap.put(key, orderStatusDTOList1);


		JsonConvertor<OrderStatusDTO> jsonConvertor = new JsonConvertor<OrderStatusDTO>();
		String orderStatusDTOMapJson = null;
		try {
			orderStatusDTOMapJson = jsonConvertor.toJson(orderStatusDTOMap);
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(orderStatusDTOMapJson);


		JsonElement orderStatusDTOMapElement = new JsonParser().parse(orderStatusDTOMapJson);
		JsonObject orderStatusDTOMapObj = orderStatusDTOMapElement.getAsJsonObject();
		@SuppressWarnings("unchecked")
		HashMap<String,List<OrderStatusDTO>> keyValueMapFromDb = new Gson().fromJson(orderStatusDTOMapObj, new HashMap<String, List<OrderStatusDTO>>().getClass());

		StringBuffer stringBuffer = new StringBuffer();
		for(Entry<String, List<OrderStatusDTO>> entry : keyValueMapFromDb.entrySet()) {
			stringBuffer.append(entry.getKey());
			stringBuffer.append(" = ");
			stringBuffer.append(entry.getValue());
			stringBuffer.append(" ");

			List<OrderStatusDTO> obj = entry.getValue();
			if(obj instanceof ArrayList) {

				ArrayList<OrderStatusDTO> orderStatusDTOList2 = (ArrayList<OrderStatusDTO>) obj;

				for(int i=0; i < orderStatusDTOList2.size(); i++) {

					OrderStatusDTO orderStatusDTO3 = (OrderStatusDTO) orderStatusDTOList2.get(0);

					LinkedTreeMap<String, Object> linkedTreeMap = (LinkedTreeMap<String, Object>) orderStatusDTOList2.get(i);
					for(Map.Entry<String, Object> entry2 : linkedTreeMap.entrySet()) {
						System.out.println("key " + entry2.getKey());
						System.out.println("val " + entry2.getValue());
					}
				}
			}
		}
		//System.out.println(stringBuffer.toString());
	}*/
