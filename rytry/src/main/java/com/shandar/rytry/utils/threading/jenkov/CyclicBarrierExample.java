/**
* Project: rytry
* Package Name:package com.shandar.rytry.utils.threading.jenkov;
* File Name: CyclicBarrierExample.java
* Create Date: Dec 21, 2016
* Create Time: 1:48:50 PM
* Copyright: Copyright (c) 2016
* @author: Shantanu Sikdar, ssikdar
* @version 1.0
*/
package com.shandar.rytry.utils.threading.jenkov;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

//http://javarevisited.blogspot.in/2012/07/cyclicbarrier-example-java-5-concurrency-tutorial.html
public class CyclicBarrierExample {

	public static void main(String[] args) {
		//creating CyclicBarrier with 3 parties i.e. 3 Threads needs to call await()
        final CyclicBarrier cb = new CyclicBarrier(2, new Runnable(){
            @Override
            public void run(){
                //This task will be executed once all thread reaches barrier
                System.out.println("All parties are arrived at barrier, lets play");
            }
        });

        //starting each of thread
        Thread t1 = new Thread(new Task(cb), "Thread 1");
        Thread t2 = new Thread(new Task(cb), "Thread 2");
        Thread t3 = new Thread(new Task(cb), "Thread 3");

        t1.start();
        t2.start();
        t3.start();

	}

}

class Task implements Runnable{
	
	private CyclicBarrier cyclicBarrier;

	public Task(CyclicBarrier cyclicBarrier) {
		this.cyclicBarrier=cyclicBarrier;
	}
	
	@Override
	public void run() {
		try{
			System.out.println(Thread.currentThread().getName() + " is waiting on barrier");
			cyclicBarrier.await();
	        System.out.println(Thread.currentThread().getName() + " has crossed the barrier");
		}catch(BrokenBarrierException ie){
			ie.printStackTrace();
		}catch(InterruptedException ie){
			ie.printStackTrace();
		}

	}
	
}