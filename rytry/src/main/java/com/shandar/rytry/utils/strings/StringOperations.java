/**
* Project: rytry
* Package Name:package com.shandar.rytry.utils.regex;
* File Name: StringOperations.java
* Create Date: Mar 7, 2017
* Create Time: 7:52:16 PM
* Copyright: Copyright (c) 2016
* @author: Shantanu Sikdar, ssikdar
* @version 1.0
*/
package com.shandar.rytry.utils.strings;

public class StringOperations {

	public static void main(String[] args) {
		StringOperations strOprn = new StringOperations();
		strOprn.firstLetterUpperCase("shantanu want to learn java");
	}
	
	private void firstLetterUpperCase(String str){
		char[] strChar = str.toCharArray();
		for (int i = 0; i < strChar.length; i++) {
			if(i==0){
				strChar[i]=((strChar[i]+"").toUpperCase()).charAt(0);
			}else if(strChar[i]==' '){
				strChar[i+1]=((strChar[i+1]+"").toUpperCase()).charAt(0);
			}
		}
		String newStr = new String(strChar);
		System.out.println(newStr);
	}

}
