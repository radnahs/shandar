/**
* Project: rytry
* Package Name:package com.shandar.rytry.utils.threading.tutorial2;
* File Name: LoopTaskA.java
* Create Date: Dec 19, 2016
* Create Time: 7:50:11 PM
* Copyright: Copyright (c) 2016
* @author: Shantanu Sikdar, ssikdar
* @version 1.0
*/
package com.shandar.rytry.utils.threading.tutorial2;

import java.util.concurrent.TimeUnit;

public class LoopTaskA implements Runnable {

	private static int count=0;
	private int id;
	
	@Override
	public void run() {
		System.out.println("######## <Task "+id +"> Starting ########");
		
		for (int i = 10; i >0; i--) {
			System.out.println(Thread.currentThread().getName()+" <"+id+"> Tick tick"+i);
			try {
				//TimeUnit.MILLISECONDS.sleep(200);
				TimeUnit.MILLISECONDS.sleep((long)Math.random()*1000);
			} catch (InterruptedException iex) {
				iex.printStackTrace();
			}
		}
		
		System.out.println("********* <Task "+id +"> Done *********");
	}
	
	public LoopTaskA() {
		this.id = ++count;
	}

}
