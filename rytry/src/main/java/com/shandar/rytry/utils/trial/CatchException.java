/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.utils.trial </p>
 * <p>File Name: CatchException.java</p>
 * <p>Create Date: Jan 27, 2015 </p>
 * <p>Create Time: 2:10:57 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.utils.trial;

/**
 * @author Shantanu Sikdar 
 *
 */
public class CatchException {

	/**
	 * @param args
	 */
	public static void main(String[] args){

		try {
			CatchException ce = new CatchException();
			ce.testCatch();			
		} catch (Exception e) {
			// TODO: handle exception
		}
		System.out.println("hello");
	}
	
	private void testCatch() throws Exception{
		String str = null;
		String str1 = null;
		try{
			str1 = "null";
			
			//if(str.equals(str1)){}
			throw new Exception();
			
			//System.out.println("after exception");
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}finally{
			System.out.println("in finally");
		}
		System.out.println("just outside finally");
	}	

}
