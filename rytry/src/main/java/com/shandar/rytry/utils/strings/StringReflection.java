/**
* Project: rytry
* Package Name:package com.shandar.rytry.utils.strings;
* File Name: StringReflection.java
* Create Date: Mar 8, 2017
* Create Time: 12:02:32 PM
* Copyright: Copyright (c) 2016
* @author: Shantanu Sikdar, ssikdar
* @version 1.0
*/
package com.shandar.rytry.utils.strings;

import java.lang.reflect.Modifier;

public class StringReflection {

	public static void main(String[] args) {
		// Get Class using reflection
		Class<?> concreteClass = String.class;
		concreteClass = new String().getClass();
		try {
			// below method is used most of the times in frameworks like JUnit
			//Spring dependency injection, Tomcat web container
			//Eclipse auto completion of method names, hibernate, Struts2 etc.
			//because ConcreteClass is not available at compile time
			concreteClass = Class.forName("java.lang.String");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		System.out.println(concreteClass.getCanonicalName()); // prints java.lang.String
		Modifier modifier = new Modifier();
		System.out.println(new Modifier().toString(concreteClass.getModifiers()) );
		//System.out.println(modifier.toString(concreteClass.getModifiers()) );// prints java.lang.String modifiers
		
		
	}

}
