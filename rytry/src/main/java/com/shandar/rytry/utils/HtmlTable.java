package com.shandar.rytry.utils;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

public class HtmlTable {
	
	public static void main(String[] args) {
		int[][] values = {
				{1,3894,5387},
				{2,4112,4459},
				{3,4886,6076}
			};
		System.out.println(new HtmlTable().createHTML(values));
	}
	
	public String createHTML(int[][] values){
		final StringBuilder sb = new StringBuilder();
		for (int ii=0; ii<values.length; ii++) {
			sb.append("<tr>");
			for (int jj=0; jj<values[ii].length; jj++) {
				sb.append("<td>");
				sb.append("" + values[ii][jj]);
				sb.append("</td>");
			}
			sb.append("</tr>\n");
		}
		return sb.toString();
	}
	
	

	public static void main1(String[] args) {
		int[][] values = {
				{1,3894,5387},
				{2,4112,4459},
				{3,4886,6076}
			};
			String prefix = "<html><body><table>\n";
	 
			final StringBuilder sb = new StringBuilder(prefix);
			sb.append("<tr>");
			sb.append("<th>");
			sb.append("Month");
			sb.append("</th>");
			sb.append("<th>");
			sb.append("Unit A<br>Sales");
			sb.append("</th>");
			sb.append("<th>");
			sb.append("Unit B<br>Sales");
			sb.append("</th>");
			sb.append("</tr>\n");
			for (int ii=0; ii<values.length; ii++) {
				sb.append("<tr>");
				for (int jj=0; jj<values[ii].length; jj++) {
					sb.append("<td>");
					sb.append("" + values[ii][jj]);
					sb.append("</td>");
				}
				sb.append("</tr>\n");
			}
			sb.append("</table>");
			sb.append("</body>");
			sb.append("</html>");
	 
			Runnable r = new Runnable() {
				public void run() {
					JOptionPane.showMessageDialog( 
						null, 
						new JTextArea(sb.toString(),20,10) );
					JOptionPane.showMessageDialog( 
						null, 
						new JLabel(sb.toString()) );
				}
			};
			SwingUtilities.invokeLater(r);

	}

}
