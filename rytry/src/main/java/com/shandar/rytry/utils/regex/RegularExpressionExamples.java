/**
* Project: rytry
* Package Name:package com.shandar.rytry.utils.regex;
* File Name: RegularExpressionExamples.java
* Create Date: Oct 3, 2016
* Create Time: 1:55:08 PM
* Copyright: Copyright (c) 2016
* @author: Shantanu Sikdar, ssikdar
* @version 1.0
*/
package com.shandar.rytry.utils.regex;

public class RegularExpressionExamples {

	public static void main(String[] args) {
		RegularExpressionExamples ree = new RegularExpressionExamples();
		//ree.validateWebItemId("23142413.345");
		//ree.regexChecker("234132341322","\\d+");//check non decimal number
		ree.regexChecker("3.3","\\d+(\\.\\d{1,2})?");//assume that you must have both at least one digit before and one after the decimal place
		
	}
	
	private boolean validateWebItemId(String checkStr) {
		System.out.println("checkStr = " + checkStr);
		boolean validWbItmId = false;
		String regex = "\\d+";
		if(checkStr.matches(regex)){
			System.out.println("checkStr = " + checkStr);
		}
		return validWbItmId; 
	}
	
	public void regexChecker(String checkStr, String regex){
		System.out.println("Matching string = " + checkStr);
		System.out.println("Regular expression = " + regex);
		if(checkStr.matches(regex)){
			System.out.println("checkStr = " + checkStr);
		}else{
			System.out.println("Not match");
		}
	}

}
