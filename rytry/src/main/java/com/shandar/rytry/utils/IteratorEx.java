/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.utils </p>
 * <p>File Name: IteratorEx.java</p>
 * <p>Create Date: Jan 29, 2014 </p>
 * <p>Create Time: 3:52:18 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author Shantanu Sikdar 
 *
 */
public class IteratorEx {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		/*List<String> myList = new ArrayList<String>();
		
		myList.add("1");
		myList.add("2");
		myList.add("3");
		myList.add("4");
		myList.add("5");
		
		Iterator<String> it = myList.iterator();
		while(it.hasNext()){
			String value = it.next();
			System.out.println("List Value:"+value);
			if(value.equals("3")) myList.remove(value);
		}*/
		
		
		
		Map<String,String> myMap = new HashMap<String,String>();
		myMap.put("1", "1");
		myMap.put("2", "2");
		myMap.put("3", "3");

		Iterator<String> it1 = myMap.keySet().iterator();
		while(it1.hasNext()){
			String key = it1.next();
			System.out.println("Map Value:"+myMap.get(key));
			myMap.remove(key);
			/*if(key.equals("2")){
				myMap.remove(key);
				//myMap.put("1","4");
				//myMap.put("4", "4");
			}*/
		}
		
		
	}

}
