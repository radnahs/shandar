/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.utils.threading </p>
 * <p>File Name: CrunchifyProducerConsumer.java</p>
 * <p>Create Date: Apr 17, 2015 </p>
 * <p>Create Time: 5:14:09 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.utils.threading;

import java.util.Iterator;
import java.util.Vector;

/**
 * @author Shantanu Sikdar 
 *
 */
public class CrunchifyProducerConsumer {
	private static Vector<Object> data = new Vector<Object>();
	 
    public static void main(String[] args) {
        new Producer().start();
        new Consumer().start();
    }
 
    public static class Consumer extends Thread {
        Consumer() {
            super("ConsumerYield");
        }
 
        @Override
        public void run() {
            for (;;) {
                try {
                    Thread.sleep(1);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //@SuppressWarnings("rawtypes")
                synchronized(data){
                	System.out.println("ConsumerYield = "+data.size());
                	Iterator it = data.iterator();
                	while (it.hasNext())
                		it.next();
                }
            }
        }
    }
 
    public static class Producer extends Thread {
        Producer() {
            super("ProducerYield");
        }
 
        @Override
        public void run() {
            for (;;) {
                try {
                    Thread.sleep(1);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                synchronized(data){
                	System.out.println("ProducerYield = "+data.size());
                	data.add(new Object());
                	if (data.size() > 10)
                		data.remove(data.size() - 1);
                }
            }
        }
    }
}	
