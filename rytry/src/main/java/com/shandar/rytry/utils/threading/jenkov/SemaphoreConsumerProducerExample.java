/**
* Project: rytry
* Package Name:package com.shandar.rytry.utils.threading.jenkov;
* File Name: SemaphoreConsumerProducerExample.java
* Create Date: Dec 20, 2016
* Create Time: 12:03:29 PM
* Copyright: Copyright (c) 2016
* @author: Shantanu Sikdar, ssikdar
* @version 1.0
*/
package com.shandar.rytry.utils.threading.jenkov;

import java.util.concurrent.Semaphore;

public class SemaphoreConsumerProducerExample {
	
	 public static void main(String[] args) {
		Semaphore semaphoreProducer = new Semaphore(1);
		Semaphore semaphoreConsumer = new Semaphore(0);
		
		System.out.println("semaphoreProducer permit=1 | semaphoreConsumer permit=0");
		
		Producer producer=new Producer(semaphoreProducer,semaphoreConsumer);
	    Consumer consumer=new Consumer(semaphoreConsumer,semaphoreProducer);
	    
	    Thread producerThread = new Thread(producer, "ProducerYield Thread");
	    Thread consumerThread = new Thread(consumer, "ConsumerYield Thread");
	    
	    producerThread.start();
	    consumerThread.start();
	    
	 }
}

class Producer implements Runnable{
	
	Semaphore semaphoreProducer;
    Semaphore semaphoreConsumer;
    
    public Producer(Semaphore semaphoreProducer,Semaphore semaphoreConsumer) {
    	this.semaphoreProducer=semaphoreProducer;
    	this.semaphoreConsumer=semaphoreConsumer;
	}

	@Override
	public void run() {
		 for(int i=1;i<=5;i++){
             try {
                 semaphoreProducer.acquire();
                 System.out.println("Produced : "+i);
                 semaphoreConsumer.release();
                   
             } catch (InterruptedException e) {
                   e.printStackTrace();
             }
		 } 
	}
}

class Consumer implements Runnable{
	
	Semaphore semaphoreProducer;
    Semaphore semaphoreConsumer;
    
    public Consumer(Semaphore semaphoreConsumer,Semaphore semaphoreProducer) {
    	this.semaphoreConsumer=semaphoreConsumer;
    	this.semaphoreProducer=semaphoreProducer;
	}
    
	@Override
	public void run() {
		for(int i=1;i<=5;i++){
            try {
                semaphoreConsumer.acquire();
                System.out.println("Consumed : "+i);
                semaphoreProducer.release();
            } catch (InterruptedException e) {
                  e.printStackTrace();
            }
		}
	}
	
}
