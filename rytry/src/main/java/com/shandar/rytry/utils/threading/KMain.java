/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.utils.threading </p>
 * <p>File Name: K1.java</p>
 * <p>Create Date: Jul 23, 2015 </p>
 * <p>Create Time: 4:24:54 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.utils.threading;

/**
 * @author Shantanu Sikdar 
 *
 */
public class KMain {

	public static void main(String[] args) throws Exception{
		Thread t1 = new Thread(new K1());
		Thread t2 = new Thread(new K2());
		Thread t3 = new Thread(new K3());
		t1.start();
		Thread.sleep(100);
 
		t2.start();
		Thread.sleep(100);
		
		t3.start();
		Thread.sleep(100);
	}

}

class K1 implements Runnable {

	@Override
	public void run() {
		for (int i = 0; i < 5; i++) {
			System.out.println("T1");
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}	
	}
}

class K2 implements Runnable {
	@Override
	public void run() {
		for (int i = 0; i < 5; i++) {
			System.out.println("T2");
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}			
	}

}
class K3 implements Runnable {

	@Override
	public void run() {
		for (int i = 0; i < 5; i++) {
			System.out.println("T3");
			try {
				Thread.sleep(500);				
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}	
	}

}
