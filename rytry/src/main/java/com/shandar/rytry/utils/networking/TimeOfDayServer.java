/**
* Project: rytry
* Package Name:package com.shandar.rytry.utils.networking;
* File Name: TimeOfDayServer.java
* Create Date: Dec 25, 2016
* Create Time: 11:27:46 AM
* Copyright: Copyright (c) 2016
* @author: Shantanu Sikdar, ssikdar
* @version 1.0
*/
package com.shandar.rytry.utils.networking;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.Channel;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Set;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;


public class TimeOfDayServer {

	public static void main(String[] args) {
		try {
			CharsetEncoder encoder = Charset.forName("US-ASCII").newEncoder();
			int port=55555;
			
			SocketAddress localPort =  new InetSocketAddress(port);

			ServerSocketChannel tcpServer = ServerSocketChannel.open();
			tcpServer.socket().bind(localPort);
			
			DatagramChannel udpServer = DatagramChannel.open();
			udpServer.socket().bind(localPort);
			
			tcpServer.configureBlocking(false);
			udpServer.configureBlocking(false);
			
			Selector selector = Selector.open();
			
			tcpServer.register(selector, SelectionKey.OP_ACCEPT);
			udpServer.register(selector, SelectionKey.OP_READ);
			
			ByteBuffer receiveBuffer = ByteBuffer.allocate(0);
			
			for(;;){
				try {
					 selector.select();
					 
					 Calendar calParis = Calendar.getInstance();
					 calParis.setTimeZone(TimeZone.getTimeZone("Europe/Paris"));
					 String time = "Time in Paris .... " + calParis.get(Calendar.HOUR_OF_DAY) + " :" +calParis.get(Calendar.MINUTE);
					 
					 ByteBuffer response = encoder.encode(CharBuffer.wrap(time));
					 
					 Set<SelectionKey> keys = selector.selectedKeys();
					 
					 for (Iterator iterator = keys.iterator(); iterator.hasNext();) {
						SelectionKey selectionKey = (SelectionKey) iterator.next();
						iterator.remove();
						
						Channel chnl = (Channel)selectionKey.channel();
						
						if(selectionKey.isAcceptable() && chnl == tcpServer){
							SocketChannel client = tcpServer.accept();
							if (client != null) {
								client.write(response);
								client.close();
							}
						}else if (selectionKey.isReadable() && chnl == udpServer) {
							SocketAddress clientAddress = udpServer.receive(receiveBuffer);
							if(clientAddress != null){
								udpServer.send(response, clientAddress);
							}
						}
					}
				} catch (IOException ioe) {
					Logger l = Logger.getLogger(TimeOfDayServer.class.getName());
					l.log(Level.WARNING, "IOException in TimeOfDayServer", ioe);
				}catch (Throwable t) {
					Logger l = Logger.getLogger(TimeOfDayServer.class.getName());
					l.log(Level.SEVERE, "Fatal Error in TimeOfDayServer", t);
				}
			}
			
		} catch (Exception e) {
			System.err.println(e);
			System.exit(1);
		}
	}

}
