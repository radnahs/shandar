/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.utils.threading </p>
 * <p>File Name: ITCMain.java</p>
 * <p>Create Date: Jul 24, 2015 </p>
 * <p>Create Time: 1:11:30 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.utils.threading;

/**
 * @author Shantanu Sikdar 
 *
 */
public class ITCMain {
	
	
	public static void main(String[] args) {
		String s1="shantanu";
		String s2="shantanu";
		System.out.println(s1==s2);
		String s3 = new String("shantanu");
		System.out.println(s1==s3);
		String s4 = new String(s2);
		System.out.println(s4==s1);
		System.out.println(s1.compareTo(s2));
		System.out.println(s1.compareTo(s3));
	}

}
