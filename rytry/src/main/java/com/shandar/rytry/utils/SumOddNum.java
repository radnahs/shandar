/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.utils </p>
 * <p>File Name: SumOddNum.java</p>
 * <p>Create Date: Jun 18, 2014 </p>
 * <p>Create Time: 6:15:00 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.utils;

/**
 * @author Shantanu Sikdar 
 *
 */
public class SumOddNum {
	
	public static void main(String[] args) {
		int[] iarr =  {1,3,5,7,13};//{1,3,5,7,9};
		//{1,3,5,7,9,11,13,15};
		System.out.println(1/5);
		sum(iarr);
	}
	
	private static void sum(int[] iarr){
		int sum=0;
		int prod=1;
		for (int i : iarr) {			
			prod*=i;
		}		
		for (int i : iarr) {			
			sum+=prod/i;
		}
		System.out.println(sum);
		System.out.println(prod);
	}

}
