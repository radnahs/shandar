/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.utils </p>
 * <p>File Name: CalvinHobbes.java</p>
 * <p>Create Date: Jan 10, 2014 </p>
 * <p>Create Time: 4:52:41 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Shantanu Sikdar 
 *
 */
public class CalvinHobbes {
	
	
	public static void main(String[] args) {
		
		List<Integer> list = new ArrayList<>();
		list.add(6); 		list.add(4); 		list.add(2); 		list.add(8); 		list.add(2); 		list.add(1);
		
		//combFromSeries(list,new ArrayList(list));
		//combFromSeries(0, "", list,new ArrayList(list));
		permutationsWithoutRepetition(0, 0, "", list,new ArrayList<Integer>(list));
		System.out.println(shasha +"---"+ calvin);
		System.out.println(list);
		
		/*String prefx="6|1|";
		System.out.println(prefx);
		String[] str=prefx.split("|");
		for (String string : str) {
			System.out.println(string);
		}
		System.out.println(str.length);*/
		
	}
	
	static List<Integer> calvin = new ArrayList<>();
	static List<Integer> shasha = new ArrayList<>();
	
	private static void combFromSeries(int n, String prefix, List<Integer>...incrList){		
		 if(n >= incrList.length){	        
			System.out.println("{"+prefix+"}");			
	        return;
	     }	     
	     for(Integer intgr : incrList[n]){	    	 
	    	 combFromSeries(n+1, prefix+intgr+" || ", incrList);
	     }		
	}
		
	private static void permutationsWithoutRepetition(int n, int index, String prefix, List<Integer>...incrList) throws NumberFormatException{		
		if(n >= incrList.length){
			System.out.println(prefix);
			String[] str=prefix.split("::");
			System.out.println(str.length);
			shasha.add(new Integer(str[0]));
			calvin.add(new Integer(str[1]));
			
	        return;
	    }
	    for(int i=index;i<incrList[n].size();i++){	    	     
	    	permutationsWithoutRepetition(n+1, i+1, prefix+incrList[n].get(i)+"::", incrList);
	    }		
	}
	
	
	/*private static void combFromSeries(List<Integer>...incrList){
		System.out.println("\ni=="+i);
		int numOLists= incrList.length;		
		if(i>numOLists-1){
			System.out.println("END");
		}else{
			System.out.println(incrList[0].get(i) + " : " + incrList[1].get(i+1));
			System.out.println(incrList[0] + " || " + incrList[1]);
			incrList[1].remove(i);			
			combFromSeries(incrList[i],incrList[i+1]);
			i++;
		}
	}*/
	
	/*	int j=0;
			List<Integer> lst = new ArrayList<>(list);
			while(lst.size()>0){
				System.out.println(lst.get(j));
				lst.remove(j);
				j++;
			}
			list.remove(i);
			i++;
			combFromSeries(list);
	*/
	
	/*public static void main(String[] args){
        String[] set1 = {"1","2"};
        String[] set2 = {"A","B","C"};
        String[] set3 = {"$", "%", "�", "!"};
        String[][] sets = {set1, set2, set3};
        printCombinations(sets, 0, "");
    }
	private static void printCombinations(String[][] sets, int n, String prefix){
        if(n >= sets.length){
            System.out.println("{"+prefix.substring(0,prefix.length()-1)+"}");
            return;
        }
        for(String s : sets[n]){
            printCombinations(sets, n+1, prefix+s+",");
        }
    }*/

	public static void mainNotWorking(String[] args) {
		
		List<Integer> list = new ArrayList<>();
		list.add(6); 		list.add(4); 		list.add(2); 		list.add(8); 		list.add(2); 		list.add(1);
		
		List<Integer> calvin = new ArrayList<>();
		List<Integer> shasha = new ArrayList<>();
				
		while(list.size()>0){
			int i=0,j=list.size()-1;
			
			if(list.get(i)>list.get(j)){
				calvin.add(list.get(i));
				list.remove(i);
				j=list.size()-1;
				
				if(list.get(i)>list.get(j)){
					shasha.add(list.get(i));					
					list.remove(i);
				}else{
					shasha.add(list.get(j));					
					list.remove(j);
				}								
			}else{
				calvin.add(list.get(j));
				list.remove(j);
				j=list.size()-1;
				
				if(list.get(i)>list.get(j)){
					shasha.add(list.get(i));					
					list.remove(i);
				}else{
					shasha.add(list.get(j));					
					list.remove(j);
				}
			}			
		}
		System.out.println("calvin = "+calvin);
		System.out.println("shasha = "+shasha);
		
	}

}
