package com.shandar.rytry.utils.fileConversion;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CSVToSpaceSeparated {

	public static void main(String[] args) {
		CSVToSpaceSeparated ctss = new CSVToSpaceSeparated();
		ctss.readDATFile();
	}

	private List<String> readDATFile() {
		List<String> lstStr = new ArrayList<String>();
		try {
			//String fileName = "C:\\Data\\misc\\Load Data batch job\\Product_hierarchy_hist_data(KS 20000)";
			//String fileName = "C:\\Data\\misc\\Load Data batch job\\Product_hierarchy_hist_data(LS 20000)";
			//String fileName = "C:\\Data\\misc\\Load Data batch job\\Product_hierarchy_hist_data(SL 20000)";
			String fileName = "C:\\Data\\misc\\Load Data batch job\\prod_hier_1_lakh_rec";
			File file = new File(fileName+".csv");
			Scanner scanner = new Scanner(file);

			File fileWrite = new File(fileName+".DAT");
			FileWriter fw = new FileWriter(fileWrite.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			if (!fileWrite.exists()) {
				fileWrite.createNewFile();
			}
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				String strq = processData("02," + line);
				bw.write(strq);
				bw.newLine();
			}
			scanner.close();
			bw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lstStr;
	}

	private String processData(String str) {
		StringBuilder sbldr = new StringBuilder();
		try {
			String[] strArr = str.split("\\,");
			for (FileDataStructure start : FileDataStructure.values()) {
				int indx = start.getKey() - 1;
				if (indx < strArr.length) {
					sbldr.append(appendSpaces(strArr[indx], start.getCount()));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sbldr.toString();
	}

	private String appendSpaces(String str, int count) {
		String lftPadStr = String.format("%-" + count + "s", str);
		return lftPadStr;
	}

}
