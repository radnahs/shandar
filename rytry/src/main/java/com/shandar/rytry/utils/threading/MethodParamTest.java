/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.utils.threading </p>
 * <p>File Name: MethodParamTest.java</p>
 * <p>Create Date: Mar 7, 2016 </p>
 * <p>Create Time: 5:56:52 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.utils.threading;

/**
 * @author Shantanu Sikdar
 * 
 */

public class MethodParamTest {

	public static void main(String[] args) {
		final MethodClass mc = new MethodClass();
		final EmployeeVO emp = new EmployeeVO();
		final EmployeeClass empcl = new EmployeeClass();
		Thread t1 = new Thread(){
								public void run(){
									/*EmployeeVO emp = new EmployeeVO();*/
									emp.setName("qwe");
									empcl.method2(emp);
									//mc.method1("abc");
								}
							};

		Thread t2 = new Thread(){
								public void run(){
									/*EmployeeVO emp = new EmployeeVO();*/
									emp.setName("asd");
									empcl.method2(emp);
									//mc.method1("xyz");
								}
							};

		t1.setName("thread-1");
		t2.setName("thread-2");
		t1.start();
		t2.start();
	}
}

class MethodClass {
	/*public void method1(String name) {
		EmployeeVO emp = new EmployeeVO();
		emp.setName(name);
		EmployeeClass employee = new EmployeeClass();
		employee.method2(emp);
	}*/
	 //public void method1(EmployeeVO emp ) {
	/*public synchronized void method1(EmployeeVO emp ) {
		EmployeeClass employee = new EmployeeClass();
		for (int i = 0; i < 5; i++) {
			try {
				System.out.println(i);
				System.out.println(Thread.currentThread().getName());
				Thread.sleep(400);
			} catch (Exception e) {
				System.out.println(e);
			}
		}		
		employee.method2(emp);
	}*/
}

class EmployeeClass {
	public void method2(EmployeeVO emp) {
		
		System.out.println("thread name :" + Thread.currentThread() + "name: "	+ emp.getName());
	}
}

class EmployeeVO {
	private String name;
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
