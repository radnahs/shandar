/**
 *
 * <p>Project: trunk_rytry </p>
 * <p>Package Name: com.shandar.rytry.utils.langSupport </p>
 * <p>File Name: LangSupp.java</p>
 * <p>Create Date: Jul 22, 2013 </p>
 * <p>Create Time: 5:50:53 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.utils.langSupport;

import java.util.Locale;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author Shantanu Sikdar
 * 
 */
public class LangSupp {
	
	
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("locale.xml");
		//ApplicationContext context = new ClassPathXmlApplicationContext("/WEB-INF/spring/appServlet/servlet-context.xml");
		String customerName = context.getMessage("customer.name", new Object[] { 31,"www.shandar.com" }, Locale.US);
		System.out.println("Customer name (English) : " + customerName);
		String namechinese = context.getMessage("customer.name", new Object[] {31, "www.shandar.com" }, Locale.SIMPLIFIED_CHINESE);
		System.out.println("Customer name (Chinese) : " + namechinese);
		
		String devayani = context.getMessage("label.devayani.pujari", new Object[] { 31,"www.shandar.com" }, Locale.US);
		System.out.println("Customer name (Hindi) : " + devayani);
		
		String appurva = context.getMessage("label.appurva", new Object[] { 31,"www.shandar.com" }, Locale.US);
		System.out.println("Customer name (Hindi) : " + appurva);
				
	}

}
