/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.utils.computer </p>
 * <p>File Name: Logger.java</p>
 * <p>Create Date: Jul 10, 2015 </p>
 * <p>Create Time: 2:57:49 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.utils.computer;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;

/**
 * @author Shantanu Sikdar 
 *
 */
public class rytryLogger extends PrintStream {
	
	 private final PrintStream second;

     public rytryLogger(OutputStream main, PrintStream second) {
         super(main);
         this.second = second;
     }
     
     @Override
     public void close() {
         // just for documentation
         super.close();
     }

     @Override
     public void flush() {
         super.flush();
         second.flush();
     }

     @Override
     public void write(byte[] buf, int off, int len) {
         super.write(buf, off, len);
         second.write(buf, off, len);
     }

     @Override
     public void write(int b) {
         super.write(b);
         second.write(b);
     }

     @Override
     public void write(byte[] b) throws IOException {
         super.write(b);
         second.write(b);
     }
	

	
	/*public static void main(String[] args) throws IOException{
		System.out.println("loggerssssss");
	    BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

	    BufferedWriter out = new BufferedWriter(new FileWriter("C:\\Data\\testLoggers.txt"));
	    try {
	        String inputLine = null;
	        do {
	            inputLine=in.readLine();
	            out.write(inputLine);
	            out.newLine();
	        } while (!inputLine.equalsIgnoreCase("eof"));
	        System.out.print("Write Successful");
	    } catch(IOException e1) {
	        System.out.println("Error during reading/writing");
	    } finally {
	        out.close();
	        in.close();
	    }
	}*/
}
