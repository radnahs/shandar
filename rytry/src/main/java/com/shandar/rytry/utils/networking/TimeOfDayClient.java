/**
* Project: rytry
* Package Name:package com.shandar.rytry.utils.networking;
* File Name: TimeOfDayClient.java
* Create Date: Dec 25, 2016
* Create Time: 11:01:34 AM
* Copyright: Copyright (c) 2016
* @author: Shantanu Sikdar, ssikdar
* @version 1.0
*/
package com.shandar.rytry.utils.networking;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketTimeoutException;


public class TimeOfDayClient {

	public static void main(String[] args) {
		try {
			String host = "localhost";
			int port = 55555;
			
			DatagramSocket socket = new DatagramSocket();
			socket.setSoTimeout(1000);
			byte[] buffer = new byte[512];
			DatagramPacket packet = new DatagramPacket(buffer, buffer.length, new InetSocketAddress(host,port));
			
			for (int i = 0; i < 3; i++) {
				try {
					packet.setLength(0);
					socket.send(packet);
					
					packet.setLength(buffer.length);
					socket.receive(packet);
					
					System.out.println(new String(buffer, 0, packet.getLength(), "US-ASCII"));
					
					break;
				} catch (SocketTimeoutException ste) {
					System.out.println("No Response");
					ste.printStackTrace();
				}
			}
			
			socket.close();
		
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}

}
