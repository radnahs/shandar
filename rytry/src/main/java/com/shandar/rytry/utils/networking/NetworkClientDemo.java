/**
* Project: rytry
* Package Name:package com.shandar.rytry.utils.networking;
* File Name: NetworkClientDemo.java
* Create Date: Dec 25, 2016
* Create Time: 10:44:11 AM
* Copyright: Copyright (c) 2016
* @author: Shantanu Sikdar, ssikdar
* @version 1.0
*/
package com.shandar.rytry.utils.networking;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

public class NetworkClientDemo {

	public static void main(String[] args) {
		javaClientSocketDemo();
	}
	
	private static void javaClientSocketDemo() {
		try {
			System.out.println("Starting client");
			Socket socket = new Socket("localhost",8182);
			
			PrintWriter pw =  new PrintWriter(socket.getOutputStream(),true);
			pw.println("Hello from client");
			pw.print("connected yes");
			
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}

}
