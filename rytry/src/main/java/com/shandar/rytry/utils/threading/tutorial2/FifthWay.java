/**
* Project: rytry
* Package Name:package com.shandar.rytry.utils.threading.tutorial2;
* File Name: FifthWay.java
* Create Date: Dec 19, 2016
* Create Time: 5:03:37 PM
* Copyright: Copyright (c) 2016
* @author: Shantanu Sikdar, ssikdar
* @version 1.0
*/
package com.shandar.rytry.utils.threading.tutorial2;

import java.util.concurrent.TimeUnit;

public class FifthWay {

	public static void main(String[] args) {
		System.out.println("Main method starts .... ");
		
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				for (int i = 10; i >0; i--) {
					System.out.println(Thread.currentThread().getName()+" Tick tick"+i);
					try {
						TimeUnit.MILLISECONDS.sleep(200);
					} catch (InterruptedException iex) {
						iex.printStackTrace();
					}
				}

			}
		}).start();
		
		System.out.println("Main method ends .... ");

	}

}

