package com.shandar.rytry.utils.fileConversion;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

public class ReadLogFileFindException {

	public static void main(String[] args) {
		ReadLogFileFindException rffe = new ReadLogFileFindException();
		//rffe.readFile("C:\\Data\\misc\\priyankaLog\\logs_18_05.txt");
		//rffe.readFile("C:\\Data\\misc\\priyankaLog\\temp_ps_18_05.txt");
		//rffe.readFile("C:\\Data\\misc\\priyankaLog\\logs_ps_20_05.txt");
		//rffe.readFile("C:\\Data\\misc\\priyankaLog\\logs_24_05_ps.txt");
		rffe.readFile("C:\\Data\\misc\\priyankaLog\\logs_25_05_ps.txt");
		//rffe.readFile( args[0]);
	}
	
	private List<String> readFile(String fileName) {
		List<String> lstStr = new ArrayList<String>();
		try {
			File file = new File(fileName);
			Scanner scanner = new Scanner(file);
			int lNum = 1;
			Map<String, List<List<String>>> compExcepTyp = new HashMap<String, List<List<String>>>();
			Map<String, Integer> compExcep = new HashMap<String, Integer>();
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				String[] strArr = line. split("\\|",-1);
				List<String> strLst = new ArrayList<>();
				strLst.add(lNum+"");
				for (int i = 0; i < strArr.length; i++) {
					strLst.add(strArr[i]);
				}
				String s1 = isBlankOrNull(strArr[2]);
				String s2 = isBlankOrNull(strArr[1]);
				String key = s1+" ===== "+s2;
				if(compExcepTyp.containsKey(key)){
					List<List<String>> lstLstStr = compExcepTyp.get(key); 
					lstLstStr.add(strLst);
					compExcepTyp.put(key, lstLstStr);
					compExcep.put(key, compExcep.get(key)+1);					
				}else{
					List<List<String>> lstLstStr = new ArrayList<>();
					lstLstStr.add(strLst);
					compExcepTyp.put(key, lstLstStr);
					compExcep.put(key, 1);
				}
				lNum++;
			}
			scanner.close();
			writeToFile(fileName, compExcep);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lstStr;
	}
	
	private void writeToFile(String fileName, Map<String, Integer> compExcep){
		try {
			File fileWrite = new File(fileName.replace(".txt", "CSV.csv"));
			FileWriter fw = new FileWriter(fileWrite.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			if (!fileWrite.exists()) {
				fileWrite.createNewFile();
			}

			for (Entry<String, Integer> entrMap : compExcep.entrySet()) {
				String[] splitKey = entrMap.getKey().split(" ===== ");
				String csvLine = "\"" + splitKey[0] + "\",\"" + splitKey[1]	+ "\",\"" + entrMap.getValue() + "\"";
				System.out.println(csvLine);
				bw.write(csvLine);
				bw.newLine();
			}
			bw.close();
		}catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private String isBlankOrNull(String str){
		return str==null||str.equalsIgnoreCase("")?"NULL":str;
	}
	
}
