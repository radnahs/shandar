/**
* Project: rytry
* Package Name:package com.shandar.rytry.utils.threading.tutorial2;
* File Name: CallableWay.java
* Create Date: Dec 26, 2016
* Create Time: 1:14:34 PM
* Copyright: Copyright (c) 2016
* @author: Shantanu Sikdar, ssikdar
* @version 1.0
*/
package com.shandar.rytry.utils.threading.tutorial2;

import java.math.BigInteger;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

//http://javarevisited.blogspot.com/2015/06/how-to-use-callable-and-future-in-java.html#ixzz4Tvo5bzay
public class CallableWay {

	public static void main(String[] args) throws Exception{
		// creating thread pool to execute task which implements Callable 
		ExecutorService es = Executors.newSingleThreadExecutor(); 

		System.out.println("submitted callable task to calculate factorial of 10"); 
		Future result10 = es.submit(new FactorialCalculator(10)); 
		
		System.out.println("submitted callable task to calculate factorial of 15");
		Future result15 = es.submit(new FactorialCalculator(15)); 
		
		System.out.println("submitted callable task to calculate factorial of 20"); 
		Future result20 = es.submit(new FactorialCalculator(20)); 
		
		System.out.println("Calling get method of Future to fetch result of factorial 10");
		long factorialof10 = new BigInteger(""+result10.get()).longValue(); 
		System.out.println("factorial of 10 is : " + factorialof10); 
		
		System.out.println("Calling get method of Future to get result of factorial 15"); 
		long factorialof15 = new BigInteger(""+result15.get()).longValue();
		System.out.println("factorial of 15 is : " + factorialof15);
		
		System.out.println("Calling get method of Future to get result of factorial 20"); 
		long factorialof20 = new BigInteger(""+result20.get()).longValue(); 
		System.out.println("factorial of 20 is : " + factorialof20);

	}

}

class FactorialCalculator implements Callable<BigInteger>{
	
	private BigInteger bint;
	
	FactorialCalculator(int number){
		BigInteger bintgr = new BigInteger(number+"");
		this.bint=bintgr;
	}
	
	@Override
	public BigInteger call() throws Exception {
		return factorialRecursion(bint);
	}
	
	private static BigInteger factorialRecursion(BigInteger bint){
		if(bint.equals(BigInteger.ONE))
			return BigInteger.ONE;
		else{
			return (bint.multiply(factorialRecursion(bint.subtract(BigInteger.ONE))));
		}				
	}
	
}
