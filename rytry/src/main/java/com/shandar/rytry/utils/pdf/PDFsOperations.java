package com.shandar.rytry.utils.pdf;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class PDFsOperations {
	
	private List<String> getPdfsList(String dirContaingPdfFile){
		System.out.println("Start : "+Thread.currentThread().getStackTrace()[1]);
		List<String> pdfList= new ArrayList<String>();
		String[] strList= null;
		try{
			File dir = new File(dirContaingPdfFile);
			
			if(dir.isDirectory()){
				strList = dir.list();
			}
			
			for (String string : strList) {
				pdfList.add(string);
			}
			System.out.println(pdfList);
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		System.out.println("End : "+Thread.currentThread().getStackTrace()[1]);
		return pdfList;
	}
	
	
	public static void main(String[] args) {		
		PDFsOperations mp = new PDFsOperations();
		System.out.println("Start Time : " + Calendar.getInstance().getTime());
		try{
			//PDFsUtils.splitPdfs("C:\\Data\\joinpdf\\Guide to the Java Ecosystem.pdf");
			//PDFsUtils.splitPdfs("C:\\Data\\joinpdf\\CIF_Scanned.pdf");
			//PDFsUtils.splitPdfs("D:\\pdf\\aadhar_passprt.pdf");
			//PDFsUtils.splitPdfs("D:\\Work_Ex\\Sears\\split\\Shantanu Sikdar_OL_3046.pdf","D:\\Work_Ex\\Sears\\split\\Shantanu Sikdar_OL_3046.pdf");
			//System.out.println("test");
			//PDFsUtils.splitPdfs("E:\\data\\ShantanuSikdar.pdf","E:\\data\\splitpdfs");
		}catch(Exception ioe){
			ioe.printStackTrace();
		}
		//PDFsUtils.mergePdfs(mp.getPdfsList("C:\\Data\\Misc\\typhoid\\ds"), "C:\\Data\\Misc\\typhoid\\ds");
		//PDFsUtils.mergePdfs(mp.getPdfsList("D:\\pdf\\aadhr"), "D:\\pdf\\aadhr");
		//PDFsUtils.mergePdfs(mp.getPdfsList("D:\\Interviews\\Barclays\\tosend"), "D:\\Interviews\\Barclays\\tosend");
		//PDFsUtils.mergePdfs(mp.getPdfsList("D:\\Work_Ex\\Sears\\split\\Shantanu Sikdar_OL_3046.pdf"), "D:\\Work_Ex\\Sears\\split");
		//PDFsUtils.mergePdfs(mp.getPdfsList("C:\\DATA\\misc_barclays\\investment_19-20"), "C:\\DATA\\misc_barclays\\investment_19-20");
		PDFsUtils.mergePdfs(mp.getPdfsList("E:\\data\\splitpdfs\\"), "E:\\data\\splitpdfs\\");
		System.out.println("Stop Time : " + Calendar.getInstance().getTime());
	}
	
}