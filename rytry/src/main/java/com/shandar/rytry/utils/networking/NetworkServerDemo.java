/**
* Project: rytry
* Package Name:package com.shandar.rytry.utils.networking;
* File Name: NetworkServerDemo.java
* Create Date: Dec 25, 2016
* Create Time: 10:43:24 AM
* Copyright: Copyright (c) 2016
* @author: Shantanu Sikdar, ssikdar
* @version 1.0
*/
package com.shandar.rytry.utils.networking;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

public class NetworkServerDemo {

	public static void main(String[] args) {
		javaServerSocketDemo();
	}
	
	private static void javaServerSocketDemo() {
		try {
			System.out.println("Starting server");
			ServerSocket serverSocket = new ServerSocket(8182);
			Socket clientSocket = serverSocket.accept();
			
			BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			String inputLine;
			while((inputLine=in.readLine()) != null){
				System.out.println(inputLine);
			}
			in.close();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}
}
