/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.utils.threading </p>
 * <p>File Name: EvenOddUsingThread.java</p>
 * <p>Create Date: Apr 17, 2015 </p>
 * <p>Create Time: 1:30:32 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.utils.threading;

/**
 * @author Shantanu Sikdar 
 *
 */
public class EvenOddUsingThread {

	public static void main(String[] args) {
		
		Thread tOdd = new Thread(new OddNumberThread());
		Thread tEven = new Thread(new EvenNumberThread());
		//tOdd.start();
		tEven.start();
	}
	
}

class OddNumberThread implements Runnable{

	@Override
	public void run() {
		for(int i=0;i<50;i++){
			if(!(i%2==0)){
				System.out.println("odd number = "+i);
			}
		}
	}
}
class EvenNumberThread implements Runnable{

	@Override
	public void run() {
		for(int i=0;i<50;i++){
			if(i%2==0){
				System.out.println("even number = "+i);
			}
		}
	}
}
