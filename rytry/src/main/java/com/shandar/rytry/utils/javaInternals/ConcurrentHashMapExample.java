/**
* Project: rytry
* Package Name:package com.shandar.rytry.utils.javaInternals;
* File Name: ConcurrentHashMapExample.java
* Create Date: Jan 6, 2017
* Create Time: 5:18:23 PM
* Copyright: Copyright (c) 2016
* @author: Shantanu Sikdar, ssikdar
* @version 1.0
*/
package com.shandar.rytry.utils.javaInternals;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;


public class ConcurrentHashMapExample {

	private static Map<String, Long> orders = new ConcurrentHashMap<String, Long>();
	
	static Map<String, Long> processOrders(){
		for (String city : orders.keySet()) {
			for (int i = 0; i < 50; i++) {
				Long oldOrder = orders.get(city);
				orders.put(city, oldOrder+1);
			}
		}
		return orders;
	}
	
	public static void main(String[] args) throws InterruptedException{
		orders.put("Mumbai", 0l);
		orders.put("Delhi", 0l);
		orders.put("Chennai", 0l);
		orders.put("Kolkata", 0l);
		
		ExecutorService exService = Executors.newFixedThreadPool(2);

//		not able to solve below 2 lines as they seem to compatible with java8 
//		
//		exService.submit(new ConcurrentHashMapExample().processOrders());
//		exService.submit(ConcurrentHashMapExample::processOrders);
		
		exService.awaitTermination(1, TimeUnit.SECONDS);
		exService.shutdown();
		System.out.println(orders);
		
	}

}

