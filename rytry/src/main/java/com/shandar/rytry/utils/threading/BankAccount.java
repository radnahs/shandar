/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.utils.threading </p>
 * <p>File Name: BankAccount.java</p>
 * <p>Create Date: Jul 13, 2015 </p>
 * <p>Create Time: 2:21:07 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.utils.threading;

/**
 * @author Shantanu Sikdar 
 *
 */
public class BankAccount {
	
	private int amnt = 10000;
	
	//public void withdraw(int wAmnt){
	public synchronized void withdraw(int wAmnt){
		if(this.amnt<wAmnt){
			System.out.println("Less balance; waiting for deposit...");
			try {
				wait();
			} catch (InterruptedException ie) {
				ie.printStackTrace();
			}
		}
		this.amnt-=wAmnt;  
		System.out.println("withdraw completed...");
	}
	
	//public void deposit(int dAmnt){
	public synchronized void deposit(int dAmnt){
		System.out.println("going to deposit...");  
		this.amnt+=dAmnt;  
		System.out.println("deposit completed... ");  
		notify();  
	}
	
	public static void main(String args[]){
		System.out.println("inside BA");
		final BankAccount ba=new BankAccount();  
		new Thread(){  
			public void run(){ba.withdraw(15000);}  
		}.start();  
		new Thread(){  
			public void run(){ba.deposit(10000);}  
		}.start();  
	}
	
}