/**
* Project: rytry
* Package Name:package com.shandar.rytry.utils.threading.concurrency;
* File Name: JoinExample.java
* Create Date: Dec 16, 2016
* Create Time: 7:05:27 PM
* Copyright: Copyright (c) 2016
* @author: Shantanu Sikdar, ssikdar
* @version 1.0
*/
package com.shandar.rytry.utils.threading.concurrency;

import java.util.Random;

public class JoinExample implements Runnable{
	
	private Random rndm = new Random(System.currentTimeMillis());

	@Override
	public void run() {
		//simulate some CPU expensive task
		for(int i=0; i<100000000; i++) {
			rndm.nextInt();
		}
		System.out.println("["+Thread.currentThread().getName()+"] finished.");
	}
	
	public static void main(String[] args) {
		Thread[] threads = new Thread[5];
		try{
			for(int i=0; i<threads.length; i++) {
				threads[i] = new Thread(new JoinExample(), "joinThread-"+i);
				threads[i].start();
			}
			for(int i=0; i<threads.length; i++) {
				threads[i].join();
			}
			System.out.println("["+Thread.currentThread().getName()+"] All threads have finished.");
				
		}catch(InterruptedException iex){
			iex.printStackTrace();
		}
		
	}

}
