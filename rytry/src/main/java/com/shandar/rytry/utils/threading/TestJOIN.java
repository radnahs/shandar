/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.utils.threading </p>
 * <p>File Name: TestJOIN.java</p>
 * <p>Create Date: Apr 24, 2015 </p>
 * <p>Create Time: 7:13:56 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.utils.threading;

/**
 * @author Shantanu Sikdar 
 *
 */
public class TestJOIN extends Thread{
	
	public void run(){
		System.out.println("Thread Name == "+Thread.currentThread().getName());
		for (int i = 1; i <= 5; i++) {			
			try {
				Thread.sleep(500);
				System.out.println("Thread State = "+ Thread.currentThread().getState());
			} catch (InterruptedException e) {
				System.out.println(e);
			}
			System.out.println(i);
		}		
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		TestJOIN tJ1 = new TestJOIN();
		TestJOIN tJ2 = new TestJOIN();
		TestJOIN tJ3 = new TestJOIN();
		tJ1.start();
		try{
			tJ1.join(1500);
		}catch(InterruptedException ie){
			ie.printStackTrace();
		}
		tJ2.start();
		tJ3.start();
	}

}
