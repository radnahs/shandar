/**
* Project: rytry
* Package Name:package com.shandar.rytry.utils.threading.concurrency;
* File Name: InterruptExample.java
* Create Date: Dec 2, 2016
* Create Time: 4:33:47 PM
* Copyright: Copyright (c) 2016
* @author: Shantanu Sikdar, ssikdar
* @version 1.0
*/
package com.shandar.rytry.utils.threading.concurrency;

public class InterruptExample implements Runnable {

	//int i=0;
	@Override
	public void run() {
		try {
			Thread.sleep(Long.MAX_VALUE);
		} catch (InterruptedException e) {
			System.out.println("["+Thread.currentThread().getName()+"]  - Interrupted by exception!");
		}
		while(!Thread.interrupted()) {
			// do nothing here
			//System.out.println(i++);
		}
		System.out.println("["+Thread.currentThread().getName()+"]  -  Interrupted for the second time.");
	}

	public static void main(String[] args) throws InterruptedException{
		Thread myThread = new Thread(new InterruptExample(), "myThread");
		myThread.start();
		System.out.println("["+Thread.currentThread().getName()+"] Sleeping  - in main thread for 5s...");
		Thread.sleep(50);
		System.out.println("["+Thread.currentThread().getName()+"]  - Interrupting myThread");
		myThread.interrupt();
		System.out.println("["+Thread.currentThread().getName()+"] Sleeping  - 	in main thread for 5s...");
		Thread.sleep(50);
		System.out.println("["+Thread.currentThread().getName()+"]  - Interrupting myThread");
		myThread.interrupt();
	}

}
