/**
* Project: rytry
* Package Name:package com.shandar.rytry.utils.threading.jenkov;
* File Name: CountDownLatchExample.java
* Create Date: Dec 20, 2016
* Create Time: 1:06:21 PM
* Copyright: Copyright (c) 2016
* @author: Shantanu Sikdar, ssikdar
* @version 1.0
*/
package com.shandar.rytry.utils.threading.jenkov;

import java.util.concurrent.CountDownLatch;

public class CountDownLatchExample {

	public static void main(String[] args) throws InterruptedException{
		CountDownLatch countDownLatch = new CountDownLatch(3);
		
		Waiter waiter = new Waiter(countDownLatch);
		Decrementer decrementer = new Decrementer(countDownLatch);
		
		new Thread(waiter).start();
		new Thread(decrementer).start();
		
		Thread.sleep(4000);
	}

}

class Waiter implements Runnable{

	CountDownLatch countDownLatch = null;
	
	public Waiter(CountDownLatch countDownLatch) {
		this.countDownLatch=countDownLatch;
	}
	
	@Override
	public void run() {
		try{
			System.out.println("await started .. ");
			countDownLatch.await();
			System.out.println("await released .. ");
		}catch(InterruptedException iex){
			iex.printStackTrace();
		}
		System.out.println("Waiter released .. ");
	}
	
}

class Decrementer implements Runnable{

	CountDownLatch countDownLatch = null;
	public Decrementer(CountDownLatch countDownLatch) {
		this.countDownLatch=countDownLatch;
	}
	
	@Override
	public void run() {
		try{
			Thread.sleep(1000);
			this.countDownLatch.countDown();
			
			Thread.sleep(1000);
			this.countDownLatch.countDown();
			
			Thread.sleep(1000);
			this.countDownLatch.countDown();
			
		}catch(InterruptedException iex){
			iex.printStackTrace();
		}
	}
	
}
