/**
* Project: rytry
* Package Name:package com.shandar.rytry.utils.threading.tutorial1;
* File Name: FirstThreads.java
* Create Date: Nov 25, 2016
* Create Time: 6:04:12 PM
* Copyright: Copyright (c) 2016
* @author: Shantanu Sikdar, ssikdar
* @version 1.0
*/
package com.shandar.rytry.utils.threading.tutorial1;

import java.util.Scanner;

public class FirstThreads {

	public static void main(String[] args) {
		Scanner scannerInput = new Scanner(System.in);
		
		Fortune fortuneThread = new Fortune();
		fortuneThread.start();
		
		System.out.println("Enter Anything");
		scannerInput.next();
		System.out.println("Imput request was satisfied in main");
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println("I slept for 2 seconds");
		
	}

}
