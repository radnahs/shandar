/**
* Project: rytry
* Package Name:package com.shandar.rytry.utils.threading.tutorial1;
* File Name: Fortune.java
* Create Date: Nov 25, 2016
* Create Time: 6:02:41 PM
* Copyright: Copyright (c) 2016
* @author: Shantanu Sikdar, ssikdar
* @version 1.0
*/
package com.shandar.rytry.utils.threading.tutorial1;

public class Fortune extends Thread{
	public void run(){
		while (true) {
			System.out.println("Good things will come your way");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
}
