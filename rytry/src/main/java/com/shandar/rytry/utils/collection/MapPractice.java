/**
* Project: rytry
* Package Name:package com.shandar.rytry.utils.collection;
* File Name: MapPractice.java
* Create Date: Dec 20, 2016
* Create Time: 7:51:47 AM
* Copyright: Copyright (c) 2016
* @author: Shantanu Sikdar, ssikdar
* @version 1.0
*/
package com.shandar.rytry.utils.collection;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.IdentityHashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.WeakHashMap;

public class MapPractice {

	public static void main(String[] args) {
		Map<String, Object> hashtable =  new Hashtable();
		
		Map<String, Object> hashMap =  new HashMap();
		
		Map<String, Object> linkedHashMap =  new LinkedHashMap();
		
		Map<String, Object> treeMap =  new TreeMap();
		
		Map<String, Object> identityHashMap =  new IdentityHashMap();
		
		//Map<Object, Object> enumMap =  new EnumMap<Object extends Enum<Object>,Object>();
		
		Map<String, Object> weakHashMap =  new WeakHashMap();
	}

}
