/**
* Project: rytry
* Package Name:package com.shandar.rytry.utils.threading.concurrency;
* File Name: MyRunnable.java
* Create Date: Sep 22, 2016
* Create Time: 6:59:41 PM
* Copyright: Copyright (c) 2016
* @author: Shantanu Sikdar, ssikdar
* @version 1.0
*/
package com.shandar.rytry.utils.threading.concurrency;

public class MyRunnable implements Runnable{
	
	@Override
	public void run() {
		System.out.println("Executing thread "+Thread.currentThread().getName());
	}

	public static void main(String[] args) {
		Thread myThread = new Thread(new MyRunnable(), "myRunnable");
		myThread.start();
	}

}
