/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.utils.trial </p>
 * <p>File Name: BitOperations.java</p>
 * <p>Create Date: Aug 19, 2014 </p>
 * <p>Create Time: 6:13:03 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.utils.trial;

/**
 * @author Shantanu Sikdar 
 *
 */
public class BitOperations {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		/*System.out.println("bit operations");
		String i = Integer.toBinaryString(46);
		System.out.println(i);
		String j = Integer.toBinaryString(2 << 11);
		System.out.println(j);
		*/
		int a = 5; int b = 10;
		System.out.println(3 ^ 9);
		System.out.println(3 ^ 10);
		System.out.println(9 ^ 10);
		System.out.println(10 ^ 9);
		a = a ^ b;
		System.out.println(a);
		b = a ^ b;
		System.out.println(b);
		a = a ^ b;
		System.out.println(a);

		
		
		/*System.out.println( "Binary of 2 == "  +Integer.toBinaryString(2) + "||  2 << 11 == "  + Integer.toBinaryString(2 << 11));
		System.out.println( "Binary of 2 == "  +Integer.toBinaryString(2) + "||  2 >> 11 == "  + Integer.toBinaryString(2 >> 11));
		System.out.println( "Binary of 2 == "  +Integer.toBinaryString(2) + "||  11 >> 2 == "  + Integer.toBinaryString(11 >> 2));
		System.out.println( "Binary of 2 == "  +Integer.toBinaryString(2) + "||  2 << 22 == "  + Integer.toBinaryString(2 << 22));
		System.out.println( "Binary of 2 == "  +Integer.toBinaryString(2) + "||  2 << 33 == "  + Integer.toBinaryString(2 << 33));
		System.out.println(Integer.toBinaryString(2 << 44));
		System.out.println(Integer.toBinaryString(2 << 55));*/
	}

}
