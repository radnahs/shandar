package com.shandar.rytry.news;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class TOIMain {

	// http://localhost:8080/RESTfulExample/json/product/get
	public static void main(String[] args) {

		try {

			//URL url = new URL("http://localhost:8080/RESTfulExample/json/product/get");
			URL url = new URL("https://dev132-toi-times-of-india-v1.p.mashape.com/news?token=YUhSMGNEb3ZMM1JwYldWemIyWnBibVJwWVM1cGJtUnBZWFJwYldWekxtTnZiUzltWldWa2N5OXVaWGR6Wm1WbFpDOHRNakV5T0Rrek5qZ3pOUzVqYlhNL1ptVmxaSFI1Y0dVOWMycHpiMjQ9NTc1ZDZlMzE0MmViMA");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

			String output;
			System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
				System.out.println(output);
			}
			conn.disconnect();

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	
	{
		//HttpResponse<JsonNode> response = Unirest.get("https://dev132-toi-times-of-india-v1.p.mashape.com/news?token=YUhSMGNEb3ZMM1JwYldWemIyWnBibVJwWVM1cGJtUnBZWFJwYldWekxtTnZiUzltWldWa2N5OXVaWGR6Wm1WbFpDOHRNakV5T0Rrek5qZ3pOUzVqYlhNL1ptVmxaSFI1Y0dVOWMycHpiMjQ9NTc1ZDZlMzE0MmViMA%3D%3D")
			//	.header("X-Mashape-Key", "l28dvtNBSimshdpMo4BT95YayatBp1RiwdBjsnPWoMEZsFKWAr")
			//	.header("Accept", "application/json")
			//	.asJson();
	}
}
