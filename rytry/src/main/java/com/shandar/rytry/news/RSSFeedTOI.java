package com.shandar.rytry.news;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Iterator;
import java.util.List;
import java.util.zip.GZIPInputStream;

import org.xml.sax.InputSource;

import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.FeedException;
import com.sun.syndication.io.SyndFeedInput;
import com.sun.syndication.io.XmlReader;

public class RSSFeedTOI {

	public SyndFeed getSyndFeedForUrl(String url)
			throws MalformedURLException, IOException, IllegalArgumentException, FeedException {
		SyndFeed feed = null;
		InputStream is = null;
		try {
			URLConnection openConnection = new URL(url).openConnection();
			is = new URL(url).openConnection().getInputStream();
			if ("gzip".equals(openConnection.getContentEncoding())) {
				is = new GZIPInputStream(is);
			}
			InputSource source = new InputSource(is);
			SyndFeedInput input = new SyndFeedInput();
			feed = input.build(source);
		} catch (Exception e) {
			System.out.println("Exception occured when building the feed object out of the url" + e);
			e.printStackTrace();
		} finally {
			if (is != null)
				is.close();
		}
		return feed;
	}

	public static void main1(String[] args) throws Exception {
		RSSFeedTOI rssFeedTOI = new RSSFeedTOI();
		SyndFeed feed = rssFeedTOI.getSyndFeedForUrl("http://news.yahoo.com/rss/sports");
		System.out.println(feed);
	}

	public static void main2(String[] args) throws Exception { 
		// URL url = new URL("https://viralpatel.net/blogs/feed");

		URL url = new URL("http://news.yahoo.com/rss/sports");
		XmlReader reader = null;
		try {
			reader = new XmlReader(url);
			SyndFeed feed = new SyndFeedInput().build(reader);
			System.out.println("Feed Title: " + feed.getAuthor());
			for (Iterator i = feed.getEntries().iterator(); i.hasNext();) {
				SyndEntry entry = (SyndEntry) i.next();
				System.out.println(entry.getTitle());
			}
		} finally {
			if (reader != null)
				reader.close();
		}
	}

	public static void main(String[] args) {
		//String printFeeds = getRSSfeeds("https://www.yahoo.com/news/rss");
		//String printFeeds = getRSSfeeds("https://www.yahoo.com/news/rss/sports");
		//String printFeeds = getRSSfeeds("http://feeds.feedburner.com/viralpatelnet");
		String printFeeds = getRSSfeeds("https://timesofindia.indiatimes.com/rssfeedstopstories.cms");
		System.out.println(printFeeds);
	}

	@SuppressWarnings({ "rawtypes", "finally", "unused" })
	protected static String getRSSfeeds(String feedUrl) {
		StringBuffer sb = new StringBuffer();
		try {
			URL url = new URL(feedUrl);
			SyndFeedInput input = new SyndFeedInput();
			SyndFeed feed = input.build(new XmlReader(url));
			List list = feed.getEntries();
			if (list.size() > 0) {
				for (int j = 0; j < list.size(); j++) {
					String container = "<p class=\"RSSlist\">";
					sb.append(container);
					String title = ((SyndEntry) list.get(j)).getTitle();
					sb.append(title + "\n");
					String link = ((SyndEntry) list.get(j)).getLink();
					String readmore = " Read more ";
					sb.append(readmore);
					String endContainer = "";
					sb.append(endContainer);
				}
			}
		}

		catch (Exception e) {
			e.printStackTrace();
			System.err.print("Exception");
		}

		finally {
			return sb.toString();
		}

	}
}
