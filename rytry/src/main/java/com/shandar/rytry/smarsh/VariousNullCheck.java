/**
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.smarsh </p>
 * <p>File Name: SmarshMS.java </p>
 * <p>Create Date: 30-Jun-2021 </p>
 * <p>Create Time: 6:41:46 PM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.rytry.smarsh;

import java.util.Objects;

/**
 * @author : Shantanu Sikdar
 * @Description : SmarshMS
 */
public class VariousNullCheck {

	public static void main(String[] args) {
		System.out.println("Hello Smarsh");
			
		CustomObj customObjNotNull = new CustomObj();
		customObjNotNull.setId(10);
		
		CustomObj customObjNull = null;
		
		VariousNullCheck vnc = new VariousNullCheck();
		vnc.nullCheck(customObjNull);
		vnc.nullCheck(customObjNotNull);
		
		String ss = "ste";
		vnc.nullCheck(ss);
		
	}
	
	private void nullCheck(Object anyTypeOfObj){
		
		if(Objects.isNull(anyTypeOfObj)) {
			System.out.println("null");
		}
		if(anyTypeOfObj==null) {
			System.out.println("==null");
		}else {
			System.out.println(anyTypeOfObj.getClass());
			System.out.println("not null");
		}
	}
	
	

}


class CustomObj{
	private int id;
	private int value;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	
}