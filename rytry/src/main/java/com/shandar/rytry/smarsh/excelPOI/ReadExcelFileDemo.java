/**
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.smarsh.excelPOI </p>
 * <p>File Name: ReadExcelFileDemo.java </p>
 * <p>Create Date: 02-Nov-2021 </p>
 * <p>Create Time: 3:27:01 PM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.rytry.smarsh.excelPOI;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * @author : Shantanu Sikdar
 * @Description : ReadExcelFileDemo
 */
public class ReadExcelFileDemo {

	public static void main(String[] args) throws IOException {
		// obtaining input bytes from a file
		// FileInputStream fis = new FileInputStream(new
		// File("C:\\data\\student.xlsx"));
		FileInputStream fis = new FileInputStream(new File("C:\\data\\smarsh\\additionalContentType.xlsx"));
		// creating workbook instance that refers to .xls file
		// HSSFWorkbook wb = new HSSFWorkbook(fis);
		XSSFWorkbook wb = new XSSFWorkbook(fis);
		// creating a Sheet object to retrieve the object
		// HSSFSheet sheet = wb.getSheetAt(0);
		XSSFSheet sheet = wb.getSheetAt(0);
		// evaluating cell type
		FormulaEvaluator formulaEvaluator = wb.getCreationHelper().createFormulaEvaluator();
		int colContentType = 1;
		int colAPI = 2;
		int colCreated = 3;
		int colEdited = 4;
		int colAttachment = 5;
		int colFile = 6;
		int colAutomated = 7;
		int cellCount = 0;
		String commonFolder = "C:\\data\\smarsh\\jsons\\";
		for (Row row : sheet) // iteration over row using for each loop
		{
			String dirName = commonFolder;
			for (Cell cell : row) // iteration over cell using for each loop
			{
				switch (formulaEvaluator.evaluateInCell(cell).getCellType()) {
				case Cell.CELL_TYPE_NUMERIC: // field that represents numeric cell type
					// getting the value of the cell as a number
					System.out.print(cell.getNumericCellValue() + "\t\t");
					cellCount++;
					break;
				case Cell.CELL_TYPE_STRING: // field that represents string cell type
					// getting the value of the cell as a string
					System.out.print(cell.getStringCellValue() + "\t\t");
					if (cellCount % 8 == colContentType) {
						dirName = createDirectory(commonFolder, cell.getStringCellValue().trim() )+"\\";
					}
					if (cellCount % 8 == colCreated) {
						createJsonFile(dirName, "post-item-text.json", cell.getStringCellValue());
					}
					if (cellCount % 8 == colEdited) {
						createJsonFile(dirName, "edit-itme-file.json", cell.getStringCellValue());
					}
					if (cellCount % 8 == colAttachment) {
						createJsonFile(dirName, "item-file-attachments-intermediate.json", cell.getStringCellValue());
					}
					if (cellCount % 8 == colFile) {
						createJsonFile(dirName, "attachment.json", cell.getStringCellValue());
					}

					cellCount++;
					break;
				case Cell.CELL_TYPE_BLANK:
					cellCount++;
					break;
				}
			}

			System.out.println();
		}
	}

	private static String createDirectory(String commonFolder, String dirName) {
		if (new File(commonFolder + dirName).mkdirs()) {
			return commonFolder + dirName;
		}
		return commonFolder;
	}

	private static void createJsonFile(String dirName, String fileName, String content) {
		try {
			FileWriter fw = new FileWriter(dirName + fileName);
			fw.write(content);
			fw.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		System.out.println("Success...");
	}

}
