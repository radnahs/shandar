/**
 *
 * <p>Project: rytry </p>
 * <p>Package Name: com.shandar.rytry.login.utility </p>
 * <p>File Name: LoginUtils.java</p>
 * <p>Create Date: Mar 27, 2013 </p>
 * <p>Create Time: 11:54:28 AM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.login.utility;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.misc.*;

/**
 * @author Shantanu Sikdar 
 *
 */
public class LoginUtils {
	
	private static final Logger logger = LoggerFactory.getLogger(LoginUtils.class);
	
	@SuppressWarnings("restriction")
	public static String encryptPassword(String password){
		String encryptedPwd=null;
		StringBuilder strBldr = new StringBuilder(password);
		try{
			encryptedPwd= strBldr.reverse().toString();
			encryptedPwd=new BASE64Encoder().encode(encryptedPwd.getBytes());
			encryptedPwd=new BASE64Encoder().encode(encryptedPwd.getBytes());
		}catch(Exception ex){
			logger.error("",ex);			
		}		
		return encryptedPwd;
	}
		
	public static void main(String[] args) {
		LoginUtils lu = new LoginUtils();		
		System.out.println(lu.encryptPassword("123123"));//TXpJeE16SXg=	
	}	
}
