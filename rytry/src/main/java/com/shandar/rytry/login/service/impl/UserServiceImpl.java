package com.shandar.rytry.login.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.shandar.rytry.login.dao.UserDAO;
import com.shandar.rytry.login.dao.impl.UserDAOImpl;
import com.shandar.rytry.login.model.User;
import com.shandar.rytry.login.service.UserService;

public class UserServiceImpl implements UserService {
	
	private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

	@Override
	public void findAllUsers() {
		UserDAO userDAO = new UserDAOImpl();
		userDAO.fetchAllUsers();		
	}
	

	@Override
	public boolean isAuthenitcUser(String username, String password) {
		UserDAO userDAO = null;
		List<User> listUser= null;		
		try{
			userDAO = new UserDAOImpl();
			listUser= userDAO.authenitcUserDetails(username, password);			
		}catch(Exception ex){
			logger.error(ex.getMessage());
			ex.printStackTrace();
		}
		return listUser.size()>0 && listUser.size()<2;
	}
	
}
