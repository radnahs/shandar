/**
 *
 * <p>Project: rytry </p>
 * <p>Package Name: com.shandar.rytry.login.dao </p>
 * <p>File Name: UserDAO.java</p>
 * <p>Create Date: Mar 6, 2013 </p>
 * <p>Create Time: 3:06:58 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.login.dao;

import java.util.ArrayList;
import java.util.List;

import com.shandar.rytry.login.model.User;

/**
 * @author Shantanu Sikdar 
 *
 */
public interface UserDAO {
	
	public List<User> authenitcUserDetails(String userName,String userPassword);
	
	public User findByUserName(String[] userNames);
	
	public ArrayList<User> findByUserIds(String[] userIds);
	
	public void createUser(User user);
	
	public List<User> fetchAllUsers();
	
}
