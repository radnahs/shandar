/**
 *
 * <p>Project: rytry </p>
 * <p>Package Name: com.shandar.rytry.login.servive </p>
 * <p>File Name: LoginService.java</p>
 * <p>Create Date: Mar 25, 2013 </p>
 * <p>Create Time: 6:47:11 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.login.service;

/**
 * @author Shantanu Sikdar 
 *
 */
public interface LoginService {
	
	public boolean userAuthentication(String userName, String userPassword);
}
