package com.shandar.rytry.login.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.shandar.rytry.common.properties.rytryProperties;
import com.shandar.rytry.db.ConnectionFactory;
import com.shandar.rytry.db.DBConnection;
import com.shandar.rytry.db.DBQueries;
import com.shandar.rytry.db.impl.DBQueriesImpl;
import com.shandar.rytry.db.impl.QueryResultSetMapperImpl;
import com.shandar.rytry.login.dao.UserDAO;
import com.shandar.rytry.login.model.User;

public class UserDAOImpl implements UserDAO{
	
	private static final Logger logger = LoggerFactory.getLogger(UserDAOImpl.class);
	
	DBQueries dbQuery = new DBQueriesImpl("userDAO-queries.xml");
	DBConnection dbConn = ConnectionFactory.getInstance().getConnectionMySQL();
	
	@Override
	public List<User> authenitcUserDetails(String userName, String userPassword) {
		Map<String, String> queryMap = null;
		String query ;
		List<Map<String, Object>> mappedUserList = null;
		List<User> listUser = null;
		try{			
			System.out.println("2 inside === "+rytryProperties.installationPath);			
			listUser = new ArrayList<User>();
			queryMap = dbQuery.getQueryBeanFromFactory("VALIDATE_USER_CREDENTIAL");
			query =  dbQuery.getQuery(queryMap);						
			mappedUserList = dbConn.dbQueryRead(query, new Object[]{userName,userPassword});			
			if(mappedUserList.size()>0){
				listUser =  new QueryResultSetMapperImpl<User>().mapRersultSetToObject(mappedUserList, User.class);
			}
		}catch(Exception  ex){
			logger.error("fetchAllUser Error:: ", ex);
		}		
		return listUser;
	}

	@Override
	public User findByUserName(String[] userNames) {
		
		return null;
	}

	@Override
	public ArrayList<User> findByUserIds(String[] userIds) {
		
		return null;
	}

	@Override
	public void createUser(User user) {
		
		
	}
	
	@Override
	public List<User> fetchAllUsers(){
		Map<String, String> queryMap = null;
		String query ;
		List<Map<String, Object>> mappedUserList = null;
		List<User> listUser = null;
		try{
			queryMap = dbQuery.getQueryBeanFromFactory("FIND_ALL_USER");
			query =  dbQuery.getQuery(queryMap);
			mappedUserList = dbConn.dbQueryRead(query);	
			listUser=  new QueryResultSetMapperImpl<User>().mapRersultSetToObject(mappedUserList, User.class);			
		}catch(Exception ex){
			logger.error("fetchAllUser Error:: ", ex);
		}
		return listUser;
	}	
}
