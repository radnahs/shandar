/**
 *
 * <p>Project: rytry </p>
 * <p>Package Name: com.shandar.rytry.login.controller </p>
 * <p>File Name: LoginController.java</p>
 * <p>Create Date: Mar 25, 2013 </p>
 * <p>Create Time: 1:28:15 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.login.controller;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.shandar.rytry.login.model.User;
import com.shandar.rytry.login.service.LoginService;
import com.shandar.rytry.login.service.impl.LoginServiceImpl;

/**
 * @author Shantanu Sikdar 
 *
 */

@Controller
public class LoginController {
	
	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);
		
	@RequestMapping( value="Login", method = RequestMethod.POST)
    public String login(User user, BindingResult result, Map model, HttpSession session) {	
            String userName = user.getUserName();
            String userPassword = user.getUserPassword();            
            boolean skipDBAuth = user.isSkipDBAuth();    
            
            if(!skipDBAuth){
            	session.setAttribute("userName", userName);
                
                if (result.hasErrors()) {
                        return "home";
                }
                user = (User) model.get("user");
                LoginService loginService = new LoginServiceImpl();            
                boolean isValidUser = loginService.userAuthentication(userName, userPassword);            
                if (!isValidUser) {            	
                	return "home";
                }
            }
            model.put("user", user);            
            return "dashboard/dashboardMain";
    }
	
	@RequestMapping( value="Logout", method = RequestMethod.GET)
    public String logout(HttpSession session) {
		logger.debug("Logout == "+session.getAttribute("userName"));
		session.invalidate();		
		return "";
	}
	
}
