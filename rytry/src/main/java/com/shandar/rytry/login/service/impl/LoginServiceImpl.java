package com.shandar.rytry.login.service.impl;

import com.shandar.rytry.login.service.LoginService;
import com.shandar.rytry.login.service.UserService;
import com.shandar.rytry.login.utility.LoginUtils;

public class LoginServiceImpl implements LoginService {
	
	UserService userService;

	@Override
	public boolean userAuthentication(String userName, String userPassword) {
		userService = new UserServiceImpl();
		String password = LoginUtils.encryptPassword(userPassword);
		return userService.isAuthenitcUser(userName,password);
	}	
}
