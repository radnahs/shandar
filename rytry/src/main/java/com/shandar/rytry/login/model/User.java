/**
 *
 * <p>Project: rytry </p>
 * <p>Package Name: com.shandar.rytry.login.model </p>
 * <p>File Name: User.java</p>
 * <p>Create Date: Mar 6, 2013 </p>
 * <p>Create Time: 2:55:49 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.login.model;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class User {
	
	@Column(name="user_id")
	private long userId;
	
	@Column(name="user_name")
	private String userName;
	
	@Column(name="user_password")
	private String userPassword;
		
	private boolean skipDBAuth;
	
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserPassword() {
		return userPassword;
	}
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}
	
	public boolean isSkipDBAuth() {
		return skipDBAuth;
	}
	public void setSkipDBAuth(boolean skipDBAuth) {
		this.skipDBAuth = skipDBAuth;
	}
	
	

	
}
