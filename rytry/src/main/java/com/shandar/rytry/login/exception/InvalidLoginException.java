/**
 *
 * <p>Project: trunk_rytry </p>
 * <p>Package Name: com.shandar.rytry.login.exception </p>
 * <p>File Name: InvalidLoginException.java</p>
 * <p>Create Date: Oct 31, 2013 </p>
 * <p>Create Time: 6:26:58 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.login.exception;

/**
 * @author Shantanu Sikdar 
 *
 */
public class InvalidLoginException extends Exception{

	public InvalidLoginException() {
			
	}
	
	public InvalidLoginException(String message) {
		super(message);
		
	}
	
	

}
