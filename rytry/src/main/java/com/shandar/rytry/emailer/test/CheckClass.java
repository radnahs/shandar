package com.shandar.rytry.emailer.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CheckClass {
	private TestClass testClass;
	
	public CheckClass() {
		System.out.println("in CheckClass constructor");
	}

	@Autowired
	public void setTestClass(TestClass testClass) {
		System.out.println("Setter Injection");
		this.testClass = testClass;
	}
}
