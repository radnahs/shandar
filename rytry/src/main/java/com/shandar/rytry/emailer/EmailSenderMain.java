package com.shandar.rytry.emailer;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.ui.velocity.VelocityEngineUtils;

public class EmailSenderMain {
	private static final Logger logger = LoggerFactory.getLogger(EmailSenderMain.class);
	VelocityEngine velocityEngine;
	public static void main(String[] args) {
		new EmailSenderMain().formattedMailMethod();
	}
	StringWriter writer = new StringWriter();
	public void formattedMailMethod() {
		
		try{			
			ApplicationContext ctx= new ClassPathXmlApplicationContext("mail-velocity-context.xml");
			velocityEngine = (VelocityEngine)ctx.getBean("velocityEngine");
			JavaMailSender mls = (JavaMailSender)ctx.getBean("mailSender");
			
			VelocityContext context = new VelocityContext();
			context.put("colHead1", "Col Head Id " );
			context.put("colHead2", "Col Head Value " );
			context.put("prdList1", foreachData(0,2));
			context.put("prdList2", foreachData(11,21));
			Properties p = new Properties();
			p.setProperty("file.resource.loader.path", "C:/Data/Projects/rytry/src/main/resources/templates");
			
			Velocity.init(p);
			Template t = Velocity.getTemplate("reminderForeach.vm");			
			t.merge(context, writer);
			System.out.println("body ====== "+ writer.toString());
			
			MimeMessagePreparator preparator = new MimeMessagePreparator() {
				
				public void prepare(MimeMessage mimeMessage) throws Exception {
					
					MimeMessageHelper message = new MimeMessageHelper(mimeMessage,true);
					message.setTo("shsikdar@inautix.co.in");
					message.setFrom(new InternetAddress("shantanu.sikdar@bnymellon.com"));
					message.setSubject("hope this works" + Calendar.getInstance().getTime());

					//String body = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "templates/reminder.vm", "UTF-8", null);//normal html mail
					//String body = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "templates/wbhmStatus.vm", "UTF-8", null);//html mail with data in .vm 
					String body = writer.toString();
					
					message.setText(body, true);
					
					/*if (!StringUtils.isEmpty(attachmentPath)) {
						FileSystemResource file = new FileSystemResource(attachmentPath);
						message.addAttachment(attachmentName, file);
					}*/
				}
			};
			
	        mls.send(preparator);
	        
			System.out.println("mls.sent message");
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	public static void simpleMailMethod(String[] args) {
		try{			
			ApplicationContext ctx= new ClassPathXmlApplicationContext("mail-velocity-context.xml");
			VelocityEngine velocityEngine = (VelocityEngine)ctx.getBean("velocityEngine");
			String body = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "templates/reminder.vm", "UTF-8", null);
			System.out.println(body);
			MailSender mls = (MailSender)ctx.getBean("mailSender");
			SimpleMailMessage message = new SimpleMailMessage();
			message.setTo("shsikdar@inautix.co.in");
			message.setSubject("User Registration successful subject" + Calendar.getInstance().getTime());
	        message.setText(body + "The user is successfully registered "+Calendar.getInstance().getTime());
	        System.out.println("mls.send message");
	        mls.send(message);
			System.out.println("mls.sent message");
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	public List<Map<String, String>> foreachData(int init, int max){
		List<Map<String, String>> lis1 = null;
		try{
			lis1 =  new ArrayList<Map<String, String>>();			
			for(int i =init ; i<max; i++){
				Map<String, String> ma = new HashMap<String, String>();
				ma.put("Pid", "shan - "+i+"- dar");
				ma.put("cost", i+"000000");
				lis1.add(ma);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}
		System.out.println(lis1);
		return lis1;		
	}	

}
