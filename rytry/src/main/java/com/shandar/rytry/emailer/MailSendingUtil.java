package com.shandar.rytry.emailer;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;

import com.shandar.rytry.login.service.impl.UserServiceImpl;

public class MailSendingUtil {
	private static final Logger logger = LoggerFactory.getLogger(MailSendingUtil.class);
	
	@Autowired
	private MailSender mailSender;
	//private Set userEmailIds;
	
	public void send(Set<String> userEmailIds){
		SimpleMailMessage[] mailMessageArray = new SimpleMailMessage[userEmailIds.size()];
        Iterator<String> iterator = userEmailIds.iterator();
        System.out.println("Sending email 1 ....");
        for (int index = 0; iterator.hasNext(); index ++){
        	System.out.println("Sending email ...." + index);
            SimpleMailMessage message = new SimpleMailMessage();
 
            String toAddress = (String)iterator.next();
            message.setTo(toAddress);
            message.setSubject("User Registration successful");
            message.setText("The user '" + toAddress + "' is successfully registered");
            mailMessageArray[index] = message;
        }
        System.out.println("Sending email ....");
        mailSender.send(mailMessageArray);
	}
	
	
	public static void main(String[] args) {		
		ApplicationContext context = new ClassPathXmlApplicationContext("mail-velocity-context.xml");
		Set<String> userEmailIds = new HashSet<String>();
		userEmailIds.add("shsikdar@inautix.co.in");
		userEmailIds.add("pankumar@inautix.co.in");
		
		new MailSendingUtil().send(userEmailIds);
	}

}
