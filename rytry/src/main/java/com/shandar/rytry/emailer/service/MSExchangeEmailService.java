/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.emailer.service </p>
 * <p>File Name: MSExchangeEmailService.java</p>
 * <p>Create Date: Nov 3, 2014 </p>
 * <p>Create Time: 3:41:50 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.emailer.service;

import java.net.URI;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import microsoft.exchange.webservices.data.Appointment;
import microsoft.exchange.webservices.data.AppointmentSchema;
import microsoft.exchange.webservices.data.CalendarFolder;
import microsoft.exchange.webservices.data.CalendarView;
import microsoft.exchange.webservices.data.EmailMessage;
import microsoft.exchange.webservices.data.ExchangeCredentials;
import microsoft.exchange.webservices.data.ExchangeService;
import microsoft.exchange.webservices.data.ExchangeVersion;
import microsoft.exchange.webservices.data.FindItemsResults;
import microsoft.exchange.webservices.data.Folder;
import microsoft.exchange.webservices.data.Item;
import microsoft.exchange.webservices.data.ItemId;
import microsoft.exchange.webservices.data.ItemView;
import microsoft.exchange.webservices.data.PropertySet;
import microsoft.exchange.webservices.data.ServiceLocalException;
import microsoft.exchange.webservices.data.WebCredentials;
import microsoft.exchange.webservices.data.WellKnownFolderName;

/**
 * @author Shantanu Sikdar 
 *
 */

public class MSExchangeEmailService {
			
	private static ExchangeService service;
	private static Integer NUMBER_EMAILS_FETCH =5; // only latest 5 emails/appointments are fetched.
			
	/**
	 * Firstly check, whether "https://webmail.xxxx.com/ews/Services.wsdl" and "https://webmail.xxxx.com/ews/Exchange.asmx"
	 * is accessible, if yes that means the Exchange Webservice is enabled on your MS Exchange.
	 */
	static{
		try{						
			service = new ExchangeService(ExchangeVersion.Exchange2010_SP1);
			//service = new ExchangeService(ExchangeVersion.Exchange2007_SP1); //depending on the version of your Exchange. 
			service.setUrl(new URI("https://webmail.bnymellon.com/ews/Exchange.asmx"));		
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Initialize the Exchange Credentials. 
	 * 	Don't forget to replace the "USERNAME","PASSWORD","DOMAIN_NAME" variables. 
	 */
	public MSExchangeEmailService() {
		//ExchangeCredentials credentials = new WebCredentials("USERNAME","PASSWORD","DOMAIN_NAME");
		ExchangeCredentials credentials = new WebCredentials("xbbkngi","24@Seven","pac");
		service.setCredentials(credentials);
	}
	
	/**
	 * Reading one email at a time. Using Item ID of the email.
	 * Creating a message data map as a return value.   
	 */
	public Map<String, Object> readEmailItem(ItemId itemId){
		Map<String, Object> messageData = new HashMap<String, Object>();
		try{				
			Item itm = Item.bind(service, itemId, PropertySet.FirstClassProperties);
			EmailMessage emailMessage = EmailMessage.bind(service, itm.getId());			
			messageData.put("emailItemId", emailMessage.getId().toString());			
			messageData.put("subject", emailMessage.getSubject().toString());
			messageData.put("fromAddress",emailMessage.getFrom().getAddress().toString());
			messageData.put("senderName",emailMessage.getSender().getName().toString());
			Date dateTimeCreated = emailMessage.getDateTimeCreated();
			messageData.put("SendDate",dateTimeCreated.toString());			
			Date dateTimeRecieved = emailMessage.getDateTimeReceived();
			messageData.put("RecievedDate",dateTimeRecieved.toString());			
			messageData.put("Size",emailMessage.getSize()+"");						
			messageData.put("emailBody",emailMessage.getBody().toString());			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return messageData;
	}
	
	/**
	 * Number of email we want to read is defined as NUMBER_EMAILS_FETCH, 
	 */
	public List<Map<String, Object>> readEmails(){
		List<Map<String, Object>> msgDataList = new ArrayList<Map<String, Object>>();
		try{
			Folder folder = Folder.bind( service, WellKnownFolderName.Inbox );
			FindItemsResults<Item> results = service.findItems(folder.getId(), new ItemView(NUMBER_EMAILS_FETCH));		
			int i =1;
			for (Item item : results){				
				Map<String, Object> messageData = new HashMap<String, Object>();
				messageData = readEmailItem(item.getId());
				System.out.println("\nEmails #" + (i++ ) + ":" );
				System.out.println("subject : " + messageData.get("subject").toString());
				System.out.println("Sender : " + messageData.get("senderName").toString());
				msgDataList.add(messageData);
			}			
		}catch (Exception e) { 	e.printStackTrace();		}
		return msgDataList;
	}

	/**
	 * Reading one appointment at a time. Using Appointment ID of the email.
	 * Creating a message data map as a return value.   
	 */
	public Map<String, Object> readAppointment(Appointment appointment){
		Map<String, Object> appointmentData = new HashMap<String, Object>();
		try {			
			appointmentData.put("appointmentItemId", appointment.getId().toString());
			appointmentData.put("appointmentSubject", appointment.getSubject());
			appointmentData.put("appointmentStartTime", appointment.getStart()+"");
			appointmentData.put("appointmentEndTime", appointment.getEnd()+"");
			//appointmentData.put("appointmentBody", appointment.getBody().toString());
		} catch (ServiceLocalException e) {
			e.printStackTrace();
		}		
		return appointmentData;
	}

	/**
 	 *	Number of Appointments we want to read is defined as NUMBER_EMAILS_FETCH,
 	 *  Here I also considered the start data and end date which is a 30 day span.
 	 *  We need to set the CalendarView property depending upon the need of ours.   
	 */
	public List<Map<String, Object>> readAppointments(){		
		List<Map<String, Object>> apntmtDataList = new ArrayList<Map<String, Object>>();		
		Calendar now = Calendar.getInstance();
		Date startDate = Calendar.getInstance().getTime();
		now.add(Calendar.DATE, 30);
        Date endDate = now.getTime();  
		try{
			CalendarFolder calendarFolder = CalendarFolder.bind(service, WellKnownFolderName.Calendar, new PropertySet());			
			CalendarView cView = new CalendarView(startDate, endDate, 5);			
			cView.setPropertySet(new PropertySet(AppointmentSchema.Subject, AppointmentSchema.Start, AppointmentSchema.End));	// we can set other properties as well depending upon our need.		
			FindItemsResults<Appointment> appointments = calendarFolder.findAppointments(cView);			
			
			int i =1;
			List<Appointment> appList = appointments.getItems();			
			for (Appointment appointment : appList) {
				System.out.println("\nAPPOINTMENT #" + (i++ ) + ":" );
				Map<String, Object> appointmentData = new HashMap<String, Object>();
				appointmentData = readAppointment(appointment);
				System.out.println("subject : " + appointmentData.get("appointmentSubject").toString());
				System.out.println("On : " + appointmentData.get("appointmentStartTime").toString());
				apntmtDataList.add(appointmentData);
			}			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return apntmtDataList;
	}	
	
	public static void main(String[] args) {		
		MSExchangeEmailService msees = new MSExchangeEmailService();
		msees.readEmails();
		msees.readAppointments();		
	}

}
