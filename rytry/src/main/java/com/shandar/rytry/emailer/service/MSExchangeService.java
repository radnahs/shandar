/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.emailer.service </p>
 * <p>File Name: MSExchangeService.java</p>
 * <p>Create Date: Sep 16, 2014 </p>
 * <p>Create Time: 8:43:53 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.emailer.service;

import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import microsoft.exchange.webservices.data.EmailMessage;
import microsoft.exchange.webservices.data.ExchangeCredentials;
import microsoft.exchange.webservices.data.ExchangeService;
import microsoft.exchange.webservices.data.ExchangeVersion;
import microsoft.exchange.webservices.data.FindItemsResults;
import microsoft.exchange.webservices.data.Folder;
import microsoft.exchange.webservices.data.Item;
import microsoft.exchange.webservices.data.ItemView;
import microsoft.exchange.webservices.data.PropertySet;
import microsoft.exchange.webservices.data.SearchFilter;
import microsoft.exchange.webservices.data.WebCredentials;
import microsoft.exchange.webservices.data.WellKnownFolderName;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.shandar.rytry.common.dataObject.SimpleDataObject;

/**
 * @author Shantanu Sikdar 
 *
 */
public class MSExchangeService {

	private static final Logger logger = LoggerFactory.getLogger(MSExchangeService.class);
	
	private static ExchangeService service;
	
	static{
		try{						
			service = new ExchangeService(ExchangeVersion.Exchange2010_SP1);
			//service = new ExchangeService(ExchangeVersion.Exchange2007_SP1);
			service.setUrl(new URI("https://webmail.bnymellon.com/ews/Exchange.asmx"));			
			ExchangeCredentials credentials = new WebCredentials("xbbkngi","20@Three","PAC");
			service.setCredentials(credentials);
			System.out.println("what == " + service==null);
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	public String receiveEmails() {
		List<SimpleDataObject> msgDataList = new ArrayList<SimpleDataObject>();
		try{
			msgDataList = readEmails(5);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return msgDataList.toString();
	}
	
	
	public SimpleDataObject readFromItem(Item item){
		SimpleDataObject msgData = new SimpleDataObject();
		try{
			Item itm = Item.bind(service, item.getId(), PropertySet.FirstClassProperties);				
			EmailMessage emailMessage = EmailMessage.bind(service, itm.getId());
			
			SimpleDataObject messageData = new SimpleDataObject();
			
			System.out.println("item.getId == "+item.getId());
			System.out.println("emailMessage.getId == "+emailMessage.getId());
			
			System.out.println(item.getId()==emailMessage.getId());
			System.out.println((item.getId()).equals(emailMessage.getId()));
			
			messageData.setAttribute("emailItemId", emailMessage.getId());
			System.out.println(emailMessage.getId());
			
			messageData.setAttribute("subject", emailMessage.getSubject());
			System.out.println(emailMessage.getSubject());
			
			messageData.setAttribute("FromAddress",emailMessage.getFrom().getAddress());
			System.out.println(emailMessage.getFrom().getAddress());
			
			Date dateTimeCreated = emailMessage.getDateTimeCreated();				
			messageData.setAttribute("SendDate",dateTimeCreated);
			System.out.println(dateTimeCreated);
			
			Date dateTimeRecieved = emailMessage.getDateTimeReceived();
			messageData.setAttribute("RecievedDate",dateTimeRecieved);
			System.out.println(dateTimeRecieved);
			
			messageData.setAttribute("Size",emailMessage.getSize());
							
			if("html".equalsIgnoreCase(emailMessage.getBody().getBodyType().toString())){
				messageData.setAttribute("HtmlBody",emailMessage.getBody().toString());
			}else{
				messageData.setAttribute("TextBody",emailMessage.getBody().toString());
			}
					
		}catch (Exception e) {
			e.printStackTrace();
		}
		return msgData;
	}
		
	public List<SimpleDataObject> readEmails(int numMails){
		List<SimpleDataObject> msgDataList = new ArrayList<SimpleDataObject>();
		try{
			Folder folder = Folder.bind( service, WellKnownFolderName.Inbox );
			FindItemsResults<Item> results = service.findItems(folder.getId(), new ItemView( numMails ) );		
			
			int i =1;
			for (Item item : results){				
				System.out.println("\nMESSAGE #" + (i++ ) + ":");				
				SimpleDataObject messageData = new SimpleDataObject();				
				messageData = readFromItem(item);
				msgDataList.add(messageData);
			}			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return msgDataList;
	}
	
	public List<String[]> getSubjectList(int numSubject){		
		List<SimpleDataObject> mailList=readEmails(numSubject);
		List<String[]> subList = new ArrayList<String[]>();		
		for (SimpleDataObject sdo : mailList) {
			String[] strArr = new String[3];
			strArr[0]=sdo.getAttribute("emailSetId").toString();
			strArr[1]=sdo.getAttribute("subject").toString();
			strArr[2]=sdo.getAttribute("emailItemId").toString();			
			subList.add(strArr);
		}		
		return subList;
	}
	
	
	public static void main(String[] args) {		
		//new MSExchangeService().readMail(5);
		System.out.println(new MSExchangeService().getSubjectList(5));
		
		//System.out.println(new MSExchangeService().readFromItem();
		
		//new MSExchangeService().getSubjectList(5);
		/*String str = new ExchTest().receiveEmails();
		System.out.println(str);*/
	}

}
