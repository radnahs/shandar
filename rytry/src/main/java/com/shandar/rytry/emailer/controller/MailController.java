/**
 *
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.emailer.exchange </p>
 * <p>File Name: ExchangeController.java</p>
 * <p>Create Date: Feb 6, 2014 </p>
 * <p>Create Time: 2:44:24 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.rytry.emailer.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpSession;

import microsoft.exchange.webservices.data.EmailMessage;
import microsoft.exchange.webservices.data.ExchangeCredentials;
import microsoft.exchange.webservices.data.ExchangeService;
import microsoft.exchange.webservices.data.ExchangeVersion;
import microsoft.exchange.webservices.data.FindItemsResults;
import microsoft.exchange.webservices.data.Folder;
import microsoft.exchange.webservices.data.Item;
import microsoft.exchange.webservices.data.ItemId;
import microsoft.exchange.webservices.data.ItemView;
import microsoft.exchange.webservices.data.PropertySet;
import microsoft.exchange.webservices.data.WebCredentials;
import microsoft.exchange.webservices.data.WellKnownFolderName;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shandar.rytry.common.dataObject.SimpleDataObject;
import com.shandar.rytry.login.model.User;

/**
 * @author Shantanu Sikdar 
 *
 */
@Controller
public class MailController {
	
	
	private static final Logger logger = LoggerFactory.getLogger(MailController.class);
	
	@RequestMapping( value="ExchangeMail")
    public String exchangeMail(User user, BindingResult result, Map model, HttpSession session) {		
        return "mail/mail";
    }
	
	
	@RequestMapping( value="ExchangeMailLink")    	
	public @ResponseBody String fetchScraperLink(HttpSession session) {		
		String num = new MailController().receiveEmails();
        return num;        
    };

	
	private void initExchangeInstance(){		
		ExchangeService service;
		try{						
			service = new ExchangeService(ExchangeVersion.Exchange2010_SP1);
			//service = new ExchangeService(ExchangeVersion.Exchange2007_SP1);
			service.setUrl(new URI("https://webmail.bnymellon.com/ews/Exchange.asmx"));			
			ExchangeCredentials credentials = new WebCredentials("xbbkngi","1@Escape","PAC");
			service.setCredentials(credentials);
		}catch (Exception e) {
			logger.error(e+"");
		}
	}
	
	
	
	public String receiveEmails() {
		String str=""; 
		try{
			logger.debug("########## Inside Receive Emails ####################");
			ExchangeService service = null;			
			service = new ExchangeService(ExchangeVersion.Exchange2010_SP1);			
			//service = new ExchangeService(ExchangeVersion.Exchange2007_SP1);
			service.setUrl(new URI("https://webmail.bnymellon.com/ews/Exchange.asmx"));
			//ExchangeCredentials credentials = new WebCredentials("xbbkngi","13@FTwelve","PAC");
			//ExchangeCredentials credentials = new WebCredentials("xbbkngi","1@Escape","PAC");
			ExchangeCredentials credentials = new WebCredentials("xbbkngi","20@Three","PAC");
			
			service.setCredentials(credentials);
			str = readMail(service);
		}catch (Exception e) {
			logger.error(e+"");
		}
		return str;
	}
	
	
	public String readMail(ExchangeService service){
		String str="";
		try{
			Folder folder = Folder.bind( service, WellKnownFolderName.Inbox );
			FindItemsResults<Item> results = service.findItems(folder.getId(), new ItemView( 2 ) );		
			ArrayList<ItemId> itemsToDelete = new ArrayList<ItemId>();
			
			int i =1;
			for (Item item : results){
				logger.debug("--------------------------");
				//logger.debug("MESSAGE #" + (i++ ) + ":");
				
				System.out.println("\nMESSAGE #" + (i++ ) + ":");
				
				Item itm = Item.bind(service, item.getId(), PropertySet.FirstClassProperties);				
				EmailMessage emailMessage = EmailMessage.bind(service, itm.getId());
				
				SimpleDataObject messageData = new SimpleDataObject();
				
				messageData.setAttribute("subject", emailMessage.getSubject());
				System.out.println(emailMessage.getSubject());
				
				messageData.setAttribute("FromAddress",emailMessage.getFrom().getAddress());
				System.out.println(emailMessage.getFrom().getAddress());
				
				Date dateTimeCreated = emailMessage.getDateTimeCreated();				
				messageData.setAttribute("SendDate",dateTimeCreated);
				System.out.println(dateTimeCreated);
				
				Date dateTimeRecieved = emailMessage.getDateTimeReceived();
				messageData.setAttribute("RecievedDate",dateTimeRecieved);
				System.out.println(dateTimeRecieved);
				
				messageData.setAttribute("Size",emailMessage.getSize());
				
				
				if("html".equalsIgnoreCase(emailMessage.getBody().getBodyType().toString())){
					messageData.setAttribute("HtmlBody",emailMessage.getBody().toString());
				}else{
					messageData.setAttribute("TextBody",emailMessage.getBody().toString());
				}
				//System.out.println(emailMessage.getBody().toString());
				str = emailMessage.getBody().toString();
				
				logger.debug("--------------------------");
				//System.out.println(str);
			}
			/*if(!itemsToDelete.isEmpty()){
				deleteInboxEmails(service,itemsToDelete);
			}*/
		}catch (Exception e) {
			logger.error(""+e);
		}
		return str;
	}
	
	
	
	public static void main(String[] args) {
		new MailController().receiveEmails(); 
	}
	


}
