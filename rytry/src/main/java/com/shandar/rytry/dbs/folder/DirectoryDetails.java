/**
 * <p>Project: rytry </p>
 * <p>Package Name: com.shandar.rytry.dbs.folder </p>
 * <p>File Name: DirectoryDetails.java </p>
 * <p>Create Date: Oct 15, 2020 </p>
 * <p>Create Time: 2:32:10 PM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 *
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.rytry.dbs.folder;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author : Shantanu Sikdar
 * @Description : DirectoryDetails http://zetcode.com/java/listdirectory/
 */
public class DirectoryDetails {

    private static List<File> onlyDirectories = new ArrayList<>();
    private static Map<String, List<File>> onlyFilesMap = new HashMap<>();
    private static List<File> files = new ArrayList<>();
    private static Map<File, List<File>> dirContents = new HashMap<File, List<File>>();

    public static void main(String[] args) {
        // File file = new File("C:\\data\\projects\\zips\\");
        //File file = new File("C:\\data\\projects\\celerity-query-runner\\");
        File file = new File("C:\\data\\projects\\celerity-data-drive\\");
        List<File> myfiles = doListing(file);
        // System.out.println(myfiles);
        // myfiles.forEach(System.out::println);
        // System.out.println(dirContents);
        // onlyDirectories.forEach(System.out::println);
        // onlyFilesMap.forEach(System.out::println);
        onlyFilesMap.forEach((key, value) -> System.out.printf("Key is %s %n value is %s %n", key, value));
        // dirContents.forEach((key, value) -> System.out.println(key + ":" + value));
    }

    public static List<File> doListing(File dirName) {
        File[] fileList = dirName.listFiles();
        dirContents.put(dirName, new ArrayList<File>());
        File tempKey = dirName;
        for (File file : fileList) {

            if (file.isFile()) {
                files.add(file);
                buildFilesMap(file);
                List<File> onlyFiles = dirContents.get(tempKey);
                onlyFiles.add(file);
                dirContents.put(tempKey, onlyFiles);
            } else if (file.isDirectory()) {
                files.add(file);
                onlyDirectories.add(file);
                List<File> onlyFiles = new ArrayList<>();
                tempKey = file;
                dirContents.put(file, onlyFiles);
                doListing(file);
            }
        }
        return files;
    }

    private static void buildFilesMap(File file) {
        String[] nameFileType = file.getName().split("\\.");
        String ext = nameFileType.length > 1 ? nameFileType[nameFileType.length - 1] : file.getName();
        if (onlyFilesMap.containsKey(ext)) {
            List<File> fileWithPath = onlyFilesMap.get(ext);
            fileWithPath.add(file);
            onlyFilesMap.put(ext, fileWithPath);
        } else {
            List<File> fileWithPath = new ArrayList<File>();
            onlyFilesMap.put(ext, fileWithPath);
        }

    }

}
