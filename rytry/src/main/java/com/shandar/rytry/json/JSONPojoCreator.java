/**
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.json </p>
 * <p>File Name: JSONPojoCreator.java </p>
 * <p>Create Date: 25-Oct-2021 </p>
 * <p>Create Time: 5:58:07 PM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.rytry.json;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import org.jsonschema2pojo.DefaultGenerationConfig;
import org.jsonschema2pojo.GenerationConfig;
import org.jsonschema2pojo.Jackson2Annotator;
import org.jsonschema2pojo.SchemaGenerator;
import org.jsonschema2pojo.SchemaMapper;
import org.jsonschema2pojo.SchemaStore;
import org.jsonschema2pojo.SourceType;
import org.jsonschema2pojo.rules.RuleFactory;

import com.sun.codemodel.JCodeModel;

/**
 * @author : Shantanu Sikdar
 * @Description : JSONPojoCreator
 * https://github.com/eugenp/tutorials/blob/master/json-2/src/main/java/com/baeldung/jsontojavaclass/JsonToJavaClassConversion.java
 */
public class JSONPojoCreator {

	public void convertJsonToJavaClass(URL inputJsonUrl, File outputJavaClassDirectory, String packageName,
			String javaClassName) throws IOException {
		JCodeModel jcodeModel = new JCodeModel();

		GenerationConfig config = new DefaultGenerationConfig() {
			@Override
			public boolean isGenerateBuilders() {
				return true;
			}

			@Override
			public SourceType getSourceType() {
				return SourceType.JSON;
			}
		};

		SchemaMapper mapper = new SchemaMapper(
				new RuleFactory(config, new Jackson2Annotator(config), new SchemaStore()), new SchemaGenerator());
		mapper.generate(jcodeModel, javaClassName, packageName, inputJsonUrl);

		jcodeModel.build(outputJavaClassDirectory);
	}

	public static void main(String[] args) {
		System.out.println("JSON to Java");
		/*
		 * String packageName = "com.shandar.rytry.json"; String basePath =
		 * "/rytry_trunk/src/main/resources/jsonToJava/inputJson"; File inputJson = new
		 * File(basePath + File.separator + "input.json"); //File outputPojoDirectory =
		 * new File(basePath + File.separator + "convertedPojo"); File
		 * outputPojoDirectory = new File(
		 * "C:/data/projects/shandar_ws/shandar/src/main/resources/jsonToJava/inputJson"
		 * + File.separator + "convertedPojo"); outputPojoDirectory.mkdirs(); try { new
		 * JSONPojoCreator().convertJsonToJavaClass(inputJson.toURI().toURL(),
		 * outputPojoDirectory, packageName, inputJson.getName().replace(".json", ""));
		 * } catch (IOException e) {
		 * System.out.println("Encountered issue while converting to pojo: " +
		 * e.getMessage()); e.printStackTrace(); }
		 */
	}
}
 