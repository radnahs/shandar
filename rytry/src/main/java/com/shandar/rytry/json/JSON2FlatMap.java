/**
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.json </p>
 * <p>File Name: JSON2FlatMap.java </p>
 * <p>Create Date: 26-Jan-2021 </p>
 * <p>Create Time: 1:15:27 PM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
 
package com.shandar.rytry.json;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * https://stackoverflow.com/questions/443499/convert-json-to-map
 */
public class JSON2FlatMap {

    public static void main(String[] args) throws Exception {
        JSON2FlatMap json2FlatMap = new JSON2FlatMap();
        String json = "{\n" +
                "  \"medications\": [\n" +
                "    {\n" +
                "      \"aceInhibitors\": [\n" +
                "        {\n" +
                "          \"name\": \"lisinopril\",\n" +
                "          \"strength\": \"10 mg Tab\",\n" +
                "          \"dose\": \"1 tab\",\n" +
                "          \"route\": \"PO\",\n" +
                "          \"sig\": \"daily\",\n" +
                "          \"pillCount\": \"#90\",\n" +
                "          \"refills\": \"Refill 3\"\n" +
                "        }\n" +
                "      ],\n" +
                "      \"antianginal\": [\n" +
                "        {\n" +
                "          \"name\": \"nitroglycerin\",\n" +
                "          \"strength\": \"0.4 mg Sublingual Tab\",\n" +
                "          \"dose\": \"1 tab\",\n" +
                "          \"route\": \"SL\",\n" +
                "          \"sig\": \"q15min PRN\",\n" +
                "          \"pillCount\": \"#30\",\n" +
                "          \"refills\": \"Refill 1\"\n" +
                "        }\n" +
                "      ],\n" +
                "      \"anticoagulants\": [\n" +
                "        {\n" +
                "          \"name\": \"warfarin sodium\",\n" +
                "          \"strength\": \"3 mg Tab\",\n" +
                "          \"dose\": \"1 tab\",\n" +
                "          \"route\": \"PO\",\n" +
                "          \"sig\": \"daily\",\n" +
                "          \"pillCount\": \"#90\",\n" +
                "          \"refills\": \"Refill 3\"\n" +
                "        }\n" +
                "      ],\n" +
                "      \"betaBlocker\": [\n" +
                "        {\n" +
                "          \"name\": \"metoprolol tartrate\",\n" +
                "          \"strength\": \"25 mg Tab\",\n" +
                "          \"dose\": \"1 tab\",\n" +
                "          \"route\": \"PO\",\n" +
                "          \"sig\": \"daily\",\n" +
                "          \"pillCount\": \"#90\",\n" +
                "          \"refills\": \"Refill 3\"\n" +
                "        }\n" +
                "      ],\n" +
                "      \"diuretic\": [\n" +
                "        {\n" +
                "          \"name\": \"furosemide\",\n" +
                "          \"strength\": \"40 mg Tab\",\n" +
                "          \"dose\": \"1 tab\",\n" +
                "          \"route\": \"PO\",\n" +
                "          \"sig\": \"daily\",\n" +
                "          \"pillCount\": \"#90\",\n" +
                "          \"refills\": \"Refill 3\"\n" +
                "        }\n" +
                "      ],\n" +
                "      \"mineral\": [\n" +
                "        {\n" +
                "          \"name\": \"potassium chloride ER\",\n" +
                "          \"strength\": \"10 mEq Tab\",\n" +
                "          \"dose\": \"1 tab\",\n" +
                "          \"route\": \"PO\",\n" +
                "          \"sig\": \"daily\",\n" +
                "          \"pillCount\": \"#90\",\n" +
                "          \"refills\": \"Refill 3\"\n" +
                "        }\n" +
                "      ]\n" +
                "    }\n" +
                "  ],\n" +
                "  \"labs\": [\n" +
                "    {\n" +
                "      \"name\": \"Arterial Blood Gas\",\n" +
                "      \"time\": \"Today\",\n" +
                "      \"location\": \"Main Hospital Lab\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"name\": \"BMP\",\n" +
                "      \"time\": \"Today\",\n" +
                "      \"location\": \"Primary Care Clinic\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"name\": \"BNP\",\n" +
                "      \"time\": \"3 Weeks\",\n" +
                "      \"location\": \"Primary Care Clinic\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"name\": \"BUN\",\n" +
                "      \"time\": \"1 Year\",\n" +
                "      \"location\": \"Primary Care Clinic\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"name\": \"Cardiac Enzymes\",\n" +
                "      \"time\": \"Today\",\n" +
                "      \"location\": \"Primary Care Clinic\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"name\": \"CBC\",\n" +
                "      \"time\": \"1 Year\",\n" +
                "      \"location\": \"Primary Care Clinic\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"name\": \"Creatinine\",\n" +
                "      \"time\": \"1 Year\",\n" +
                "      \"location\": \"Main Hospital Lab\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"name\": \"Electrolyte Panel\",\n" +
                "      \"time\": \"1 Year\",\n" +
                "      \"location\": \"Primary Care Clinic\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"name\": \"Glucose\",\n" +
                "      \"time\": \"1 Year\",\n" +
                "      \"location\": \"Main Hospital Lab\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"name\": \"PT/INR\",\n" +
                "      \"time\": \"3 Weeks\",\n" +
                "      \"location\": \"Primary Care Clinic\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"name\": \"PTT\",\n" +
                "      \"time\": \"3 Weeks\",\n" +
                "      \"location\": \"Coumadin Clinic\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"name\": \"TSH\",\n" +
                "      \"time\": \"1 Year\",\n" +
                "      \"location\": \"Primary Care Clinic\"\n" +
                "    }\n" +
                "  ],\n" +
                "  \"imaging\": [\n" +
                "    {\n" +
                "      \"name\": \"Chest X-Ray\",\n" +
                "      \"time\": \"Today\",\n" +
                "      \"location\": \"Main Hospital Radiology\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"name\": \"Chest X-Ray\",\n" +
                "      \"time\": \"Today\",\n" +
                "      \"location\": \"Main Hospital Radiology\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"name\": \"Chest X-Ray\",\n" +
                "      \"time\": \"Today\",\n" +
                "      \"location\": \"Main Hospital Radiology\"\n" +
                "    }\n" +
                "  ]\n" +
                "}";

        Map<String, String> map = json2FlatMap.fromJsonToFlatMap(json);
        System.out.println(map);
    }

    public Map<String, String> fromJsonToFlatMap(String json) throws IOException {
        JsonFactory factory = new JsonFactory();
        Map<String, String> result = new HashMap<>();

        ObjectMapper mapper = new ObjectMapper(factory);
        JsonNode rootNode = mapper.readTree(json);
        toFlatMap(result, "", rootNode);

        return result;
    }

    private void toFlatMap(Map<String, String> result, String parentKey, JsonNode node) {
        if (node.isContainerNode()) {
            String parent = parentKey.isEmpty() ? "" : (parentKey + ".");
            if (node.isArray()) {
                int i = 0;
                for (JsonNode arrayNode : node) {
                    i++;
                    toFlatMap(result, parent + i, arrayNode);
                }
                result.put(parent + "size", i + "");
            } else {
                Iterator<Map.Entry<String, JsonNode>> fieldsIterator = node.fields();
                //node.getFields();
                while (fieldsIterator.hasNext()) {
                    Map.Entry<String, JsonNode> field = fieldsIterator.next();
                    //JsonNode nodeValue = field.getValue();
                    toFlatMap(result, parent + field.getKey(), field.getValue());
                }
            }
        } else {
            result.put(parentKey, node.asText());
        }
    }
}
