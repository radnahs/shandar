/**
 * <p>Project: rytry_trunk </p>
 * <p>Package Name: com.shandar.rytry.json </p>
 * <p>File Name: GeneratePojoFromJson.java </p>
 * <p>Create Date: 26-Oct-2021 </p>
 * <p>Create Time: 9:58:07 PM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
 
 
 package com.shandar.rytry.json;

import com.sun.codemodel.JCodeModel;
import org.jsonschema2pojo.*;
import org.jsonschema2pojo.rules.RuleFactory;

import java.io.File;
import java.io.IOException;

public class GeneratePojoFromJson {

    public void convertJsonToJavaClass(String inputJsonUrl, File outputJavaClassDirectory, String packageName, String javaClassName) throws IOException {
        JCodeModel jcodeModel = new JCodeModel();

        GenerationConfig config = new DefaultGenerationConfig() {
            @Override
            public boolean isGenerateBuilders() {
                return true;
            }

            @Override
            public SourceType getSourceType() {
                return SourceType.JSON;
            }
        };

        SchemaMapper mapper = new SchemaMapper(new RuleFactory(config, new Jackson2Annotator(config), new SchemaStore()), new SchemaGenerator());
        mapper.generate(jcodeModel, javaClassName, packageName, inputJsonUrl);

        jcodeModel.build(outputJavaClassDirectory);
    }

    public static void main(String[] args) throws IOException {
        GeneratePojoFromJson generatePojoFromJson = new GeneratePojoFromJson();
        String jsonString = "{\"name\":\"Ram\",\"age\":30},{\"name\":\"Sita\",\"age\":25}";
        String packagePath = "E:\\data\\project\\src\\main\\java\\";// need to adjust as per the path
        String packageName = "com.shandar.rytry.json";// need to adjust as per the package
        generatePojoFromJson.convertJsonToJavaClass(jsonString, new File(packagePath), packageName, "Test");
    }

}
