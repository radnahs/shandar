/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.concepts </p>
 * <p>File Name: PassByValue.java </p>
 * <p>Create Date: 16-Feb-2023 </p>
 * <p>Create Time: 3:49:41 PM </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.concepts;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author : Shantanu Sikdar
 *
 */
public class PassByValue {

	public static void main(String[] args) {
		int ssInt = 102;
		System.out.println("1: " + ssInt);
		testDatatypeMethod(ssInt);
		System.out.println("2: " + ssInt);
		ssInt = testDatatypeMethod(ssInt);
		System.out.println("3: " + ssInt);
		
		String ss = "shantanu";
		System.out.println("1: " + ss);
		testImmutableObjectsMethod(ss);
		System.out.println("2: " + ss);
		ss = testImmutableObjectsMethod(ss);
		System.out.println("3: " + ss);

		List<StringBuilder> lstStrBldr = new ArrayList<StringBuilder>();
		lstStrBldr.add(new StringBuilder("String1"));
		lstStrBldr.add(new StringBuilder("String2"));
		lstStrBldr.add(new StringBuilder("String3"));

		System.out.println("1 " + lstStrBldr);
		testNonMutableObjectsMethod(lstStrBldr);
		System.out.println("2 " + lstStrBldr);
		lstStrBldr = testNonMutableObjectsMethod(lstStrBldr);
		System.out.println("3 " + lstStrBldr);

	}
	private static int testDatatypeMethod(int ssInt) {
		System.out.println(ssInt);
		ssInt = 105;
		System.out.println(ssInt);
		return ssInt;
	}
	
	private static String testImmutableObjectsMethod(String ss) {
		System.out.println(ss);
		ss = "new String";
		System.out.println(ss);
		return ss;
	}

	private static List<StringBuilder> testNonMutableObjectsMethod(List<StringBuilder> lstStrBldr) {
		System.out.println(lstStrBldr);
		lstStrBldr = new ArrayList<StringBuilder>();
		lstStrBldr.add(new StringBuilder("new String1"));
		lstStrBldr.add(new StringBuilder("new String2"));
		lstStrBldr.add(new StringBuilder("new String3"));
		System.out.println(lstStrBldr);
		return lstStrBldr;
	}
}
