/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.tricks </p>
 * <p>File Name: Print1To100WithoutUsingNumbers.java </p>
 * <p>Create Date: 20-Mar-2021 </p>
 * <p>Create Time: 7:17:42 am </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.tricks;

/**
 * @author : Shantanu Sikdar
 *
 */
public class Print1To100WithoutUsingNumbers {

	public static void main(String[] args) {
		int cnt = 'A' / 'A';
		String limit = "onehundred";

		for (int i = cnt; i <= limit.length() * limit.length(); i++) {
			System.out.print(i + " ");
		}

		System.out.println();

		for (int i = cnt; i <= 'd'; i++) {
			System.out.print(i + " ");
		}
	}

}
