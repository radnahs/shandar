/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.tricks.customMarkerInterface </p>
 * <p>File Name: CustomMIMain.java </p>
 * <p>Create Date: 03-Nov-2022 </p>
 * <p>Create Time: 11:30:38 PM </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.tricks.customMarkerInterface;

/**
 * @author : Shantanu Sikdar
 *
 *	https://stackoverflow.com/questions/11008033/how-to-write-our-own-marker-interface-in-java
 */
public class CustomMIMain extends CustomMIImplementationClass implements CustomMarkerInterface{

	public static void main(String[] args) {
		CustomMIMain cmim =  new CustomMIMain();
		try {
			cmim.method();
		} catch (CustomMIException e) {
			e.printStackTrace();
		}
	}

}
