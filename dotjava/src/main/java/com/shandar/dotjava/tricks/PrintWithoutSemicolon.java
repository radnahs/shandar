/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.tricks </p>
 * <p>File Name: PrintWithoutSemicolon.java </p>
 * <p>Create Date: 19-Mar-2021 </p>
 * <p>Create Time: 6:42:38 pm </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.tricks;

/**
 * @author : Shantanu Sikdar
 *
 */
public class PrintWithoutSemicolon {

	public static void main(String[] args) {

		if (System.out.printf("Hello Java using System.out.printf and == \n") == null) {
		}
		
		if (System.out.printf("Hello Java using System.out.printf and equals(Obj)\n").equals(null)) {
		}

		if (System.out.append("Hello Java using System.out.append and == \n") == null) {
		}
		
		if (System.out.append("Hello Java using System.out.append and equals(Obj)\n").equals(null)) {
		}

		if (System.out.format("Hello Java using System.out.format and == \n") == null) {
		}
		
		if (System.out.format("Hello Java using System.out.format and equals(Obj) \n").equals(null)) {
		}

		for (int i = 0; i < 1; System.out.println("Hello Java in for loop"), i++) {
		}

	}
	
}
