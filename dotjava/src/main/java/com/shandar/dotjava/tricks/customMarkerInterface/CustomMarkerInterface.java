/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.tricks.customMarkerInterface </p>
 * <p>File Name: CustomMarkerInterface.java </p>
 * <p>Create Date: 04-Nov-2022 </p>
 * <p>Create Time: 3:28:03 PM </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.tricks.customMarkerInterface;

/**
 * @author : Shantanu Sikdar
 *
 */
public interface CustomMarkerInterface {

}
