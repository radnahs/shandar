/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.tricks.quine </p>
 * <p>File Name: SelfReplicatingProgram.java </p>
 * <p>Create Date: 24-Nov-2022 </p>
 * <p>Create Time: 1:31:17 PM </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.tricks.quine;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.util.Scanner;

/**
 * @author : Shantanu Sikdar
 *
 */
public class SelfReplicatingProgram {

	public static void main(String[] args) throws FileNotFoundException {
		File fileToRead = new File("E:\\data\\project\\shandar\\dotjava\\src\\main\\java\\com\\shandar\\dotjava\\alGods\\quine\\SelfReplicatingProgram.java");
		Scanner scanner = new Scanner(fileToRead);
		while(scanner.hasNext()) {
			System.out.println(scanner.nextLine());
		}
	}
	
	public static void main1(String args[]) {
		try {
			// open the file
			FileInputStream fstream = new FileInputStream("E:\\data\\project\\shandar\\dotjava\\src\\main\\java\\com\\shandar\\dotjava\\alGods\\quine\\SelfReplicatingProgram.java");// here pass the own java file name with full path
			// use DataInputStream to read binary NOT text
			// DataInputStream in=new DataInputStream(fstream);
			//
			BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
			// read data line by line from the file
			String s;
			while ((s = br.readLine()) != null) {
				System.out.println(s);
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
