/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.tricks.customMarkerInterface </p>
 * <p>File Name: CustomMIException.java </p>
 * <p>Create Date: 04-Nov-2022 </p>
 * <p>Create Time: 3:32:11 PM </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.tricks.customMarkerInterface;

/**
 * @author : Shantanu Sikdar
 *
 */
public class CustomMIException extends Exception{

	/**
	 * 
	 */
	public CustomMIException() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public CustomMIException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 */
	public CustomMIException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 */
	public CustomMIException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param cause
	 */
	public CustomMIException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	
}
