/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.tricks.customMarkerInterface </p>
 * <p>File Name: CustomMIImplementationClass.java </p>
 * <p>Create Date: 04-Nov-2022 </p>
 * <p>Create Time: 3:28:49 PM </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.tricks.customMarkerInterface;

/**
 * @author : Shantanu Sikdar
 *
 */
public class CustomMIImplementationClass {

	public void method() throws CustomMIException{
		if(this instanceof CustomMarkerInterface) {
			System.out.println("successful");
		}else {
			throw new CustomMIException("Must implement interface CustomMarkerInterface");
		}
	}
	
}
