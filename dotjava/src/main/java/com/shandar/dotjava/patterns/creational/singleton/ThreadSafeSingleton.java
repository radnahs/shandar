/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.patterns.creational.singleton </p>
 * <p>File Name: ThreadSafeSingleton.java </p>
 * <p>Create Date: 27-Feb-2014 </p>
 * <p>Create Time: 12:38:00 am </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.patterns.creational.singleton;

/**
 * @author : Shantanu Sikdar
 * @Description : ThreadSafeSingleton
 */
public class ThreadSafeSingleton{
	
	private static ThreadSafeSingleton instance;
	
	private ThreadSafeSingleton(){}
	
	public static synchronized ThreadSafeSingleton getInstance() {
		if(instance==null) {
			instance = new ThreadSafeSingleton();
		}
		return instance;
	}
}
