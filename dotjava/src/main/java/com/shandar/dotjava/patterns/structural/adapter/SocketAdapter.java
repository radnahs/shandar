/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.patterns.structural.adapter </p>
 * <p>File Name: SocketAdapter.java </p>
 * <p>Create Date: 01-Mar-2020 </p>
 * <p>Create Time: 11:21:23 am </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.patterns.structural.adapter;

/**
 * @author : Shantanu Sikdar
 *
 */
public interface SocketAdapter {

	public Volt get120Volt();

	public Volt get12Volt();

	public Volt get3Volt();

}
