/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.patterns.creational.factory </p>
 * <p>File Name: Computer.java </p>
 * <p>Create Date: 27-Feb-2020 </p>
 * <p>Create Time: 10:15:19 am </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.patterns.creational.factory;

/**
 * @author : Shantanu Sikdar
 * @Description : Computer
 */
public abstract class Computer {

	public abstract String getRAM();

	public abstract String getHDD();

	public abstract String getCPU();

	@Override
	public String toString() {
		return "Computer [getRAM()=" + this.getRAM() + ", getHDD()=" + this.getHDD() + ", getCPU()=" + this.getCPU() + "]";
	}

	
	
	
}
