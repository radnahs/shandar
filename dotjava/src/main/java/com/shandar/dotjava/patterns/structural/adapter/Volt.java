/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.patterns.structural.adapter </p>
 * <p>File Name: Volt.java </p>
 * <p>Create Date: 01-Mar-2020 </p>
 * <p>Create Time: 11:11:50 am </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.patterns.structural.adapter;

/**
 * @author : Shantanu Sikdar
 *
 */
public class Volt {
	
	private int volts;

	public Volt(int volts) {
		this.volts = volts;
	}

	public int getVolts() {
		return volts;
	}

	public void setVolts(int volts) {
		this.volts = volts;
	}
	
	

}
