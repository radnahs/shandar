/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.patterns.creational.factory </p>
 * <p>File Name: PC.java </p>
 * <p>Create Date: 27-Feb-2020 </p>
 * <p>Create Time: 10:17:15 am </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.patterns.creational.factory;

/**
 * @author : Shantanu Sikdar
 * @Description : PC
 */
public class PC extends Computer{

	private String ram;
	private String hdd;
	private String cpu;

	public PC(String ram, String hdd, String cpu) {
		this.ram = ram;
		this.hdd = hdd;
		this.cpu = cpu;
	}

	@Override
	public String getRAM() {
		return this.ram;
	}

	@Override
	public String getHDD() {
		return this.hdd;
	}

	@Override
	public String getCPU() {
		return this.cpu;
	}
	
	
}
