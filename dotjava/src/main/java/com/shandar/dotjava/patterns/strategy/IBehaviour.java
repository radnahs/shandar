/**
 *
 * <p>Project: trunk_dotjava </p>
 * <p>Package Name: com.shandar.dotjava.patterns.strategy </p>
 * <p>File Name: IBehaviour.java</p>
 * <p>Create Date: Nov 28, 2013 </p>
 * <p>Create Time: 4:32:53 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.dotjava.patterns.strategy;

/**
 * @author Shantanu Sikdar 
 *
 */
public interface IBehaviour {
	public int moveCommand();
}
