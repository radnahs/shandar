package com.shandar.dotjava.patterns.factory;

public interface BrakingSystem {
	public void applyBrake();
}
