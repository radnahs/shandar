/**
 *
 * <p>Project: trunk_dotjava </p>
 * <p>Package Name: com.shandar.dotjava.patterns.strategy </p>
 * <p>File Name: Robot.java</p>
 * <p>Create Date: Nov 28, 2013 </p>
 * <p>Create Time: 4:39:33 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.dotjava.patterns.strategy;

/**
 * @author Shantanu Sikdar 
 *
 */
public class Robot {
	
	private IBehaviour behaviour;
	private String name;
	
	
	public Robot(String name) {		
		this.name = name;
	}
	
	public IBehaviour getBehaviour() {
		return behaviour;
	}
	public void setBehaviour(IBehaviour behaviour) {
		this.behaviour = behaviour;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public void move(){
		System.out.println(this.name + ": Based on current position the behaviour object decide the next move:");
		int command = behaviour.moveCommand();
		// 	... send the command to mechanisms
		System.out.println("\tThe result returned by behaviour object is sent to the movement mechanisms for the robot '"  + this.name + "'");		
	}

}
