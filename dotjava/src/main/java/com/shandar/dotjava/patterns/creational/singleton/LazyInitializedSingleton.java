/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.patterns.creational.singleton </p>
 * <p>File Name: LazyInitializedSingleton.java </p>
 * <p>Create Date: 27-Feb-2014 </p>
 * <p>Create Time: 12:33:53 am </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.patterns.creational.singleton;

/**
 * @author : Shantanu Sikdar
 * @Description : LazyInitializedSingleton
 */
public class LazyInitializedSingleton {
	
	private static LazyInitializedSingleton instance;
	
	private LazyInitializedSingleton() {
	}
	
	public static LazyInitializedSingleton getInstance() {
		if(instance==null) {
			instance = new LazyInitializedSingleton();
		}
		return instance;
	}

}
