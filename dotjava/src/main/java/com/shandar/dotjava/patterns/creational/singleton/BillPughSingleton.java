/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.patterns.creational.singleton </p>
 * <p>File Name: BillPughSingleton.java </p>
 * <p>Create Date: 27-Feb-2014 </p>
 * <p>Create Time: 12:57:35 am </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.patterns.creational.singleton;

/**
 * @author : Shantanu Sikdar
 * @Description : BillPughSingleton
 */
public class BillPughSingleton {

	private BillPughSingleton() {}
	
	private static class BillPughSingletonHelper{
		private static final BillPughSingleton INSTANCE = new BillPughSingleton();
	}
	
	public static BillPughSingleton getInstance() {
		return BillPughSingletonHelper.INSTANCE;
	}
	
}
