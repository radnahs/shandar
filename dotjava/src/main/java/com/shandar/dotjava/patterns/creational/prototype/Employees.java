/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.patterns.creational.prototype </p>
 * <p>File Name: Employees.java </p>
 * <p>Create Date: 01-Mar-2020 </p>
 * <p>Create Time: 8:40:55 am </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.patterns.creational.prototype;

import java.util.ArrayList;
import java.util.List;

/**
 * @author : Shantanu Sikdar
 *
 */
public class Employees implements Cloneable {

	private List<String> employeeList;

	public Employees() {
		employeeList = new ArrayList<String>();
	}

	public Employees(List<String> employeeList) {
		this.employeeList = employeeList;
	}
	
	
	public void loadData() {
		this.employeeList.add("Shantanu");
		this.employeeList.add("Nandita");
		this.employeeList.add("Ranjit");
		this.employeeList.add("Priti");
		this.employeeList.add("Piyali");
	}

	public List<String> getEmployeeList(){
		return employeeList;
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException{
		List<String> temp = new ArrayList<String>();
		for (String string : this.getEmployeeList()) {
			temp.add( string);
		}
		return new Employees(temp);
	}

	@Override
	public String toString() {
		return "Employees [employeeList=" + employeeList + "]";
	}
	
	
	
}
