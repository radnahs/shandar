/**
 *
 * <p>Project: trunk_dotjava </p>
 * <p>Package Name: com.shandar.dotjava.patterns </p>
 * <p>File Name: CarBrake.java</p>
 * <p>Create Date: Nov 8, 2013 </p>
 * <p>Create Time: 6:35:54 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.dotjava.patterns.factory;

/**
 * @author Shantanu Sikdar 
 *
 */
public class BoshBrake implements BrakingSystem {

	/* (non-Javadoc)
	 * @see com.shandar.dotjava.patterns.BrakingSystem#applyBrake()
	 */
	@Override
	public void applyBrake() {
		// TODO Auto-generated method stub
		System.out.println("Bosh brakes");

	}

}
