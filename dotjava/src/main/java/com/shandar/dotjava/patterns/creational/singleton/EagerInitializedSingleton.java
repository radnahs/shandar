/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.patterns.creational.singleton </p>
 * <p>File Name: EagerInitializedSingleton.java </p>
 * <p>Create Date: 27-Feb-2014 </p>
 * <p>Create Time: 12:19:46 am </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.patterns.creational.singleton;

/**
 * @author : Shantanu Sikdar
 * @Description : EagerInitializedSingleton
 */
public class EagerInitializedSingleton {
	
	private static final EagerInitializedSingleton instance = new EagerInitializedSingleton();

	private EagerInitializedSingleton() {
	}

	public static EagerInitializedSingleton getInstance() {
		return instance;
	}

}
/*
class A extends EagerInitializedSingleton{
	
}*/
