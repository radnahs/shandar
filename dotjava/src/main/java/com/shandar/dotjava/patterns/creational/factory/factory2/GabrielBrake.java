/**
 *
 * <p>Project: trunk_dotjava </p>
 * <p>Package Name: com.shandar.dotjava.patterns </p>
 * <p>File Name: BikeBrake.java</p>
 * <p>Create Date: Nov 8, 2013 </p>
 * <p>Create Time: 6:35:05 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.dotjava.patterns.factory;

/**
 * @author Shantanu Sikdar 
 *
 */
public class GabrielBrake implements BrakingSystem {

	/* (non-Javadoc)
	 * @see com.shandar.dotjava.patterns.BrakingSystem#applyBrake()
	 */
	@Override
	public void applyBrake() {
		System.out.println("Gabriel brakes");
	}

}
