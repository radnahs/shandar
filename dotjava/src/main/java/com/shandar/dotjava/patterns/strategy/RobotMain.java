/**
 *
 * <p>Project: trunk_dotjava </p>
 * <p>Package Name: com.shandar.dotjava.patterns.strategy </p>
 * <p>File Name: RobotMain.java</p>
 * <p>Create Date: Nov 28, 2013 </p>
 * <p>Create Time: 4:46:03 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.dotjava.patterns.strategy;

/**
 * @author Shantanu Sikdar 
 *
 */
public class RobotMain {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		Robot r1 = new Robot("Black Robot");
		Robot r2 = new Robot("White Robot");
		Robot r3 = new Robot("Normal Robot");

		r1.setBehaviour(new AgressiveBehaviour());
		r2.setBehaviour(new DefensiveBehaviour());
		r3.setBehaviour(new NormalBehaviour());

		r1.move();
		r2.move();
		r3.move();

		System.out.println("\r\nNew behaviours: " +
				"\r\n\t'Black Robot' gets really scared" +
				"\r\n\t, 'White Robot' becomes really mad because " +
				"it's always attacked by other robots" +
				"\r\n\t and Robot keeps its calm\r\n");

		r1.setBehaviour(new DefensiveBehaviour());
		r2.setBehaviour(new AgressiveBehaviour());

		r1.move();
		r2.move();
		r3.move();
	}

}
