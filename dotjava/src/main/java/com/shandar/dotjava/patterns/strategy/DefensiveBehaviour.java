/**
 *
 * <p>Project: trunk_dotjava </p>
 * <p>Package Name: com.shandar.dotjava.patterns.strategy </p>
 * <p>File Name: DefensiveBehaviour.java</p>
 * <p>Create Date: Nov 28, 2013 </p>
 * <p>Create Time: 4:36:13 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.dotjava.patterns.strategy;

/**
 * @author Shantanu Sikdar 
 *
 */
public class DefensiveBehaviour implements IBehaviour {

	/* (non-Javadoc)
	 * @see com.shandar.dotjava.patterns.strategy.IBehaviour#moveCommand()
	 */
	@Override
	public int moveCommand() {
		// TODO Auto-generated method stub
		System.out.println("Defensive Behaviour : Defend");
		return -1;
	}

}
