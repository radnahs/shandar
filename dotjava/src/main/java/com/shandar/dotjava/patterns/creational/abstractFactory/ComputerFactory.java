/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.patterns.creational.abstractFactory </p>
 * <p>File Name: ComputerFactory.java </p>
 * <p>Create Date: 29-Feb-2020 </p>
 * <p>Create Time: 2:08:26 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.patterns.creational.abstractFactory;


/**
 * @author : Shantanu Sikdar
 * @Description : ComputerFactory
 */
public class ComputerFactory {

	public static Computer getComputer(ComputerAbstractFactory factory) {
		return factory.craeteComputer();
	}
}
