/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.patterns.structural.adapter </p>
 * <p>File Name: SocketClassAdapterImpl.java </p>
 * <p>Create Date: 01-Mar-2020 </p>
 * <p>Create Time: 11:26:26 am </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.patterns.structural.adapter;

/**
 * @author : Shantanu Sikdar
 *
 */
public class SocketClassAdapterImpl extends Socket implements SocketAdapter {

	@Override
	public Volt get120Volt() {
		return getVolt();
	}

	@Override
	public Volt get12Volt() {
		Volt vlt = getVolt();
		return convertVolt(vlt, 10);
	}

	@Override
	public Volt get3Volt() {
		Volt vlt = getVolt();
		return convertVolt(vlt, 40);
	}

	private Volt convertVolt(Volt v, int i) {
		return new Volt(v.getVolts() / i);
	}

}
