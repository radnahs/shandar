/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.patterns.creational.abstractFactory </p>
 * <p>File Name: Computer.java </p>
 * <p>Create Date: 29-Feb-2016 </p>
 * <p>Create Time: 8:51:06 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.patterns.creational.abstractFactory;

/**
 * @author : Shantanu Sikdar
 * @Description : Computer
 */
public abstract class Computer {

	public abstract String getRAM();

	public abstract String getHDD();

	public abstract String getCPU();

	@Override
	public String toString() {
		return "Computer [getRAM()=" + this.getRAM() + ", getHDD()=" + this.getHDD() + ", getCPU()=" + this.getCPU() + "]";
	}

	
	
}
