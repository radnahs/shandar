/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.patterns.creational.singleton </p>
 * <p>File Name: ThreadSafeSingletonDoubleCheckLocking.java </p>
 * <p>Create Date: 27-Feb-2014 </p>
 * <p>Create Time: 12:42:41 am </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.patterns.creational.singleton;

/**
 * @author : Shantanu Sikdar
 * @Description : ThreadSafeSingletonDoubleCheckLocking
 */
public class ThreadSafeSingletonDoubleCheckLocking {

	private static ThreadSafeSingletonDoubleCheckLocking instance;
	
	private ThreadSafeSingletonDoubleCheckLocking(){
	}
	
	public static ThreadSafeSingletonDoubleCheckLocking getInstance() {
		if(instance==null) {
			synchronized(ThreadSafeSingletonDoubleCheckLocking.class) {
				instance = new ThreadSafeSingletonDoubleCheckLocking();
			}
		}
		return instance;
	}
}
