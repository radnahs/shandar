/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.patterns.structural.adapter </p>
 * <p>File Name: TestAdapterPattern.java </p>
 * <p>Create Date: 01-Mar-2020 </p>
 * <p>Create Time: 12:30:12 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.patterns.structural.adapter;

/**
 * @author : Shantanu Sikdar
 *
 */
public class TestAdapterPattern {

	public static void main(String[] args) {
		testObjectAdapter();
	}
	
	private static void testObjectAdapter(){
		SocketAdapter sock = new SocketObjectAdapterImpl();
		Volt v3 = getVolt(sock, 3);
		Volt v12 = getVolt(sock, 12);
		Volt v120 = getVolt(sock, 120);
		System.out.println("v3 volts using Object Adapter= "+v3.getVolts());
		System.out.println("v12 volts using Object Adapter= "+v12.getVolts());
		System.out.println("v120 volts using Object Adapter= "+v120.getVolts());
	}
	
	
	private static Volt getVolt(SocketAdapter sockAdptr, int i){
		 switch (i) {
		case 3:
			return sockAdptr.get3Volt();
		case 12:
			return sockAdptr.get12Volt();
		case 120:
			return sockAdptr.get120Volt();
		default:
			return sockAdptr.get120Volt();
		}
	}

}
