/**
 *
 * <p>Project: trunk_dotjava </p>
 * <p>Package Name: com.shandar.dotjava.patterns </p>
 * <p>File Name: P1_AbstractBrakeThrottleSystem.java</p>
 * <p>Create Date: Nov 8, 2013 </p>
 * <p>Create Time: 6:55:52 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.dotjava.patterns.factory;

/**
 * @author Shantanu Sikdar 
 *
 */
public class Maruti_BrakeThrottleSystem extends AbstractBrakeThrottleSystem{

	/* (non-Javadoc)
	 * @see com.shandar.dotjava.patterns.AbstractBrakeThrottleSystem#getBrakeThrottleSystem()
	 */
	@Override
	public void getBrakeThrottleSystem(String brakeComapny,String throttleComapny) {
				
	}
	
	
	

	/* (non-Javadoc)
	 * @see com.shandar.dotjava.patterns.AbstractBrakeThrottleSystem#getBrake()
	 */
	/*@Override
	public void getBrake(String brakeComapny) {
		BrakingSystem brakingSystem;
		if(brakeComapny.equalsIgnoreCase("Gabriel")){
			brakingSystem = new GabrielBrake();
			brakingSystem.applyBrake();
		}else if(brakeComapny.equalsIgnoreCase("Bosh")){
			brakingSystem = new BoshBrake();
			brakingSystem.applyBrake();
		}else{
			System.out.println("Not available");
		}
	}*/

	/* (non-Javadoc)
	 * @see com.shandar.dotjava.patterns.AbstractBrakeThrottleSystem#getThrottle()
	 */
	/*@Override
	public void getThrottle(String throttleComapny) {
		ThrottleSystem throttleSystem;
		if(throttleComapny.equalsIgnoreCase("Force")){
			throttleSystem = new ForceThrottle();
			throttleSystem.applyThrottle();
		}else if(throttleComapny.equalsIgnoreCase("Telco")){
			throttleSystem = new TelcoThrottle();
			throttleSystem.applyThrottle();
		}else{
			System.out.println("Not available");
		}
		
	}*/

}
