/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.patterns.creational.abstractFactory </p>
 * <p>File Name: TestAbstractFactory.java </p>
 * <p>Create Date: 29-Feb-2016 </p>
 * <p>Create Time: 2:16:59 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.patterns.creational.abstractFactory;

/**
 * @author : Shantanu Sikdar
 * @Description : TestAbstractFactory
 */
public class TestAbstractFactory {

	public static void main(String[] args) {
		testAbstractFactories();
	}
	
	private static void testAbstractFactories() {
		Computer pc = ComputerFactory.getComputer(new PCFactory("8 GB", "1 TB", "2.4 GHz"));
		Computer server = ComputerFactory.getComputer(new ServerFactory("32 GB", "1 TB", "3.6 GHz"));
		System.out.println("AbstractFactory PC Config::"+pc);
		System.out.println("AbstractFactory server Config::"+server);
	
	}

}
