/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.patterns.creational.builder </p>
 * <p>File Name: TestBuilderPattern.java </p>
 * <p>Create Date: 01-Mar-2020 </p>
 * <p>Create Time: 2:11:00 am </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.patterns.creational.builder;

/**
 * @author : Shantanu Sikdar
 *
 */
public class TestBuilderPattern {

	public static void main(String[] args) {
		Computer comp1 = new Computer.ComputerBuilder("1 TB", "8 GB").build();
		System.out.println(comp1);
		Computer comp2 = new Computer.ComputerBuilder("1 TB", "8 GB").setBluetoothEnabled(true).build();
		System.out.println(comp2);
		Computer comp3 = new Computer.ComputerBuilder("1 TB", "8 GB").setGraphicsCardEnabled(true).build();
		System.out.println(comp3);
		Computer comp4 = new Computer.ComputerBuilder("1 TB", "8 GB").setGraphicsCardEnabled(true).setBluetoothEnabled(true).build();
		System.out.println(comp4);
	}

}
