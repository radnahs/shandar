/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.patterns.creational.prototype </p>
 * <p>File Name: TestPrototypeEmployee.java </p>
 * <p>Create Date: 01-Mar-2020 </p>
 * <p>Create Time: 9:11:35 am </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.patterns.creational.prototype;

import java.util.List;

/**
 * @author : Shantanu Sikdar
 *
 */
public class TestPrototypeEmployee {

	public static void main(String[] args) throws CloneNotSupportedException{
		Employees empls = new Employees();
		empls.loadData();
		System.out.println(empls);
		Employees emplsclone1 = (Employees) empls.clone();
		System.out.println(emplsclone1);
		Employees emplsclone2 = (Employees) empls.clone();
		System.out.println(emplsclone2);
		
		emplsclone1.getEmployeeList().add("Dev");
		
		emplsclone2.getEmployeeList().remove("Nandita");
		
		System.out.println(empls.getEmployeeList());
		System.out.println(emplsclone1.getEmployeeList());
		System.out.println(emplsclone2.getEmployeeList());
	}

}
