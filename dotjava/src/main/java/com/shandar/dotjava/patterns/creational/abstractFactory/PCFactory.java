/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.patterns.creational.abstractFactory </p>
 * <p>File Name: PCFactory.java </p>
 * <p>Create Date: 29-Feb-2020 </p>
 * <p>Create Time: 2:01:00 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.patterns.creational.abstractFactory;

/**
 * @author : Shantanu Sikdar
 * @Description : PCFactory
 */
public class PCFactory implements ComputerAbstractFactory {

	private String ram;
	private String hdd;
	private String cpu;

	public PCFactory(String ram, String hdd, String cpu) {
		this.ram = ram;
		this.hdd = hdd;
		this.cpu = cpu;
	}

	@Override
	public Computer craeteComputer() {
		return new PC(ram, hdd, cpu);
	}
	
}
