/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.patterns.creational.singleton </p>
 * <p>File Name: ReflectionSingletonTest.java </p>
 * <p>Create Date: 27-Feb-2016 </p>
 * <p>Create Time: 8:39:23 am </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.patterns.creational.singleton;

import java.lang.reflect.Constructor;

/**
 * @author : Shantanu Sikdar
 * @Description : ReflectionSingletonTest
 */
public class ReflectionSingletonTest {
	
	public static void main(String[] args) {
		ReflectionSingletonTest rst = new ReflectionSingletonTest();
		rst.breakingEagerInitializedSingleton();
		rst.breakingThreadSafeSingletonDoubleCheckLocking();
	}

	private void breakingEagerInitializedSingleton(){
		EagerInitializedSingleton eisInstanceONE = EagerInitializedSingleton.getInstance();
		EagerInitializedSingleton eisInstanceTWO = null;
		try {
			Constructor[] constructors = EagerInitializedSingleton.class.getDeclaredConstructors();
			for (Constructor constructor : constructors) {
				constructor.setAccessible(true);
				eisInstanceTWO = (EagerInitializedSingleton)constructor.newInstance();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println(eisInstanceONE.hashCode());
		System.out.println(eisInstanceTWO.hashCode());
	}
	
	
	private void breakingThreadSafeSingletonDoubleCheckLocking() {
		ThreadSafeSingletonDoubleCheckLocking tssdclInstanceONE = ThreadSafeSingletonDoubleCheckLocking.getInstance();
		ThreadSafeSingletonDoubleCheckLocking tssdclInstanceTWO =null;
		try {
			Constructor[] constructors = ThreadSafeSingletonDoubleCheckLocking.class.getDeclaredConstructors();
			for (Constructor constructor : constructors) {
				constructor.setAccessible(true);
				tssdclInstanceTWO =  (ThreadSafeSingletonDoubleCheckLocking)constructor.newInstance();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println(tssdclInstanceONE.hashCode());
		System.out.println(tssdclInstanceTWO.hashCode());

	}
	
	//incomplete
	private void breakingBillPughSingleton() {
		BillPughSingleton bpsInstanceONE = BillPughSingleton.getInstance();
		BillPughSingleton bpsInstanceTWO = null;
		try {
			Constructor[] constructors = BillPughSingleton.class.getDeclaredConstructors();
		} catch (Exception e) {
		}
		System.out.println(bpsInstanceONE.hashCode());
		System.out.println(bpsInstanceTWO.hashCode());
	}
	
	
}
