/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.patterns.creational.abstractFactory </p>
 * <p>File Name: ComputerAbstractFactory.java </p>
 * <p>Create Date: 29-Feb-2016 </p>
 * <p>Create Time: 1:58:36 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.patterns.creational.abstractFactory;

/**
 * @author : Shantanu Sikdar
 * @Description : ComputerAbstractFactory
 */
public interface ComputerAbstractFactory {

	public Computer craeteComputer();

}
