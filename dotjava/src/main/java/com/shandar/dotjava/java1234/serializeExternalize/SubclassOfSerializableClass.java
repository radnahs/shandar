/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.java1234.serializeExternalize </p>
 * <p>File Name: SubclassOfSerializableClass.java </p>
 * <p>Create Date: 21-Feb-2020 </p>
 * <p>Create Time: 10:21:35 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.java1234.serializeExternalize;

/**
 * @author : Shantanu Sikdar
 * @Description : SubclassOfSerializableClass
 */
public class SubclassOfSerializableClass extends SerializableClass {

	private int a;
	private String str;

	public int getA() {
		return a;
	}
	public void setA(int a) {
		this.a = a;
	}
	
	public String getStr() {
		return str;
	}
	public void setStr(String str) {
		this.str = str;
	}
	
}
