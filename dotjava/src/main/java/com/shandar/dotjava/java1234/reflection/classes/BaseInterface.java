/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.java1234.reflection.classes </p>
 * <p>File Name: BaseInterface.java </p>
 * <p>Create Date: 08-Nov-2016 </p>
 * <p>Create Time: 12:46:21 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.java1234.reflection.classes;

/**
 * @author : Shantanu Sikdar
 * @Description : BaseInterface
 */
public interface BaseInterface {
	public int interfaceInt = 0;

	void method1();

	int method2(String str);
}
