/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.java1234.serializeExternalize </p>
 * <p>File Name: SerializationExample.java </p>
 * <p>Create Date: Feb 20, 2014 </p>
 * <p>Create Time: 2:32:55 PM </p>
 * <p>Copyright: Copyright (c) 2014</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.dotjava.java1234.serializeExternalize;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * @author Shantanu Sikdar
 *
 */
public class SerializationExample {

	public static void main(String[] args) {
		SerializationExample serExmpl = new SerializationExample();
		SerializableClass serializeClassObject = new SerializableClass();
		
		String fileName = "E:\\data\\project\\shandar\\dotjava\\src\\main\\java\\com\\shandar\\dotjava\\java1234\\serializeExternalize\\SerializeClass.ser";
		//serExmpl.serailizeObject(serializeClassObject, fileName);
		serExmpl.deserializeObject(fileName);
	}
	
	private SerializableClass createUsingSetter(){
		SerializableClass serializeClassObject = new SerializableClass();
		serializeClassObject.setA(100);
		serializeClassObject.setStr("Value 100");
		serializeClassObject.setStrTransient("Transient Value 100");
		serializeClassObject.setStrStatic("Static Value 100");
		return serializeClassObject;
	}
	
	private SerializableClass createUsingConstructor(){
		SerializableClass serializeClassObject = new SerializableClass();
		
		
		return serializeClassObject;
	}
	
	private void serailizeObject(SerializableClass serializeClassObject,String fileName){
		try {
			FileOutputStream fos = new FileOutputStream(fileName);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(serializeClassObject);
			fos.close();
			oos.close();
		}catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void deserializeObject(String fileName) {
		SerializableClass deserializeClassObject = null;
		try {
			FileInputStream fis = new FileInputStream(fileName);
			ObjectInputStream ois = new ObjectInputStream(fis);
			deserializeClassObject = (SerializableClass) ois.readObject();
			fis.close();
			ois.close();
			System.out.println(deserializeClassObject.getA());
			System.out.println(deserializeClassObject.getStr());
			
		}catch (IOException e) {
			e.printStackTrace();
		}catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
}
