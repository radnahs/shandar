/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.java1234.reflection.classes </p>
 * <p>File Name: BaseClass.java </p>
 * <p>Create Date: 08-Nov-2016 </p>
 * <p>Create Time: 12:44:40 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.java1234.reflection.classes;

/**
 * @author : Shantanu Sikdar
 * @Description : BaseClass
 */
public class BaseClass {
	
	public int baseInt;

	private static void method3() {
		System.out.println("Method3");
	}

	public int method4() {
		System.out.println("Method4");
		return 0;
	}

	public static int method5() {
		System.out.println("Method5");
		return 0;
	}

	void method6() {
		System.out.println("Method6");
	}

	// inner public class
	public class BaseClassInnerClass {
	}

	// member public enum
	public enum BaseClassMemberEnum {
	}


}
