/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.java1234.reflection.classes </p>
 * <p>File Name: MainClass.java </p>
 * <p>Create Date: 08-Nov-2016 </p>
 * <p>Create Time: 12:53:36 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.java1234.reflection.classes;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.Arrays;

/**
 * @author : Shantanu Sikdar
 * @Description : MainClass
 */
public class MainClass {

	public static void main(String[] args) {

		// Get Class using reflection
		Class<?> concreteClass = ConcreteClass.class;
		concreteClass = new ConcreteClass(5).getClass();
		try {
			concreteClass = Class.forName("com.shandar.dotjava.java1234.reflection.classes.ConcreteClass");
		} catch (ClassNotFoundException cnfe) {
			cnfe.printStackTrace();
		}
		System.out.println("1. " + concreteClass.getCanonicalName());// com.shandar.dotjava.java1234.reflection.classes.ConcreteClass

		// for primitive types, wrapper classes and arrays
		Class<?> booleanClass = boolean.class;
		System.out.println("2. " + booleanClass.getCanonicalName());
		System.out.println("3. " + Double.TYPE.getCanonicalName());
		try {
			System.out.println("4. " + Class.forName("[D").getCanonicalName());
		} catch (ClassNotFoundException cnfe) {
			cnfe.printStackTrace();
		}
		Class<?> twoDStringArray = String[][].class;
		System.out.println("5. " + twoDStringArray.getCanonicalName());

		try {
			Class<?> superClass = Class.forName("com.shandar.dotjava.java1234.reflection.classes.ConcreteClass")
					.getSuperclass();
			System.out.println("6. " + superClass);
		} catch (ClassNotFoundException cnfe) {
			cnfe.printStackTrace();
		}
		System.out.println("7. " + Object.class.getSuperclass());
		System.out.println("8. " + String[][].class.getSuperclass());

		Class<?>[] classes = concreteClass.getClasses();
		System.out.println("9. " + Arrays.toString(classes));

		try {
			Class<?>[] explicitClasses = Class.forName("com.shandar.dotjava.java1234.reflection.classes.ConcreteClass").getDeclaredClasses();
			System.out.println("10. " + Arrays.toString(explicitClasses));
		} catch (ClassNotFoundException cnfe) {
			cnfe.printStackTrace();
		}

		// Get Declaring Class 
		getDeclaringClass();

		// Getting Package Name
		getPackageName();

		// Getting Class Modifiers
		getClassModifiers(concreteClass);
		
		//Get Type Parameters
		getTypeParameters();
		
		//Get Implemented Interfaces
		getImplementedInterfaces();
		
	}
	
	// Get Declaring Class
	public static void getDeclaringClass() {
		try {
			Class<?> innerClass = Class.forName("com.shandar.dotjava.java1234.reflection.classes.ConcreteClass$ConcreteClassDefaultClass");
			System.out.println("11. " + innerClass.getDeclaringClass().getCanonicalName());
			System.out.println("12. " + innerClass.getEnclosingClass().getCanonicalName());
		} catch (ClassNotFoundException cnfe) {
			cnfe.printStackTrace();
		}
	}
	
	// Getting Package Name
	public static void getPackageName() {
		try {
			System.out.println("13. " + Class.forName("com.shandar.dotjava.java1234.reflection.classes.ConcreteClass").getPackage());
			System.out.println("14. " + Class.forName("com.shandar.dotjava.java1234.reflection.classes.ConcreteClass").getPackage().getName());
		} catch (ClassNotFoundException cnfe) {
			cnfe.printStackTrace();
		}
	}
	
	// Getting Class Modifiers
	public static void getClassModifiers(Class<?> concreteClass) {
		System.out.println("15. " + Modifier.toString(concreteClass.getModifiers()));
		try {
			System.out.println("16. " + Modifier.toString(Class.forName("com.shandar.dotjava.java1234.reflection.classes.BaseInterface").getModifiers()));
		} catch (ClassNotFoundException cnfe) {
			cnfe.printStackTrace();
		}
	}
	
	//Get Type Parameters
	public static void getTypeParameters() {
		try {
			TypeVariable<?>[] typeParameters = Class.forName("java.util.HashMap").getTypeParameters();
			for (TypeVariable<?> typeVariable : typeParameters) {
				System.out.println("17. " + typeVariable);
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	//Get Implemented Interfaces
	public static void getImplementedInterfaces() {
		try {
			Type[] interfaces = Class.forName("java.util.HashMap").getGenericInterfaces();
			System.out.println("18. " + Arrays.toString(interfaces));
			System.out.println("19. " + Arrays.toString(Class.forName("java.util.HashMap").getInterfaces()));
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	//Get All Public Methods
	public static void getAllPublicMethods() {
		try {
			Method[] publicMethods = Class.forName("com.shandar.dotjava.java1234.reflection.classes.ConcreteClass").getMethods();
			
		} catch (SecurityException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		
	}
	

}
