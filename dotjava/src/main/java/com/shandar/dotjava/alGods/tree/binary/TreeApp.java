/**
 * <p>Project: alGods </p>
 * <p>Package Name: com.shandar.dotjava.alGods.tree.binary </p>
 * <p>File Name: TreeApp.java </p>
 * <p>Create Date: Oct 30, 2019 </p>
 * <p>Create Time: 2:14:23 PM </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.dotjava.alGods.tree.binary;

/**
 * @author Shantanu Sikdar
 * @Deescription: TreeApp.java 
 */
public class TreeApp {

	public static void main(String[] args) {
		
	}	
	
}

class Node{
	public int iData;
	public double dData;
	public Node leftChild;
	public Node rightChild;
	
	public void displayNode() {
		System.out.println("{ "+iData+" , "+ dData+"}");
	}
}
class Tree{
	private Node root;

	/**
	 * @param root
	 */
	public Tree(Node root) {
		this.root = null;
	}
	
	
	
}
