/**
 * <p>Project: alGods </p>
 * <p>Package Name: com.shandar.dotjava.alGods.sortings </p>
 * <p>File Name: ObjectInsertionSortApp.java </p>
 * <p>Create Date: Oct 23, 2017 </p>
 * <p>Create Time: 4:13:43 PM </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.dotjava.alGods.sortings;

/**
 * @author Shantanu Sikdar
 * @Deescription: ObjectInsertionSortApp.java
 */
public class ObjectInsertionSortApp {

	public static void main(String[] args) {

		ArrayInObj insertionArray = new ArrayInObj(5);
		insertionArray.insert("shan5", "dar5", 41);
		insertionArray.insert("shan1", "dar1", 37);
		insertionArray.insert("shan4", "dar4", 40);
		insertionArray.insert("shan3", "dar3", 39);
		insertionArray.insert("shan2", "dar2", 38);

		System.out.println("before sorting");
		insertionArray.display();

		insertionArray.insertionSortObj();

		System.out.println("after sorting");
		insertionArray.display();

	}

}

class Person {
	private String firstName;
	private String lastName;
	private int age;

	public Person(String firstName, String lastName, int age) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
	}

	public void displayPerson() {
		System.out.print(" Last Name : " + lastName);
		System.out.print(", First Name : " + firstName);
		System.out.println(", Age : " + age);
	}

	public String getLastName() {
		return lastName;
	}

}

class ArrayInObj {
	private Person[] persons;
	private int nElems;

	/**
	 * @param persons
	 * @param nElems
	 */
	public ArrayInObj(int max) {
		persons = new Person[max];
		nElems = 0;
	}

	public void insert(String fName, String lName, int age) {
		persons[nElems] = new Person(fName, lName, age);
		nElems++;
	}

	public void display() {
		for (Person prsn : persons) {
			prsn.displayPerson();
		}
		System.out.println();
	}

	public void insertionSortObj() {
		int in, out;
		for (out = 1; out < nElems; out++) {
			Person pTemp = persons[out];
			in = out;
			while (in > 0 && persons[in - 1].getLastName().compareTo(pTemp.getLastName()) > 0) {
				persons[in] = persons[in - 1];
				--in;
			}
			persons[in] = pTemp;
		}
	}

}
