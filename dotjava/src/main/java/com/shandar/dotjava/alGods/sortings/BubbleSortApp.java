/**
 * <p>Project: alGods </p>
 * <p>Package Name: com.shandar.dotjava.alGods.sortings </p>
 * <p>File Name: BubbleSortApp.java </p>
 * <p>Create Date: Oct 23, 2017 </p>
 * <p>Create Time: 2:37:51 PM </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.dotjava.alGods.sortings;

/**
 * @author Shantanu Sikdar
 * @Deescription: BubbleSortApp.java
 */
public class BubbleSortApp {

	private long[] a;
	private int nElems;

	public BubbleSortApp(int max) {
		a = new long[max];
		nElems = 0;
	}

	public void insert(long value) {
		a[nElems] = value;
		nElems++;
	}

	public void display() {
		for (long l : a) {
			System.out.print(l + " ");
		}
		System.out.println();
	}

	public void bubbleSort() {
		int out, in;
		for (out = 0; out < nElems; out++) {
			for (in = nElems - 1; in > out; in--) {
				if (a[out] > a[in]) {
					long temp = a[out];
					a[out] = a[in];
					a[in] = temp;
				}
			}
		}
	}
	
	public static void main(String[] args) {
		BubbleSortApp bubbleArray = new BubbleSortApp(10);
		bubbleArray.insert(77);
		bubbleArray.insert(55);
		bubbleArray.insert(22);
		bubbleArray.insert(99);
		bubbleArray.insert(44);
		bubbleArray.insert(88);
		bubbleArray.insert(11);
		bubbleArray.insert(00);
		bubbleArray.insert(66);
		bubbleArray.insert(33);
		bubbleArray.display();
		bubbleArray.bubbleSort();
		bubbleArray.display();
	}

}

