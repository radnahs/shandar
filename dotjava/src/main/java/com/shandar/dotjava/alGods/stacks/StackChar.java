/**
 * <p>Project: alGods </p>
 * <p>Package Name: com.shandar.dotjava.alGods.stacks </p>
 * <p>File Name: StackChar.java </p>
 * <p>Create Date: Oct 23, 2017 </p>
 * <p>Create Time: 9:42:43 PM </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.dotjava.alGods.stacks;

/**
 * @author Shantanu Sikdar
 * @Deescription: StackChar.java
 */
public class StackChar {

	private int maxSize;
	private char[] stackCharArr;
	private int top;

	public StackChar(int max) {
		maxSize = max;
		stackCharArr = new char[maxSize];
		top = -1;
	}

	public void push(char chrVar) {
		stackCharArr[++top] = chrVar;
	}

	public char pop() {
		return stackCharArr[top--];
	}

	public char peek() {
		return stackCharArr[top];
	}

	public boolean isEmpty() {
		return top == -1;
	}

	public boolean isFull() {
		return top == maxSize;
	}

}
