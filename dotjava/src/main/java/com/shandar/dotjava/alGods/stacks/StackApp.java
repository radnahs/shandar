/**
 * <p>Project: alGods </p>
 * <p>Package Name: com.shandar.dotjava.alGods.stacks </p>
 * <p>File Name: StackApp.java </p>
 * <p>Create Date: Oct 23, 2017 </p>
 * <p>Create Time: 6:24:30 PM </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.dotjava.alGods.stacks;

/**
 * @author Shantanu Sikdar
 * @Deescription: StackApp.java
 */
public class StackApp {

	public static void main(String[] args) {

		StackNumber theStack = new StackNumber(10);
		theStack.push(99);
		theStack.push(33);
		theStack.push(22);
		theStack.push(88);
		theStack.push(99);
		theStack.push(22);

		while (!theStack.isEmpty()) {
			System.out.print(theStack.pop() + "	");
		}
		System.out.println("ends");
	}
}

class StackNumber {

	private int maxSize;
	private long[] stackArray;
	private int top;

	/**
	 * @param max
	 */
	public StackNumber(int max) {
		maxSize = max;
		stackArray = new long[maxSize];
		this.top = -1;
	}

	public void push(long value) {
		stackArray[++top] = value;
	}

	public long pop() {
		return stackArray[top--];
	}

	public long peek() {
		return stackArray[top];
	}

	public boolean isEmpty() {
		return top == -1;
	}

	public boolean isFull() {
		return top == maxSize - 1;
	}

}
