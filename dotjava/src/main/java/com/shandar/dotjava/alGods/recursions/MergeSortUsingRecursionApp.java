/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.alGods.recursions </p>
 * <p>File Name: MergeSortUsingRecursionApp.java </p>
 * <p>Create Date: 29-Oct-2014 </p>
 * <p>Create Time: 10:30:25 am </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.alGods.recursions;

/**
 * @author : Shantanu Sikdar
 * @Description : MergeSortUsingRecursionApp
 */
public class MergeSortUsingRecursionApp {

	public static void main(String[] args) {
	}


	public static void display(int[] theArray) {
		for (int j = 0; j < theArray.length; j++) {
			System.out.print(theArray[j] + " ");
		}
		System.out.println();
	}

}
