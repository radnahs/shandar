/**
 * <p>Project: alGods </p>
 * <p>Package Name: com.shandar.dotjava.alGods.queues </p>
 * <p>File Name: QueueAppV2.java </p>
 * <p>Create Date: Oct 24, 2015 </p>
 * <p>Create Time: 9:16:38 PM </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.dotjava.alGods.queues;

/**
 * @author Shantanu Sikdar
 * @Deescription: QueueAppV2.java
 */
public class QueueAppV2 {

	public static void main(String[] args) {

		QueueNumberV2 theQueueV2 = new QueueNumberV2(5);
		theQueueV2.insert(11);
		theQueueV2.insert(22);
		theQueueV2.insert(33);
		theQueueV2.insert(44);

		theQueueV2.remove();
		theQueueV2.remove();
		theQueueV2.remove();

		theQueueV2.insert(55);
		theQueueV2.insert(66);
		theQueueV2.insert(77);
		theQueueV2.insert(88);

		while (!theQueueV2.isEmpty()) {
			long n = theQueueV2.remove();
			System.out.print(n + " ");
		}
		System.out.println();

	}

}

class QueueNumberV2 {

	private int maxSize;
	private long[] queueArray;
	private int front;
	private int rear;

	public QueueNumberV2(int max) {
		this.maxSize = max + 1;
		this.queueArray = new long[maxSize];
		this.front = 0;
		this.rear = -1;
	}

	public void insert(long value) {
		if (rear == maxSize - 1) {
			rear = -1;
		}
		queueArray[++rear] = value;
	}

	public long remove() {
		long temp = queueArray[front++];
		if (front == maxSize) {
			front = 0;
		}
		return temp;
	}

	public long peekFront() {
		return queueArray[front];
	}

	public boolean isEmpty() {
		return (front == rear + 1 || front + maxSize - 1 == rear);
	}

	public boolean isFull() {
		return (front == rear + 2 || front + maxSize - 2 == rear);
	}

	public int size() {
		if (rear >= front) {
			return rear - front + 1;
		} else {
			return maxSize - front + rear + 1;
		}
	}

}