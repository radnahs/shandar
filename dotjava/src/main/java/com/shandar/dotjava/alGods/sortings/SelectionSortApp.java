/**
 * <p>Project: alGods </p>
 * <p>Package Name: com.shandar.dotjava.alGods.sortings </p>
 * <p>File Name: SelectionSortApp.java </p>
 * <p>Create Date: Oct 23, 2019 </p>
 * <p>Create Time: 2:49:38 PM </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.dotjava.alGods.sortings;

/**
 * @author Shantanu Sikdar
 * @Deescription: SelectionSortApp.java
 */
public class SelectionSortApp {

	private long[] a;
	private int nElems;

	public SelectionSortApp(int max) {
		a = new long[max];
		nElems = 0;
	}

	public void insert(long value) {
		a[nElems] = value;
		nElems++;
	}

	public void display() {
		for (long l : a) {
			System.out.print(l + " ");
		}
		System.out.println();
	}

	public void selectionSort() {
		int out, in, min;
		for (out = 0; out < nElems - 1; out++) {
			min = out;
			for (in = out + 1; in < nElems; in++) {
				if (a[in] < a[min]) {
					min = in;
				}
			}
			swap(out, min);
		}
	}

	private void swap(int one, int two) {
		long tmp = a[one];
		a[one] = a[two];
		a[two] = tmp;
	}
	
	public static void main(String[] args) {
		SelectionSortApp selectionArray = new SelectionSortApp(10);
		selectionArray.insert(77);
		selectionArray.insert(55);
		selectionArray.insert(22);
		selectionArray.insert(99);
		selectionArray.insert(44);
		selectionArray.insert(88);
		selectionArray.insert(11);
		selectionArray.insert(00);
		selectionArray.insert(66);
		selectionArray.insert(33);
		selectionArray.display();
		selectionArray.selectionSort();
		selectionArray.display();
	}

}
