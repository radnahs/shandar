/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.alGods.recursions </p>
 * <p>File Name: TriangularNumbersApp.java </p>
 * <p>Create Date: 28-Oct-2014 </p>
 * <p>Create Time: 5:30:15 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.alGods.recursions;

/**
 * @author : Shantanu Sikdar
 * @Description : TriangularNumbersApp <br>
 * 					The nth term in the series is obtained by adding n to the
 * 					previous term. Thus, the second term is found by adding 2
 * 					to the first term (which is 1), giving 3. The third term is 3
 * 					added to the second term (which is 3) giving 6, and so on.
 * 					the series of numbers 1, 3, 6, 10, 15, 21, …
 */
public class TriangularNumbersApp {

	public static void main(String[] args) {
		TriangularNumbersApp triangularNumbers = new TriangularNumbersApp();
		System.out.println(triangularNumbers.triangularNumberRecursively(6));
		System.out.println(triangularNumbers.triangularNumberForLoopwise(6));
		System.out.println(triangularNumbers.triangularNumberWhileLoopwise(6));
	}
	
	public int triangularNumberRecursively(int n) {
		if(n==1) {
			return 1;
		}else {
			return (n + triangularNumberRecursively(n-1));
		}
	}

	public int triangularNumberForLoopwise(int n) {
		int temp = 0;
		for(int i=1; i<=n; i++) {
			temp += i;
		}
		return temp;
	}

	public int triangularNumberWhileLoopwise(int n) {
		int temp= 0,i = 0;
		while(i<n) {
			temp += ++i;
		}
		return temp;
	}

}
