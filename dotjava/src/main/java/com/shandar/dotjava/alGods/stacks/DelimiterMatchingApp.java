/**
 * <p>Project: alGods </p>
 * <p>Package Name: com.shandar.dotjava.alGods.stacks </p>
 * <p>File Name: DelimiterMatchingApp.java </p>
 * <p>Create Date: Oct 23, 2017 </p>
 * <p>Create Time: 9:40:29 PM </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.dotjava.alGods.stacks;

/**
 * @author Shantanu Sikdar
 * @Deescription: DelimiterMatchingApp.java
 */
public class DelimiterMatchingApp {

	public static void main(String[] args) {
		DelimiterChecker delimiterChecker = new DelimiterChecker("a{b(c]d}e");
	}

}

class DelimiterChecker {

	private String input;

	public DelimiterChecker(String in) {
		this.input = in;
	}

	public void check() {
		int stackSize = input.length();
		StackChar stackChr = new StackChar(stackSize);

		for (int i = 0; i < input.length(); i++) {
			char chr = input.charAt(i);
			switch (chr) {
			case '{':
			case '[':
			case '(':
				stackChr.push(chr);
				break;
			case '}':
			case ']':
			case ')':
				if (!stackChr.isEmpty()) {
					char chrx = stackChr.pop();
					if ((chr == '}' && chrx != '{') || (chr == ']' && chrx != '[') || (chr == ')' && chrx != '(')) {
						System.out.println("Error " + chr + " at " + i);
					}
				} else {
					System.out.println("Error " + chr + " at " + i);
				}
				break;
			default:
				break;

			}
		}

		if (!stackChr.isEmpty()) {
			System.out.println("missing right delimiter");
		}
	}

}