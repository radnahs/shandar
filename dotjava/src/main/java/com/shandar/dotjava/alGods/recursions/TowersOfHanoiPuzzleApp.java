/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.alGods.recursions </p>
 * <p>File Name: TowersOfHanoiPuzzleApp.java </p>
 * <p>Create Date: 29-Oct-2014 </p>
 * <p>Create Time: 12:31:36 am </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.alGods.recursions;

/**
 * @author : Shantanu Sikdar
 * @Description : TowersOfHanoiPuzzleApp <br>
 *              The disks all have different diameters and holes in the middle
 *              so they will fit over the columns. All the disks start out on
 *              column A. The object of the puzzle is to transfer all the disks
 *              from column A to column C. Only one disk can be moved at a time,
 *              and no disk can be placed on a disk that’s smaller than itself.
 */
public class TowersOfHanoiPuzzleApp {

	public static void main(String[] args) {
		doTowers(3, 'A', 'B', 'C');
	}

	public static void doTowers(int topN, char from, char inter, char to) {
		if (topN == 1) {
			System.out.println("Disk 1 from " + from + " to " + to);
		} else {
			doTowers(topN - 1, from, to, inter);
			System.out.println("Disk " + topN + " from " + from + " to " + to);
			doTowers(topN - 1, inter, from, to);
		}
	}

}
