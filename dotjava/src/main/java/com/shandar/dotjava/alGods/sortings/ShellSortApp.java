/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.alGods.sortings </p>
 * <p>File Name: ShellSortApp.java </p>
 * <p>Create Date: 29-Oct-2019 </p>
 * <p>Create Time: 12:01:39 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.alGods.sortings;

/**
 * @author : Shantanu Sikdar
 * @Description : ShellSortApp
 */
public class ShellSortApp {

	private long[] a;
	private int nElems;

	public ShellSortApp(int max) {
		a = new long[max];
		nElems = 0;
	}

	public void insert(long value) {
		a[nElems] = value;
		nElems++;
	}

	public void display() {
		for (long l : a) {
			System.out.print(l + " ");
		}
		System.out.println();
	}

	public void shellSort() {
		int inner, outer;
		long temp;
		int h = 1;

		while (h <= nElems / 3) {
			h = (h * 3) + 1;
		}

		while (h > 0) {
			for (outer = h; outer < nElems; outer++) {
				temp = a[outer];
				inner = outer;
				while (inner > h - 1 && a[inner - h] >= temp) {
					a[inner] = a[inner - h];
					inner -= h;
				}
				a[inner] = temp;
			}
			h = (h - 1) / 3;
		}
	}

	public static void main(String[] args) {
		int maxSize = 10;
		ShellSortApp theSSArray = new ShellSortApp(maxSize);
		for (int j = 0; j < maxSize; j++) {
			long n = (int) (java.lang.Math.random() * 99);
			theSSArray.insert(n);
		}
		theSSArray.display();
		theSSArray.shellSort();
		theSSArray.display();
	}

}
