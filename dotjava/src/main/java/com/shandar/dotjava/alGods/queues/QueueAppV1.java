/**
 * <p>Project: alGods </p>
 * <p>Package Name: com.shandar.dotjava.alGods.queues </p>
 * <p>File Name: QueueAppV1.java </p>
 * <p>Create Date: Oct 23, 2017 </p>
 * <p>Create Time: 10:11:22 PM </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.dotjava.alGods.queues;

/**
 * @author Shantanu Sikdar
 * @Deescription: QueueAppV1.java
 */
public class QueueAppV1 {

	public static void main(String[] args) {
		QueueNumberV1 theQueueV1 = new QueueNumberV1(5);
		theQueueV1.insert(11);
		theQueueV1.insert(22);
		theQueueV1.insert(33);
		theQueueV1.insert(44);

		theQueueV1.remove();
		theQueueV1.remove();
		theQueueV1.remove();

		theQueueV1.insert(55);
		theQueueV1.insert(66);
		theQueueV1.insert(77);
		theQueueV1.insert(88);

		while (!theQueueV1.isEmpty()) {
			long n = theQueueV1.remove();
			System.out.print(n + " ");
		}
		System.out.println();

	}

}

class QueueNumberV1 {

	private int maxSize;
	private long[] queueArray;
	private int front;
	private int rear;
	private int nItems;

	public QueueNumberV1(int max) {
		this.maxSize = max;
		this.queueArray = new long[maxSize];
		this.front = 0;
		this.rear = -1;
		this.nItems = 0;
	}

	public void insert(long value) {
		if (rear == maxSize - 1) {
			rear = -1;
		}
		queueArray[++rear] = value;
		nItems++;
	}

	public long remove() {
		long temp = queueArray[front++];
		if (front == maxSize) {
			front = 0;
		}
		nItems--;
		return temp;
	}

	public long peekFront() {
		return queueArray[front];
	}

	public boolean isEmpty() {
		return nItems == 0;
	}

	public boolean isFull() {
		return nItems == maxSize;
	}

	public int size() {
		return nItems;
	}

}
