/**
 * <p>Project: alGods </p>
 * <p>Package Name: com.shandar.dotjava.alGods.stacks </p>
 * <p>File Name: ReverseAStringApp.java </p>
 * <p>Create Date: Oct 23, 2017 </p>
 * <p>Create Time: 9:03:15 PM </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.dotjava.alGods.stacks;

/**
 * @author Shantanu Sikdar
 * @Deescription: ReverseAStringApp.java
 */
public class ReverseAStringApp {

	public static void main(String[] args) {
		Reverser reverser = new Reverser("Shantanu is a Good google employee");
		System.out.println(reverser.doReversal());
	}
}

class Reverser {
	private String input;
	private String output;

	public Reverser(String in) {
		this.input = in;
	}

	public String doReversal() {
		int stackSize = input.length();
		StackChar stackChar = new StackChar(stackSize);
		for (int i = 0; i < stackSize; i++) {
			char chr = input.charAt(i);
			stackChar.push(chr);
		}
		output = "";
		while (!stackChar.isEmpty()) {
			char chr = stackChar.pop();
			output = output + chr;
		}
		return output;
	}

}