/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.alGods.sortings </p>
 * <p>File Name: Radix.java </p>
 * <p>Create Date: 29-Oct-2017 </p>
 * <p>Create Time: 2:00:09 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.alGods.sortings;

import java.util.Arrays;

/**
 * @author : Shantanu Sikdar
 * @Description : Radix
 * 
 *              https://www.geeksforgeeks.org/java-program-for-radix-sort/
 */
public class Radix {

	// A utility function to get maximum value in arr[]
	int getMax(int arr[], int n) {
		int mx = arr[0];
		for (int i = 1; i < n; i++)
			if (arr[i] > mx)
				mx = arr[i];
		return mx;
	}

	// A function to do counting sort of arr[] according to
	// the digit represented by exp.
	void countSort(int arr[], int n, int exp) {
		int output[] = new int[n]; // output array
		int i;
		int count[] = new int[10];
		Arrays.fill(count, 0);

		// Store count of occurrences in count[]
		for (i = 0; i < n; i++)
			count[(arr[i] / exp) % 10]++;

		// Change count[i] so that count[i] now contains
		// actual position of this digit in output[]
		for (i = 1; i < 10; i++)
			count[i] += count[i - 1];

		// Build the output array
		for (i = n - 1; i >= 0; i--) {
			output[count[(arr[i] / exp) % 10] - 1] = arr[i];
			count[(arr[i] / exp) % 10]--;
		}

		// Copy the output array to arr[], so that arr[] now
		// contains sorted numbers according to current digit
		for (i = 0; i < n; i++) {
			arr[i] = output[i];
		}
	}

	// The main function to that sorts arr[] of size n using
	// Radix Sort
	void radixSort(int arr[]) {
		int n = arr.length;
		// Find the maximum number to know number of digits
		int m = getMax(arr, n);

		// Do counting sort for every digit. Note that instead
		// of passing digit number, exp is passed. exp is 10^i
		// where i is current digit number
		for (int exp = 1; m / exp > 0; exp *= 10) {
			countSort(arr, n, exp);
		}
	}

	// A utility function to print an array
	void dispalyArray(int arr[]) {
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}

	/* Driver function to check for above function */
	public static void main(String[] args) {
		Radix radix = new Radix();
		int arr[] = { 170, 45, 75, 90, 802, 24, 2, 66 };
		int n = arr.length;
		radix.dispalyArray(arr);
		radix.radixSort(arr);
		radix.dispalyArray(arr);
	}

}
