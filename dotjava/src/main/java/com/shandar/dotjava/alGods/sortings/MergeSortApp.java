/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.alGods.recursions </p>
 * <p>File Name: MergeSortApp.java </p>
 * <p>Create Date: 29-Oct-2014 </p>
 * <p>Create Time: 2:06:48 am </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.alGods.sortings;

/**
 * @author : Shantanu Sikdar
 * @Description : MergeSortApp
 */
public class MergeSortApp {

	public void merge(int[] arrayA, int[] arrayB, int[] arrayC) {
		int sizeA = arrayA.length, sizeB = arrayB.length;
		int aDex = 0, bDex = 0, cDex = 0;

		while (aDex < sizeA && bDex < sizeB) {
			if (arrayA[aDex] < arrayB[bDex]) {
				arrayC[cDex++] = arrayA[aDex++];
			} else {
				arrayC[cDex++] = arrayB[bDex++];
			}
		}

		while (aDex < sizeA) {
			arrayC[cDex++] = arrayA[aDex++];
		}

		while (bDex < sizeA) {
			arrayC[cDex++] = arrayA[bDex++];
		}
	}

	public void display(int[] theArray) {
		for (int j = 0; j < theArray.length; j++) {
			System.out.print(theArray[j] + " ");
		}
		System.out.println();
	}
	
	public static void main(String[] args) {
		MergeSortApp mergeSort = new MergeSortApp();
		int[] arrayA = { 23, 47, 81, 95 };
		int[] arrayB = { 7, 14, 39, 55, 62, 74 };
		int[] arrayC = new int[10];
		mergeSort.merge(arrayA, arrayB, arrayC);
		mergeSort.display(arrayC);
	}
}
