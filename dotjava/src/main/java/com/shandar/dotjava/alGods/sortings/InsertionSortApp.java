/**
 * <p>Project: alGods </p>
 * <p>Package Name: com.shandar.dotjava.alGods.sortings </p>
 * <p>File Name: InsertionSortApp.java </p>
 * <p>Create Date: Oct 23, 2015 </p>
 * <p>Create Time: 2:08:04 PM </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.dotjava.alGods.sortings;

/**
 * @author Shantanu Sikdar
 * @Deescription: InsertionSortApp.java
 */
public class InsertionSortApp {

	private long[] a;
	private int nElems;

	public InsertionSortApp(int max) {
		a = new long[max];
		nElems = 0;
	}

	public void insert(long value) {
		a[nElems] = value;
		nElems++;
	}

	public void display() {
		for (long l : a) {
			System.out.print(l + " ");
		}
		System.out.println();
	}

	public void insertionSort() {
		int out, in;
		for (out = 1; out < nElems; out++) {
			long temp = a[out];
			in = out;
			while (in > 0 && a[in - 1] >= temp) {
				a[in] = a[in - 1];
				--in;
			}
			a[in] = temp;
		}
	}
	
	public static void main(String[] args) {
		InsertionSortApp insertionArray = new InsertionSortApp(10);
		insertionArray.insert(77);
		insertionArray.insert(55);
		insertionArray.insert(22);
		insertionArray.insert(99);
		insertionArray.insert(44);
		insertionArray.insert(88);
		insertionArray.insert(11);
		insertionArray.insert(00);
		insertionArray.insert(66);
		insertionArray.insert(33);
		insertionArray.display();
		insertionArray.insertionSort();
		insertionArray.display();
	}

}

class InsertionSortArray {



}
