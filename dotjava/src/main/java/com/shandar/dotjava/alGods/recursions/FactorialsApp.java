/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.alGods.recursions </p>
 * <p>File Name: FactorialsApp.java </p>
 * <p>Create Date: 28-Oct-2014 </p>
 * <p>Create Time: 6:13:51 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.alGods.recursions;

/**
 * @author : Shantanu Sikdar
 * @Description : FactorialsApp
 */
public class FactorialsApp {

	public static void main(String[] args) {
		FactorialsApp factorialsApp = new FactorialsApp();
		System.out.println(factorialsApp.factorialsRecursively(8));

		System.out.println(factorialsApp.factorialsForLoopwise(8));
		System.out.println(factorialsApp.factorialsWhileLoopwise(8));

	}

	public int factorialsRecursively(int n) {
		if (n == 1) {
			return 1;
		} else {
			return n * factorialsRecursively(--n);
		}
	}

	public int factorialsForLoopwise(int n) {
		int temp = 1;
		for (int i = 1; i <= n; i++) {
			temp *= i;
		}
		return temp;
	}

	public int factorialsWhileLoopwise(int n) {
		int temp = 1, i = 1;
		while (i <= n) {
			temp *= i++;
		}
		return temp;
	}

}
