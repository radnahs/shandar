/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.java8.streams </p>
 * <p>File Name: Product.java </p>
 * <p>Create Date: 02-Mar-2020 </p>
 * <p>Create Time: 10:01:18 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.java8.streams;

/**
 * @author : Shantanu Sikdar
 *
 */
public class Product {

	int id;
	String name;
	double price;

	public Product(int id, String name, double price) {
		this.id = id;
		this.name = name;
		this.price = price;
	}

}