/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.java8.methodReferences </p>
 * <p>File Name: MethodReference.java </p>
 * <p>Create Date: 07-Feb-2020 </p>
 * <p>Create Time: 9:41:21 am </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.java8.methodReferences;

import java.util.Map;
import java.util.function.BiFunction;

/**
 * @author : Shantanu Sikdar
 * @Description : MethodReference
 */

interface Sayable {
	void say();
}

public class MethodReference {

	public static void saySomething() {
		System.out.println("Hello, this is static method.");
	}

	public static void main(String[] args) {
		// Referring static method
		Sayable sayable = MethodReference::saySomething;
		// Calling interface method
		sayable.say();
	}

}
