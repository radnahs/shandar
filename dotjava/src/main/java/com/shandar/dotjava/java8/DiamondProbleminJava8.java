/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.java8 </p>
 * <p>File Name: DiamondProbleminJava8.java </p>
 * <p>Create Date: 28-Jul-2020 </p>
 * <p>Create Time: 3:05:27 pm </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.java8;

/**
 * @author : Shantanu Sikdar
 *
 */
public class DiamondProbleminJava8 implements FED, BED {

	// remove this override method to see the diamond problem
	@Override
	public void code() {
		System.out.println(Thread.currentThread().getStackTrace()[1]);
	}

	public static void main(String[] args) {
		DiamondProbleminJava8 ss = new DiamondProbleminJava8();
		ss.code();
	}

}

interface FED {
	default void code() {
		System.out.println(Thread.currentThread().getStackTrace()[1]);
	}
}

interface BED {
	default void code() {
		System.out.println(Thread.currentThread().getStackTrace()[1]);
	}
}
