/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.java8.streams </p>
 * <p>File Name: StreamIterating.java </p>
 * <p>Create Date: 02-Mar-2020 </p>
 * <p>Create Time: 10:55:12 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.java8.streams;

import java.util.stream.Stream;

/**
 * @author : Shantanu Sikdar
 *
 */
public class StreamIterating {

	public static void main(String[] args) {
		streamIterating();	
	}

	private static void streamIterating(){
		Stream.iterate(1, element -> element+1).filter(element->element%5==0).limit(5).forEach(System.out::println);
	}
}
