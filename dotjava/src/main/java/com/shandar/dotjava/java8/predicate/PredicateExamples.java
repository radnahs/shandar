/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.java8.predicate </p>
 * <p>File Name: PredicateExamples.java </p>
 * <p>Create Date: 31-Oct-2022 </p>
 * <p>Create Time: 7:11:55 PM </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.java8.predicate;

import java.util.Objects;
import java.util.function.Predicate;

/**
 * @author : Shantanu Sikdar
 *         https://www.geeksforgeeks.org/java-8-predicate-with-examples/
 */
public class PredicateExamples {

	public static void main(String[] args) {

		// predicateExample1();
		// predicateChaining();
		// predicateInToFunction();
		// predicateORFunction();
		predicateANDFunction();
	}

	private static void predicateExample1() {
		// Creating Predicate
		Predicate<Integer> lesserThan = i -> (i > 18);
		// Calling Predicate
		System.out.println(lesserThan.test(10));
	}

	//Predicate Chaining
	private static void predicateChaining() {
		Predicate<Integer> greaterThan10 = i -> (i > 10);

		//
		Predicate<Integer> lowerThan20 = i -> (i < 20);
		boolean result1 = greaterThan10.and(lowerThan20).test(16);
		System.out.println(result1);

		//
		boolean result2 = greaterThan10.and(lowerThan20).negate().test(16);
		System.out.println(result2);
	}

	//passing Predicate into function
	private static void predicateInToFunction() {
		pred(10, i -> i > 7);
	}

	private static void pred(int numbr, Predicate<Integer> pred) {
		if (pred.test(numbr)) {
			System.out.println(numbr);
		}
	}

	//OR Predicate
	private static void predicateORFunction() {
		predicateOR();
	}

	private static void predicateOR(){
		Predicate<String> containsLetterQ = s -> s.contains("Q");
		String containsQ = "Queue";
		boolean outcome = hasLengthOf10.or(containsLetterQ).test(containsQ);
		System.out.println(outcome);
	}
	
	private static Predicate<String> hasLengthOf10 = new Predicate<String>() {
		@Override
		public boolean test(String t) {
			return t.length() >10;
		}
	};

	//AND Predicate
	private static void predicateANDFunction(){
		predicateAND();
	}
	private static void predicateAND(){
		Predicate<String> nonNullPredicate = Objects::nonNull;
		String nullString = null;
		boolean outcome = nonNullPredicate.and(hasLengthOf10).test(nullString);
		System.out.println(outcome);
	
		String hasLengthGreaterThan10 = "Welcome to the machine";
		boolean outcome2 = nonNullPredicate.and(hasLengthOf10).test(hasLengthGreaterThan10);
		System.out.println(outcome2);
	}
	
}
