/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.java8.lambda </p>
 * <p>File Name: Eg1Lambda.java </p>
 * <p>Create Date: 30-Mar-2016 </p>
 * <p>Create Time: 7:49:49 am </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.java8.lambda;

/**
 * @author : Shantanu Sikdar
 * @Description : Eg1Lambda
 */
interface Drawable{
	public void draw();
}

public class Eg1Lambda {
	
	//without lambda
	private void withoutLambda(int width) {
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		System.out.println("Method Name :"+methodName);
		Drawable d2 = new Drawable() {
			@Override
			public void draw() {
				System.out.println("drawing " + width);
			}
		};
		d2.draw();
	}

	//with lambda
	public void withLambda(int width) {
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		System.out.println("Method Name :"+methodName);
		Drawable d2 = () ->{
			System.out.println("drawing " + width);
		};
		d2.draw();
	}

	public static void main(String[] args) {
		Eg1Lambda eg1 = new Eg1Lambda();
		eg1.withoutLambda(100);//without lambda
		eg1.withLambda(10);//with lambda
	}
	
}
