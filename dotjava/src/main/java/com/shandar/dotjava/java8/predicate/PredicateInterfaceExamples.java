/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.java8.predicate </p>
 * <p>File Name: PredicateInterfaceExamples.java </p>
 * <p>Create Date: 15-Nov-2022 </p>
 * <p>Create Time: 9:43:23 PM </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.java8.predicate;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

/**
 * @author : Shantanu Sikdar
 *
 *         https://zetcode.com/java/predicate/
 */
public class PredicateInterfaceExamples {

	public static void main(String[] args) {
		List<Integer> lstInt = List.of(1, 3, 5, 22, 11, 75, 3, 42, 57);
		//predicateExample1(lstInt);
		//predicateExample2(lstInt);

		listRemoveIf();
	}

	//
	static private void predicateExample1(List<Integer> lstInt) {
		GreaterThanFive<Integer> gtf = new GreaterThanFive<Integer>();
		lstInt.stream().filter(gtf).forEach(e -> {
			System.out.println(e);
		});
		lstInt.stream().filter(gtf).forEach(System.out::println);
	}

	static class GreaterThanFive<E> implements Predicate<Integer> {
		@Override
		public boolean test(Integer t) {
			return t > 5;
		}
	}

	// Java Predicate with lambda
	static private void predicateExample2(List<Integer> lstInt) {
		Predicate<Integer> gtf = prdc -> prdc > 5;
		lstInt.stream().filter(gtf).forEach(e -> {
			System.out.println(e);
		});
	}
	
	//Java Predicate removeIf
	private static void listRemoveIf(){
		var words = new ArrayList<String>();
		words.add("sky");
        words.add("warm");
        words.add("winter");
        words.add("cloud");
        words.add("pen");
        words.add("den");
        words.add("tree");
        words.add("sun");
        words.add("silk");
        
        Predicate<String> has3Chars = word -> word.length()==3;
        words.removeIf(has3Chars);
        System.out.println(words);

        /*List<String> lstStr = List.of("sky","warm");
        lstStr.removeIf(has3Chars);
        System.out.println(lstStr);*/
	}

}
