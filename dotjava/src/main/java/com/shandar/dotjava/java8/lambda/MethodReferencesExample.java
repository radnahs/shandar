/**
 * <p>Project: rytry </p>
 * <p>Package Name: com.shandar.rytry.java8.lambda </p>
 * <p>File Name: MethodReferencesExample.java </p>
 * <p>Create Date: Mar 27, 2020 </p>
 * <p>Create Time: 12:54:09 PM </p>
 * <p>Copyright: Copyright (c) 2014</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package  com.shandar.dotjava.java8.lambda;

import java.util.Arrays;
import java.util.List;

/**
 * @author Shantanu Sikdar
 *
 */
public class MethodReferencesExample {

	public static void methodReferenceAndLambda() {
		System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());
		List<String> listVal = Arrays.asList("***", "*", "**", "****");
		System.out.println("without methodReference");

		listVal.forEach(val -> {
			System.out.println(val);
		});
		
		System.out.println("with methodReference");
		
		listVal.forEach(System.out::println);
	}

	public static void main(String[] args) {
		methodReferenceAndLambda();
	}

}
