/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.java8.forEach </p>
 * <p>File Name: Java8ForEachExample.java </p>
 * <p>Create Date: 12-Nov-2022 </p>
 * <p>Create Time: 6:52:27 PM </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.java8.forEach;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.DoubleConsumer;
import java.util.function.IntConsumer;

/**
 * @author : Shantanu Sikdar
 * 
 *         https://zetcode.com/java/foreach/
 * 
 *         The forEach method was introduced in Java 8. It provides programmers
 *         a new, concise way of iterating over a collection. The forEach method
 *         performs the given action for each element of the Iterable until all
 *         elements have been processed or the action throws an exception.
 * 
 *         void forEach(Consumer<? super T> action);
 *
 */
public class Java8ForEachExample {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		List<String> itemsList = new ArrayList<String>();
		itemsList.add("coins");
		itemsList.add("pens");
		itemsList.add("keys");
		itemsList.add("cards");
		// javaForEachListConsumer(itemsList);
		// javaForEachListLambda(itemsList);

		Map<String, Integer> itemsMap = new HashMap<String, Integer>();
		itemsMap.put("coins", 6);
		itemsMap.put("pens", 5);
		itemsMap.put("keys", 8);
		itemsMap.put("cards", 2);
		// javaForEachMap(itemsMap);
		// javaForEachMapEntry(itemsMap);

		Set<String> brandsSet = new HashSet<>();
		brandsSet.add("Nike");
		brandsSet.add("IBM");
		brandsSet.add("Google");
		brandsSet.add("Apple");
		// javaForEachSet(brandsSet);

		// javaForEachArray(new int[] { 3, 6, 7, 2, 9 });

		//javaForEachListFilter(itemsList);
		
		
		//javaForEachIntConsumer();
		javaForEachIntDouble();
	}

	/**
	 * Consumer interface The Consumer interface is a functional interface (an
	 * interface with a single abstract method), which accepts a single input and
	 * returns no result.
	 * 
	 * @param itemsList
	 */
	private static void javaForEachListConsumer(List<String> itemsList) {
		itemsList.forEach(new Consumer<String>() {
			@Override
			public void accept(String str) {
				System.out.println(str);
			}
		});
	}

	private static void javaForEachListLambda(List<String> itemsList) {
		itemsList.forEach((item) -> {
			System.out.println(item);
		});
	}

	/**
	 * 
	 * @param itemsMap
	 */
	private static void javaForEachMap(Map<String, Integer> itemsMap) {
		itemsMap.forEach((item, value) -> {
			System.out.printf("%s : %d%n", item, value);
		});
	}

	private static void javaForEachMapEntry(Map<String, Integer> itemsMap) {
		Consumer<Map.Entry<String, Integer>> action = entry -> {
			System.out.printf("Key : %s ", entry.getKey());
			System.out.printf(" Value : %s%n ", entry.getValue());
		};
		itemsMap.entrySet().forEach(action);
	}

	private static void javaForEachSet(Set<String> brandsSet) {
		brandsSet.forEach((e) -> {
			System.out.println(e);
		});
	}

	/**
	 * 
	 * @param numbrs
	 */
	private static void javaForEachArray(int[] numbrs) {
		Arrays.stream(numbrs).forEach((e) -> {
			System.out.printf("%n%d", e);
		});
	}

	private static void javaForEachListFilter(List<String> itemsList) {
		itemsList.stream().filter(item -> item.length() == 4).forEach(e -> {
			System.out.println(e);
		});
	}
	
	/**
	 *  built-in consumer interfaces for primitive data types 
	 */
	private static void javaForEachIntConsumer() {
		int[] inums = { 3, 5, 6, 7, 5 };
		IntConsumer itemsInt = new IntConsumer() {
			@Override
			public void accept(int intgr) {
				System.out.println(intgr);
			}
		};
		Arrays.stream(inums).forEach(itemsInt);
	}
	
	private static void javaForEachIntDouble() {
		double[] dnums = {3.4d, 9d, 6.8d, 10.3d, 2.3d};
		DoubleConsumer itemsDnums = (d)->{System.out.println(d);};
		Arrays.stream(dnums).forEach(itemsDnums);
	}
	
	
}
