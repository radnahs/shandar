/**
 * <p>Project: rytry </p>
 * <p>Package Name: com.shandar.rytry.java8.lambda </p>
 * <p>File Name: LambdaExamples.java </p>
 * <p>Create Date: Mar 26, 2020 </p>
 * <p>Create Time: 12:48:50 PM </p>
 * <p>Copyright: Copyright (c) 2014</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.dotjava.java8.lambda;

import java.io.File;
import java.io.FileFilter;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author Shantanu Sikdar
 *
 */
public class LambdaExamples {

	public void runnableUsingAnonymousClass() {
		System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());

		// implementation of Runnable in Thread Constructor using anonymous class
		Thread t1 = new Thread(new Runnable() {
			public void run() {
				for (int i = 0; i < 3; i++) {
					System.out.println(i);
					System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());
					System.out.println(Thread.currentThread().getName());
				}
			}
		});
		try {
			t1.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void runnableUsingLambda() {
		System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());

		// implementation of Runnable in Thread Constructor using lambda
		Thread t1 = new Thread(() -> {
			for (int i = 0; i < 3; i++) {
				System.out.println(i);
				System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());
				System.out.println(Thread.currentThread().getName());
			}
		});
		t1.start();

		try {
			t1.join();
		} catch (InterruptedException e) {

		}

	}

	public void fileFilterUsingAnonymousClass() {
		System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());

		File dir = new File(
				"C:\\DATA\\projects\\shandar\\rytry\\src\\main\\java\\com\\shandar\\rytry\\java8\\lambda");

		// implementation of FileFilter in listFiles() of File class using anonymous
		// class
		File[] files = dir.listFiles(new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());
				return pathname.getName().endsWith(".java");
			}
		});
		for (File file : files) {
			System.out.println(file);
		}

	}

	public void fileFilterUsingLambda() {
		System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());

		File dir = new File(
				"C:\\DATA\\projects\\shandar\\rytry\\src\\main\\java\\com\\shandar\\rytry\\java8\\lambda");

		// implementation of FileFilter in listFiles method of File class using lambda
		File[] files = dir.listFiles((File pathname) -> {
			System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());
			return pathname.getName().endsWith(".java");
		});
		Arrays.stream(files).forEach(file -> {
			System.out.println(file);
		});

	}

	public void comparatorUsingAnonymousClass() {
		System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());
		List<String> listVal = Arrays.asList("*", "***", "**", "****");
		System.out.println("before sorting");

		for (String string : listVal) {
			System.out.println(string);
		}

		// implementation of Comparator in Collections.sort method using anonymous class
		Collections.sort(listVal, new Comparator<String>() {

			@Override
			public int compare(String o1, String o2) {
				return Integer.compare(o1.length(), o2.length());
			}
		});
		System.out.println("after sorting");
		for (String string : listVal) {
			System.out.println(string);
		}

	}

	public void comparatorUsingLambda() {
		System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());
		List<String> listVal = Arrays.asList("***", "*", "**", "****");
		System.out.println("before sorting");

		listVal.forEach(val -> {
			System.out.println(val);
		});

		// implementation of Comparator in Collections.sort method using lambda
		Collections.sort(listVal, (String o1, String o2) -> {
			return Integer.compare(o1.length(), o2.length());
		});

		listVal.forEach(val -> {
			System.out.println(val);
		});
	}

	public static void main(String[] args) {
		LambdaExamples lambdaExample = new LambdaExamples();
		lambdaExample.runnableUsingAnonymousClass();
		lambdaExample.runnableUsingLambda();
		lambdaExample.fileFilterUsingAnonymousClass();
		lambdaExample.fileFilterUsingLambda();
		lambdaExample.comparatorUsingAnonymousClass();
		lambdaExample.comparatorUsingLambda();

	}
}
