/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.java8.lambda </p>
 * <p>File Name: Eg2LambdaNoParam.java </p>
 * <p>Create Date: 30-Jan-2016 </p>
 * <p>Create Time: 8:46:06 am </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.java8.lambda;

/**
 * @author : Shantanu Sikdar
 * @Description : Eg2LambdaMultipleParameter
 */
@FunctionalInterface
interface SayableNoParam {
	public String say();
}

@FunctionalInterface
interface SayableOneParam {
	public String say(String name);
}

//@FunctionalInterface
interface SayableMultipleParam {
	public String say(String name, String... atrArr);
}

public class Eg2LambdaNoAndMultipleParameter {

	private void lambdaNoParam() {
		System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());
		SayableNoParam say = () -> {
			return "lambda with No-Param";
		};
		System.out.println(say.say());
	}

	private void lambdaOneParam(String oneStr) {
		System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());
		SayableOneParam say = (param1) -> {
			return "lambda with" + param1;
		};
		System.out.println(say.say(oneStr));
	}

	private void lambdaMultipleParam(String str1, String... strArr) {
		System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());
		SayableMultipleParam say = (param1, paramArr) -> {
			return "lambda with" + param1 + paramArr;
		};
		System.out.println(say.say(str1));
	}

	public static void main(String[] args) {
		Eg2LambdaNoAndMultipleParameter elnamp = new Eg2LambdaNoAndMultipleParameter();
		elnamp.lambdaNoParam();
		elnamp.lambdaOneParam(" one param");
		elnamp.lambdaMultipleParam("str one ", new String[] { "two", "three" });
	}

}
