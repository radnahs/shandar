/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.java8 </p>
 * <p>File Name: ExecutorServiceExample.java </p>
 * <p>Create Date: 22-Jan-2016 </p>
 * <p>Create Time: 10:59:30 am </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.java8;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * @author : Shantanu Sikdar
 * @Description : ExecutorServiceExample
 */
public class ExecutorServiceExample {

	public static void main(String[] args) {
		ExecutorServiceExample exmp = new ExecutorServiceExample();
		exmp.lamdaWayThread();
		//exmp.usingExecutor();
	}

	void lamdaWayThread(){
		Runnable task = () -> System.out.println("lambda way"+Thread.currentThread().getStackTrace()[1]);
		Thread thrd1 = new Thread(task);
		Thread thrd2 = new Thread(task);
		thrd1.start();
		thrd2.start();
	}
	
	void usingExecutor() {
		Executor executor = Executors.newSingleThreadExecutor();
		Runnable task = ()-> System.out.println("Running Executors.newSingleThreadExecutor");
		executor.execute(task);
	}
	
}
