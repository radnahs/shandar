/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.java8.streams </p>
 * <p>File Name: StreamsMap.java </p>
 * <p>Create Date: 04-Aug-2020 </p>
 * <p>Create Time: 11:49:02 am </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.java8.streams;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author : Shantanu Sikdar
 *
 */
public class StreamsMap {

	public static void main(String[] args) {
		List<String> familyMember1 = Arrays.asList("Shantanu", "Nandita", "Piyali", "Ranjit", "Priti");

		// Before Java 8
		List<String> familyMember2 = new ArrayList<String>();
		for (String string : familyMember1) {
			familyMember2.add(string);
		}
		System.out.println(familyMember1);
		System.out.println(familyMember2);

		// From Java 8
		// List<String> familyMember3 = familyMember1.stream().map(fm ->
		// name).collect();

		List<String> familyMemberNickName = Arrays.asList("Shantanu", "Kiti", "Mamon", "Ranjit", "Kanchi");

	}

}
