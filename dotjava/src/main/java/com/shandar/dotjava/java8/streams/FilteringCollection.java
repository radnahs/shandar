/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.java8.streams </p>
 * <p>File Name: FilteringCollection.java </p>
 * <p>Create Date: Jan 10, 2016 </p>
 * <p>Create Time: 1:53:19 PM </p>
 * <p>Copyright: Copyright (c) 2014</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.dotjava.java8.streams;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Shantanu Sikdar
 * https://www.javatpoint.com/java-8-stream
 */

public class FilteringCollection {
	
	private List<Product> productList() {
		List<Product> lstProduct = new ArrayList<Product>();
		lstProduct.add(new Product(1, "Mi Note 8 Pro", 20000));
		lstProduct.add(new Product(1, "One + 8", 35000));
		lstProduct.add(new Product(1, "Vivo ", 25000));
		lstProduct.add(new Product(1, "Samsung Galaxy s10", 55000));
		lstProduct.add(new Product(1, "Lenovo Note 10", 18000));
		return lstProduct;
	}
	
	private void filteringCollectionWithoutStreams() {
		System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());
		List<Product> lstProduct = productList();
		List<Double> lstProductPrice = new ArrayList<Double>();
		for (Product prdct : lstProduct) {
			if(prdct.price>30000) {
				lstProductPrice.add(prdct.price);
			}
		}
		System.out.println(lstProductPrice);
	}

	private void filteringCollectionWithStreams() {
		System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());
		List<Product> lstProduct = productList();
		List<Double> lstProductPrice = lstProduct.stream().filter(p->p.price>30000).map(p->p.price).collect(Collectors.toList());
		System.out.println(lstProductPrice);
		List<Double> lstProductPrice1 = lstProduct.stream().filter(p->p.price>30000).map(p->p.price).collect(Collectors.toList());
		System.out.println(lstProductPrice1);
	}
	
	private void streamIterating(){
		System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());
		Stream.iterate(1, element -> element+1).filter(element->element%5==0).limit(5).forEach(System.out::println);
	}

	public static void main(String[] args) {
		FilteringCollection as = new FilteringCollection();
		//as.filteringCollectionWithoutStreams();
		as.filteringCollectionWithStreams();
		//as.streamIterating();
	}

}
