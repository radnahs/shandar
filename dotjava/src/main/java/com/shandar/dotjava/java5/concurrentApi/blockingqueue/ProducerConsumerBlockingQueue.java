/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.java5.concurrentApi.blockingqueue </p>
 * <p>File Name: ProducerConsumerBlockingQueue.java </p>
 * <p>Create Date: 03-Aug-2020 </p>
 * <p>Create Time: 5:57:50 pm </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.dotjava.java5.concurrentApi.blockingqueue;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author : Shantanu Sikdar
 *
 */
public class ProducerConsumerBlockingQueue {

	public static void main(String[] args) {
		BlockingQueue<String> bQueue = new ArrayBlockingQueue<>(10);

		Lock lock = new ReentrantLock();

		// Producer
		final Runnable producer = () -> {
			int counter = 0;
			while (counter < 50) {
				try {
					bQueue.put("shantanu" + counter++);
					System.out.println(bQueue);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		};

		new Thread(producer).start();

		// consumer
		final Runnable consumer = () -> {
			while (true) {
				try {
					String str;
					str = bQueue.take();
					System.out.println("consumed" + str);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		};

		new Thread(consumer).start();
	}

}
