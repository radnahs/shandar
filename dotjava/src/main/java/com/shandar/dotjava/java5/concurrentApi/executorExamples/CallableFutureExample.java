/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.java5.concurrentApi.executorExamples </p>
 * <p>File Name: CallableFutureExample.java </p>
 * <p>Create Date: 15-Jun-2020 </p>
 * <p>Create Time: 11:30:56 am </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.java5.concurrentApi.executorExamples;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * @author : Shantanu Sikdar
 *
 */
public class CallableFutureExample {

	public static void main(String[] args) {
		// singleOutputCallableFuture();
		multipleOutputCallableFuture();

		System.out.println("Thread name = " + Thread.currentThread().getName());
	}

	static void singleOutputCallableFuture() {
		ExecutorService service = Executors.newFixedThreadPool(10);
		Future<Integer> future = service.submit(new TaskCF());
		for (int i = 0; i < 10; i++) {
			System.out.print(i + " ");
		}
		try {
			Integer result = future.get();
			System.out.println(result);
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
		for (int i = 11; i < 20; i++) {
			System.out.print(i + " ");
		}
	}

	static void multipleOutputCallableFuture() {
		ExecutorService service = Executors.newFixedThreadPool(10);
		List<Future<Integer>> futureList = new ArrayList<Future<Integer>>();
		for (int i = 0; i < 10; i++) {
			Future<Integer> future = service.submit(new TaskCF());
			futureList.add(future);
		}

		for (int i = 0; i < 10; i++) {
			Future<Integer> future = futureList.get(i);
			Integer intgr = 0;
			try {
				intgr = future.get();
			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
			}
			System.out.println(intgr);
		}

	}

}

class TaskCF implements Callable<Integer> {

	@Override
	public Integer call() throws Exception {
		Thread.sleep(5000);
		return new Random().nextInt();
	}

}
