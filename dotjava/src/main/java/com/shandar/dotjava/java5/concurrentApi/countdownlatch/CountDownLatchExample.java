/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.java5.concurrentApi </p>
 * <p>File Name: CountDownLatchExample.java </p>
 * <p>Create Date: 18-Feb-2020 </p>
 * <p>Create Time: 9:22:52 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.java5.concurrentApi.countdownlatch;

import java.util.concurrent.CountDownLatch;

/**
 * @author : Shantanu Sikdar
 * @Description : CountDownLatchExample
 *              https://www.polarsparc.com/xhtml/Synchronization.html
 */
class CountDownLatchExample {

	public static void main(String[] args) {
		final int COUNT = 3; // 3 Threads

		CountDownLatch latch = new CountDownLatch(COUNT);

		Thread as = new Thread(new AuthenticationService(latch));
		as.setName("AuthenticationServiceThread");

		Thread bs = new Thread(new AccountBalanceService(latch));
		bs.setName("AccountBalanceServiceThread");

		Thread fs = new Thread(new FraudService(latch));
		fs.setName("FraudServiceThread");

		System.out.printf("Initialization started ...\n");

		as.start();
		bs.start();
		fs.start();

		try {
			latch.await();
		} catch (InterruptedException ex) {
			ex.printStackTrace(System.err);
		}

		System.out.printf("Initialization completed !!!\n");
	}

	static class AuthenticationService implements Runnable {
		private final CountDownLatch latch;

		AuthenticationService(CountDownLatch latch) {
			this.latch = latch;
		}

		@Override
		public void run() {
			try {
				System.out.printf("Initializing authentication service ...\n");
				Thread.sleep(2000); // 2 seconds
				System.out.printf("Authentication service ready !!!\n");
				latch.countDown();
			} catch (InterruptedException ex) {
				ex.printStackTrace(System.err);
			}
		}
	}

	static class AccountBalanceService implements Runnable {
		private final CountDownLatch latch;

		AccountBalanceService(CountDownLatch latch) {
			this.latch = latch;
		}

		@Override
		public void run() {
			try {
				System.out.printf("Initializing account balance service ...\n");
				Thread.sleep(2000); // 2 seconds
				System.out.printf("Account balance service ready !!!\n");
				latch.countDown();
			} catch (InterruptedException ex) {
				ex.printStackTrace(System.err);
			}
		}
	}

	static class FraudService implements Runnable {
		private final CountDownLatch latch;

		FraudService(CountDownLatch latch) {
			this.latch = latch;
		}

		@Override
		public void run() {
			try {
				System.out.printf("Initializing fraud service ...\n");
				Thread.sleep(2000); // 2 seconds
				System.out.printf("Fraud service ready !!!\n");
				latch.countDown();
			} catch (InterruptedException ex) {
				ex.printStackTrace(System.err);
			}
		}
	}

}
