/**
 * <p>Project: rytry </p>
 * <p>Package Name: com.shandar.dotjava.java5.concurrentApi.executorExamples </p>
 * <p>File Name: NewCachedThreadPoolExample.java </p>
 * <p>Create Date: 15-Jun-2020 </p>
 * <p>Create Time: 7:23:28 am </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.java5.concurrentApi.executorExamples;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author : Shantanu Sikdar
 * @Description : NewCachedThreadPoolExample
 */
public class NewCachedThreadPoolExample {

	public static void main(String[] args) {
		ExecutorService servive = Executors.newCachedThreadPool();
		for (int i = 0; i < 10; i++) {
			servive.execute(new TaskNCTP());
		}
	}

}


class TaskNCTP implements Runnable {

	@Override
	public void run() {
		System.out.println("Thread Name : " + Thread.currentThread().getName());
	}

}