/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.java5 </p>
 * <p>File Name: AutoboxingUnboxing.java </p>
 * <p>Create Date: Feb 3, 2015 </p>
 * <p>Create Time: 7:35:39 PM </p>
 * <p>Copyright: Copyright (c) 2014</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.dotjava.java5;

/**
 * @author Shantanu Sikdar
 *
 */
public class AutoboxingUnboxing {

	public static void main(String[] args) {
		AutoboxingUnboxing wbb = new AutoboxingUnboxing();

		// widening and boxing, widening beats boxing.
		short val1 = 24;
		wbb.wideningBeatsBoxing(val1);
		Integer val2 = 24;
		wbb.wideningBeatsBoxing(val2);
		

		// widening and varargs, widening beats var-args.
		short val3 = 21;
		wbb.wideningBeatsVarargs(val3, val3);
		int val4 = 31;
		wbb.wideningBeatsVarargs(val4, val4);

		// boxing and varargs, boxing beats variable argument
		int val5 = 41;
		wbb.boxingBeatsVarargs(val5);

		//Widening and Boxing can't be performed as given below:
		int val6=51;
		//wbb.wideningAndBoxing(val6);
	}

	void wideningBeatsBoxing(int i) {
		System.out.println("int");
	}

	void wideningBeatsBoxing(Integer i) {
		System.out.println("Integer");
	}

	void wideningBeatsVarargs(int i, int i2) {
		System.out.println("int int");
	}

	void wideningBeatsVarargs(int... i) {
		System.out.println("int...");
	}

	void boxingBeatsVarargs(Integer i) {
		System.out.println("Integer");
	}

	void boxingBeatsVarargs(Integer... i) {
		System.out.println("Integer...");
	}

	void wideningAndBoxing(Long l) {
		System.out.println("Long");
	}
}
