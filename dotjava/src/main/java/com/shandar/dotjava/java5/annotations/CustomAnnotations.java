/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.java5.annotations </p>
 * <p>File Name: CustomAnnotations.java </p>
 * <p>Create Date: 04-Feb-2016 </p>
 * <p>Create Time: 10:48:03 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.java5.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Method;

/**
 * @author : Shantanu Sikdar
 * @Description : CustomAnnotations
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@interface MarkerAnnotationExample {
	
}

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@interface SingleValueAnnotationExample{
	int value() default 100;
}


//Applying annotation  
class Hello {
	
	@SingleValueAnnotationExample(value = 10) //try removing the value the default value will be printed
	public void sayHello() {
		System.out.println("hello annotation");
	}
}

public class CustomAnnotations {

	public static void main(String[] args) {
		try {
			Hello h = new Hello();
			Method mthd = h.getClass().getMethod("sayHello");
			SingleValueAnnotationExample mann=mthd.getAnnotation(SingleValueAnnotationExample.class);
			System.out.println(mann.value());
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
	}	

}
