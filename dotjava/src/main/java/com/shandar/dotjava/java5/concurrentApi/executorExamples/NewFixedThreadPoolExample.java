/**
 * <p>Project: rytry </p>
 * <p>Package Name: com.shandar.dotjava.java5.concurrentApi.executorExamples </p>
 * <p>File Name: ExecutorThread.java </p>
 * <p>Create Date: 14-Jun-2020 </p>
 * <p>Create Time: 10:57:11 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.java5.concurrentApi.executorExamples;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author : Shantanu Sikdar
 * @Description : ExecutorThread
 */
public class NewFixedThreadPoolExample {

	public static void main(String[] args) {

		cpuIntensiveTasksSingleObj();
		System.out.println();
		cpuIntensiveTasksMultiObj();
	}

	// Single Object having Multiple thread
	private static void cpuIntensiveTasksSingleObj() {
		System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());

		int n = Runtime.getRuntime().availableProcessors();

		ExecutorService serviceNFTP = Executors.newFixedThreadPool(n);

		TaskNFTP tsk1 = new TaskNFTP();// single obj

		for (int i = 0; i < 10; i++) {
			serviceNFTP.execute(tsk1);
		}

		System.out.println("Thread Name : " + Thread.currentThread().getName());
	}

	// Multiple Object every object having one thread
	private static void cpuIntensiveTasksMultiObj() {
		System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());

		int n = Runtime.getRuntime().availableProcessors();

		ExecutorService serviceNFTP = Executors.newFixedThreadPool(n);

		for (int i = 0; i < 10; i++) {
			serviceNFTP.execute(new TaskNFTP());// multiple object
		}

		System.out.println("Thread Name : " + Thread.currentThread().getName());
	}

}

class TaskNFTP implements Runnable {

	@Override
	public void run() {
		System.out.println("Thread Name : " + Thread.currentThread().getName());
	}

}
