/**
 * <p>Project: rytry </p>
 * <p>Package Name: com.shandar.dotjava.java5.concurrentApi.executorExamples </p>
 * <p>File Name: NormalThread.java </p>
 * <p>Create Date: 14-Jun-2020 </p>
 * <p>Create Time: 9:32:34 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.java5.concurrentApi.executorExamples;

/**
 * @author : Shantanu Sikdar
 * @Description : NormalThread
 */
public class NormalThread {

	public static void main(String[] args) {

		// 1 object 10 thread, object having 10 thread
		TaskOne to1 = new TaskOne();
		for (int i = 0; i < 10; i++) {
			System.out.println(to1.hashCode());
			Thread t1 = new Thread(to1);
			t1.setName("Thread-Name - " + i);
			t1.start();
		}

		// 10 object 10 thread every object having 1 thread
		for (int i = 0; i < 10; i++) {
			TaskOne to2 = new TaskOne();
			System.out.println(to2.hashCode());
			Thread t2 = new Thread(to2);
			t2.start();
		}

	}

}

class TaskOne implements Runnable {

	@Override
	public void run() {
		System.out.println("Hash code of Current Thread : " + Thread.currentThread().hashCode() + "|| Current Thread : "
				+ Thread.currentThread().getName());
	}

}
