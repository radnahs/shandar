/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.java5.concurrentApi.executorExamples </p>
 * <p>File Name: NewScheduledThreadPoolExample.java </p>
 * <p>Create Date: 15-Jun-2020 </p>
 * <p>Create Time: 7:57:07 am </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.java5.concurrentApi.executorExamples;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @author : Shantanu Sikdar
 *
 */
public class NewScheduledThreadPoolExample {

	public static void main(String[] args) {
		ScheduledExecutorService servive = Executors.newScheduledThreadPool(10);
		
		servive.schedule(new TaskNSTPRunnable(), 10, TimeUnit.SECONDS);
		servive.scheduleAtFixedRate(new TaskNSTPRunnable(), 15, 10, TimeUnit.SECONDS);
		
	}

}

class TaskNSTPRunnable implements Runnable{

	@Override
	public void run() {
		System.out.println();
	}
	
}

//class TaskNSTPCallable implements Callable<V>