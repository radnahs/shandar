/**
 * <p>Project: dotjava </p>
 * <p>Package Name: com.shandar.dotjava.java5.concurrentApi </p>
 * <p>File Name: AtomicExamples.java </p>
 * <p>Create Date: 15-Feb-2020 </p>
 * <p>Create Time: 7:44:58 am </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.dotjava.java5.concurrentApi;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author : Shantanu Sikdar
 * @Description : AtomicExamples
 */
public class AtomicExamples {

	public static void main(String[] args) {
		createGetSet();
	}
	
	private static void createGetSet() {
		AtomicInteger atomicInteger = new AtomicInteger();
		System.out.println(atomicInteger.get());
		AtomicInteger atomicIntegerArgs = new AtomicInteger(100);
		System.out.println(atomicIntegerArgs.get());
		atomicIntegerArgs.set(200);
		System.out.println(atomicIntegerArgs.get());
	}

}
