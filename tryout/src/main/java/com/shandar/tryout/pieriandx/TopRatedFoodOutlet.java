/**
 * Project: tryout 
 * Package Name: com.shandar.tryout.pieriandx 
 * File Name: PeiriandxProblem.java 
 * Create Date: 23-May-2021 
 * Create Time: 3:08:23 pm 
 * Copyright: Copyright (c) 2016
 * Company:  
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.pieriandx;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.google.gson.Gson;

/**
 * @author : Shantanu Sikdar
 *
 *         <h1 class="question-view__title">
 *         6. REST API: Top Rated Food Outlets
 *         A REST API contains information about food outlets across multiple
 *         cities. Given the city name, get all the food outlets with the
 *         highest rating in this city. The API returns paginated data.
 *         
 *         To access the information, perform an HTTP GET request to:
 *         
 *         <em>https://jsonmock.hackerrank.com/api/food_outlets?city=&lt;city&gt;&amp;page=&lt;pageNumber&gt;</em>
 *         
 *         where &lt;<em>city</em>&gt; is the city to get the food
 *         outlets for and <em>&lt;pageNumber&gt;</em> is an
 *         integer that denotes the page of the results to return.
 *         
 *         For example, a GET request to

 *         <em>https://jsonmock.hackerrank.com/api/food_outlets?city=Seattle&amp;page=2</em>
 * 
 *         
 *         returns data associated with city <em>Seattle</em>, and on the second
 *         page of the results.
 *         
 *         Similarly, a GET request to
 *         
 *         <em>https://jsonmock.hackerrank.com/api/food_outlets?city=Houston&amp;page=1</em>
 *         
 *         returns data associated with city <em>Houston</em>, and on the first
 *         page of the results.
 *         
 *         The response to such a request is a JSON with the following 5 fields:
 *         
 *          page: The current page of the results. 
 *          per_page: The maximum number of records returned per
 *         page. 
 *          total: The total number of records in the
 *         database. 
 *          total_pages: The total number of pages with
 *         results. 
 *          data: Either an empty array or an array of outlet
 *         objects. Each object has the following schema:
 *         
 *         city: city we queried for where the outlet is located
 *         [STRING]
 *         
 *         name: name of the outlet [STRING]
 *         
 *         estimated_cost: estimated cost for 2 persons [INTEGER]
 *         user_rating:
 *         
 *         average_rating: average rating of the outlet [FLOAT]
 *         
 *         votes: total votes for the outlet [INTEGER]
 *          
 *         
 *         id: unique identifier of the outlet [INTEGER]
 *         
 *         Below is an example of an outlet object:
 *         
 * {
    "city": "Houston",
    "name": "Cocoa Tree",
    "estimated_cost": 10,
    "user_rating": {
        "average_rating": 4.5,
        "votes": 969
    },
    "id": 938
},
 *         
 *         Given a string <em>city</em>, return the list of food outlet names
 *         that are located in this city and have the highest
 *         <em>average_rating</em> in this city.
 *         
 *         Function Description 
 *         
 *         Complete the function <em>getTopRatedFoodOutlets</em> in the editor
 *         below. Use the property <em>average_rating </em>for
 *         comparison. Please note that there can be multiple outlets with
 *         the highest rating. In this case return all the outlets in the order
 *         they appear in the API response. Please perform pagination in order
 *         to get all the results.
 *         
 *         <em>getTopRatedFoodOutlets</em> has the following parameter:
 *         
 *             <em>string category:</em> string denoting the
 *         category we want to query for
 *         
 *         Return 
 *         
 *         An array of strings denoting the food outlet names that have the
 *         highest rating in this city<em>. </em>The names in the array must be
 *         ordered in the order they appear in the API response.
 *         
 *         Constraints 
 *          
 *          The given city will always have data returned from the API. 
 *          <em>0 ≤ Rating ≤ 5.</em> 
 * 
 *         Note:
 *         Required libraries can be imported in order to solve the question.
 *         Check our full list of supported libraries
 *         at <a data-sk="tooltip_parent" data-stringify-link=
 *         "https://www.hackerrank.com/environment" href=
 *         "https://www.hackerrank.com/environment" rel="noopener noreferrer"
 *         target="_blank">https://www.hackerrank.com/environment</a>.
 *         
 *         <!-- <StartOfInputFormat> DO NOT REMOVE THIS LINE-->
 * 
 *         Input Format For Custom
 *         Testing
 *         
 *         In the first and the only line of input, there is a string,
 *         <em>city.</em>
 *         
 *           DO NOT REMOVE THIS
 *         LINE-->
 * 
 *         Sample Case
 *         0
 *         
 *         Sample Input For Custom Testing
 *         
 * STDIN            Function
-----            --------
Denver         → city = 'Denver'
 *         
 *         
 *         Sample Output
 *         
 * Sushi Kashiba
Peninsula Grand Hotel
Plum by Bent Chair
R' ADDA
Palladium Social
 *         
 *         Explanation
 *         
 *         The function makes paginated calls to the api
 *         <em>https://jsonmock.hackerrank.com/api/food_outlets?city=Denver&amp;page=1</em>,
 *         <em>https://jsonmock.hackerrank.com/api/food_outlets?city=Denver&amp;page=2</em>
 *         and so on, to get all the food outlets in <em>Denver</em> city. It
 *         returns the outlets which have the highest <em>average_rating </em>in
 *         city <em>Denver</em> (highest <em>average_rating </em>is 4.9 in this
 *         case). The list of outlets is then printed by the stub code.
 *         
 *         Sample Case 1
 * 
 *         Sample Input For Custom Testing
 *         
 *         Houston

 *         Sample Output
 *         
 * Barbeque Nation
 *         
 *         Explanation
 *         
 *         The function makes paginated calls to the api
 *         <em>https://jsonmock.hackerrank.com/api/food_outlets?city=Houston&amp;page=1</em>,
 *         <em>https://jsonmock.hackerrank.com/api/food_outlets?city=Houston&amp;page=2</em>
 *         and so on, to get all the food outlets in <em>Houston</em> city. It
 *         returns the outlets which have the highest <em>average_rating </em>in
 *         city <em>Houston</em> (highest <em>average_rating </em>is 4.9 in this
 *         case). The list of outlets is then printed by the stub code.
 * 
 *
 */

public class TopRatedFoodOutlet {

	
	public static List<String> getTopRatedFoodOutlets(String city) {
		List<String> outputList = new ArrayList<String>();
		//https://jsonmock.hackerrank.com/api/food_outlets?city=&lt;city&gt;&amp;page=&lt;pageNumber&gt;
		String strUrl = "https://jsonmock.hackerrank.com/api/food_outlets?city="+ city ;
		PageDTO page = jsonPerPage(strUrl);
		
		//System.out.println(page.getTotal_pages());
		
		List<TopRatedFoodOutlet.OutletDTO> outletDTOList = new ArrayList<TopRatedFoodOutlet.OutletDTO>();
		for (int i = 1; i <= page.getTotal_pages(); i++) {
			PageDTO pageTemp = jsonPerPage(strUrl +"&page="+i);
			outletDTOList.addAll(pageTemp.getData());
		}
		
		Collections.sort(outletDTOList, new Comparator<TopRatedFoodOutlet.OutletDTO>() {
			@Override
			public int compare(OutletDTO o1, OutletDTO o2) {
				return Double.compare(o1.getUser_rating().getAverage_rating(), o2.getUser_rating().getAverage_rating());
			}
		} );
		
		double dd = 0 ;
		for (TopRatedFoodOutlet.OutletDTO odto: outletDTOList) {
			double tempdd = odto.getUser_rating().getAverage_rating();
			if(tempdd>dd) {
				dd=tempdd;
				outputList.add(odto.getName());
			}
			
		}
		System.out.println(outputList);
		return outputList;
	}

	private static PageDTO jsonPerPage(String strUrl) {
		PageDTO page = null;
		try {
			URL url = new URL(strUrl);
			HttpURLConnection huc = (HttpURLConnection) url.openConnection();
			huc.setRequestMethod("GET");
			System.out.println(huc.getResponseCode());

			StringBuilder response = new StringBuilder();
			try (BufferedReader br = new BufferedReader(new InputStreamReader(huc.getInputStream(), "utf-8"))) {
				String responseLine = null;
				while ((responseLine = br.readLine()) != null) {
					response.append(responseLine.trim());
				}
				System.out.println(response.toString());
			}
			Gson gson = new Gson();
			page = gson.fromJson(response.toString(), PageDTO.class);

		} catch (Exception e) {

		}
		return page;
	}
	
	public static void main(String[] args) {
		System.out.println("test");
		getTopRatedFoodOutlets("Denver");
	}
	
	
	class PageDTO {

		private String page;
		private Integer per_page;
		private Integer total;
		private Integer total_pages;
		private List<OutletDTO> data;

		public String getPage() {
			return page;
		}

		public void setPage(String page) {
			this.page = page;
		}

		public Integer getPer_page() {
			return per_page;
		}

		public void setPer_page(Integer per_page) {
			this.per_page = per_page;
		}

		public Integer getTotal() {
			return total;
		}

		public void setTotal(Integer total) {
			this.total = total;
		}

		public Integer getTotal_pages() {
			return total_pages;
		}

		public void setTotal_pages(Integer total_pages) {
			this.total_pages = total_pages;
		}

		public List<OutletDTO> getData() {
			return data;
		}

		public void setData(List<OutletDTO> data) {
			this.data = data;
		}

	}

	class OutletDTO {
		
		private String city;
		private String name;
		private Integer estimated_cost;
		private UserRatingDTO user_rating;
		private Integer id;
		
		public String getCity() {
			return city;
		}
		public void setCity(String city) {
			this.city = city;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public Integer getEstimated_cost() {
			return estimated_cost;
		}
		public void setEstimated_cost(Integer estimated_cost) {
			this.estimated_cost = estimated_cost;
		}
		public UserRatingDTO getUser_rating() {
			return user_rating;
		}
		public void setUser_rating(UserRatingDTO user_rating) {
			this.user_rating = user_rating;
		}
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		
	}

	class UserRatingDTO {
		private Double average_rating;
		private Integer votes;
		
		public Double getAverage_rating() {
			return average_rating;
		}
		public void setAverage_rating(Double average_rating) {
			this.average_rating = average_rating;
		}
		public Integer getVotes() {
			return votes;
		}
		public void setVotes(Integer votes) {
			this.votes = votes;
		}

	}
	

}



