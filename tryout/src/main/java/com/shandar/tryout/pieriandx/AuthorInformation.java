/**
 * Project: tryout 
 * Package Name: com.shandar.tryout.pieriandx 
 * File Name: PieriandxOne.java 
 * Create Date: 20-May-2021 
 * Create Time: 2:07:49 pm 
 * Copyright: Copyright (c) 2016
 * Company:  
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.pieriandx;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;

/**
 * @author : Shantanu Sikdar
 *
 *
 *         6. Author Information
 * 
 *         Write an <em>HTTP GET</em> method to retrieve information from the
 *         <em>articles</em> and <em>article_users</em> databases. There may be
 *         multiple pages that are accessed by appending <em>&amp;page=num</em>
 *         where <em>num</em> is replaced with the page number.
 * 
 *         Function Description
 * 
 *         Given a string of author<em>, getAuthorHistory</em> must perform the
 *         following tasks:
 * 
 * 
 *         Initialize the <em>history</em> array to store a list of string
 *         elements. Query
 *         <code>https://jsonmock.hackerrank.com/api/article_users?username=&lt;authorName&gt;</code>(replace
 *         &lt;authorName&gt;) to retrieve author information in the
 *         <code>data</code> field. Store the value of the <code>about</code>
 *         field from the query response. If the <code>about</code> field is
 *         empty or null, do not store a value for this item. Query
 *         <code>https://jsonmock.hackerrank.com/api/articles?author=&lt;authorName&gt;</code>(replace
 *         &lt;authorName&gt;), to retrieve the list of author's articles in the
 *         <code>data</code> field. Add the <code>title</code> from each record
 *         returned in the <code>data</code> field to the <em>history</em>
 *         array.
 * 
 *         If the <code>title</code> field is null or empty then use the
 *         <code>story_title</code> to add in the <em>history</em> array. If the
 *         <code>title</code> and <code>story_title</code> fields are null or
 *         empty then ignore the record to add in the <em>history</em> array.
 * 
 * 
 *         Based on the <code>total_pages</code>count, fetch all the data
 *         (pagination), and repeat steps 4 and 5. Return the <em>history</em>
 *         array.
 * 
 *         The query response from the website is a JSON response with the
 *         following five fields:
 * 
 *         <code>page</code>: the current page <code>per_page</code>: the
 *         maximum number of results per page <code>total</code>: the total
 *         number of records in the search result <code>total_pages</code>: the
 *         total number of pages which must be queried to get all the results
 *         <code>data</code>: an array of JSON objects that contain article
 *         information
 * 
 *         <!-- <StartOfInputFormat> DO NOT REMOVE THIS LINE-->
 * 
 *         Schema
 * 
 *         There are 2 tables: ARTICLE_USERS and ARTICLES.
 * 
 *         ARTICLE_USERS
 * 
 *         ARTICLE_USERS
 * 
 *         Name Type Description
 * 
 *         id LONG This is the first column. unique identifier number for the
 *         article user
 * 
 *         username STRING the author-name
 * 
 *         about STRING description (about) of the author
 * 
 *         submission_count LONG total number of submission
 * 
 *         comment_count LONG total number of comments for the articles
 * 
 *         created_at STRING created time of the article
 * 
 *         updated_at LONG updated time of the article
 * 
 *         ARTICLES
 * 
 *         Name Type Description
 * 
 *         title STRING the title of the article
 * 
 *         url STRING URL of the article
 * 
 *         author STRING the author name of the article
 * 
 *         num_comments LONG total number of comments
 * 
 *         story_id LONG unique identifier number for the article
 * 
 *         story_title STRING an additional title for the article
 * 
 *         story_url STRING an additional URL for the article
 * 
 *         parent_id LONG unique identifier number of the parent article
 * 
 *         created_at LONG created time of the article
 * 
 *         <!-- </StartOfInputFormat> DO NOT REMOVE THIS LINE-->
 * 
 *         Sample Case 0
 * 
 *         Sample Input For Custom Testing epaga
 * 
 *         Sample Output
 * 
 * 
 *         Java developer / team leader at inetsoftware.de by day>p<iOS
 *         developer by
 *         night>p<http://www.mindscopeapp.com&lt;p&gt;http://inflightassistant.info&lt;p&gt;http://appstore.com/johngoering&lt;p&gt;[
 *         my public key: https://keybase.io/johngoering; my proof:
 *         https://keybase.io/johngoering/sigs/I1UIk7t3PjfB5v2GI-fhiOMvdkzn370_Z2iU5GitXa0
 *         ]&lt;p&gt;hnchat:oYwa7PJ4Yaf1Vw9Om4ju A Message to Our Customers “Was
 *         isolated from 1999 to 2006 with a 486. Built my own late 80s OS”
 *         Apple’s declining software quality
 * 
 *         Explanation
 * 
 *         ARTICLE_USERS
 * 
 *         id username about submission_count comment_count
 * 
 *         1 epaga Java developer / team leader at inetsoftware.de by
 *         day&lt;p&gt;iOS developer by
 *         night&lt;p&gt;http://www.mindscopeapp.com&lt;p&gt;http://inflightassistant.info&lt;p&gt;http://appstore.com/johngoering&lt;p&gt;[
 *         my public key: https://keybase.io/johngoering; my proof:
 *         https://keybase.io/johngoering/sigs/I1UIk7t3PjfB5v2GI-fhiOMvdkzn370_Z2iU5GitXa0
 *         ]&lt;p&gt;hnchat:oYwa7PJ4Yaf1Vw9Om4juJava developer / team leader at
 *         inetsoftware.de 654 197
 * 
 *         3
 * 
 *         saintamh
 * 
 *         4
 * 
 *         4
 * 
 *         5 olalonde
 *         olalonde@gmail.com&lt;p&gt;http://www.github.com/olalonde&lt;p&gt;CTO/Co-Founder @
 *         https://binded.com 1032 3045
 * 
 *         ARTICLE
 * 
 *         title url author num_comments story_id story_title story_url
 * 
 *         A Message to Our Customers
 * 
 *         null
 * 
 *         epaga 967 null null null “Was isolated from 1999 to 2006 with a 486.
 *         Built my own late 80s OS” null epaga 265 null null null Google Is
 *         Eating Our Mail null saintamh 685
 * 
 *         null null null
 * 
 *         null null
 * 
 *         epaga 705 null Apple’s declining software quality null
 * 
 *         Show HN: This up votes itself null olalonde 83 null null null
 * 
 *         Why I’m Suing the US Government null saintamh 305 null null null
 * 
 *         Sample Case 1
 * 
 *         Sample Input For Custom Testing saintamh
 * 
 *         Sample Output
 * 
 * 
 *         Google Is Eating Our Mail Why I’m Suing the US Government
 * 
 *         Explanation
 * 
 *         ARTICLE_USERS
 * 
 *         id username about submission_count comment_count
 * 
 *         1 epaga Java developer / team leader at inetsoftware.de by
 *         day&lt;p&gt;iOS developer by
 *         night&lt;p&gt;http://www.mindscopeapp.com&lt;p&gt;http://inflightassistant.info&lt;p&gt;http://appstore.com/johngoering&lt;p&gt;[
 *         my public key: https://keybase.io/johngoering; my proof:
 *         https://keybase.io/johngoering/sigs/I1UIk7t3PjfB5v2GI-fhiOMvdkzn370_Z2iU5GitXa0
 *         ]&lt;p&gt;hnchat:oYwa7PJ4Yaf1Vw9Om4juJava developer / team leader at
 *         inetsoftware.de 654 197 3 saintamh
 * 
 *         4 4
 * 
 *         5 olalonde
 *         olalonde@gmail.com&lt;p&gt;http://www.github.com/olalonde&lt;p&gt;CTO/Co-Founder @
 *         https://binded.com 1032 3045
 * 
 *         ARTICLE
 * 
 *         title url author num_comments story_id story_title story_url
 * 
 *         A Message to Our Customers
 * 
 *         null
 * 
 *         epaga
 * 
 *         967 null null null “Was isolated from 1999 to 2006 with a 486. Built
 *         my own late 80s OS” null epaga 265 null null null
 * 
 *         Google Is Eating Our Mail null saintamh
 * 
 *         685 null null null null null
 * 
 *         epaga 705 null Apple’s declining software quality null Show HN: This
 *         up votes itself null olalonde 83 null null null Why I’m Suing the US
 *         Government null saintamh 305 null null null
 */
public class AuthorInformation {

	class PageDTO {

		private String page;
		private Integer per_page;
		private Integer total;
		private Integer total_pages;
		private List<DataDTO> data;

		public String getPage() {
			return page;
		}

		public void setPage(String page) {
			this.page = page;
		}

		public Integer getPer_page() {
			return per_page;
		}

		public void setPer_page(Integer per_page) {
			this.per_page = per_page;
		}

		public Integer getTotal() {
			return total;
		}

		public void setTotal(Integer total) {
			this.total = total;
		}

		public Integer getTotal_pages() {
			return total_pages;
		}

		public void setTotal_pages(Integer total_pages) {
			this.total_pages = total_pages;
		}

		public List<DataDTO> getData() {
			return data;
		}

		public void setData(List<DataDTO> data) {
			this.data = data;
		}

	}

	class DataDTO {

		private String id;
		private String username;
		private String about;
		private String submitted;
		private String updated_at;
		private String submission_count;
		private String comment_count;
		private String created_at;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getAbout() {
			return about;
		}

		public void setAbout(String about) {
			this.about = about;
		}

		public String getSubmitted() {
			return submitted;
		}

		public void setSubmitted(String submitted) {
			this.submitted = submitted;
		}

		public String getUpdated_at() {
			return updated_at;
		}

		public void setUpdated_at(String updated_at) {
			this.updated_at = updated_at;
		}

		public String getSubmission_count() {
			return submission_count;
		}

		public void setSubmission_count(String submission_count) {
			this.submission_count = submission_count;
		}

		public String getComment_count() {
			return comment_count;
		}

		public void setComment_count(String comment_count) {
			this.comment_count = comment_count;
		}

		public String getCreated_at() {
			return created_at;
		}

		public void setCreated_at(String created_at) {
			this.created_at = created_at;
		}

	}

	class PageAuthorDTO {

		private String page;
		private Integer per_page;
		private Integer total;
		private Integer total_pages;
		private List<AuthorDTO> data;

		public String getPage() {
			return page;
		}

		public void setPage(String page) {
			this.page = page;
		}

		public Integer getPer_page() {
			return per_page;
		}

		public void setPer_page(Integer per_page) {
			this.per_page = per_page;
		}

		public Integer getTotal() {
			return total;
		}

		public void setTotal(Integer total) {
			this.total = total;
		}

		public Integer getTotal_pages() {
			return total_pages;
		}

		public void setTotal_pages(Integer total_pages) {
			this.total_pages = total_pages;
		}

		public List<AuthorDTO> getData() {
			return data;
		}

		public void setData(List<AuthorDTO> data) {
			this.data = data;
		}
	}

	class AuthorDTO {
		private String title;
		private String url;
		private String author;
		private String num_comments;
		private String story_id;
		private String story_title;
		private String story_url;
		private String parent_id;
		private String created_at;

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String getUrl() {
			return url;
		}

		public void setUrl(String url) {
			this.url = url;
		}

		public String getAuthor() {
			return author;
		}

		public void setAuthor(String author) {
			this.author = author;
		}

		public String getNum_comments() {
			return num_comments;
		}

		public void setNum_comments(String num_comments) {
			this.num_comments = num_comments;
		}

		public String getStory_id() {
			return story_id;
		}

		public void setStory_id(String story_id) {
			this.story_id = story_id;
		}

		public String getStory_title() {
			return story_title;
		}

		public void setStory_title(String story_title) {
			this.story_title = story_title;
		}

		public String getStory_url() {
			return story_url;
		}

		public void setStory_url(String story_url) {
			this.story_url = story_url;
		}

		public String getParent_id() {
			return parent_id;
		}

		public void setParent_id(String parent_id) {
			this.parent_id = parent_id;
		}

		public String getCreated_at() {
			return created_at;
		}

		public void setCreated_at(String created_at) {
			this.created_at = created_at;
		}
	}

	public static List<String> getAuthorHistory(String author) {
		try {
			String strUrl = "https://jsonmock.hackerrank.com/api/article_users?username=" + author;
			PageDTO page = jsonPerPage(strUrl);

			List<PageDTO> pageLst = new ArrayList<AuthorInformation.PageDTO>();
			pageLst.add(page);

			String strUrlAuthor = "https://jsonmock.hackerrank.com/api/articles?author=" + author;
			AuthorDTO authorDTO = jsonPerAuthor(strUrlAuthor);

			List<AuthorDTO> authorLst = new ArrayList<AuthorInformation.AuthorDTO>();
			authorLst.add(authorDTO);

			int k = 2;
			for (int i = 0; i < page.getTotal_pages() - 1; i++) {
				PageDTO pageTmp = jsonPerPage(strUrl + "&page=" + k);
				k++;
				pageLst.add(pageTmp);
			}
			List<String> authorDataLst = new ArrayList<String>();
			for (int i = 0; i < pageLst.size(); i++) {
				PageDTO pageTmp = pageLst.get(i);
				List<DataDTO> dtLst = pageTmp.getData();
				for (int j = 0; j < dtLst.size(); j++) {
					DataDTO data = dtLst.get(j);

					System.out.println(data);
					authorDataLst.add(data.getAbout());
				}
			}

		} catch (Exception e) {
		}
		return null;
	}

	private static PageDTO jsonPerPage(String strUrl) {
		PageDTO page = null;
		try {
			URL url = new URL(strUrl);
			HttpURLConnection huc = (HttpURLConnection) url.openConnection();
			huc.setRequestMethod("GET");
			System.out.println(huc.getResponseCode());

			StringBuilder response = new StringBuilder();
			try (BufferedReader br = new BufferedReader(new InputStreamReader(huc.getInputStream(), "utf-8"))) {
				String responseLine = null;
				while ((responseLine = br.readLine()) != null) {
					response.append(responseLine.trim());
				}
				System.out.println(response.toString());
			}
			Gson gson = new Gson();
			page = gson.fromJson(response.toString(), PageDTO.class);

		} catch (Exception e) {

		}
		return page;
	}

	private static AuthorDTO jsonPerAuthor(String strUrl) {
		AuthorDTO page = null;
		try {
			URL url = new URL(strUrl);// https://jsonmock.hackerrank.com/api/article_users?username=
			HttpURLConnection huc = (HttpURLConnection) url.openConnection();
			huc.setRequestMethod("GET");
			System.out.println(huc.getResponseCode());

			StringBuilder response = new StringBuilder();
			try (BufferedReader br = new BufferedReader(new InputStreamReader(huc.getInputStream(), "utf-8"))) {
				String responseLine = null;
				while ((responseLine = br.readLine()) != null) {
					response.append(responseLine.trim());
				}
				System.out.println(response.toString());
			}
			Gson gson = new Gson();
			page = gson.fromJson(response.toString(),AuthorDTO.class);

		} catch (Exception e) {

		}
		return page;
	}

	public static void main(String[] args) {
		getAuthorHistory("epaga");
	}

}
