/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.dbs </p>
 * <p>File Name: ProblemB.java </p>
 * <p>Create Date: 31-Jul-2020 </p>
 * <p>Create Time: 11:11:00 am </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.dbs;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * @author : Shantanu Sikdar
 *
 */
public class ParagraphFormatting {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {

		final Map<String, String> map = new HashMap<String, String>();
		map.put("Shantanu", "Sikdar");
		map.put("Shantanu", "jalali");
		map.put("as", "jalali");
		System.out.println(map);
		
		
		String str1 ="[Font1]Technology Stack\r\n" + 
				"[Font2]JRE 1.6, spring3.0, REST, Javascript, Jackson, Tomcat. "
				+ "we fetched the mentors from the various micro-services exposed by various LOB.\r\n" + 
				"[Font3]Technology Summary\r\n" + 
				"[Font2]Distributed system, using Google’s geolocation and postal-code service, Barclays branches are fetched. Further using the branch-id,\r\n"; 
		
		
		String[] inpt1 = str1.split("\r\n");
		
		String str2 = "Technology Stack\r\n" + 
				"JRE 1.6, spring3.0, REST, Javascript, Jackson, Tomcat.\r\n" + 
				"Technology Summary\r\n" + 
				"Distributed system, using Google’s geolocation and postal-code service, Barclays branches are fetched. Further using the branch-id, we fetched the mentors from the various micro-services exposed by various LOB.\r\n";
		
		String[] inpt2 = str2.split("\r\n");


		/*for (int i = 0; i < inpt1.length; i++) {
			String[] s1 = inpt1[i].split("]");
			String s2 = inpt2[i];
			StringBuilder sb = new StringBuilder();
			if(!s1[1].equalsIgnoreCase(s2)) {
				sb.append(s1[0]+"]"+s2);		
			}else {
				sb.append(s1[0]+"]"+s2);
			}
			System.out.println(sb.toString());
		}*/
		
		Scanner sc = new Scanner(str1);
		while(sc.hasNext()) {
			System.out.println(sc.nextLine());
		}

	}

	/*
	 * [Font1]Technology Stack[Font2]JRE 1.6, spring3.0, REST, Javascript, Jackson,
	 * Tomcat. we fetched the mentors from the various micro-services exposed by
	 * various LOB.
	 * 
	 * [Font3]Technology Summary[Font2]Distributed system, using Google’s
	 * geolocation and postal-code service, Barclays branches are fetched. Further
	 * using the branch-id,
	 * 
	 * 
	 * 
	 * 
	 * Technology StackJRE 1.6, spring3.0, REST, Javascript, Jackson, Tomcat.
	 * 
	 * Technology SummaryDistributed system, using Google’s geolocation and
	 * postal-code service, Barclays branches are fetched. Further using the
	 * branch-id, we fetched the mentors from the various micro-services exposed by
	 * various LOB.
	 */

}
