/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.globant </p>
 * <p>File Name: FindAnagram.java </p>
 * <p>Create Date: 30-Dec-2021 </p>
 * <p>Create Time: 11:12:59 AM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.globant;

import java.util.Arrays;

/**
 * @author : Shantanu Sikdar
 *
 * Find the elements which are anagram of a particular string from a
 * given array of strings String[] array = {"cat", "act", "abc", "pqr"},
 * String input = "tac" Expected output: {"cat", "act"}
 * 
 */
public class FindAnagram {

	/**
	 * @param args
	 * 
	 */
	public static void main(String[] args) {
		// System.out.println("Hello globant");
		String[] inptArrayStr = { "cat", "act", "abc", "pqr" };

		String inpt = "taC";
		char[] inptCharArr = inpt.toLowerCase().toCharArray();

		int size = inptCharArr.length;

		for (int i = 0; i < inptArrayStr.length; i++) {
			char[] inptChrArr = inptArrayStr[i].toLowerCase().toCharArray();

			if (inptChrArr.length == size) {

				Arrays.sort(inptChrArr);
				Arrays.sort(inptCharArr);

				if (new String(inptChrArr).equals(new String(inptCharArr))) {
					System.out.println(inptArrayStr[i]);
				}

				/*
				 * for(int j=0;j<inptCharArr.length;j++) { if(inptCharArr[j] == inptChrArr[i]) {
				 * 
				 * } }
				 */

			}

		}
	}

}
