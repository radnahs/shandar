/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.globant </p>
 * <p>File Name: GlobantTest.java </p>
 * <p>Create Date: 29-Dec-2021 </p>
 * <p>Create Time: 4:07:40 PM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.globant;

/**
 * @author : Shantanu Sikdar
 *	
 *	Merge to sorted array and the result will be also sorted.
 */
public class MergeSortedArray {

	public static void main(String[] args) {
    	int[] arr1 = {1,13,15,18,23};
        int[] arr2 = {12,20,28};
        int[] result = new int[arr1.length+arr2.length];
        
        int i=0, j=0, k=0;

        while(i< arr1.length && j< arr2.length) {
        	if(arr1[i] < arr2[j]) {
        		result[k++] = arr1[i++];
        	}else {
        		result[k++] = arr2[j++];
        	}
        }
        
        while(i< arr1.length) {
        	result[k++] = arr1[i++];
        	
        }
        
        while(j< arr2.length) {
        	result[k++] = arr2[j++];
        }
        
        System.out.println(result);
	}


}

//shopping cart
// 
