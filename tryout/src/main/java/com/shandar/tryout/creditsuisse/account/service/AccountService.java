/**
 * <p>Project: account </p>
 * <p>Package Name: com.cs.account.service </p>
 * <p>File Name: AccountService.java </p>
 * <p>Create Date: 21-Apr-2020 </p>
 * <p>Create Time: 9:13:01 am </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.creditsuisse.account.service;

import java.math.BigDecimal;

import com.shandar.tryout.creditsuisse.account.bean.Account;

/**
 * @author Shantanu Sikdar
 *
 */
public interface AccountService {

	public void transfer(Account from, Account to, BigDecimal amountToTransfer) ;
	
}
