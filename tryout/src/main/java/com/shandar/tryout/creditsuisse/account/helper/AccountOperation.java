/**
 * <p>Project: account </p>
 * <p>Package Name: com.cs.account.helper </p>
 * <p>File Name: AccountOperation.java </p>
 * <p>Create Date: 21-Apr-2020 </p>
 * <p>Create Time: 9:23:30 am </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.creditsuisse.account.helper;

import java.math.BigDecimal;

import com.shandar.tryout.creditsuisse.account.bean.Account;

/**
 * @author Shantanu Sikdar
 *
 */
public class AccountOperation {

	public Account withdraw(Account from, BigDecimal amountToTransfer) {
		System.out.printf("Withdraw %s, from Account No : %s having balance : %s \n", amountToTransfer.toString(),
				from.getAccountId(), from.getBalance().toString());
		from.setBalance(from.getBalance().subtract(amountToTransfer));
		System.out.printf("After withdrawal of %s, balance is %s \n", amountToTransfer, from.getBalance().toString());
		return from;
	}

	public Account deposit(Account to, BigDecimal amountToTransfer) {
		System.out.printf("Deposit %s, at Account No : %s having balance : %s \n", amountToTransfer.toString(),
				to.getAccountId(), to.getBalance().toString());
		to.setBalance(to.getBalance().add(amountToTransfer));
		System.out.printf("After deposit of %s, balance is %s \n", amountToTransfer, to.getBalance().toString());
		return to;
	}

}
