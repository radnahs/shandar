/**
 * <p>Project: account </p>
 * <p>Package Name: com.cs.account.controller.impl </p>
 * <p>File Name: AccountControllerImpl.java </p>
 * <p>Create Date: 21-Apr-2020 </p>
 * <p>Create Time: 9:47:10 am </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.creditsuisse.account.controller.impl;

import java.math.BigDecimal;

import com.shandar.tryout.creditsuisse.account.bean.Account;
import com.shandar.tryout.creditsuisse.account.controller.AccountController;
import com.shandar.tryout.creditsuisse.account.service.AccountService;
import com.shandar.tryout.creditsuisse.account.service.AccountTransactionService;
import com.shandar.tryout.creditsuisse.account.service.impl.AccountServiceImpl;


/**
 * @author Shantanu Sikdar
 *
 */
public class AccountControllerImpl implements AccountController {

	public static void main(String[] args) {
		Account from = new Account("ACC-123", new BigDecimal(0));
		Account to = new Account("ACC-456", new BigDecimal(100));
		BigDecimal amountToTransfer = new BigDecimal(100);

		AccountService accountService = new AccountServiceImpl(from, to);

		Thread t1 = new Thread(new AccountTransactionService(accountService, from, to, amountToTransfer));
		t1.start();

		Account from1 = new Account("ACC-789", new BigDecimal(1000));
		Thread t2 = new Thread(new AccountTransactionService(accountService, from1, from, amountToTransfer));
		t2.start();

	}

}
