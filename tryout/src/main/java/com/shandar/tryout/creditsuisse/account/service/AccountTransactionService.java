/**
 * <p>Project: account </p>
 * <p>Package Name: com.cs.account.helper </p>
 * <p>File Name: AccountTransaction.java </p>
 * <p>Create Date: 21-Apr-2020 </p>
 * <p>Create Time: 9:37:54 am </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.creditsuisse.account.service;

import java.math.BigDecimal;

import com.shandar.tryout.creditsuisse.account.bean.Account;

/**
 * @author Shantanu Sikdar
 *
 */
public class AccountTransactionService implements Runnable {

	private AccountService accountService;
	private Account from;
	private Account to;
	BigDecimal amountToTransfer;

	public AccountTransactionService(AccountService accountService, Account from, Account to, BigDecimal amountToTransfer) {
		this.accountService = accountService;
		this.from = from;
		this.to = to;
		this.amountToTransfer = amountToTransfer;
	}

	@Override
	public void run() {
		if (this.to.getAccountId().equals(this.from.getAccountId())) {
			System.out.print(" Payee and Payer are same");
		} else {
			accountService.transfer(from, to, amountToTransfer);

			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
