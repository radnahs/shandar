/**
 * <p>Project: account </p>
 * <p>Package Name: com.cs.account.bean </p>
 * <p>File Name: Account.java </p>
 * <p>Create Date: 21-Apr-2020 </p>
 * <p>Create Time: 9:14:47 am </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.creditsuisse.account.bean;

import java.math.BigDecimal;

/**
 * @author Shantanu Sikdar
 *
 */
public class Account {

	private String accountId;
	private BigDecimal balance;

	public Account(String accountId, BigDecimal balance) {
		this.accountId = accountId;
		this.balance = balance;
	}

	/**
	 * @return the accountId
	 */
	public String getAccountId() {
		return accountId;
	}

	/**
	 * @param accountId the accountId to set
	 */
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	/**
	 * @return the balance
	 */
	public BigDecimal getBalance() {
		return balance;
	}

	/**
	 * @param balance the balance to set
	 */
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

}
