/**
 * <p>Project: account </p>
 * <p>Package Name: com.cs.account.service.impl </p>
 * <p>File Name: AccountServiceImpl.java </p>
 * <p>Create Date: 21-Apr-2020 </p>
 * <p>Create Time: 9:16:17 am </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.creditsuisse.account.service.impl;

import java.math.BigDecimal;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import com.shandar.tryout.creditsuisse.account.bean.Account;
import com.shandar.tryout.creditsuisse.account.helper.AccountOperation;
import com.shandar.tryout.creditsuisse.account.service.AccountService;


/**
 * @author Shantanu Sikdar
 *
 */
public class AccountServiceImpl implements AccountService {

	Account from;
	Account to;
	private Lock accountLock;
	private Condition availableFund;

	public AccountServiceImpl(Account from, Account to) {
		this.from = from;
		this.to = to;
		accountLock = new ReentrantLock();
		availableFund = accountLock.newCondition();
	}

	@Override
	public void transfer(Account from, Account to, BigDecimal amountToTransfer) {
		accountLock.lock();
		try {
			if (from.getBalance().compareTo(amountToTransfer) < 0) {
				System.out.println("Less balance at " + from.getAccountId() + ", thus waiting for deposit...");
				availableFund.await();
			}
			AccountOperation ao = new AccountOperation();
			ao.withdraw(from, amountToTransfer);
			ao.deposit(to, amountToTransfer);

			String message = "%s transfered %s from %s to %s. Total balance: %s\n";
			String threadName = Thread.currentThread().getName();
			System.out.printf(message, threadName, amountToTransfer.toString(), from.getAccountId(), to.getAccountId(),
					getTotalBalance(from, to));

			availableFund.signalAll();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			accountLock.unlock();
		}
	}

	private String getTotalBalance(Account from, Account to) {
		accountLock.lock();
		try {
			System.out.printf("Amount at From Account %s is %s \n", from.getAccountId(), from.getBalance().toString());
			System.out.printf("Amount at to Account %s is %s \n", to.getAccountId(), to.getBalance().toString());
			BigDecimal total = from.getBalance().add(to.getBalance());
			return total.toString();
		} finally {
			accountLock.unlock();
		}
	}

}
