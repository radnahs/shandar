/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.barclays </p>
 * <p>File Name: IndexAndValueComparison.java </p>
 * <p>Create Date: Feb 17, 2016 </p>
 * <p>Create Time: 3:08:39 PM </p>
 * <p>Copyright: Copyright (c) 2014</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.tryout.misc;

/**
 * @author Shantanu Sikdar
 *
 */
public class IndexAndValueComparison {

	public static void main(String[] args) {
		maxDifferenceValuePair();
		maxDistanceOfIndexHavingHighestValDifference();
	}
	
	private static void maxDifferenceValuePair(){
		int[] intArr = {3,2,6,8,9};
		for(int i=0;i<intArr.length-1;i++) {
			for (int j = 1; j < intArr.length; j++) {
				if(intArr[i]<intArr[j]) {
					System.out.println("arr["+i+"] = "+intArr[i]+" =< "+ "arr["+j+"] = "+ intArr[j]);
				}
			}
		}
	}
	
	private static void maxDistanceOfIndexHavingHighestValDifference(){
		int[] intArr = {3,2,6,8,11};
		int indxDis = 0;
		int indxValDis = 0;
		for(int i=0;i<intArr.length-1;i++) {
			for (int j = 1; j < intArr.length; j++) {
				if(intArr[i]<intArr[j]) {
					if(indxValDis<intArr[j]-intArr[i] && indxDis<j-i) {
						indxValDis= intArr[j]-intArr[i];
						indxDis=j-i;
					}
				}
			}
		}
		System.out.println(indxDis);
	}
	
}
