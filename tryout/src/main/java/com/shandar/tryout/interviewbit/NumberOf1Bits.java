/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.interviewbit </p>
 * <p>File Name: NumberOf1Bits.java </p>
 * <p>Create Date: 11-Aug-2020 </p>
 * <p>Create Time: 2:32:45 am </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.interviewbit;

/**
 * @author : Shantanu Sikdar
 * 
 *         https://www.interviewbit.com/problems/number-of-1-bits/
 *
 */
public class NumberOf1Bits {

	public static void main(String[] args) {
		binaryRepresentaion(25);
	}

	static private int binaryRepresentaion(long a) {
		int count = 0;
		long m;
		// String x = "";
		while (a > 0) {
			m = a % 2;
			if (m == 1) {
				count++;
			}
			// x = m + "" + x;
			a = a / 2;
		}
		// System.out.println("Binary Number = " + x);
		System.out.println("Num of 1s = " + count);
		return count;
	}

}
