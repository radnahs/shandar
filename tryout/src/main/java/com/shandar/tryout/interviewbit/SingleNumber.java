/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.interviewbit </p>
 * <p>File Name: SingleNumber.java </p>
 * <p>Create Date: 11-Aug-2020 </p>
 * <p>Create Time: 2:52:10 am </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.interviewbit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * @author : Shantanu Sikdar
 *
 *         https://www.interviewbit.com/problems/single-number/
 */
public class SingleNumber {

	public static void main(String[] args) {
		final List<Integer> A1 = Arrays.asList(1, 2, 2, 3, 1);

		final List<Integer> A = Arrays.asList(723, 256, 668, 723, 140, 360, 597, 233, 128, 845, 737, 804, 986, 701, 906,
				512, 845, 510, 510, 227, 430, 701, 366, 946, 464, 619, 946, 627, 209, 771, 424, 555, 959, 711, 530, 937,
				716, 261, 505, 658, 706, 140, 511, 277, 396, 233, 819, 196, 475, 906, 583, 261, 147, 658, 517, 197, 196,
				702, 944, 711, 128, 555, 149, 483, 530, 291, 716, 258, 430, 464, 601, 749, 149, 415, 802, 573, 627, 771,
				660, 601, 360, 986, 291, 51, 415, 51, 227, 258, 937, 366, 923, 669, 33, 517, 417, 702, 475, 706, 110,
				417, 275, 804, 500, 473, 746, 973, 669, 275, 973, 147, 817, 657, 277, 923, 144, 660, 197, 511, 793, 893,
				944, 505, 322, 817, 586, 512, 322, 668, 33, 424, 962, 597, 144, 746, 345, 753, 345, 269, 819, 483, 368,
				802, 573, 962, 583, 615, 208, 209, 269, 749, 256, 657, 619, 893, 959, 473, 753, 299, 396, 299, 500, 368,
				586, 110, 793, 737, 615);

		System.out.println(singleNumberMap(A));
		System.out.println(singleNumberList(A));
	}

	//score 85/275
	static private int singleNumberList(final List<Integer> A) {
		List<Integer> B = new ArrayList<Integer>(A.size() / 2 + 1);
		for (Integer a : A) {
			if (B.contains(a)) {
				B.remove(a);
			} else {
				B.add(a);
			}
		}
		return B.get(0);
	}

	//score 82/275
	static private int singleNumberMap(final List<Integer> A) {
		Map<Integer, Integer> mapForCnt = new HashMap<Integer, Integer>();
		for (int i = 0; i < A.size(); i++) {
			if (mapForCnt.containsKey(A.get(i))) {
				mapForCnt.remove(A.get(i));
			} else {
				mapForCnt.put(A.get(i), i);
			}
		}
		int retVal = 0;
		for (Entry<Integer, Integer> mapKey : mapForCnt.entrySet()) {
			retVal = mapKey.getKey();
		}
		return retVal;
	}

}
