/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.wissen.samples </p>
 * <p>File Name: Banana.java </p>
 * <p>Create Date: 29-Dec-2022 </p>
 * <p>Create Time: 8:48:34 AM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.wissen.samples;

/**
 * @author : Shantanu Sikdar
 *
 */
public class Banana extends Apple {

	public void xyz() {
		System.out.println("in Banana");
	}

}
