/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.wissen </p>
 * <p>File Name: HackerEarthTest.java </p>
 * <p>Create Date: 06-Apr-2020 </p>
 * <p>Create Time: 8:19:46 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.wissen;

import java.util.ArrayList;
import java.util.List;

/**
 * @author : Shantanu Sikdar
 *
 */
public class NearestToElement {

	public static void main(String[] args) {
		int[] A = new int[] { 3, 5, 1, 1, 1, 1, 3, 1, 0 };
		int temp = A[0];
		List<Integer> lsInt = new ArrayList<Integer>();
		for (int i = 0; i < A.length; i++) {
			if (Math.abs(A[i] - 0) <= Math.abs(temp)) {
				temp = A[i] - 0;
				lsInt.add(temp);
			}
		}
		if (lsInt.contains(Math.abs(temp))) {
			System.out.println(Math.abs(temp));
		} else {
			System.out.println(temp);
		}
	}

}
