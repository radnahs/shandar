/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.wissen.samples </p>
 * <p>File Name: SquaresOfOddNumbersInList.java </p>
 * <p>Create Date: 03-Jan-2023 </p>
 * <p>Create Time: 8:20:29 PM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.wissen.samples;

import java.util.Arrays;
import java.util.List;

/**
 * @author : Shantanu Sikdar
 * @Description:
 * 
 *               <pre>
 * 	                 Given a List of integers (List<Integer>), write code in Java 8 style 
 * 					  to get the sum of the squares of all the odd numbers in the array.
 *                   Evaluation Parameter 
 *                   1. Used stream() correctly.
 *                   2. Used filter() correctly.
 *                   3. Used map() correctly.
 *                   4. Used sum() or reduce() correctly.
 *               </pre>
 */
public class SquaresOfOddNumbersInList {

	public static void main(String[] args) {
		List<Integer> lstInt = Arrays.asList(1, 3, 4, 5, 6, 7, 10, 12, 3, 9); // 1+9+25+49+9+81=174
		int sum = lstInt.stream().filter(n -> n % 2 == 1).map(n -> n * n).reduce(0, Integer::sum);
		System.out.println(sum);
	}

}
