/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.wissen.samples </p>
 * <p>File Name: AppleBananaMain.java </p>
 * <p>Create Date: 29-Dec-2022 </p>
 * <p>Create Time: 8:50:13 AM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.wissen.samples;

/**
 * @author : Shantanu Sikdar
 * @description :
 * 
 *              <pre>
 *
 * 				             	
 *              </pre>
 * 
 */
public class AppleBananaMain {

	public static void main(String[] args) {
		// testMethod('1');//the method is not static

		Apple a = new Apple();
		a.xyz();
		Banana b = new Banana();
		b.xyz();
		Apple ab = new Banana();
		ab.xyz();
		Banana ba = (Banana) new Apple();
		ba.xyz();

	}

	private void testMethod(char c) {
		System.out.println(c);
	}

}
