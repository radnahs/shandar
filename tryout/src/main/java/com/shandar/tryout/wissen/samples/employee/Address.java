/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.wissen.samples.employee </p>
 * <p>File Name: Address.java </p>
 * <p>Create Date: 03-Jan-2023 </p>
 * <p>Create Time: 1:01:53 AM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.wissen.samples.employee;

import java.util.Objects;

/**
 * @author : Shantanu Sikdar
 *
 */
public class Address {

	private String house;
	private String locality;
	private String city;
	private int pin;

	/**
	 * @param house
	 * @param locality
	 * @param city
	 * @param pin
	 */
	public Address(String house, String locality, String city, int pin) {
		super();
		this.house = house;
		this.locality = locality;
		this.city = city;
		this.pin = pin;
	}

	public String getHouse() {
		return house;
	}

	public void setHouse(String house) {
		this.house = house;
	}

	public String getLocality() {
		return locality;
	}

	public void setLocality(String locality) {
		this.locality = locality;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public int getPin() {
		return pin;
	}

	public void setPin(int pin) {
		this.pin = pin;
	}

	@Override
	public int hashCode() {
		return Objects.hash(city, house, locality, pin);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Address other = (Address) obj;
		return Objects.equals(city, other.city) && Objects.equals(house, other.house)
				&& Objects.equals(locality, other.locality) && pin == other.pin;
	}

	@Override
	public String toString() {
		return "Address [house=" + house + ", locality=" + locality + ", city=" + city + ", pin=" + pin + "]";
	}

}
