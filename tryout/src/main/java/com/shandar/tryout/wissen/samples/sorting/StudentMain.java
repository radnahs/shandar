/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.wissen.samples.sorting </p>
 * <p>File Name: StudentMain.java </p>
 * <p>Create Date: 03-Jan-2023 </p>
 * <p>Create Time: 11:57:37 AM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.wissen.samples.sorting;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author : Shantanu Sikdar
 * @Description :
 * 
 *              <pre>
 * You are using a class from a library (say Student). 
 * You have a list of Student objects. You need to 
 * sort this list based on first name. 
 * How will you do it? 
 * 
 * Constraint: (You do not have the ability to change 
 * the source code of the Student class)
 *              </pre>
 */
public class StudentMain {

	public static void main(String[] args) {

		List<Student> lstStdnt = new ArrayList<Student>();
		Student stdnt1 = new Student(101, "Krishanu", "Sikdar");
		Student stdnt2 = new Student(103, "Nillohit", "Talukdar");
		Student stdnt3 = new Student(104, "Indrajit", "Das");
		Student stdnt4 = new Student(102, "Rajdeep", "Mandal");

		lstStdnt.add(stdnt1);
		lstStdnt.add(stdnt2);
		lstStdnt.add(stdnt3);
		lstStdnt.add(stdnt4);

		System.out.println(lstStdnt);

		sortUsingCollections(lstStdnt);

		sortUsingStreams(lstStdnt);
	}

	private static void sortUsingCollections(List<Student> lstStdnt) {
		Collections.sort(lstStdnt, (s1, s2) -> {
			return s1.getfName().compareTo(s2.getfName());
		});
		System.out.println(lstStdnt);
	}

	private static void sortUsingStreams(List<Student> lstStdnt) {
		lstStdnt.stream().sorted((s1, s2) -> {
			return s1.getfName().compareTo(s2.getfName());
		}).forEach(System.out::println);
	}

}
