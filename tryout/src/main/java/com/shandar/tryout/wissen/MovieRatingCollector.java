/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.wissen </p>
 * <p>File Name: MovieRatingStatistics.java </p>
 * <p>Create Date: 06-May-2020 </p>
 * <p>Create Time: 8:45:29 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.wissen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author : Shantanu Sikdar
 *
 */

public class MovieRatingCollector {

	public static class RatingCollectorImpl implements RatingCollector {
		Map<String, List<Double>> mapRat = new HashMap<String, List<Double>>();

		@Override
		public void putNewRating(String movie, double rating) {
			// YOUR CODE HERE
			if (mapRat.containsKey(movie)) {
				List<Double> rats = mapRat.get(movie);
				rats.add(rating);
				mapRat.put(movie, rats);
			} else {
				List<Double> rats = new ArrayList<Double>();
				rats.add(rating);
				mapRat.put(movie, rats);
			}
			System.out.println(mapRat);
		}

		@Override
		public double getAverageRating(String movie) {
			// YOUR CODE HERE
			List<Double> rats = mapRat.get(movie);
			int cnt = getRatingCount(movie);
			double value = 0;
			for (Double dbl1 : rats) {
				value+=dbl1;
			}
			return value/cnt;
		}

		@Override
		public int getRatingCount(String movie) {
			// YOUR CODE HERE
			return mapRat.get(movie).size();
		}
	}

	////////////////// DO NOT MODIFY BELOW THIS LINE ///////////////////

	public interface RatingCollector {
		// This is an input. Make note of this rating. Rating can be a double number
		// between 0 and 100.
		void putNewRating(String movie, double rating);

		// Get the average rating
		double getAverageRating(String movie);

		// Get the total number of ratings received for the movie
		int getRatingCount(String movie);
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int numLines = Integer.parseInt(scanner.nextLine());
		int currentLine = 0;
		while (currentLine++ < numLines) {
			final RatingCollector stats = new RatingCollectorImpl();
			final Set<String> movies = new TreeSet<>();

			String line = scanner.nextLine();
			String[] inputs = line.split(",");
			for (int i = 0; i < inputs.length; ++i) {
				String[] tokens = inputs[i].split(" ");
				final String symbol = tokens[0];
				movies.add(symbol);
				final double price = Double.parseDouble(tokens[1]);

				stats.putNewRating(symbol, price);

			}

			for (String movie : movies) {
				System.out.println(
						String.format("%s %.4f %d", movie, stats.getAverageRating(movie), stats.getRatingCount(movie)));
			}

		}
		scanner.close();

	}
}
