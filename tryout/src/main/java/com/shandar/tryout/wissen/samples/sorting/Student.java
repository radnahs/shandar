/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.wissen.samples.sorting </p>
 * <p>File Name: Student.java </p>
 * <p>Create Date: 03-Jan-2023 </p>
 * <p>Create Time: 10:48:18 AM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.wissen.samples.sorting;

/**
 * @author : Shantanu Sikdar
 *
 */
public class Student {

	private int rollNo;
	private String fName;
	private String lName;

	/**
	 * @param rollNo
	 * @param fName
	 * @param lName
	 */
	public Student(int rollNo, String fName, String lName) {
		super();
		this.rollNo = rollNo;
		this.fName = fName;
		this.lName = lName;
	}

	public int getRollNo() {
		return rollNo;
	}

	public void setRollNo(int rollNo) {
		this.rollNo = rollNo;
	}

	public String getfName() {
		return fName;
	}

	public void setfName(String fName) {
		this.fName = fName;
	}

	public String getlName() {
		return lName;
	}

	public void setlName(String lName) {
		this.lName = lName;
	}

	@Override
	public String toString() {
		return "Student [rollNo=" + rollNo + ", fName=" + fName + ", lName=" + lName + "] \n";
	}

}
