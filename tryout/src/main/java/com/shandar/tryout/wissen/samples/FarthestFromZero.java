/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.wissen.samples </p>
 * <p>File Name: FarthestFromZero.java </p>
 * <p>Create Date: 29-Dec-2022 </p>
 * <p>Create Time: 12:45:14 AM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.wissen.samples;

import java.util.Arrays;
import java.util.TreeSet;

/**
 * @author : Shantanu Sikdar
 * @description:
 * 
 *               <pre>
 *		Given an integer array A of size N elements
 * 		Write a program to print the farthest element from 0.
 *  	Number with highest absolute value negative or positive.
 * 
 *               </pre>
 */
public class FarthestFromZero {

	public static void main(String[] args) {
		int[] arr = { -10, -13, -11, 1, 2, 10, 0, 12 };
		wayOne(arr);
		wayTwo(arr);
		wayThree(arr);
	}

	private static void wayOne(int[] arr) {
		TreeSet<Integer> ts = new TreeSet<>(); // donot use Set as a type as Set donot have the first and last method
		for (int value : arr) {
			ts.add(value);
		}

		System.out.println(ts);
		int minValue = ts.first();
		int maxValue = ts.last();

		if (Math.abs(minValue) >= Math.abs(maxValue)) {
			System.out.println(minValue);
		} else {
			System.out.println(maxValue);
		}
	}

	private static void wayTwo(int[] arr) {
		Arrays.sort(arr);
		if (Math.abs(arr[0]) >= Math.abs(arr[arr.length - 1])) {
			System.out.println(arr[0]);
		} else {
			System.out.println(arr[arr.length - 1]);
		}

	}

	private static void wayThree(int[] arr) {
		int min = 0, max = 0;
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] < min) {
				min = arr[i];
			}
			if (arr[i] > max) {
				max = arr[i];
			}
		}
		System.out.println(min + " " + max);
		if (Math.abs(min) >= Math.abs(max)) {
			System.out.println(min);
		} else {
			System.out.println(max);
		}

	}

}
