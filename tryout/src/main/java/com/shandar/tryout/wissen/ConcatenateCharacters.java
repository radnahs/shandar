/**
 * <p>Project: com.shandar.tryout.wissen </p>
 * <p>Package Name: com.shandar.tryout.wissen </p>
 * <p>File Name: Problem2.java </p>
 * <p>Create Date: Jun 23, 2017 </p>
 * <p>Create Time: 5:46:34 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.tryout.wissen;

/**
 * @author: Shantanu Sikdar
 * 
 * Sample 1 
 * Input : 
 * 4
 * S h a n
 * 
 * output
 * Shan
 * 
 * 
 */
public class ConcatenateCharacters {

	public static void main(String[] args) {
		String ss1 = "4";
		int i = Integer.parseInt(ss1);
		if(i >0) {
			String str = "s h a n";
			String ss = str.replace(" " , "");
			System.out.println(ss);
		}
		
	}

}
