/**
 * <p>Project: rytry </p>
 * <p>Package Name: com.shandar.tryout.wissen </p>
 * <p>File Name: PrintThreadDemo.java </p>
 * <p>Create Date: 05-Jan-2023 </p>
 * <p>Create Time: 2:58:25 PM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.wissen;

/**
 * @author : Shantanu Sikdar
 * @Description : ThreadDemo
 */
public class PrintThreadDemo {

	public static void main(String[] args) {
		
		Thread t1 = new Thread(new PrintThread(1),"thrd-1");
		Thread t2 = new Thread(new PrintThread(2),"thrd-2");
		Thread t3 = new Thread(new PrintThread(0),"thrd-3");
		
		t1.start();
		t2.start();
		t3.start();
	}

}
