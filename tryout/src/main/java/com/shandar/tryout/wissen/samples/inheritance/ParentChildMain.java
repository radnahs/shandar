/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.wissen.samples.inheritance </p>
 * <p>File Name: ParentChildMain.java </p>
 * <p>Create Date: 03-Jan-2023 </p>
 * <p>Create Time: 7:27:17 PM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.wissen.samples.inheritance;

/**
 * @author : Shantanu Sikdar
 * 
 *         <pre>
 *		We have a Parent class with 2 methods walk and run. We have a child class 
 *		which overrides both the methods. Both child class methods just call 
 *		their respective Super implementation.
 *      Parent class run method calls walk().
 *      class Parent
 *      Parent p = new Child();
 *      p.run();
 *      Tell the order in which each method is called
 *      1. Correctly tells the order: child.run -> parent.run -> child.walk -> parent.walk.
 *
 *         </pre>
 */
public class ParentChildMain {

	public static void main(String[] args) {
		Parent p1 = new Child();
		p1.run();
	}

}
