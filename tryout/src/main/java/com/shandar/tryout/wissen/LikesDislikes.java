/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.wissen </p>
 * <p>File Name: LikesDislikes.java </p>
 * <p>Create Date: 06-May-2020 </p>
 * <p>Create Time: 6:51:00 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.wissen;

/**
 * @author : Shantanu Sikdar
 *
 */
public class LikesDislikes {

	public static void main(String[] args) {
		char[] A = "010101".toCharArray();
		char[] P = "101101".toCharArray();
		int count=0;
		for (int i = 0; i < P.length; i++) {
			if(A[i]==P[i]) {
				count++;
			}
		}
		System.out.println(count);
		
	}

}
