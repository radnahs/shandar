/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.wissen.samples </p>
 * <p>File Name: SplitAnArray.java </p>
 * <p>Create Date: 02-Jan-2023 </p>
 * <p>Create Time: 9:20:59 PM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.wissen.samples;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author : Shantanu Sikdar
 * 
 *         <pre>
 *			 Split an array into chunks with a specified size. 
 *			 Code Template - https://onlinegdb.com/Ptod_C-qS 
 *			 Ask candidate to fork this code Ensure the candidate 
 *			 fork is given the detailed feedback Ensure the 
 *			 candidate output is copy pasted into the detailed 
 *			 feedback 
 *	
 *			 Example - 
 *			 	array = [1,2,3,4,5] 
 *			 	chunkSize = 1 
 *			 	o/p: [1] [2] [3] [4] [5] 
 *			 	chunkSize = 2 
 *			 	o/p: [1, 2] [3, 4] [5] 
 *			 	chunkSize = 3 
 *			 	o/p: [1, 2, 3] [4, 5] 
 *			 	chunkSize = 4 
 *			 	[1, 2, 3, 4] [5] 
 *			 	chunkSize = 5 
 *			 	[1, 2, 3, 4, 5] 
 *			 	chunkSize = 6 
 *			 	[1, 2, 3, 4, 5]
 *         </pre>
 */
public class SplitAnArray {

	public static void main(String[] args) {
		int[] original = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		// int[] original = {1, 2};
		int splitSize = 3;

		/**
		 * <pre>
		 * expected Output 
		 * [0, 1, 2] 
		 * [3, 4, 5] 
		 * [6, 7, 8] ` 
		 * [9]
		 * </pre>
		 */

		List<int[]> list = splitArray(original, splitSize);
		list.forEach(splitArray -> System.out.println(Arrays.toString(splitArray)));
	}

	public static List<int[]> splitArray(int[] array, int splitSize) {
		List<int[]> lstIntArr = new ArrayList<int[]>();
		if (array.length > splitSize) {
			for (int i = 0; i < array.length; /* i++ */) {
				int[] arrCnt = new int[array.length - i > splitSize ? splitSize : array.length - i];
				for (int j = 0; j < splitSize; j++) {
					if (i < array.length) {
						arrCnt[j] = array[i];
						i++;
					} else {
						break;
					}
				}
				/* i--; */
				lstIntArr.add(arrCnt);
			}
		} else {
			lstIntArr.add(array);
		}
		return lstIntArr;
	}

}
