/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.wissen.samples </p>
 * <p>File Name: CommonCharcters.java </p>
 * <p>Create Date: 02-Jan-2023 </p>
 * <p>Create Time: 10:04:04 PM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.wissen.samples;

/**
 * @author : Shantanu Sikdar
 * @description:
 * 
 *               <pre>
 *               Find common characters between strings Code snippet - 
 *               https://onlinegdb.com/2_k3E2v6H 
 *               Ask the candidate to fork the above code The candidates 
 *               response url must be added in the comments 
 *               Example: 
 *               string1 = "abc"; string2 = "dcb"; 
 *               common characters = "bc";
 *               </pre>
 */
public class CommonCharcters {

	public static void main(String[] args) {
		String first = "abcbcd";
		String second = "cdefef";
		String common = commonCharacters(first, second);
		System.out.println("common characters in '" + first + "' and '" + second + "' are '" + common + "'");

	}

	public static String commonCharacters(String a, String b) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < a.length(); i++) {
			for (int j = 0; j < b.length(); j++) {
				if (a.charAt(i) == b.charAt(j)) {
					sb.append(a.charAt(i));
				}
			}
		}
		return sb.toString();
	}

}
