/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.wissen </p>
 * <p>File Name: MinimumParkingSpace.java </p>
 * <p>Create Date: 25-Apr-2020 </p>
 * <p>Create Time: 10:09:57 am </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.wissen;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

/**
 * @author : Shantanu Sikdar
 *
 */
public class MinimumParkingSpace {

	static int minParkingSpaces(int[][] parkingStartEndTimes) {
		// YOUR CODE HERE
		int count =0;
		for (int i = 0; i < parkingStartEndTimes.length; i++) {
			for (int j = 1; j < parkingStartEndTimes.length; j++) {
				if(parkingStartEndTimes[i][1]>parkingStartEndTimes[j][0]) {
					count++;
					break;
				}else if(parkingStartEndTimes[j][1]>parkingStartEndTimes[i][0]) {
					count++;
					break;
				}else {
					
				}
				
			}
		}
		
		return count;
	}
	

	// DO NOT MODIFY ANYTHING BELOW THIS LINE!!

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter wr = new PrintWriter(System.out);
		int n = Integer.parseInt(br.readLine().trim());
		int[][] parkingStartEndTimeList = new int[n][2];
		String[] parkingStartEndTimes = br.readLine().split(" ");
		for (int i = 0; i < n; i++) {
			String[] parkingStartEndTime = parkingStartEndTimes[i].split(",");
			for (int j = 0; j < parkingStartEndTime.length; j++) {
				parkingStartEndTimeList[i][j] = Integer.parseInt(parkingStartEndTime[j]);
			}
		}

		int out = minParkingSpaces(parkingStartEndTimeList);
		System.out.println(out);

		wr.close();
		br.close();
	}

}
