/**
 * <p>Project: rytry </p>
 * <p>Package Name: com.shandar.tryout.wissen </p>
 * <p>File Name: PrintThread.java </p>
 * <p>Create Date: 05-Jan-2023 </p>
 * <p>Create Time: 3:02:48 PM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.wissen;

/**
 * @author : Shantanu Sikdar
 * @Description :
 */
public class PrintThread implements Runnable {

	static final int limit = 10;
	static int number = 1;
	int remainder;
	static final Object lock = new Object();

	PrintThread(int remainder) {
		this.remainder = remainder;
	}

	@Override
	public void run() {
		while (number < limit - 1) {
			synchronized (lock) {
			//synchronized (this) {	
				while (number % 3 != remainder) {
					try {
						lock.wait();
						//wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				System.out.println(Thread.currentThread().getName() + " - " + number);
				number++;
				
				System.out.println("Object lock is : " + lock);
				Class abc = lock.getClass();
		        System.out.println("Class of Object obj is : " + abc);

		        
		        lock.notifyAll();
		        //notifyAll();
			}
		}
	}
}
