/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.wissen.samples.immutability </p>
 * <p>File Name: ImmutablePersonMain.java </p>
 * <p>Create Date: 03-Jan-2023 </p>
 * <p>Create Time: 3:42:53 PM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.wissen.samples.immutability;

import java.util.ArrayList;
import java.util.List;

/**
 * @author : Shantanu Sikdar
 * @Description :
 * 
 *              <pre>
 * Consider a class Person with two attributes - 
 * String name and List<String> degrees. 
 * How will you make this class immutable. 
 *  
 * Ask what are the advantages of immutable classes
 * Evaluation Parameter- 
 * Make all fields private and final,
 * Remove the setter methods, 
 * Return either a new copy of degrees or an unmodifiableList 
 * in the the getter method, 
 * In the constructor, make a copy of the List argument passed, 
 * Make the class final, 
 * Knows the benefits of immutable class in multi-threaded 
 * programs and as a good design practice.
 *              </pre>
 *
 */
public class ImmutablePersonMain {

	public static void main(String[] args) {
		List<String> lstDegrees = new ArrayList<String>();
		lstDegrees.add("BTech");
		lstDegrees.add("PGDM");

		ImmutablePerson immutablePerson1 = new ImmutablePerson("Krishanu", lstDegrees);

		System.out.println(immutablePerson1.getName());
		System.out.println(immutablePerson1.getDegrees());

		lstDegrees.add("PhD");
		System.out.println(immutablePerson1.getDegrees());

		lstDegrees.add("Post Doctoral");
		System.out.println(immutablePerson1.getDegrees());

	}

}
