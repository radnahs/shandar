/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.wissen.samples.employee </p>
 * <p>File Name: Employee.java </p>
 * <p>Create Date: 03-Jan-2023 </p>
 * <p>Create Time: 12:59:12 AM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.wissen.samples.employee;

import java.util.Comparator;
import java.util.Objects;

/**
 * @author : Shantanu Sikdar
 *
 */
public class Employee implements Comparable<Employee> {

	private int id;
	private String fname;
	private String lname;
	private Address address;

	/**
	 * @param id
	 * @param fname
	 * @param lname
	 * @param address
	 */
	public Employee(int id, String fname, String lname, Address address) {
		super();
		this.id = id;
		this.fname = fname;
		this.lname = lname;
		this.address = address;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	@Override
	public int hashCode() {
		return Objects.hash(address, fname, id, lname);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		return Objects.equals(address, other.address) && Objects.equals(fname, other.fname) && id == other.id
				&& Objects.equals(lname, other.lname);
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", fname=" + fname + ", lname=" + lname + ", \t address=" + address + "] \n";
	}

	/**
	 * Sorted by id
	 */
	@Override
	public int compareTo(Employee e) {
		return e.getId() >= this.getId() ? (e.getId() == this.getId() ? 0 : 1) : -1;
	}

}
