/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.wissen.samples.inheritance </p>
 * <p>File Name: Child.java </p>
 * <p>Create Date: 03-Jan-2023 </p>
 * <p>Create Time: 7:26:09 PM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.wissen.samples.inheritance;

/**
 * @author : Shantanu Sikdar
 *
 */
public class Child extends Parent{

	@Override
	public void walk() {
		System.out.println("Child walk()");
		super.walk();
	}

	@Override
	public void run() {
		System.out.println("Child run()");
		super.run();
	}

}
