/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.wissen.samples </p>
 * <p>File Name: PairHavingGivenDifference.java </p>
 * <p>Create Date: 03-Jan-2023 </p>
 * <p>Create Time: 8:40:22 PM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.wissen.samples;

import java.util.HashSet;
import java.util.Set;

/**
 * @author : Shantanu Sikdar
 *         https://www.techiedelight.com/find-pairs-with-given-difference-array/
 * 
 *         <pre>
 *
 *	        Given an array of n integers and a number k, find the pairs of numbers 
 *			in the array such that the difference between the pair is k. 
 *			Find the optimal solution with and without extra storage
 *       	 	1. Gave O(n) solution with extra storage by using HashMap.
 *    			2. Gave O(n log n) solution without extra storage by using sorting.
 *    			3. If the array is sorted, is able to give O(n) solution by using iteration with 2 pointers.
 *         </pre>
 *
 */
public class PairHavingGivenDifference {

	public static void main(String[] args) {
		int[] arr = { 1, 5, 12, 2, 2, 5, 5, 4, 13, 16, 7 };
		int diff = 3;
		findPair(arr, diff);
	}

	public static void findPair(int[] arr, int diff) {
		Set<Integer> set = new HashSet<>();

		for (int elem : arr) {
			if (set.contains(elem - diff)) {
				System.out.println("(" + elem + ", " + (elem - diff) + ")");
			}

			if (set.contains(elem + diff)) {
				System.out.println("(" + (elem + diff) + ", " + elem + ")");
			}
			set.add(elem);
		}
	}

}
