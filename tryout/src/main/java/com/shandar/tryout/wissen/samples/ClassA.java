/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.wissen.samples </p>
 * <p>File Name: ClassA.java </p>
 * <p>Create Date: 03-Jan-2023 </p>
 * <p>Create Time: 1:19:32 PM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.wissen.samples;

import java.time.LocalTime;

/**
 * @author : Shantanu Sikdar
 *
 */
public class ClassA {

	public synchronized void m1() throws InterruptedException {
		System.out.println(this.hashCode());
		System.out.println(LocalTime.now());
		Thread.sleep(5000);
	}
}
