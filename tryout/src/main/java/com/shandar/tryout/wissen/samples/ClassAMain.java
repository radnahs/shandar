/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.wissen.samples </p>
 * <p>File Name: ClassAMain.java </p>
 * <p>Create Date: 03-Jan-2023 </p>
 * <p>Create Time: 1:21:39 PM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.wissen.samples;

/**
 * @author : Shantanu Sikdar
 * @Description :
 * 
 *              <pre>
 *  
 * Consider a class A with a synchronized method
 * class A {
 * 		public void synchronized m1() {
 * 			Thread.sleep(5000);
 * 		}
 * }
 * We create two objects of this class - o1 and o2. We 
 * call o1.m1() on one thread and o2.m1() on another 
 * thread, at the same time. 
 * 
 * What will be the behaviour?
 * Follow up with - 
 * how will you force these calls to execute one after the other
 * 1. Understands object level synchronization. These two calls do not block each other.
 * 2. Suggests a solution using a shared lock object or class level synchronization.
 *              </pre>
 *
 */
public class ClassAMain {

	public static void main(String[] args) throws InterruptedException {
		ClassA a1 = new ClassA();
		ClassA a2 = new ClassA();
		a1.m1();
		a2.m1();
		a1.m1();
	}

}
