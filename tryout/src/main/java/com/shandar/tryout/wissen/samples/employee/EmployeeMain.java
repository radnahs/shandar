/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.wissen.samples.employee </p>
 * <p>File Name: EmployeeMain.java </p>
 * <p>Create Date: 03-Jan-2023 </p>
 * <p>Create Time: 1:00:02 AM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.wissen.samples.employee;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author : Shantanu Sikdar
 * @Description :
 * 
 *              <pre>
 * Can one use an Employee class as a key in a HashMap? 
 * (This is a very important question. Please cover this in depth)
 * Evaluation Parameter- 
 * overriding hashCode, 
 * overriding equals, 
 * the behavior if hashCode returns a constant (including get and 
 * put time complexity), 
 * the behavior if hashCode is not imlemented but equals is .
 * 
 *              </pre>
 */
public class EmployeeMain {

	public static <T> void main(String[] args) {
		Map<Employee, String> empSkill = new HashMap<Employee, String>();

		Address address1 = new Address("Flat 6 GR", "Dapodi", "Pune", 411012);
		Employee empl1 = new Employee(101, "Shantanu", "Sikdar", address1);

		Address address2 = new Address("Flat C-202 Sneh Society (Building 23)", "Goregaon", "Mumbai", 400065);
		Employee empl2 = new Employee(102, "Nandita", "Sikdar", address2);

		Address address3 = new Address("Flat 6 GR", "Goregaon", "Mumbai", 400065);
		Employee empl3 = new Employee(102, "Piyali", "Sarkar", address3);

		empSkill.put(empl1, "Java");
		empSkill.put(empl1, "HR");
		empSkill.put(empl1, "Magento");

		List<Employee> lstEmp = new ArrayList<Employee>();
		lstEmp.add(empl1);
		lstEmp.add(empl2);
		lstEmp.add(empl3);
		System.out.println(lstEmp);

		System.out.println("Using Collections.sort Method using anonymous class");
		Collections.sort(lstEmp, new Comparator<Employee>() {
			@Override
			public int compare(Employee o1, Employee o2) {
				return o1.getFname().compareTo(o2.getFname());
			}
		});

		System.out.println(lstEmp);

		System.out.println("Using Collections.sort Method with lamda");
		List<Employee> lstEmp8 = new ArrayList<Employee>();
		lstEmp8.add(empl1);
		lstEmp8.add(empl2);
		lstEmp8.add(empl3);
		System.out.println(lstEmp8);
		Collections.sort(lstEmp8, (o1, o2) -> {
			return o1.getFname().compareTo(o2.getFname());
		});
		System.out.println(lstEmp8);

		System.out.println("Using Stream Sorted Method");
		List<Employee> lstEmpStrm = new ArrayList<Employee>();
		lstEmpStrm.add(empl1);
		lstEmpStrm.add(empl2);
		lstEmpStrm.add(empl3);
		System.out.println(lstEmpStrm);
		lstEmpStrm.stream().sorted((o1, o2) -> {
			return o1.getFname().compareTo(o2.getFname());
		}).forEach(System.out::println);
	}

}
