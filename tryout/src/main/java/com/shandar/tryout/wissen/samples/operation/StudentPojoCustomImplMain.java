/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.wissen.samples.operation </p>
 * <p>File Name: StudentMain.java </p>
 * <p>Create Date: 10-Jan-2023 </p>
 * <p>Create Time: 12:54:27 PM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.wissen.samples.operation;

import java.util.HashSet;

/**
 * @author : Shantanu Sikdar
 *
 */
public class StudentPojoCustomImplMain {

	public static void main(String[] args) {
		HashSet<StudentPojoCustomImpl> studentList = new HashSet<>();

		StudentPojoCustomImpl st1 = new StudentPojoCustomImpl("Nimit", 1);
		StudentPojoCustomImpl st2 = new StudentPojoCustomImpl("Rahul", 3);
		StudentPojoCustomImpl st3 = new StudentPojoCustomImpl("Nimit", 2);
		studentList.add(st1);
		studentList.add(st2);
		studentList.add(st3);

		System.out.println(studentList.size());
		System.out.println(studentList);
		st1.id = 3;
		System.out.println(studentList.size()); //what will be the output?
		System.out.println(studentList);
	}
	
	

}
