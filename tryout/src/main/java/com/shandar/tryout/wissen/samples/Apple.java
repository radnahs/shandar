/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.wissen.samples </p>
 * <p>File Name: Apple.java </p>
 * <p>Create Date: 29-Dec-2022 </p>
 * <p>Create Time: 8:46:38 AM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.wissen.samples;

/**
 * @author : Shantanu Sikdar
 *
 */
public class Apple {

	public void xyz() {
		System.out.println("in Apple");
	}

}
