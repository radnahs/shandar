/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.wissen.samples.immutability </p>
 * <p>File Name: ImmutablePerson.java </p>
 * <p>Create Date: 03-Jan-2023 </p>
 * <p>Create Time: 1:44:40 PM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.wissen.samples.immutability;

import java.util.ArrayList;
import java.util.List;

/**
 * @author : Shantanu Sikdar
 *
 */
public final class ImmutablePerson {

	private final String name;
	private final List<String> degrees;

	public ImmutablePerson(String name, List<String> degrees) {
		super();
		this.name = name;
		List<String> tempDeg = new ArrayList<String>();
		for (String string : degrees) {
			tempDeg.add(string);
		}
		this.degrees = tempDeg;
	}

	public String getName() {
		return name;
	}

	public List<String> getDegrees() {
		List<String> tempDeg = new ArrayList<String>();
		for (String string : this.degrees) {
			tempDeg.add(string);
		}
		return tempDeg;
	}

}
