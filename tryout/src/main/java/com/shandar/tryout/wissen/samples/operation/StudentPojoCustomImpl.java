/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.wissen.samples.operation </p>
 * <p>File Name: StudentPojoCustomImpl.java </p>
 * <p>Create Date: 10-Jan-2023 </p>
 * <p>Create Time: 12:52:29 PM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.wissen.samples.operation;

/**
 * @author : Shantanu Sikdar
 *
 */
public class StudentPojoCustomImpl {

	public int id;
	public String name;

	public StudentPojoCustomImpl(String name, int id) {
		this.name = name;
		this.id = id;
	}

	public int hashCode() {
		return this.id;
	}

	public String toString() {
		return "StudentPojoCustomImpl: " + this.name + "@" + Integer.toHexString(hashCode());
	}

	public boolean equals(Object o) {
		if (o instanceof StudentPojoCustomImpl) {
			StudentPojoCustomImpl s = (StudentPojoCustomImpl) o;
			return s.id == this.id ? true : false;
		}
		return false;
	}

}
