/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.wissen.samples </p>
 * <p>File Name: MedianQueries.java </p>
 * <p>Create Date: 29-Dec-2022 </p>
 * <p>Create Time: 12:34:16 AM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.wissen.samples;

import java.util.Arrays;

/**
 * @author : Shantanu Sikdar
 * @description :
 * 
 *              <pre>
 * 		
 *         Median Queries
 * 
 *         You are given an array A consisting of N elements. For a subarray of
 *         array A of length len, you have to sort the elements of the subarray
 *         in a non-decreasing order
 *              </pre>
 */
public class MedianQueries {

	public static void main(String[] args) {
		int[] arr = { 1, 8, 3, 12, 2, 10, 4, 14 };
		int t = findMedianOfSubArray(arr, arr.length, arr.length);
		System.out.println(t);
		int p = findMedianOfSubArrayWayOne(arr, arr.length, arr.length);
		System.out.println(p);

	}

	// this solution provided (by job consultant) is not giving the proper median
	private static int findMedianOfSubArray(int[] arr, int L, int R) {
		int[] temp = new int[R - L + 1];
		int j = 0;
		for (int i = L - 1; i < R; i++) {
			temp[j] = arr[i];
			j++;
		}

		Arrays.sort(temp);

		return temp[(temp.length - 1) / 2];
	}

	private static int findMedianOfSubArrayWayOne(int[] arr, int L, int R) {
		Arrays.sort(arr);
		// even case
		if (L % 2 != 0)
			return arr[L / 2];

		return (arr[(L - 1) / 2] + arr[L / 2]) / 2;
	}

}
