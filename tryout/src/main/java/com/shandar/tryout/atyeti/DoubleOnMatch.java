/**
 *  Project: tryout  
 *  Package Name: com.shandar.tryout.atyeti  
 *  File Name: HackerRankQ2.java  
 *  Create Date: 21-Jan-2022  
 *  Create Time: 8:21:29 PM  
 *  Copyright: Copyright (c) 2016 
 *  Company:   
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.atyeti;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author : Shantanu Sikdar
 *
 *         BETA
 *         Can’t read the text?
 *         Switch
 *         theme
 *         2. Double on Match
 *         
 *         Iterate
 *         through an array doubling the value of b at each index with an
 *         element equal to the current value of b.
 * 
 *         Given an array of long integers 'arr' and a number
 *         'num', iterate through the elements in arr and
 *         double the value of num whenever an element equals
 *         num. Find the maximum possible value of num,
 *         knowing that arr can be reordered before the iteration.
 * 
 *         Example
 * 
 *         arr = [1, 2, 4, 11, 12, 8]
 *         num = 2

 *         Iterating through arr:
 * 
 *         
 * 	 arr num
	     2
	 1   2
	 2   4
	 4   8
	11   8
	12   8
   	 8  16
 *          
 * 
 *         The maximal value of num = 16. Note that arr could
 *         have been reordered before iterating.
 * 
 *         Function Description 
 *         Complete the function doubleSize in the editor below.
 *         doubleSize has the following parameter(s):
 * 
 *             long int arr[n]:  an
 *         array of long integers
 * 
 *             long int num: the base long
 *         integer
 * 
 *         Returns:
 *            long int: the maximal value
 *         of num
 * 
 *         Constraints
 * 
 *         1 ≤ n ≤ 106
 *         0 ≤ arr[i] ≤ 1016
 *         0 ≤ num ≤ 104
 *         
 * 
 *           <!-- <StartOfInputFormat> DO NOT REMOVE THIS LINE-->
 * 
 *         <details><summary class="section-title">Input Format for Custom
 *         Testing
 * 
 *         Input from stdin will be processed
 *         as follows and passed to the function.
 * 
 *          
 * 
 *         The first line contains an integer n, the size of the array
 *         arr.
 * 
 *         Each of the next n lines contains an integer arr[i]
 *         where 0 ≤ i &lt; n.
 * 
 *         The last line contains a long integer, num. 
 * 
 *         Sample Case
 *         0
 * 
 *         Sample Input 0
 * 
 *         
 * STDIN     Function
-----     --------
5    →    arr[] size n = 5
1    →    arr = [1, 2, 3, 1, 2]
2
3
1
2
1    →    num = 1
 *         
 *         Sample Output 0
 *         4

 *         Explanation 0
 *         Rearrange arr to arr = {1, 1, 2, 2, 3}.
 *         
 * 	arr num
	    1
	1   2
	1   2
	2   4
	2   4
	3   4
 *         
 * 
 *         Sample Case 1
 * 
 *         Sample Input 1
 * 
 *         
 * STDIN     Function
-----     --------
3    →    arr[] size n = 3
1    →    arr = [1, 1, 1]
1
1
1    →    num = 1
 *         
 * 
 *         Sample Output 1
 *         2

 *         Explanation 1
 *         
 * 	arr  num
	     1
	1    2
	1    2
	1    2
 *         
 * 
 *         Sample Case 2
 * 
 *         Sample Input 2
 *         
 * STDIN     Function
-----     --------
5    →    arr[] size n = 5
2    →    arr = [2, 5, 4, 6, 8]
8
2    →    num = 2
 * 
 *         Sample Output 2
 *         16
 * 
 *         Explanation 2
 *         Rearrange arr to arr = {2, 4, 5, 6, 8}.
 * 
 *         
 * 	arr  num
	     2
	2    4
	4    8
	5    8
	6    8
	8   16
 * 
 */
public class DoubleOnMatch {

	public static void main(String[] args) {
		List<Integer> intLst = Arrays.asList(1, 2, 4, 11, 12, 8);
		List<Long> lngLst = intLst.stream().mapToLong(Integer::longValue).boxed().collect(Collectors.toList());
		System.out.println(doubleSize(lngLst,2));
	}

	public static long doubleSize(List<Long> arr, long b) {
		// Write your code here
		Collections.sort(arr);
		System.out.println(arr);
		for (int i = 0; i < arr.size(); i++) {
			if (arr.get(i).equals(b)) {
				b = b * 2;
			}
		}
		return b;
	}
}
