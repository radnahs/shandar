/**
 * Project: tryout 
 * Package Name: com.shandar.tryout.atyeti 
 * File Name: HackerRankQ1.java 
 * Create Date: 21-Jan-2022 
 * Create Time: 8:21:14 PM 
 * Copyright: Copyright (c) 2016
 * Company:  
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.atyeti;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author : Shantanu Sikdar
 *BETA
 *Can’t read the text? 
 *Switch
 *theme
 *1. Parking Dilemma
 *There are many cars parked in a parking lot. The parking lot is a straight line with a 
 *parking spot for every meter. There are n cars currently parked and a roofer wants to 
 *cover them with a roof. The requirement is that at least k cars currently in the lot 
 *are covered by the roof. Determine the minimum length of the roof to cover k cars.

Example

n = 4
cars = [6, 2, 12, 7]
k = 3

Two roofs that cover three cars are possible: one covering spots 2through 7with 
a length of 6, and another covering slots 6through 12with a length of 7. The 
shortest roof that meets the requirement is of length 6.

<div class="ps-content-wrapper-v0">


Function Description 

Complete the function carParkingRoof in the editor below.

carParkingRoof has the following parameter(s):
int cars[n]: the parking spots where cars are parked
int k: the number of cars that have to be covered by the roof

Returns

int: the minimum length of a roof that covers k cars where they are parked currently

Constraints

	1 ≤ n ≤ 10<sup>5</sup>
	1 ≤ k ≤ n
	1 ≤ cars[i] ≤ 10<sup>14</sup>
	All spots taken by cars are unique

Input Format Format for Custom Testing
Input from stdin will be processed as follows and passed to the function.
In the first line, there is a single integer, n, the size of cars[].
Then, n line follows. In the i<sup>th</sup> of them, there is a single integer cars[i].
In the last line, there is a single integer k.

Sample Case 0

Sample Input

STDIN   Function 
-----   --------
4→  cars[] size n = 4
2→  cars[] = [ 2, 10, 8, 17 ]
10
8
17
3→  k = 3

Sample Output
9

Explanation
A roof can be built of length 9 covering all parking spots from the 2nd through the 
10th, so covering 3 cars in spots 2, 8, and 10. There is no shorter roof that can 
cover 3 cars.

Sample Case 1
Sample Input

STDIN Function
----- --------
4 → cars[] size n = 4
1→ cars[] = [ 1, 2, 3, 10 ]
2
3
10
4   → k = 4

Sample Output
10

Explanation
All of the cars must be covered. The shortest roof that can cover them has a length of 10 and starts at slot 1 and ends at slot 10.

 */

public class ParkingDillema {

	public static long carParkingRoof(List<Long> cars, int k) {
	    // Write your code here
        Collections.sort(cars);
        Long min =cars.get(k-1)-cars.get(0)+1;
        System.out.println(min);
        for(int i=1;i<cars.size();i++){
            if(i+k<cars.size()){
                if((cars.get(i+k-1)+1-cars.get(i)) < min){
                    min=cars.get(i+k-1)+1-cars.get(i);    
                }    
            }        
        }
        System.out.println(min);
        return min;
        
    }
	
	public static void main(String[] args) {
		List<Integer> intLst = Arrays.asList(6, 2, 12, 7);
		List<Long> lngLst = intLst.stream().mapToLong(Integer::longValue).boxed().collect(Collectors.toList());
		carParkingRoof(lngLst, 3);
	}

}
