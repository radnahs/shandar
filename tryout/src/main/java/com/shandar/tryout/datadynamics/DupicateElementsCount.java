/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.datadynamics </p>
 * <p>File Name: ProblemOne.java </p>
 * <p>Create Date: 03-Jul-2020 </p>
 * <p>Create Time: 12:12:46 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.datadynamics;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * @author : Shantanu Sikdar
 *
 *         21. Count Duplicate Elements Given an integer array, numbers, count
 *         the number of elements that occur more than once. Function
 *         Description
 * 
 *         Complete the function 
 *         countDuplicates in the editor.
 * 
 *         Parameters
 * 
 *         Name
 *         Type
 *         Description
 *         numbers Integer Array
 *         array of integers to process
 * 
 *         Return
 *         The function must return an integer denoting the
 *         number of non-unique (duplicate) values in the numbers array.</td>
 * 
 *         Example
 * 
 *         numbers = [1, 3, 3, 4, 4, 4]
 * 
 *         There are two non-unique elements: 3&nbsp;and 4. Function Description
 *         
 *         Complete the function countDuplicate in the editor below.
 * 
 *         countDuplicate has the following parameter(s):
 *         </p>
 * 
 *         <p>
 *         &nbsp;&nbsp;&nbsp; int numbers[n]:&nbsp; an array of integers
 *         Returns:
 * 
 *         &nbsp;&nbsp;&nbsp;&nbsp;int: an integer that denotes the number of
 *         non-unique values in the numbers array
 * 
 *         Constraints
 *         <li>3 ≤ n ≤ 1000</li>
 *         <li>1 ≤ numbers[i]&nbsp;≤ 1000,&nbsp;0 ≤ i &lt; n</li> <!--
 *         <StartOfInputFormat> DO NOT REMOVE THIS LINE-->
 * 
 *         Input Format Format
 *         for Custom Testing
 * 
 *         <div class="collapsable-details"> Input from stdin will be processed
 *         as follows and passed to the function.
 * 
 *         The first line contains an integer n, the size of the numbers array.
 * 
 *         Each of the next n lines contains an integer, numbers[i], where 0 ≤ i
 *         < n.   <!-- </StartOfInputFormat> DO NOT REMOVE
 *         THIS LINE-->
 * 
 *         Sample Case 0
 * 
 *         Sample Input
 *         STDIN Function ----- ----- 8 → numbers[] 
 *         size n = 8 1 → numbers = [1, 3, 1, 4, 5, 6, 3, 2] 
 *         3 1 4 5 6 3 2
 * 
 *         Sample Output
 *         2
 * 
 *         Explanation
 *         The values 1 and 3 occur more than once, therefore the answer is 2.
 * 
 *         Sample Case 1
 *         Sample Input
 *         STDIN Function ----- ----- 
 *         6 → numbers[] size n = 6 1 → 
 *         numbers = [1, 1, 2, 2, 2, 3] 
 *         1 2 2 2 3
 * 
 *         Sample Output
 *         2
 *         Explanation
 *         The values 1 and 2 occur more than once, therefore the answer is 2.
 */
public class DupicateElementsCount {

	public static void main(String[] args) {
		System.out.println(countDuplicate(Arrays.asList(1, 3, 3, 4, 4, 4)));
		System.out.println(countDuplicate(Arrays.asList(1, 1, 2, 2, 2, 3)));
		System.out.println(countDuplicate(Arrays.asList(1, 3, 1, 4, 5, 6, 3, 2)));
		System.out.println(countDuplicate(Arrays.asList(1, 3, 1, 4, 5, 6, 3, 2, 6)));
	}

	public static int countDuplicate(List<Integer> numbers) {
		Map<Integer, List<Integer>> mp = new HashMap<Integer, List<Integer>>();

		numbers.stream().forEach(number -> {
			if (mp.containsKey(number)) {
				List<Integer> lst = mp.get(number);
				lst.add(number);
				mp.put(number, lst);
			} else {
				List<Integer> lst = new ArrayList<>();
				lst.add(number);
				mp.put(number, lst);
			}
		});

		int count = 0;
		for (Entry<Integer, List<Integer>> m1 : mp.entrySet()) {
			if (m1.getValue().size() > 1) {
				count++;
			}
		}
		return count;

	}

}
