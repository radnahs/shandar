/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.datadynamics </p>
 * <p>File Name: ProblemTwo.java </p>
 * <p>Create Date: 03-Jul-2020 </p>
 * <p>Create Time: 12:13:16 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.datadynamics;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;

/**
 * @author : Shantanu Sikdar
 *
 */
public class MovieTitles {

	public static void main(String[] args) {
		getMovieTitles("spiderman");
	}

	class Page {

		public String page;// ": 1,
		public Integer per_page;// ": 10,
		public Integer total;// ": 13,
		public Integer total_pages;// ": 2,
		public List<Data> data;// ": [

		public String getPage() {
			return page;
		}

		public void setPage(String page) {
			this.page = page;
		}

		public Integer getPer_page() {
			return per_page;
		}

		public void setPer_page(Integer per_page) {
			this.per_page = per_page;
		}

		public Integer getTotal() {
			return total;
		}

		public void setTotal(Integer total) {
			this.total = total;
		}

		public Integer getTotal_pages() {
			return total_pages;
		}

		public void setTotal_pages(Integer total_pages) {
			this.total_pages = total_pages;
		}

		public List<Data> getData() {
			return data;
		}

		public void setData(List<Data> data) {
			this.data = data;
		}

		@Override
		public String toString() {
			return "Page [page=" + page + ", per_page=" + per_page + ", total=" + total + ", total_pages=" + total_pages
					+ ", data=" + data + "]";
		}

	}

	class Data {
		private String Title;
		private String Year;
		private String imdbID;

		public String getTitle() {
			return Title;
		}

		public void setTitle(String title) {
			Title = title;
		}

		public String getYear() {
			return Year;
		}

		public void setYear(String year) {
			Year = year;
		}

		public String getImdbID() {
			return imdbID;
		}

		public void setImdbID(String imdbID) {
			this.imdbID = imdbID;
		}

		@Override
		public String toString() {
			return "Data [Title=" + Title + ", Year=" + Year + ", imdbID=" + imdbID + "]";
		}

		
	}

	static String[] getMovieTitles(String substr) {

		try {
			
			String strUrl = "https://jsonmock.hackerrank.com/api/movies/search/?Title="+substr ;
			Page page = jsonPerPage(strUrl);

			List<Page> pageLst = new ArrayList<MovieTitles.Page>();
			pageLst.add(page);
			int k =2;
			for(int i = 0; i<page.getTotal_pages()-1 ; i++) {
				Page pageTmp = jsonPerPage(strUrl+"&page="+k);
				k++;
				pageLst.add(pageTmp);
			}
			System.out.println(page);
			System.out.println(page.getTotal_pages());
			System.out.println(page.data.get(1));
			List<String> movieLst = new ArrayList<String>();
			for (int i = 0; i < pageLst.size(); i++) {
				Page pageTmp = pageLst.get(i);
				List<Data> dtLst = pageTmp.getData();
				for (int j = 0; j < dtLst.size(); j++) {
					Data data = dtLst.get(j);
					
					if(data.getTitle().contains(substr)) {
						System.out.println(data.getTitle().contains(substr));
					}
				}
			}
			
		} catch (Exception e) {
		}

		return null;
	}
	
	
	private static Page jsonPerPage(String strUrl) {
		Page page = null;
		try {
			URL url = new URL(strUrl);//("https://jsonmock.hackerrank.com/api/movies/search/?Title=" + substr);
			HttpURLConnection huc = (HttpURLConnection) url.openConnection();
			huc.setRequestMethod("GET");
			System.out.println(huc.getResponseCode());

			StringBuilder response = new StringBuilder();
			try (BufferedReader br = new BufferedReader(new InputStreamReader(huc.getInputStream(), "utf-8"))) {
				String responseLine = null;
				while ((responseLine = br.readLine()) != null) {
					response.append(responseLine.trim());
				}
				System.out.println(response.toString());
			} 
			Gson gson = new Gson();
			page = gson.fromJson(response.toString(),Page.class); 

		} catch (Exception e) {

		}
		return page;
	}

}
