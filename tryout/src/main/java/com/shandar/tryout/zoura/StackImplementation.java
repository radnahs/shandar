/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.zoura </p>
 * <p>File Name: StackImplementation.java </p>
 * <p>Create Date: 24-May-2021 </p>
 * <p>Create Time: 7:17:20 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.zoura;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

/**
 * @author : Shantanu Sikdar
 *
 *         <pre>
 *   From Tony Zhai to Everyone: 09:07 AM /** Design a stack that supports
 *   push, pop, top, getMin
 *
 *   push(x) -- Push element x onto stack. O(1) time 
 *   pop() -- Remove the top element of the stack. O(1) time 
 *   top() -- Peek the top one element of the stack. O(1) time 
 *   getMin() -- Peek the minimum element of the stack. O(1) time
 *         </pre>
 * 
 *         NEED to COMPLETE the minValue part.
 */
public class StackImplementation {

	// {1,3,5,4}
	public static void main(String[] args) {
		StackJavaUsingLinkedList.push(5);
		StackJavaUsingLinkedList.push(3);
		StackJavaUsingLinkedList.push(1);
		// push(4);

		System.out.println(StackJavaUsingLinkedList.pop());
		System.out.println("min value ==  " + StackJavaUsingLinkedList.minValue);
		System.out.println();

		StackJavaUsingStack.push(2);
		StackJavaUsingStack.push(3);
		StackJavaUsingStack.push(5);
		StackJavaUsingStack.push(4);
		StackJavaUsingStack.push(1);
		System.out.println("StackJavaUsingStack min value ==  " + StackJavaUsingStack.minValue());
		System.out.println("StackJavaUsingStack min value ==  " + StackJavaUsingStack.pop());
		System.out.println("StackJavaUsingStack min value ==  " + StackJavaUsingStack.minValue());
		System.out.println("StackJavaUsingStack min value ==  " + StackJavaUsingStack.pop());
	}

}

//given by me during inerview.
class StackJavaUsingLinkedList {
	static int minValue = 0;
	static List<List> minVal = new ArrayList<List>();
	static LinkedList<Integer> ll = new LinkedList<Integer>();

	public static void push(int i) {
		if (ll.isEmpty()) {
			minValue = i;
		}
		if (minValue > i) {
			minValue = i;
		}
		ll.push(i);
	}

	public static int pop() {

		return ll.pop();
	}

	public static int top() {
		return ll.getLast();
	}

	private static int getMin() {
		return minValue;
	}
}

//Correct
class StackJavaUsingStack {
	static int minValue = 0;
	static Stack<Integer> stack = new Stack<Integer>();

	public static void push(Integer val) {
		if (stack.isEmpty()) {
			minValue = val;
		}
		if (minValue > val) {
			minValue = val;
		}
		stack.push(val);
	}

	public static Integer pop() {
		int val = stack.pop();
		if (minValue > val) {
			minValue = val;
		}
		return val;
	}

	public static int top() {
		return stack.lastElement();
	}

	public static int minValue() {
		return minValue;
	}

}