/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.epam </p>
 * <p>File Name: ThirdHighest.java </p>
 * <p>Create Date: 14-Feb-2023 </p>
 * <p>Create Time: 6:32:04 PM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.epam;

/**
 * @author : Shantanu Sikdar
 *
 */
public class ThirdHighest {

	/**
	 * @param args
	 */

	public static void main(String[] args) {
		int[] intArr = { 3, 6, 7, 2, 1, 5, 17, };// 3
		int temp = intArr[0];
		int count = 0;
		for (int i = 0; i < intArr.length; i++) {
			if (intArr[i] < temp) {
				temp = intArr[i];
			} else {
				count++;
				if (count == 3) {
					break;
				}
			}
		}
		System.out.println(temp);

	}
	
	

}
