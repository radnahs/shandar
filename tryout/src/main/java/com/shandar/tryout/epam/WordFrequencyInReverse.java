/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.epam </p>
 * <p>File Name: WordFrequencyInReverse.java </p>
 * <p>Create Date: 06-Feb-2023 </p>
 * <p>Create Time: 4:35:26 PM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.epam;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author : Shantanu Sikdar
 *
 */
public class WordFrequencyInReverse {

	public static void main(String[] args) {
		List<String> items = Arrays.asList("papaya", "apple", "apple", "banana", "apple", "orange", "banana", "apple",
				"banana", "papaya");

		System.out.println(items);

		Map<String, Long> mapCount = items.stream()
				.collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
		System.out.println(mapCount);

		//sort by using values
		Map<String, Long> result = mapCount.entrySet().stream()
				.sorted(Map.Entry.comparingByValue(Comparator.reverseOrder())).collect(Collectors.toMap(
						Map.Entry::getKey, Map.Entry::getValue, (oldValue, newValue) -> oldValue, LinkedHashMap::new));
		System.out.println(result);

		System.out.println(items);
		
		
	}
	

	
}


