/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.bnymellon </p>
 * <p>File Name: ArrayRotation.java </p>
 * <p>Create Date: 10-Jun-2020 </p>
 * <p>Create Time: 7:44:52 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.bnymellon;

/**
 * @author : Shantanu Sikdar
 *
 */
public class ArrayRotation {

	public static void main(String[] args) {
		//int[] a = new int[]{1,2,3,4,5};
		//int[] a = new int[]{1,2,3,4,5};
		//int[] a = new int[]{1,2,3,4,5};
		int[] a = new int[]{11,24,3,34,15};
		display(a);
		int[] b = arrayLeftRotation(a, 2);//
		display(b);
		int[] c = arrayRightRotation(a, 2);//
		display(c);
	}
	

	public static int[] arrayLeftRotation(int[] a, int k) {
		System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());
		int size = a.length;
		int[] newArr = new int[size];		
		for(int i=0; i<size;i++){
			int index = Math.abs(size+i-k)<size?Math.abs(size+i-k):Math.abs(i-k); 
			newArr[index]=a[i];
		}		
		return newArr;
    }
	
	public static int[] arrayRightRotation(int[] a, int k) {
		System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());
		int size = a.length;
		int[] newArr = new int[size];		
		for(int i=0; i<size;i++){
			int index = Math.abs(i+k)<size?Math.abs(i+k):Math.abs(size-i-k); 
			newArr[index]=a[i];
		}		
		return newArr;
    }
	
	public static void display(int[] arr) {
		for (int i : arr) {
			System.out.print(i+",");
		}
		System.out.println();
	}
}
