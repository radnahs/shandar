/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.bnymellon </p>
 * <p>File Name: AmazonFreshPromotion.java </p>
 * <p>Create Date: 31-May-2020 </p>
 * <p>Create Time: 8:00:54 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.bnymellon;

import java.util.Arrays;
import java.util.List;

/**
 * @author : Shantanu Sikdar
 *
 * @description : 
 * 2. Collision Course: 
 * Find the number of times a particle will collide with other particles. 
 * There are n particles numbered from 0 to n − 1 lined up from smallest 
 * to largest ID along the x-axis. 
 * For example:
 */
public class CollisionCourse {

	public static void main(String[] args) {
		// List<Integer> numbers = Arrays.asList(6, 6, 1, 6, 3, 4, 6, 8);
		// System.out.println(collision(numbers, 2));
		// List<Integer> numbers = Arrays.asList(8, 3, 6, 3, 2, 2, 4, 8, 1, 6);
		// System.out.println(collision(numbers, 7));
		List<Integer> numbers = Arrays.asList(1, 3, 7, 4, 6, 4);
		System.out.println(collision(numbers, 3));
	}

	public static int collision(List<Integer> speed, int pos) {
		// Write your code here
		int consider = speed.get(pos);
		int count = 0;
		for (int i = pos + 1; i < speed.size(); i++) {
			if (speed.get(i) < consider) {
				count++;
			}
		}
		for (int i = 0; i < pos; i++) {
			if (consider < speed.get(i)) {
				count++;
			}
		}

		return count;
	}

}
