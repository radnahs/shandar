/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.bnymellon </p>
 * <p>File Name: LoopInLinkedList.java </p>
 * <p>Create Date: 16-Jun-2020 </p>
 * <p>Create Time: 12:11:12 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.bnymellon;

/**
 * @author : Shantanu Sikdar
 *
 */
public class LoopInLinkedList {

	static Node head;

	static class Node{
		int data;
		Node next;

		public Node(int data) {
			this.data = data;
			this.next = null;
		}
	}

	boolean findLoop(Node node) {
		boolean isLoop = false;
		Node n1 = node;
		Node n2 = node;
		while (n1 != null && n2 != null && n2.next != null) {
			if (n1 == n2) {
				System.out.println("loop detected");
				isLoop = true;
			}
		}
		return isLoop;
	}
	
	
	public static void main(String[] args) {
		System.out.println("BNY Mellon");

		LoopInLinkedList ll = new LoopInLinkedList();
		ll.head = new LoopInLinkedList.Node(50);
		ll.head.next = new LoopInLinkedList.Node(45);
		ll.head.next.next = new LoopInLinkedList.Node(40);
		ll.head.next.next.next = new LoopInLinkedList.Node(35);
		ll.head.next.next.next.next = new LoopInLinkedList.Node(30);

		// creating a loop for testing
		//head.next.next.next.next = head.next.next;
		ll.findLoop(ll.head);

	}

}

