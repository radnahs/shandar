/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.bnymellon </p>
 * <p>File Name: Problem2.java </p>
 * <p>Create Date: 31-May-2020 </p>
 * <p>Create Time: 2:44:19 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.bnymellon;

import java.util.ArrayList;
import java.util.List;

/**
 * @author : Shantanu Sikdar
 *
 * @ Description : 
 * 
 * Given two integers, L and R print all the odd numbers between
 * L and R (L and R inclusive). Complete the oddNumbers function in the editor
 * below. It has 2 parameters: An integer, L, denoting the left part of the
 * range. An integer, R, denoting the right part of the range. The function must
 * return an array of integers&nbsp;denoting the odd numbers between L and R.
 * 
 * Input Format
 * 
 * Locked stub code in the editor reads the following input from stdin and
 * passes it to the function: The first line contains an integer, L, denoting
 * the left part of the range. The second line contains an integer, R, denoting
 * the right part of the range.
 *
 * Constraints 1 ≤ L ≤ R ≤ 10 5
 * 
 * Output Format
 * 
 * The function must return an array of integers denoting the odd numbers
 * between L and R. This is printed to stdout by locked stub code in the editor.
 * 
 * Sample Input:0 
 * 2 
 * 5 
 * Sample Output:0 
 * 3,5 
 * Explanation:0 
 * The value of l is 2 and value of r is 5. The odd numbers between [2, 5] are 3 and 5.
 * 
 * Sample Input:1 
 * 3 
 * 9 
 * Sample Output:1 
 * 3,5,7,9 
 * Explanation:1 
 * The value of L is 3 and value of R is 9. The odd numbers between [3, 9] are 3, 5, 7 and 9.
 * 
 * 
 */
public class OddNumbers {

	public static void main(String[] args) {
		System.out.println(oddNumber(3, 9));
	}

	public static List<Integer> oddNumber(int l, int r) {
		List<Integer> lstInt = new ArrayList<Integer>();
		if (l % 2 == 0) {
			l = l + 1;
		}
		for (int i = l; i <= r; i = 2 + i) {
			lstInt.add(i);
		}
		return lstInt;
	}

}
