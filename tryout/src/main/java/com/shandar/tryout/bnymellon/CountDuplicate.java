/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.bnymellon </p>
 * <p>File Name: Prob3.java </p>
 * <p>Create Date: 31-May-2020 </p>
 * <p>Create Time: 8:01:15 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.bnymellon;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * @author : Shantanu Sikdar
 *
 * In an Array count only duplicate elements
 */
public class CountDuplicate {

	public static void main(String[] args) {
		List<Integer> numbers = Arrays.asList(1, 3, 3, 4, 4, 4);
		System.out.println(countOnlyDuplicate(numbers));
	}

	public static int countOnlyDuplicate(List<Integer> numbers) {
		Map<Integer, List<Integer>> mp = new HashMap<Integer, List<Integer>>();

		numbers.stream().forEach(number -> {
			if (mp.containsKey(number)) {
				List<Integer> lst = mp.get(number);
				lst.add(number);
				mp.put(number, lst);
			} else {
				List<Integer> lst = new ArrayList<>();
				lst.add(number);
				mp.put(number, lst);
			}
		});

		int count = 0;
		for (Entry<Integer, List<Integer>> m1 : mp.entrySet()) {
			if (m1.getValue().size() > 1) {
				count++;
			}
		}

		return count;
	}

}
