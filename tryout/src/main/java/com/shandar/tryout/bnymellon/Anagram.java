/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.bnymellon </p>
 * <p>File Name: Anagram.java </p>
 * <p>Create Date: 16-Jun-2020 </p>
 * <p>Create Time: 12:06:12 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.bnymellon;

/**
 * @author : Shantanu Sikdar
 *
 */
public class Anagram {

	public static void main(String[] args) {
		String str1 = "tops";
		String str2 = "spot";
		String str3 = "post";
		String str4 = "a chevrolet";
		String str5 = "love the car";

		System.out.println(isAnagram(str1, str2));
		System.out.println(isAnagram(str4, str5));
	}

	private static boolean isAnagram(String s1, String s2) {
		char[] chrArr1 = s1.replace(" ", "").toCharArray();
		char[] chrArr2 = s2.replace(" ", "").toCharArray();
		boolean isAnagram = false;
		if (chrArr1.length == chrArr2.length) {
			for (int i = 0; i < chrArr1.length; i++) {
				for (int j = 0; j < chrArr2.length; j++) {
					if (chrArr1[i] == chrArr2[j]) {
						chrArr2[j] = ' ';
						break;
					}
				}
			}
		}

		if ("".equals(String.valueOf(chrArr2).trim())) {
			isAnagram = true;
		}
		return isAnagram;
	}

}
