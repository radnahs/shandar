/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.bnymellon </p>
 * <p>File Name: ObjectSorting.java </p>
 * <p>Create Date: 01-Jun-2020 </p>
 * <p>Create Time: 10:45:39 am </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.bnymellon;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author : Shantanu Sikdar
 *
 * 4. Sort the Persons List
 * 
 * Given the list of persons, and the sort order, write the code to sort the list. 
 * You have to implement the logic in PersonUtils.sortPersons() method by leveraging 
 * java collection sorting. The 2nd parameter of this method includes the sortBy 
 * String array. You have to sort the Persons List as per the elements in this 
 * sortBy array. You may have to sort by more than one field based on this input. 
 * Please assume ascending sort order for both Strings and numbers.
 * 
 * Person class is like this:
 * Person {
 * 		String firstName;
 * 		String lastName;
 * 		int age;
 *		String country;
 * }
 * Example-1:
 * Sample Input:
 * 3
 * firstname
 * Marlon|Brando|80|USA
 * Bruce|Lee|32|China
 * Brandon|Lee|28|China
 * (you don't have to worry about this; parsing the inputs is taken care by the Solution 
 * already)
 *	1st line indicates the number of Person records (rows) in the input
 *  2nd line - list of sortby fields separated by semicolon
 *  Person records start from 3rd line.
 *  
 *  Required Output:
 *  (sorted by firstName)
 *  Brandon|Lee|28|China
 *  Bruce|Lee|32|China
 *  Marlon|Brando|80|USA
 *
 *
 * Example-2:
 * Sample Input:
 * 6
 * country;firstname
 * Marlon|Brando|80|USA
 * Bruce|Lee|32|China
 * Brandon|Lee|28|China
 * Amjad|Khan|51|India
 * Amrish|Puri|72|India
 * Sanjeev|Kumar|47|India
 * 
 * Required Output: 
 * (sorted by country  
 * Brandon|Lee|28|China
 * Bruce|Lee|32|China
 * Amjad|Khan|51|India
 * Amrish|Puri|72|India
 * Sanjeev|Kumar|47|India
 * Marlon|Brando|80|USA
 * 
 * 
 */
public class ObjectSorting {

	public static void main(String[] args) {
		List<Person> persons = new ArrayList<Person>();
		persons.add(new Person("Marlon","Brando", 80, "USA"));
		persons.add(new Person("Bruce","Lee", 32, "China"));
		persons.add(new Person("Brandon","Lee", 28, "China"));
		persons.add(new Person("Amjad","Khan", 51, "India"));
		persons.add(new Person("Amrish","Puri", 72, "India"));
		persons.add(new Person("Sanjeev","Kumar", 47, "India"));
		//String[] sortBy = {"country" , "firstname"}; 
		String[] sortBy = {"age" , "firstname"};
		System.out.println(persons);
		PersonUtils.sortPersons(persons, sortBy);
	}

}

class PersonUtils {

	public static void sortPersons(List<Person> persons, String[] sortBy) {

		for (int i = sortBy.length - 1; i > -1; i--) {
			if (sortBy[i].equalsIgnoreCase("firstName")) {
				Collections.sort(persons, (p1, p2) -> p1.getFirstName().compareTo(p2.getFirstName()));
			} else if (sortBy[i].equalsIgnoreCase("lastName")) {
				Collections.sort(persons, (p1, p2) -> p1.getLastName().compareTo(p2.getLastName()));
			} else if (sortBy[i].equalsIgnoreCase("age")) {
				Collections.sort(persons, (p1, p2) -> new Integer(p1.getAge()).compareTo(new Integer(p2.getAge())));
			} else if (sortBy[i].equalsIgnoreCase("country")) {
				Collections.sort(persons, (p1, p2) -> p1.getCountry().compareTo(p2.getCountry()));
			}
		}
		
		System.out.println(persons);
	}
}

class Person {
	String firstName;
	String lastName;
	int age;
	String country;

	public Person(String firstName, String lastName, int age, String country) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
		this.country = country;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public int getAge() {
		return age;
	}

	public String getCountry() {
		return country;
	}

	@Override
	public String toString() {
		return firstName + "|" + lastName + "|" + age + "|" + country;
	}
}
