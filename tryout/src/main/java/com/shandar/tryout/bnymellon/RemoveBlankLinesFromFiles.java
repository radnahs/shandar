/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.bnymellon </p>
 * <p>File Name: RemoveBlankLines.java </p>
 * <p>Create Date: 10-Jun-2020 </p>
 * <p>Create Time: 8:57:15 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.bnymellon;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 * @author : Shantanu Sikdar
 *
 */
public class RemoveBlankLinesFromFiles {

	public static void main(String[] args) {
		creatingNewFileWithRemovedLines();

	}

	private static void creatingNewFileWithRemovedLines() {
		String filePath = "E:\\data\\project\\shandar\\tryout\\src\\main\\java\\com\\shandar\\tryout\\bnymellon\\test1.txt";
		try {
			Scanner sc = new Scanner(new File(filePath));
			StringBuilder sbr = new StringBuilder();
			while (sc.hasNext()) {
				String str = sc.nextLine();
				if (!str.isBlank()) {
					sbr.append(str + "\n");
				}
			}
			sc.close();

			PrintWriter pwr = new PrintWriter(filePath);
			pwr.print(sbr.toString());
			pwr.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

}
