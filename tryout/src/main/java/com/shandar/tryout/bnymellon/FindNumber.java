/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.bnymellon </p>
 * <p>File Name: Prob1.java </p>
 * <p>Create Date: 31-May-2020 </p>
 * <p>Create Time: 2:21:16 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.bnymellon;

import java.util.ArrayList;
import java.util.List;

/**
 * @author : Shantanu Sikdar
 *
 * @description :  
 * Given an unsorted array of n elements, find if the element k
 *    is present in the array or not. Complete the findNumber function
 *              in the editor below. It has 2 parameters:
 * An array of integers, arr, denoting the elements in the array. 
 * An integer, k, denoting the element to be searched in the array.
 *
 * The function must return a string "YES" or "NO" denoting if the element is present in the array or not.
 * 
 * Input Format
 * The first line contains an integer n, denoting the number of elements in the array arr.
 * Each line  i  of the  n  subsequent lines (where 0 ≤ i < n) contains an integer describing arr i
 * The next line contains an integer, k, the element that needs to be searched.
 * 
 * Constraints:
 * 1 ≤ n ≤ 10^5
 * 1 ≤ arr[i] ≤ 10^9
 * 
 * Output Format:
 * The function must return a string "YES" or "NO" denoting if the element is present in the array or not. This is printed to stdout by locked stub code in the editor.
 * 
 * Sample Input 0
 * 5
 * 1
 * 2
 * 3
 * 4
 * 5
 * 1
 * Sample Output 0
 * YES
 * Explanation 0
 * Given the array = [1, 2, 3, 4, 5], we want to find the element 1 if it is present or not. We can find the element 1 at index = 0. Therefore we print "YES".
 *
 * Sample Input 1
 * 3
 * 2
 * 3
 * 1
 * 5
 * Sample Output 1
 * NO
 * Explanation 1
 * Given the array [2, 3, 1] and k = 5. There is no element 5 in the array and therefore we print "NO".
 * 
 * 
 */

public class FindNumber {

	public static void main(String[] args) {
		List<Integer> arr = new ArrayList<Integer>();
		arr.add(1);
		arr.add(3);
		arr.add(4);
		arr.add(12);
		arr.add(11);
		int find = 10;
		System.out.println(findNumber(arr, find));
	}

	private static String findNumber(List<Integer> arr, int find) {
		String[] hasVal = { "NO" };
		arr.stream().forEach(itm -> {
			if (itm == find) {
				hasVal[0] = "YES";
			}
		});
		return hasVal[0];
	}

}
