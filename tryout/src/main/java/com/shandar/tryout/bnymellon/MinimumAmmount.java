/**
 * Project: tryout 
 * Package Name: com.shandar.tryout.bnymellon 
 * File Name: MinimumAmmount.java 
 * Create Date: 31-Mar-2021 
 * Create Time: 3:01:37 pm 
 * Copyright: Copyright (c) 2016
 * Company:  
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.bnymellon;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author : Shantanu Sikdar
 *
 *
 *         1. Minimum Amount Help Alex determine the cost of n discounted
 *         items.
 * 
 *         Alex has a list of items to purchase at a market. The owner offers to
 *         discount each item after the first one by the lowest marked price
 *         among the prior items. No item's price can be discounted below 0, and
 *         the list of items may not be reordered. Calculate the payable amount.
 * 
 *       	Alex is shopping at a flea market and has chosen some items to
 *         purchase. Noticing Alex&#39;s interest, the stand owner makes the
 *         following offer. If Alex agrees to buy everything selected, the owner
 *         will discount each item after the first by the lesser of the
 *         item&#39;s cost or the minimum non-discounted price of any earlier
 *         item in the list.No item&#39;s cost can be below 0 and the prices may
 *         not be reordered.
 * 
 *         Example
 *         prices = [2, 5, 1, 4]
 * 
 *         Alex pays2for the first item since there are no previous items to
 *         compare to.
 * 
 *         The second item costs 5 - 2 = 3.
 *         The third item is free: max(1 - min(2, 5), 0) = max(-1, 0) = 0.
 *         The fourth item costs 4 - 1 = 3.
 * 
 *         The total cost to purchase all items is 2 + 3 + 0 + 3 = 8.
 * 
 *         The first item is never discounted and the minimum cost of any item
 *         is 0.
 * 
 *         Function Description 
 *         Complete the function calculateAmount in the editor below. The
 *         function must return Alex's total cost to purchase all the items.
 * 
 *         calculateAmount has the following parameter(s):
 * 
 *         int prices[n]: the original prices of each of the items selected
 * 
 *         Returns:
 *         int:the total cost to purchase the items after any discounts are
 *         applied
 * 
 *         Constraints
 *         1 ≤ n ≤ 10<sup>5</sup>
 *         1 ≤ prices[i] ≤ 10<sup>7</sup>, where 0 ≤ i < n
 * 
 *         <!-- <StartOfInputFormat> DO NOT REMOVE THIS LINE-->
 * 
 *         Input Format for Custom
 *         Testing
 * 
 *          Input from stdin will be processed
 *         as follows and passed to the function.
 * 
 *         The first line contains an integer n, the size of the array prices.
 * 
 *         The next n lines each contain an element prices[i] where 0 ≤ i < n.
 * 
 *         Sample Case 0

 *         Sample Input 0
 *         STDIN Function ----- -------- 4→prices[] size n = 4 4→prices = [4, 9,
 *         2, 3] 9 2 3
 * 
 *         Sample Output 0
 *         10
 * 
 *         Explanation 0
 *         n = 4, prices = [4, 9, 2, 3]
 *         prices[0] = 4; d[0] = 0; cost[0] = 4 because the first item is never
 *         discounted.
 *         prices[1] = 9; d[1] = 4; cost[1] = 9 − 4 = 5
 *         prices[2] = 2; d[2] = min(4, 9) = 4; cost[2] = 0.
 *         prices[3] = 3; d[3] = min(4, 9, 2) = 2; cost[3] = 3 − 2 = 1
 *         The total cost returned is 4 + 5 + 0 + 1 = 10.
 * 
 *         Sample Case 1
 * 
 *         Sample Input 1
 *         STDIN Function ----- -------- 4→prices[] size n = 4 1→prices = [1, 2,
 *         3, 4] 2 3 4
 * 
 *         Sample Output 1
 *         7
 * 
 *         Explanation 1
 *         n = 4, prices = [1, 2, 3, 4]
 *         prices[0] = 1; d[0] = 0; cost[0] = 1.
 *         prices[1] = 2; d[1] = 1; cost[1] = 2 − 1 = 1
 *         prices[2] = 3 d[2] = min(1, 2) = 1; cost[2] = 3 − 1 = 2
 *         prices[3] = 4; d[3] = min(1, 2, 3) = 1; cost[3] = 4 − 1 = 3
 *         The total cost returned is 1 + 1 + 2 + 3 = 7.
 *
 */
public class MinimumAmmount {

	public static void main(String[] args) {
		System.out.println(calculateAmount(Arrays.asList(2, 5, 1, 4)));
		// System.out.println(calculateAmount(Arrays.asList(4, 9, 2, 3)));
	}

	public static long calculateAmount(List<Integer> prices) {
		int sum = prices.get(0);
		for (int i = 1; i < prices.size(); i++) {
			long min = Collections.min(prices.subList(0, i + 1));
			long val = min < 0 ? 0 : min;
			sum += prices.get(i) - val;
		}
		return sum;
	}
}

class AccountHolder {
	private int idNumber;
	private String firstName;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public int getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(int idNumber) {
		this.idNumber = idNumber;
	}

}
