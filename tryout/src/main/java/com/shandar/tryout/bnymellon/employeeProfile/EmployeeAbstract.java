/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.bnymellon.employeeProfile </p>
 * <p>File Name: Employee.java </p>
 * <p>Create Date: 31-Mar-2021 </p>
 * <p>Create Time: 2:38:54 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.bnymellon.employeeProfile;

/**
 * @author : Shantanu Sikdar
 *
 */
public abstract class EmployeeAbstract {

	abstract void setSalary(int salary);
	abstract int getSalary();
	
	abstract void setGrade(String grade);
	abstract String getGrade();
	
	void label() {
		System.out.println("Employees Data:\n");
	}
	
	
}

class Engineer extends EmployeeAbstract{
	
	private int salary;
	private String grade;

	public int getSalary() {
		return salary;
	}
	public void setSalary(int salary) {
		this.salary = salary;
	}
	public String getGrade() {
		return grade;
	}
	public void setGrade(String grade) {
		this.grade = grade;
	}	
	
}

class Manager extends EmployeeAbstract{

	private int salary;
	private String grade;
	
	public int getSalary() {
		return salary;
	}
	public void setSalary(int salary) {
		this.salary = salary;
	}
	public String getGrade() {
		return grade;
	}
	public void setGrade(String grade) {
		this.grade = grade;
	}
	
}