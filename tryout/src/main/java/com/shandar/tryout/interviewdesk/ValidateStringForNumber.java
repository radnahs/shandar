/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.interviewdesk </p>
 * <p>File Name: ValidateStringForNumber.java </p>
 * <p>Create Date: 18-Feb-2022 </p>
 * <p>Create Time: 3:00:51 PM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.interviewdesk;

/**
 * @author : Shantanu Sikdar
 *
 */
public class ValidateStringForNumber {

	public static void main(String[] args) {
		String myString = "123qwe123";
		//int foo = Integer.parseInt(myString);//will throw NumberFormatException
		
		String myString1 = "1234";
		String myString2 = "10010";

		if (isNumber(myString2)) {
			int val = parseInt(myString2);
			System.out.println(val);
		} else {
			System.out.println("Use a valid number");
		}

	}

	private static int parseInt(String myString) {
		int val;
		char[] chrArr = myString.toCharArray();
		//int chrArrLength = chrArr.length;
		int num = 0;
		for (int i = 0; i < chrArr.length; i++) {
			char c = chrArr[i];
			int nn = Character.getNumericValue(c);
			num = num * 10 + nn;
			//chrArrLength--;
		}
		val = num;

		return val;
	}

	private static boolean isNumber(String isNumber) {
		boolean val = true;
		char[] chrArr = isNumber.toCharArray();
		for (char c : chrArr) {
			if (c == '0' || c == '1' || c == '2' || c == '3' || c == '4' || c == '5' || c == '6' || c == '7' || c == '8'
					|| c == '9') {

			} else {
				val = false;
				break;
			}
		}
		return val;
	}

}
