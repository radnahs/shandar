/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.paypal </p>
 * <p>File Name: DecodeString.java </p>
 * <p>Create Date: 30-Apr-2021 </p>
 * <p>Create Time: 1:47:55 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.paypal;

import java.util.Stack;

/**
 * @author : Shantanu Sikdar
 *
 */
public class DecodeString {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String input = "3[a253[xc]]";
		System.out.println(solution(input));
	}

	// Decode String

	// Example 1:
	// Input: s = "3[a]2[bc]"
	// Output: "aaabcbc"

	// Example 2:
	// Input: s = "3[a2[c]]"
	// Output: "accaccacc"

	// Example 3:
	// Input: s = "2[abc]3[cd]ef"
	// Output: "abcabccdcdcdef"
	private static String solution(String input) {
		Stack<String> strStck = new Stack();
		Stack<Integer> numStck = new Stack();
		int indx = 0;
		StringBuilder sb = new StringBuilder();
		while (indx < input.length()) {
			if (Character.isDigit(input.charAt(indx))) {
				int num = 0;
				while (Character.isDigit(input.charAt(indx))) {
					num = num * 10 + (input.charAt(indx) - '0');
					indx++;
				}
				numStck.push(num);
			} else if (input.charAt(indx) == '[') {
				strStck.push(sb.toString());
				sb = new StringBuilder("");
				indx++;
			} else if (input.charAt(indx) == ']') {
				StringBuilder sbtmp = new StringBuilder(strStck.pop());
				for (int i = 0; i < numStck.pop(); i++) {
					sbtmp.append(sb);
				}
				sb = sbtmp;
				indx++;
			} else {
				sb.append(input.charAt(indx++));
			}
		}
		return sb.toString();
	}
	
}
