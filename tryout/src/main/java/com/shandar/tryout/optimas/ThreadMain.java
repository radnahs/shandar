/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.optimas </p>
 * <p>File Name: ThreadMain.java </p>
 * <p>Create Date: 07-Dec-2022 </p>
 * <p>Create Time: 3:44:51 PM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.optimas;

/**
 * @author : Shantanu Sikdar
 *
 */
public class ThreadMain {

	public static void main1(String[] args) {
		// creating two objects t1 & t2 of TestThread
		TestThread tThread1 = new TestThread("Thread One");
		TestThread tThread2 = new TestThread("Thread Two");
		try {
			Thread.sleep(500);
			tThread1.stop(); // stopping thread tThread1
			tThread2.stop(); // stopping thread tThread2
			Thread.sleep(500);
		} catch (InterruptedException e) {
			System.out.println("Caught:" + e);
		}
		System.out.println("Exiting the main Thread");
	}

	
	public static void main(String[] args) {
		Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
		    @Override
		    public void uncaughtException(Thread thread, Throwable throwable) {
		        // TODO
		    }
		});
	}
}

class TestThread implements Runnable {

	private boolean exit; // to stop the thread
	private String name;
	Thread thrd;

	TestThread(String threadname) {
		name = threadname;
		thrd = new Thread(this, name);
		System.out.println("New thread: " + thrd);
		exit = false;
		thrd.start(); // Starting the thread
	}

	// execution of thread starts from run() method
	public void run() {
		int i = 0;
		while (!exit) {
			System.out.println(name + ": " + i);
			i++;
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				System.out.println("Caught:" + e);
			}
		}
		System.out.println(name + " Stopped.");
	}

	// for stopping the thread
	public void stop() {
		exit = true;
	}
}