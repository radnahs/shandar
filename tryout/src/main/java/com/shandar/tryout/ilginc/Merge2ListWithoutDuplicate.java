/**
 * <p>Project: rytry </p>
 * <p>Package Name: com.shandar.tryout.ilginc </p>
 * <p>File Name: Merge2ListWithoutDuplicate.java </p>
 * <p>Create Date: 31-Mar-2022 </p>
 * <p>Create Time: 8:09:35 PM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.ilginc;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author : Shantanu Sikdar
 * @Description : Merge2ListWithoutDuplicate
 */
public class Merge2ListWithoutDuplicate {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		List<Integer> listOne = new ArrayList<Integer>();
		listOne.add(2);
		listOne.add(3);
		listOne.add(4);
		listOne.add(5);
		listOne.add(6);
		listOne.add(7);

		List<Integer> listTwo  = new ArrayList<Integer>();
		listTwo.add(1);
		listTwo.add(3);
		listTwo.add(9);
		listTwo.add(11);
		listTwo.add(2);
		listTwo.add(8);

		Set<Integer> combinedList = Stream.of(listOne, listTwo).flatMap(x -> x.stream()).collect(Collectors.toSet());
		
		System.out.println(combinedList);
	}

}

