/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.ilginc </p>
 * <p>File Name: TrieAutocomplete.java </p>
 * <p>Create Date: 04-Mar-2022 </p>
 * <p>Create Time: 10:27:36 PM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.ilginc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author : Shantanu Sikdar
 *
 *         https://www.geeksforgeeks.org/auto-complete-feature-using-trie/
 * 
 */
public class TrieAutocomplete {

	public static void main(String[] args) {
		List<String> words = List.of("hello", "dog", "hell", "cat", "a", "hel", "help", "helps", "helping");
		Trie trie = new Trie(words);

		System.out.println(trie.suggest("hel"));
	}

}

class Trie {

	class TrieNode {
		Map<Character, TrieNode> children;
		char c;
		boolean isWord;

		public TrieNode(char c) {
			this.c = c;
			children = new HashMap<>();
		}

		public TrieNode() {
			children = new HashMap<>();
		}

		public void insert(String word) {
			if (word == null || word.isEmpty()) {
				return;
			}
			char firstChar = word.charAt(0);
			TrieNode child = children.get(firstChar);
			if (child == null) {
				child = new TrieNode(firstChar);
				children.put(firstChar, child);
			}

			if (word.length() > 1) {
				child.insert(word.substring(1));
			} else {
				child.isWord = true;
			}
		}
	}

	TrieNode root;

	public Trie(List<String> words) {
		root = new TrieNode();
		for (String word : words) {
			root.insert(word);
		}
	}

	public boolean find(String prefix, boolean exact) {
		TrieNode lastNode = root;
		for (char chr : prefix.toCharArray()) {
			lastNode = lastNode.children.get(chr);
			if (lastNode == null) {
				return false;
			}
		}
		return !exact || lastNode.isWord;
	}

	public boolean find(String prefix) {
		return find(prefix, false);
	}

	public void suggestHelper(TrieNode root, List<String> list, StringBuffer curr) {
		if (root.isWord) {
			list.add(curr.toString());
		}
		if (root == null || root.children.isEmpty()) {
			return;
		}
		for (TrieNode child : root.children.values()) {
			suggestHelper(root, list, curr.append(child.c));
			curr.setLength(curr.length() - 1);
		}
	}

	public List<String> suggest(String prefix) {
		List<String> list = new ArrayList<>();
		TrieNode lastNode = root;
		StringBuffer curr = new StringBuffer();
		for (char c : prefix.toCharArray()) {
			lastNode = lastNode.children.get(c);
			if (lastNode == null) {
				return list;
			}
			curr.append(c);
		}
		suggestHelper(lastNode, list, curr);
		return list;
	}

}
