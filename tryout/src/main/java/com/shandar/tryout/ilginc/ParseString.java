/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.ilginc </p>
 * <p>File Name: ParseString.java </p>
 * <p>Create Date: 04-Mar-2022 </p>
 * <p>Create Time: 10:02:46 PM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.ilginc;

/**
 * @author : Shantanu Sikdar
 *	
 *	Please parse out the numerals in the following string and print them to the screen
 *	@@37&*(23510)000{[h68
 */
public class ParseString {


	public static void main(String[] args) {
		//System.out.println("Run");
		pasreNumbers("@@37&*(23510)000{[h68");
	}
	
	private static String pasreNumbers(String str){
		char[] regMatch =  str.toCharArray();//"\\d";
		for (char c : regMatch) {
			if(c>47 && c<=57) {
				System.out.print(c+" ");
			}
		}
		return null;
	}
	
	
	
}
