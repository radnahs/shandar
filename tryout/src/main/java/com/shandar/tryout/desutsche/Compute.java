/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.desutsche </p>
 * <p>File Name: Compute.java </p>
 * <p>Create Date: 08-Apr-2020 </p>
 * <p>Create Time: 3:11:46 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.desutsche;

/**
 * @author : Shantanu Sikdar
 *
 */
public class Compute {

	public static void main(String[] args) {
		int result=0, x=1;
		while(x<=10) {
			  if (x%2 == 0) 
				  result += x ;
              ++ x ;
		}
		System.out.println(result) ;
	}

}
