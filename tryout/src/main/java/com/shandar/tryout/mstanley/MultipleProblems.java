/**
 * Project: tryout 
 * Package Name: com.shandar.tryout.mstanley 
 * File Name: MultipleProblems.java 
 * Create Date: 30-May-2021 
 * Create Time: 9:59:43 pm 
 * Copyright: Copyright (c) 2016
 * Company:  
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.mstanley;

/**
 * @author : Shantanu Sikdar
 *
 */
public class MultipleProblems {

	public static void main1(String[] args) {
		int x = 0;
		int y = 0;
		for (int z = 0; z < 5; z++) {
			if ((++x > 2) && (++y > 2)) {
				x++;
			}
		}
		System.out.println(x + " " + y);
	}

	public static void main2(String args[]) {
		try {
			throw new Exc1(); /* Line 9 */
		} catch (Exc0 e0) /* Line 11 */
		{
			System.out.println("Ex0 caught");
		} catch (Exception e) {
			System.out.println("exception caught");
		}
	}

	public static void main(String[] args) {
		PassA p = new PassA();
		p.start();
	}

}

class Exc0 extends Exception {
}

class Exc1 extends Exc0 {
}

class PassA {

	void start() {
		long[] a1 = { 3, 4, 5 };
		long[] a2 = fix(a1);
		System.out.print(a1[0] + a1[1] + a1[2] + " ");
		System.out.println(a2[0] + a2[1] + a2[2]);
	}

	long[] fix(long[] a3) {
		a3[1] = 7;
		return a3;
	}
}
