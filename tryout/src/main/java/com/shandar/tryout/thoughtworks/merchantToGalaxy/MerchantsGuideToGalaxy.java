/**
 * <p>Project: com.shandar.tryout.thoughtworks.merchantToGalaxy </p>
 * <p>Package Name: com.shandar.tryout.thoughtworks.merchantToGalaxy </p>
 * <p>File Name: MerchantsGuideToGalaxy.java </p>
 * <p>Create Date: Apr 18, 2017 </p>
 * <p>Create Time: 10:19:14 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.tryout.thoughtworks.merchantToGalaxy;

import com.shandar.tryout.thoughtworks.merchantToGalaxy.util.InputOutput;

/**
 * @author: Shantanu Sikdar
 */
public class MerchantsGuideToGalaxy {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		InputOutput io = new InputOutput();
		io.readFile("D:\\Projects\\WebApplications\\tryout\\trunk\\src\\main\\java\\com\\webtual\\tryout\\tests\\thoughtworks\\merchantToGalaxy\\input.txt");
		
		io.writeFile("D:\\Projects\\WebApplications\\tryout\\trunk\\src\\main\\java\\com\\webtual\\tryout\\tests\\thoughtworks\\merchantToGalaxy\\output.txt");
	}

}
