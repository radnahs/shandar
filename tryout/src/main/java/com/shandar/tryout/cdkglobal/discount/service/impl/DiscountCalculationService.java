/**
 * <p>Project: bend </p>
 * <p>Package Name: com.shandar.tryout.cdkglobal.discount.service.impl </p>
 * <p>File Name: ShoppingCartService.java </p>
 * <p>Create Date: 05-Apr-2020 </p>
 * <p>Create Time: 11:54:38 am </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.cdkglobal.discount.service.impl;

import java.io.IOException;

import com.shandar.tryout.cdkglobal.discount.bean.DiscountedValue;
import com.shandar.tryout.cdkglobal.discount.bean.OrderValue;
import com.shandar.tryout.cdkglobal.discount.domain.Discount;
import com.shandar.tryout.cdkglobal.discount.domain.DiscountList;
import com.shandar.tryout.cdkglobal.discount.domain.DiscountSlab;
import com.shandar.tryout.cdkglobal.discount.service.DiscountCalculation;
import com.shandar.tryout.cdkglobal.discount.service.DiscountRule;

public class DiscountCalculationService implements DiscountCalculation {

	@Override
	public DiscountedValue calculateDiscountedValue(OrderValue orderValue) throws IOException {
		DiscountedValue discountedInvoice = new DiscountedValue();
		discountedInvoice.setOrderedAmount(orderValue);
		double discountAmount = processDiscount(orderValue, discountedInvoice);
		double payableAmount = Double.parseDouble(orderValue.getOrderAmount()) -  discountAmount;
		discountedInvoice.setDiscountedAmount(Double.toString(discountAmount));
		discountedInvoice.setPayableAmount(Double.toString(payableAmount));
		return discountedInvoice;
	}

	/**
	 * 
	 * @param orderValue
	 * @param discountedInvoice
	 * @return
	 * @throws IOException
	 */
	private double processDiscount(OrderValue orderValue, DiscountedValue discountedInvoice) throws IOException {
		DiscountRule discountRule = new DiscountRuleService();
		DiscountList discountList = discountRule.readDiscountJSON();
		double discountAmount = 0;
		for (Discount discount : discountList.getDiscountList()) {
			if (discount.getDiscountType().equals(orderValue.getCustomerType())) {
				for (DiscountSlab discountSlab : discount.getDiscountSlab()) {
					if (Double.parseDouble(orderValue.getOrderAmount()) > Double.parseDouble(discountSlab.getRange().getLower())
							&& ( "above".equalsIgnoreCase(discountSlab.getRange().getUpper())
								|| Double.parseDouble(orderValue.getOrderAmount()) <= Double.parseDouble(discountSlab.getRange().getUpper())
								) ) {
						discountAmount += calculateDiscount(discountSlab, orderValue.getOrderAmount());
						break;
					} else {
						discountAmount += calculateDiscount(discountSlab, discountSlab.getRange().getUpper());
					}
				}
				break;
			}
		}
		
		return discountAmount;
	}
	
	private double calculateDiscount(DiscountSlab discountSlab, String vals){
		Double amnt = Double.parseDouble(vals);
		Double lower = Double.parseDouble(discountSlab.getRange().getLower());
		Double percent = Double.parseDouble(discountSlab.getDiscount());
		return (amnt-lower)*percent/100;
	}

}
