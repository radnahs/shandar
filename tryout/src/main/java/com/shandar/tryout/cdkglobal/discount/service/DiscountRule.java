/**
 * <p>Project: bend </p>
 * <p>Package Name: com.shandar.tryout.cdkglobal.discount.service </p>
 * <p>File Name: DomainService.java </p>
 * <p>Create Date: 05-Apr-2020 </p>
 * <p>Create Time: 4:13:55 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.cdkglobal.discount.service;

import java.io.IOException;

import com.shandar.tryout.cdkglobal.discount.domain.DiscountList;

public interface DiscountRule {

	public DiscountList readDiscountJSON() throws IOException;
	
}
