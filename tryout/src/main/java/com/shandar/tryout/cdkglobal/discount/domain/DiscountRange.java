/**
 * <p>Project: bend </p>
 * <p>Package Name: com.shandar.tryout.cdkglobal.discount.domain </p>
 * <p>File Name: DiscountRange.java </p>
 * <p>Create Date: 06-Apr-2020 </p>
 * <p>Create Time: 12:42:58 am </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.cdkglobal.discount.domain;

/**
 * @author Shantanu Sikdar
 *
 */
public class DiscountRange {

	private String lower;
	private String upper;

	/**
	 * @return the lower
	 */
	public String getLower() {
		return lower;
	}

	/**
	 * @param lower the lower to set
	 */
	public void setLower(String lower) {
		this.lower = lower;
	}

	/**
	 * @return the upper
	 */
	public String getUpper() {
		return upper;
	}

	/**
	 * @param upper the upper to set
	 */
	public void setUpper(String upper) {
		this.upper = upper;
	}

	@Override
	public String toString() {
		return "DiscountRange [lower=" + lower + ", upper=" + upper + "]";
	}

}
