/**
 * <p>Project: com.shandar.tryout.cdkglobal </p>
 * <p>Package Name: com.shandar.tryout.cdkglobal </p>
 * <p>File Name: Problem14.java </p>
 * <p>Create Date: May 10, 2017 </p>
 * <p>Create Time: 9:25:30 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.tryout.cdkglobal;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: Shantanu Sikdar
 */
public class Problem14 {

	public static void main(String[] args) {
		final List<String> lst = new ArrayList<>();
		lst.add("hello");
		System.out.println(lst.size());
	}

}
