/**
 * <p>Project: bend </p>
 * <p>Package Name: com.shandar.tryout.cdkglobal.discount.bean </p>
 * <p>File Name: DiscountedValue.java </p>
 * <p>Create Date: 05-Apr-2020 </p>
 * <p>Create Time: 6:30:55 pm </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.cdkglobal.discount.bean;

public class DiscountedValue {

	private String discountedAmount;
	private String payableAmount;
	private OrderValue orderedAmount;

	private String error;

	/**
	 * @return the discountedAmount
	 */
	public String getDiscountedAmount() {
		return discountedAmount;
	}

	/**
	 * @param discountedAmount the discountedAmount to set
	 */
	public void setDiscountedAmount(String discountedAmount) {
		this.discountedAmount = discountedAmount;
	}

	/**
	 * @return the payableAmount
	 */
	public String getPayableAmount() {
		return payableAmount;
	}

	/**
	 * @param payableAmount the payableAmount to set
	 */
	public void setPayableAmount(String payableAmount) {
		this.payableAmount = payableAmount;
	}

	/**
	 * @return the orderedAmount
	 */
	public OrderValue getOrderedAmount() {
		return orderedAmount;
	}

	/**
	 * @param orderedAmount the orderedAmount to set
	 */
	public void setOrderedAmount(OrderValue orderedAmount) {
		this.orderedAmount = orderedAmount;
	}

	/**
	 * @return the error
	 */
	public String getError() {
		return error;
	}

	/**
	 * @param error the error to set
	 */
	public void setError(String error) {
		this.error = "No Error";
	}

	@Override
	public String toString() {
		return "DiscountedValue [discountedAmount=" + discountedAmount + ", payableAmount=" + payableAmount
				+ ", orderedAmount=" + orderedAmount + "]";
	}

}
