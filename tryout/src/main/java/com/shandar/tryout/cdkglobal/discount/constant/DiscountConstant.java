/**
 * <p>Project: bend </p>
 * <p>Package Name: com.shandar.tryout.cdkglobal.discount.constant </p>
 * <p>File Name: DiscountConstant.java </p>
 * <p>Create Date: 05-Apr-2020 </p>
 * <p>Create Time: 6:13:57 pm </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.cdkglobal.discount.constant;

/**
 * @author Shantanu Sikdar
 *
 */
public interface DiscountConstant {
	
	String JSON_FILE = "E:\\cdk_discount\\bend\\src\\main\\resources\\static\\disount.json";
	String CUSTOMER_TYPE_REGULAR = "Regular";
	String CUSTOMER_TYPE_PREMIUM = "Premium";
}
