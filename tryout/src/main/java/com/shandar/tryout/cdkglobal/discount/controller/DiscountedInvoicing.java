/**
 * <p>Project: bend </p>
 * <p>Package Name: com.shandar.tryout.cdkglobal.discount.controller </p>
 * <p>File Name: Invoicing.java </p>
 * <p>Create Date: 05-Apr-2020 </p>
 * <p>Create Time: 5:54:27 pm </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.cdkglobal.discount.controller;
//import org.springframework.stereotype.Controller;

import com.shandar.tryout.cdkglobal.discount.bean.DiscountedValue;
import com.shandar.tryout.cdkglobal.discount.bean.OrderValue;

//@Controller
public interface DiscountedInvoicing {

	public DiscountedValue discountedInvoice(OrderValue orderValue)throws Exception;

}
