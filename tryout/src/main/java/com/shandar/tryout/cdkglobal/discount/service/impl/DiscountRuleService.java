/**
 * <p>Project: bend </p>
 * <p>Package Name: com.shandar.tryout.cdkglobal.discount.service.impl </p>
 * <p>File Name: DomainServiceImpl.java </p>
 * <p>Create Date: 05-Apr-2020 </p>
 * <p>Create Time: 4:17:59 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.cdkglobal.discount.service.impl;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.shandar.tryout.cdkglobal.discount.domain.DiscountList;
import com.shandar.tryout.cdkglobal.discount.service.DiscountRule;
import static com.shandar.tryout.cdkglobal.discount.constant.DiscountConstant.*;


public class DiscountRuleService implements DiscountRule {

	@Override
	public DiscountList readDiscountJSON() throws IOException {
		String strFileName = JSON_FILE;
		DiscountList discountList = new DiscountList();
		byte[] json = Files.readAllBytes(Paths.get(strFileName));
		ObjectMapper mapper = new ObjectMapper();
		try {
			discountList = mapper.readValue(json, DiscountList.class);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return discountList;
	}

	
}
