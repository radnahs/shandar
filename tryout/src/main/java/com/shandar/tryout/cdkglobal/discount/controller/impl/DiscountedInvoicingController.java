/**
 * <p>Project: bend </p>
 * <p>Package Name: com.shandar.tryout.cdkglobal.discount.controller.impl </p>
 * <p>File Name: DiscountedInvoicingController.java </p>
 * <p>Create Date: 05-Apr-2020 </p>
 * <p>Create Time: 5:55:40 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.cdkglobal.discount.controller.impl;

import static com.shandar.tryout.cdkglobal.discount.constant.DiscountConstant.*;

import java.util.Objects;

import com.shandar.tryout.cdkglobal.discount.bean.DiscountedValue;
import com.shandar.tryout.cdkglobal.discount.bean.OrderValue;
import com.shandar.tryout.cdkglobal.discount.controller.DiscountedInvoicing;
import com.shandar.tryout.cdkglobal.discount.service.DiscountCalculation;
import com.shandar.tryout.cdkglobal.discount.service.impl.DiscountCalculationService;

public class DiscountedInvoicingController implements DiscountedInvoicing {

	@Override
	public DiscountedValue discountedInvoice(OrderValue orderValue) throws Exception{
		DiscountCalculation ds = new DiscountCalculationService();
		if(!Objects.isNull(orderValue)) {
			return ds.calculateDiscountedValue(orderValue);
		}else {
			DiscountedValue dv = new DiscountedValue();
			dv.setError("some error");
			return dv;
		}
		
	}
	
	public static void main(String[] args) throws Exception{
		OrderValue orderValue = new OrderValue();
		orderValue.setOrderAmount("14000");
		//orderValue.setCustomerType(CUSTOMER_TYPE_REGULAR);
		orderValue.setCustomerType(CUSTOMER_TYPE_PREMIUM);
		DiscountedInvoicing di = new DiscountedInvoicingController();
		DiscountedValue dv = di.discountedInvoice(orderValue);
		System.out.println(dv);
	}
	
}
