/**
 * <p>Project: bend </p>
 * <p>Package Name: com.shandar.tryout.cdkglobal.discount.service </p>
 * <p>File Name: ShoppingCartService.java </p>
 * <p>Create Date: 05-Apr-2020 </p>
 * <p>Create Time: 11:54:01 am </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.cdkglobal.discount.service;

import java.io.IOException;

import com.shandar.tryout.cdkglobal.discount.bean.DiscountedValue;
import com.shandar.tryout.cdkglobal.discount.bean.OrderValue;

public interface DiscountCalculation {
	
	/**
	 * 
	 * @param orderValue
	 * @return
	 * @throws IOException
	 */
	public DiscountedValue calculateDiscountedValue(OrderValue orderValue) throws IOException;
	
}
