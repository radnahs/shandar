/**
 * <p>Project: bend </p>
 * <p>Package Name: com.shandar.tryout.cdkglobal.discount.domain </p>
 * <p>File Name: Slab.java </p>
 * <p>Create Date: 05-Apr-2020 </p>
 * <p>Create Time: 2:45:12 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.cdkglobal.discount.domain;

/**
 * @author Shantanu Sikdar
 *
 */
public class DiscountSlab {

	private DiscountRange range;
	private String discount;

	/**
	 * @return the range
	 */
	public DiscountRange getRange() {
		return range;
	}

	/**
	 * @param range the range to set
	 */
	public void setRange(DiscountRange range) {
		this.range = range;
	}

	/**
	 * @return the discount
	 */
	public String getDiscount() {
		return discount;
	}

	/**
	 * @param discount the discount to set
	 */
	public void setDiscount(String discount) {
		this.discount = discount;
	}

	@Override
	public String toString() {
		return "DiscountSlab [range=" + range + ", discount=" + discount + "]";
	}

}
