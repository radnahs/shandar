/**
 * <p>Project: bend </p>
 * <p>Package Name: com.shandar.tryout.cdkglobal.discount.domain </p>
 * <p>File Name: DiscountList.java </p>
 * <p>Create Date: 05-Apr-2020 </p>
 * <p>Create Time: 6:15:41 pm </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.cdkglobal.discount.domain;

import java.util.List;

public class DiscountList {

	private List<Discount> discountList;

	/**
	 * @return the discountList
	 */
	public List<Discount> getDiscountList() {
		return discountList;
	}

	/**
	 * @param discountList the discountList to set
	 */
	public void setDiscountList(List<Discount> discountList) {
		this.discountList = discountList;
	}

	@Override
	public String toString() {
		return "DiscountList [discountList=" + discountList + "]";
	}
	
}
