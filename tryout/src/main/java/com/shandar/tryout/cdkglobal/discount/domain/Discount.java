/**
 * <p>Project: bend </p>
 * <p>Package Name: com.shandar.tryout.cdkglobal.discount.domain </p>
 * <p>File Name: Discount.java </p>
 * <p>Create Date: 05-Apr-2020 </p>
 * <p>Create Time: 12:59:06 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.cdkglobal.discount.domain;

import java.util.List;

public class Discount {

	private String discountType;
	private List<DiscountSlab> discountSlab;

	/**
	 * @return the discountType
	 */
	public String getDiscountType() {
		return discountType;
	}

	/**
	 * @param discountType the discountType to set
	 */
	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}

	/**
	 * @return the discountSlab
	 */
	public List<DiscountSlab> getDiscountSlab() {
		return discountSlab;
	}

	/**
	 * @param discountSlab the discountSlab to set
	 */
	public void setDiscountSlab(List<DiscountSlab> discountSlab) {
		this.discountSlab = discountSlab;
	}

	@Override
	public String toString() {
		return "Discount [discountType=" + discountType + ", discountSlab=" + discountSlab + "]";
	}

}
