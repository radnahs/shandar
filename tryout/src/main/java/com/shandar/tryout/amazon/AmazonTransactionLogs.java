/**
 * Project: tryout 
 * Package Name: com.shandar.tryout.amazon 
 * File Name: AmazonTransactionLogs.java 
 * Create Date: 12-Jul-2022 
 * Create Time: 6:24:45 PM 
 * Copyright: Copyright (c) 2016
 * Company:  
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.amazon;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author : Shantanu Sikdar
 * 
 *         BETA Can’t read the text? Switch theme 1. Amazon Transaction Logs
 *         (example question) 
 * 
 *         1 &le; |source| &le; 10^6 Your Amazonian team is responsible for
 *         maintaining a monetary transaction service. The transactions are
 *         tracked in a log file.
 * 
 *         A log file is provided as a string array where each entry represents
 *         a transaction to service. Each transaction consists of:
 * 
 * 
 *         sender_user_id : Unique identifier for the user that initiated the
 *         transaction. It consists of only digits with at most 9 digits.
 *         recipient_user_id: Unique identifier for the user that is receiving
 *         the transaction. It consists of only digits with at most 9 digits.
 *         amount_of_transaction : The amount of the transaction. It consists of
 *         only digits with at most 9 digits.
 * 
 * 
 *         The values are separated by a space. For example, " sender_user_id
 *         recipient_user_id amount_of_transaction ".
 * 
 *         Users that perform an excessive amount of transactions might be
 *         abusing the service so you have been tasked to identify the users
 *         that have a number of transactions over a threshold. The list of user
 *         ids should be ordered in ascending numeric value.
 * 
 * 
 *         Example
 *         logs = ["88 99 200", "88 99 300", "99 32 100", " 12 12 15"]
 * 
 *         threshold = 2
 * 
 *         The transactions count for each user, regardless of role are:
 * 
 *         ID Transactions -- ------------ 99 3 88 2 12 1 32 1
 * 
 *         There are two users with at least threshold = 2 transactions: 99 and
 *         88. In ascending order, the return array is ['88', '99'].
 * 
 * 
 *         Note: In the last log entry, user 12 was on both
 *         sides of the transaction. This counts as only 1 transaction for user
 *         12 .
 * 
 *         Function Description 
 * 
 *         Complete the function processLogs in the editor below.
 * 
 *         The function has the following parameter(s):
 * 
 *         string logs[n] : each logs[i] denotes the ith entry in the
 *         logs
 * 
 *         int threshold : the minimum number of transactions that a user must
 *         have to be included in the result
 * 
 *         Returns:
 * 
 *         string[]: an array of user id's as strings, sorted ascending by
 *         numeric value
 * 
 *         Constraints
 *         1 ≤ n ≤ 105 1 ≤ threshold ≤ n
 * 
 *         The sender_user_id, recipient_user_id and amount_of_transaction
 *         contain only characters in the range ascii['0'-'9'].
 * 
 *         The sender_user_id, recipient_user_id and amount_of_transaction start
 *         with a non-zero digit.
 * 
 *         0 < length of sender_user_id, recipient_user_id,
 *         amount_of_transaction ≤ 9.
 * 
 *         The result will contain at least one element.
 * 
 *         <!-- <StartOfInputFormat> DO NOT REMOVE THIS LINE-->
 * 
 *         Input Format Format for Custom Testing
 * 
 *         Input from stdin will be processed as follows and passed to the
 *         function .
 * 
 *         The first line contains the integer, n, the size of logs
 * 
 *         The following n lines contain a string, logs[i] .
 * 
 *         The last line contains an integer, threshold.
 * 
 *         <!-- </StartOfInputFormat> DO NOT REMOVE THIS LINE-->
 * 
 *         Sample Case 0
 * 
 *         Sample Input
 * 
 *         STDIN Function ----- -------- 4 → logs[] size n = 4 1 2 50 → logs =
 *         ["1 2 50", "1 7 70", "1 3 20", "2 2 17"] 1 7 70 1 3 20 2 2 17 2 →
 *         threshold = 2
 * 
 *         Sample Output
 *         1 2
 * 
 *         Explanation
 * 
 *         ID Transactions -- ------------ 1 3 2 2 7 1 3 1
 * 
 *         Only users 1 and 2 have at least threshold = 2 transactions. The
 *         return array in numerically ascending order is ["1", "2"] . Note that
 *         in the last log entry, the user with id 2 performed both roles in the
 *         transaction. This is counted as one transaction for the user.
 * 
 *         Sample Case 1
 * 
 *         Sample Input
 *         STDIN Function ----- -------- 4 → logs[] size n = 4 9 7 50 → logs =
 *         ["9 7 50", "22 7 20", "33 7 50", "22 7 30"] 22 7 20 33 7 50 2 2 7 30
 *         3 → threshold = 3
 * 
 *         Sample Output
 *         7
 * 
 *         Explanation
 * 
 *         ID Transactions -- ------------ 9 1 7 4 22 2 33 1
 * 
 *         Only user 7 has 3 or more transactions. The return array is ["7"].
 */

public class AmazonTransactionLogs {

	public static void main(String[] args) {

	}

	public static List<String> processLogs(List<String> logs, int threshold) {
		// Write your code
		Set<Integer> returnSet = new HashSet<>();
		Map<Integer, Long> mapIdCount = new HashMap<>();
		for (String str : logs) {
			String[] idArr = str.split(" ");
			for (int i = 0; i < idArr.length - 1; i++) {
				Integer id = Integer.parseInt(idArr[i]);
				Long count = 0l;
				if (mapIdCount.containsKey(id)) {
					count = mapIdCount.get(id) + 1;
					mapIdCount.put(id, count);
				} else {
					mapIdCount.put(id, 1l);
				}
				if (count > threshold) {
					returnSet.add(id);
				}
			}

		}
		List<Integer> returnList = new ArrayList<>(returnSet);
		Collections.sort(returnList);
		List<String> returnListOrig = new ArrayList<>();
		for (Integer itn : returnList) {
			returnListOrig.add(itn + "");
		}
		return returnListOrig;

	}

}
