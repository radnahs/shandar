/**
 * Project: tryout 
 * Package Name: com.shandar.tryout.smarsh 
 * File Name: Pangrams.java 
 * Create Date: 27-May-2021 
 * Create Time: 9:54:55 pm 
 * Copyright: Copyright (c) 2016
 * Company:  
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.smarsh;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author : Shantanu Sikdar
 * 
 *         6. Are they Pangrams
 * 
 *         A string is a pangram if it contains all letters of the English
 *         alphabet, ascii['a'-'z']. Given a list of strings, determine if each
 *         one is a pangram or not. Return "1" if true and "0" if false.
 *         
 *         Example
 *         
 *         pangram = ['pack my box with five dozen liquor jugs', 'this is
 *         not a pangram']
 *         
 *         the string 'pack my box with five dozen liquor jugs' is
 *         a pangram , because it contains all the letters 'a' through 'z'
 *         the string'this is not a pangram' is not a pangram
 *         
 *         Assemble a string of the two results, in order. The result
 *         is '10'.
 * 
 *         Function Description Complete the function
 *         isPangram n the editor below.
 *         
 *         isPangram has the following parameter(s):
 *         
 *         string pangram[n]: an array of
 *         strings
 *         
 *         Returns:
 *         
 *         string: a string where each position
 *         represents the results of a test. Use '1' for true and
 *         '0' for false.
 *         
 *         Constraints
 *         
 * 
 *         <ul>
 *         1 ≤ n ≤ 100
 *         Each string pangram[i] (where 0 ≤ i &lt; n) is
 *         composed of lowercase letters and spaces.
 *         1 ≤ length of pangram[i] ≤ 10<sup>5</sup>
 *         </ul>
 * 
 *         <!-- <StartOfInputFormat> DO NOT REMOVE THIS LINE-->
 * 
 *         <details><summary class="section-title">Input Format for Custom
 *         Testing</summary>
 * 
 *         <div class="collapsable-details">
 *         
 *         Input from stdin will be processed as follows and passed to the
 *         function.
 * 
 *         The first line contains an integer n, the size of the array
 *         pangram.
 *         
 *         The next n lines each contain an element,
 *         pangram[i], where 0 ≤ i &lt; n.
 *         
 *         </div> </details>
 * 
 *         <details open="open"><summary class="section-title">Sample Case
 *         0</summary>
 * 
 *         <div class="collapsable-details">
 *         
 *         Sample Input 0
 *         
 * 
 *         
 * STDIN                                                        Function Parameters 
-----                                                        ------------------- 
4                                                             → pangram[] size n = 4 
we promptly judged antique ivory buckles for the next prize   → pangram[] = ["we promptly judged antique ivory buckles for the next prize",
we promptly judged antique ivory buckles for the prizes                       "we promptly judged antique ivory buckles for the prizes",
the quick brown fox jumps over the lazy dog                                   "the quick brown fox jumps over the lazy dog", 
the quick brown fox jump over the lazy dog                                    "the quick brown fox jump over the lazy dog" ]
 * 
 *         Sample Output 0
 *         
 *         1010
 *         
 *         Explanation 0
 *         
 * 
 * 	pangram[0] = True
	pangram[1] = False
	pangram[2] = True
	pangram[3] = False
 *         
 * 
 *         The strings pangram[0] and pangram[2] are
 *         pangrams, and the others are not.
 *         The result is '1010'
 * 
 *         Sample Case
 *         1

 *         Sample Input 1
 * 
 *         
 * STDIN                                                                   Function Parameters
-----                                                                   -------------------
4                                                                         →pangram[] Size n = 4 
cfchcfcvpalpqxenhbytcwazpxtthjumliiobcznbefnofyjfsrwfecxcbmoafes tnulqkvx
oxhctvhybtikkgeptqulzukfmmavacshugpouxoliggcomykdnfayayqutgwivwldrkp
gpecfrak zzaxrigltstcrdyhelhz rasrzibduaq  cnpuommogatqem
hbybsegucruhxkebrvmrmwhweirx mbkluwhfapjtga liiylfphmzkq
 *         
 *         Sample Output 1
 *         
 *         0000
 *         
 *         Explanation 1
 * 
 *         <pre style="font-style: normal;font-variant: normal;font-weight:
 *         normal;">
 * pangram[0] = False
pangram[1] = False
pangram[2] = False
pangram[3] = False
 *         
 * 
 *         No string is a pangram.
 *         The result is '0000'
 */
public class Pangrams {

	public static void main(String[] args) {
		List<String> strLst = new ArrayList<String>();
		strLst.add("We promptly judged antique ivory buckles for the prize");
		strLst.add("We promptly judged antique ivory buckles for the next prize");
		
		System.out.println(isPangram(strLst));
		
		
	}

	public static String isPangram(List<String> pangram) {
		// Write your code here
		Set<Character> setAlphabetCount = new HashSet<Character>();
		for (char ch = 'a'; ch <= 'z'; ch++) {
			setAlphabetCount.add(ch);
		}

		StringBuilder retStr=new StringBuilder();
		for (String sentence : pangram) {
			List<Character> listChr = new ArrayList<Character>();
			for (String word : sentence.split(" ")) {
				//strBldr.add(character);
				char[] chrarr = word.toCharArray();
				for(int i=0;i<chrarr.length; i++) {
					listChr.add(chrarr[i]);
				}
			}
			
			boolean contain = false;
			for (Character character : setAlphabetCount) {
				if (listChr.contains(Character.toLowerCase(character))
						|| listChr.contains(Character.toUpperCase(character))) {
					contain = true;
				} else {
					contain = false;
					break;
				}
			}
			String ss = contain ? "1" : "0";
			retStr.append(ss);
			
		}
		
		return retStr.toString();
		
	}

}
