/**
 * Project: tryout 
 * Package Name: com.shandar.tryout.smarsh 
 * File Name: Smarsh4.java 
 * Create Date: 27-May-2021 
 * Create Time: 8:10:46 pm 
 * Copyright: Copyright (c) 2016
 * Company:  
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.smarsh;

/**
 * @author : Shantanu Sikdar
 *
 *         <pre>
 *  1. Find the number! 
 *  
 *  Given an unsorted array of n elements, find if
 *  the element k is present in the array or not.
 * 
 *  Complete the findNumber&nbsp;function in the editor below. It has 2
 *  parameters:
 * 
 *  An array of integers, arr, denoting the elements in the array. An
 *  integer, k, denoting the element to be searched in the array.
 * 
 *  The function must return a string "YES" or "NO"&nbsp;denoting if the
 *  element is present in the array or not.
 * 
 *  Input Format
 * 
 *  The first line contains an integer n, denoting the number of elements
 *  in the array arr.
 * 
 *  Each line i of the n subsequent lines (where 0 ≤ i &lt; n) contains
 *  an integer describing arr<sub>i</sub>.
 * 
 *  The next line contains an integer, k, the element that needs to be
 *  searched.&nbsp;
 * 
 *  Constraints 1 ≤ n ≤ 10<sup>5</sup> 1 ≤ arr[i] ≤ 10<sup>9</sup>
 * 
 *  Output Format
 * 
 *  The function must return a string "YES" or "NO"&nbsp;denoting if the
 *  element is present in the array or not. This is printed to stdout by
 *  locked stub code in the editor.
 * 
 *  Sample Input 0
 * 
 *  5 1 2 3 4 5 1
 * 
 *  Sample Output 0
 * 
 *  YES
 * 
 *  Explanation 0
 * 
 *  Given the array = [1, 2, 3, 4, 5], we want to find the element 1 if
 *  it is present or not. We can find the element 1 at index = 0.
 *  Therefore we print "YES".
 * 
 *  Sample Input 1
 * 
 *  3 2 3 1 5
 * 
 *  Sample Output 1
 * 
 *  NO
 * 
 * 
 *  Explanation 1
 * 
 *  Given the array [2, 3, 1] and k = 5. There is no element 5 in the
 *  array and therefore we print "NO".
 *         </pre>
 * 
 */
public class FindTheNumber {

	public static void main(String[] args) {

	}

}
