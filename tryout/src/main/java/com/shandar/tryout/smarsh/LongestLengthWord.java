/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.smarsh </p>
 * <p>File Name: HackerR.java </p>
 * <p>Create Date: 27-May-2021 </p>
 * <p>Create Time: 8:02:23 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.smarsh;

/**
 * @author : Shantanu Sikdar
 *
 */
public class LongestLengthWord {

	public static void main(String[] args) {

		System.out.println(evenLengthLongestWord("I am shantanu sikdar, and I am an software developer"));

	}

	private static String evenLengthLongestWord(String ss) {
		String[] strArr = ss.split(" ");
		String retValue = "";
		for (String string : strArr) {
			if (string.length() % 2 == 0) {
				if (retValue.length() < string.length()) {
					retValue = string;
				}
			}
		}

		if (retValue.length() == 0) {
			retValue = "00";
		}
		System.out.println(retValue);
		return retValue;
	}

}
