/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.smarsh </p>
 * <p>File Name: Smarsh3.java </p>
 * <p>Create Date: 27-May-2021 </p>
 * <p>Create Time: 8:03:18 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.smarsh;

import java.util.Collections;
import java.util.List;

/**
 * @author : Shantanu Sikdar
 *
 */
public class Merge2Arrays {

	public static void main(String[] args) {

	}

	public static List<Integer> mergeArrays(List<Integer> a, List<Integer> b) {
		a.addAll(b);
		Collections.sort(a);
		return a;
	}

}
