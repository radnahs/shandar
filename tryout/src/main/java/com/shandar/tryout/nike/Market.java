/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.nike </p>
 * <p>File Name: Market.java </p>
 * <p>Create Date: 30-Apr-2021 </p>
 * <p>Create Time: 4:18:24 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.nike;

import java.util.Arrays;

/**
 * @author : Shantanu Sikdar
 *
 *         Today is Sunday and Disha is out for the shopping. She has reached to
 *         the famous market of the city. She has a list of items which she
 *         wants to buy but she has a very low amount of cash today and no shop
 *         accepts any online payment method in the market. Disha has only D
 *         rupees with her and wants to buy N items (she wants to buy as many
 *         units of item as possible). She wants to shop in a way such that
 *         first she buy 1 quantity of all the items she wants to buy and then
 *         she will move to buy 2nd quantity of any item.
 * 
 *         Can you help her by calculating how many items she can buy today if
 *         she shops optimally?
 * 
 *         You are given an array which represents the price of items Disha
 *         wants to buy. Example :- If she wants to buy 4 items having price 4,
 *         6, 2, 5 respectively and has 49 rupees only then she will buy in this
 *         order:- 1st item, 2nd item, 3rd item, 4th item (It will cost 17
 *         rupees). Again she will buy in this order 1st, 2nd, 3rd, 4th (it will
 *         cost 17 rupees). Now, she has remaining 15 rupees so she can buy any
 *         3 items.
 * 
 * 
 *         Input Format The first line of input consists of two space-separated
 *         integers, N (number of items Disha wants to buy) and D (amount of
 *         cash Disha has with her). The second line of input consists of N
 *         space-separated integers which represents the price of the N items
 *         Disha wants to buy.
 * 
 * 
 *         Constraints 1<= N <=1e9 1<= Ni <=10^15 (1e15) 1<= D <=10^18 (1e18)
 * 
 * 
 *         Output Format Print the count of items which Disha can buy.
 * 
 *         Sample TestCase 1 Input 5 24 4 8 9 7 2 Output 4
 */
public class Market {

	public static void main(String[] args) {
		/*String[] line1 = { "5", "73" };
		String[] line2 = { "4", "8", "9", "7", "2" };*/
		
		String[] line1 = { "4", "49" };
		String[] line2 = { "4", "6", "5", "2" };
		int numElem = Integer.parseInt(line1[0]);
		Integer[] arr = new Integer[numElem];
		
		//Integer[] arr = new Integer[line2.length];
		Integer sum = 0;
		for (int i = 0; i < line2.length; i++) {
			arr[i] = Integer.parseInt(line2[i]);
			sum += arr[i];
		}
		int tot = Integer.parseInt(line1[1]);
		Arrays.sort(arr);
		int count = 0;
		if (tot > sum) {
			int tottemp = tot % sum;
			count = tot / sum * numElem;
			int tempsum = 0;
			for (int i = 0; i < arr.length; i++) {
				if (tottemp >= tempsum) {
					tempsum += arr[i];
					if(tottemp >= tempsum) {
						count++;
					}
				}/* else {
					tempsum = tempsum - arr[i - 1];
					count--;
					
					break;
				}*/
			}
		} else {
			count = 0;
			int tempsum = 0;
			for (int i = 0; i < arr.length; i++) {
				if (tempsum <= tot) {
					tempsum += arr[i];
					if (tempsum <= tot) {
						count++;
					}
				}
			}
		}
		System.out.println(count);
	}

}
