/**
 *
 * <p>Project: tryout_trunk </p>
 * <p>Package Name: com.webtual.tryout.utils.tests.ptc </p>
 * <p>File Name: Child.java</p>
 * <p>Create Date: Jun 22, 2015 </p>
 * <p>Create Time: 2:50:47 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.tryout.ptc;

/**
 * @author Shantanu Sikdar 
 *
 */
public class Child  extends Base{
	static{
		System.out.println("static blox Child ||");
	}

	public Child(){
		System.out.println("Constructor Child ||");
	}

}
