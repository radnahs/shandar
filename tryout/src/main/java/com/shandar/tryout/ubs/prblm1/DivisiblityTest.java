/**
 *
 * <p>Project: tryout_trunk </p>
 * <p>Package Name: com.shandar.tryout.prblm1 </p>
 * <p>File Name: DivisiblityTest.java</p>
 * <p>Create Date: Feb 22, 2016 </p>
 * <p>Create Time: 6:47:11 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.tryout.prblm1;

/**
 * @author Shantanu Sikdar
 * @Desc Problem by Raju Mishra
 */
public class DivisiblityTest {

	/**
	 * 16:2,2,2,2; 1,3,2,2,1
	 * 
	 * @param args
	 */
	public static void main1(String[] args) {
		int n = 10;
		int count = 0;
		for (int i = 0; n > 1; i++) {
			if (n % 2 == 0) {
				n /= 2;
				count++;
			} else if (n % 3 == 0) {
				n /= 3;
				count++;
			} else {
				n -= 1;
				count++;
			}
		}
		System.out.println(count);
	}

	public static void main(String[] args) {
		DivisiblityTest dt = new DivisiblityTest();
		System.out.println(dt.bottomUp(10));
		System.out.println(dt.bottomUp(19));
		System.out.println(dt.bottomUp(18));
	}

	public int bottomUp(int n) {
		int[] btmUp = new int[n + 1];
		btmUp[0] = 0;
		btmUp[1] = 0;
		for (int i = 2; i <= n; i++) {
			int r = 1 + btmUp[i - 1];
			if (i % 2 == 0)
				r = Math.min(r, 1 + btmUp[i / 2]);
			if (i % 3 == 0)
				r = Math.min(r, 1 + btmUp[i / 3]);
			btmUp[i] = r;
		}
		return btmUp[n];
	}

}
