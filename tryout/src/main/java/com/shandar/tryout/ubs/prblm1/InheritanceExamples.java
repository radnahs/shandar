/**
* Project: tryout
* Package Name:package com.shandar.tryout.prblm1;
* File Name: InheritanceExamples.java
* Create Date: Sep 20, 2016
* Create Time: 4:33:55 PM
* Copyright: Copyright (c) 2016
* @author: Shantanu Sikdar, ssikdar
* @version 1.0
*/
package com.shandar.tryout.prblm1;

/**
 * @author ssikdar
 *
 */
public class InheritanceExamples {
	
	public static void main(String[] args) {
		Animal an = new Animal();
		Horse hrse = new Horse();
		Animal ah = new Horse();
		
		an.doStuffWP(an);
		hrse.doStuffWP(hrse);
		ah.doStuffWP(ah);//animal
		/*ah.doStuffWP(hrse);//horse
		ah.doStuffWoP();//horse
		ah.doStuffWPO(hrse);//horse
		ah.doStuffWPO(an);//horse
*/	}
}
class Animal{
	public void doStuffWP(Animal an){
		System.out.println("With Param Animal Called");
	}
	/*public void doStuffWPO(Animal an){
		System.out.println("With Param Overload Animal Called");
	}
	public void doStuffWPO(Horse an){
		System.out.println("With Horse Param Overload Animal Called");
	}
	public void doStuffWoP(){
		System.out.println("Without Param Animal Called");
	}*/
}

class Horse extends Animal{
	public void doStuffWP(Horse hrse){
		System.out.println("With Param Horse Called");
	}
/*	public void doStuffWPO(Animal an){
		System.out.println("With Animal Param overlaod Horse Called");
	}
	public void doStuffWPO(Horse hrse){
		System.out.println("With Horse Param overlaod Horse Called");
	}
	public void doStuffWoP(){
		System.out.println("Without Param Horse Called");
	}
*/}