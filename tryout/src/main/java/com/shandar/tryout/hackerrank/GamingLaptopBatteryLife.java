/**
 * Project: tryout 
 * Package Name: com.shandar.tryout.hackerrank 
 * File Name: GamingLaptopBatteryLife.java 
 * Create Date: 01-Jun-2020 
 * Create Time: 3:13:10 pm 
 * Copyright: Copyright (c) 2016
 * Company:  
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.hackerrank;

import java.util.Arrays;
import java.util.List;

/**
 * @author : Shantanu Sikdar
 *
 *         2. Gaming Laptop Battery Life You bought a new gaming laptop and like
 *         playing a game on it all day. It requires frequent charging due to
 *         high graphics usage. You want to know how much battery will be left
 *         after a number of playing and charging events. Write a function that
 *         receives n records of the laptop's battery consumption and charging
 *         events. An events[i] value represents the number of minutes spent
 *         charging the laptop (positive value) or playing a game (negative
 *         value). Every minute, the laptop charges 1% or loses 1% of its
 *         energy. The battery's charge cannot go over 100% . Return the
 *         laptop's final charge percentage given that the initial charge is 50%
 *         .
 * 
 *         Example n = 4 events = [10, -20, 61, -15]
 * 
 *         Initially the laptop is charged to 50%. It is plugged in for
 *         events[0] = 10 minutes and is charged to 50 + 10 = 60. A game is
 *         played for 20 minutes at events[1] = -20 bringing the charge to 40.
 *         Charging for another 61 minutes stops when the charge reaches 100 ,
 *         then 15 minutes of play results in a final charge of 85.
 * 
 *         Function Description Complete the function getBattery in the editor
 *         below. The function must return an integer. getBattery has the
 *         following parameter: events[events[0],...events[n-1]]: an array of
 *         integers Constraints 1 ≤ n ≤ 10^5 -100 events[i] ≤ 100 The battery
 *         charge never goes negative.
 * 
 *         <!-- <StartOfInputFormat> DO NOT REMOVE THIS LINE-->
 * 
 *         Input Format For Custom Testing

 *         The first line contains an integer,
 *         n , denoting the number of elements in events .
 *         Each i line of the n subsequent lines (where 0 ≤ i &lt; n ) contains
 *         an integer describing events[i] .
 * 
 *         <!-- </StartOfInputFormat> DO NOT REMOVE THIS LINE-->
 *         Sample Case 0
 * 
 *         Sample Input For Custom Testing
 * 			4
 *			5
 *			-30
 *			70
 *			s-10
 * 
 *         Sample Output
 *         90
 *         
 *         Explanation
 *         n = 4
 *         events = [25, -30, 70, 10]
 *         The battery starts at charge = 50% .
 *         It is charged for 25 minutes, charge = 75% .
 *         Then it is used for 30 minutes, charge = 45% .
 *         After that, it is charged for 70 minutes, charge = 100% since it
 *         cannot charge over that.
 *         Finally, it is used for 10 minutes, charge = 90% .

 *         Sample Case 1 
 *         Sample Input For Custom Testing 3 -10 60 10
 *         Sample Output
 *         100
 * 
 *         Explanation
 *         n = 3
 *         events = [-10, 60, 10]
 *         The battery starts at 50%.
 *         It is used for 10 minutes, charge = 40% .
 *         Then it is charged for 60 minutes, charge = 100% .
 *         Finally, it is used for 10 minutes, charge = 100%.
 */
public class GamingLaptopBatteryLife {

	public static void main(String[] args) {
		List<Integer> events = Arrays.asList(25, -30, 70, -10);
		getBattery(events);
	}

	public static int getBattery(List<Integer> events) {
		// Write your code here
		Integer ret = 50;
		for (Integer evnt : events) {
			ret += evnt;
			if (ret > 100) {
				ret = 100;
			}
		}
		return ret;
	}

}
