/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.hackerrank </p>
 * <p>File Name: UniversityGradingRounding.java </p>
 * <p>Create Date: 29-Nov-2021 </p>
 * <p>Create Time: 5:49:57 PM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.hackerrank;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author : Shantanu Sikdar
 *         https://www.hackerrank.com/challenges/grading/problem
 */
public class UniversityGradingRounding {

	public static List<Integer> gradingStudents(List<Integer> grades) {
		// Write your code here
		List<Integer> newMarks = new ArrayList<Integer>();
		for (Integer grade : grades) {
			if (grade > 37) {
				if (grade % 5 > 2) {
					grade = grade + 5 - grade % 5;
					newMarks.add(grade);
				} else {
					newMarks.add(grade);
				}
			} else {
				newMarks.add(grade);
			}
		}
		return newMarks;

	}

	public static void main(String[] args) {
		List<Integer> grades = Arrays.asList(74, 67, 38, 33);
		System.out.println(gradingStudents(grades));
	}
}
