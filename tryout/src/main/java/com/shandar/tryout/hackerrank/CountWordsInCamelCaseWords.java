/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.hackerrank </p>
 * <p>File Name: CamelCaseWords.java </p>
 * <p>Create Date: 05-Apr-2021 </p>
 * <p>Create Time: 8:40:16 am </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.hackerrank;

/**
 * @author : Shantanu Sikdar
 *
 */
public class CountWordsInCamelCaseWords {

	public static void main(String[] args) {
		System.out.println(camelWords("saveChangesInTheEditor"));
	}

	private static int camelWords(String input) {
		String output = "";
		char chr1 = input.charAt(0);
		output = output + Character.toLowerCase(chr1);
		for (int i = 1; i < input.length(); i++) {
			char chr = input.charAt(i);
			if (Character.isUpperCase(chr)) {
				output = output + '#';
				output = output + chr;//Character.toLowerCase(chr);
			} else {
				output = output + chr;
			}

		}
		//System.out.println(output);
		//System.out.println(output.split("#").length);
		return output.split("#").length;
	}

}
