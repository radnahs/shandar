/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.hackerrank </p>
 * <p>File Name: SockMerchantUnPairCount.java </p>
 * <p>Create Date: 28-Nov-2021 </p>
 * <p>Create Time: 8:36:25 PM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.hackerrank;

import java.util.Arrays;
import java.util.List;

/**
 * @author : Shantanu Sikdar
 * https://www.hackerrank.com/challenges/sock-merchant/problem
 */
public class SockMerchantUnPairCount {

	public static int sockMerchant(int n, List<Integer> ar) {
		// Write your code here
		int count = 0;
		for (int i = 0; i < ar.size() - 1; i++) {
			if (ar.get(i) != -1) {
				for (int j = i + 1; j < ar.size(); j++) {
					if (ar.get(j).equals(ar.get(i))) {
						ar.set(j, -1);
						ar.set(i, -1);
						count = count + 1;
						break;
					}
				}
			}
		}
		System.out.println(ar.size());
		System.out.println(count);
		return count;
	}

	public static void main(String[] args) {
		List<Integer> ar = Arrays.asList(10, 20, 20, 10, 10, 30, 50, 10, 20);
		sockMerchant(ar.size(), ar);
		List<Integer> ar1 = Arrays.asList(10, 20, 20, 10, 10, 30, 50, 10, 20);
		sockMerchant(ar1.size(), ar1);
	}

}
