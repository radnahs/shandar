/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.hackerrank </p>
 * <p>File Name: CountScorecards.java </p>
 * <p>Create Date: 30-Jun-2020 </p>
 * <p>Create Time: 12:36:00 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.hackerrank;

/**
 * @author : Shantanu Sikdar
 *
 */
public class CountScorecards {

	public static void main(String[] args) {
		permute("102", 0, 2);
	}

	private static void permute(String str, int l, int r) {
		if (l == r) {
			System.out.println(str);
		} else {
			for (int i = l; i <= r; i++) {
				str = swap(str, l, i);
				permute(str, l + 1, r);
				str = swap(str, l, i);
			}
		}

	}

	public static String swap(String a, int i, int j) {
		char temp;
		char[] charArray = a.toCharArray();
		temp = charArray[i];
		charArray[i] = charArray[j];
		charArray[j] = temp;
		return String.valueOf(charArray);
	}
}
