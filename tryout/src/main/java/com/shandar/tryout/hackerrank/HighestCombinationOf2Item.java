/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.hackerrank </p>
 * <p>File Name: HighestCombinationOf2Item.java </p>
 * <p>Create Date: 28-Nov-2021 </p>
 * <p>Create Time: 1:06:52 PM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.hackerrank;

/**
 * @author : Shantanu Sikdar
 *
 * 			https://www.hackerrank.com/challenges/electronics-shop
 * 
 *         A person wants to determine the most expensive computer keyboard and
 *         USB drive that can be purchased with a give budget. Given price lists
 *         for keyboards and USB drives and a budget, find the cost to buy them.
 *         If it is not possible to buy both items, return .
 * 
 *         Example
 * 
 *         The person can buy a , or a . Choose the latter as the more expensive
 *         option and return .
 * 
 *         Function Description
 * 
 *         Complete the getMoneySpent function in the editor below.
 * 
 *         getMoneySpent has the following parameter(s):
 * 
 *         int keyboards[n]: the keyboard prices int drives[m]: the drive prices
 *         int b: the budget Returns
 * 
 *         int: the maximum that can be spent, or if it is not possible to buy
 *         both items Input Format
 * 
 *         The first line contains three space-separated integers , , and , the
 *         budget, the number of keyboard models and the number of USB drive
 *         models. The second line contains space-separated integers , the
 *         prices of each keyboard model. The third line contains
 *         space-separated integers , the prices of the USB drives.
 * 
 *         Constraints
 *         The price of each item is in the inclusive range . 
 *         
 *         Sample Input 0:
 *         10 2 3 
 *         3 1 5 
 *         2 8 
 *         
 *         Sample Output 0
 *         9 
 *         
 *         Explanation 0
 *         Buy the keyboard and the USB drive for a total cost of .
 * 
 *         Sample Input 1
 *         5 1 1 4 5 Sample Output 1
 * 
 *         -1 Explanation 1
 * 
 *         There is no way to buy one keyboard and one USB drive because , so
 *         return .
 * 
 *         WelcomeF
 */
public class HighestCombinationOf2Item {

	static int getMoneySpent(int[] keyboards, int[] drives, int b) {
		/*
		 * Write your code here.
		 */
		int finalCost = -1;
		for (int i = 0; i < keyboards.length; i++) {
			for (int j = 0; j < drives.length; j++) {
				if (keyboards[i] + drives[j] <= b) {
					if (finalCost < keyboards[i] + drives[j]) {
						finalCost = keyboards[i] + drives[j];
					}
				}
			}
		}
		return finalCost;
	}

	public static void main(String[] args) {
		int[] keyboards = new int[] { 3, 1, 5 };
		int[] drives = new int[] { 2, 8 };
		int b=10;
		System.out.println(getMoneySpent(keyboards, drives, b));
	
	}

}
