/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.hackerrank </p>
 * <p>File Name: Handshake.java </p>
 * <p>Create Date: 28-Nov-2022 </p>
 * <p>Create Time: 12:16:05 PM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.hackerrank;

/**
 * @author : Shantanu Sikdar
 *
 *         https://www.hackerrank.com/challenges/handshake/problem
 */
public class Handshake {

	public static void main(String[] args) {
		System.out.println(combinationMathematically(5));
		System.out.println(combinationMathematically(4));
	}

	// Combination formula mathematically : !n /{!r !(n-r)}
	private static int combinationMathematically(int n) {
		if (n == 1) {
			return 0;
		}
		int r = 2;
		int numerator = 1, denominator = 1;
		if (n - r > r) {
			for (int i = n - r + 1; i <= n; i++) {
				numerator = numerator * i;
			}
			for (int i = 1; i <= r; i++) {
				denominator = denominator * i;
			}
		} else {
			for (int i = r + 1; i <= n; i++) {
				numerator = numerator * i;
			}
			for (int i = 1; i <= n - r; i++) {
				denominator = denominator * i;
			}
		}
		int combination = numerator / denominator;

		return combination;
	}

}
