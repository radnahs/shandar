/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.hackerrank </p>
 * <p>File Name: MiniMaxSum.java </p>
 * <p>Create Date: 02-Jul-2020 </p>
 * <p>Create Time: 9:12:32 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.hackerrank;

import java.util.Arrays;

/**
 * @author : Shantanu Sikdar
 *
 * Given five positive integers, find the minimum and maximum values that can be 
 * calculated by summing exactly four of the five integers. Then print the respective 
 * minimum and maximum values as a single line of two space-separated long integers.
 * 
 * For example, . Our minimum sum is  and our maximum sum is . We would print
 * 
 * 16 24
 * Function Description
 * 
 * Complete the miniMaxSum function in the editor below. It should print two 
 * space-separated integers on one line: the minimum sum and the maximum sum of 
 * elements.
 * 
 * miniMaxSum has the following parameter(s):
 * 
 * arr: an array of  integers
 * Input Format
 * 
 * A single line of five space-separated integers.
 * 
 * Constraints
 * 
 * 
 * Output Format
 * 
 * Print two space-separated long integers denoting the respective minimum and maximum 
 * values that can be calculated by summing exactly four of the five integers. (The output 
 * can be greater than a 32 bit integer.)
 * 
 * Sample Input
 * 
 * 1 2 3 4 5
 * Sample Output
 * 
 * 10 14
 * Explanation
 * 
 * Our initial numbers are , , , , and . We can calculate the following sums using four 
 * of the five integers:
 * 
 * If we sum everything except , our sum is .
 * If we sum everything except , our sum is .
 * If we sum everything except , our sum is .
 * If we sum everything except , our sum is .
 * If we sum everything except , our sum is .
 * Hints: Beware of integer overflow! Use 64-bit Integer.
 * 
 * 
 */

public class MiniMaxSum {

	public static void main(String[] args) {
		//miniMaxSum(new int[]{2, 1, 3, 4, 5});
		miniMaxSum(new int[]{769082435, 210437958, 673982045, 375809214, 380564127});
	}

	static void miniMaxSum(int[] arr) {
		display(arr);
		Arrays.sort(arr);
		display(arr);
        Long sumMin=0l;
        Long sumMax = 0l;
        for(int i = 0; i<arr.length;i++){
            if(i<arr.length-1){
            	sumMin+=arr[i];
            }
            if(i>0){
            	sumMax+=arr[i];
            }
        }
        System.out.println(sumMin+" "+sumMax);

    }

	private static void display(int[] arr){
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i]+" ");
		}
		System.out.println("");
	}
	
}
