/**
 *  Project: tryout 
 *  Package Name: com.shandar.tryout.hackerrank 
 *  File Name: SortedSums.java 
 *  Create Date: 05-Apr-2021 
 *  Create Time: 10:05:35 am 
 *  Copyright: Copyright (c) 2016
 *  Company:  
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.hackerrank;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author : Shantanu Sikdar
 *
 *         1. Sorted Sums
 * 
 *         Alex has asked Madison out on a date. But Madison wants to date only
 *         intelligent people. So, to test Alex's intelligence, Madison gave
 *         Alex a puzzle. Madison gives Alex a sequence of n integers collection
 *         1 ,collection 2 ,....collection n and asks the value of
 *         f(1)+f(2)+....f(n).
 * 
 *         f(i) is defined as follows:
 * 
 *         Consider the sub-sequence collection 1 ,collection 2 ,....collection
 *         i sorted in non-descending order. Now for this sorted sub-sequence,
 *         f(i) = 1*collection 1 + 2*collection 2 + .... i*collection i .
 * 
 *         For example if the sequence given by Madison to Alex is [4,3,2,1]:
 * 
 *         (1) = 1*collection 1 = <strong>4</strong> (2) = 1*collection 1 +
 *         2*collection 2 = 1*3 + 2*4 = 3 + 8 = <strong>11</strong> ( because
 *         the sorted sub-sequence is [3,4] ) (3) = 1*collection 1 +
 *         2*collection 2 + 3*collection 3 = 1*2 + 2*3 +3*4 = 2 + 6 + 12 =
 *         <strong>20</strong> ( because the sorted sub-sequence is [2,3,4] )
 *         (4) = 1*collection 1 + 2*collection 2 + 3*collection 3 + 4*collection
 *         4 = 1*1 + 2*2 + 3*3 + 4*4 = 1 + 4 + 9 + 16 = <strong>30</strong> (
 *         because the sorted sub-sequence is [1,2,3,4] ) (1)+f(2)+....f(n) =
 *         f(1)+f(2)+....f(4) = 4 + 11 + 20 + 30 = <strong>65</strong>, hence
 *         the answer is 65.
 * 
 *         Alex finds this puzzle difficult so they ask for your help.
 * 
 *         Note that this value may be large so your function should return the
 *         answer modulo <strong>109 + 7.</strong> For a sequence of
 *         integers [a 1 ,a 2 , ... a n ], define the function f(i) as follows:
 * 
 *         Take the first i elements of a (a 1 , a 2 , ... a i ) and sort them
 *         in non-descending order. Call this new sequence s i . Let f(i) = 1*s
 *         1 + 2*s 2 + ... i*s i
 * 
 *         Given a sequence of n integers, sort them in non-descending order
 *         then compute f(1)+f(2)+f(3)+... f(n). As the result may be very
 *         large, return it modulo<strong> </strong>(109 + 7).
 * 
 *         Example n = 4 a = [ 4, 3, 2, 1]
 * 
 *         s 1 = [4], f(1) = 1*4 = 4 s 2 = [3,4], f(2) = 1*3+2*4 = 11 s 3 =
 *         [2,3,4], f(3) = 1*2+2*3+3*4 = 20 s 4 = [1,2,3,4], f(4) =
 *         1*1+2*2+3*3+4*4 = 30
 * 
 *         f(1)+f(2)+..f(n) = 4+11+20+30 = 65 and 65 modulo (109 + 7)
 *         = 65
 * 
 *         Function Description Complete the function sortedSum in the editor
 *         below. The function should return the value of f(1)+f(2)+....f(n)
 *         modulo (109 + 7). sortedSum has the following
 *         parameter(s): a: a sequence of integers
 * 
 *         Constraints
 * 
 *         1 ≤ n ≤ 105 1 ≤ a[i] ≤ 106
 * 
 *         <!-- <StartOfInputFormat> DO NOT REMOVE THIS LINE-->
 * 
 *         Input Format For Custom Testing
 * 
 *         The first line contains an integer, n, denoting the number of
 *         elements in a.<br>
 *         Each line i of the n subsequent lines (where 1 ≤ i ≤ n) contains an
 *         integer describing a[i].
 * 
 *         <!-- </StartOfInputFormat> DO NOT REMOVE THIS LINE-->
 * 
 *         Sample Case 0
 * 
 *         Sample Input For Custom Testing 3 9 5 8
 * 
 *         Sample Output 80
 * 
 *         Explanation n = 3
 * 
 *         a = [9, 5, 8]
 * 
 *         s 1 = [9]; f(1) = 1*9 = 9 s 2 = [5,9]; f(2) = 1*5 + 2*9 = 23 s 3 =
 *         [5,8,9]; f(3) = 1*5 + 2*8 +3*9 = 48 f(1)+f(2)+....f(n) =
 *         f(1)+f(2)+f(3) = 9 + 23 + 48 = 80
 * 
 *         80 modulo (109 + 7) = 80
 * 
 *         Sample Case 1
 * 
 *         Sample Input For Custom Testing 2 5 9
 * 
 *         Sample Output 28 Explanation n = 2 a = [5, 9] f(1) = 1*5 = 5 f(2) =
 *         1*5+2*9 = 23 f(1)+f(2)+....f(n) = f(1)+f(2) = 5 + 23 = 28
 * 
 *         28 modulo (109 + 7) = 28
 */
public class SortedSums {

	public static void main(String[] args) {
		// sortedSum(Arrays.asList(5, 9));
		sortedSum(Arrays.asList(9, 5, 8));
	}

	public static int sortedSum(List<Integer> a) {
		// Write your code here
		int sum = 0;
		for (int i = 1; i <= a.size(); i++) {
			List<Integer> aNew = a.subList(0, i);
			Collections.sort(aNew);
			int tempSum = 0;
			for (int j = 0; j < aNew.size(); j++) {
				if (j < i) {
					tempSum = tempSum + aNew.get(j) * (j + 1);
				} else {
					break;
				}
			}
			System.out.println(tempSum);
			sum = sum + tempSum;
		}
		System.out.println(sum);
		return sum;
	}
}
