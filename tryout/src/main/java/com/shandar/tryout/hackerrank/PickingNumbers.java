/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.hackerrank </p>
 * <p>File Name: PickingNumbers.java </p>
 * <p>Create Date: 25-Nov-2021 </p>
 * <p>Create Time: 3:50:13 PM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.hackerrank;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * @author : Shantanu Sikdar
 *         https://www.hackerrank.com/challenges/picking-numbers/problem
 */
public class PickingNumbers {
	
	public static int pickingNumbers(List<Integer> a) {
		Collections.sort(a);
		int count = 0;
		List<List<Integer>> listIntgrIntgr = new ArrayList<List<Integer>>();
		for (int i = 0; i < a.size() - 1; i++) {
			List<Integer> listIntgr = new ArrayList<Integer>();
			listIntgr.add(a.get(i));
			
			for (int j = i + 1; j < a.size(); j++) {
				if (Math.abs(a.get(i) - a.get(j)) < 2) {
					listIntgr.add(a.get(j));
				}
			}
			if (count <= listIntgr.size()) {
				count = listIntgr.size();
				listIntgrIntgr.add(listIntgr);
			}
		}
		System.out.println(listIntgrIntgr);
		System.out.println(count);
		return count;
	}

	public static void main(String[] args) {
		List<Integer> lstIntgr = Arrays.asList(1, 1, 2, 2, 4, 4, 5, 5, 5);
		pickingNumbers(lstIntgr);// 5
		List<Integer> lstIntgr1 = Arrays.asList(4, 6, 5, 3, 3, 1);
		pickingNumbers(lstIntgr1);// 3
		List<Integer> lstIntgr2 = Arrays.asList(1, 2, 2, 3, 1, 2);
		pickingNumbers(lstIntgr2);// 5
	}

}
