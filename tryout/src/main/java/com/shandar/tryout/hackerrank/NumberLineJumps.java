/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.hackerrank </p>
 * <p>File Name: NumberLineJumps.java </p>
 * <p>Create Date: 01-Apr-2021 </p>
 * <p>Create Time: 6:08:38 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.hackerrank;

/**
 * @author : Shantanu Sikdar
 *
 */
public class NumberLineJumps {

	public static void main(String[] args) {
		System.out.println(kangaroo(0,2,5,3));
		System.out.println(kangaroo(0,3,4,2));
		System.out.println(kangaroo(1,2,0,3));
		System.out.println(kangaroo(1,1,1,1));
		System.out.println(kangaroo(23,9867,9814,5861));//expected answer is NO
		
	}

	static String kangaroo(int x1, int v1, int x2, int v2) {
        if( (x1<=x2 && v1<v2) || (x1>=x2 && v1>v2 ) || (x1<x2 && v1<=v2) || (x1>x2 && v1>=v2 )){
            return "NO";
        }else {
        	return "YES";
        }

    }

}
