/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.xoriant </p>
 * <p>File Name: ConvertCamelCaseToSnakeCase.java </p>
 * <p>Create Date: 22-Mar-2021 </p>
 * <p>Create Time: 9:58:20 am </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.xoriant;

/**
 * @author : Shantanu Sikdar
 * Ref:https://www.geeksforgeeks.org/convert-camel-case-string-to-snake-case-in-java/
 * 
 */
public class ConvertCamelCaseToSnakeCase {

	public static void main(String[] args) {
		ConvertCamelCaseToSnakeCase ccctsc = new ConvertCamelCaseToSnakeCase();
		System.out.println(ccctsc.camelToSnake("CamelCaseToSnakeCase"));
		System.out.println(ccctsc.camelToSnake("camelCaseToSnakeCase"));
		
	}

	private String camelToSnake(String input) {
		String output = "";
		char chr1 = input.charAt(0);
		output = output + Character.toLowerCase(chr1);
		for (int i = 1; i < input.length(); i++) {
			char chr = input.charAt(i);
			if (Character.isUpperCase(chr)) {
				output = output + '_';
				output = output + Character.toLowerCase(chr);
			} else {
				output = output + chr;
			}

		}
		return output;
	}

}
