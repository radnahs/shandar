/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.xoriant </p>
 * <p>File Name: SplitBySpaceNotByDoubleQuotes.java </p>
 * <p>Create Date: 20-Mar-2021 </p>
 * <p>Create Time: 6:21:11 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.xoriant;

/**
 * @author : Shantanu Sikdar
 *	Ref : https://stackoverflow.com/questions/25477562/split-java-string-by-space-and-not-by-double-quotation-that-includes-space
 *
 */
public class SplitBySpaceNotByDoubleQuotes {

	public static void main(String[] args) {

		String str = "shantanu sikdar \"is a\" java developer";

		boolean quoted = false;
		String quotes = "\"";
		for (String q : str.split(quotes)) {
			if (quoted) {
				System.out.println(quotes + q.trim() + quotes);
			} else {
				for (String s : q.split(" ")) {
					if (!s.trim().isEmpty()) {
						System.out.println(s.trim());
					}
				}
			}
			quoted = !quoted;
		}
	}
}
