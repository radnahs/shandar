/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.xoriant </p>
 * <p>File Name: XoriantProblem.java </p>
 * <p>Create Date: 18-Aug-2020 </p>
 * <p>Create Time: 3:56:56 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.xoriant;

import java.util.Scanner;

/**
 * @author : Shantanu Sikdar
 *
 */
public class XoriantProblem {
	
	public static void main(String[] args) {
		XoriantProblem xp = new XoriantProblem();
		xp.solution();
	}

	private void solution() {
		Scanner scan = new Scanner("HEllo");
		String str = scan.nextLine();
		System.out.println(str);
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < str.length(); i++) {
			sb.append( (str.charAt(i)) == ' ' ? "": str.charAt(i) );
		}
		System.out.println(sb.toString());
		
	}
}
