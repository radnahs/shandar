/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.fractalenterprises </p>
 * <p>File Name: ShortestPathFromPrimeToPrime.java </p>
 * <p>Create Date: 22-Apr-2020 </p>
 * <p>Create Time: 6:12:02 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.fractalenterprises;

/**
 * 
 * Given two four digit prime numbers, suppose 1033 and 8179, we need to find 
 * the shortest path from 1033 to 8179 by altering only single digit at a time 
 * such that every number that we get after changing a digit is prime. 
 * For example a solution is 1033, 1733, 3733, 3739, 3779, 8779, 8179
 * 
 * Examples:
 * Input : 1033 8179
 * Output :6
 * 
 * Input : 1373 8017
 * Output : 7
 * 
 * Input : 1033 1033
 * Output : 0
 * 
 * 
 * @author : Shantanu Sikdar
 *
 * https://www.geeksforgeeks.org/shortest-path-reach-one-prime-changing-single-digit-time/
 * 
 * solution : at https://matoski.com/article/coding-problem-primepath/
 */
public class ShortestPathFromPrimeToPrime {

	public static void main(String[] args) {
		
	}

}
