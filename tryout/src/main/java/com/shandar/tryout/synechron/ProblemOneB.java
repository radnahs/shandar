/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.synechron </p>
 * <p>File Name: ProblemOneB.java </p>
 * <p>Create Date: 01-Jul-2020 </p>
 * <p>Create Time: 7:36:13 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.synechron;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author : Shantanu Sikdar
 *
 */
public class ProblemOneB {

	public static void main(String[] args) {

		/*
		 * String effectiveFinal = "I am non final local variable"; Runnable r1 = () ->
		 * { System.out.println("Value of effectively variable is : " + effectiveFinal);
		 * };
		 * 
		 * Thread t1 = new Thread(r1); t1.start();
		 */

		String nonFinal = "I am non final local variable";
		Runnable r2 = new Runnable() {

			@Override
			public void run() {
				System.out.println("Using non-final local variable inside anonymous class in Java");
				System.out.println("Value of non-final variable is : " + nonFinal);
			}
		};

		Thread t2 = new Thread(r2);
		t2.start();

	}

	// Will give compile time error, prior to java 8. at line no. 34
	private static void usingAnonymousClass() {

		List<Integer> list = Arrays.asList(3,56,1,5,7);
		Integer iTest = 100;
		
		Collections.sort(list, new Comparator<Integer>() {

			@Override
			public int compare(Integer o1, Integer o2) {
				if(iTest>0) {
					System.out.println("Below Java 8 will give compile time error ");
				}
				return 0;
			}
		});
	}

}
