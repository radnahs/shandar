/**
 * Project: tryout 
 * Package Name: com.shandar.tryout.synechron.codility 
 * File Name: ProblemB.java 
 * Create Date: 17-Jul-2020 
 * Create Time: 12:12:38 pm 
 * Copyright: Copyright (c) 2016
 * Company:  
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.synechron.codility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

/**
 * @author : Shantanu Sikdar
 * 
 * Write a function 
 * class Solution { 
 * 	public int solution(String S);
 * }
 *  that, given a string S consisting of N lowercase
 * letters, returns the minimum number of letters that must be deleted
 * to obtain a word in which every letter occurs a unique number of
 * times. We only care about occurrences of letters that appear at least
 * once in result. 
 * Examples: 
 * 1. Given S = "aaaabbbb", the
 * function should return 1. 
 * We can delete one occurrence of a
 * or one occurrence of b. Then one letter will occur
 * four times and the other one three times. 
 * 2. Given S ="ccaaffddecee", the 
 * function should return 6. 
 * For example, we can delete all occurrences of e and
 * f and one occurrence of
 * d to obtain the word "ccaadc".
 * Note that both e and
 * f will occur zero times in the
 * new word, but that is fine, since we only care about letters that
 * appear at least once. 3. Given S = "eeee", the function should return
 * 0 (there is no need to delete any characters). 4. Given S =
 * "example", the function should return 4. Write an
 * efficient algorithm for the following assumptions:

 * N is an integer within the range
 * [0...300,000];
 * string S consists only of lowercase letters
 * (a−z).
 *         
 */
public class DistinctAlphabetOccurence {

	public static void main(String[] args) {
		System.out.println(solution("aaaabbbb")); // 1
		System.out.println(solution("ccaaffddecee"));
		System.out.println(solution("aaaa"));
	}

	public static int solution(String S) {
		char[] chrArr = S.toCharArray();
		System.out.println("chrArr size  = " + chrArr.length);
		Map<Character, Integer> mpCnt = new HashMap<Character, Integer>();
		for (int i = 0; i < chrArr.length; i++) {
			if (mpCnt.containsKey(chrArr[i])) {
				Integer intgr = mpCnt.get(chrArr[i]);
				intgr++;
				mpCnt.put(chrArr[i], intgr);
			} else {
				mpCnt.put(chrArr[i], 1);
			}
		}
		System.out.println(mpCnt);
		Map<Integer, List<Character>> mpSame = new TreeMap<Integer, List<Character>>();

		for (Entry<Character, Integer> mpSet : mpCnt.entrySet()) {
			if (mpSame.containsKey(mpSet.getValue())) {
				List<Character> chrLst = mpSame.get(mpSet.getValue());
				chrLst.add(mpSet.getKey());
				mpSame.put(mpSet.getValue(), chrLst);
			} else {
				List<Character> chrLst = new ArrayList<Character>();
				chrLst.add(mpSet.getKey());
				mpSame.put(mpSet.getValue(), chrLst);
			}

		}
		System.out.println(mpSame);
		Integer temp = 0;
		Integer returnVal = 0;
		for (Entry<Integer, List<Character>> mpVal : mpSame.entrySet()) {
			returnVal += (mpVal.getKey() - temp);
			temp = mpVal.getKey();
		}
		System.out.println(chrArr.length - returnVal);
		return returnVal;
	}

}
