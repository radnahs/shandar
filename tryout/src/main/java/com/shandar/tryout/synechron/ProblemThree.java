/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.synechron </p>
 * <p>File Name: ProblemThree.java </p>
 * <p>Create Date: 01-Jul-2020 </p>
 * <p>Create Time: 9:21:49 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.synechron;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * @author : Shantanu Sikdar
 *
 */

public class ProblemThree {

	// Method to find page faults using indexes
	static int pageFaults(int pages[], int n, int capacity) {
		// To represent set of current pages. We use
		// an unordered_set so that we quickly check
		// if a page is present in set or not
		Set<Integer> s = new HashSet<>(capacity);

		// To store least recently used indexes
		// of pages.
		Map<Integer, Integer> indexes = new HashMap<>();

		// Start from initial page
		int page_faults = 0;
		for (int i = 0; i < n; i++) {
			// Check if the set can hold more pages
			if (s.size() < capacity) {
				// Insert it into set if not present
				// already which represents page fault
				if (!s.contains(pages[i])) {
					s.add(pages[i]);

					// increment page fault
					page_faults++;
				}

				// Store the recently used index of
				// each page
				indexes.put(pages[i], i);
			}

			// If the set is full then need to perform lru
			// i.e. remove the least recently used page
			// and insert the current page
			else {
				// Check if current page is not already
				// present in the set
				if (!s.contains(pages[i])) {
					// Find the least recently used pages
					// that is present in the set
					int lru = Integer.MAX_VALUE, val = Integer.MIN_VALUE;

					Iterator<Integer> itr = s.iterator();

					while (itr.hasNext()) {
						int temp = itr.next();
						if (indexes.get(temp) < lru) {
							lru = indexes.get(temp);
							val = temp;
						}
					}

					// Remove the indexes page
					s.remove(val);
					// remove lru from hashmap
					indexes.remove(val);
					// insert the current page
					s.add(pages[i]);

					// Increment page faults
					page_faults++;
				}

				// Update the current page index
				indexes.put(pages[i], i);
			}
		}

		return page_faults;
	}

	// Driver method
	public static void main(String args[]) {
		// int pages[] = { 7, 0, 1, 2, 0, 3, 0, 4, 2, 3, 0, 3, 2 };
		int pages[] = { 5, 0, 1, 3, 2, 4, 1, 0, 5 };

		int capacity = 4;

		System.out.println(pageFaults(pages, pages.length, capacity));
	}

}
