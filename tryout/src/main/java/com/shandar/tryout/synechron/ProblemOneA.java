/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.synechron </p>
 * <p>File Name: ProblemOne.java </p>
 * <p>Create Date: 01-Jul-2020 </p>
 * <p>Create Time: 3:56:22 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.synechron;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author : Shantanu Sikdar
 *
 */
public class ProblemOneA {

	public static void main(String[] args) {

		class Employee {
			String firstName;
			String lastName;
			int age;

			public Employee(String firstName, String lastName, int age) {
				this.firstName = firstName;
				this.lastName = lastName;
				this.age = age;
			}

			@Override
			public String toString() {
				return "Employee [firstName=" + firstName + ", lastName=" + lastName + ", age=" + age + "]";
			}
		}

		List<Employee> lstEmp = Arrays.asList(new Employee("Shantanu", "Sikdar", 38),
				new Employee("Aniket", "Bhalerao", 27), new Employee("Faisal", "Husain", 24),
				new Employee("Sandeep", "Kumar", 26));
		System.out.println(lstEmp);
		List<Employee> lstEmp1 = lstEmp.stream().filter(empage -> empage.age > 26)
				.sorted((emp1, emp2) -> emp1.firstName.compareTo(emp2.firstName)).collect(Collectors.toList());
		System.out.println(lstEmp1);

	}
}
