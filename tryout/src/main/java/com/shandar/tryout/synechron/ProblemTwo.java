/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.synechron </p>
 * <p>File Name: ProblemTwo.java </p>
 * <p>Create Date: 01-Jul-2020 </p>
 * <p>Create Time: 3:56:41 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.synechron;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * @author : Shantanu Sikdar
 * 
 * https://stackoverflow.com/questions/3741765/ordering-threads-to-run-in-the-order-they-were-created-started
 * 
 */
public class ProblemTwo {

	static int NUM_THREADS = 10;

	public static void main(String[] args) {
		ExecutorService execServ = Executors.newFixedThreadPool(NUM_THREADS);

		class CallableThread implements Callable {
			private final Integer threadNumber;

			public CallableThread(Integer threadNum) {
				this.threadNumber = threadNum;
			}

			@Override
			public Integer call() throws Exception {
				System.out.println("Thread # " + threadNumber + " Started");
				return threadNumber;
			}
		}

		List<Callable<Integer>> callLst = new ArrayList<Callable<Integer>>();
		for (int i = 0; i < NUM_THREADS; i++) {
			callLst.add(new CallableThread(i));
		}

		try {
			List<Future<Integer>> futures = execServ.invokeAll(callLst);
			for (Future<Integer> future : futures) {
				System.out.println("Thread # " + future.get() + " finished");
			}
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}finally {
			execServ.shutdownNow();
		}
		

	}

	public static void main1(String[] args) {
		ExecutorService exec = Executors.newFixedThreadPool(NUM_THREADS);
		class MyCallable implements Callable<Integer> {
			private final int threadnumber;

			MyCallable(int threadnumber) {
				this.threadnumber = threadnumber;
			}

			public Integer call() {
				System.out.println("Running thread #" + threadnumber);
				return threadnumber;
			}
		}

		List<Callable<Integer>> callables = new ArrayList<Callable<Integer>>();
		for (int i = 1; i <= NUM_THREADS; i++) {
			callables.add(new MyCallable(i));
		}
		try {
			List<Future<Integer>> results = exec.invokeAll(callables);
			for (Future<Integer> result : results) {
				System.out.println("Got result of thread #" + result.get());
			}
		} catch (InterruptedException ex) {
			ex.printStackTrace();
		} catch (ExecutionException ex) {
			ex.printStackTrace();
		} finally {
			exec.shutdownNow();
		}
	}

}
