/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.hitachivantara </p>
 * <p>File Name: JosephusProblem.java </p>
 * <p>Create Date: 09-Dec-2021 </p>
 * <p>Create Time: 11:14:09 AM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.hitachivantara;

/**
 * @author : Shantanu Sikdar
 *         https://www.geeksforgeeks.org/josephus-problem-iterative-solution/
 */
public class JosephusProblem {
	
	/**
	 * https://www.geeksforgeeks.org/josephus-problem-set-1-a-on-solution/
	 * @param n
	 * @param k
	 * @return
	 */
	private static int josephusRecusrsive(int n, int k) {
		if (n == 1) {
			return 1;
		} else {
			return (josephusRecusrsive(n - 1, k) + k - 1) % n + 1;
		}

	}

	public static void main(String[] args) {
		System.out.println(josephusRecusrsive(13, 2));
	}

}
