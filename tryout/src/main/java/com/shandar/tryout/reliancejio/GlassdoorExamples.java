/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.reliancejio </p>
 * <p>File Name: GlassdoorExamples.java </p>
 * <p>Create Date: 24-Mar-2020 </p>
 * <p>Create Time: 10:37:15 am </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.tryout.reliancejio;

/**
 * @author Shantanu Sikdar
 *
 */
public class GlassdoorExamples {

	public static void main(String[] args) {
		swapWithoutTempVarialble();
		factorialUsingLoop(5);
		int factorial = factorialRecusrion(5);
		System.out.println(factorial);
	}

	private static void swapWithoutTempVarialble() {
		System.out.println(Thread.currentThread().getStackTrace()[1]);
		int a = 20, b = 10;
		System.out.println("a = " + a + " b = " + b);
		b = a + b;
		a = b - a;
		b = b - a;
		System.out.println("a = " + a + " b = " + b);
	}

	private static void factorialUsingLoop(int factorialNum) {
		int factorial = 1;
		for (int i = 1; i <= factorialNum; i++) {
			factorial *= i;
		}
		System.out.println(factorial);
	}

	public static int factorialRecusrion(int n) {
		if (n == 1) {
			return 1;
		} else {
			return n * factorialRecusrion(--n);
		}
	}
	
}
