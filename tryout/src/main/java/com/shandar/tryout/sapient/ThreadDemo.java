/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.sapient </p>
 * <p>File Name: ThreadDemo.java </p>
 * <p>Create Date: 31-Mar-2021 </p>
 * <p>Create Time: 10:40:44 am </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.sapient;

/**
 * @author : Shantanu Sikdar
 *
 */
public class ThreadDemo extends Thread{

	final StringBuffer sb1 = new StringBuffer();
	final StringBuffer sb2 = new StringBuffer();
	
	public static void main(String[] args) {
		final ThreadDemo h = new ThreadDemo();
		
		new Thread() {
			public void run() {
				synchronized (this) {
					h.sb1.append("java");
					h.sb2.append("thread");
					System.out.println(h.sb1);
					System.out.println(h.sb2);
				}
			}
		}.start();
		
		
		new Thread() {
			public void run() {
				synchronized (this) {
					h.sb1.append("Multithreaded");
					h.sb2.append("example");
					System.out.println(h.sb2);
					System.out.println(h.sb1);
				}
			}
		}.start();
	}
}
