/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.quickheal </p>
 * <p>File Name: SingletonForXNumerOfObjects.java </p>
 * <p>Create Date: 02-Apr-2020 </p>
 * <p>Create Time: 5:45:49 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.quickheal;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author : Shantanu Sikdar
 *
 */
public class SingletonForXNumerOfObjects {

	public static void main(String[] args) {
		ObjectPool objPool = ObjectPool.GetInstance();
        objPool.SetMaxPoolSize(10);
        PooledObject obj = objPool.acquireReusable();
        objPool.releaseReusable(obj);
        //Console.WriteLine(obj.CreatedAt);
        System.out.println(obj.createdAt());
        
        //Console.ReadKey();
    }
}

class ObjectPool {
	private static List<PooledObject> _available = new ArrayList<>();
	private List<PooledObject> _inUse = new ArrayList<>();

	private int counter = 0;
	private int MAXTotalObjects;

	private static ObjectPool instance = null;

	public static ObjectPool GetInstance() {
		if (instance == null) {
			instance = new ObjectPool();
		} else {
			System.out.println("This is singleton!");
		}
		return instance;
	}

	public PooledObject acquireReusable() {
		if (_available.size() != 0 && _available.size() < 10) {
			PooledObject item = _available.get(0);
			_inUse.add(item);
			_available.remove(0);
			counter--;
			return item;
		} else {
			PooledObject obj = new PooledObject();
			_inUse.add(obj);
			return obj;
		}
	}

	public void releaseReusable(PooledObject item) {
		if (counter <= MAXTotalObjects) {
			_available.add(item);
			counter++;
			_inUse.remove(item);
		} else {
			System.out.println("To much object in pool!");
		}
	}

	public void SetMaxPoolSize(int settingPoolSize) {
		MAXTotalObjects = settingPoolSize;
	}
}

class PooledObject {
	Calendar createdAt = Calendar.getInstance();

	public Date createdAt() {
		return createdAt.getTime();
	}

}
