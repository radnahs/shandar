/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.foghorn </p>
 * <p>File Name: OddEvenPrintBy2Thread.java </p>
 * <p>Create Date: 27-Mar-2020 </p>
 * <p>Create Time: 4:19:14 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
 
package com.shandar.tryout.foghorn;
        
/**
 * @author Shantanu Sikdar
 *
 */
public class OddEvenPrintBy2Thread {

	public static void main(String[] args) {
		OddEvenPrintBy2Thread obj = new OddEvenPrintBy2Thread();
		obj.evenThreadOddThread();
	}

	private void evenThreadOddThread() {

		final Object lock = new Object();

		Thread tOdd = new Thread(() -> {
			synchronized (lock) {
				for (int i = 1; i < 10; i += 2) {
					System.out.println(i);
					try {
						lock.wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					lock.notify();
				}
			}

		});

		Thread tEven = new Thread(() -> {
			for (int i = 2; i < 10; i += 2) {
				synchronized (lock) {
					lock.notify();
					System.out.println(i);
					try {
						lock.wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		});

		tOdd.start();
		tEven.start();
	}

}
