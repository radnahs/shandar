/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.hackerearth </p>
 * <p>File Name: FredoAndArrayUpdate.java </p>
 * <p>Create Date: 10-Mar-2020 </p>
 * <p>Create Time: 9:26:26 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.hackerearth;
/**
 * Fredo is assigned a new task today. He is given an array A containing N integers.
 *  His task is to update all elements of array to some minimum value x , that is,  ; 
 *   such that sum of this new array is strictly greater than the sum of the initial array. Note that x should be as minimum as possible such that sum of the new array is greater than the sum of the initial array.

Input Format:
First line of input consists of an integer N denoting the number of elements in the array A.
Second line consists of N space separated integers denoting the array elements.

Output Format:
The only line of output consists of the value of x.

Input Constraints:


SAMPLE INPUT 
5
1 2 3 4 5
SAMPLE OUTPUT 
4
Explanation
Initial sum of array 
When we update all elements to 4, sum of array  which is greater than .
Note that if we had updated the array elements to 3,  which is not greater than . So, 4 is the minimum value to which array elements need to be updated.
 */

import java.util.Arrays;
import java.util.Scanner;

/**
 * @author : Shantanu Sikdar
 *
 */
public class FredoAndArrayUpdate {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
        Integer int1 = s.nextInt();
        String[] strArr = s.nextLine().split(" ");
        int[] intArr = Arrays.asList(strArr).stream().mapToInt(Integer::parseInt).toArray();
        
		//int[] intArr = new int[] {2,3,4,5};
		int sum = arrayElemSum(intArr);
		System.out.println(sum);
		int size = intArr.length;
		int num = sum/size +1;
		System.out.println(num);
	}
	
	private static int arrayElemSum(int[] intArray){
		int sum =0;
		for (Integer integer : intArray) {
			sum+=integer;
		}
		return sum;
	}
}
