/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.hackerearth </p>
 * <p>File Name: LittleShinoAndCommonFactors.java </p>
 * <p>Create Date: 10-Mar-2020 </p>
 * <p>Create Time: 8:25:55 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.hackerearth;

/**
 * Little Shino loves maths. Today her teacher gave her two integers. Shino is now wondering how many integers can divide both the numbers. She is busy with her assignments. Help her to solve the problem.

Input:
First line of the input file contains two integers, a and b.

Output:
Print the number of common factors of a and b.

Constraints:

SAMPLE INPUT 
10 15
SAMPLE OUTPUT 
2
Explanation
The common factors of  and  are 1 and 5.
 */
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @author : Shantanu Sikdar
 *
 */
public class LittleShinoAndCommonFactors {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
        Integer int1 = s.nextInt();
        Integer int2 = s.nextInt();
        
		List<Integer> num1 = factorsOfANumber(25);
		List<Integer> num2 = factorsOfANumber(9);
		num1.retainAll(num2);
		System.out.println(num1);
		System.out.println(num1.size());
	}

	private static List<Integer> factorsOfANumber(Integer n) {
		List<Integer> lstInt = new ArrayList<Integer>();
		for (int i = 1; i <= n/2; i++) {
			if(n%i==0) {
				lstInt.add(i);
			}
		}
		System.out.println("inpt = "+n+" factors list = "+lstInt);
		return lstInt;
	}

}
