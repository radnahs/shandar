/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.hackerearth </p>
 * <p>File Name: TwoStringAnagram.java </p>
 * <p>Create Date: 24-Jul-2020 </p>
 * <p>Create Time: 9:50:23 am </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.hackerearth;

import java.util.Arrays;

/**
 * @author : Shantanu Sikdar
 *
 */
public class TwoStringAnagram {

	public static void main(String[] args) {
		//String s1 = "shantanuh", s2 = "unatnahss";
		String s1 = "ra3d2kis", s2 = "sikdar23";
		char[] chr1 = s1.toCharArray();
		char[] chr2 = s2.toCharArray();
		Arrays.sort(chr1);
		Arrays.sort(chr2);
		String isAnagram ="YES";
		for(int i = 0; i< chr1.length;i++) {
			if(!(chr1[i]==chr2[i])) {
				isAnagram ="NO";
				break;
			}
		}
		System.out.println(isAnagram);
	}

}
