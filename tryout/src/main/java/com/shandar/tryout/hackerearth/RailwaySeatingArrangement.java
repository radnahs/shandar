/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.hackerearth </p>
 * <p>File Name: RailwaySeatingArrangement.java </p>
 * <p>Create Date: 23-Jul-2020 </p>
 * <p>Create Time: 5:10:00 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.hackerearth;

import java.util.Scanner;

/**
 * @author : Shantanu Sikdar
 *
 *         Akash and Vishal are quite fond of travelling. They mostly travel by
 *         railways. They were travelling in a train one day and they got
 *         interested in the seating arrangement of their compartment. The
 *         compartment looked something like
 * 
 * 
 *         So they got interested to know the seat number facing them and the
 *         seat type facing them. The seats are denoted as follows :
 * 
 *         Window Seat : WS Middle Seat : MS Aisle Seat : AS
 * 
 *         You will be given a seat number, find out the seat number facing you
 *         and the seat type, i.e. WS, MS or AS.
 * 
 *         INPUT First line of input will consist of a single integer T denoting
 *         number of test-cases. Each test-case consists of a single integer N
 *         denoting the seat-number.
 * 
 *         OUTPUT For each test case, print the facing seat-number and the
 *         seat-type, separated by a single space in a new line.
 * 
 *         CONSTRAINTS 1<=T<=105 1<=N<=108 SAMPLE INPUT 2 18 40 SAMPLE OUTPUT 19
 *         WS 45 AS Time Limit: 1.0 sec(s) for each input file. Memory Limit:
 *         256 MB Source Limit: 1024 KB
 *
 *
 */
public class RailwaySeatingArrangement {

	public static void main(String[] args) {
		seatingArrangement(6);
		seatingArrangement(1);
		seatingArrangement(11);
		seatingArrangement(100);
		seatingArrangement(106);
	}

	private static void seatingArrangement(int seatNo) {

//		1   2   3   4   5   6 
//		12	11  10  9   8   7	
		String seatType = "";
		if (seatNo % 6 == 0 || seatNo % 6 == 1) {
			seatType = "WS";
			// System.out.println("WS");
		} else if (seatNo % 6 == 2 || seatNo % 6 == 5) {
			seatType = "MS";
			// System.out.println("MS");
		} else if (seatNo % 6 == 3 || seatNo % 6 == 4) {
			seatType = "AS";
			// System.out.println("AS");
		} else {
			// System.out.println("standing");
		}

		int oppositeSeat = 0;
		int bucket = seatNo % 12;
		if (bucket == 1) {
			oppositeSeat = seatNo + 11;
		} else if (bucket == 2) {
			oppositeSeat = seatNo + 9;
		} else if (bucket == 3) {
			oppositeSeat = seatNo + 7;
		} else if (bucket == 4) {
			oppositeSeat = seatNo + 5;
		} else if (bucket == 5) {
			oppositeSeat = seatNo + 3;
		} else if (bucket == 6) {
			oppositeSeat = seatNo + 1;
		} else if (bucket == 7) {
			oppositeSeat = seatNo - 1;
		} else if (bucket == 8) {
			oppositeSeat = seatNo - 3;
		} else if (bucket == 9) {
			oppositeSeat = seatNo - 5;
		} else if (bucket == 10) {
			oppositeSeat = seatNo - 7;
		} else if (bucket == 11) {
			oppositeSeat = seatNo - 9;
		} else {
			oppositeSeat = seatNo - 11;
		}
		System.out.println(oppositeSeat + " " + seatType);
	}

	private void solution() {
		Scanner s = new Scanner(System.in);
		int name = Integer.parseInt(s.nextLine()); // Reading input from STDIN
		// System.out.println("Hi, " + name + "."); // Writing output to STDOUT
		// System.out.println(name);
		for (int i = 0; i < name; i++) {
			// Write your code here
			int seatNo = Integer.parseInt(s.nextLine());
			String seatType = "";
			if (seatNo % 6 == 0 || seatNo % 6 == 1) {
				seatType = "WS";
			} else if (seatNo % 6 == 2 || seatNo % 6 == 5) {
				seatType = "MS";
			} else if (seatNo % 6 == 3 || seatNo % 6 == 4) {
				seatType = "AS";
			} else {
			}

			int oppositeSeat = 0;
			int bucket = seatNo % 12;
			if (bucket == 1) {
				oppositeSeat = seatNo + 11;
			} else if (bucket == 2) {
				oppositeSeat = seatNo + 9;
			} else if (bucket == 3) {
				oppositeSeat = seatNo + 7;
			} else if (bucket == 4) {
				oppositeSeat = seatNo + 5;
			} else if (bucket == 5) {
				oppositeSeat = seatNo + 3;
			} else if (bucket == 6) {
				oppositeSeat = seatNo + 1;
			} else if (bucket == 7) {
				oppositeSeat = seatNo - 1;
			} else if (bucket == 8) {
				oppositeSeat = seatNo - 3;
			} else if (bucket == 9) {
				oppositeSeat = seatNo - 5;
			} else if (bucket == 10) {
				oppositeSeat = seatNo - 7;
			} else if (bucket == 11) {
				oppositeSeat = seatNo - 9;
			} else {
				oppositeSeat = seatNo - 11;
			}
			System.out.println(oppositeSeat + " " + seatType);
		}
	}
}
