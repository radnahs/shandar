/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.hackerearth </p>
 * <p>File Name: ArrayOperations.java </p>
 * <p>Create Date: 10-Mar-2020 </p>
 * <p>Create Time: 11:24:26 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.hackerearth;

/**
 * @author : Shantanu Sikdar
 *
 */
public class ArrayOperations {

	public static void main(String[] args) {
		int[] intArr = new int[] { 10, 1, 20, 1 };
		int[] intArr1 = new int[] { 1, 10, 20, 1 };
		int sum = 1 & 10 & 20 & 1;
		int sum1 = 1 & 1 & 10 & 20;
		int sum2 = 10 & 1 & 20 & 1;
		int sum3 = (10 & 1) + (10 & 20) + (1 & 20) + (1 & 1) + (20 & 1);
		// System.out.println(sum3);
		printCombination(intArr,intArr.length, 2);
		// combine(intArr1);
	}

	static void combinationUtil(int arr[], int data[], int start, int end, int index, int r) {
		if (index == r) {
			for (int j = 0; j < r; j++) {
				System.out.print(data[j] + " ");
			}
			System.out.println("");
			return;
		}

		for (int i = start; i <= end && end - i + 1 >= r - index; i++) {
			data[index] = arr[i];
			combinationUtil(arr, data, i + 1, end, index + 1, r);
		}
	}

	static void printCombination(int arr[], int n, int r) {
		int data[] = new int[r];

		combinationUtil(arr, data, 0, n - 1, 0, r);
	}

}
