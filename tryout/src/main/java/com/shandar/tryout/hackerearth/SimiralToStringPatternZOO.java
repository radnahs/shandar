/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.hackerearth </p>
 * <p>File Name: PrintTheNumbers.java </p>
 * <p>Create Date: 24-Jul-2020 </p>
 * <p>Create Time: 11:40:15 am </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.hackerearth;

/**
 * @author : Shantanu Sikdar
 *
 */
public class SimiralToStringPatternZOO {

	public static void main(String[] args) {
		//char[] charArr = "zzzooooooo".toCharArray();//no
		//char[] charArr = "zooozo".toCharArray();//yes
		char[] charArr = "zooozoo".toCharArray();//no
		int countZ = 0, countO = 0;
		for (int i = 0; i < charArr.length; i++) {
			if (charArr[i] == 'O' || charArr[i] == 'o') {
				countO += 1;
			} else if (charArr[i] == 'Z' || charArr[i] == 'z') {
				countZ += 1;
			} else {

			}
		}
		if (countO == countZ * 2) {
			System.out.println("Yes");
		} else {
			System.out.println("No");
		}

	}

}
