/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.hackerearth </p>
 * <p>File Name: OtherElementSumDivisible.java </p>
 * <p>Create Date: 24-Jul-2020 </p>
 * <p>Create Time: 6:29:12 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.hackerearth;

import java.util.Arrays;

/**
 * @author : Shantanu Sikdar
 * 
 *         Sussutu is a world-renowned magician. And recently, he was blessed
 *         with the power to remove EXACTLY ONE element from an array.
 * 
 *         Given, an array A (index starting from 0) with N elements. Now,
 *         Sussutu CAN remove only that element which makes the sum of ALL the
 *         remaining elements exactly divisible by 7.
 * 
 *         Throughout his life, Sussutu was so busy with magic that he could
 *         never get along with maths. Your task is to help Sussutu find the
 *         first array index of the smallest element he CAN remove.
 * 
 *         Input: The first line contains a single integer N. Next line contains
 *         N space separated integers Ak , 0 < k < N.
 * 
 *         Output: Print a single line containing one integer, the first array
 *         index of the smallest element he CAN remove, and -1 if there is no
 *         such element that he can remove!
 * 
 *         Constraints: 1 < N < 105 0 < Ak < 109
 * 
 *         SAMPLE INPUT 5 14 7 8 2 4 SAMPLE OUTPUT 1 Explanation Both 14 and 7
 *         are valid answers, but since 7 is the smallest, the required array
 *         index is 1.
 */
public class OtherElementSumDivisible {

	public static void main(String[] args) {
		// String str = "14 28 8 2 4 7";
		String str = "18 794 259 404 945 483 884 155 214 279 965 830 941 249 491 353 459 40 293 749 783 21 619 68 650 321 939 230 767 786 78 604 776 864 740 721 615 32 4 501 6 643 150 676 152 955 598 582 402 967 143 254 675 769 340 458 707 867 666 206 565 733 175 810 925 972 675 449 684 854 854 901 39 853 618 527 456 525 685 850 47 770 133 521 263 944 264 906 669 858 265 724 827 432 641 654 73 538 687 324";
		horizontalScalingWay(str);
	}

	private static void horizontalScalingWay(String str) {
		int[] arrVal = Arrays.asList(str.split(" ")).stream().mapToInt(Integer::parseInt).toArray();
		int arrSum = Arrays.stream(arrVal).sum();
		int indx = -1;
		for (int i = 0; i < arrVal.length; i++) {
			int tmpSum = arrSum - arrVal[i];
			if (tmpSum % 7 == 0) {
				if (indx == -1) {
					indx = i;
				}
				if (arrVal[indx] > arrVal[i]) {
					indx = i;
				}
			}
		}
		System.out.println(indx);
	}

	private static void verticalScalingWay(String str) {
		int[] arrInt = Arrays.asList(str.split(" ")).stream().mapToInt(Integer::parseInt).toArray();
		int indx = -1;
		for (int i = 0; i < arrInt.length; i++) {
			long sum = 0;
			for (int j = 0; j < arrInt.length; j++) {
				if (i != j) {
					sum += arrInt[j];
				}
			}
			if (sum % 7 == 0) {
				if (indx == -1) {
					indx = i;
				}
				if (arrInt[indx] > arrInt[i]) {
					indx = i;
				}
			}
		}
		System.out.println(indx);
	}

}
