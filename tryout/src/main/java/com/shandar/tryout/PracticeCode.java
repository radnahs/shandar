/**
 *
 * <p>Project: tryout_trunk </p>
 * <p>Package Name: com.shandar.tryout </p>
 * <p>File Name: practice.java</p>
 * <p>Create Date: Dec 9, 2015 </p>
 * <p>Create Time: 8:21:24 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.tryout;

/**
 * @author Shantanu Sikdar
 * 
 */
public class PracticeCode {

	public static void main(String[] args) {
		try {
			bar();
		} catch (NullPointerException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		foo();
	}

	public static void bar() {
	}

	public static void foo() throws NullPointerException {
	}

}
