/**
 * Project: tryout 
 * Package Name: com.shandar.tryout.visa 
 * File Name: Problem1.java 
 * Create Date: 14-May-2021 
 * Create Time: 11:53:53 am 
 * Copyright: Copyright (c) 2016
 * Company:  
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.visa;

import java.util.Collections;
import java.util.List;

/**
 * @author : Shantanu Sikdar
 * 
 *    7. Merge 2 Arrays Merge two sorted arrays into a single
 *    non-decreasing array.
 * 
 *    Given two sorted arrays, merge them to form a single, sorted array
 *    with all items in non-decreasing order.
 * 
 *    Example
 * 
 *    a = [1, 2, 3]
 * 
 *    b = [2, 5, 5]
 * 
 *    Merge the arrays to create array c as follows:
 * 
 *    a[0] < b[0] → c = [a[0]] = [1] a[1] = b[0] → c = [a[0], b[0]] = [1,
 *    2] a[1] < b[1] → c = [a[0], b[0], a[1]] = [1, 2, 2] a[2] < b[1] → c =
 *    [a[0], b[0], a[1], a[2]]= [1, 2, 2, 3] No more elements in a → c =
 *    [a[0], b[0], a[1], a[2], b[1], b[2]] = [1, 2, 2, 3, 5, 5]
 * 
 * 
 *    Elements were alternately taken from the arrays in the order given,
 *    maintaining precedence.
 * 
 *    Function Description
 * 
 *    Complete the function mergeArrays in the editor below.
 * 
 *    mergeArrays has the following parameter(s): int a[n]: a sorted array
 *    of integers int b[n]: a sorted array of integers
 * 
 *    Returns
 * 
 *    int[n]: an array of all the elements from both input arrays in
 *    non-decreasing order
 * 
 *    Constraints
 * 
 * 
 *    1 < n < 5 × 10<sup>5</sup>
 * 
 *    0 ≤ a[i], b[i] ≤ 10<sup>9</sup>, where 0 ≤ i < n
 * 
 * 
 *    <!-- <StartOfInputFormat> DO NOT REMOVE THIS LINE-->
 * 
 *    Input Format for Custom
 *    Testing
 * 
 *    Input from stdin will be processed
 *    as follows and passed to the function.
 * 
 * 
 *    The first line contains an integer n, the size of the array a.
 * 
 *    The next n lines each contain an element a[i] where 0 ≤ i < n.
 * 
 *    The next line contains an integer n, the size of the array b.
 * 
 *    The next n lines each contain an element b[i] where 0 ≤ i < n.
 * 
 *    Sample Case 0
 * 
 *    Sample Input 0
 * 
 *    STDIN Function ----- -------- 4 → a[] size n = 4 1 → a = [1, 5, 7, 7]
 *    1 5 7 7 4 → b[] size n = 4 0 → b = [0, 1, 2, 3] 1 2 3
 * 
 * 
 *    Sample Output 0
 * 
 *    0 1 1 2 3 5 7 7
 * 
 * 
 *    Explanation
 * 
 *    The following arrays are passed to mergeArrays as arguments:
 * 
 *    a = [ 1, 5, 7, 7]
 * 
 *    b = [ 0, 1, 2, 3]
 * 
 *    The mergedArray function returns the following merged, non-decreasing
 *    array: [ 0, 1, 1, 2, 3, 5, 7, 7 ]
 * 
 *    Sample Case 1
 * 
 *    Sample Input 1
 * 
 *    STDIN Function ----- -------- 5 → a[] size n = 5 2 → a = [2, 4, 5, 9,
 *    9] 4 5 9 9 5 → b[] size n = 5 0 → b = [0, 1, 2, 3, 4] 1 2 3 4
 * 
 * 
 *    Sample Output 1
 * 
 *    0 1 2 2 3 4 4 5 9 9
 * 
 *    Explanation
 * 
 *    The following arrays are passed to mergeArrays as arguments:
 * 
 *    a = [ 2, 4, 5, 9, 9]
 * 
 *    b = [ 0, 1, 2, 3, 4]
 * 
 *    The mergedArray function returns the following merged, non-decreasing
 *    array: [0, 1, 2, 2, 3, 4, 4, 5, 9, 9]
 * 
 *
 */

public class MergeArrays {

	public static void main(String[] args) {

	}

	public static List<Integer> mergeArrays(List<Integer> a, List<Integer> b) {
		a.addAll(b);
		Collections.sort(a);
		return a;
	}

}
