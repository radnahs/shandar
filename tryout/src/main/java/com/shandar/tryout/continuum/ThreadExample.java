/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.continuum </p>
 * <p>File Name: ThreadExample.java </p>
 * <p>Create Date: 03-Apr-2020 </p>
 * <p>Create Time: 5:00:09 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.continuum;

/**
 * @author : Shantanu Sikdar
 *
 */
public class ThreadExample {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Thread t1 = new Thread(new Runnable() {
			
			@Override
			public void run() {
				for (int i = 0; i < 10; i++) {
					System.out.println(i);
				}
			}
		});
		t1.start();
		System.out.println("main");
		
	}

}
