/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.continuum </p>
 * <p>File Name: LossInLongestLosingStreak.java </p>
 * <p>Create Date: 21-Mar-2020 </p>
 * <p>Create Time: 4:19:14 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.continuum;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

/**
 * Consider this C Program, C++ Program, Java Program, C# Program, or Python
 * Program. It reads integers from the standard input (until it gets a negative
 * number) and puts them into an array. After that it calls processArray on the
 * array, and then prints the value that is returned to standard output.
 * 
 * 
 * Currently, processArray does not do anything useful - it just returns 0.
 * 
 * You have to change processArray so that it finds the longest losing streak in
 * the array, and returns the loss of the streak.
 * 
 * A sequence of consecutive numbers in which each number is less than or equal
 * to the previous number is called a losing streak. The difference in value
 * between the last number of the streak and the first number of the streak is
 * known as the loss.
 * 
 * For example, if these numbers were provided on the standard input:
 * 
 * 3 6 36 32 32 121 66 24 22 371 661 6 4 8 -1
 * 
 * The losing streaks in this sequence are 36, 32, 32, and 121,66,24,22, and
 * 661,6,4, and the corresponding losses are 4 and 99 and 657. In this case, the
 * 2nd streak with 4 numbers is the longest, so your program should print it's
 * loss:
 * 
 * 99
 * 
 * Please read this example carefully to make sure you really, really understand
 * what the program is supposed to do. If your submission is rejected by the
 * system, it is very likely that you did not read the instructions properly, or
 * did not study the example closely.
 * 
 * @author : Shantanu Sikdar
 *
 */

public class LossInLongestLosingStreak {

	public static void main(String[] args) {
		LossInLongestLosingStreak lills = new LossInLongestLosingStreak();
		int[] intArr = new int[] { 3, 6, 36, 32, 32, 121, 66, 24, 22, 371, 661, 6, 4, 8, -1 };
		lills.processArray(intArr);

		Integer[] intArrObj = new Integer[] { 3, 6, 36, 32, 32, 121, 66, 24, 22, 371, 661, 6, 4, 8, -1 };
		List<Integer> array = Arrays.asList(intArrObj);
		Integer ngr = processArray(new ArrayList(array));
		System.out.println(ngr);
	}

	public static void main1(String[] args) {
		ArrayList<Integer> arrayList = new ArrayList<Integer>();
		Scanner in = new Scanner(System.in);
		while (in.hasNextInt()) {
			int num = in.nextInt();
			if (num < 0)
				break;
			arrayList.add(new Integer(num));
		}
		int result = processArray(arrayList);
		System.out.println(result);
	}

	public static int processArray(ArrayList<Integer> array) {
		if (array.size() == 0) {
			return 0;
		}
		Map<Integer, List<Integer>> mapLstInt = new HashMap<Integer, List<Integer>>();
		int cnt = 0;
		boolean temp = true;
		List<Integer> tmpLst = new ArrayList<Integer>();
		for (int i = 1; i < array.size(); i++) {
			if (array.get(i - 1) >= array.get(i)) {
				if (temp == true) {
					tmpLst.add(array.get(i - 1));
					temp = false;
				}
				tmpLst.add(array.get(i));
			} else {
				cnt++;
				temp = true;
				List<Integer> tst = new ArrayList<Integer>(tmpLst);
				mapLstInt.put(cnt, tst);
				tmpLst = new ArrayList<Integer>();
			}
		}
		Integer tmpSize = 0;
		List<Integer> finalLst = new ArrayList<Integer>();
		for (Entry<Integer, List<Integer>> ntryMap : mapLstInt.entrySet()) {
			if (ntryMap.getValue().size() > tmpSize) {
				tmpSize = ntryMap.getValue().size();
				finalLst = ntryMap.getValue();
			}
		}
		return finalLst.get(0) - finalLst.get(tmpSize - 1);
	}

	private void processArray(int[] intArr) {
		Map<Integer, List<Integer>> mapLstInt = new HashMap<Integer, List<Integer>>();
		int cnt = 0;
		boolean temp = true;
		List<Integer> tmpLst = new ArrayList<Integer>();
		for (int i = 1; i < intArr.length; i++) {
			if (intArr[i - 1] >= intArr[i]) {
				if (temp == true) {
					System.out.println(intArr[i - 1]);
					tmpLst.add(intArr[i - 1]);
					temp = false;
				}
				System.out.println(intArr[i]);
				tmpLst.add(intArr[i]);
			} else {
				cnt++;
				temp = true;
				List<Integer> tst = new ArrayList<Integer>(tmpLst);
				mapLstInt.put(cnt, tst);
				tmpLst = new ArrayList<Integer>();
			}
			// System.out.println(tmpLst);
		}
		System.out.println(mapLstInt);
		Integer tmpSize = 0;
		List<Integer> finalLst = new ArrayList<Integer>();
		for (Entry<Integer, List<Integer>> ntryMap : mapLstInt.entrySet()) {
			if (ntryMap.getValue().size() > tmpSize) {
				tmpSize = ntryMap.getValue().size();
				finalLst = ntryMap.getValue();
			}
		}
		System.out.println(tmpSize);
		System.out.println(finalLst);
		System.out.println(finalLst.get(0) - finalLst.get(tmpSize - 1));
	}

}
