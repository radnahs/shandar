/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.innominds </p>
 * <p>File Name: AlphabetV.java </p>
 * <p>Create Date: 16-Sep-2022 </p>
 * <p>Create Time: 11:50:32 AM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.innominds;

/**
 * @author : Shantanu Sikdar
 *
 */
public class AlphabetV {
	public static void main(String[] args) {
		// System.out.println("Hello, World!");
		printV(10, "*");
	}

	private static void printV(int height, String charVal) {
		int k = (height) - 1;
		for (int i = 0; i < height / 2; i++) {
			for (int j = 0; j < height; j++) {
				if (j == i || j == k) {
					System.out.print(charVal);
				} else {
					System.out.print(" ");
				}
			}
			k--;
			System.out.println("");
		}
	}

}
