/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.jpmc </p>
 * <p>File Name: DistinctCombinationOfArrayElements.java </p>
 * <p>Create Date: 16-Feb-2023 </p>
 * <p>Create Time: 7:32:27 PM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.jpmc;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author : Shantanu Sikdar
 * 
 *
 *         Array of names={"coffee", "toffee", "beans","coffee"}
 * 
 *         coffee-toffee
 * 
 *         coffee-beans
 * 
 *         toffee-beans
 * 
 *         swap 1st letter if remaining letters are same of the word pair. if
 *         not count such
 * 
 *         toffee-coffee //invalid
 * 
 *         boffee-ceans //valid
 * 
 *         boffee-teans //valid
 * 
 *         o/p 2
 *
 *
 */
public class DistinctCombinationOfArrayElements {

	public static void main(String[] args) {

		String[] names = { "coffee", "toffee", "beans" };
		List<String> list = Arrays.asList(names);
		List<String> comboList = list.parallelStream().flatMap(str1 -> list.stream().map(str2 -> str1 + "-" + str2))
				.collect(Collectors.toList()); // used parallel stream
		System.out.println(comboList);

		List<String> finalList = comboList.stream().filter(str -> {
			String[] strArr = str.split("-");
			if (!((strArr[0].subSequence(1, strArr[0].length() - 1))
					.equals((strArr[1].subSequence(1, strArr[1].length() - 1))))) {
				return true;
			} else {
				return false;
			}
		}).collect(Collectors.toList());

		System.out.println(finalList);
	}

}
