/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.ideas.bookingSystem </p>
 * <p>File Name: DateOperations.java </p>
 * <p>Create Date: 06-Feb-2020 </p>
 * <p>Create Time: 8:32:27 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.ideas.bookingSystem;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author : Shantanu Sikdar
 *
 */
public class DateOperations {

	public static void main(String[] args) {
		DateOperations dtOp = new DateOperations();
		dtOp.listWeekEndWeekDaysBetweenDates("2020-02-24","2020-03-03");
	}

	
	public void listWeekEndWeekDaysBetweenDates(String startDateStr, String endDate){
		Map<String, List<LocalDate>> mapWeekEndWeekDay = new HashMap<String, List<LocalDate>>();
		LocalDate start = LocalDate.parse(startDateStr);
		LocalDate end = LocalDate.parse(endDate);
		List<LocalDate> totalDates = new ArrayList<>();
		while (!start.isAfter(end)) {

			/*if(mapWeekEndWeekDay.containsKey("WeekEnd") || mapWeekEndWeekDay.containsKey("WeekDay")) {
				if(start.getDayOfWeek().equals(DayOfWeek.SATURDAY) || start.getDayOfWeek().equals(DayOfWeek.SUNDAY)) {
					if(mapWeekEndWeekDay.containsKey("WeekEnd")) {
						
					}
				}else {
					
				}
			}else {
				
			}*/
			
			
			if(start.getDayOfWeek().equals(DayOfWeek.SATURDAY) || start.getDayOfWeek().equals(DayOfWeek.SUNDAY)) {
				if(mapWeekEndWeekDay.containsKey("WeekEnd")) {
					List<LocalDate> weekendDates = mapWeekEndWeekDay.get("WeekEnd");
					weekendDates.add(start);
					mapWeekEndWeekDay.put("WeekEnd", weekendDates);
				}else {
					List<LocalDate> weekendDates = new ArrayList<LocalDate>();
					weekendDates.add(start);
					mapWeekEndWeekDay.put("WeekEnd", weekendDates);
				}
				List<LocalDate> weekDay = mapWeekEndWeekDay.get("WeekEnd");
				
			}else {
				//mapWeekEndWeekDay.put("WeekDay", start);
			}
			
			totalDates.add(start);
		    start = start.plusDays(1);
		}
		
	}
	
}
