/**
 *
 * <p>Project: tryout_trunk </p>
 * <p>Package Name: com.shandar.tryout.nice </p>
 * <p>File Name: AbstractParent.java</p>
 * <p>Create Date: Feb 2, 2016 </p>
 * <p>Create Time: 4:48:46 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.tryout.nice;

/**
 * @author Shantanu Sikdar 
 *
 */
public abstract class AbstractParent {

	public AbstractParent(){
		System.out.println("Constructor Abstract Parent");
		call();
	}
	
	abstract void call();
}
