/**
 *
 * <p>Project: tryout_trunk </p>
 * <p>Package Name: com.shandar.tryout.nice </p>
 * <p>File Name: Child.java</p>
 * <p>Create Date: Feb 2, 2016 </p>
 * <p>Create Time: 4:50:22 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.tryout.nice;

/**
 * @author Shantanu Sikdar 
 *
 */
public class Child extends AbstractParent {

	private Integer intgr;
	public Child(){
		intgr= new Integer(10);
		System.out.println("Constructor Non-Abstract Child");
	}

	@Override
	void call() {
		System.out.println("In call, value of intgr = "+intgr );
	}
	
	public static void main(String[] args) {
		Child c = new Child();
	}

}
