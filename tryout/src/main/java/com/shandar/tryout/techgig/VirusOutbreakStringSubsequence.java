/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.techgig </p>
 * <p>File Name: VirusOutbreakStringSubsequence.java </p>
 * <p>Create Date: 05-Apr-2021 </p>
 * <p>Create Time: 11:44:54 am </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.techgig;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @author : Shantanu Sikdar
 *
 *         <pre>
 *  Virus Outbreak (100 Marks) 
 *  
 *  In the Martian land faraway, a new virus
 *  has evolved and is attacking the individuals at a fast pace. The
 *  scientists have figured out the virus composition, V. The big task is
 *  to identify the people who are infected. The sample of N people is
 *  taken to check if they are POSITIVE or NEGATIVE. A report is
 *  generated which provides the current blood composition B of the
 *  person.
 *
 *  POSITIVE or NEGATIVE ?
 *
 *  If the blood composition of the person is a subsequence of the virus
 *  composition V, then the person is identified as POSITIVE otherwise
 *  NEGATIVE.
 *
 *  Example:
 *
 *  Virus Composition, V = coronavirus
 *
 *  Blood Composition of the person , B = ravus
 *
 *  The person in question is POSITIVE as B is the subsequence of the V.
 *
 *  The scientists are busy with their research for medicine and request
 *  you to build a program which can quickly figure out if the person is
 *  POSITIVE or NEGATIVE. They will provide you with the virus
 *  composition V and all the people’s current blood composition. Can you
 *  help them?
 *
 *  Note: The virus and blood compositions are lowercase alphabet
 *  strings.
 *
 *  Input Format The first line of the input consists of the virus
 *  composition, V
 *
 *  The second line of he input consists of the number of people, N
 *
 *  Next N lines each consist of the blood composition of the ith person,
 *  Bi
 *
 *  Constraints 1<= N <=10
 *
 *  1<= |B|<= |V|<= 10^5
 *
 *  Output Format For each person, print POSITIVE or NEGATIVE in a
 *  separate line
 *
 *  Sample TestCase 1 
 *  
 *  Input 
 *  coronavirus 
 *  3 
 *  abcde 
 *  crnas 
 *  onarous 
 *  
 *  Output
 *  NEGATIVE 
 *  POSITIVE 
 *  NEGATIVE
 * 
 *         </pre>
 */

public class VirusOutbreakStringSubsequence {

	public static void main(String[] args) {
		String main = "coronavirus";
		String inp1 = "onarous";// "abcde";//;//"crnas";
		String inp2 = "";

		// Scanner sc = new Scanner(System.in);
		// String main = sc.nextILine();

		// method1(main, inp1);

		// method2
		// boolean result = isSubSequenceRecursive(new StringBuilder(inp1), new
		// StringBuilder(main), inp1.length(), main.length());

		// method3
		boolean result = isSubSequenceIterative(inp1, main, inp1.length(), main.length());

		if (result) {
			System.out.println("POSITIVE");
		} else {
			System.out.println("NEGATIVE");
		}

	}

	// method3
	static boolean isSubSequenceIterative(String str1, String str2, int m, int n) {
		int j = 0;

		for (int i = 0; i < n && j < m; i++) {
			if (str1.charAt(j) == str2.charAt(i)) {
				j++;
			}
		}

		return (j == m);
	}

	// method2
	static boolean isSubSequenceRecursive(StringBuilder str1, StringBuilder str2, int m, int n) {
		// Base Cases
		if (m == 0)
			return true;
		if (n == 0)
			return false;

		// If last characters of two strings are matching
		if (str1.charAt(m - 1) == str2.charAt(n - 1))
			return isSubSequenceRecursive(str1, str2, m - 1, n - 1);

		// If last characters are not matching
		return isSubSequenceRecursive(str1, str2, m, n - 1);
	}

	// menthod1
	private static boolean method1(String main, String inp1) {
		char[] mainChrArr = main.toCharArray();
		char[] inpChrArr = inp1.toCharArray();
		List<Integer> lstIntgr = new ArrayList<Integer>();
		for (int i = 0; i < inpChrArr.length; i++) {
			for (int j = 0; j < mainChrArr.length; j++) {
				if (inpChrArr[i] == mainChrArr[j]) {
					mainChrArr[j] = ' ';
					inpChrArr[i] = '#';
					lstIntgr.add(j);
					break;
				}
			}
		}
		System.out.println(lstIntgr);
		boolean result = false;
		if (lstIntgr.size() == inp1.length()) {
			for (int k = 0; k < lstIntgr.size() - 1; k++) {
				if (lstIntgr.get(k) > lstIntgr.get(k + 1)) {
					result = false;
					break;
				} else {
					result = true;
				}
			}
		}
		return result;
	}

}
