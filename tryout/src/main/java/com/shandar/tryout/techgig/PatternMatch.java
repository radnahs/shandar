/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.techgig </p>
 * <p>File Name: PatternMatch.java </p>
 * <p>Create Date: 07-Dec-2022 </p>
 * <p>Create Time: 7:18:07 PM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.techgig;

/**
 * @author : Shantanu Sikdar
 *		
 *	200
 *	400	800	1600
 *	500
 *	3200 6400 12800 25600
 *	1100
 *	51200 102400 204800 409600 819200
 */
public class PatternMatch {

	public static void main(String[] args) {
		int numOfLines = 6; // input from console
		int oddLn = 200, oddLnFact = 300, oddLnMultFact = 0;
		int evnLn = 400, evnLnNumValues = 3;
		for (int j = 0; j < numOfLines; j++) {
			oddLn = oddLn + oddLnFact * oddLnMultFact;
			System.out.println(oddLn);
			oddLnMultFact++;
			j++;
			for (int k = 0; k < evnLnNumValues; k++) {
				System.out.print(evnLn + " ");
				evnLn = evnLn * 2;
			}
			System.out.println();
			evnLnNumValues++;
		}

	}

}
