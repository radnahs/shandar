/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.techgig </p>
 * <p>File Name: VirusOutbreak.java </p>
 * <p>Create Date: 08-May-2021 </p>
 * <p>Create Time: 4:34:31 pm </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.techgig;

import java.util.Scanner;

/**
 * @author : Shantanu Sikdar
 * 
 *         Prime Game (100 Marks) Rax, a school student, was bored at home in
 *         the pandemic. He wanted to play but there was no one to play with. He
 *         was doing some mathematics questions including prime numbers and
 *         thought of creating a game using the same. After a few days of work,
 *         he was ready with his game. He wants to play the game with you.
 * 
 * 
 *         GAME: Rax will randomly provide you a range [ L , R ] (both
 *         inclusive) and you have to tell him the maximum difference between
 *         the prime numbers in the given range. There are three answers
 *         possible for the given range. There are two distinct prime numbers in
 *         the given range so the maximum difference can be found. There is only
 *         one distinct prime number in the given range. The maximum difference
 *         in this case would be 0. There are no prime numbers in the given
 *         range. The output for this case would be -1.
 * 
 * 
 *         To win the game, the participant should answer the prime difference
 *         correctly for the given range.
 * 
 * 
 *         Example:
 * 
 *         Range: [ 1, 10 ]
 * 
 *         The maximum difference between the prime numbers in the given range
 *         is 5.
 * 
 *         Difference = 7 - 2 = 5
 * 
 *         Range: [ 5, 5 ] There is only one distinct prime number so the
 *         maximum difference would be 0.
 * 
 *         Range: [ 8 , 10 ] There is no prime number in the given range so the
 *         output for the given range would be -1.
 * 
 *         Can you win the game?
 * 
 *         Input Format The first line of input consists of the number of test
 *         cases, T
 * 
 *         Next T lines each consists of two space-separated integers, L and R
 * 
 *         Constraints 1<= T <=10
 * 
 *         2<= L<= R<=10^6
 * 
 *         Output Format For each test case, print the maximum difference in the
 *         given range in a separate line.
 * 
 *         Sample TestCase 1 Input 5 5 5 2 7 8 10 10 20 4 5 Output 0 5 -1 8 0
 *         Explanation Test Case 1: [ 5 - 5 ] = 0 Test Case 2: [ 7 - 2 ] = 5
 *         Test Case 3: No prime number in the given range. Output = -1 Test
 *         Case 4: [ 19 - 11 ] = 8 Test Case 5: The difference would be 0 since
 *         there is only one prime number in the given range.
 *
 * 
 */
public class PrimeNumberGame {

	/**
	 * @param args
	 */
	public static void main(String args[]) throws Exception {

		// Write code here
		Scanner scn = new Scanner(System.in);
		Integer tstCases = Integer.valueOf(scn.nextLine());
		for (int i = 0; i < tstCases; i++) {
			String[] inptString = scn.nextLine().split(" ");
			int start = Integer.valueOf(inptString[0]);
			int end = Integer.valueOf(inptString[1]);
			int minPrime = -1;
			int maxPrime = -1;
			boolean isMin = true;
			boolean isMax = true;
			for (int j = start, k = end; j <= (start + end) / 2; j++, k--) {
				if (isPrimeNumber(j) && isMin) {
					minPrime = j;
					isMin = false;
				}
				if (isPrimeNumber(k) && isMax) {
					maxPrime = k;
					isMax = false;
				}
			}
			if (minPrime == -1 && maxPrime == -1) {
				System.out.println(-1);
			} else if (minPrime == -1 || maxPrime == -1) {
				System.out.println(0);
			} else {
				System.out.println(maxPrime - minPrime);
			}

		}
	}

	public static boolean isPrimeNumber(int number) {
		for (int i = 2; i <= number / 2; i++) {
			if (number % i == 0) {
				return false;
			}
		}
		return true;
	}

}
