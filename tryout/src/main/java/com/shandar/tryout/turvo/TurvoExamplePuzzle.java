/**
 * <p>Project: tryout </p>
 * <p>Package Name: com.shandar.tryout.turvo </p>
 * <p>File Name: TurvoExamplePuzzle.java </p>
 * <p>Create Date: 12-Jan-2022 </p>
 * <p>Create Time: 2:11:28 PM </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */

package com.shandar.tryout.turvo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * @author : Shantanu Sikdar
 *
 *         Bridge Crossing puzzle
 * 
 *         Bridge - constraints 1. At a time, a maximum of 2 people can cross.
 *         2. During the night, one can't cross the bridge w/o a light.
 * 
 *         Now, during a night - A group of 7 people came across this place &
 *         they want to cross the bridge; but they had only one torch among
 *         them.
 * 
 *         The least time in which each person in the group can cross the bridge
 *         is as follows. A B C D E F G 1 75 50 150 100 2 200
 * 
 *         Tasks.
 * 
 *         1. Find the minimum time for the group to cross the bridge. 2. Write
 *         a generic program which accepts any number of people & their time(for
 *         crossing the bridge) and returns the minimum time.
 * 
 */
public class TurvoExamplePuzzle {

	/**
	 * @param args From Sreekumar Aniyappan to Everyone: 02:10 PM
	 * 
	 */
	public static void main(String[] args) {
		System.out.println("Truvo");
		Map<String, Integer> mapPpl = new HashMap<String, Integer>();
		mapPpl.put("A", 1);
		mapPpl.put("B", 75);
		mapPpl.put("C", 50);
		mapPpl.put("D", 150);
		mapPpl.put("E", 100);
		mapPpl.put("F", 2);
		mapPpl.put("G", 200);
		List<Map.Entry<String, Integer>> srtLst = new ArrayList<Map.Entry<String, Integer>>(mapPpl.entrySet());
		Collections.sort(srtLst, new Comparator<Map.Entry<String, Integer>>() {

			public int compare(Map.Entry<String, Integer> pp1, Map.Entry<String, Integer> pp2) {
				return (pp1.getValue()).compareTo(pp2.getValue());
			}
		});
		System.out.println(srtLst);

		Map<String, Integer> mapPplDest = new HashMap<String, Integer>();
		int sum = 0;
		int count = 0;
		// System.out.println(mapPpl.size());
		int least = srtLst.get(0).getValue();
		int least2 = srtLst.get(1).getValue();
		for (int i = srtLst.size() - 1; i > 1;) {
			Entry<String, Integer> sm = srtLst.get(i);
			sum = sm.getValue() + sum + (least + least2) * 2;
			i = i - 2;
		}
		System.out.println(sum);

	}

	/*
	 * a,f = 2min a=1min d,g=200 min f=2min a,f=2min a=1min b,e=100 f=2min a,c=50min
	 * a=1min a,f=2min
	 */
}
