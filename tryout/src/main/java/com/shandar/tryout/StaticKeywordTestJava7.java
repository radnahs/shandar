/**
 *
 * <p>Project: tryout_trunk </p>
 * <p>Package Name: com.shandar.tryout </p>
 * <p>File Name: StaticKeywordTestJava7.java</p>
 * <p>Create Date: Sep 21, 2015 </p>
 * <p>Create Time: 6:50:14 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2012</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.tryout;

/**
 * @author Shantanu Sikdar 
 *
 */
public class StaticKeywordTestJava7 {
	
	static{
		System.out.println("help");
	}
}
