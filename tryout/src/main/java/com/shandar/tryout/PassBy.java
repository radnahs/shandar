/**
 * <p>Project: com.shandar.tryout </p>
 * <p>Package Name: com.shandar.tryout </p>
 * <p>File Name: PassBy.java </p>
 * <p>Create Date: May 25, 2017 </p>
 * <p>Create Time: 7:23:41 PM </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company:  </p>
 * @author Shantanu Sikdar
 * @version 1.0
 */
package com.shandar.tryout;

/**
 * @author: Shantanu Sikdar
 */
public class PassBy {

	public static void main(String[] args) {
		PassBy passBy = new PassBy();
		int arr[] = new int[]{100,200};
		passBy.display(arr);
		passBy.intArrSwap(arr);
		passBy.display(arr);
		
		int x=1,y=2;
		System.out.println("x="+x+" y="+y);
		passBy.intSwap(x, y);
		System.out.println("x="+x+" y="+y);
		
		int age=35;
		String name="Shantanu";
		MyObject myObj = new MyObject(name,age);
		System.out.println(myObj.name+" "+myObj.age);
		passBy.objSwap(myObj);
		System.out.println(myObj.name+" "+myObj.age);
	}
	
	private void display(int[] arr){
		for (int i = 0; i < arr.length; i++) {
			System.out.println("arr["+i+"]="+arr[i]);
		}
	}
	
	private void intArrSwap(int arr[]){
		int k=arr[0];
		arr[0]=arr[1];
		arr[1]=k;
	}
	
	private void intSwap(int x, int y){
		int k=x;
		x=y;
		x=k;
	}
	
	/*private void objSwap(MyObject myObj){
		myObj=new MyObject("Sikdar",38);
		System.out.println(myObj.name+" "+myObj.age);
	}*/
	private void objSwap(MyObject myObj){
		System.out.println(myObj.name+" "+myObj.age);
		myObj.name="sikdar";
		myObj.age=38;
		//System.out.println(myObj.name+" "+myObj.age);
	}
}

class MyObject{
	int age;
	String name;
	
	MyObject(String name,int age){
		this.age=age;
		this.name=name;
	}
	
}
